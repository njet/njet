## Process this file with automake to produce Makefile.in
ACLOCAL_AMFLAGS = -I m4

# pkgconfigdir = $(libdir)/pkgconfig
# pkgconfig_DATA = njet.pc

# DISTCHECK_CONFIGURE_FLAGS = --enable-demo

AM_CXXFLAGS = @NJET_CXXFLAGS@

SUBDIRS = ngluon2 chsums analytic examples

if BUILD_QD
  SUBDIRS += lib-qd
endif
if BUILD_QCDLOOP1
  SUBDIRS += lib-qcdloop1
endif
if ENABLE_DEMO
  SUBDIRS += tools
endif

dist_bin_SCRIPTS = blha/njet.py
lib_LTLIBRARIES = libnjet2.la

libnjet2_la_SOURCES = blha/njet_olp.cpp
EXTRA_libnjet2_la_SOURCES = blha/njet.h blha/njet_olp.h common.h

libnjet2_la_CPPFLAGS = -DUSE_SD
libnjet2_la_LDFLAGS = -no-undefined -version-info @PACKAGE_VERSION_INFO@

libnjet2_la_LIBADD = ngluon2/libngluon2.la chsums/libchsums.la analytic/libanalytic.la
libnjet2_la_DEPENDENCIES = ngluon2/libngluon2.la chsums/libchsums.la analytic/libanalytic.la

if ENABLE_VC
  libnjet2_la_CPPFLAGS += -DUSE_VC
  libnjet2_la_LDFLAGS += -lVc
endif

if ENABLE_DD
  libnjet2_la_CPPFLAGS += -DUSE_DD
endif

if ENABLE_QD
  libnjet2_la_CPPFLAGS += -DUSE_QD
endif

if BUILD_QCDLOOP1
  libnjet2_la_LIBADD += lib-qcdloop1/libqcdloop1.la
  libnjet2_la_DEPENDENCIES += lib-qcdloop1/libqcdloop1.la
endif
if BUILD_QD
  libnjet2_la_LIBADD += lib-qd/src/libqd.la
  libnjet2_la_DEPENDENCIES += lib-qd/src/libqd.la
endif

if HAVE_LD_VERSION_SCRIPT
  # Versioned symbols and restricted exports
  libnjet2_la_LDFLAGS += -Wl,--version-script=$(builddir)/libnjet.ver
  libnjet2_la_DEPENDENCIES += $(builddir)/libnjet.ver
endif

nodist_include_HEADERS = njet.h

EXTRA_DIST = blha/BLHA-doc.txt \
             LICENSE README.rst

CLEANFILES = njet.h
DISTCLEANFILES = $(top_builddir)/version.h
BUILT_SOURCES = version-stamp

njet.h: blha/njet.h
	cp $< $@

.PHONY: version-stamp
version-stamp:
	@if test -d $(top_srcdir)/.git -a -f $(top_srcdir)/.git/HEAD; then \
	  if test ! -f version.h -o $(top_srcdir)/.git -nt version.h -o $(top_srcdir)/.git/HEAD -nt version.h; then \
	    echo "#define HAVE_VERSION_H 1" > version.h.tmp; \
	    echo "#define GIT_VERSION \"`git --git-dir=$(top_srcdir)/.git rev-list --max-count=1 HEAD`\"" >> version.h.tmp; \
	    cmp version.h.tmp version.h && rm -f version.h.tmp || mv version.h.tmp version.h; \
	  fi; \
	else \
	  touch -a version.h; \
	fi

dist-hook: version-stamp
	cp $(top_builddir)/version.h $(distdir)

OLE_contract_%.lh: examples/OLE_order_%.lh $(top_srcdir)/lh/njet.py
	$(top_srcdir)/blha/njet.py -o $@ $<

demo: all
	$(am__cd) tools && $(MAKE) $(AM_MAKEFLAGS) demo

# check-recursive:
# 	@true
