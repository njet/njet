/*
* common.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef NJET_COMMON_H
#define NJET_COMMON_H

#ifdef HAVE_CONFIG_H
  #include <config.h>
#else
  #define PACKAGE_VERSION "2-unknown"
#endif

#endif /* NJET_COMMON_H */
