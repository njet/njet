/*
* examples/ex_4j.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012, 2013 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cmath>
#include <cstdlib>
#include <iostream>

#include "njet.h"

using std::cout;
using std::endl;
using std::ios_base;

int main()
{
  cout.setf(ios_base::scientific, ios_base::floatfield);
  cout.precision(16);

  cout << endl;
  cout << "  NJet: simple example of the BLHA interface" << endl;
  cout << endl;

  const int legs = 6;
  double Momenta[legs][4] = {
    {  0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00,  0.5000000000000000E+03},
    {  0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00, -0.5000000000000000E+03},
    {  0.8855133305450298E+02, -0.2210069028768998E+02,  0.4008035319168533E+02, -0.7580543095693663E+02},
    {  0.3283294192270985E+03, -0.1038496118834563E+03, -0.3019337553895401E+03,  0.7649492138716589E+02},
    {  0.1523581094674306E+03, -0.1058809596665922E+03, -0.9770963832697571E+02,  0.4954838522679282E+02},
    {  0.4307611382509676E+03,  0.2318312618377385E+03,  0.3595630405248305E+03, -0.5023787565702211E+02}
  };

  int status;
  OLP_Start("OLE_contract_4j.lh", &status);
  if (status) {
    cout << "OLP read in correctly" << endl;
  } else {
    cout << "seems to be a problem with the contract file..." << endl;
    exit(1);
  }

  char olpname[15];
  char olpversion[15];
  char olpmessage[255];
  OLP_Info(olpname, olpversion, olpmessage);
  cout << "Running " << olpname
       << " version " << olpversion
       << " note " << olpmessage << endl;

  cout << endl;
  cout << "Notation: Tree     = A0.cA0" << endl;
  cout << "          Loop(-2) = 2*Re(A1.cA0)/eps^2" << endl;
  cout << "          Loop(-1) = 2*Re(A1.cA0)/eps^1" << endl;
  cout << "          Loop( 0) = 2*Re(A1.cA0)/eps^0" << endl;
  cout << endl;

  cout << "==================== Test point ====================" << endl;
  double LHMomenta[legs*5];
  for (int p=0; p<legs; p++) {
    for (int mu=0; mu<4; mu++) {
      LHMomenta[mu+p*5] = Momenta[p][mu];
      cout << Momenta[p][mu] << " ";
    }
    LHMomenta[4+p*5] = 0.;
    cout << endl;
  }
  cout << endl;

  const int channels = 7;
  for (int p=1; p<=channels; p++) {
    double out[7] = {};
    double acc = 0.;
    int rstatus;

    const double alphas = 0.118;
    const double zero = 0.;
    const double mur = 91.188;

    OLP_SetParameter("alphas", &alphas, &zero, &rstatus);
    if (rstatus == 1) {
      cout << "Setting AlphaS: OK" << endl;
    } else if (rstatus == 0) {
      cout << "Setting AlphaS: FAIL" << endl;
    } else {
      cout << "Setting AlphaS: UNKNOWN" << endl;
      exit(2);
    }
    OLP_EvalSubProcess2(&p, LHMomenta, &mur, out, &acc);

    cout << "---- process number " << p << " ----" << endl;
    cout << "OLP accuracy check: " << acc << endl;

    cout << "Tree            = " << out[3] << endl;
    cout << "Loop(-2)        = " << out[0] << endl;
    cout << "Loop(-1)        = " << out[1] << endl;
    cout << "Loop( 0)        = " << out[2] << endl;
    cout << "Loop( 0)/Tree   = " << out[2]/out[3] << endl;
    cout << " only if NJetReturnAccuracy is used:"<< endl;
    cout << "Loop(-2) error  = " << out[4] << endl;
    cout << "Loop(-1) error  = " << out[5] << endl;
    cout << "Loop( 0) error  = " << out[6] << endl;
    cout << endl;
  }

  return 0;
}
