/*
* examples/ex_3j_cc.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012, 2013 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cmath>
#include <cstdlib>
#include <iostream>

#include "njet.h"

using std::cout;
using std::endl;
using std::ios_base;

static int nis(int i, int j)
{
  return ( i<=j ? i+j*(j-1)/2 : j+i*(i-1)/2 );
}

int main()
{
  cout.setf(ios_base::scientific, ios_base::floatfield);
  cout.precision(16);

  cout << endl;
  cout << "  NJet: simple example of the ccTree BLHA interface" << endl;
  cout << endl;

  const int legs = 5;
  const int pspoints = 3;
  double Momenta[pspoints][legs][4] = {
    {
      {0.5000000000000000E+03,0.0000000000000000E+00,0.0000000000000000E+00,0.5000000000000000E+03},
      {0.5000000000000000E+03,0.0000000000000000E+00,0.0000000000000000E+00,-0.5000000000000000E+03},
      {0.4585787878854402E+03,0.1694532203096798E+03,0.3796536620781987E+03,-0.1935024746502525E+03},
      {0.3640666207368177E+03,-0.1832986929319185E+02,-0.3477043013193671E+03,0.1063496077587081E+03},
      {0.1773545913777421E+03,-0.1511233510164880E+03,-0.3194936075883156E+02,0.8715286689154436E+02}
    },{
      {0.5000000000000000E+03,0.0000000000000000E+00,0.0000000000000000E+00,0.5000000000000000E+03},
      {0.5000000000000000E+03,0.0000000000000000E+00,0.0000000000000000E+00,-0.5000000000000000E+03},
      {0.4951533920773834E+03,0.1867229157692120E+03,0.3196835780850242E+03,-0.3288066974913060E+03},
      {0.1026779138750091E+03,-0.7062042730772224E+02,-0.4345595295696615E+02,0.6055649756384902E+02},
      {0.4021686940476072E+03,-0.1161024884614897E+03,-0.2762276251280580E+03,0.2682501999274569E+03}
    },{
      {2.5000000000000000e+02,0.0000000000000000e+00,0.0000000000000000e+00,2.5000000000000000e+02},
      {2.5000000000000000e+02,0.0000000000000000e+00,0.0000000000000000e+00,-2.5000000000000000e+02},
      {1.5943717455296405e+02,-9.6097656622523473e+01,1.0897987122994738e+02,-6.5641760242973277e+01},
      {2.4437697027369072e+02,1.2354774195573731e+02,-2.0113465489985822e+02,6.3252744257477040e+01},
      {9.6185855173345232e+01,-2.7450085333213842e+01,9.2154783669910842e+01,2.3890159854962425e+00}
    }
  };

  int status;
  OLP_Start("OLE_contract_3j_cc.lh", &status);
  if (status) {
    cout << "OLP read in correctly" << endl;
  } else {
    cout << "seems to be a problem with the contract file..." << endl;
    exit(1);
  }

  char olpname[15];
  char olpversion[15];
  char olpmessage[255];
  OLP_Info(olpname, olpversion, olpmessage);
  cout << "Running " << olpname
       << " version " << olpversion
       << " note " << olpmessage << endl;

  for (int pts=0; pts<pspoints; pts++) {
    cout << "==================== Test point " << pts+1 << " ====================" << endl;
    double LHMomenta[legs*5];
    for (int p=0; p<legs; p++) {
      for (int mu=0; mu<4; mu++ ) {
        LHMomenta[mu+p*5] = Momenta[pts][p][mu];
        cout << Momenta[pts][p][mu] << " ";
      }
      LHMomenta[4+p*5] = 0.;
      cout << endl;
    }
    cout << endl;

    const int channels = 13;
    for (int p=1; p<=channels; p++) {
      double out[legs*(legs-1)/2] = {};
      double acc = 0.;
      int rstatus;

      const double alphas = 0.118;
      const double zero = 0.;
      const double mur = 91.188;

      OLP_SetParameter("alphas", &alphas, &zero, &rstatus);
      if (rstatus == 1) {
        cout << "Setting AlphaS = " << alphas << ": OK" << endl;
      } else if (rstatus == 0) {
        cout << "Setting AlphaS: FAIL" << endl;
      } else {
        cout << "Setting AlphaS: UNKNOWN" << endl;
        exit(2);
      }
      OLP_EvalSubProcess2(&p, LHMomenta, &mur, out, &acc);

      cout << "---- process number " << p << " ---- muR = " << mur << endl;
      cout << "OLP accuracy check: " << acc << endl;

      for (int i=0; i<legs; i++) {
        for (int j=0; j<legs; j++) {
          if (i == j) continue;
          cout << "A(" << i << "," << j << ") = " << out[nis(i,j)] << endl;
        }
      }
      cout << endl;
    }
    cout << endl;
  }

  return 0;
}
