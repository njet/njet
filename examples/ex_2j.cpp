/*
* examples/ex_2j.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012, 2013 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cmath>
#include <cstdlib>
#include <iostream>

#include "njet.h"

using std::cout;
using std::endl;
using std::ios_base;

int main()
{
  cout.setf(ios_base::scientific, ios_base::floatfield);
  cout.precision(16);

  cout << endl;
  cout << "  NJet: simple example of the BLHA interface" << endl;
  cout << endl;

  const int legs = 4;
  const int pspoints = 5;
  double Momenta[pspoints][legs][4] = {
    {
      { 0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00,  0.5000000000000000E+03},
      { 0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00, -0.5000000000000000E+03},
      { 0.4999999999999998E+03,  0.1109242844438328E+03,  0.4448307894881214E+03, -0.1995529299308788E+03},
      { 0.5000000000000000E+03, -0.1109242844438328E+03, -0.4448307894881214E+03,  0.1995529299308787E+03},
    },
    {
      { 0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00,  0.5000000000000000E+03},
      { 0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00, -0.5000000000000000E+03},
      { 0.5000000000000000E+03, -0.2819155058093908E+03, -0.3907909666011574E+03,  0.1334393795218208E+03},
      { 0.5000000000000002E+03,  0.2819155058093908E+03,  0.3907909666011575E+03, -0.1334393795218208E+03},
    },
    {
      { 0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00,  0.5000000000000000E+03},
      { 0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00, -0.5000000000000000E+03},
      { 0.4999999999999990E+03, -0.2406210290937675E+03,  0.2804720059911282E+03, -0.3368040590805997E+03},
      { 0.5000000000000012E+03,  0.2406210290937674E+03, -0.2804720059911282E+03,  0.3368040590805997E+03},
    },
    {
      { 0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00,  0.5000000000000000E+03},
      { 0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00, -0.5000000000000000E+03},
      { 0.5000000000000001E+03,  0.4510352131035046E+03,  0.6047968715179463E+02, -0.2071459485065961E+03},
      { 0.5000000000000001E+03, -0.4510352131035046E+03, -0.6047968715179463E+02,  0.2071459485065960E+03},
    },
    {
      { 0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00,  0.5000000000000000E+03},
      { 0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00, -0.5000000000000000E+03},
      { 0.5000000000000007E+03,  0.4557289838719782E+03, -0.2036894622960882E+03, -0.2866524391218178E+02},
      { 0.5000000000000007E+03, -0.4557289838719784E+03,  0.2036894622960884E+03,  0.2866524391218240E+02},
    }
  };

  int status;
  OLP_Start("OLE_contract_2j.lh", &status);
  if (status) {
    cout << "OLP read in correctly" << endl;
  } else {
    cout << "seems to be a problem with the contract file..." << endl;
    exit(1);
  }

  char olpname[15];
  char olpversion[15];
  char olpmessage[255];
  OLP_Info(olpname, olpversion, olpmessage);
  cout << "Running " << olpname
       << " version " << olpversion
       << " note " << olpmessage << endl;

  cout << endl;
  cout << "Notation: Tree     = A0.cA0" << endl;
  cout << "          Loop(-2) = 2*Re(A1.cA0)/eps^2" << endl;
  cout << "          Loop(-1) = 2*Re(A1.cA0)/eps^1" << endl;
  cout << "          Loop( 0) = 2*Re(A1.cA0)/eps^0" << endl;
  cout << endl;

  for (int pts=0; pts<pspoints; pts++) {
    cout << "==================== Test point " << pts+1 << " ====================" << endl;
    double LHMomenta[legs*5];
    for (int p=0; p<legs; p++) {
      for (int mu=0; mu<4; mu++ ) {
        LHMomenta[mu+p*5] = Momenta[pts][p][mu];
        cout << Momenta[pts][p][mu] << " ";
      }
      LHMomenta[4+p*5] = 0.;
      cout << endl;
    }
    cout << endl;

    const int channels = 8;
    for (int p=1; p<=channels; p++) {
      double out[7] = {};
      double acc = 0.;
      int rstatus;

      const double alphas = 0.118;
      const double zero = 0.;
      const double mur = 91.188;

      OLP_SetParameter("alphas", &alphas, &zero, &rstatus);
      if (rstatus == 1) {
        cout << "Setting AlphaS: OK" << endl;
      } else if (rstatus == 0) {
        cout << "Setting AlphaS: FAIL" << endl;
      } else {
        cout << "Setting AlphaS: UNKNOWN" << endl;
        exit(2);
      }
      OLP_EvalSubProcess2(&p, LHMomenta, &mur, out, &acc);

      cout << "---- process number " << p << " ----" << endl;
      cout << "OLP accuracy check: " << acc << endl;

      cout << "Tree            = " << out[3] << endl;
      cout << "Loop(-2)        = " << out[0] << endl;
      cout << "Loop(-1)        = " << out[1] << endl;
      cout << "Loop( 0)        = " << out[2] << endl;
      cout << "Loop( 0)/Tree   = " << out[2]/out[3] << endl;
      cout << " only if NJetReturnAccuracy is used:"<< endl;
      cout << "Loop(-2) error  = " << out[4] << endl;
      cout << "Loop(-1) error  = " << out[5] << endl;
      cout << "Loop( 0) error  = " << out[6] << endl;
      cout << endl;
    }
    cout << endl;
  }

  cout << endl;

  return 0;
}
