/*
* analytic/0q5gH-analytic.h
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef ANALYTIC_0Q5GH_ANALYTIC_H
#define ANALYTIC_0Q5GH_ANALYTIC_H

#include "../chsums/0q5gH.h"

template <typename T>
class Amp0q5gH_a : public Amp0q5gH<T>
{
    typedef Amp0q5gH<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    typedef TreeValue (Amp0q5gH_a::*HelAmp)(const int* ord);

    Amp0q5gH_a(const T scalefactor);

  protected:
    using BaseClass::mhel;
    using BaseClass::mhelint;

    using BaseClass::njetan;
    using BaseClass::callTree;
    using BaseClass::callLoop;
    using BaseClass::lS;
    using BaseClass::sA;
    using BaseClass::sB;
    using BaseClass::sAB;
    using BaseClass::CyclicSpinorsA;
    using BaseClass::CyclicSpinorsB;
    using BaseClass::HelicityOrder;

    virtual TreeValue A0(int p0, int p1, int p2, int p3, int p4);

    HelAmp hA0[32];

    TreeValue hA00(const int* ord);
    TreeValue hA01(const int* ord);
    TreeValue hA02(const int* ord);
    TreeValue hA03(const int* ord);
    TreeValue hA04(const int* ord);
    TreeValue hA05(const int* ord);
    TreeValue hA06(const int* ord);
    TreeValue hA07(const int* ord);
    TreeValue hA08(const int* ord);
    TreeValue hA09(const int* ord);
    TreeValue hA010(const int* ord);
    TreeValue hA011(const int* ord);
    TreeValue hA012(const int* ord);
    TreeValue hA013(const int* ord);
    TreeValue hA014(const int* ord);
    TreeValue hA015(const int* ord);
    TreeValue hA016(const int* ord);
    TreeValue hA017(const int* ord);
    TreeValue hA018(const int* ord);
    TreeValue hA019(const int* ord);
    TreeValue hA020(const int* ord);
    TreeValue hA021(const int* ord);
    TreeValue hA022(const int* ord);
    TreeValue hA023(const int* ord);
    TreeValue hA024(const int* ord);
    TreeValue hA025(const int* ord);
    TreeValue hA026(const int* ord);
    TreeValue hA027(const int* ord);
    TreeValue hA028(const int* ord);
    TreeValue hA029(const int* ord);
    TreeValue hA030(const int* ord);
    TreeValue hA031(const int* ord);
    // temporary variables
    std::complex<T> XX[51];
};

#endif /* ANALYTIC_0Q5GH_ANALYTIC_H */
