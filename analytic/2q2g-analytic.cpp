/*
* analytic/2q2g-analytic.cpp
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>
#include "2q2g-analytic.h"

template <typename T>
Amp2q2g_a<T>::Amp2q2g_a(const T scalefactor)
  : BaseClass(scalefactor), hA0()
{
  njetan = new NJetAnalytic<T>(scalefactor, BaseClass::legsMOM());
  assert(int(sizeof(hA0)/sizeof(hA0[0])) >= (1 << njetan->legsMOM()));
  assert(int(sizeof(hAL1)/sizeof(hAL1[0])) >= (1 << njetan->legsMOM()));
  assert(int(sizeof(hAL2)/sizeof(hAL2[0])) >= (1 << njetan->legsMOM()));
  assert(int(sizeof(hAL3)/sizeof(hAL3[0])) >= (1 << njetan->legsMOM()));
  assert(int(sizeof(hAf1)/sizeof(hAf1[0])) >= (1 << njetan->legsMOM()));
//   BaseClass::clearNG();

  hA0[5]  = &Amp2q2g_a::hA05;
  hA0[6]  = &Amp2q2g_a::hA06;
  hA0[9]  = &Amp2q2g_a::hA09;
  hA0[10]  = &Amp2q2g_a::hA010;

  hAL1[1]  = &Amp2q2g_a::hAL11;
  hAL1[2]  = &Amp2q2g_a::hAL12;
  hAL1[5]  = &Amp2q2g_a::hAL15;
  hAL1[6]  = &Amp2q2g_a::hAL16;
  hAL1[9]  = &Amp2q2g_a::hAL19;
  hAL1[10]  = &Amp2q2g_a::hAL110;
  hAL1[13]  = &Amp2q2g_a::hAL113;
  hAL1[14]  = &Amp2q2g_a::hAL114;

  hAL2[1]  = &Amp2q2g_a::hAL21;
  hAL2[3]  = &Amp2q2g_a::hAL23;
  hAL2[4]  = &Amp2q2g_a::hAL24;
  hAL2[6]  = &Amp2q2g_a::hAL26;
  hAL2[9]  = &Amp2q2g_a::hAL29;
  hAL2[11]  = &Amp2q2g_a::hAL211;
  hAL2[12]  = &Amp2q2g_a::hAL212;
  hAL2[14]  = &Amp2q2g_a::hAL214;

  hAL3[1]  = &Amp2q2g_a::hAL31;
  hAL3[3]  = &Amp2q2g_a::hAL33;
  hAL3[5]  = &Amp2q2g_a::hAL35;
  hAL3[7]  = &Amp2q2g_a::hAL37;
  hAL3[8]  = &Amp2q2g_a::hAL38;
  hAL3[10]  = &Amp2q2g_a::hAL310;
  hAL3[12]  = &Amp2q2g_a::hAL312;
  hAL3[14]  = &Amp2q2g_a::hAL314;

  hAf1[1]  = &Amp2q2g_a::hAf11;
  hAf1[2]  = &Amp2q2g_a::hAf12;
  hAf1[5]  = &Amp2q2g_a::hAf15;
  hAf1[6]  = &Amp2q2g_a::hAf16;
  hAf1[9]  = &Amp2q2g_a::hAf19;
  hAf1[10]  = &Amp2q2g_a::hAf110;
  hAf1[13]  = &Amp2q2g_a::hAf113;
  hAf1[14]  = &Amp2q2g_a::hAf114;

}

template <typename T>
typename Amp2q2g_a<T>::TreeValue
Amp2q2g_a<T>::A0(int p0, int p1, int p2, int p3)
{
  const int ord[] = {p0, p1, p2, p3};
  const int hel = HelicityOrder(mhelint, ord);
  HelAmp const hamp = hA0[hel];
  if (hamp) {
    return i_*callTree(this, hamp, ord);
  } else {
    return TreeValue(0.);
  }
}

template <typename T>
LoopResult<T> Amp2q2g_a<T>::AL(int p0, int p1, int p2, int p3)
{
  const int ord[] = {p0, p1, p2, p3};
  const int hel = HelicityOrder(mhelint, ord);
  const int f0 = getFlav(0,p0);
  const int f1 = getFlav(0,p1);
  const int f2 = getFlav(0,p2);
  const int f3 = getFlav(0,p3);
  if (f0 == -1 && f1 == 1) {
    HelAmpLoop const hamp = hAL1[hel];
    if (hamp) {
      return callLoop(this, hamp, ord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -1 && f2 == 1) {
    HelAmpLoop const hamp = hAL2[hel];
    if (hamp) {
      return callLoop(this, hamp, ord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -1 && f3 == 1) {
    HelAmpLoop const hamp = hAL3[hel];
    if (hamp) {
      return callLoop(this, hamp, ord);
    } else {
      return LoopResult<T>();
    }
  } else {
    return NJetAmp4<T>::AL(p0, p1, p2, p3);
  }
}

template <typename T>
LoopResult<T> Amp2q2g_a<T>::AF(int p0, int p1, int p2, int p3)
{
  const int ord[] = {p0, p1, p2, p3};
  const int hel = HelicityOrder(mhelint, ord);
  const int f0 = getFlav(0,p0);
  const int f1 = getFlav(0,p1);
  const int f2 = getFlav(0,p2);
  const int f3 = getFlav(0,p3);
  if (f0 == -1 && f1 == 1) {
    HelAmpLoop const hamp = hAf1[hel];
    if (hamp) {
      return callLoop(this, hamp, ord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -1 && f2 == 1) {
    return LoopResult<T>();
  } else if (f0 == -1 && f3 == 1) {
    return LoopResult<T>();
  } else {
    return NJetAmp4<T>::AF(p0, p1, p2, p3);
  }
}

template <typename T>
typename Amp2q2g_a<T>::TreeValue
Amp2q2g_a<T>::hA05(const int* p) /* +-+- */
{
  return pow(sA(p[1], p[3]), 3) * sA(p[0], p[3]) / CyclicSpinorsA(p);
}

template <typename T>
typename Amp2q2g_a<T>::TreeValue
Amp2q2g_a<T>::hA06(const int* p) /* -++- */
{
  return -pow(sA(p[0], p[3]), 3) * sA(p[1], p[3]) / CyclicSpinorsA(p);
}

template <typename T>
typename Amp2q2g_a<T>::TreeValue
Amp2q2g_a<T>::hA09(const int* p) /* +--+ */
{
  return pow(sA(p[1], p[2]), 3) * sA(p[0], p[2]) / CyclicSpinorsA(p);
}

template <typename T>
typename Amp2q2g_a<T>::TreeValue
Amp2q2g_a<T>::hA010(const int* p) /* -+-+ */
{
  return -pow(sA(p[0], p[2]), 3) * sA(p[1], p[2]) / CyclicSpinorsA(p);
}

// "left-moving" primitives = 3 gluon propagators
template <typename T>
LoopResult<T> Amp2q2g_a<T>::hAL11(const int* p) /* +--- */
{
  const std::complex<T> phase = i_*sB(p[0], p[1])*sA(p[1], p[3])/(sB(p[1], p[2])*sB(p[2], p[3]));
  const T s = lS(p[0], p[1]);
  const T t = lS(p[1], p[2]);
  const T rat = T(0.5) + T(1.)/T(3.)*t/s;
  const EpsTriplet<T> amp(phase*rat, T(0.), T(0.));
  const EpsTriplet<T> camp(conj(phase)*rat, T(0.), T(0.));
  const LoopResult<T> res = {amp, camp};
  return res;
}

template <typename T>
LoopResult<T> Amp2q2g_a<T>::hAL12(const int* p) /* -+-- */
{
  const std::complex<T> phase = -i_*sB(p[0], p[1])*sA(p[0], p[2])/(sB(p[0], p[3])*sB(p[2], p[3]));
  const T s = lS(p[0], p[1]);
  const T t = lS(p[1], p[2]);
  const T rat = T(0.5) + T(1.)/T(3.)*t/s;
  const EpsTriplet<T> amp(phase*rat, T(0.), T(0.));
  const EpsTriplet<T> camp(conj(phase)*rat, T(0.), T(0.));
  const LoopResult<T> res = {amp, camp};
  return res;
}

template <typename T>
LoopResult<T> Amp2q2g_a<T>::hAL15(const int* p) /* +-+- */
{
  const std::complex<T> phase = i_*hA05(p);
  const T s = lS(p[0], p[1]);
  const T t = lS(p[1], p[2]);
  const T u = -s-t;
  const T s2 = s*s;
  const T t2 = t*t;
  const T st = s*t;
  const T u2 = u*u;
  const T u3 = u2*u;
  const T x1 = s2 + t2 + st;
  const T x2 = s2 + T(3.)*t2 + T(3.)*st;

  const T c4 = T(0.5)*st*x1*(s+T(2.)*t)/u3;
  const T c3s = -st*t2/u3;
  const T c3t = st*x2/u3;
  const T c2s = -T(0.5)*t*(s+T(3.)*t)/u2;
  const T c2t = -T(0.5)*s*(T(3.)*s+T(5.)*t)/u2;
  const T rat = -T(0.5)*t/u;
  EpsTriplet<T> amp;

  amp += c4*njetan->I4(s, t, T(0.), T(0.), T(0.), T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c3s*njetan->I3(s, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c3t*njetan->I3(t, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c2s*njetan->I2(s, T(0.), T(0.));
  amp += c2t*njetan->I2(t, T(0.), T(0.));
  amp += rat;

  const LoopResult<T> res = {phase*amp, conj(phase)*amp};
  return res;
}

template <typename T>
LoopResult<T> Amp2q2g_a<T>::hAL16(const int* p) /* -++- */
{
  const std::complex<T> phase = i_*hA06(p);
  const T s = lS(p[0], p[1]);
  const T t = lS(p[1], p[2]);
  const T u = -s-t;
  const T st = s*t;

  const T c4 = T(0.5)*st*(s+T(2.)*t)/u;
  const T c3 = -st/u;
  const T c2s = -T(1.5);
  const T rat = T(0.5);
  EpsTriplet<T> amp;

  amp += c4*njetan->I4(s, t, T(0.), T(0.), T(0.), T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c3*njetan->I3(s, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp -= c3*njetan->I3(t, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c2s*njetan->I2(s, T(0.), T(0.));
  amp += rat;

  const LoopResult<T> res = {phase*amp, conj(phase)*amp};
  return res;
}

template <typename T>
LoopResult<T> Amp2q2g_a<T>::hAL19(const int* p) /* +--+ */
{
  const std::complex<T> phase = i_*hA09(p);
  const T s = lS(p[0], p[1]);
  const T t = lS(p[1], p[2]);
  const T u = -s-t;
  const T st = s*t;

  const T c4 = T(0.5)*st*(s+T(2.)*t)/u;
  const T c3 = -st/u;
  const T c2s = -T(1.5);
  const T rat = T(0.5);
  EpsTriplet<T> amp;

  amp += c4*njetan->I4(s, t, T(0.), T(0.), T(0.), T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c3*njetan->I3(s, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp -= c3*njetan->I3(t, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c2s*njetan->I2(s, T(0.), T(0.));
  amp += rat;

  const LoopResult<T> res = {phase*amp, conj(phase)*amp};
  return res;
}

template <typename T>
LoopResult<T> Amp2q2g_a<T>::hAL110(const int* p) /* -+-+ */
{
  const std::complex<T> phase = i_*hA010(p);
  const T s = lS(p[0], p[1]);
  const T t = lS(p[1], p[2]);
  const T u = -s-t;
  const T s2 = s*s;
  const T t2 = t*t;
  const T st = s*t;
  const T u2 = u*u;
  const T u3 = u2*u;
  const T x1 = s2 + t2 + st;
  const T x2 = s2 + T(3.)*t2 + T(3.)*st;

  const T c4 = T(0.5)*st*x1*(s+T(2.)*t)/u3;
  const T c3s = -st*t2/u3;
  const T c3t = st*x2/u3;
  const T c2s = -T(0.5)*t*(s+T(3.)*t)/u2;
  const T c2t = -T(0.5)*s*(T(3.)*s+T(5.)*t)/u2;
  const T rat = -T(0.5)*t/u;
  EpsTriplet<T> amp;

  amp += c4*njetan->I4(s, t, T(0.), T(0.), T(0.), T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c3s*njetan->I3(s, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c3t*njetan->I3(t, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c2s*njetan->I2(s, T(0.), T(0.));
  amp += c2t*njetan->I2(t, T(0.), T(0.));
  amp += rat;

  const LoopResult<T> res = {phase*amp, conj(phase)*amp};
  return res;
}

template <typename T>
LoopResult<T> Amp2q2g_a<T>::hAL113(const int* p) /* +-++ */
{
  const std::complex<T> phase = -i_*sA(p[0], p[1])*sB(p[0], p[2])/(sA(p[0], p[3])*sA(p[2], p[3]));
  const T s = lS(p[0], p[1]);
  const T t = lS(p[1], p[2]);
  const T rat = T(0.5) + T(1.)/T(3.)*t/s;
  const EpsTriplet<T> amp(phase*rat, T(0.), T(0.));
  const EpsTriplet<T> camp(conj(phase)*rat, T(0.), T(0.));
  const LoopResult<T> res = {amp, camp};
  return res;
}

template <typename T>
LoopResult<T> Amp2q2g_a<T>::hAL114(const int* p) /* -+++ */
{
  const std::complex<T> phase = i_*sA(p[0], p[1])*sB(p[1], p[3])/(sA(p[1], p[2])*sA(p[2], p[3]));
  const T s = lS(p[0], p[1]);
  const T t = lS(p[1], p[2]);
  const T rat = T(0.5) + T(1.)/T(3.)*t/s;
  const EpsTriplet<T> amp(phase*rat, T(0.), T(0.));
  const EpsTriplet<T> camp(conj(phase)*rat, T(0.), T(0.));
  const LoopResult<T> res = {amp, camp};
  return res;
}

// "right-moving" primitives = 1 gluon propagator
template <typename T>
LoopResult<T> Amp2q2g_a<T>::hAL31(const int* p) /* +--- */
{
  const std::complex<T> phase = i_*sB(p[0], p[3])*sA(p[1], p[3])/(sB(p[1], p[2])*sB(p[2], p[3]));
//   const T s = lS(p[0], p[1]);
//   const T t = lS(p[1], p[2]);
  const T rat = T(0.5);
  const EpsTriplet<T> amp(phase*rat, T(0.), T(0.));
  const EpsTriplet<T> camp(conj(phase)*rat, T(0.), T(0.));
  const LoopResult<T> res = {amp, camp};
  return res;
}
template <typename T>
LoopResult<T> Amp2q2g_a<T>::hAL33(const int* p) /* ++-- */
{
  const std::complex<T> phase = i_*pow(sB(p[0], p[1]), 3) * sB(p[3], p[1]) / CyclicSpinorsB(p);
  const T s = lS(p[0], p[1]);
  const T t = lS(p[1], p[2]);
  const T u = -s-t;
  const T st = s*t;

  const T c4 = -T(0.5)*st*t/u;
  const T c3 = st/u;
  const T c2t = T(1.5);
  const T rat = T(0.5);

  EpsTriplet<T> amp;
  amp += c4*njetan->I4(s, t, T(0.), T(0.), T(0.), T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c3*njetan->I3(s, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp -= c3*njetan->I3(t, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c2t*njetan->I2(t, T(0.), T(0.));
  amp += rat;

  const LoopResult<T> res = {phase*amp, conj(phase)*amp};
  return res;
}
template <typename T>
LoopResult<T> Amp2q2g_a<T>::hAL35(const int* p) /* +-+- */
{
  const std::complex<T> phase = i_*pow(sB(p[0], p[2]), 3) * sB(p[3], p[2]) / CyclicSpinorsB(p);
  const T s = lS(p[0], p[1]);
  const T t = lS(p[1], p[2]);
  const T u = -s-t;
  const T s2 = s*s;
  const T t2 = t*t;
  const T u2 = u*u;
  const T u3 = u2*u;
  const T st = s*t;

  const T c4 = -T(0.5)*st*t2*t/u3;
  const T c3s = st*t2/u3;
  const T c3t = -st*(s2+T(3.)*(st+t2))/u3;
  const T c2s = T(0.5)*t*(s+T(3.)*t)/u2;
  const T c2t = T(0.5)*s*(T(3.)*s+T(5.)*t)/u2;
  const T rat = -T(0.5)*s/u;

  EpsTriplet<T> amp;
  amp += c4*njetan->I4(s, t, T(0.), T(0.), T(0.), T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c3s*njetan->I3(s, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c3t*njetan->I3(t, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c2s*njetan->I2(s, T(0.), T(0.));
  amp += c2t*njetan->I2(t, T(0.), T(0.));
  amp += rat;

  const LoopResult<T> res = {phase*amp, conj(phase)*amp};
  return res;
}
template <typename T>
LoopResult<T> Amp2q2g_a<T>::hAL37(const int* p) /* +++- */
{
  const std::complex<T> phase = i_*sA(p[0], p[3])*sB(p[2], p[0])/(sA(p[0], p[1])*sA(p[1], p[2]));
//   const T s = lS(p[0], p[1]);
//   const T t = lS(p[1], p[2]);
  const T rat = T(0.5);
  const EpsTriplet<T> amp(phase*rat, T(0.), T(0.));
  const EpsTriplet<T> camp(conj(phase)*rat, T(0.), T(0.));
  const LoopResult<T> res = {amp, camp};
  return res;
}
template <typename T>
LoopResult<T> Amp2q2g_a<T>::hAL38(const int* p) /* ---+ */
{
  const std::complex<T> phase = i_*sB(p[0], p[3])*sA(p[2], p[0])/(sB(p[0], p[1])*sB(p[1], p[2]));
//   const T s = lS(p[0], p[1]);
//   const T t = lS(p[1], p[2]);
  const T rat = T(0.5);
  const EpsTriplet<T> amp(phase*rat, T(0.), T(0.));
  const EpsTriplet<T> camp(conj(phase)*rat, T(0.), T(0.));
  const LoopResult<T> res = {amp, camp};
  return res;
}
template <typename T>
LoopResult<T> Amp2q2g_a<T>::hAL310(const int* p) /* -+-+ */
{
  const std::complex<T> phase = i_*pow(sA(p[0], p[2]), 3) * sA(p[3], p[2]) / CyclicSpinorsA(p);
  const T s = lS(p[0], p[1]);
  const T t = lS(p[1], p[2]);
  const T u = -s-t;
  const T s2 = s*s;
  const T t2 = t*t;
  const T u2 = u*u;
  const T u3 = u2*u;
  const T st = s*t;

  const T c4 = -T(0.5)*st*t2*t/u3;
  const T c3s = st*t2/u3;
  const T c3t = -st*(s2+T(3.)*(st+t2))/u3;
  const T c2s = T(0.5)*t*(s+T(3.)*t)/u2;
  const T c2t = T(0.5)*s*(T(3.)*s+T(5.)*t)/u2;
  const T rat = -T(0.5)*s/u;

  EpsTriplet<T> amp;
  amp += c4*njetan->I4(s, t, T(0.), T(0.), T(0.), T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c3s*njetan->I3(s, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c3t*njetan->I3(t, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c2s*njetan->I2(s, T(0.), T(0.));
  amp += c2t*njetan->I2(t, T(0.), T(0.));
  amp += rat;

  const LoopResult<T> res = {phase*amp, conj(phase)*amp};
  return res;
}
template <typename T>
LoopResult<T> Amp2q2g_a<T>::hAL312(const int* p) /* --++ */
{
  const std::complex<T> phase = i_*pow(sA(p[0], p[1]), 3) * sA(p[3], p[1]) / CyclicSpinorsA(p);
  const T s = lS(p[0], p[1]);
  const T t = lS(p[1], p[2]);
  const T u = -s-t;
  const T st = s*t;

  const T c4 = -T(0.5)*st*t/u;
  const T c3 = st/u;
  const T c2t = T(1.5);
  const T rat = T(0.5);

  EpsTriplet<T> amp;
  amp += c4*njetan->I4(s, t, T(0.), T(0.), T(0.), T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c3*njetan->I3(s, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp -= c3*njetan->I3(t, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c2t*njetan->I2(t, T(0.), T(0.));
  amp += rat;

  const LoopResult<T> res = {phase*amp, conj(phase)*amp};
  return res;
}
template <typename T>
LoopResult<T> Amp2q2g_a<T>::hAL314(const int* p) /* -+++ */
{
  const std::complex<T> phase = i_*sA(p[0], p[3])*sB(p[1], p[3])/(sA(p[1], p[2])*sA(p[2], p[3]));
//   const T s = lS(p[0], p[1]);
//   const T t = lS(p[1], p[2]);
  const T rat = T(0.5);
  const EpsTriplet<T> amp(phase*rat, T(0.), T(0.));
  const EpsTriplet<T> camp(conj(phase)*rat, T(0.), T(0.));
  const LoopResult<T> res = {amp, camp};
  return res;
}

// sub-leading colour "left-moving" primitives = 2 gluon propagators
template <typename T>
LoopResult<T> Amp2q2g_a<T>::hAL21(const int* p) /* +--- */
{
  const std::complex<T> phase = i_*sB(p[0], p[2])*sA(p[1], p[3])/(sB(p[1], p[2])*sB(p[2], p[3]));
  const T rat = T(0.5);
  const EpsTriplet<T> amp(phase*rat, T(0.), T(0.));
  const EpsTriplet<T> camp(conj(phase)*rat, T(0.), T(0.));
  const LoopResult<T> res = {amp, camp};
  return res;
}
template <typename T>
LoopResult<T> Amp2q2g_a<T>::hAL23(const int* p) /* ++-- */
{
  const std::complex<T> phase = i_*pow(sB(p[0], p[1]), 3) * sB(p[2], p[1]) / CyclicSpinorsB(p);
  const T s = lS(p[0], p[1]);
  const T t = lS(p[1], p[2]);
//   const T u = -s-t;

  const T c4 = T(0.5)*s*t;
  const T c2t = T(1.5);
  EpsTriplet<T> amp;

  amp += c4*njetan->I4(s, t, T(0.), T(0.), T(0.), T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c2t*njetan->I2(t, T(0.), T(0.));

  const LoopResult<T> res = {phase*amp, conj(phase)*amp};
  return res;
}
template <typename T>
LoopResult<T> Amp2q2g_a<T>::hAL24(const int* p) /* --+- */
{
  const std::complex<T> phase = i_*sB(p[0], p[2])*sA(p[1], p[3])/(sB(p[3], p[0])*sB(p[0], p[1]));
  const T rat = T(0.5);
  const EpsTriplet<T> amp(phase*rat, T(0.), T(0.));
  const EpsTriplet<T> camp(conj(phase)*rat, T(0.), T(0.));
  const LoopResult<T> res = {amp, camp};
  return res;
}
template <typename T>
LoopResult<T> Amp2q2g_a<T>::hAL26(const int* p) /* -++- */
{
  const std::complex<T> phase = i_*pow(sA(p[0], p[3]), 3) * sA(p[2], p[3]) / CyclicSpinorsA(p);
  const T s = lS(p[0], p[1]);
  const T t = lS(p[1], p[2]);
//   const T u = -s-t;

  const T c4 = T(0.5)*s*t;
  const T c2s = T(1.5);
  EpsTriplet<T> amp;

  amp += c4*njetan->I4(s, t, T(0.), T(0.), T(0.), T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c2s*njetan->I2(s, T(0.), T(0.));

  const LoopResult<T> res = {phase*amp, conj(phase)*amp};
  return res;
}
template <typename T>
LoopResult<T> Amp2q2g_a<T>::hAL29(const int* p) /* +--+ */
{
  const std::complex<T> phase = i_*pow(sB(p[0], p[3]), 3) * sB(p[2], p[3]) / CyclicSpinorsB(p);
  const T s = lS(p[0], p[1]);
  const T t = lS(p[1], p[2]);
//   const T u = -s-t;

  const T c4 = T(0.5)*s*t;
  const T c2s = T(1.5);
  EpsTriplet<T> amp;

  amp += c4*njetan->I4(s, t, T(0.), T(0.), T(0.), T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c2s*njetan->I2(s, T(0.), T(0.));

  const LoopResult<T> res = {phase*amp, conj(phase)*amp};
  return res;
}
template <typename T>
LoopResult<T> Amp2q2g_a<T>::hAL211(const int* p) /* ++-+ */
{
  const std::complex<T> phase = i_*sA(p[0], p[2])*sB(p[1], p[3])/(sA(p[3], p[0])*sA(p[0], p[1]));
  const T rat = T(0.5);
  const EpsTriplet<T> amp(phase*rat, T(0.), T(0.));
  const EpsTriplet<T> camp(conj(phase)*rat, T(0.), T(0.));
  const LoopResult<T> res = {amp, camp};
  return res;
}
template <typename T>
LoopResult<T> Amp2q2g_a<T>::hAL212(const int* p) /* --++ */
{
  const std::complex<T> phase = i_*pow(sA(p[0], p[1]), 3) * sA(p[2], p[1]) / CyclicSpinorsA(p);
  const T s = lS(p[0], p[1]);
  const T t = lS(p[1], p[2]);
//   const T u = -s-t;

  const T c4 = T(0.5)*s*t;
  const T c2t = T(1.5);
  EpsTriplet<T> amp;

  amp += c4*njetan->I4(s, t, T(0.), T(0.), T(0.), T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c2t*njetan->I2(t, T(0.), T(0.));

  const LoopResult<T> res = {phase*amp, conj(phase)*amp};
  return res;
}
template <typename T>
LoopResult<T> Amp2q2g_a<T>::hAL214(const int* p) /* -+++ */
{
  const std::complex<T> phase = i_*sA(p[0], p[2])*sB(p[1], p[3])/(sA(p[1], p[2])*sA(p[2], p[3]));
  const T rat = T(0.5);
  const EpsTriplet<T> amp(phase*rat, T(0.), T(0.));
  const EpsTriplet<T> camp(conj(phase)*rat, T(0.), T(0.));
  const LoopResult<T> res = {amp, camp};
  return res;
}

// "left-moving" fermion loop primitives
template <typename T>
LoopResult<T> Amp2q2g_a<T>::hAf11(const int* p) /* +--- */
{
  const std::complex<T> phase = i_*sB(p[0], p[1])*sA(p[1], p[3])/(sB(p[1], p[2])*sB(p[2], p[3]));
  const T s = lS(p[0], p[1]);
  const T t = lS(p[1], p[2]);
  const T rat = T(1.)/T(3.)*t/s;
  const EpsTriplet<T> amp(phase*rat, T(0.), T(0.));
  const EpsTriplet<T> camp(conj(phase)*rat, T(0.), T(0.));
  const LoopResult<T> res = {amp, camp};
  return res;
}
template <typename T>
LoopResult<T> Amp2q2g_a<T>::hAf12(const int* p) /* -+-- */
{
  const std::complex<T> phase = -i_*sB(p[0], p[1])*sA(p[0], p[2])/(sB(p[0], p[3])*sB(p[2], p[3]));
  const T s = lS(p[0], p[1]);
  const T t = lS(p[1], p[2]);
  const T rat = T(1.)/T(3.)*t/s;
  const EpsTriplet<T> amp(phase*rat, T(0.), T(0.));
  const EpsTriplet<T> camp(conj(phase)*rat, T(0.), T(0.));
  const LoopResult<T> res = {amp, camp};
  return res;
}
template <typename T>
LoopResult<T> Amp2q2g_a<T>::hAf15(const int* /*p*/) /* +-+- */
{
  const EpsTriplet<T> amp;
  const EpsTriplet<T> camp;
  const LoopResult<T> res = {amp, camp};
  return res;
}
template <typename T>
LoopResult<T> Amp2q2g_a<T>::hAf16(const int* /*p*/) /* -++- */
{
  const EpsTriplet<T> amp;
  const EpsTriplet<T> camp;
  const LoopResult<T> res = {amp, camp};
  return res;
}
template <typename T>
LoopResult<T> Amp2q2g_a<T>::hAf19(const int* /*p*/) /* +--+ */
{
  const EpsTriplet<T> amp;
  const EpsTriplet<T> camp;
  const LoopResult<T> res = {amp, camp};
  return res;
}
template <typename T>
LoopResult<T> Amp2q2g_a<T>::hAf110(const int* /*p*/) /* -+-+ */
{
  const EpsTriplet<T> amp;
  const EpsTriplet<T> camp;
  const LoopResult<T> res = {amp, camp};
  return res;
}
template <typename T>
LoopResult<T> Amp2q2g_a<T>::hAf113(const int* p) /* +-++ */
{
  const std::complex<T> phase = -i_*sA(p[0], p[1])*sB(p[0], p[2])/(sA(p[0], p[3])*sA(p[2], p[3]));
  const T s = lS(p[0], p[1]);
  const T t = lS(p[1], p[2]);
  const T rat = T(1.)/T(3.)*t/s;
  const EpsTriplet<T> amp(phase*rat, T(0.), T(0.));
  const EpsTriplet<T> camp(conj(phase)*rat, T(0.), T(0.));
  const LoopResult<T> res = {amp, camp};
  return res;
}
template <typename T>
LoopResult<T> Amp2q2g_a<T>::hAf114(const int* p) /* -+++ */
{
  const std::complex<T> phase = i_*sA(p[0], p[1])*sB(p[1], p[3])/(sA(p[1], p[2])*sA(p[2], p[3]));
  const T s = lS(p[0], p[1]);
  const T t = lS(p[1], p[2]);
  const T rat = T(1.)/T(3.)*t/s;
  const EpsTriplet<T> amp(phase*rat, T(0.), T(0.));
  const EpsTriplet<T> camp(conj(phase)*rat, T(0.), T(0.));
  const LoopResult<T> res = {amp, camp};
  return res;
}

#ifdef USE_SD
  template class Amp2q2g_a<double>;
#endif
#ifdef USE_DD
  template class Amp2q2g_a<dd_real>;
#endif
#ifdef USE_QD
  template class Amp2q2g_a<qd_real>;
#endif
#ifdef USE_VC
  template class Amp2q2g_a<Vc::double_v>;
#endif
