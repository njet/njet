/*
* analytic/4q0g-analytic.h
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef ANALYTIC_4Q0G_ANALYTIC_H
#define ANALYTIC_4Q0G_ANALYTIC_H

#include "../chsums/4q0g.h"

template <typename T>
class Amp4q0g_a : public Amp4q0g<T>
{
    typedef Amp4q0g<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    typedef TreeValue (Amp4q0g_a::*HelAmp)(const int* ord);
    typedef LoopResult<T> (Amp4q0g_a::*HelAmpLoop)(const int* ord);

    Amp4q0g_a(const T scalefactor, const int mFC=1, const NJetAmpTables& tables=BaseClass::amptables());

  protected:
    using BaseClass::mhel;
    using BaseClass::mhelint;

    using BaseClass::njetan;
    using BaseClass::callTree;
    using BaseClass::callLoop;
    using BaseClass::sA;
    using BaseClass::sB;
    using BaseClass::CyclicSpinorsA;
    using BaseClass::CyclicSpinorsB;
    using BaseClass::HelicityOrder;
    using BaseClass::lS;
    using BaseClass::mfv;
    using BaseClass::getFlav;
    using BaseClass::getFperm;

    using BaseClass::Nc;

    T MuR2() { return njetan->getMuR2(); }

    virtual TreeValue A0(int p0, int p1, int p2, int p3);
    virtual LoopResult<T> AL(int p0, int p1, int p2, int p3);
    virtual LoopResult<T> AF(int p0, int p1, int p2, int p3);

    HelAmp hA0[16];
    HelAmpLoop hAL1[16];
    HelAmpLoop hAL2[16];
    HelAmpLoop hAf1[16];

    TreeValue hA05(const int* ord);
    TreeValue hA06(const int* ord);
    TreeValue hA09(const int* ord);
    TreeValue hA010(const int* ord);

    LoopResult<T> hAL15(const int* ord);
    LoopResult<T> hAL16(const int* ord);
    LoopResult<T> hAL19(const int* ord);
    LoopResult<T> hAL110(const int* ord);

    LoopResult<T> hAL23(const int* ord);
    LoopResult<T> hAL25(const int* ord);
    LoopResult<T> hAL210(const int* ord);
    LoopResult<T> hAL212(const int* ord);

    LoopResult<T> hAf15(const int* ord);
    LoopResult<T> hAf16(const int* ord);
    LoopResult<T> hAf19(const int* ord);
    LoopResult<T> hAf110(const int* ord);
};

template <typename T>
class Amp4q0g2_a : public Amp4q0g_a<T>
{
    typedef Amp4q0g_a<T> BaseClass;
  public:

    Amp4q0g2_a(const T scalefactor)
      : BaseClass(scalefactor, 2, amptables())
    { }

  protected:
    using Amp4q0g<T>::fvZero;
    using Amp4q0g<T>::vhel;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp4q0g2Static>();
    }

    void markZeroFv()
    {
      fvZero.reset();
      if (vhel[0] == vhel[1] || vhel[2] == vhel[3]) {
        fvZero[0] = true;
      }
      if (vhel[0] == vhel[3] || vhel[2] == vhel[1]) {
        fvZero[1] = true;
      }
    }
};


#endif /* ANALYTIC_4Q0G_ANALYTIC_H */
