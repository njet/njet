/*
* analytic/4q0g-analytic.cpp
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>
#include "4q0g-analytic.h"

template <typename T>
Amp4q0g_a<T>::Amp4q0g_a(const T scalefactor, const int mFC/*=1*/, const NJetAmpTables& tables/*=amptables()*/)
  : BaseClass(scalefactor, mFC, tables), hA0(), hAL1(), hAL2(), hAf1()
{
  njetan = new NJetAnalytic<T>(scalefactor, BaseClass::legsMOM());
  assert(int(sizeof(hA0)/sizeof(hA0[0])) >= (1 << njetan->legsMOM()));
  assert(int(sizeof(hAL1)/sizeof(hAL1[0])) >= (1 << njetan->legsMOM()));
  assert(int(sizeof(hAL2)/sizeof(hAL2[0])) >= (1 << njetan->legsMOM()));
  assert(int(sizeof(hAf1)/sizeof(hAf1[0])) >= (1 << njetan->legsMOM()));
//   BaseClass::clearNG();

  hA0[5]  = &Amp4q0g_a::hA05;
  hA0[6]  = &Amp4q0g_a::hA06;
  hA0[9]  = &Amp4q0g_a::hA09;
  hA0[10]  = &Amp4q0g_a::hA010;

  hAL1[5]  = &Amp4q0g_a::hAL15;
  hAL1[6]  = &Amp4q0g_a::hAL16;
  hAL1[9]  = &Amp4q0g_a::hAL19;
  hAL1[10]  = &Amp4q0g_a::hAL110;

  hAL2[3]  = &Amp4q0g_a::hAL23;
  hAL2[5]  = &Amp4q0g_a::hAL25;
  hAL2[10]  = &Amp4q0g_a::hAL210;
  hAL2[12]  = &Amp4q0g_a::hAL212;

  hAf1[5]  = &Amp4q0g_a::hAf15;
  hAf1[6]  = &Amp4q0g_a::hAf16;
  hAf1[9]  = &Amp4q0g_a::hAf19;
  hAf1[10]  = &Amp4q0g_a::hAf110;

}

template <typename T>
typename Amp4q0g_a<T>::TreeValue
Amp4q0g_a<T>::A0(int p0, int p1, int p2, int p3)
{
  const int np0 = getFperm(mfv, p0);
  const int np1 = getFperm(mfv, p1);
  const int np2 = getFperm(mfv, p2);
  const int np3 = getFperm(mfv, p3);
  const int ord[] = {np0, np1, np2, np3};
  const int hel = HelicityOrder(mhelint, ord);
  HelAmp const hamp = hA0[hel];
  if (hamp) {
    return i_*callTree(this, hamp, ord);
  } else {
    return TreeValue(0.);
  }
}

// temporary placeholder functions //
template <typename T>
LoopResult<T> Amp4q0g_a<T>::AL(int p0, int p1, int p2, int p3)
{
  const int np0 = getFperm(mfv, p0);
  const int np1 = getFperm(mfv, p1);
  const int np2 = getFperm(mfv, p2);
  const int np3 = getFperm(mfv, p3);
  const int ord[] = {np0, np1, np2, np3};
  const int hel = HelicityOrder(mhelint, ord);
  const int f0 = getFlav(0,p0);
  const int f1 = getFlav(0,p1);
//   const int f2 = getFlav(0,p2);
  const int f3 = getFlav(0,p3);
  if (f0 == -1 && f1 == 1) {
    HelAmpLoop const hamp = hAL1[hel];
    if (hamp) {
      return callLoop(this, hamp, ord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -1 && f3 == 1) {
    HelAmpLoop const hamp = hAL2[hel];
    if (hamp) {
      return callLoop(this, hamp, ord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -2 && f3 == 2) {
    HelAmpLoop const hamp = hAL2[hel];
    if (hamp) {
      return callLoop(this, hamp, ord);
    } else {
      return LoopResult<T>();
    }
  } else {
    return NJetAmp4<T>::AL(p0, p1, p2, p3);
  }
}

template <typename T>
LoopResult<T> Amp4q0g_a<T>::AF(int p0, int p1, int p2, int p3)
{
  const int np0 = getFperm(mfv, p0);
  const int np1 = getFperm(mfv, p1);
  const int np2 = getFperm(mfv, p2);
  const int np3 = getFperm(mfv, p3);
  const int ord[] = {np0, np1, np2, np3};
  const int hel = HelicityOrder(mhelint, ord);
  const int f0 = getFlav(0,p0);
  const int f1 = getFlav(0,p1);
//   const int f2 = getFlav(0,p2);
  const int f3 = getFlav(0,p3);
  if (f0 == -1 && f1 == 1) {
    HelAmpLoop const hamp = hAf1[hel];
    if (hamp) {
      return callLoop(this, hamp, ord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -1 && f3 == 1) {
    return LoopResult<T>();
  } else {
    return NJetAmp4<T>::AF(p0, p1, p2, p3);
  }
}

template <typename T>
typename Amp4q0g_a<T>::TreeValue
Amp4q0g_a<T>::hA05(const int* p) /* +-+- */
{
  return -sA(p[1], p[3])*sB(p[0], p[2])/lS(p[0], p[1]);
}

template <typename T>
typename Amp4q0g_a<T>::TreeValue
Amp4q0g_a<T>::hA06(const int* p) /* -++- */
{
  return sA(p[0], p[3])*sB(p[1], p[2])/lS(p[0], p[1]);
}

template <typename T>
typename Amp4q0g_a<T>::TreeValue
Amp4q0g_a<T>::hA09(const int* p) /* +--+ */
{
  return sB(p[0], p[3])*sA(p[1], p[2])/lS(p[0], p[1]);
}

template <typename T>
typename Amp4q0g_a<T>::TreeValue
Amp4q0g_a<T>::hA010(const int* p) /* -+-+ */
{
  return -sB(p[1], p[3])*sA(p[0], p[2])/lS(p[0], p[1]);
}

// "left-moving" primitives = 2 gluon propagators
template <typename T>
LoopResult<T> Amp4q0g_a<T>::hAL15(const int* p) /* +-+- */
{
  const std::complex<T> phase = i_*hA05(p);
  const T s = lS(p[0], p[1]);
  const T t = lS(p[1], p[2]);
  const T u = -s-t;
  const T st = s*t;
  const T tu = t*u;
  const T s2 = s*s;
//   const T t2 = t*t;
  const T u2 = u*u;

  const T c4 = -T(0.5)*st*(s2-T(2.)*tu)/u2;
  const T c3s = s*(s2-T(2.)*tu)/u2;
  const T c3t = -st*(s+T(2.)*t)/u2;
  const T c2s = T(2.)/T(3.)-s/u;
  const T c2t = s/u;
  const T rat = T(7.)/T(9.);
  EpsTriplet<T> amp;

  amp += c4*njetan->I4(s, t, T(0.), T(0.), T(0.), T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c3s*njetan->I3(s, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c3t*njetan->I3(t, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c2s*njetan->I2(s, T(0.), T(0.));
  amp += c2t*njetan->I2(t, T(0.), T(0.));
  amp += rat;

  const LoopResult<T> res = {phase*amp, conj(phase)*amp};
  return res;
}

template <typename T>
LoopResult<T> Amp4q0g_a<T>::hAL16(const int* p) /* -++- */
{
  const std::complex<T> phase = i_*hA06(p);
  const T s = lS(p[0], p[1]);
  const T t = lS(p[1], p[2]);
//   const T u = -s-t;
  const T st = s*t;

  const T c4 = -st;
  const T c3s = T(2.)*s;
  const T c2s = T(2.)/T(3.);
  const T rat = T(7.)/T(9.);
  EpsTriplet<T> amp;

  amp += c4*njetan->I4(s, t, T(0.), T(0.), T(0.), T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c3s*njetan->I3(s, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c2s*njetan->I2(s, T(0.), T(0.));
  amp += rat;

  const LoopResult<T> res = {phase*amp, conj(phase)*amp};
  return res;
}

template <typename T>
LoopResult<T> Amp4q0g_a<T>::hAL19(const int* p) /* +--+ */
{
  const std::complex<T> phase = i_*hA09(p);
  const T s = lS(p[0], p[1]);
  const T t = lS(p[1], p[2]);
//   const T u = -s-t;
  const T st = s*t;

  const T c4 = -st;
  const T c3s = T(2.)*s;
  const T c2s = T(2.)/T(3.);
  const T rat = T(7.)/T(9.);
  EpsTriplet<T> amp;

  amp += c4*njetan->I4(s, t, T(0.), T(0.), T(0.), T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c3s*njetan->I3(s, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c2s*njetan->I2(s, T(0.), T(0.));
  amp += rat;

  const LoopResult<T> res = {phase*amp, conj(phase)*amp};
  return res;
}

template <typename T>
LoopResult<T> Amp4q0g_a<T>::hAL110(const int* p) /* -+-+ */
{
  const std::complex<T> phase = i_*hA010(p);
  const T s = lS(p[0], p[1]);
  const T t = lS(p[1], p[2]);
  const T u = -s-t;
  const T st = s*t;
  const T tu = t*u;
  const T s2 = s*s;
//   const T t2 = t*t;
  const T u2 = u*u;

  const T c4 = -T(0.5)*st*(s2-T(2.)*tu)/u2;
  const T c3s = s*(s2-T(2.)*tu)/u2;
  const T c3t = -st*(s+T(2.)*t)/u2;
  const T c2s = T(2.)/T(3.)-s/u;
  const T c2t = s/u;
  const T rat = T(7.)/T(9.);
  EpsTriplet<T> amp;

  amp += c4*njetan->I4(s, t, T(0.), T(0.), T(0.), T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c3s*njetan->I3(s, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c3t*njetan->I3(t, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c2s*njetan->I2(s, T(0.), T(0.));
  amp += c2t*njetan->I2(t, T(0.), T(0.));
  amp += rat;

  const LoopResult<T> res = {phase*amp, conj(phase)*amp};
  return res;
}

// "right-moving" primitives = 1 gluon propagator
template <typename T>
LoopResult<T> Amp4q0g_a<T>::hAL23(const int* p) /* ++-- */
{
  const std::complex<T> phase = i_*sB(p[0],p[1])*sA(p[2],p[3])/lS(p[1],p[2]);
  const T t = lS(p[1], p[2]);

  const T c3t = t;
  const T c2t = T(3.)/T(2.);
  const T rat = T(0.5);
  EpsTriplet<T> amp;

  amp += c3t*njetan->I3(t, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c2t*njetan->I2(t, T(0.), T(0.));
  amp += rat;

  const LoopResult<T> res = {phase*amp, conj(phase)*amp};
  return res;
}
template <typename T>
LoopResult<T> Amp4q0g_a<T>::hAL25(const int* p) /* +-+- */
{
  const std::complex<T> phase = -i_*sA(p[1],p[3])*sB(p[0],p[2])/lS(p[1],p[2]);
  const T t = lS(p[1], p[2]);

  const T c3t = t;
  const T c2t = T(3.)/T(2.);
  const T rat = T(0.5);
  EpsTriplet<T> amp;

  amp += c3t*njetan->I3(t, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c2t*njetan->I2(t, T(0.), T(0.));
  amp += rat;

  const LoopResult<T> res = {phase*amp, conj(phase)*amp};
  return res;
}
template <typename T>
LoopResult<T> Amp4q0g_a<T>::hAL210(const int* p) /* -+-+ */
{
  const std::complex<T> phase = -i_*sB(p[1],p[3])*sA(p[0],p[2])/lS(p[1],p[2]);
  const T t = lS(p[1], p[2]);

  const T c3t = t;
  const T c2t = T(3.)/T(2.);
  const T rat = T(0.5);
  EpsTriplet<T> amp;

  amp += c3t*njetan->I3(t, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c2t*njetan->I2(t, T(0.), T(0.));
  amp += rat;

  const LoopResult<T> res = {phase*amp, conj(phase)*amp};
  return res;
}
template <typename T>
LoopResult<T> Amp4q0g_a<T>::hAL212(const int* p) /* --++ */
{
  const std::complex<T> phase = i_*sA(p[0],p[1])*sB(p[2],p[3])/lS(p[1],p[2]);
  const T t = lS(p[1], p[2]);

  const T c3t = t;
  const T c2t = T(3.)/T(2.);
  const T rat = T(0.5);
  EpsTriplet<T> amp;

  amp += c3t*njetan->I3(t, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c2t*njetan->I2(t, T(0.), T(0.));
  amp += rat;

  const LoopResult<T> res = {phase*amp, conj(phase)*amp};
  return res;
}

// "left-moving" fermion loop primitives
template <typename T>
LoopResult<T> Amp4q0g_a<T>::hAf15(const int* p) /* +-+- */
{
  const std::complex<T> phase = i_*hA05(p);
  const T s = lS(p[0], p[1]);

  const T c2s = T(2.)/T(3.);
  const T rat = -T(2.)/T(9.);
  EpsTriplet<T> amp;

  amp += c2s*njetan->I2(s, T(0.), T(0.));
  amp += rat;

  const LoopResult<T> res = {phase*amp, conj(phase)*amp};
  return res;
}
template <typename T>
LoopResult<T> Amp4q0g_a<T>::hAf16(const int* p) /* -++- */
{
  const std::complex<T> phase = i_*hA06(p);
  const T s = lS(p[0], p[1]);

  const T c2s = T(2.)/T(3.);
  const T rat = -T(2.)/T(9.);
  EpsTriplet<T> amp;

  amp += c2s*njetan->I2(s, T(0.), T(0.));
  amp += rat;

  const LoopResult<T> res = {phase*amp, conj(phase)*amp};
  return res;
}
template <typename T>
LoopResult<T> Amp4q0g_a<T>::hAf19(const int* p) /* +--+ */
{
  const std::complex<T> phase = i_*hA09(p);
  const T s = lS(p[0], p[1]);

  const T c2s = T(2.)/T(3.);
  const T rat = -T(2.)/T(9.);
  EpsTriplet<T> amp;

  amp += c2s*njetan->I2(s, T(0.), T(0.));
  amp += rat;

  const LoopResult<T> res = {phase*amp, conj(phase)*amp};
  return res;
}
template <typename T>
LoopResult<T> Amp4q0g_a<T>::hAf110(const int* p) /* -+-+ */
{
  const std::complex<T> phase = i_*hA010(p);
  const T s = lS(p[0], p[1]);

  const T c2s = T(2.)/T(3.);
  const T rat = -T(2.)/T(9.);
  EpsTriplet<T> amp;

  amp += c2s*njetan->I2(s, T(0.), T(0.));
  amp += rat;

  const LoopResult<T> res = {phase*amp, conj(phase)*amp};
  return res;
}

#ifdef USE_SD
  template class Amp4q0g_a<double>;
#endif
#ifdef USE_DD
  template class Amp4q0g_a<dd_real>;
#endif
#ifdef USE_QD
  template class Amp4q0g_a<qd_real>;
#endif
#ifdef USE_VC
  template class Amp4q0g_a<Vc::double_v>;
#endif
