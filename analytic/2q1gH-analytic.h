/*
* analytic/2q1gH-analytic.h
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef ANALYTIC_2Q1GH_ANALYTIC_H
#define ANALYTIC_2Q1GH_ANALYTIC_H

#include "../chsums/2q1gH.h"

template <typename T>
class Amp2q1gH_a : public Amp2q1gH<T>
{
    typedef Amp2q1gH<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    typedef TreeValue (Amp2q1gH_a::*HelAmp)(const int* ord);

    Amp2q1gH_a(const T scalefactor);

  protected:
    using BaseClass::mhel;
    using BaseClass::mhelint;

    using BaseClass::njetan;
    using BaseClass::callTree;
    using BaseClass::callLoop;
    using BaseClass::sA;
    using BaseClass::sB;
    using BaseClass::CyclicSpinorsA;
    using BaseClass::CyclicSpinorsB;
    using BaseClass::HelicityOrder;

    virtual TreeValue A0(int p0, int p1, int p2);

    HelAmp hA0[16];

    TreeValue hA09(const int* ord);
    TreeValue hA010(const int* ord);
    TreeValue hA013(const int* ord);
    TreeValue hA014(const int* ord);
};

#endif /* ANALYTIC_2Q1GH_ANALYTIC_H */
