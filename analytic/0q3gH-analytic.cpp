/*
* analytic/0q3gH-analytic.cpp
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>
#include "0q3gH-analytic.h"

template <typename T>
Amp0q3gH_a<T>::Amp0q3gH_a(const T scalefactor)
  : BaseClass(scalefactor), hA0()
{
  njetan = new NJetAnalytic<T>(scalefactor, BaseClass::legsMOM(), -1);
//   BaseClass::clearNG();

  hA0[0] = &Amp0q3gH_a::hA00;
  hA0[1] = &Amp0q3gH_a::hA01;
  hA0[2] = &Amp0q3gH_a::hA02;
  hA0[3] = &Amp0q3gH_a::hA03;
  hA0[4] = &Amp0q3gH_a::hA04;
  hA0[5] = &Amp0q3gH_a::hA05;
  hA0[6] = &Amp0q3gH_a::hA06;
  hA0[7] = &Amp0q3gH_a::hA07;
}

template <typename T>
typename Amp0q3gH_a<T>::TreeValue
Amp0q3gH_a<T>::A0(int p0, int p1, int p2)
{
  const int ord[] = {p0, p1, p2, BaseClass::NN};
  const int hel = HelicityOrder(8^mhelint, ord);
  HelAmp const hamp = hA0[hel];
  if (hamp) {
    return 0.5*i_*callTree(this, hamp, ord);
  } else {
    return TreeValue(0.);
  }
}

template <typename T>
typename Amp0q3gH_a<T>::TreeValue
Amp0q3gH_a<T>::hA00(const int* p)
{
  return pow(lS(3, 3), 2)/(sB(p[0], p[1])*sB(p[1], p[2])*sB(p[2], p[0]));
}
template <typename T>
typename Amp0q3gH_a<T>::TreeValue
Amp0q3gH_a<T>::hA01(const int* p)
{
  const int np[] = {p[1], p[2], p[0]};
  return hA04(np);
}
template <typename T>
typename Amp0q3gH_a<T>::TreeValue
Amp0q3gH_a<T>::hA02(const int* p)
{
  const int np[] = {p[2], p[0], p[1]};
  return hA04(np);
}
template <typename T>
typename Amp0q3gH_a<T>::TreeValue
Amp0q3gH_a<T>::hA03(const int* p)
{
  return pow(sB(p[0], p[1]), 4)/(sB(p[0], p[1])*sB(p[1], p[2])*sB(p[2], p[0]));
}
template <typename T>
typename Amp0q3gH_a<T>::TreeValue
Amp0q3gH_a<T>::hA04(const int* p)
{
  return -pow(sA(p[0], p[1]), 4)/(sA(p[0], p[1])*sA(p[1], p[2])*sA(p[2], p[0]));
}
template <typename T>
typename Amp0q3gH_a<T>::TreeValue
Amp0q3gH_a<T>::hA05(const int* p)
{
  const int np[] = {p[2], p[0], p[1]};
  return hA03(np);
}
template <typename T>
typename Amp0q3gH_a<T>::TreeValue
Amp0q3gH_a<T>::hA06(const int* p)
{
  const int np[] = {p[1], p[2], p[0]};
  return hA03(np);
}
template <typename T>
typename Amp0q3gH_a<T>::TreeValue
Amp0q3gH_a<T>::hA07(const int* p)
{
  return -pow(lS(3, 3), 2)/(sA(p[0], p[1])*sA(p[1], p[2])*sA(p[2], p[0]));
}

#ifdef USE_SD
  template class Amp0q3gH_a<double>;
#endif
#ifdef USE_DD
  template class Amp0q3gH_a<dd_real>;
#endif
#ifdef USE_QD
  template class Amp0q3gH_a<qd_real>;
#endif
#ifdef USE_VC
  template class Amp0q3gH_a<Vc::double_v>;
#endif
