/*
* analytic/2q1gH-analytic.cpp
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>
#include "2q1gH-analytic.h"

template <typename T>
Amp2q1gH_a<T>::Amp2q1gH_a(const T scalefactor)
  : BaseClass(scalefactor), hA0()
{
  njetan = new NJetAnalytic<T>(scalefactor, BaseClass::legsMOM(), -1);
  assert(int(sizeof(hA0)/sizeof(hA0[0])) >= (1 << njetan->legsMOM()));
//   BaseClass::clearNG();

  hA0[9]  = &Amp2q1gH_a::hA09;
  hA0[10]  = &Amp2q1gH_a::hA010;
  hA0[13]  = &Amp2q1gH_a::hA013;
  hA0[14]  = &Amp2q1gH_a::hA014;
}

template <typename T>
typename Amp2q1gH_a<T>::TreeValue
Amp2q1gH_a<T>::A0(int p0, int p1, int p2)
{
  const int ord[] = {p0, p1, p2, BaseClass::NN};
  const int hel = HelicityOrder(mhelint, ord);
  HelAmp const hamp = hA0[hel];
  if (hamp) {
    return 0.5*i_*callTree(this, hamp, ord);
  } else {
    return TreeValue(0.);
  }
}

template <typename T>
typename Amp2q1gH_a<T>::TreeValue
Amp2q1gH_a<T>::hA09(const int* p) /* +--+ */
{
  return pow(sA(p[1], p[2]), 2) / sA(p[0], p[1]);
}

template <typename T>
typename Amp2q1gH_a<T>::TreeValue
Amp2q1gH_a<T>::hA010(const int* p) /* -+-+ */
{
  return -pow(sA(p[0], p[2]), 2)/sA(p[0], p[1]);
}

template <typename T>
typename Amp2q1gH_a<T>::TreeValue
Amp2q1gH_a<T>::hA013(const int* p) /* +-++ */
{
  return pow(sB(p[0], p[2]), 2)/sB(p[0], p[1]);
}

template <typename T>
typename Amp2q1gH_a<T>::TreeValue
Amp2q1gH_a<T>::hA014(const int* p) /* -+++ */
{
  return -pow(sB(p[1], p[2]), 2) / sB(p[0], p[1]);
}

#ifdef USE_SD
  template class Amp2q1gH_a<double>;
#endif
#ifdef USE_DD
  template class Amp2q1gH_a<dd_real>;
#endif
#ifdef USE_QD
  template class Amp2q1gH_a<qd_real>;
#endif
#ifdef USE_VC
  template class Amp2q1gH_a<Vc::double_v>;
#endif
