/*
* analytic/0q6g-analytic.cpp
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>
#include "0q6g-analytic.h"

template <typename T>
Amp0q6g_a<T>::Amp0q6g_a(const T scalefactor)
  : BaseClass(scalefactor), hA0()
{
  njetan = new NJetAnalytic<T>(scalefactor, BaseClass::legsMOM());
  assert(int(sizeof(hA0)/sizeof(hA0[0])) >= (1 << njetan->legsMOM()));
//   BaseClass::clearNG();

  hA0[3] = &Amp0q6g_a::hA03;
  hA0[5] = &Amp0q6g_a::hA05;
  hA0[6] = &Amp0q6g_a::hA06;
  hA0[7] = &Amp0q6g_a::hA07;
  hA0[9] = &Amp0q6g_a::hA09;
  hA0[10] = &Amp0q6g_a::hA010;
  hA0[11] = &Amp0q6g_a::hA011;
  hA0[12] = &Amp0q6g_a::hA012;
  hA0[13] = &Amp0q6g_a::hA013;
  hA0[14] = &Amp0q6g_a::hA014;
  hA0[15] = &Amp0q6g_a::hA015;
  hA0[17] = &Amp0q6g_a::hA017;
  hA0[18] = &Amp0q6g_a::hA018;
  hA0[19] = &Amp0q6g_a::hA019;
  hA0[20] = &Amp0q6g_a::hA020;
  hA0[21] = &Amp0q6g_a::hA021;
  hA0[22] = &Amp0q6g_a::hA022;
  hA0[23] = &Amp0q6g_a::hA023;
  hA0[24] = &Amp0q6g_a::hA024;
  hA0[25] = &Amp0q6g_a::hA025;
  hA0[26] = &Amp0q6g_a::hA026;
  hA0[27] = &Amp0q6g_a::hA027;
  hA0[28] = &Amp0q6g_a::hA028;
  hA0[29] = &Amp0q6g_a::hA029;
  hA0[30] = &Amp0q6g_a::hA030;
  hA0[33] = &Amp0q6g_a::hA033;
  hA0[34] = &Amp0q6g_a::hA034;
  hA0[35] = &Amp0q6g_a::hA035;
  hA0[36] = &Amp0q6g_a::hA036;
  hA0[37] = &Amp0q6g_a::hA037;
  hA0[38] = &Amp0q6g_a::hA038;
  hA0[39] = &Amp0q6g_a::hA039;
  hA0[40] = &Amp0q6g_a::hA040;
  hA0[41] = &Amp0q6g_a::hA041;
  hA0[42] = &Amp0q6g_a::hA042;
  hA0[43] = &Amp0q6g_a::hA043;
  hA0[44] = &Amp0q6g_a::hA044;
  hA0[45] = &Amp0q6g_a::hA045;
  hA0[46] = &Amp0q6g_a::hA046;
  hA0[48] = &Amp0q6g_a::hA048;
  hA0[49] = &Amp0q6g_a::hA049;
  hA0[50] = &Amp0q6g_a::hA050;
  hA0[51] = &Amp0q6g_a::hA051;
  hA0[52] = &Amp0q6g_a::hA052;
  hA0[53] = &Amp0q6g_a::hA053;
  hA0[54] = &Amp0q6g_a::hA054;
  hA0[56] = &Amp0q6g_a::hA056;
  hA0[57] = &Amp0q6g_a::hA057;
  hA0[58] = &Amp0q6g_a::hA058;
  hA0[60] = &Amp0q6g_a::hA060;
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::A0(int p0, int p1, int p2, int p3, int p4, int p5)
{
  const int ord[] = {p0, p1, p2, p3, p4, p5};
  const int hel = HelicityOrder(mhelint, ord);
  HelAmp const hamp = hA0[hel];
  if (hamp) {
    return i_*callTree(this, hamp, ord);
  } else {
    return TreeValue(0.);
  }
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA03(const int* p) /* 11-1-1-1-1 */
{
  return pow(sB(p[0], p[1]), 4) / CyclicSpinorsB(p);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA05(const int* p) /* 1-11-1-1-1 */
{
  return pow(sB(p[0], p[2]), 4) / CyclicSpinorsB(p);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA06(const int* p) /* -111-1-1-1 */
{
  return pow(sB(p[1], p[2]), 4) / CyclicSpinorsB(p);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA07(const int* p) /* 111-1-1-1 */
{
  const int nord[] = {p[3],p[4],p[5],p[0],p[1],p[2]};
  return hA056(nord);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA09(const int* p) /* 1-1-11-1-1 */
{
  return pow(sB(p[0], p[3]), 4) / CyclicSpinorsB(p);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA010(const int* p) /* -11-11-1-1 */
{
  return pow(sB(p[1], p[3]), 4) / CyclicSpinorsB(p);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA011(const int* p) /* 11-11-1-1 */
{
  const int nord[] = {p[4],p[5],p[0],p[1],p[2],p[3]};
  return hA044(nord);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA012(const int* p) /* -1-111-1-1 */
{
  return pow(sB(p[2], p[3]), 4) / CyclicSpinorsB(p);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA013(const int* p) /* 1-111-1-1 */
{
  const int nord[] = {p[4],p[5],p[0],p[1],p[2],p[3]};
  return hA052(nord);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA014(const int* p) /* -1111-1-1 */
{
  const int nord[] = {p[4],p[5],p[0],p[1],p[2],p[3]};
  return hA056(nord);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA015(const int* p) /* 1111-1-1 */
{
  return pow(sA(p[4], p[5]), 4) / CyclicSpinorsA(p);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA017(const int* p) /* 1-1-1-11-1 */
{
  return pow(sB(p[0], p[4]), 4) / CyclicSpinorsB(p);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA018(const int* p) /* -11-1-11-1 */
{
  return pow(sB(p[1], p[4]), 4) / CyclicSpinorsB(p);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA019(const int* p) /* 11-1-11-1 */
{
  const int nord[] = {p[2],p[3],p[4],p[5],p[0],p[1]};
  return hA052(nord);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA020(const int* p) /* -1-11-11-1 */
{
  return pow(sB(p[2], p[4]), 4) / CyclicSpinorsB(p);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA021(const int* p) /* 1-11-11-1 */
{
  const int nord[] = {p[1],p[2],p[3],p[4],p[5],p[0]};
  return hA042(nord);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA022(const int* p) /* -111-11-1 */
{
  const int nord[] = {p[5],p[0],p[1],p[2],p[3],p[4]};
  return hA044(nord);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA023(const int* p) /* 111-11-1 */
{
  return pow(sA(p[3], p[5]), 4) / CyclicSpinorsA(p);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA024(const int* p) /* -1-1-111-1 */
{
  return pow(sB(p[3], p[4]), 4) / CyclicSpinorsB(p);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA025(const int* p) /* 1-1-111-1 */
{
  const int nord[] = {p[1],p[2],p[3],p[4],p[5],p[0]};
  return hA044(nord);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA026(const int* p) /* -11-111-1 */
{
  const int nord[] = {p[5],p[0],p[1],p[2],p[3],p[4]};
  return hA052(nord);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA027(const int* p) /* 11-111-1 */
{
  return pow(sA(p[2], p[5]), 4) / CyclicSpinorsA(p);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA028(const int* p) /* -1-1111-1 */
{
  const int nord[] = {p[5],p[0],p[1],p[2],p[3],p[4]};
  return hA056(nord);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA029(const int* p) /* 1-1111-1 */
{
  return pow(sA(p[1], p[5]), 4) / CyclicSpinorsA(p);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA030(const int* p) /* -11111-1 */
{
  return pow(sA(p[0], p[5]), 4) / CyclicSpinorsA(p);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA033(const int* p) /* 1-1-1-1-11 */
{
  return pow(sB(p[0], p[5]), 4) / CyclicSpinorsB(p);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA034(const int* p) /* -11-1-1-11 */
{
  return pow(sB(p[1], p[5]), 4) / CyclicSpinorsB(p);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA035(const int* p) /* 11-1-1-11 */
{
  const int nord[] = {p[2],p[3],p[4],p[5],p[0],p[1]};
  return hA056(nord);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA036(const int* p) /* -1-11-1-11 */
{
  return pow(sB(p[2], p[5]), 4) / CyclicSpinorsB(p);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA037(const int* p) /* 1-11-1-11 */
{
  const int nord[] = {p[3],p[4],p[5],p[0],p[1],p[2]};
  return hA044(nord);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA038(const int* p) /* -111-1-11 */
{
  const int nord[] = {p[3],p[4],p[5],p[0],p[1],p[2]};
  return hA052(nord);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA039(const int* p) /* 111-1-11 */
{
  return pow(sA(p[3], p[4]), 4) / CyclicSpinorsA(p);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA040(const int* p) /* -1-1-11-11 */
{
  return pow(sB(p[3], p[5]), 4) / CyclicSpinorsB(p);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA041(const int* p) /* 1-1-11-11 */
{
  const int nord[] = {p[1],p[2],p[3],p[4],p[5],p[0]};
  return hA052(nord);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA042(const int* p) /* -11-11-11 */
{
  XX[1] = T(1.)/(sA(p[0],p[1])*sB(p[1],p[3]) + sA(p[0],p[2])*sB(p[2],p[3]));
  XX[2] = T(1.)/(sA(p[2],p[4])*sB(p[1],p[2]) + sA(p[3],p[4])*sB(p[1],p[3]));
  XX[3] = T(1.)/(sA(p[0],p[5])*sA(p[4],p[5])*sB(p[1],p[2])*sB(p[2],p[3])*lS(p[1],p[2],p[3]));
  XX[4] = sA(p[0],p[4]);
  XX[5] = sB(p[1],p[3]);
  XX[6] = T(1.)/(sA(p[0],p[4])*sB(p[3],p[4]) + sA(p[0],p[5])*sB(p[3],p[5]));
  XX[7] = T(1.)/(sA(p[2],p[3])*sB(p[3],p[5]) + sA(p[2],p[4])*sB(p[4],p[5]));
  XX[8] = T(1.)/(sA(p[0],p[1])*sA(p[1],p[2])*sB(p[3],p[4])*sB(p[4],p[5])*lS(p[3],p[4],p[5]));
  XX[9] = sA(p[0],p[2]);
  XX[10] = sB(p[3],p[5]);
  XX[11] = T(1.)/(sA(p[2],p[3])*sA(p[3],p[4])*sB(p[0],p[1])*sB(p[0],p[5])*lS(p[2],p[3],p[4]));
  XX[12] = sA(p[2],p[4]);
  XX[13] = sB(p[1],p[5]);
  XX[14] = XX[12]*XX[13];
  XX[14] =  - XX[7]*XX[11]*pow(XX[14],4);
  XX[15] = XX[4]*XX[5];
  XX[15] =  - XX[1]*XX[3]*pow(XX[15],4);
  XX[14] = XX[14] + XX[15];
  XX[14] = XX[2]*XX[14];
  XX[15] = XX[10]*XX[9];
  XX[15] =  - XX[7]*XX[8]*XX[6]*pow(XX[15],4);
  return XX[14] + XX[15];
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA043(const int* p) /* 11-11-11 */
{
  return pow(sA(p[2], p[4]), 4) / CyclicSpinorsA(p);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA044(const int* p) /* -1-111-11 */
{
  XX[1] = T(1.)/(sA(p[0],p[1])*sB(p[0],p[4]) + sA(p[1],p[5])*sB(p[4],p[5]));
  XX[2] = T(1.)/(sA(p[3],p[4])*sB(p[0],p[4]) + sA(p[3],p[5])*sB(p[0],p[5]));
  XX[3] = T(1.)/(sA(p[1],p[2])*sA(p[2],p[3])*sB(p[0],p[5])*sB(p[4],p[5])*lS(p[0],p[4],p[5]));
  XX[4] = pow(sA(p[0],p[1])*sB(p[0],p[5]) - sA(p[1],p[4])*sB(p[4],p[5]),4);
  XX[5] = T(1.)/(sA(p[0],p[5])*sB(p[0],p[2]) + sA(p[1],p[5])*sB(p[1],p[2]));
  XX[6] = T(1.)/(sA(p[1],p[3])*sB(p[0],p[1]) + sA(p[2],p[3])*sB(p[0],p[2]));
  XX[7] = T(1.)/(sA(p[3],p[4])*sA(p[4],p[5])*sB(p[0],p[1])*sB(p[1],p[2])*lS(p[0],p[1],p[2]));
  XX[8] = pow(sA(p[0],p[4])*sB(p[0],p[2]) + sA(p[1],p[4])*sB(p[1],p[2]),4);
  XX[9] = T(1.)/(sA(p[0],p[5])*sB(p[3],p[4])*lS(p[0],p[1],p[5]));
  XX[10] = sA(p[0],p[1]);
  XX[11] = sB(p[2],p[3]);
  XX[12] = XX[11]*XX[10];
  XX[12] =  - XX[5]*XX[9]*pow(XX[12],3);
  XX[13] =  - XX[3]*XX[4]*XX[2];
  XX[12] = XX[12] + XX[13];
  XX[12] = XX[1]*XX[12];
  XX[13] =  - XX[6]*XX[5]*XX[7]*XX[8];
  return XX[12] + XX[13];
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA045(const int* p) /* 1-111-11 */
{
  return pow(sA(p[1], p[4]), 4) / CyclicSpinorsA(p);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA046(const int* p) /* -1111-11 */
{
  return pow(sA(p[0], p[4]), 4) / CyclicSpinorsA(p);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA048(const int* p) /* -1-1-1-111 */
{
  return pow(sB(p[4], p[5]), 4) / CyclicSpinorsB(p);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA049(const int* p) /* 1-1-1-111 */
{
  const int nord[] = {p[1],p[2],p[3],p[4],p[5],p[0]};
  return hA056(nord);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA050(const int* p) /* -11-1-111 */
{
  const int nord[] = {p[2],p[3],p[4],p[5],p[0],p[1]};
  return hA044(nord);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA051(const int* p) /* 11-1-111 */
{
  return pow(sA(p[2], p[3]), 4) / CyclicSpinorsA(p);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA052(const int* p) /* -1-11-111 */
{
  XX[1] = T(1.)/(sA(p[0],p[1])*sB(p[0],p[4]) + sA(p[1],p[5])*sB(p[4],p[5]));
  XX[2] = T(1.)/(sA(p[0],p[5])*sB(p[0],p[2]) + sA(p[1],p[5])*sB(p[1],p[2]));
  XX[3] = T(1.)/(sA(p[0],p[5])*sB(p[2],p[3])*sB(p[3],p[4])*lS(p[0],p[1],p[5]));
  XX[4] = sA(p[0],p[1]);
  XX[5] = sB(p[2],p[4]);
  XX[6] = T(1.)/(sA(p[3],p[4])*sB(p[0],p[4]) + sA(p[3],p[5])*sB(p[0],p[5]));
  XX[7] = T(1.)/(sA(p[1],p[2])*sA(p[2],p[3])*sB(p[0],p[5])*lS(p[0],p[4],p[5]));
  XX[8] = sA(p[1],p[3]);
  XX[9] = sB(p[4],p[5]);
  XX[10] = T(1.)/(sA(p[1],p[3])*sB(p[0],p[1]) + sA(p[2],p[3])*sB(p[0],p[2]));
  XX[11] = T(1.)/(sA(p[3],p[4])*sA(p[4],p[5])*sB(p[0],p[1])*sB(p[1],p[2])*lS(p[0],p[1],p[2]));
  XX[12] = pow(sA(p[0],p[3])*sB(p[0],p[2]) + sA(p[1],p[3])*sB(p[1],p[2]),4);
  XX[13] =  - XX[6]*XX[7]*pow(XX[9],3)*pow(XX[8],4);
  XX[14] =  - XX[2]*XX[3]*pow(XX[5],4)*pow(XX[4],3);
  XX[13] = XX[13] + XX[14];
  XX[13] = XX[1]*XX[13];
  XX[14] =  - XX[2]*XX[10]*XX[11]*XX[12];
  return XX[13] + XX[14];
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA053(const int* p) /* 1-11-111 */
{
  return pow(sA(p[1], p[3]), 4) / CyclicSpinorsA(p);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA054(const int* p) /* -111-111 */
{
  return pow(sA(p[0], p[3]), 4) / CyclicSpinorsA(p);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA056(const int* p) /* -1-1-1111 */
{
  XX[1] = T(1.)/(sA(p[0],p[5])*sB(p[2],p[3])*lS(p[0],p[1],p[5]));
  XX[2] = T(1.)/(sA(p[0],p[1])*sB(p[0],p[4]) + sA(p[1],p[5])*sB(p[4],p[5]));
  XX[3] = T(1.)/(sA(p[0],p[5])*sB(p[0],p[2]) + sA(p[1],p[5])*sB(p[1],p[2]));
  XX[4] = sA(p[0],p[1]);
  XX[5] = sB(p[3],p[4]);
  XX[6] = T(1.)/(sA(p[2],p[3])*sB(p[0],p[5])*lS(p[0],p[4],p[5]));
  XX[7] = T(1.)/(sA(p[3],p[4])*sB(p[0],p[4]) + sA(p[3],p[5])*sB(p[0],p[5]));
  XX[8] = sA(p[1],p[2]);
  XX[9] = sB(p[4],p[5]);
  XX[10] = T(1.)/(sA(p[3],p[4])*sA(p[4],p[5])*sB(p[0],p[1])*sB(p[1],p[2]));
  XX[11] = T(1.)/(sA(p[1],p[3])*sB(p[0],p[1]) + sA(p[2],p[3])*sB(p[0],p[2]));
  XX[12] = lS(p[0],p[1],p[2]);
  XX[13] = XX[8]*XX[9];
  XX[13] =  - XX[6]*XX[7]*pow(XX[13],3);
  XX[14] = XX[5]*XX[4];
  XX[14] =  - XX[3]*XX[1]*pow(XX[14],3);
  XX[13] = XX[13] + XX[14];
  XX[13] = XX[2]*XX[13];
  XX[14] =  - XX[3]*XX[10]*XX[11]*pow(XX[12],3);
  return XX[13] + XX[14];
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA057(const int* p) /* 1-1-1111 */
{
  return pow(sA(p[1], p[2]), 4) / CyclicSpinorsA(p);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA058(const int* p) /* -11-1111 */
{
  return pow(sA(p[0], p[2]), 4) / CyclicSpinorsA(p);
}

template <typename T>
typename Amp0q6g_a<T>::TreeValue
Amp0q6g_a<T>::hA060(const int* p) /* -1-11111 */
{
  return pow(sA(p[0], p[1]), 4) / CyclicSpinorsA(p);
}

#ifdef USE_SD
  template class Amp0q6g_a<double>;
#endif
#ifdef USE_DD
  template class Amp0q6g_a<dd_real>;
#endif
#ifdef USE_QD
  template class Amp0q6g_a<qd_real>;
#endif
#ifdef USE_VC
  template class Amp0q6g_a<Vc::double_v>;
#endif
