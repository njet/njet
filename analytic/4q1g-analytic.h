/*
* analytic/4q1g-analytic.h
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef ANALYTIC_4Q1G_ANALYTIC_H
#define ANALYTIC_4Q1G_ANALYTIC_H

#include "../chsums/4q1g.h"

template <typename T>
class Amp4q1g_a : public Amp4q1g<T>
{
    typedef Amp4q1g<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    typedef TreeValue (Amp4q1g_a::*HelAmp)(const int* ord);
    typedef LoopResult<T> (Amp4q1g_a::*HelAmpLoop)(const int* ord);

    Amp4q1g_a(const T scalefactor, const int mFC=1,
              const NJetAmpTables& tables=BaseClass::amptables());

  protected:
    using BaseClass::mhel;
    using BaseClass::mhelint;
    using BaseClass::mfv;

    using BaseClass::njetan;
    using BaseClass::getFlav;
    using BaseClass::getFperm;
    using BaseClass::callTree;
    using BaseClass::callLoop;
    using BaseClass::sA;
    using BaseClass::sB;
    using BaseClass::lS;
    using BaseClass::CyclicSpinorsA;
    using BaseClass::CyclicSpinorsB;
    using BaseClass::HelicityOrder;

    T MuR2() { return njetan->getMuR2(); }
    T sign(T x) { return (x < T(0.)) ? -T(1.) : T(1.); }

    virtual TreeValue A0(int p0, int p1, int p2, int p3, int p4);
    virtual LoopResult<T> AL(int p0, int p1, int p2, int p3, int p4);
    virtual LoopResult<T> AF(int p0, int p1, int p2, int p3, int p4);

    virtual LoopResult<T> AL(int p0, int p1, int p2, int p3, int p4, int /*pos*/) {
      return AL(p0, p1, p2, p3, p4);
    }
    virtual LoopResult<T> AF(int p0, int p1, int p2, int p3, int p4, int /*pos*/, int /*posR*/) {
      return AF(p0, p1, p2, p3, p4);
    }

    HelAmp hA013[32];
    HelAmp hA014[32];

    HelAmpLoop hAL13[32];
    HelAmpLoop hAL14[32];
    HelAmpLoop hAL32[32];
    HelAmpLoop hAL42[32];
    HelAmpLoop hAL43[32];

    HelAmpLoop hAf13[32];
    HelAmpLoop hAf14[32];

    TreeValue hA0135(const int* p);
    TreeValue hA0136(const int* p);
    TreeValue hA0139(const int* p);
    TreeValue hA01310(const int* p);
    TreeValue hA01321(const int* p);
    TreeValue hA01322(const int* p);
    TreeValue hA01325(const int* p);
    TreeValue hA01326(const int* p);

    TreeValue hA0145(const int* p);
    TreeValue hA0146(const int* p);
    TreeValue hA01413(const int* p);
    TreeValue hA01414(const int* p);
    TreeValue hA01417(const int* p);
    TreeValue hA01418(const int* p);
    TreeValue hA01425(const int* p);
    TreeValue hA01426(const int* p);

    LoopResult<T> hAL135(const int* p);
    LoopResult<T> hAL136(const int* p);
    LoopResult<T> hAL139(const int* p);
    LoopResult<T> hAL1310(const int* p);
    LoopResult<T> hAL1321(const int* p);
    LoopResult<T> hAL1322(const int* p);
    LoopResult<T> hAL1325(const int* p);
    LoopResult<T> hAL1326(const int* p);

    LoopResult<T> hAL145(const int* p);
    LoopResult<T> hAL146(const int* p);
    LoopResult<T> hAL1413(const int* p);
    LoopResult<T> hAL1414(const int* p);
    LoopResult<T> hAL1417(const int* p);
    LoopResult<T> hAL1418(const int* p);
    LoopResult<T> hAL1425(const int* p);
    LoopResult<T> hAL1426(const int* p);

    LoopResult<T> hAL323(const int* p);
    LoopResult<T> hAL325(const int* p);
    LoopResult<T> hAL3210(const int* p);
    LoopResult<T> hAL3212(const int* p);
    LoopResult<T> hAL3219(const int* p);
    LoopResult<T> hAL3221(const int* p);
    LoopResult<T> hAL3226(const int* p);
    LoopResult<T> hAL3228(const int* p);

    LoopResult<T> hAL423(const int* p);
    LoopResult<T> hAL425(const int* p);
    LoopResult<T> hAL4211(const int* p);
    LoopResult<T> hAL4213(const int* p);
    LoopResult<T> hAL4218(const int* p);
    LoopResult<T> hAL4220(const int* p);
    LoopResult<T> hAL4226(const int* p);
    LoopResult<T> hAL4228(const int* p);

    LoopResult<T> hAL433(const int* p);
    LoopResult<T> hAL437(const int* p);
    LoopResult<T> hAL439(const int* p);
    LoopResult<T> hAL4313(const int* p);
    LoopResult<T> hAL4318(const int* p);
    LoopResult<T> hAL4322(const int* p);
    LoopResult<T> hAL4324(const int* p);
    LoopResult<T> hAL4328(const int* p);

    LoopResult<T> hAf135(const int* p);
    LoopResult<T> hAf136(const int* p);
    LoopResult<T> hAf139(const int* p);
    LoopResult<T> hAf1310(const int* p);
    LoopResult<T> hAf1321(const int* p);
    LoopResult<T> hAf1322(const int* p);
    LoopResult<T> hAf1325(const int* p);
    LoopResult<T> hAf1326(const int* p);

    LoopResult<T> hAf145(const int* p);
    LoopResult<T> hAf146(const int* p);
    LoopResult<T> hAf1413(const int* p);
    LoopResult<T> hAf1414(const int* p);
    LoopResult<T> hAf1417(const int* p);
    LoopResult<T> hAf1418(const int* p);
    LoopResult<T> hAf1425(const int* p);
    LoopResult<T> hAf1426(const int* p);

    // spinor product variables
    std::complex<T> a12, a13, a14, a15, a23, a24, a25, a34, a35, a45;
    std::complex<T> b12, b13, b14, b15, b23, b24, b25, b34, b35, b45;
    // momentum twistor variables
    std::complex<T> x1, x2, x3, x4, x5;
    // 2-particle invariants
    T s12, s23, s34, s45, s15;

    void setxi(const int *ord);
    void setsij(const int *ord);
    void setaij(const int *ord);
};

template <typename T>
class Amp4q1g2_a : public Amp4q1g_a<T>
{
    typedef Amp4q1g_a<T> BaseClass;
  public:

    Amp4q1g2_a(const T scalefactor)
      : BaseClass(scalefactor, 2, amptables())
    { }

  protected:
    using Amp4q1g<T>::fvZero;
    using Amp4q1g<T>::vhel;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp4q1g2Static>();
    }

    void markZeroFv()
    {
      fvZero.reset();
      if (vhel[0] == vhel[1] || vhel[2] == vhel[3]) {
        fvZero[0] = true;
      }
      if (vhel[0] == vhel[3] || vhel[2] == vhel[1]) {
        fvZero[1] = true;
      }
    }
};

#endif /* ANALYTIC_4Q1G_ANALYTIC_H */
