/*
* analytic/0q4g-analytic.cpp
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>
#include "0q4g-analytic.h"

template <typename T>
Amp0q4g_a<T>::Amp0q4g_a(const T scalefactor)
  : BaseClass(scalefactor), hA0(), hAg(), hAf()
{
  njetan = new NJetAnalytic<T>(scalefactor, BaseClass::legsMOM());
  assert(int(sizeof(hA0)/sizeof(hA0[0])) >= (1 << njetan->legsMOM()));
  assert(int(sizeof(hAg)/sizeof(hAg[0])) >= (1 << njetan->legsMOM()));
  assert(int(sizeof(hAf)/sizeof(hAf[0])) >= (1 << njetan->legsMOM()));
//   BaseClass::clearNG();

  hA0[3]  = &Amp0q4g_a::hA03;
  hA0[5]  = &Amp0q4g_a::hA05;
  hA0[6]  = &Amp0q4g_a::hA06;
  hA0[9]  = &Amp0q4g_a::hA09;
  hA0[10]  = &Amp0q4g_a::hA010;
  hA0[12]  = &Amp0q4g_a::hA012;

  hAg[0] = &Amp0q4g_a::hAg0;
  hAg[1] = &Amp0q4g_a::hAg1;
  hAg[2] = &Amp0q4g_a::hAg2;
  hAg[3] = &Amp0q4g_a::hAg3;
  hAg[4] = &Amp0q4g_a::hAg4;
  hAg[5] = &Amp0q4g_a::hAg5;
  hAg[6] = &Amp0q4g_a::hAg6;
  hAg[7] = &Amp0q4g_a::hAg7;
  hAg[8] = &Amp0q4g_a::hAg8;
  hAg[9] = &Amp0q4g_a::hAg9;
  hAg[10] = &Amp0q4g_a::hAg10;
  hAg[11] = &Amp0q4g_a::hAg11;
  hAg[12] = &Amp0q4g_a::hAg12;
  hAg[13] = &Amp0q4g_a::hAg13;
  hAg[14] = &Amp0q4g_a::hAg14;
  hAg[15] = &Amp0q4g_a::hAg15;

  hAf[0] = &Amp0q4g_a::hAf0;
  hAf[1] = &Amp0q4g_a::hAf1;
  hAf[2] = &Amp0q4g_a::hAf2;
  hAf[3] = &Amp0q4g_a::hAf3;
  hAf[4] = &Amp0q4g_a::hAf4;
  hAf[5] = &Amp0q4g_a::hAf5;
  hAf[6] = &Amp0q4g_a::hAf6;
  hAf[7] = &Amp0q4g_a::hAf7;
  hAf[8] = &Amp0q4g_a::hAf8;
  hAf[9] = &Amp0q4g_a::hAf9;
  hAf[10] = &Amp0q4g_a::hAf10;
  hAf[11] = &Amp0q4g_a::hAf11;
  hAf[12] = &Amp0q4g_a::hAf12;
  hAf[13] = &Amp0q4g_a::hAf13;
  hAf[14] = &Amp0q4g_a::hAf14;
  hAf[15] = &Amp0q4g_a::hAf15;

}

template <typename T>
typename Amp0q4g_a<T>::TreeValue
Amp0q4g_a<T>::A0(int p0, int p1, int p2, int p3)
{
  const int ord[] = {p0, p1, p2, p3};
  const int hel = HelicityOrder(mhelint, ord);
  HelAmp const hamp = hA0[hel];
  if (hamp) {
    return i_*callTree(this, hamp, ord);
  } else {
    return TreeValue(0.);
  }
}

// the unused final argument is present since the numerical
// version of this function used some additional caching tricks
// to optimize the computation of the rational terms
template <typename T>
LoopResult<T> Amp0q4g_a<T>::AL(int p0, int p1, int p2, int p3, int /*pos*/)
{
  const int ord[] = {p0, p1, p2, p3};
  const int hel = HelicityOrder(mhelint, ord);
  HelAmpLoop const hamp = hAg[hel];
  if (hamp) {
    return callLoop(this, hamp, ord);
  } else {
    return LoopResult<T>();
  }
}

template <typename T>
LoopResult<T> Amp0q4g_a<T>::AF(int p0, int p1, int p2, int p3, int /*pos*/)
{
  const int ord[] = {p0, p1, p2, p3};
  const int hel = HelicityOrder(mhelint, ord);
  HelAmpLoop const hamp = hAf[hel];
  if (hamp) {
    return callLoop(this, hamp, ord);
  } else {
    return LoopResult<T>();
  }
}

template <typename T>
typename Amp0q4g_a<T>::TreeValue
Amp0q4g_a<T>::hA03(const int* p)
{
  return pow(sA(p[2], p[3]), 4) / CyclicSpinorsA(p);
}

template <typename T>
typename Amp0q4g_a<T>::TreeValue
Amp0q4g_a<T>::hA05(const int* p)
{
  return pow(sA(p[1], p[3]), 4) / CyclicSpinorsA(p);
}

template <typename T>
typename Amp0q4g_a<T>::TreeValue
Amp0q4g_a<T>::hA06(const int* p)
{
  return pow(sA(p[0], p[3]), 4) / CyclicSpinorsA(p);
}

template <typename T>
typename Amp0q4g_a<T>::TreeValue
Amp0q4g_a<T>::hA09(const int* p)
{
  return pow(sA(p[1], p[2]), 4) / CyclicSpinorsA(p);
}

template <typename T>
typename Amp0q4g_a<T>::TreeValue
Amp0q4g_a<T>::hA010(const int* p)
{
  return pow(sA(p[0], p[2]), 4) / CyclicSpinorsA(p);
}

template <typename T>
typename Amp0q4g_a<T>::TreeValue
Amp0q4g_a<T>::hA012(const int* p)
{
  return pow(sA(p[0], p[1]), 4) / CyclicSpinorsA(p);
}

template <typename T>
LoopResult<T> Amp0q4g_a<T>::hAg0(const int* p) /* -1-1-1-1 */
{
  // all-minus helicity configuration //
  const std::complex<T> phase = i_*sA(p[0], p[1])*sA(p[2], p[3])/(sB(p[0], p[1])*sB(p[2], p[3]));
  const T rat = -T(1.)/T(3.);
  const EpsTriplet<T> amp(phase*rat, T(0.), T(0.));
  const EpsTriplet<T> camp(conj(phase)*rat, T(0.), T(0.));
  const LoopResult<T> res = {amp, camp};
  return res;
}

template <typename T>
LoopResult<T> Amp0q4g_a<T>::hAg1(const int* p) /* 1-1-1-1 */
{
  const int nord[] = {p[1], p[2], p[3], p[0]};
  return hAg8(nord);
}

template <typename T>
LoopResult<T> Amp0q4g_a<T>::hAg2(const int* p) /* -11-1-1 */
{
  const int nord[] = {p[2], p[3], p[0], p[1]};
  return hAg8(nord);
}

template <typename T>
LoopResult<T> Amp0q4g_a<T>::hAg3(const int* p) /* 11-1-1 */
{
  const int nord[] = {p[2], p[3], p[0], p[1]};
  return hAg12(nord);
}

template <typename T>
LoopResult<T> Amp0q4g_a<T>::hAg4(const int* p) /* -1-11-1 */
{
  const int nord[] = {p[3], p[0], p[1], p[2]};
  return hAg8(nord);
}

template <typename T>
LoopResult<T> Amp0q4g_a<T>::hAg5(const int* p) /* 1-11-1 */
{
  const int nord[] = {p[1], p[2], p[3], p[0]};
  return hAg10(nord);
}

template <typename T>
LoopResult<T> Amp0q4g_a<T>::hAg6(const int* p) /* -111-1 */
{
  const int nord[] = {p[3], p[0], p[1], p[2]};
  return hAg12(nord);
}

template <typename T>
LoopResult<T> Amp0q4g_a<T>::hAg7(const int* p) /* 111-1 */
{
  const int nord[] = {p[3], p[0], p[1], p[2]};
  return hAg14(nord);
}

template <typename T>
LoopResult<T> Amp0q4g_a<T>::hAg8(const int* p) /* -1-1-11 */
{
  // single-plus helicity configuration //
  const std::complex<T> phase = i_*sA(p[0], p[1])*sA(p[0], p[2])*sB(p[0], p[3])/(sB(p[0], p[1])*sB(p[0], p[2])*sA(p[0], p[3]));
  const T s = lS(p[0], p[1]);
  const T t = lS(p[1], p[2]);
  const T u = -s-t;
  const T rat = -u*u/(s*t*T(3.));
  const EpsTriplet<T> amp(phase*rat, T(0.), T(0.));
  const EpsTriplet<T> camp(conj(phase)*rat, T(0.), T(0.));
  const LoopResult<T> res = {amp, camp};
  return res;
}

template <typename T>
LoopResult<T> Amp0q4g_a<T>::hAg9(const int* p) /* 1-1-11 */
{
  const int nord[] = {p[1], p[2], p[3], p[0]};
  return hAg12(nord);
}

template <typename T>
LoopResult<T> Amp0q4g_a<T>::hAg10(const int* p) /* -11-11 */
{
  const std::complex<T> phase = i_*hA010(p);
  const T s = lS(p[0], p[1]);
  const T t = lS(p[1], p[2]);
  const T u = -s-t;
  const T s2 = s*s;
  const T t2 = t*t;
  const T st = s*t;
  const T u2 = u*u;
  const T u4 = u2*u2;
  const T x1 = s2 + t2 + st;
  const T x2 = T(2.)*(s2 + t2) + T(3.)*st;
  const T x3 = T(14.)*s2 + T(11.)*t2 + T(19.)*st;
  const T x4 = T(11.)*s2 + T(14.)*t2 + T(19.)*st;
  const T x5 = T(2.)*(s2 + t2) - T(5.)*st;

  const T c4 = -st*x1*x1/u4;
  const T c3 = -T(2.)*x2*st/u4;
  const T c2s = t*x3/(u2*u*T(3.));
  const T c2t = s*x4/(u2*u*T(3.));
  const T rat = x5/(u2*T(9.));
  EpsTriplet<T> amp;

  amp += c4*njetan->I4(s, t, T(0.), T(0.), T(0.), T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c3*(  s*njetan->I3(s, T(0.), T(0.), T(0.), T(0.), T(0.))
             + t*njetan->I3(t, T(0.), T(0.), T(0.), T(0.), T(0.)));
  amp += c2s*njetan->I2(s, T(0.), T(0.));
  amp += c2t*njetan->I2(t, T(0.), T(0.));
  amp += rat;

  const LoopResult<T> res = {phase*amp, conj(phase)*amp};
  return res;
}

template <typename T>
LoopResult<T> Amp0q4g_a<T>::hAg11(const int* p) /* 11-11 */
{
  const int nord[] = {p[2], p[3], p[0], p[1]};
  return hAg14(nord);
}

template <typename T>
LoopResult<T> Amp0q4g_a<T>::hAg12(const int* p) /* -1-111 */
{
  const std::complex<T> phase = i_*hA012(p);
  const T s = lS(p[0], p[1]);
  const T t = lS(p[1], p[2]);

  const T c4 = -s*t;
  const T c2 = -T(11.)/T(3.);
  const T rat = T(2.)/T(9.);
  EpsTriplet<T> amp;

  amp += c4*njetan->I4(s, t, T(0.), T(0.), T(0.), T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c2*njetan->I2(t, T(0.), T(0.));
  amp += rat;

  const LoopResult<T> res = {phase*amp, conj(phase)*amp};
  return res;
}

template <typename T>
LoopResult<T> Amp0q4g_a<T>::hAg13(const int* p) /* 1-111 */
{
  const int nord[] = {p[1], p[2], p[3], p[0]};
  return hAg14(nord);
}

template <typename T>
LoopResult<T> Amp0q4g_a<T>::hAg14(const int* p) /* -1111 */
{
  // single-minus helicity configuration //
  const std::complex<T> phase = i_*sA(p[0], p[1])*sB(p[1], p[3])*sB(p[1], p[2])/(sB(p[0], p[1])*sA(p[1], p[3])*sA(p[1], p[2]));
  const T s = lS(p[0], p[1]);
  const T t = lS(p[1], p[2]);
  const T u = -s-t;
  const T rat = -u*u/(s*t*T(3.));
  const EpsTriplet<T> amp(phase*rat, T(0.), T(0.));
  const EpsTriplet<T> camp(conj(phase)*rat, T(0.), T(0.));
  const LoopResult<T> res = {amp, camp};
  return res;
}

template <typename T>
LoopResult<T> Amp0q4g_a<T>::hAg15(const int* p) /* 1111 */
{
  // all-plus helicity configuration //
  const std::complex<T> phase = i_*sB(p[0], p[1])*sB(p[2], p[3])/(sA(p[0], p[1])*sA(p[2], p[3]));
  const T rat = -T(1.)/T(3.);
  const EpsTriplet<T> amp(phase*rat, T(0.), T(0.));
  const EpsTriplet<T> camp(conj(phase)*rat, T(0.), T(0.));
  const LoopResult<T> res = {amp, camp};
  return res;
}

template <typename T>
LoopResult<T> Amp0q4g_a<T>::hAf0(const int* p) /* -1-1-1-1 */
{
  // all-minus helicity configuration //
  const std::complex<T> phase = i_*sA(p[0], p[1])*sA(p[2], p[3])/(sB(p[0], p[1])*sB(p[2], p[3]));
  const T rat = -T(1.)/T(3.);
  const EpsTriplet<T> amp(phase*rat, T(0.), T(0.));
  const EpsTriplet<T> camp(conj(phase)*rat, T(0.), T(0.));
  const LoopResult<T> res = {amp, camp};
  return res;
}

template <typename T>
LoopResult<T> Amp0q4g_a<T>::hAf1(const int* p) /* 1-1-1-1 */
{
  const int nord[] = {p[1], p[2], p[3], p[0]};
  return hAf8(nord);
}

template <typename T>
LoopResult<T> Amp0q4g_a<T>::hAf2(const int* p) /* -11-1-1 */
{
  const int nord[] = {p[2], p[3], p[0], p[1]};
  return hAf8(nord);
}

template <typename T>
LoopResult<T> Amp0q4g_a<T>::hAf3(const int* p) /* 11-1-1 */
{
  const int nord[] = {p[2], p[3], p[0], p[1]};
  return hAf12(nord);
}

template <typename T>
LoopResult<T> Amp0q4g_a<T>::hAf4(const int* p) /* -1-11-1 */
{
  const int nord[] = {p[3], p[0], p[1], p[2]};
  return hAf8(nord);
}

template <typename T>
LoopResult<T> Amp0q4g_a<T>::hAf5(const int* p) /* 1-11-1 */
{
  const int nord[] = {p[1], p[2], p[3], p[0]};
  return hAf10(nord);
}

template <typename T>
LoopResult<T> Amp0q4g_a<T>::hAf6(const int* p) /* -111-1 */
{
  const int nord[] = {p[3], p[0], p[1], p[2]};
  return hAf12(nord);
}

template <typename T>
LoopResult<T> Amp0q4g_a<T>::hAf7(const int* p) /* 111-1 */
{
  const int nord[] = {p[3], p[0], p[1], p[2]};
  return hAf14(nord);
}

template <typename T>
LoopResult<T> Amp0q4g_a<T>::hAf8(const int* p) /* -1-1-11 */
{
  // single-plus helicity configuration //
  const std::complex<T> phase = i_*sA(p[0], p[1])*sA(p[0], p[2])*sB(p[0], p[3])/(sB(p[0], p[1])*sB(p[0], p[2])*sA(p[0], p[3]));
  const T s = lS(p[0], p[1]);
  const T t = lS(p[1], p[2]);
  const T u = -s-t;
  const T rat = -u*u/(s*t*T(3.));
  const EpsTriplet<T> amp(phase*rat, T(0.), T(0.));
  const EpsTriplet<T> camp(conj(phase)*rat, T(0.), T(0.));
  const LoopResult<T> res = {amp, camp};
  return res;
}

template <typename T>
LoopResult<T> Amp0q4g_a<T>::hAf9(const int* p) /* 1-1-11 */
{
  const int nord[] = {p[1], p[2], p[3], p[0]};
  return hAf12(nord);
}

template <typename T>
LoopResult<T> Amp0q4g_a<T>::hAf10(const int* p) /* -11-11 */
{
  const std::complex<T> phase = i_*hA010(p);
  const T s = lS(p[0], p[1]);
  const T t = lS(p[1], p[2]);
  const T u = -s-t;
  const T s2 = s*s;
  const T t2 = t*t;
  const T st = s*t;
  const T u2 = u*u;
  const T u4 = u2*u2;
  const T x1 = s2 + t2;
  const T x2 = T(5.)*s2 + T(2.)*t2 + st;
  const T x3 = T(2.)*s2 + T(5.)*t2 + st;
  const T x4 = T(2.)*x1 - T(5.)*st;

  const T c4 = T(0.5)*x1*s2*t2/u4;
  const T c3 = -x1*s*t/u4;
  const T c2s = t*x2/(u2*u*T(3.));
  const T c2t = s*x3/(u2*u*T(3.));
  const T rat = x4/(u2*T(9.));
  EpsTriplet<T> amp;

  amp += c4*njetan->I4(s, t, T(0.), T(0.), T(0.), T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c3*(  s*njetan->I3(s, T(0.), T(0.), T(0.), T(0.), T(0.))
             + t*njetan->I3(t, T(0.), T(0.), T(0.), T(0.), T(0.)));
  amp += c2s*njetan->I2(s, T(0.), T(0.));
  amp += c2t*njetan->I2(t, T(0.), T(0.));
  amp += rat;

  const LoopResult<T> res = {phase*amp, conj(phase)*amp};
  return res;
}

template <typename T>
LoopResult<T> Amp0q4g_a<T>::hAf11(const int* p) /* 11-11 */
{
  const int nord[] = {p[2], p[3], p[0], p[1]};
  return hAf14(nord);
}

template <typename T>
LoopResult<T> Amp0q4g_a<T>::hAf12(const int* p) /* -1-111 */
{
  const std::complex<T> phase = i_*hA012(p);
  const T t = lS(p[1], p[2]);

  const T c2 = -T(2.)/T(3.);
  const T rat = T(2.)/T(9.);
  EpsTriplet<T> amp;

  amp += c2*njetan->I2(t, T(0.), T(0.));
  amp += rat;

  const LoopResult<T> res = {phase*amp, conj(phase)*amp};
  return res;
}

template <typename T>
LoopResult<T> Amp0q4g_a<T>::hAf13(const int* p) /* 1-111 */
{
  const int nord[] = {p[1], p[2], p[3], p[0]};
  return hAf14(nord);
}

template <typename T>
LoopResult<T> Amp0q4g_a<T>::hAf14(const int* p) /* -1111 */
{
  // single-minus helicity configuration //
  const std::complex<T> phase = i_*sA(p[0], p[1])*sB(p[1], p[3])*sB(p[1], p[2])/(sB(p[0], p[1])*sA(p[1], p[3])*sA(p[1], p[2]));
  const T s = lS(p[0], p[1]);
  const T t = lS(p[1], p[2]);
  const T u = -s-t;
  const T rat = -u*u/(s*t*T(3.));
  const EpsTriplet<T> amp(phase*rat, T(0.), T(0.));
  const EpsTriplet<T> camp(conj(phase)*rat, T(0.), T(0.));
  const LoopResult<T> res = {amp, camp};
  return res;
}

template <typename T>
LoopResult<T> Amp0q4g_a<T>::hAf15(const int* p) /* 1111 */
{
  // all-plus helicity configuration //
  const std::complex<T> phase = i_*sB(p[0], p[1])*sB(p[2], p[3])/(sA(p[0], p[1])*sA(p[2], p[3]));
  const T rat = -T(1.)/T(3.);
  const EpsTriplet<T> amp(phase*rat, T(0.), T(0.));
  const EpsTriplet<T> camp(conj(phase)*rat, T(0.), T(0.));
  const LoopResult<T> res = {amp, camp};
  return res;
}

#ifdef USE_SD
  template class Amp0q4g_a<double>;
#endif
#ifdef USE_DD
  template class Amp0q4g_a<dd_real>;
#endif
#ifdef USE_QD
  template class Amp0q4g_a<qd_real>;
#endif
#ifdef USE_VC
  template class Amp0q4g_a<Vc::double_v>;
#endif
