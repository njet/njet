/*
* analytic/0q5g-analytic.cpp
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>
#include "0q5g-analytic.h"

template <typename T>
Amp0q5g_a<T>::Amp0q5g_a(const T scalefactor, const int mFC,
                        const NJetAmpTables& tables)
  : BaseClass(scalefactor, mFC, tables), hA0()
{
  njetan = new NJetAnalytic<T>(scalefactor, BaseClass::legsMOM());
  assert(int(sizeof(hA0)/sizeof(hA0[0])) >= (1 << njetan->legsMOM()));
//   BaseClass::clearNG();

  hA0[3]  = &Amp0q5g_a::hA03;
  hA0[5]  = &Amp0q5g_a::hA05;
  hA0[6]  = &Amp0q5g_a::hA06;
  hA0[7]  = &Amp0q5g_a::hA07;
  hA0[9]  = &Amp0q5g_a::hA09;
  hA0[10]  = &Amp0q5g_a::hA010;
  hA0[11]  = &Amp0q5g_a::hA011;
  hA0[12]  = &Amp0q5g_a::hA012;
  hA0[13]  = &Amp0q5g_a::hA013;
  hA0[14]  = &Amp0q5g_a::hA014;
  hA0[17]  = &Amp0q5g_a::hA017;
  hA0[18]  = &Amp0q5g_a::hA018;
  hA0[19]  = &Amp0q5g_a::hA019;
  hA0[20]  = &Amp0q5g_a::hA020;
  hA0[21]  = &Amp0q5g_a::hA021;
  hA0[22]  = &Amp0q5g_a::hA022;
  hA0[24]  = &Amp0q5g_a::hA024;
  hA0[25]  = &Amp0q5g_a::hA025;
  hA0[26]  = &Amp0q5g_a::hA026;
  hA0[28]  = &Amp0q5g_a::hA028;

  hAg[0] = &Amp0q5g_a<T>::hAg0;
  hAg[1] = &Amp0q5g_a<T>::hAg1;
  hAg[2] = &Amp0q5g_a<T>::hAg2;
  hAg[3] = &Amp0q5g_a<T>::hAg3;
  hAg[4] = &Amp0q5g_a<T>::hAg4;
  hAg[5] = &Amp0q5g_a<T>::hAg5;
  hAg[6] = &Amp0q5g_a<T>::hAg6;
  hAg[7] = &Amp0q5g_a<T>::hAg7;
  hAg[8] = &Amp0q5g_a<T>::hAg8;
  hAg[9] = &Amp0q5g_a<T>::hAg9;
  hAg[10] = &Amp0q5g_a<T>::hAg10;
  hAg[11] = &Amp0q5g_a<T>::hAg11;
  hAg[12] = &Amp0q5g_a<T>::hAg12;
  hAg[13] = &Amp0q5g_a<T>::hAg13;
  hAg[14] = &Amp0q5g_a<T>::hAg14;
  hAg[15] = &Amp0q5g_a<T>::hAg15;
  hAg[16] = &Amp0q5g_a<T>::hAg16;
  hAg[17] = &Amp0q5g_a<T>::hAg17;
  hAg[18] = &Amp0q5g_a<T>::hAg18;
  hAg[19] = &Amp0q5g_a<T>::hAg19;
  hAg[20] = &Amp0q5g_a<T>::hAg20;
  hAg[21] = &Amp0q5g_a<T>::hAg21;
  hAg[22] = &Amp0q5g_a<T>::hAg22;
  hAg[23] = &Amp0q5g_a<T>::hAg23;
  hAg[24] = &Amp0q5g_a<T>::hAg24;
  hAg[25] = &Amp0q5g_a<T>::hAg25;
  hAg[26] = &Amp0q5g_a<T>::hAg26;
  hAg[27] = &Amp0q5g_a<T>::hAg27;
  hAg[28] = &Amp0q5g_a<T>::hAg28;
  hAg[29] = &Amp0q5g_a<T>::hAg29;
  hAg[30] = &Amp0q5g_a<T>::hAg30;
  hAg[31] = &Amp0q5g_a<T>::hAg31;

  hAf[0] = &Amp0q5g_a<T>::hAf0;
  hAf[1] = &Amp0q5g_a<T>::hAf1;
  hAf[2] = &Amp0q5g_a<T>::hAf2;
  hAf[3] = &Amp0q5g_a<T>::hAf3;
  hAf[4] = &Amp0q5g_a<T>::hAf4;
  hAf[5] = &Amp0q5g_a<T>::hAf5;
  hAf[6] = &Amp0q5g_a<T>::hAf6;
  hAf[7] = &Amp0q5g_a<T>::hAf7;
  hAf[8] = &Amp0q5g_a<T>::hAf8;
  hAf[9] = &Amp0q5g_a<T>::hAf9;
  hAf[10] = &Amp0q5g_a<T>::hAf10;
  hAf[11] = &Amp0q5g_a<T>::hAf11;
  hAf[12] = &Amp0q5g_a<T>::hAf12;
  hAf[13] = &Amp0q5g_a<T>::hAf13;
  hAf[14] = &Amp0q5g_a<T>::hAf14;
  hAf[15] = &Amp0q5g_a<T>::hAf15;
  hAf[16] = &Amp0q5g_a<T>::hAf16;
  hAf[17] = &Amp0q5g_a<T>::hAf17;
  hAf[18] = &Amp0q5g_a<T>::hAf18;
  hAf[19] = &Amp0q5g_a<T>::hAf19;
  hAf[20] = &Amp0q5g_a<T>::hAf20;
  hAf[21] = &Amp0q5g_a<T>::hAf21;
  hAf[22] = &Amp0q5g_a<T>::hAf22;
  hAf[23] = &Amp0q5g_a<T>::hAf23;
  hAf[24] = &Amp0q5g_a<T>::hAf24;
  hAf[25] = &Amp0q5g_a<T>::hAf25;
  hAf[26] = &Amp0q5g_a<T>::hAf26;
  hAf[27] = &Amp0q5g_a<T>::hAf27;
  hAf[28] = &Amp0q5g_a<T>::hAf28;
  hAf[29] = &Amp0q5g_a<T>::hAf29;
  hAf[30] = &Amp0q5g_a<T>::hAf30;
  hAf[31] = &Amp0q5g_a<T>::hAf31;
}

template <typename T>
void Amp0q5g_a<T>::setsij(const int *p)
{
  s12 = lS(p[0], p[1]);
  s23 = lS(p[1], p[2]);
  s34 = lS(p[2], p[3]);
  s45 = lS(p[3], p[4]);
  s15 = lS(p[4], p[0]);
}

template <typename T>
void Amp0q5g_a<T>::setxi(const int *p)
{
  x1 = lS(p[0], p[1]);
  x2 = -sB(p[0], p[1])*sA(p[1],p[2])*sA(p[0],p[3])/sA(p[2],p[3]);
  x3 = -sB(p[0], p[1])*sA(p[1],p[2])*sA(p[0],p[3])*sA(p[0],p[4])/(sA(p[0],p[2])*sA(p[3], p[4]));
  x4 = sA(p[2], p[3])*sB(p[1], p[2])/(sA(p[0], p[3])*sB(p[0], p[1]));
  x5 = -sA(p[2],p[3])*(lS(p[1],p[2]) - lS(p[3],p[4]))/sB(p[0],p[1])/(sA(p[0],p[3])*sA(p[1],p[2]));
}

template <typename T>
typename Amp0q5g_a<T>::TreeValue
Amp0q5g_a<T>::A0(int p0, int p1, int p2, int p3, int p4)
{
  const int ord[] = {p0, p1, p2, p3, p4};
  const int hel = HelicityOrder(mhelint, ord);
  HelAmp const hamp = hA0[hel];
  if (hamp) {
    return i_*callTree(this, hamp, ord);
  } else {
    return TreeValue(0.);
  }
}

// the unused final argument is present since the numerical
// version of this function used some additional caching tricks
// to optimize the computation of the rational terms
template <typename T>
LoopResult<T> Amp0q5g_a<T>::AL(int p0, int p1, int p2, int p3, int p4)
{
  const int ord[] = {p0, p1, p2, p3, p4};
  const int hel = HelicityOrder(mhelint, ord);
  HelAmpLoop const hamp = hAg[hel];
  if (hamp) {
    return callLoop(this, hamp, ord);
  } else {
    return LoopResult<T>();
  }
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::AF(int p0, int p1, int p2, int p3, int p4)
{
  const int ord[] = {p0, p1, p2, p3, p4};
  const int hel = HelicityOrder(mhelint, ord);
  HelAmpLoop const hamp = hAf[hel];
  if (hamp) {
    return callLoop(this, hamp, ord);
  } else {
    return LoopResult<T>();
  }
}

template <typename T>
typename Amp0q5g_a<T>::TreeValue
Amp0q5g_a<T>::hA03(const int* p) /* ++--- */
{
  return -pow(sB(p[0], p[1]), 4)/CyclicSpinorsB(p);
}

template <typename T>
typename Amp0q5g_a<T>::TreeValue
Amp0q5g_a<T>::hA05(const int* p) /* +-+-- */
{
  return -pow(sB(p[0], p[2]), 4)/CyclicSpinorsB(p);
}

template <typename T>
typename Amp0q5g_a<T>::TreeValue
Amp0q5g_a<T>::hA06(const int* p) /* -++-- */
{
  return -pow(sB(p[1], p[2]), 4)/CyclicSpinorsB(p);
}

template <typename T>
typename Amp0q5g_a<T>::TreeValue
Amp0q5g_a<T>::hA07(const int* p) /* +++-- */
{
  return pow(sA(p[3], p[4]), 4)/CyclicSpinorsA(p);
}

template <typename T>
typename Amp0q5g_a<T>::TreeValue
Amp0q5g_a<T>::hA09(const int* p) /* +--+- */
{
  return -pow(sB(p[0], p[3]), 4)/CyclicSpinorsB(p);
}

template <typename T>
typename Amp0q5g_a<T>::TreeValue
Amp0q5g_a<T>::hA010(const int* p) /* -+-+- */
{
  return -pow(sB(p[1], p[3]), 4)/CyclicSpinorsB(p);
}

template <typename T>
typename Amp0q5g_a<T>::TreeValue
Amp0q5g_a<T>::hA011(const int* p) /* ++-+- */
{
  return pow(sA(p[2], p[4]), 4)/CyclicSpinorsA(p);
}

template <typename T>
typename Amp0q5g_a<T>::TreeValue
Amp0q5g_a<T>::hA012(const int* p) /* --++- */
{
  return -pow(sB(p[2], p[3]), 4)/CyclicSpinorsB(p);
}

template <typename T>
typename Amp0q5g_a<T>::TreeValue
Amp0q5g_a<T>::hA013(const int* p) /* +-++- */
{
  return pow(sA(p[1], p[4]), 4)/CyclicSpinorsA(p);
}

template <typename T>
typename Amp0q5g_a<T>::TreeValue
Amp0q5g_a<T>::hA014(const int* p) /* -+++- */
{
  return pow(sA(p[0], p[4]), 4)/CyclicSpinorsA(p);
}

template <typename T>
typename Amp0q5g_a<T>::TreeValue
Amp0q5g_a<T>::hA017(const int* p) /* +---+ */
{
  return -pow(sB(p[0], p[4]), 4)/CyclicSpinorsB(p);
}

template <typename T>
typename Amp0q5g_a<T>::TreeValue
Amp0q5g_a<T>::hA018(const int* p) /* -+--+ */
{
  return -pow(sB(p[1], p[4]), 4)/CyclicSpinorsB(p);
}

template <typename T>
typename Amp0q5g_a<T>::TreeValue
Amp0q5g_a<T>::hA019(const int* p) /* ++--+ */
{
  return pow(sA(p[2], p[3]), 4)/CyclicSpinorsA(p);
}

template <typename T>
typename Amp0q5g_a<T>::TreeValue
Amp0q5g_a<T>::hA020(const int* p) /* --+-+ */
{
  return -pow(sB(p[2], p[4]), 4)/CyclicSpinorsB(p);
}

template <typename T>
typename Amp0q5g_a<T>::TreeValue
Amp0q5g_a<T>::hA021(const int* p) /* +-+-+ */
{
  return pow(sA(p[1], p[3]), 4)/CyclicSpinorsA(p);
}

template <typename T>
typename Amp0q5g_a<T>::TreeValue
Amp0q5g_a<T>::hA022(const int* p) /* -++-+ */
{
  return pow(sA(p[0], p[3]), 4)/CyclicSpinorsA(p);
}

template <typename T>
typename Amp0q5g_a<T>::TreeValue
Amp0q5g_a<T>::hA024(const int* p) /* ---++ */
{
  return -pow(sB(p[3], p[4]), 4)/CyclicSpinorsB(p);
}

template <typename T>
typename Amp0q5g_a<T>::TreeValue
Amp0q5g_a<T>::hA025(const int* p) /* +--++ */
{
  return pow(sA(p[1], p[2]), 4)/CyclicSpinorsA(p);
}

template <typename T>
typename Amp0q5g_a<T>::TreeValue
Amp0q5g_a<T>::hA026(const int* p) /* -+-++ */
{
  return pow(sA(p[0], p[2]), 4)/CyclicSpinorsA(p);
}

template <typename T>
typename Amp0q5g_a<T>::TreeValue
Amp0q5g_a<T>::hA028(const int* p) /* --+++ */
{
  return pow(sA(p[0], p[1]), 4)/CyclicSpinorsA(p);
}

////  gluon loops ////

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAg0(const int* p) /* -1-1-1-1-1 */
{
  const LoopResult<T> camp = -hAg31(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAg1(const int* p) /* 1-1-1-1-1 */
{
  const LoopResult<T> camp = -hAg30(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAg2(const int* p) /* -11-1-1-1 */
{
  const int nord[] = {p[1],p[2],p[3],p[4],p[0]};
  return hAg1(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAg3(const int* p) /* 11-1-1-1 */
{
  const LoopResult<T> camp = -hAg28(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAg4(const int* p) /* -1-11-1-1 */
{
  const int nord[] = {p[2],p[3],p[4],p[0],p[1]};
  return hAg1(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAg5(const int* p) /* 1-11-1-1 */
{
  const LoopResult<T> camp = -hAg26(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAg6(const int* p) /* -111-1-1 */
{
  const int nord[] = {p[1],p[2],p[3],p[4],p[0]};
  return hAg3(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAg7(const int* p) /* 111-1-1 */
{
  const int nord[] = {p[3],p[4],p[0],p[1],p[2]};
  return hAg28(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAg8(const int* p) /* -1-1-11-1 */
{
  const int nord[] = {p[3],p[4],p[0],p[1],p[2]};
  return hAg1(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAg9(const int* p) /* 1-1-11-1 */
{
  const int nord[] = {p[3],p[4],p[0],p[1],p[2]};
  return hAg5(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAg10(const int* p) /* -11-11-1 */
{
  const int nord[] = {p[1],p[2],p[3],p[4],p[0]};
  return hAg5(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAg11(const int* p) /* 11-11-1 */
{
  const int nord[] = {p[2],p[3],p[4],p[0],p[1]};
  return hAg26(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAg12(const int* p) /* -1-111-1 */
{
  const int nord[] = {p[2],p[3],p[4],p[0],p[1]};
  return hAg3(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAg13(const int* p) /* 1-111-1 */
{
  const int nord[] = {p[4],p[0],p[1],p[2],p[3]};
  return hAg26(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAg14(const int* p) /* -1111-1 */
{
  const int nord[] = {p[4],p[0],p[1],p[2],p[3]};
  return hAg28(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAg15(const int* p) /* 1111-1 */
{
  const int nord[] = {p[4],p[0],p[1],p[2],p[3]};
  return hAg30(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAg16(const int* p) /* -1-1-1-11 */
{
  const int nord[] = {p[4],p[0],p[1],p[2],p[3]};
  return hAg1(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAg17(const int* p) /* 1-1-1-11 */
{
  const int nord[] = {p[4],p[0],p[1],p[2],p[3]};
  return hAg3(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAg18(const int* p) /* -11-1-11 */
{
  const int nord[] = {p[4],p[0],p[1],p[2],p[3]};
  return hAg5(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAg19(const int* p) /* 11-1-11 */
{
  const int nord[] = {p[2],p[3],p[4],p[0],p[1]};
  return hAg28(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAg20(const int* p) /* -1-11-11 */
{
  const int nord[] = {p[2],p[3],p[4],p[0],p[1]};
  return hAg5(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAg21(const int* p) /* 1-11-11 */
{
  const int nord[] = {p[1],p[2],p[3],p[4],p[0]};
  return hAg26(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAg22(const int* p) /* -111-11 */
{
  const int nord[] = {p[3],p[4],p[0],p[1],p[2]};
  return hAg26(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAg23(const int* p) /* 111-11 */
{
  const int nord[] = {p[3],p[4],p[0],p[1],p[2]};
  return hAg30(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAg24(const int* p) /* -1-1-111 */
{
  const int nord[] = {p[3],p[4],p[0],p[1],p[2]};
  return hAg3(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAg25(const int* p) /* 1-1-111 */
{
  const int nord[] = {p[1],p[2],p[3],p[4],p[0]};
  return hAg28(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAg26(const int* p) /* -11-111 */
{
  const std::complex<T> phase = i_*hA026(p);
  setxi(p);
  setsij(p);

  const TreeValue t1 = x2*x4;
  const TreeValue c1234 = -t1*x1*T(0.5);
  const TreeValue t2 = x2 + x3;
  TreeValue t3 = t2*t2;
  const TreeValue t4 = t3*t3;
  TreeValue t5 = t2*t3;
  const TreeValue t6 = x3*x3;
  const TreeValue t7 = t6*t6;
  const TreeValue t8 = t6*t7;
  const TreeValue t9 = x3*t7;
  const TreeValue t10 = x3*t6;
  const TreeValue t11 = x1*x1;
  TreeValue t12 = t11*t11;
  TreeValue t13 = x1*t11;
  const TreeValue t14 = t13*t12;
  TreeValue t15 = t11*t12;
  TreeValue t16 = x1*t12;
  const TreeValue t17 = x2*x2;
  const TreeValue t18 = t17*t17;
  TreeValue t19 = x2*t17;
  TreeValue t20 = t17*t18;
  const TreeValue t21 = x2*t18;
  const TreeValue t22 = -T(1.) + x5;
  TreeValue t23 = x1*t2;
  TreeValue t24 = x2*x3;
  TreeValue t25 = t24 + t23;
  t25 = T(1.)/t25;
  TreeValue t26 = t25*t25;
  const TreeValue t27 = t26*t26;
  TreeValue t28 = x3*t22;
  TreeValue t29 = t28*T(0.5);
  const TreeValue c1235 = t29*x1*(t18*t7 + t12*t4)*t27;
  const TreeValue t30 = x4 - x5;
  TreeValue t31 = x2*t30;
  const TreeValue c1245 = t31*t29;
  t29 = -T(1.) + x4;
  const TreeValue t32 = x3*t29 + t1;
  TreeValue t33 = x1*t32;
  TreeValue t34 = t31*x3;
  const TreeValue t35 = t33 + t34;
  TreeValue t36 = t35*T(0.5);
  const TreeValue c1345 = -t36*t30;
  TreeValue t37 = x1 + x2;
  TreeValue t38 = T(1.)/t37;
  const TreeValue t39 = t38*t38;
  TreeValue t40 = t39*t39;
  TreeValue t41 = t38*t39;
  const TreeValue c2345 = -t36*(t12 + t18)*x4*t40;
  t36 = T(3.)*x2;
  TreeValue t42 = t17*t6;
  TreeValue t43 = T(2.)*t11;
  TreeValue t44 = T(2.)*t42 + t23*t36*x3 + t43*t3;
  TreeValue t45 = t11*t2;
  const TreeValue c123 = -t24*t45*t44*t27;
  const TreeValue c125 = t23*x2*t6*t44*t22*t27;
  TreeValue t46 = t23*t29;
  const TreeValue c135 = t23*x3*t44*(t46 + t34)*t27;
  t34 = t11 + t17;
  TreeValue t47 = t36*x1;
  TreeValue t48 = T(2.)*t34 + t47;
  TreeValue t49 = x1*t17;
  const TreeValue c234 = -t49*t48*x4*t40;
  const TreeValue t50 = t17*x3;
  t45 = -t50 + t45;
  const TreeValue t51 = T(2.)*x3;
  TreeValue t52 = x2 + t51;
  TreeValue t53 = x1*x2;
  const TreeValue c235 = -t53*t45*(t47*(t50*t52 + t11*(t17 + (t36 + t51)*x3)) + T(2.)*t12*t3
    + T(2.)*(t11*t52*t52 + t42)*t17)*(t24*t29 + t33)*t40*t27;
  const TreeValue c245 = -t53*t48*(t1 + t28)*t40;
  const TreeValue c345 = -x1*t48*t35*t40;
//  c2_2315_0 = T(0.);
  t28 = T(1.)/x3;
  t40 = T(1.)/t22;
  const TreeValue c2_2315_1 = -T(2.)*t33*t48*t41*t28*t40;
  t33 = x3 - t2*x4;
  t47 = t40*t40;
  t48 = t28*t28;
  const TreeValue c2_2315_2 = t11*t33*t33*t39*t48*t47;
  t33 = t32*t32;
  const TreeValue c2_2315_3 = -T(2.)/T(3.)*x1*t32*t33*t38*t28*t48*t40*t47;
  t33 = T(49.)*x3;
  t47 = T(5.)*t17;
  t48 = T(43.)*x3;
  t52 = T(3.)*t11;
  t53 = T(1.)/T(6.);
  const TreeValue c2_1534_0 = t53*(T(11.)*t19*t18*t7 - T(11.)*t14*t4 - t15*x2*t5*(T(5.)*x2 + t33)
    + x1*t20*t10*(T(16.)*x2 + t33) + t16*t17*t3*(t47 + (-T(46.)*x2 - T(81.)*x3)*x3)
    + t52*t21*t6*(T(12.)*t17 + (T(28.)*x2 + T(27.)*x3)*x3) + t12*t19*t2*(T(11.)*t19 + (t47
    + (-T(41.)*x2 - t48)*x3)*x3) + t13*t18*x3*(T(20.)*t19 + (T(72.)*t17 + (T(84.)*x2
    + t48)*x3)*x3))*t41*t27;
  t33 = T(2.)*x1;
  t23 = (-x1 + t31)*x3 + t23*x4;
  t23 = T(1.)/t23;
  const TreeValue c2_1534_1 = t43*t18*x3*(t11*t17*(t52 + (T(4.)*x1 + T(2.)*x2)*x2) + t33*t24*t37*(t52
    + (t33 + x2)*x2) + T(3.)*t37*t37*t34*t6)*t41*t27*t23;
  t31 = t23*t23;
  t33 = t23*t31;
  const TreeValue c2_1534_2 = t13*t20*t6*(T(2.)*t45 + t49)*t39*t27*t31;
  const TreeValue c2_1534_3 = T(2.)/T(3.)*t12*t18*t18*t10*t38*t27*t33;
//  c2_1234_0 = T(0.);
  const TreeValue c2_1234_1 = -T(2.)*t46*t24*t44*t25*t26*t23;
  t20 = t29*t29;
  t23 = t20*t20;
  t34 = t29*t20;
  const TreeValue c2_1234_2 = t42*t11*t3*t20*t26*t31;
  const TreeValue c2_1234_3 = -T(2.)/T(3.)*t24*t13*t5*t34*t25*t33;
  const TreeValue c2_tree = -T(11.)/T(6.);
  t20 = T(2.)*x5;
  t24 = T(9.)*x4;
  t25 = t17*x4;
  t26 = T(3.)*x4;
  t31 = T(8.)*x5;
  t33 = -T(23.)*x5;
  t37 = T(12.)*x5;
  t19 = t19*x4;
  t38 = T(3.)*t10;
  t41 = -T(29.)*t22;
  t42 = T(45.)*x4;
  t43 = T(40.)*x5;
  t44 = T(15.)*x4;
  t45 = T(5.)*x5;
  t46 = x4*x4;
  t47 = t17*t10;
  t48 = T(4.)*x5;
  t49 = T(45.)*t9*t23;
  t52 = T(15.)*x2*t7*t34;
  t53 = T(207.)*x4;
  t13 = t13*t10*(t19*(t17*((-T(26.) + t31)*x5 + (t41 + (T(6.) + t24)*x4)*x4 + T(18.))
    + (x2*(x5*(T(48.)*x5 - T(27.)) + (T(145.) - T(127.)*x5 + (-T(84.) + T(72.)*x4)*x4)*x4 - T(21.))
    + ((T(2.) + T(80.)*x5)*x5 + (T(474.) - T(186.)*x5 + (-T(441.) + t53)*x4)*x4 - T(136.))*x3)*x3) + t49
    + t52*(-T(5.) + T(12.)*x4) + t47*(T(30.) + (-T(336.) + T(8.)*(T(1.) + t45)*x5 + (T(853.) - T(88.)*x5
    + (-T(786.) + T(279.)*x4)*x4)*x4)*x4));
  t12 = t12*t6*(t18*(t17*(t20*t22 + (T(19.) - T(22.)*x5 + (T(21.) + t26)*x4)*x4 - T(3.))
    + (x2*(x5*(T(24.)*x5 - T(16.)) + (T(80.) - T(98.)*x5 + (-T(12.) + t42)*x4)*x4 - T(8.))
    + (x5*(T(72.)*x5 - T(24.)) + (T(416.) - T(182.)*x5 + (-T(411.) + t53)*x4)*x4 - T(84.))*x3)*x3)*x4
    + T(60.)*t8*t23 + T(30.)*x2*t9*t34*(-T(5.) + t24) + t47*(x2*(T(30.) + (-T(410.) + T(2.)*(-T(9.)
    + t43)*x5 + (T(1217.) - T(158.)*x5 + (-T(1182.) + T(441.)*x4)*x4)*x4)*x4) + t51*(T(60.) + (-T(407.)
    + (-T(4.) + T(15.)*x5)*x5 + (T(881.) - T(26.)*x5 + (-T(762.) + T(243.)*x4)*x4)*x4)*x4)));
  t3 = t16*x3*t3*(t21*t46*(T(2.) + t44 - t45) + t19*x3*(x3*((-T(14.) + T(16.)*x5)*x5
    + (T(171.) - T(27.)*x5 + (-T(198.) + T(63.)*x4)*x4)*x4 - T(11.)) + x2*((T(7.) + t48)*x5
    + (-T(4.) - T(17.)*x5 + (-T(12.) + t24)*x4)*x4 - T(11.))) + t49 + t52*(-T(4.) + t24) + t38*t17*(T(5.)
    + (-T(67.) + (-T(3.) + t48)*x5 + (T(170.) - T(5.)*x5 + (-T(152.) + T(48.)*x4)*x4)*x4)*x4));
  t5 = t15*t5*(T(18.)*t9*t23 + T(3.)*t21*x4*t46 + t36*t7*t34*(-T(7.) + t44)
    + t18*x3*t46*(-T(8.) - t44 + t20) + t19*t6*((-T(7.) + t20)*x5 + (T(66.) - T(3.)*x5 + (-T(72.)
    + t24)*x4)*x4 + T(5.)) + t47*(T(3.) + (-T(75.) + (T(1.) + t20)*x5 + (T(176.) - t45 + (-T(138.)
    + T(36.)*x4)*x4)*x4)*x4));
  t15 = T(1.)/t35;
  t16 = -T(1.)/T(9.);
  const TreeValue rat = t16*(-t14*t4*t32*(-T(3.)*t6*t2*t34 + t25*(T(6.)*t1 + x3*(-T(11.) + t24
    + t20))) + (t5 + (t3 + (t12 + (t13 + (t11*t7*(t25*(t17*((-T(3.) + t37)*x5 + (t41 + (-T(6.)
    + t24)*x4)*x4 - T(9.)) + (x2*((-T(2.) + t43)*x5 + (T(123.) - T(96.)*x5 + (-T(72.)
    + t42)*x4)*x4 - T(38.)) + (T(3.)*(T(3.) + T(10.)*x5)*x5 + T(3.)*(T(74.) + t33 + (-T(66.)
    + T(27.)*x4)*x4)*x4 - T(75.))*x3)*x3) + T(18.)*t7*t23 + t36*t10*t34*(-T(5.) + T(21.)*x4))
    + (x1*t9*(t38*t23 + T(9.)*t1*t6*t34 + t19*(x4*(t26*t29 - T(19.)*t22) + (-T(2.) + t31)*x5 - T(6.))
    + t50*x4*((-T(1.) + t37)*x5 + (T(32.) + t33 + (-T(18.) + t24)*x4)*x4 - T(11.)))
    - T(2.)*t19*t8*t30*t22)*x2)*x2)*x2)*x2)*x2)*x2)/(x2*x2)*t39*t28*t27/x4*t15*t40;

  EpsTriplet<T> amp, camp;
  amp += c1234*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  amp += c245*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
//  amp += c2_2315_0*(njetan->L0(s23,s15));
  amp += c2_2315_1*(njetan->L1(s23,s15));
  amp += c2_2315_2*(njetan->L2hat(s23,s15));
  amp += c2_2315_3*(njetan->L3hat(s23,s15));
  amp += c2_1534_0*(njetan->L0(s15,s34));
  amp += c2_1534_1*(njetan->L1(s15,s34));
  amp += c2_1534_2*(njetan->L2hat(s15,s34));
  amp += c2_1534_3*(njetan->L3hat(s15,s34));
//  amp += c2_1234_0*(njetan->L0(s12,s34));
  amp += c2_1234_1*(njetan->L1(s12,s34));
  amp += c2_1234_2*(njetan->L2hat(s12,s34));
  amp += c2_1234_3*(njetan->L3hat(s12,s34));
  amp += c2_tree*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  amp += rat;

  camp += conj(c1234)*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  camp += conj(c245)*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
//  camp += conj(c2_2315_0)*(njetan->L0(s23,s15));
  camp += conj(c2_2315_1)*(njetan->L1(s23,s15));
  camp += conj(c2_2315_2)*(njetan->L2hat(s23,s15));
  camp += conj(c2_2315_3)*(njetan->L3hat(s23,s15));
  camp += conj(c2_1534_0)*(njetan->L0(s15,s34));
  camp += conj(c2_1534_1)*(njetan->L1(s15,s34));
  camp += conj(c2_1534_2)*(njetan->L2hat(s15,s34));
  camp += conj(c2_1534_3)*(njetan->L3hat(s15,s34));
//  camp += conj(c2_1234_0)*(njetan->L0(s12,s34));
  camp += conj(c2_1234_1)*(njetan->L1(s12,s34));
  camp += conj(c2_1234_2)*(njetan->L2hat(s12,s34));
  camp += conj(c2_1234_3)*(njetan->L3hat(s12,s34));
  camp += conj(c2_tree)*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAg27(const int* p) /* 11-111 */
{
  const int nord[] = {p[2],p[3],p[4],p[0],p[1]};
  return hAg30(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAg28(const int* p) /* -1-1111 */
{
  const std::complex<T> phase = i_*hA028(p);
  setxi(p);
  setsij(p);

  const TreeValue t1 = x2*x4;
  const TreeValue c1234 = -t1*x1*T(0.5);
  TreeValue t2 = -T(1.) + x5;
  const TreeValue c1235 = x1*x3*t2*T(0.5);
  const TreeValue t3 = x4 - x5;
  const TreeValue c1245 = x2*x3*t3*t2*T(0.5);
  const TreeValue t4 = -T(1.) + x4;
  const TreeValue t5 = x3*t4;
  const TreeValue t6 = t5 + t1;
  TreeValue t7 = x1*t6*T(0.5) + x2*x3*t3*T(0.5);
  const TreeValue c1345 = -t7*t3;
  const TreeValue c2345 = -t7*x4;
  t7 = T(2.)*x2;
  TreeValue t8 = x1 + t7;
  const TreeValue t9 = x1*x1;
  TreeValue t10 = x2*x2;
  const TreeValue t11 = x2*t10;
  TreeValue t12 = T(1.)/x1;
  TreeValue t13 = t12*t12;
  t12 = t12*t13;
  const TreeValue c2_2315_0 = t8*(T(22.)*t9 + T(4.)*x1*x2 + T(4.)*t10)*t12/T(12.);
  t13 = x1 + x2;
  t2 = T(1.)/t2;
  const TreeValue t14 = T(1.)/x3;
  const TreeValue c2_2315_1 = -t13*(T(4.)*t9 + t7*x1 + T(2.)*t10)*t6*t12*t14*t2;
  t7 = t2*t2;
  TreeValue t15 = t14*t14;
  TreeValue t16 = t13*t13;
  TreeValue t17 = t6*t6;
  const TreeValue c2_2315_2 = t16*t8*t17*t12*t15*t7;
  const TreeValue c2_2315_3 = -T(2.)/T(3.)*t13*t16*t6*t17*t12*t14*t15*t2*t7;
  const TreeValue c2_tree = -T(11.)/T(6.);
  t7 = T(3.)*x4;
  t8 = t7*t10;
  t13 = T(2.)*x5;
  t15 = x3*x3;
  t16 = t4*t4;
  t10 = t10*x4;
  t17 = T(1.) + x5;
  const TreeValue t18 = t17*x5;
  const TreeValue t19 = t3*t3;
  const TreeValue t20 = x4*x4;
  const TreeValue t21 = T(1.) + t18;
  const TreeValue t22 = T(1.)/T(9.);
  const TreeValue rat = t22*(x1*t9*(-T(3.)*t15*(x2 + x3)*t4*t16 + t10*(T(6.)*t1 + x3*(-T(11.)
    + T(9.)*x4 + t13))) + (-T(9.)*t9*t6*(-t8 + (x2*(T(1.) - T(2.)*x4 + x5) + t5*t3)*x3)
    + (T(9.)*x1*(T(4.)*t11*t20 + t10*x3*(-T(5.) + T(7.)*x4 - t13) - t15*(x3*t19 + x2*((-T(2.)*t17
    + x4)*x4 + t18 + T(1.)))*t4) + (T(15.)*t11*t20 + T(3.)*(t8*(-T(2.) + t7 - x5)
    + (x2*(x4*(T(3.)*x4*t17 - T(3.)*t21 - t20) + t21*x5 + T(1.)) - t3*t19*x3)*x3)*x3)*x2)*x2)*x2)*t12/(x2*x2)*t14/x4*t2;

  EpsTriplet<T> amp, camp;
  amp += c1234*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c2_2315_0*(njetan->L0(s23,s15));
  amp += c2_2315_1*(njetan->L1(s23,s15));
  amp += c2_2315_2*(njetan->L2hat(s23,s15));
  amp += c2_2315_3*(njetan->L3hat(s23,s15));
  amp += c2_tree*(njetan->I2(s23, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  amp += rat;

  camp += conj(c1234)*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c2_2315_0)*(njetan->L0(s23,s15));
  camp += conj(c2_2315_1)*(njetan->L1(s23,s15));
  camp += conj(c2_2315_2)*(njetan->L2hat(s23,s15));
  camp += conj(c2_2315_3)*(njetan->L3hat(s23,s15));
  camp += conj(c2_tree)*(njetan->I2(s23, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAg29(const int* p) /* 1-1111 */
{
  const int nord[] = {p[1],p[2],p[3],p[4],p[0]};
  return hAg30(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAg30(const int* p) /* -11111 */
{
  const std::complex<T> phase =
    i_*sB(p[1],p[4])*sB(p[1],p[4])/(sB(p[0],p[1])*sA(p[1],p[2])*sA(p[2],p[3])*sA(p[3],p[4])*sB(p[4],p[0]));
  setxi(p);
  setsij(p);

  const TreeValue XX1 = T(1.)/( - T(1.) + x4);
  const TreeValue XX2 = pow(x1,-1);
  const TreeValue XX3 = pow(x3,-1);
  TreeValue XX4 = x4 - x5;
  TreeValue XX5 = x5 - T(1.);
  XX4 = XX2*XX4*XX5;
  const TreeValue XX6 = x3*XX4;
  XX5 = XX5*x4*XX3;
  XX4 = - XX5 - XX4;
  XX4 = x2*XX4;
  XX5 = x1*XX5;
  XX4 = - XX4 + XX5 + XX6;
  XX5 = - T(1.) + T(1.)/T(3.)*x4;
  XX5 = x4*XX5;
  XX5 = T(1.) + XX5;
  XX5 = x4*XX5;
  XX4 = - T(1.)/T(3.) + XX5 - T(1.)/T(3.)*XX4;
  XX4 = x2*XX4*XX1*XX1;

  const std::complex<T> rat = XX4;
  EpsTriplet<T> amp;
  amp += rat;
  const LoopResult<T> res = {phase*amp, conj(phase*amp)};
  return res;
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAg31(const int* p) /* 11111 */
{
  const std::complex<T> phase = i_/(sA(p[0],p[1])*sA(p[1],p[2])*sA(p[2],p[3])*sA(p[3],p[4])*sA(p[4],p[0]));
  setxi(p);
  setsij(p);

  TreeValue XX1 = x4*x4;
  TreeValue XX2 = - T(2.)*x4 + x5;
  XX2 = x5*XX2;
  XX2 = XX1 + XX2;
  XX2 = x3*XX2;
  XX1 = x1*XX1;
  XX1 = XX1 + XX2;
  XX1 = x2*XX1;
  XX2 = - T(2.) + x4;
  XX2 = x4*XX2;
  XX2 = T(1.) + XX2;
  XX2 = x3*x1*XX2;
  XX1 = XX2 + XX1;
  XX1 = T(1.)/T(3.)*XX1;

  const std::complex<T> rat = XX1;

  EpsTriplet<T> amp;
  amp += rat;
  const LoopResult<T> res = {phase*amp, conj(phase*amp)};
  return res;
}

//// fermion loops ////

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAf0(const int* p) /* -1-1-1-1-1 */
{
  return hAg0(p);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAf1(const int* p) /* 1-1-1-1-1 */
{
  return hAg1(p);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAf2(const int* p) /* -11-1-1-1 */
{
  const int nord[] = {p[1],p[2],p[3],p[4],p[0]};
  return hAf1(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAf3(const int* p) /* 11-1-1-1 */
{
  const LoopResult<T> camp = -hAf28(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAf4(const int* p) /* -1-11-1-1 */
{
  const int nord[] = {p[2],p[3],p[4],p[0],p[1]};
  return hAf1(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAf5(const int* p) /* 1-11-1-1 */
{
  const LoopResult<T> camp = -hAf26(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAf6(const int* p) /* -111-1-1 */
{
  const int nord[] = {p[1],p[2],p[3],p[4],p[0]};
  return hAf3(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAf7(const int* p) /* 111-1-1 */
{
  const int nord[] = {p[3],p[4],p[0],p[1],p[2]};
  return hAf28(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAf8(const int* p) /* -1-1-11-1 */
{
  const int nord[] = {p[3],p[4],p[0],p[1],p[2]};
  return hAf1(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAf9(const int* p) /* 1-1-11-1 */
{
  const int nord[] = {p[3],p[4],p[0],p[1],p[2]};
  return hAf5(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAf10(const int* p) /* -11-11-1 */
{
  const int nord[] = {p[1],p[2],p[3],p[4],p[0]};
  return hAf5(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAf11(const int* p) /* 11-11-1 */
{
  const int nord[] = {p[2],p[3],p[4],p[0],p[1]};
  return hAf26(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAf12(const int* p) /* -1-111-1 */
{
  const int nord[] = {p[2],p[3],p[4],p[0],p[1]};
  return hAf3(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAf13(const int* p) /* 1-111-1 */
{
  const int nord[] = {p[4],p[0],p[1],p[2],p[3]};
  return hAf26(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAf14(const int* p) /* -1111-1 */
{
  const int nord[] = {p[4],p[0],p[1],p[2],p[3]};
  return hAf28(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAf15(const int* p) /* 1111-1 */
{
  const int nord[] = {p[4],p[0],p[1],p[2],p[3]};
  return hAf30(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAf16(const int* p) /* -1-1-1-11 */
{
  const int nord[] = {p[4],p[0],p[1],p[2],p[3]};
  return hAf1(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAf17(const int* p) /* 1-1-1-11 */
{
  const int nord[] = {p[4],p[0],p[1],p[2],p[3]};
  return hAf3(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAf18(const int* p) /* -11-1-11 */
{
  const int nord[] = {p[4],p[0],p[1],p[2],p[3]};
  return hAf5(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAf19(const int* p) /* 11-1-11 */
{
  const int nord[] = {p[2],p[3],p[4],p[0],p[1]};
  return hAf28(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAf20(const int* p) /* -1-11-11 */
{
  const int nord[] = {p[2],p[3],p[4],p[0],p[1]};
  return hAf5(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAf21(const int* p) /* 1-11-11 */
{
  const int nord[] = {p[1],p[2],p[3],p[4],p[0]};
  return hAf26(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAf22(const int* p) /* -111-11 */
{
  const int nord[] = {p[3],p[4],p[0],p[1],p[2]};
  return hAf26(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAf23(const int* p) /* 111-11 */
{
  const int nord[] = {p[3],p[4],p[0],p[1],p[2]};
  return hAf30(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAf24(const int* p) /* -1-1-111 */
{
  const int nord[] = {p[3],p[4],p[0],p[1],p[2]};
  return hAf3(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAf25(const int* p) /* 1-1-111 */
{
  const int nord[] = {p[1],p[2],p[3],p[4],p[0]};
  return hAf28(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAf26(const int* p) /* -11-111 */
{
  const std::complex<T> phase = i_*hA026(p);
  setxi(p);
  setsij(p);

  const TreeValue t1 = x2 + x3;
  TreeValue t2 = t1*t1;
  TreeValue t3 = t1*t2;
  const TreeValue t4 = x3*x3;
  const TreeValue t5 = t4*t4;
  const TreeValue t6 = t4*t5;
  const TreeValue t7 = x3*t5;
  TreeValue t8 = x3*t4;
  const TreeValue t9 = x1*x1;
  TreeValue t10 = t9*t9;
  TreeValue t11 = x1*t9;
  TreeValue t12 = t9*t10;
  const TreeValue t13 = x1*t10;
  const TreeValue t14 = x2*x2;
  const TreeValue t15 = t14*t14;
  TreeValue t16 = x2*t14;
  TreeValue t17 = t14*t15;
  const TreeValue t18 = x2*t15;
  TreeValue t19 = t14*t4;
  TreeValue t20 = t19 + t9*t2;
  const TreeValue t21 = -T(1.) + x5;
  TreeValue t22 = x2*x3;
  TreeValue t23 = x1*t1;
  TreeValue t24 = t22 + t23;
  t24 = T(1.)/t24;
  TreeValue t25 = t24*t24;
  const TreeValue t26 = t25*t25;
  TreeValue t27 = t9*t1;
  TreeValue t28 = t27*x2*T(0.5);
  const TreeValue c1235 = -t28*t4*t20*t21*t26;
  TreeValue t29 = t9 + t14;
  const TreeValue t30 = -T(1.) + x4;
  const TreeValue t31 = x2*x4;
  const TreeValue t32 = x3*t30 + t31;
  const TreeValue t33 = x4 - x5;
  TreeValue t34 = x1*t32;
  TreeValue t35 = t22*t33;
  const TreeValue t36 = t34 + t35;
  TreeValue t37 = x1 + x2;
  TreeValue t38 = T(1.)/t37;
  const TreeValue t39 = t38*t38;
  TreeValue t40 = t39*t39;
  TreeValue t41 = t38*t39;
  const TreeValue c2345 = t31*x1*t29*t36*t40*T(0.5);
  const TreeValue c123 = -t28*x3*t20*t26;
//  c124 = T(0.);
  t28 = t23*T(0.5);
  const TreeValue c125 = t28*x2*t4*t20*t21*t26;
//  c134 = T(0.);
  TreeValue t42 = t23*t30;
  const TreeValue c135 = t28*x3*t20*(t42 + t35)*t26;
//  c145 = T(0.);
  t28 = x1*t14;
  const TreeValue c234 = -t28*t29*x4*t40*T(0.5);
  t27 = -t14*x3 + t27;
  t35 = T(2.)*t22;
  TreeValue t43 = x1*T(0.5);
  TreeValue t44 = t43*x2;
  const TreeValue c235 = -t44*t27*(t10*t2 + (t9*(t14 - t35 - T(2.)*t4) + t19)*t14)*(t22*t30 + t34)*t40*t26;
  const TreeValue c245 = -t44*t29*(t31 + x3*t21)*t40;
  const TreeValue c345 = -t43*t29*t36*t40;
//  c2_2315_0 = T(0.);
  t19 = T(1.)/x3;
  t40 = T(1.)/t21;
  const TreeValue c2_2315_1 = -t34*t29*t41*t19*t40;
  t29 = x3 - t1*x4;
  t34 = t40*t40;
  t43 = t19*t19;
  const TreeValue c2_2315_2 = t9*t29*t29*t39*t43*t34;
  t29 = t32*t32;
  const TreeValue c2_2315_3 = -T(2.)/T(3.)*x1*t32*t29*t38*t19*t43*t40*t34;
  t29 = T(2.)*x3;
  t34 = T(3.)*x2;
  t43 = T(2.)*x2;
  t44 = T(3.)*t9;
  const TreeValue t45 = t11*t10*t2*t2;
  TreeValue t46 = T(1.)/T(3.);
  const TreeValue c2_1534_0 = t46*(-x1*t17*(x2 - t29)*t8 + t16*t15*t5 + t43*t12*(x2 - x3)*t3 - t45
    + t44*t17*t4*(t34 + t29) - t13*t16*t2*(t43 + T(5.)*x3) + t11*t15*x3*(t16 + (T(9.)*t14
    + (T(6.)*x2 - x3)*x3)*x3) + t10*t16*t1*(t16 + (-T(11.)*t14 + (-T(7.)*x2
    + x3)*x3)*x3))*t41*t26;
  t43 = t9 - t14;
  t46 = t9*t14;
  t23 = (-x1 + x2*t33)*x3 + t23*x4;
  t23 = T(1.)/t23;
  const TreeValue c2_1534_1 = t9*t15*x3*(t46*(t44 + (T(2.)*x1 + x2)*x2)
    + t35*x1*(x1 - x2)*t37*(T(3.)*x1 + x2) + T(3.)*t43*t43*t4)*t41*t26*t23;
  t35 = t23*t23;
  t37 = t23*t35;
  const TreeValue c2_1534_2 = t11*t17*t4*(T(2.)*t27 + t28)*t39*t26*t35;
  const TreeValue c2_1534_3 = T(2.)/T(3.)*t10*t15*t15*t8*t38*t26*t37;
//  c2_1234_0 = T(0.);
  const TreeValue c2_1234_1 = -t42*t22*t20*t24*t25*t23;
  t17 = t30*t30;
  t20 = t17*t17;
  t23 = t30*t17;
  const TreeValue c2_1234_2 = t46*t4*t2*t17*t25*t35;
  const TreeValue c2_1234_3 = -T(2.)/T(3.)*t22*t11*t3*t23*t24*t37;
  const TreeValue c2_tree = -T(1.)/T(3.);
  t17 = T(9.)*x4;
  t22 = T(2.)*x5;
  t24 = t14*x4;
  t25 = T(3.)*x4;
  t27 = T(8.)*x5;
  t28 = -T(23.)*x5;
  t35 = T(12.)*x5;
  t37 = T(3.)*t8;
  t16 = t16*x4;
  t38 = -T(29.)*t21;
  t41 = T(45.)*x4;
  t42 = T(40.)*x5;
  t43 = T(15.)*x4;
  t44 = T(5.)*x5;
  t46 = x4*x4;
  const TreeValue t47 = t14*t8;
  const TreeValue t48 = T(4.)*x5;
  const TreeValue t49 = T(45.)*t7*t20;
  const TreeValue t50 = T(15.)*x2*t5*t23;
  const TreeValue t51 = T(207.)*x4;
  t8 = t11*t8*(t16*(t14*((-T(26.) + t27)*x5 + (t38 + (T(6.) + t17)*x4)*x4 + T(18.))
    + (x2*(x5*(T(48.)*x5 - T(27.)) + (T(145.) - T(127.)*x5 + (-T(84.) + T(72.)*x4)*x4)*x4 - T(21.))
    + ((T(2.) + T(80.)*x5)*x5 + (T(474.) - T(186.)*x5 + (-T(441.) + t51)*x4)*x4 - T(136.))*x3)*x3) + t49
    + t50*(-T(5.) + T(12.)*x4) + t47*(T(30.) + (-T(336.) + T(8.)*(T(1.) + t44)*x5 + (T(853.) - T(88.)*x5
    + (-T(786.) + T(279.)*x4)*x4)*x4)*x4));
  t10 = t10*t4*(t15*(t14*(t22*t21 + (T(19.) - T(22.)*x5 + (T(21.) + t25)*x4)*x4 - T(3.))
    + (x2*(x5*(T(24.)*x5 - T(16.)) + (T(80.) - T(98.)*x5 + (-T(12.) + t41)*x4)*x4 - T(8.))
    + (x5*(T(72.)*x5 - T(24.)) + (T(416.) - T(182.)*x5 + (-T(411.) + t51)*x4)*x4 - T(84.))*x3)*x3)*x4
    + T(60.)*t6*t20 + T(30.)*x2*t7*t23*(-T(5.) + t17) + t47*(x2*(T(30.) + (-T(410.) + T(2.)*(-T(9.)
    + t42)*x5 + (T(1217.) - T(158.)*x5 + (-T(1182.) + T(441.)*x4)*x4)*x4)*x4) + t29*(T(60.) + (-T(407.)
    + (-T(4.) + T(15.)*x5)*x5 + (T(881.) - T(26.)*x5 + (-T(762.) + T(243.)*x4)*x4)*x4)*x4)));
  t2 = t13*x3*t2*(t18*t46*(T(2.) + t43 - t44) + t16*x3*(x3*((-T(14.) + T(16.)*x5)*x5
    + (T(171.) - T(27.)*x5 + (-T(198.) + T(63.)*x4)*x4)*x4 - T(11.)) + x2*((T(7.) + t48)*x5
    + (-T(4.) - T(17.)*x5 + (-T(12.) + t17)*x4)*x4 - T(11.))) + t49 + t50*(-T(4.) + t17) + t37*t14*(T(5.)
    + (-T(67.) + (-T(3.) + t48)*x5 + (T(170.) - T(5.)*x5 + (-T(152.) + T(48.)*x4)*x4)*x4)*x4));
  t3 = t12*t3*(T(18.)*t7*t20 + T(3.)*t18*x4*t46 + t34*t5*t23*(-T(7.) + t43)
    + t15*x3*t46*(-T(8.) - t43 + t22) + t16*t4*((-T(7.) + t22)*x5 + (T(66.) - T(3.)*x5 + (-T(72.)
    + t17)*x4)*x4 + T(5.)) + t47*(T(3.) + (-T(75.) + (T(1.) + t22)*x5 + (T(176.) - t44 + (-T(138.)
    + T(36.)*x4)*x4)*x4)*x4));
  t11 = T(1.)/t36;
  t12 = -T(1.)/T(9.);
  const TreeValue rat = t12*(-t45*t32*(-T(3.)*t4*t1*t23 + t24*(T(6.)*t31 + x3*(-T(11.) + t17 + t22)))
    + (t3 + (t2 + (t10 + (t8 + (t9*t5*(t24*(t14*((-T(3.) + t35)*x5 + (t38 + (-T(6.)
    + t17)*x4)*x4 - T(9.)) + (x2*((-T(2.) + t42)*x5 + (T(123.) - T(96.)*x5 + (-T(72.)
    + t41)*x4)*x4 - T(38.)) + (T(3.)*(T(3.) + T(10.)*x5)*x5 + T(3.)*(T(74.) + t28 + (-T(66.)
    + T(27.)*x4)*x4)*x4 - T(75.))*x3)*x3) + T(18.)*t5*t20 + t37*x2*t23*(-T(5.) + T(21.)*x4))
    + (x1*t7*(t37*t20 + T(9.)*t31*t4*t23 + t16*(x4*(t25*t30 - T(19.)*t21) + (-T(2.) + t27)*x5 - T(6.))
    + t24*x3*((-T(1.) + t35)*x5 + (T(32.) + t28 + (-T(18.) + t17)*x4)*x4 - T(11.)))
    - T(2.)*t16*t6*t33*t21)*x2)*x2)*x2)*x2)*x2)*x2)/(x2*x2)*t39*t19*t26/x4*t11*t40;

  EpsTriplet<T> amp, camp;
  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  amp += c245*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
//  amp += c2_2315_0*(njetan->L0(s23,s15));
  amp += c2_2315_1*(njetan->L1(s23,s15));
  amp += c2_2315_2*(njetan->L2hat(s23,s15));
  amp += c2_2315_3*(njetan->L3hat(s23,s15));
  amp += c2_1534_0*(njetan->L0(s15,s34));
  amp += c2_1534_1*(njetan->L1(s15,s34));
  amp += c2_1534_2*(njetan->L2hat(s15,s34));
  amp += c2_1534_3*(njetan->L3hat(s15,s34));
//  amp += c2_1234_0*(njetan->L0(s12,s34));
  amp += c2_1234_1*(njetan->L1(s12,s34));
  amp += c2_1234_2*(njetan->L2hat(s12,s34));
  amp += c2_1234_3*(njetan->L3hat(s12,s34));
  amp += c2_tree*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  amp += rat;

  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  camp += conj(c245)*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
//  camp += conj(c2_2315_0)*(njetan->L0(s23,s15));
  camp += conj(c2_2315_1)*(njetan->L1(s23,s15));
  camp += conj(c2_2315_2)*(njetan->L2hat(s23,s15));
  camp += conj(c2_2315_3)*(njetan->L3hat(s23,s15));
  camp += conj(c2_1534_0)*(njetan->L0(s15,s34));
  camp += conj(c2_1534_1)*(njetan->L1(s15,s34));
  camp += conj(c2_1534_2)*(njetan->L2hat(s15,s34));
  camp += conj(c2_1534_3)*(njetan->L3hat(s15,s34));
//  camp += conj(c2_1234_0)*(njetan->L0(s12,s34));
  camp += conj(c2_1234_1)*(njetan->L1(s12,s34));
  camp += conj(c2_1234_2)*(njetan->L2hat(s12,s34));
  camp += conj(c2_1234_3)*(njetan->L3hat(s12,s34));
  camp += conj(c2_tree)*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAf27(const int* p) /* 11-111 */
{
  const int nord[] = {p[2],p[3],p[4],p[0],p[1]};
  return hAf30(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAf28(const int* p) /* -1-1111 */
{
  const std::complex<T> phase = i_*hA028(p);
  setxi(p);
  setsij(p);

  TreeValue t1 = T(2.)*x2;
  TreeValue t2 = x1 + t1;
  TreeValue t3 = x1 + x2;
  const TreeValue t4 = x1*x1;
  TreeValue t5 = T(1.)/x1;
  TreeValue t6 = t5*t5;
  t5 = t5*t6;
  const TreeValue c2_2315_0 = t2*(T(4.)*t4 + T(4.)*t3*x2)*t5/T(12.);
  t6 = x2*x2;
  const TreeValue t7 = x2*t6;
  const TreeValue t8 = -T(1.) + x4;
  const TreeValue t9 = x3*t8;
  const TreeValue t10 = x2*x4;
  const TreeValue t11 = t9 + t10;
  TreeValue t12 = -T(1.) + x5;
  t12 = T(1.)/t12;
  const TreeValue t13 = T(1.)/x3;
  const TreeValue c2_2315_1 = -t3*(t4 + t1*x1 + T(2.)*t6)*t11*t5*t13*t12;
  t1 = t13*t13;
  TreeValue t14 = t11*t11;
  TreeValue t15 = t12*t12;
  TreeValue t16 = t3*t3;
  const TreeValue c2_2315_2 = t16*t2*t14*t5*t1*t15;
  const TreeValue c2_2315_3 = -T(2.)/T(3.)*t3*t16*t11*t14*t5*t13*t1*t12*t15;
  const TreeValue c2_tree = -T(1.)/T(3.);
  t1 = x4 - x5;
  t2 = T(3.)*x4;
  t3 = t2*t6;
  t14 = T(2.)*x5;
  t15 = x3*x3;
  t16 = t8*t8;
  t6 = t6*x4;
  const TreeValue t17 = T(1.) + x5;
  const TreeValue t18 = t17*x5;
  const TreeValue t19 = x4*x4;
  const TreeValue t20 = t1*t1;
  const TreeValue t21 = T(1.) + t18;
  const TreeValue t22 = T(1.)/T(9.);
  const TreeValue rat = t22*(x1*t4*(-T(3.)*t15*(x2 + x3)*t8*t16 + t6*(T(6.)*t10 + x3*(-T(11.)
    + T(9.)*x4 + t14))) + (-T(9.)*t4*t11*(-t3 + (x2*(T(1.) - T(2.)*x4 + x5) + t9*t1)*x3)
    + (T(9.)*x1*(T(4.)*t7*t19 + t6*x3*(-T(5.) + T(7.)*x4 - t14) - t15*(x3*t20 + x2*((-T(2.)*t17 + x4)*x4
    + t18 + T(1.)))*t8) + (T(15.)*t7*t19 + T(3.)*(t3*(-T(2.) + t2 - x5) + (x2*(x4*(T(3.)*x4*t17 - T(3.)*t21 - t19)
    + t21*x5 + T(1.)) - t1*t20*x3)*x3)*x3)*x2)*x2)*x2)*t5/(x2*x2)*t13/x4*t12;

  EpsTriplet<T> amp, camp;
  amp += c2_2315_0*(njetan->L0(s23,s15));
  amp += c2_2315_1*(njetan->L1(s23,s15));
  amp += c2_2315_2*(njetan->L2hat(s23,s15));
  amp += c2_2315_3*(njetan->L3hat(s23,s15));
  amp += c2_tree*(njetan->I2(s23, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  amp += rat;

  camp += conj(c2_2315_0)*(njetan->L0(s23,s15));
  camp += conj(c2_2315_1)*(njetan->L1(s23,s15));
  camp += conj(c2_2315_2)*(njetan->L2hat(s23,s15));
  camp += conj(c2_2315_3)*(njetan->L3hat(s23,s15));
  camp += conj(c2_tree)*(njetan->I2(s23, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAf29(const int* p) /* 1-1111 */
{
  const int nord[] = {p[1],p[2],p[3],p[4],p[0]};
  return hAf30(nord);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAf30(const int* p) /* -11111 */
{
  return hAg30(p);
}

template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAf31(const int* p) /* 11111 */
{
  return hAg31(p);
}

#ifdef USE_SD
  template class Amp0q5g_a<double>;
#endif
#ifdef USE_DD
  template class Amp0q5g_a<dd_real>;
#endif
#ifdef USE_QD
  template class Amp0q5g_a<qd_real>;
#endif
#ifdef USE_VC
  template class Amp0q5g_a<Vc::double_v>;
#endif
