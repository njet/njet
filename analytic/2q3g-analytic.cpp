/*
* analytic/2q3g-analytic.cpp
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

// vim: set foldmethod=marker:

#include <cassert>
#include "2q3g-analytic.h"

template <typename T>
Amp2q3g_a<T>::Amp2q3g_a(const T scalefactor, const int mFC,
                        const NJetAmpTables& tables)
  : BaseClass(scalefactor, mFC, tables), hA0()
{
  njetan = new NJetAnalytic<T>(scalefactor, BaseClass::legsMOM());
  assert(int(sizeof(hA0)/sizeof(hA0[0])) >= (1 << njetan->legsMOM()));
  assert(int(sizeof(hAL1)/sizeof(hAL1[0])) >= (1 << njetan->legsMOM()));
  assert(int(sizeof(hAL2)/sizeof(hAL2[0])) >= (1 << njetan->legsMOM()));
  assert(int(sizeof(hAL3)/sizeof(hAL3[0])) >= (1 << njetan->legsMOM()));
  assert(int(sizeof(hAL4)/sizeof(hAL4[0])) >= (1 << njetan->legsMOM()));
  assert(int(sizeof(hAf1)/sizeof(hAf1[0])) >= (1 << njetan->legsMOM()));
  assert(int(sizeof(hAf2)/sizeof(hAf2[0])) >= (1 << njetan->legsMOM()));
//   BaseClass::clearNG();

  hA0[5] = &Amp2q3g_a<T>::hA05;
  hA0[6] = &Amp2q3g_a<T>::hA06;
  hA0[9] = &Amp2q3g_a<T>::hA09;
  hA0[10] = &Amp2q3g_a<T>::hA010;
  hA0[13] = &Amp2q3g_a<T>::hA013;
  hA0[14] = &Amp2q3g_a<T>::hA014;
  hA0[17] = &Amp2q3g_a<T>::hA017;
  hA0[18] = &Amp2q3g_a<T>::hA018;
  hA0[21] = &Amp2q3g_a<T>::hA021;
  hA0[22] = &Amp2q3g_a<T>::hA022;
  hA0[25] = &Amp2q3g_a<T>::hA025;
  hA0[26] = &Amp2q3g_a<T>::hA026;

  hAL1[1] = &Amp2q3g_a<T>::hAL11;
  hAL1[2] = &Amp2q3g_a<T>::hAL12;
  hAL1[5] = &Amp2q3g_a<T>::hAL15;
  hAL1[6] = &Amp2q3g_a<T>::hAL16;
  hAL1[9] = &Amp2q3g_a<T>::hAL19;
  hAL1[10] = &Amp2q3g_a<T>::hAL110;
  hAL1[13] = &Amp2q3g_a<T>::hAL113;
  hAL1[14] = &Amp2q3g_a<T>::hAL114;
  hAL1[17] = &Amp2q3g_a<T>::hAL117;
  hAL1[18] = &Amp2q3g_a<T>::hAL118;
  hAL1[21] = &Amp2q3g_a<T>::hAL121;
  hAL1[22] = &Amp2q3g_a<T>::hAL122;
  hAL1[25] = &Amp2q3g_a<T>::hAL125;
  hAL1[26] = &Amp2q3g_a<T>::hAL126;
  hAL1[29] = &Amp2q3g_a<T>::hAL129;
  hAL1[30] = &Amp2q3g_a<T>::hAL130;

  hAL2[1] = &Amp2q3g_a<T>::hAL21;
  hAL2[3] = &Amp2q3g_a<T>::hAL23;
  hAL2[4] = &Amp2q3g_a<T>::hAL24;
  hAL2[6] = &Amp2q3g_a<T>::hAL26;
  hAL2[9] = &Amp2q3g_a<T>::hAL29;
  hAL2[11] = &Amp2q3g_a<T>::hAL211;
  hAL2[12] = &Amp2q3g_a<T>::hAL212;
  hAL2[14] = &Amp2q3g_a<T>::hAL214;
  hAL2[17] = &Amp2q3g_a<T>::hAL217;
  hAL2[19] = &Amp2q3g_a<T>::hAL219;
  hAL2[20] = &Amp2q3g_a<T>::hAL220;
  hAL2[22] = &Amp2q3g_a<T>::hAL222;
  hAL2[25] = &Amp2q3g_a<T>::hAL225;
  hAL2[27] = &Amp2q3g_a<T>::hAL227;
  hAL2[28] = &Amp2q3g_a<T>::hAL228;
  hAL2[30] = &Amp2q3g_a<T>::hAL230;

  hAL3[1] = &Amp2q3g_a<T>::hAL31;
  hAL3[3] = &Amp2q3g_a<T>::hAL33;
  hAL3[5] = &Amp2q3g_a<T>::hAL35;
  hAL3[7] = &Amp2q3g_a<T>::hAL37;
  hAL3[8] = &Amp2q3g_a<T>::hAL38;
  hAL3[10] = &Amp2q3g_a<T>::hAL310;
  hAL3[12] = &Amp2q3g_a<T>::hAL312;
  hAL3[14] = &Amp2q3g_a<T>::hAL314;
  hAL3[17] = &Amp2q3g_a<T>::hAL317;
  hAL3[19] = &Amp2q3g_a<T>::hAL319;
  hAL3[21] = &Amp2q3g_a<T>::hAL321;
  hAL3[23] = &Amp2q3g_a<T>::hAL323;
  hAL3[24] = &Amp2q3g_a<T>::hAL324;
  hAL3[26] = &Amp2q3g_a<T>::hAL326;
  hAL3[28] = &Amp2q3g_a<T>::hAL328;
  hAL3[30] = &Amp2q3g_a<T>::hAL330;

  hAL4[1] = &Amp2q3g_a<T>::hAL41;
  hAL4[3] = &Amp2q3g_a<T>::hAL43;
  hAL4[5] = &Amp2q3g_a<T>::hAL45;
  hAL4[7] = &Amp2q3g_a<T>::hAL47;
  hAL4[9] = &Amp2q3g_a<T>::hAL49;
  hAL4[11] = &Amp2q3g_a<T>::hAL411;
  hAL4[13] = &Amp2q3g_a<T>::hAL413;
  hAL4[15] = &Amp2q3g_a<T>::hAL415;
  hAL4[16] = &Amp2q3g_a<T>::hAL416;
  hAL4[18] = &Amp2q3g_a<T>::hAL418;
  hAL4[20] = &Amp2q3g_a<T>::hAL420;
  hAL4[22] = &Amp2q3g_a<T>::hAL422;
  hAL4[24] = &Amp2q3g_a<T>::hAL424;
  hAL4[26] = &Amp2q3g_a<T>::hAL426;
  hAL4[28] = &Amp2q3g_a<T>::hAL428;
  hAL4[30] = &Amp2q3g_a<T>::hAL430;

  hAf1[1] = &Amp2q3g_a<T>::hAf11;
  hAf1[2] = &Amp2q3g_a<T>::hAf12;
  hAf1[5] = &Amp2q3g_a<T>::hAf15;
  hAf1[6] = &Amp2q3g_a<T>::hAf16;
  hAf1[9] = &Amp2q3g_a<T>::hAf19;
  hAf1[10] = &Amp2q3g_a<T>::hAf110;
  hAf1[13] = &Amp2q3g_a<T>::hAf113;
  hAf1[14] = &Amp2q3g_a<T>::hAf114;
  hAf1[17] = &Amp2q3g_a<T>::hAf117;
  hAf1[18] = &Amp2q3g_a<T>::hAf118;
  hAf1[21] = &Amp2q3g_a<T>::hAf121;
  hAf1[22] = &Amp2q3g_a<T>::hAf122;
  hAf1[25] = &Amp2q3g_a<T>::hAf125;
  hAf1[26] = &Amp2q3g_a<T>::hAf126;
  hAf1[29] = &Amp2q3g_a<T>::hAf129;
  hAf1[30] = &Amp2q3g_a<T>::hAf130;

  hAf2[1] = &Amp2q3g_a<T>::hAf21;
  hAf2[3] = &Amp2q3g_a<T>::hAf23;
  hAf2[4] = &Amp2q3g_a<T>::hAf24;
  hAf2[6] = &Amp2q3g_a<T>::hAf26;
  hAf2[9] = &Amp2q3g_a<T>::hAf29;
  hAf2[11] = &Amp2q3g_a<T>::hAf211;
  hAf2[12] = &Amp2q3g_a<T>::hAf212;
  hAf2[14] = &Amp2q3g_a<T>::hAf214;
  hAf2[17] = &Amp2q3g_a<T>::hAf217;
  hAf2[19] = &Amp2q3g_a<T>::hAf219;
  hAf2[20] = &Amp2q3g_a<T>::hAf220;
  hAf2[22] = &Amp2q3g_a<T>::hAf222;
  hAf2[25] = &Amp2q3g_a<T>::hAf225;
  hAf2[27] = &Amp2q3g_a<T>::hAf227;
  hAf2[28] = &Amp2q3g_a<T>::hAf228;
  hAf2[30] = &Amp2q3g_a<T>::hAf230;

}

template <typename T>
void Amp2q3g_a<T>::setsij(const int *p)
{
  s12 = lS(p[0], p[1]);
  s23 = lS(p[1], p[2]);
  s34 = lS(p[2], p[3]);
  s45 = lS(p[3], p[4]);
  s15 = lS(p[4], p[0]);
}

template <typename T>
void Amp2q3g_a<T>::setaij(const int *p)
{
  a12 = sA(p[0], p[1]);
  a13 = sA(p[0], p[2]);
  a14 = sA(p[0], p[3]);
  a15 = sA(p[0], p[4]);
  a23 = sA(p[1], p[2]);
  a24 = sA(p[1], p[3]);
  a25 = sA(p[1], p[4]);
  a34 = sA(p[2], p[3]);
  a35 = sA(p[2], p[4]);
  a45 = sA(p[3], p[4]);

  b12 = sB(p[0], p[1]);
  b13 = sB(p[0], p[2]);
  b14 = sB(p[0], p[3]);
  b15 = sB(p[0], p[4]);
  b23 = sB(p[1], p[2]);
  b24 = sB(p[1], p[3]);
  b25 = sB(p[1], p[4]);
  b34 = sB(p[2], p[3]);
  b35 = sB(p[2], p[4]);
  b45 = sB(p[3], p[4]);
}

template <typename T>
void Amp2q3g_a<T>::setxi(const int *p)
{
  x1 = lS(p[0], p[1]);
  x2 = -sB(p[0], p[1])*sA(p[1],p[2])*sA(p[0],p[3])/sA(p[2],p[3]);
  x3 = -sB(p[0], p[1])*sA(p[1],p[2])*sA(p[0],p[3])*sA(p[0],p[4])/(sA(p[0],p[2])*sA(p[3], p[4]));
  x4 = sA(p[2], p[3])*sB(p[1], p[2])/(sA(p[0], p[3])*sB(p[0], p[1]));
  x5 = -sA(p[2],p[3])*(lS(p[1],p[2]) - lS(p[3],p[4]))/sB(p[0],p[1])/(sA(p[0],p[3])*sA(p[1],p[2]));
}

template <typename T>
typename Amp2q3g_a<T>::TreeValue
Amp2q3g_a<T>::A0(int p0, int p1, int p2, int p3, int p4)
{
  const int ord[] = {p0, p1, p2, p3, p4};
  const int hel = HelicityOrder(mhelint, ord);
  HelAmp const hamp = hA0[hel];
  if (hamp) {
    return i_*callTree(this, hamp, ord);
  } else {
    return TreeValue(0.);
  }
}

template <typename T>
LoopResult<T> Amp2q3g_a<T>::AL(int p0, int p1, int p2, int p3, int p4)
{
  const int ord[] = {p0, p1, p2, p3, p4};
  const int hel = HelicityOrder(mhelint, ord);
  const int f0 = getFlav(0,p0);
  const int f1 = getFlav(0,p1);
  const int f2 = getFlav(0,p2);
  const int f3 = getFlav(0,p3);
  const int f4 = getFlav(0,p4);
  if (f0 == -1 && f1 == 1) {
    HelAmpLoop const hamp = hAL1[hel];
    if (hamp) {
      return callLoop(this, hamp, ord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -1 && f2 == 1) {
    HelAmpLoop const hamp = hAL2[hel];
    if (hamp) {
      return callLoop(this, hamp, ord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -1 && f3 == 1) {
    HelAmpLoop const hamp = hAL3[hel];
    if (hamp) {
      return callLoop(this, hamp, ord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -1 && f4 == 1) {
    HelAmpLoop const hamp = hAL4[hel];
    if (hamp) {
      return callLoop(this, hamp, ord);
    } else {
      return LoopResult<T>();
    }
  } else {
    return NJetAmp5<T>::AL(p0, p1, p2, p3, p4);
  }
}

template <typename T>
LoopResult<T> Amp2q3g_a<T>::AF(int p0, int p1, int p2, int p3, int p4)
{
  const int ord[] = {p0, p1, p2, p3, p4};
  const int hel = HelicityOrder(mhelint, ord);
  const int f0 = getFlav(0,p0);
  const int f1 = getFlav(0,p1);
  const int f2 = getFlav(0,p2);
  const int f3 = getFlav(0,p3);
  const int f4 = getFlav(0,p4);
  if (f0 == -1 && f1 == 1) {
    HelAmpLoop const hamp = hAf1[hel];
    if (hamp) {
      return callLoop(this, hamp, ord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -1 && f2 == 1) {
    HelAmpLoop const hamp = hAf2[hel];
    if (hamp) {
      return callLoop(this, hamp, ord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -1 && f3 == 1) {
    return LoopResult<T>();
  } else if (f0 == -1 && f4 == 1) {
    return LoopResult<T>();
  } else {
    return NJetAmp5<T>::AF(p0, p1, p2, p3, p4);
  }
}

//{{{ tree
template <typename T>
typename Amp2q3g_a<T>::TreeValue
Amp2q3g_a<T>::hA05(const int* p) /* 1-11-1-1 */
{
  return pow(sB(p[0], p[2]), 3)*sB(p[1], p[2])/CyclicSpinorsB(p);
}
template <typename T>
typename Amp2q3g_a<T>::TreeValue
Amp2q3g_a<T>::hA06(const int* p) /* -111-1-1 */
{
  const int nord[] = {p[1],p[0],p[4],p[3],p[2]};
  return  hA017(nord);
}
template <typename T>
typename Amp2q3g_a<T>::TreeValue
Amp2q3g_a<T>::hA09(const int* p) /* 1-1-11-1 */
{
  return pow(sB(p[0], p[3]), 3)*sB(p[1], p[3])/CyclicSpinorsB(p);
}
template <typename T>
typename Amp2q3g_a<T>::TreeValue
Amp2q3g_a<T>::hA010(const int* p) /* -11-11-1 */
{
  const int nord[] = {p[1],p[0],p[4],p[3],p[2]};
  return  hA09(nord);
}
template <typename T>
typename Amp2q3g_a<T>::TreeValue
Amp2q3g_a<T>::hA013(const int* p) /* 1-111-1 */
{
  const int nord[] = {p[1],p[0],p[4],p[3],p[2]};
  return  hA026(nord);
}
template <typename T>
typename Amp2q3g_a<T>::TreeValue
Amp2q3g_a<T>::hA014(const int* p) /* -1111-1 */
{
  return -pow(sA(p[0], p[4]), 3)*sA(p[1], p[4])/CyclicSpinorsA(p);
}
template <typename T>
typename Amp2q3g_a<T>::TreeValue
Amp2q3g_a<T>::hA017(const int* p) /* 1-1-1-11 */
{
  return pow(sB(p[0], p[4]), 3)*sB(p[1], p[4])/CyclicSpinorsB(p);
  return TreeValue();
}
template <typename T>
typename Amp2q3g_a<T>::TreeValue
Amp2q3g_a<T>::hA018(const int* p) /* -11-1-11 */
{
  const int nord[] = {p[1],p[0],p[4],p[3],p[2]};
  return  hA05(nord);
}
template <typename T>
typename Amp2q3g_a<T>::TreeValue
Amp2q3g_a<T>::hA021(const int* p) /* 1-11-11 */
{
  const int nord[] = {p[1],p[0],p[4],p[3],p[2]};
  return  hA022(nord);
}
template <typename T>
typename Amp2q3g_a<T>::TreeValue
Amp2q3g_a<T>::hA022(const int* p) /* -111-11 */
{
  return -pow(sA(p[0], p[3]), 3)*sA(p[1], p[3])/CyclicSpinorsA(p);
}
template <typename T>
typename Amp2q3g_a<T>::TreeValue
Amp2q3g_a<T>::hA025(const int* p) /* 1-1-111 */
{
  const int nord[] = {p[1],p[0],p[4],p[3],p[2]};
  return  hA014(nord);
}
template <typename T>
typename Amp2q3g_a<T>::TreeValue
Amp2q3g_a<T>::hA026(const int* p) /* -11-111 */
{
  return -pow(sA(p[0], p[2]), 3)*sA(p[1], p[2])/CyclicSpinorsA(p);
}
//}}}

//{{{ L1
//{{{ 1
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL11(const int* p) /* 1-1-1-1-1 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*hAL130(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 2
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL12(const int* p) /* -11-1-1-1 */
{
  const int nord[] = {p[1],p[0],p[4],p[3],p[2]};
  return hAL11(nord);
}
//}}}
//{{{ 5
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL15(const int* p) /* 1-11-1-1 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*hAL126(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 6
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL16(const int* p) /* -111-1-1 */
{
  const int nord[] = {p[1],p[0],p[4],p[3],p[2]};
  return hAL117(nord);
}
//}}}
//{{{ 9
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL19(const int* p) /* 1-1-11-1 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*hAL122(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 10
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL110(const int* p) /* -11-11-1 */
{
  const int nord[] = {p[1],p[0],p[4],p[3],p[2]};
  return hAL19(nord);
}
//}}}
//{{{ 13
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL113(const int* p) /* 1-111-1 */
{
  const int nord[] = {p[1],p[0],p[4],p[3],p[2]};
  return hAL126(nord);
}
//}}}
//{{{ 14
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL114(const int* p) /* -1111-1 */
{
  setxi(p);
  setsij(p);
  setaij(p);

  const TreeValue phase = -i_*a15*a15*a25/(a12*a23*a34*a45);
  TreeValue t1 = x2 + x3;
  TreeValue t2 = x1*t1;
  TreeValue t3 = x2*x3;
  TreeValue t4 = T(2.)*t3 + T(2.)*t2;
  TreeValue t5 = T(1.)/t4;
  const TreeValue t6 = x2*x2;
  TreeValue t7 = x1*t6;
  const TreeValue c1234 = t7*x3*x4*t5;
  const TreeValue t8 = -T(1.) + x5;
  const TreeValue t9 = x3*t8;
  const TreeValue c1235 = -t9*x1*T(0.5);
  const TreeValue t10 = x4 - x5;
  const TreeValue t11 = x3*x3;
  const TreeValue c1245 = -x2*(x1 + x2)*t11*t10*t8*t5;
  TreeValue t12 = x2*x4;
  const TreeValue t13 = x3*(-T(1.) + x4) + t12;
  TreeValue t14 = x1*t13*T(0.5) + t3*t10*T(0.5);
  const TreeValue c1345 = t14*t10;
  const TreeValue c2345 = t14*x4;
  const TreeValue c123 = -x1*(T(2.)*t3 + t2)*t5;
  const TreeValue c124 = t3*x1*x5*t5;
  const TreeValue c125 = -t9*x1*x2*t5;
  const TreeValue c134 = t2*(x1 - x2*t10)*t5;
  const TreeValue c145 = t7*t10*t5;
  const TreeValue c234 = t12*t2*t5;
  t7 = x1*x2;
  const TreeValue c245 = -t7*(t12 + t9)*t5;
  t5 = T(12.)*t4;
  t5 = T(1.)/t5;
  const TreeValue c2_4512_0 = -(-T(70.)*t3 - T(18.)*t2)*t5;
  t2 = T(2.)*t2;
  t4 = t4*T(0.5);
  t4 = T(1.)/t4;
  t5 = T(1.)/x3;
  const TreeValue c2_4512_1 = t1*(-T(8.)*t3 - t2)*x4*t5*t4*T(0.5);
  t12 = x4*x4;
  t14 = t1*t1;
  t5 = t5*t5;
  const TreeValue c2_4512_2 = -t14*(-T(4.)*t3 + t2)*t12*t5*t4*T(0.25);
  const TreeValue c2_4512_3 = -T(2.)/T(3.)*x2*t1*t14*x4*t12*t5*t4;
  const TreeValue c2_tree = T(3.)*T(0.25);
  t1 = T(3.)*x5;
  t2 = T(2.)*x5;
  t3 = T(1.)/t10;
  t14 = T(1.)/t8;
  const TreeValue rat = (-t6*x3*t11*t10*t8*T(0.5) + x1*x1*t13*t13*(t9 + x2*x5)*T(0.5)
    + t7*(T(4.)*x2*t6*t12*x5 + (T(10.)*t6*x4*t10 + (T(6.)*x2*(x5*(-T(1.) + t2) + (T(1.)
    - t1 + (T(3.) - t2)*x4)*x4) + T(4.)*t9*(-T(2.)*x4*(T(1.) + x4) + t1))*x3)*x3)/T(12.))/x2*t5*t4*t3*t14;
  EpsTriplet<T> amp, camp;
  amp += c1234*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  amp += c124*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c134*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
  amp += c245*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
  amp += c2_4512_0*(njetan->L0(s45,s12));
  amp += c2_4512_1*(njetan->L1(s45,s12));
  amp += c2_4512_2*(njetan->L2hat(s45,s12));
  amp += c2_4512_3*(njetan->L3hat(s45,s12));
  amp += c2_tree*(njetan->I2(s45, T(0.), T(0.))+njetan->I2(s12, T(0.), T(0.)));
  amp += rat;

  camp += conj(c1234)*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  camp += conj(c124)*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c134)*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
  camp += conj(c245)*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
  camp += conj(c2_4512_0)*(njetan->L0(s45,s12));
  camp += conj(c2_4512_1)*(njetan->L1(s45,s12));
  camp += conj(c2_4512_2)*(njetan->L2hat(s45,s12));
  camp += conj(c2_4512_3)*(njetan->L3hat(s45,s12));
  camp += conj(c2_tree)*(njetan->I2(s45, T(0.), T(0.))+njetan->I2(s12, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//{{{ 17
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL117(const int* p) /* 1-1-1-11 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*hAL114(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 18
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL118(const int* p) /* -11-1-11 */
{
  const int nord[] = {p[1],p[0],p[4],p[3],p[2]};
  return hAL15(nord);
}
//}}}
//{{{ 21
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL121(const int* p) /* 1-11-11 */
{
  const int nord[] = {p[1],p[0],p[4],p[3],p[2]};
  return hAL122(nord);
}
//}}}
//{{{ 22
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL122(const int* p) /* -111-11 */
{
  setxi(p);
  setsij(p);
  setaij(p);

  const TreeValue phase = -i_*pow(a14, 3)*a24/(a12*a15*a23*a34*a45);
  TreeValue t1 = T(2.)*x1 + T(2.)*x2;
  TreeValue t2 = T(1.)/t1;
  const TreeValue t3 = x2*x2;
  const TreeValue t4 = t3*t3;
  TreeValue t5 = x2*t4;
  const TreeValue t6 = x2*t3;
  TreeValue t7 = x1*t3;
  const TreeValue c1234 = t7*x4*t2;
  t1 = t1*T(0.5);
  TreeValue t8 = -T(1.) + x5;
  const TreeValue t9 = x2 + x3;
  TreeValue t10 = x1*t9;
  TreeValue t11 = x2*x3;
  TreeValue t12 = t11 + t10;
  t12 = T(1.)/t12;
  TreeValue t13 = t1*t1;
  const TreeValue t14 = x3*x3;
  const TreeValue t15 = t14*t14;
  const TreeValue t16 = x3*t14;
  TreeValue t17 = t12*t12;
  const TreeValue t18 = t12*t17;
  TreeValue t19 = x1*T(0.5);
  const TreeValue c1235 = -t19*t1*t13*t15*t8*t18;
  const TreeValue t20 = x4 - x5;
  const TreeValue c1245 = -t11*t20*t8*T(0.5);
  TreeValue t21 = -T(1.) + x4;
  const TreeValue t22 = x3*t21;
  TreeValue t23 = x1*(t22 + x2*x4);
  TreeValue t24 = t11*t20;
  TreeValue t25 = t23 + t24;
  const TreeValue t26 = T(1.)/t1;
  TreeValue t27 = T(1.)/t9;
  const TreeValue t28 = t27*t27;
  TreeValue t29 = t28*t28;
  TreeValue t30 = t27*t28;
  const TreeValue c1345 = (t5*T(0.5) + (x2*(x1 + x3) + x1*x3)*t16*T(0.5))*t25*t20*t26*t29;
  const TreeValue c2345 = x4*t25*T(0.5);
  TreeValue t31 = T(3.)*x2;
  TreeValue t32 = T(3.)*x3;
  const TreeValue t33 = T(2.)*x2;
  TreeValue t34 = T(5.)*x3;
  const TreeValue t35 = T(3.)*t3;
  const TreeValue t36 = x1*x1;
  const TreeValue t37 = t36*t36;
  const TreeValue t38 = x1*t36;
  const TreeValue t39 = T(2.)*t4;
  const TreeValue t40 = x1*t6;
  const TreeValue t41 = t38*x2;
  const TreeValue c123 = -t19*(t41*(t6 + (t35 + (t31 + t34)*x3)*x3) + (t35*t36*(t3 + (t33 + t32)*x3)
    + (t40*(t31 + T(7.)*x3) + (t39 + t37)*x3)*x3)*x3)*t26*t18;
  t31 = x1*x2;
  const TreeValue c124 = t31*x5*t2;
  const TreeValue t42 = x2 + T(2.)*x3;
  TreeValue t43 = T(3.)*t11;
  const TreeValue t44 = t3 + t43 + T(3.)*t14;
  const TreeValue t45 = t35*t14 + t43*x1*t42 + t36*t44;
  const TreeValue t46 = t11*t19;
  const TreeValue c125 = -t46*t45*t8*t18;
  t8 = t3 + t14;
  t43 = T(2.)*t8 + t43;
  const TreeValue t47 = T(2.)*x1;
  const TreeValue c134 = x3*(t47*t14*t9 - T(4.)*t3*t43)*(x1 - x2*t20)*t26*t29*T(0.25);
  const TreeValue t48 = T(6.)*t6;
  const TreeValue t49 = T(4.)*x3;
  const TreeValue t50 = t9*t9;
  const TreeValue c135 = -x2*(-T(4.)*t6*t15*t43 + t7*t14*t9*(t48 + (-T(8.)*t3
    + (-T(24.)*x2 - T(24.)*x3)*x3)*x3) + T(2.)*t38*t9*t50*(t6 - T(4.)*t16)
    + T(6.)*t11*t36*t50*(t6 + (-t33 - t49)*t14))*(t10*t21 + t24)*t26*t29*t18*T(0.25);
  t7 = T(4.)*t11*t43 + t47*t9*t44;
  const TreeValue c145 = t3*t7*t20*t26*t29*T(0.25);
  const TreeValue c234 = t31*x4*t2;
  const TreeValue c235 = -t19*t45*(t11*t21 + t23)*t18;
  const TreeValue c345 = t7*t25*t26*t29*T(0.25);
  const TreeValue c2_4512_0 = x3*(T(3.)*t10*(x2 + t32) + t33*(T(14.)*t3 + (T(19.)*x2 + T(11.)*x3)*x3))*t26*t30/T(6.);
  t2 = T(8.)*x2;
  t7 = T(4.)*x2;
  const TreeValue c2_4512_1 = -x3*(t48 + (x2*(x1 + t2) + (x1 + t7)*x3)*x3)*x4*t26*t30;
  t19 = x4*x4;
  const TreeValue c2_4512_2 = -x3*(t10 - t33*(t33 + x3))*t19*t26*t28*T(0.5);
  const TreeValue c2_4512_3 = -T(2.)/T(3.)*t11*x4*t19*t26*t27;
  t23 = T(2.)*t31;
  const TreeValue c2_1534_0 = t46*t1*(t23 + t34*t1)*t18;
  t24 = (-x1 + x2*t20)*x3 + t10*x4;
  t24 = T(1.)/t24;
  const TreeValue c2_1534_1 = -t36*t3*t1*x3*(t31 + t32*t1)*t18*t24;
  t29 = t24*t24;
  const TreeValue c2_1534_2 = t38*t6*t13*t14*t18*t29*T(0.5);
//  c2_1534_3 = T(0.);
  t31 = T(3.)*x1;
  t32 = T(1.)/T(12.);
  const TreeValue c2_1234_0 = -t32*(t6*(T(9.)*x1 + T(35.)*x2) + (T(7.)*t3*(t31 + T(7.)*x2) + (x2*(t31
    + T(29.)*x2) - T(9.)*t1*x3)*x3)*x3)*t26*t30;
  const TreeValue c2_1234_1 = t40*(t39*t36 + t40*(T(8.)*x1 + T(5.)*x2)*x3 + t3*(T(16.)*t36
    + (T(17.)*x1 + t7)*x2)*t14 + t2*t1*(t47 + x2)*t16 + T(6.)*t13*t15)*t21*t26*t30*t17*t24;
  t1 = t21*t21;
  t2 = t21*t1;
  const TreeValue c2_1234_2 = -t36*t4*(T(2.)*t11*t42 + t10*(x2 + t49))*t1*t26*t28*t12*t29*T(0.5);
  const TreeValue c2_1234_3 = T(2.)/T(3.)*t38*t5*t2*t26*t27*t24*t29;
  const TreeValue c2_tree = T(3.)*T(0.25);
  t1 = T(2.)*x4;
  t7 = T(9.)*x5;
  t10 = T(6.)*x4;
  t11 = T(12.)*x5;
  t12 = T(4.)*x4;
  t13 = T(15.)*x5;
  t17 = T(18.)*x5;
  t21 = T(3.)*x5;
  t24 = T(8.)*x4;
  t27 = T(6.)*x5;
  t29 = T(4.)*x5;
  t30 = T(5.)*x5;
  t31 = T(24.)*x4;
  t34 = t6*t20;
  t23 = t23*(t4*(T(1.) + (-T(6.) + t1)*x4)*t20 + (t34*(T(8.) + t21 + (-T(31.) + t24)*x4) + (-t3*(x5*(T(31.) + t11)
    + (-T(25.) - T(78.)*x5 + (T(48.) + T(18.)*x4)*x4)*x4) + (-x2*((T(48.) + t13)*x5 + (-T(63.) - T(102.)*x5 + (T(78.)
    -x5 + T(22.)*x4)*x4)*x4 + T(9.)) + ((-T(30.) - t27)*x5 + (T(51.) + T(49.)*x5 + (-T(52.) - t30 + t1)*x4)*x4 - T(9.))*x3)*x3)*x3)*x3);
  t19 = t36*t4*t14*t9*(T(6.)*t3*(x2*(T(3.) + t21 + (-T(10.) + t1)*x4) + x3*(T(6.)*t19 + T(6.)*x5 - T(25.)*x4
    + T(13.)))*t20 - T(2.)*t14*(x3*(t7*(T(2.) + t21) + (-T(16.) - T(69.)*x5 + (T(36.) - T(9.)*x5
    + T(13.)*x4)*x4)*x4) + x2*(t27*(T(1.) + t27) + (-T(4.) - T(81.)*x5 + (T(39.) - t17
    + t31)*x4)*x4)));
  t5 = t47*t5*t16*t20*(t6*(T(4.) + t7 + (-T(18.) + t1)*x4) + (t3*(T(13.) + t11 + (-T(31.) + t10)*x4)
    + (x2*(t17 + (-T(17.) - t10)*x4) + (T(3.) + t13 + (-T(16.) - t12)*x4)*x3)*x3)*x3);
  t17 = T(1.)/t20;
  t25 = T(1.)/t25;
  const TreeValue rat = t32*(T(6.)*t36*t37*x3*t9*t50*t50*t2 + t41*(t35*x3*(T(2.)*t34*(T(1.) + x5
    + (-T(7.) + t1)*x4) + T(2.)*t3*x3*t20*(T(13.) + t29 + (-T(23.) + t10)*x4) - T(2.)*t16*((T(12.)
    + T(7.)*x5)*x5 + (-T(13.) - T(27.)*x5
    + (T(17.) - x5 + t12)*x4)*x4 + T(1.)) + x2*t14*(-t29*(T(3.) + t30) + (T(8.) + T(62.)*x5 + (-T(30.)
    + T(12.)*x5 - t31)*x4)*x4)) - T(2.)*t36*(T(3.)*t4*x4*t20 + (t6*(t21 + (-T(1.) - t13 + (T(9.)
    + t10)*x4)*x4) + (t3*(T(9.) + T(21.)*x5 + (-T(42.) - T(6.)*t27 + (T(45.) + T(3.)*x5 + t12)*x4)*x4)
    + (t33*(T(9.) + t11 + (-T(36.) - T(18.)*x5 + (T(36.) + T(3.)*x5 - T(5.)*x4)*x4)*x4) - t22*(t7
    + (-T(25.) - t21 + t24)*x4 + T(9.)))*x3)*x3)*x3) + t23)*t50 + t19 - T(6.)*t3*t4*t15*t8*t20*t20
    + t5)/x2*t26*t28*t18*t25*t17;
  EpsTriplet<T> amp, camp;
  amp += c1234*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  amp += c124*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c134*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  amp += c2_4512_0*(njetan->L0(s45,s12));
  amp += c2_4512_1*(njetan->L1(s45,s12));
  amp += c2_4512_2*(njetan->L2hat(s45,s12));
  amp += c2_4512_3*(njetan->L3hat(s45,s12));
  amp += c2_1534_0*(njetan->L0(s15,s34));
  amp += c2_1534_1*(njetan->L1(s15,s34));
  amp += c2_1534_2*(njetan->L2hat(s15,s34));
//  amp += c2_1534_3*(njetan->L3hat(s15,s34));
  amp += c2_1234_0*(njetan->L0(s12,s34));
  amp += c2_1234_1*(njetan->L1(s12,s34));
  amp += c2_1234_2*(njetan->L2hat(s12,s34));
  amp += c2_1234_3*(njetan->L3hat(s12,s34));
  amp += c2_tree*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s12, T(0.), T(0.)));
  amp += rat;

  camp += conj(c1234)*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  camp += conj(c124)*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c134)*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  camp += conj(c2_4512_0)*(njetan->L0(s45,s12));
  camp += conj(c2_4512_1)*(njetan->L1(s45,s12));
  camp += conj(c2_4512_2)*(njetan->L2hat(s45,s12));
  camp += conj(c2_4512_3)*(njetan->L3hat(s45,s12));
  camp += conj(c2_1534_0)*(njetan->L0(s15,s34));
  camp += conj(c2_1534_1)*(njetan->L1(s15,s34));
  camp += conj(c2_1534_2)*(njetan->L2hat(s15,s34));
//  camp += conj(c2_1534_3)*(njetan->L3hat(s15,s34));
  camp += conj(c2_1234_0)*(njetan->L0(s12,s34));
  camp += conj(c2_1234_1)*(njetan->L1(s12,s34));
  camp += conj(c2_1234_2)*(njetan->L2hat(s12,s34));
  camp += conj(c2_1234_3)*(njetan->L3hat(s12,s34));
  camp += conj(c2_tree)*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s12, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//{{{ 25
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL125(const int* p) /* 1-1-111 */
{
  const int nord[] = {p[1],p[0],p[4],p[3],p[2]};
  return hAL114(nord);
}
//}}}
//{{{ 26
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL126(const int* p) /* -11-111 */
{
  setxi(p);
  setsij(p);
  setaij(p);

  const TreeValue phase = -i_*pow(a13, 3)/(a12*a15*a34*a45);
  TreeValue t1 = x1*x2;
  TreeValue t2 = t1*x4*T(0.5);
  const TreeValue c1234 = t2;
  const TreeValue t3 = -T(1.) + x5;
  const TreeValue t4 = x2 + x3;
  TreeValue t5 = x1*t4;
  TreeValue t6 = x2*x3;
  TreeValue t7 = t6 + t5;
  t7 = T(1.)/t7;
  TreeValue t8 = t7*t7;
  const TreeValue t9 = t7*t8;
  const TreeValue t10 = x3*x3;
  const TreeValue t11 = t10*t10;
  const TreeValue t12 = x3*t10;
  const TreeValue t13 = x2*x2;
  const TreeValue t14 = t13*t13;
  const TreeValue t15 = x2*t13;
  TreeValue t16 = t13*t14;
  TreeValue t17 = x1*T(0.5);
  TreeValue t18 = t17*t15;
  const TreeValue c1235 = -t18*t11*t3*t9;
  const TreeValue t19 = x4 - x5;
  const TreeValue c1245 = -t6*t19*t3*T(0.5);
  const TreeValue t20 = -T(1.) + x4;
  TreeValue t21 = x2*x4;
  TreeValue t22 = x3*t20 + t21;
  TreeValue t23 = t6*t19;
  TreeValue t24 = x1*t22;
  TreeValue t25 = t24 + t23;
  TreeValue t26 = t25*T(0.5);
  const TreeValue c1345 = t26*t19;
  TreeValue t27 = x1 + x2;
  const TreeValue t28 = T(1.)/t27;
  TreeValue t29 = t28*t28;
  TreeValue t30 = t28*t29;
  const TreeValue c2345 = t26*t15*x4*t30;
  t26 = x1*x1;
  const TreeValue t31 = t26*t26;
  const TreeValue t32 = x1*t26;
  const TreeValue t33 = t4*t4;
  const TreeValue t34 = t4*t33;
  const TreeValue t35 = T(2.)*t15;
  const TreeValue t36 = t35*t12;
  TreeValue t37 = t26*t33;
  const TreeValue t38 = T(3.)*t13;
  TreeValue t39 = t32*t34;
  const TreeValue c123 = -t17*(t36 + t5*t38*t10 + T(3.)*t37*t6 + t39)*t9;
  const TreeValue t40 = t38*t10;
  TreeValue t41 = T(3.)*t1;
  TreeValue t42 = t41*x3;
  TreeValue t43 = t40 + t42*t4 + t37;
  const TreeValue t44 = x3*t3;
  const TreeValue c125 = -t44*t17*t4*t43*t9;
  TreeValue t45 = t5*t20;
  const TreeValue t46 = T(1.)/x2;
  const TreeValue c135 = -t17*t4*t43*(t45 + t23)*t46*t9;
  t23 = t26 + t41 + t38;
  const TreeValue c234 = t2*t23*t30;
  const TreeValue c235 = -t18*(t40 + t42*(x2 + T(2.)*x3) + t26*(t13 + T(3.)*t6 + T(3.)*t10))*(t6*t20 + t24)*t30*t9;
  t2 = t17*t23;
  const TreeValue c245 = t2*(t21 + t44)*t30;
  const TreeValue c345 = t2*t25*t46*t30;
//  c2_2315_0 = T(0.);
  t2 = T(2.)*x2;
  t18 = T(1.)/x3;
  t21 = T(1.)/t3;
  const TreeValue c2_2315_1 = t24*(x1 + t2)*t29*t18*t21;
  const TreeValue c2_2315_2 = t17*t22*t22*t28*t18*t18*t21*t21;
//  c2_2315_3 = T(0.);
  const TreeValue c2_1534_0 = (-T(3.)*T(0.25)*t15 + (t13*(T(1.) + T(10.)*t13*t7)
    + (x2*(T(7.) - T(16.)*t14*t8) + (T(3.) + T(6.)
* t16*t9)*x1)*x1)*x1*T(0.25))*t30;
  t17 = (-x1 + x2*t19)*x3 + t5*x4;
  t17 = T(1.)/t17;
  const TreeValue c2_1534_1 = -t26*x2*t14*x3*(t1 + T(3.)*t27*x3)*t29*t9*t17;
  t22 = t17*t17;
  const TreeValue c2_1534_2 = t32*t16*t10*t28*t9*t22*T(0.5);
//  c2_1534_3 = T(0.);
  const TreeValue c2_1234_0 = -T(13.)/T(6.);
  const TreeValue c2_1234_1 = t45*(T(4.)*t13*t10 + T(5.)*t1*x3*t4 + T(2.)*t37)*t8*t17;
  t1 = T(2.)*t6;
  t6 = t20*t20;
  t8 = t6*t20;
  const TreeValue c2_1234_2 = -t37*(t1 + t5)*t6*t7*t22*T(0.5);
  const TreeValue c2_1234_3 = T(2.)/T(3.)*t39*t8*t17*t22;
  const TreeValue c2_tree = T(3.)*T(0.25);
  t5 = T(2.)*x4;
  t7 = T(2.)*x5;
  t16 = T(1.) + t7;
  t17 = x4*x4;
  t22 = T(6.)*x2*t6;
  t23 = t22*(-x5 + x4*(-T(1.) + t7));
  t24 = T(6.)*t13;
  t27 = T(4.)*x5;
  t29 = T(7.)*x5;
  t30 = T(9.)*x5;
  t37 = T(5.)*x5;
  t39 = T(3.)*x5;
  t41 = -T(4.) + t29;
  t42 = T(13.)*x5;
  t43 = x5*x5;
  t45 = T(8.)*t11*t8*t3;
  t6 = t2*t12*t6;
  t1 = t1*t31*t33*(t15*(x2*(-T(1.) + x5 + (T(6.) - t30 + t5*(T(5.) + x5))*x4)
    + x3*((T(9.) - T(51.)*x5 + t5*(T(11.) + t29))*x4 + (T(16.) + t39)*x5 - T(13.)))*x4 + t45
    + t6*(T(1.) - t29 + x4*(-T(7.) + t42)) + t40*(-x5 + (-T(4.) + (T(17.) + x5)*x5 + (T(3.) - T(28.)*x5
    + t5*(T(1.) + t37))*x4)*x4));
  t22 = t36*t26*(t45 + t22*t12*(T(1.) - t39 + x4*(-T(3.) + t37)) + t38*(t10*(-t39 + (-T(12.) + T(6.)*(T(4.) + x5)*x5
    + (T(20.) - T(47.)*x5 + t5*(-T(1.) + t29))*x4)*x4) + t13*x4*(-T(3.) + T(3.)*t43 + (T(10.) - t42 + t5*(T(1.) + x5))*x4))
    + t15*x3*x4*((-T(18.)*t41 + t5*(T(5.) + t42))*x4 + (T(25.) + T(27.)*x5)*x5 - T(34.)));
  t25 = T(1.)/t25;
  t26 = T(1.)/T(12.);
  const TreeValue rat = t26*(-T(6.)*t15*t14*x3*t11*x4*t19*t3 + x1*t31*t34*(T(6.)*t14*x4*t17
    + (t35*x4*(-T(1.) + x5 + (-T(3.)*t16 + (T(7.) + t7)*x4)*x4) + (t24*t20*x4*((-T(3.) + t5)*x5 + x4)
    + (t23 + T(4.)*t44*t8)*x3)*x3)*x3) + x1*t14*t11*(t13*(T(6.)*x3*(x4*(T(7.) - T(11.)*x5) + (T(1.)
    + T(2.)*t17 + t27)*x5 - T(3.)) + t2*((T(18.) - T(3.)*t29 + x4*t16)*x4 + (-T(5.)
    + t30)*x5 - T(4.)))*x4 + T(4.)*t12*t8*t3 + t23*t10) + t22 + t24*t32*t10*t4*(t15*(x2*(-T(1.) + t43
    + (T(7.) - T(10.)*x5 + t5*(T(2.) + x5))*x4) + x3*((T(17.) - T(41.)*x5 + t5*(T(4.) + t37))*x4
    + (T(12.) + t37)*x5 - T(11.)))*x4 + T(4.)*t11*t8*t3 + t6*(T(1.) - t27 + x4*t41) + t13*t10*(-t39 + (-T(12.) + T(4.)*(T(8.)
    + x5)*x5 + (T(16.) - T(55.)*x5 + T(18.)*x4*x5)*x4)*x4)) + t1)*t46*t46*t28*t18*t9/x4*t25*t21;
  EpsTriplet<T> amp, camp;
  amp += c1234*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  amp += c245*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
//  amp += c2_2315_0*(njetan->L0(s23,s15));
  amp += c2_2315_1*(njetan->L1(s23,s15));
  amp += c2_2315_2*(njetan->L2hat(s23,s15));
//  amp += c2_2315_3*(njetan->L3hat(s23,s15));
  amp += c2_1534_0*(njetan->L0(s15,s34));
  amp += c2_1534_1*(njetan->L1(s15,s34));
  amp += c2_1534_2*(njetan->L2hat(s15,s34));
//  amp += c2_1534_3*(njetan->L3hat(s15,s34));
  amp += c2_1234_0*(njetan->L0(s12,s34));
  amp += c2_1234_1*(njetan->L1(s12,s34));
  amp += c2_1234_2*(njetan->L2hat(s12,s34));
  amp += c2_1234_3*(njetan->L3hat(s12,s34));
  amp += c2_tree*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  amp += rat;

  camp += conj(c1234)*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  camp += conj(c245)*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
//  camp += conj(c2_2315_0)*(njetan->L0(s23,s15));
  camp += conj(c2_2315_1)*(njetan->L1(s23,s15));
  camp += conj(c2_2315_2)*(njetan->L2hat(s23,s15));
//  camp += conj(c2_2315_3)*(njetan->L3hat(s23,s15));
  camp += conj(c2_1534_0)*(njetan->L0(s15,s34));
  camp += conj(c2_1534_1)*(njetan->L1(s15,s34));
  camp += conj(c2_1534_2)*(njetan->L2hat(s15,s34));
//  camp += conj(c2_1534_3)*(njetan->L3hat(s15,s34));
  camp += conj(c2_1234_0)*(njetan->L0(s12,s34));
  camp += conj(c2_1234_1)*(njetan->L1(s12,s34));
  camp += conj(c2_1234_2)*(njetan->L2hat(s12,s34));
  camp += conj(c2_1234_3)*(njetan->L3hat(s12,s34));
  camp += conj(c2_tree)*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//{{{ 29
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL129(const int* p) /* 1-1111 */
{
  const int nord[] = {p[1],p[0],p[4],p[3],p[2]};
  return hAL130(nord);
}
//}}}
//{{{ 30
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL130(const int* p) /* -11111 */
{
  setxi(p);
  setsij(p);
  setaij(p);
  const TreeValue phase = i_/(a23*a25*a34*a45*b12);
  const TreeValue t1 = x2 + x3;
  const TreeValue t2 = x2*x3;
  const TreeValue t3 = x4 - x5;
  const TreeValue t4 = -T(1.)/T(6.);
  const TreeValue rat = t4*(t2 + x1*t1)*((T(3.)*x1*x1*x4 + T(2.)*x3*t1*t3)*x2 + x1*(T(2.)*x2*x2*x4
    + T(2.)*x3*x3*t3 - t2*(T(2.) + T(3.)*x5 + (-T(7.) + T(2.)*x4)*x4)))/(x1*x2*x3);
  EpsTriplet<T> amp;
  amp += rat;
  const LoopResult<T> res = {phase*amp, conj(phase*amp)};
  return res;
}
//}}}
//}}}

//{{{ L2
//{{{ 1
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL21(const int* p) /* 1-1-1-1-1 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*hAL230(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 3
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL23(const int* p) /* 11-1-1-1 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*hAL228(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 4
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL24(const int* p) /* -1-11-1-1 */
{
  const int nord[] = {p[2],p[1],p[0],p[4],p[3]};
  return hAL21(nord);
}
//}}}
//{{{ 6
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL26(const int* p) /* -111-1-1 */
{
  const int nord[] = {p[2],p[1],p[0],p[4],p[3]};
  return hAL23(nord);
}
//}}}
//{{{ 9
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL29(const int* p) /* 1-1-11-1 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*hAL222(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 11
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL211(const int* p) /* 11-11-1 */
{
  const int nord[] = {p[2],p[1],p[0],p[4],p[3]};
  return hAL222(nord);
}
//}}}
//{{{ 12
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL212(const int* p) /* -1-111-1 */
{
  const int nord[] = {p[2],p[1],p[0],p[4],p[3]};
  return hAL217(nord);
}
//}}}
//{{{ 14
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL214(const int* p) /* -1111-1 */
{
  setxi(p);
  setsij(p);
  setaij(p);

  const TreeValue phase = -i_*a15*a15*a35/(a12*a23*a34*a45);
//  c1234 = T(0.);
  TreeValue t1 = -T(1.) + x5;
  TreeValue t2 = x1*T(0.5);
  const TreeValue t3 = x3*t1;
  const TreeValue c1235 = -t3*t2;
  TreeValue t4 = x4 - x5;
  TreeValue t5 = T(2.)*x2 + T(2.)*x3;
  const TreeValue t6 = T(1.)/t5;
  const TreeValue t7 = x2*x3*x3*t4*t1;
  const TreeValue c1245 = -t7*t6;
  const TreeValue t8 = -T(1.) + x4;
  const TreeValue t9 = x2*x4;
  const TreeValue t10 = x3*t8 + t9;
  const TreeValue t11 = x1*t10;
  const TreeValue t12 = x2*x3;
  const TreeValue t13 = t11*T(0.5) + t12*t4*T(0.5);
  const TreeValue c1345 = t13*t4;
  t5 = t5*T(0.5);
  const TreeValue t14 = t12 + x1*t5;
  TreeValue t15 = x1 + x2;
  const TreeValue t16 = T(1.)/t5;
  t15 = T(1.)/t15;
  const TreeValue c2345 = t13*t14*x4*t15*t16;
  const TreeValue c123 = -t2;
  const TreeValue c124 = t12*x5*t6;
  const TreeValue c125 = -t3*x2*t6;
  const TreeValue c134 = x1*T(0.5) - x2*t4*T(0.5);
//  c135 = T(0.);
  const TreeValue c145 = x2*x2*t4*t6;
  const TreeValue c234 = -t9*t14*t15*t16*T(0.5);
  const TreeValue c235 = x2*(t12*t8 + t11)*t15*t16*T(0.5);
  const TreeValue c245 = -t2*x2*(t9 + t3)*t15*t16;
  const TreeValue c345 = t13*x2*t15*t16;
  const TreeValue c2_4512_0 = T(3.)*T(0.25);
  t2 = T(1.)/x3;
  const TreeValue c2_4512_1 = -t5*x4*t2;
  t2 = t2*t2;
  const TreeValue c2_4512_2 = -t5*t5*x4*x4*t2*T(0.5);
//  c2_4512_3 = T(0.);
  const TreeValue c2_tree = T(3.)*T(0.25);
  t1 = T(1.)/t1;
  t4 = T(1.)/t4;
  const TreeValue rat = (-t7*t5*T(0.5) + x1*t10*t10*(t3 + x2*x5)*T(0.5))/x2*t2*t16*t4*t1;
  EpsTriplet<T> amp, camp;
//  amp += c1234*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  amp += c124*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c134*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  amp += c245*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  amp += c2_4512_0*(njetan->L0(s45,s12));
  amp += c2_4512_1*(njetan->L1(s45,s12));
  amp += c2_4512_2*(njetan->L2hat(s45,s12));
//  amp += c2_4512_3*(njetan->L3hat(s45,s12));
  amp += c2_tree*(njetan->I2(s45, T(0.), T(0.))+njetan->I2(s12, T(0.), T(0.)));
  amp += rat;

//  camp += conj(c1234)*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  camp += conj(c124)*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c134)*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  camp += conj(c245)*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  camp += conj(c2_4512_0)*(njetan->L0(s45,s12));
  camp += conj(c2_4512_1)*(njetan->L1(s45,s12));
  camp += conj(c2_4512_2)*(njetan->L2hat(s45,s12));
//  camp += conj(c2_4512_3)*(njetan->L3hat(s45,s12));
  camp += conj(c2_tree)*(njetan->I2(s45, T(0.), T(0.))+njetan->I2(s12, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//{{{ 17
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL217(const int* p) /* 1-1-1-11 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*hAL214(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 19
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL219(const int* p) /* 11-1-11 */
{
  const int nord[] = {p[2],p[1],p[0],p[4],p[3]};
  return hAL214(nord);
}
//}}}
//{{{ 20
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL220(const int* p) /* -1-11-11 */
{
  const int nord[] = {p[2],p[1],p[0],p[4],p[3]};
  return hAL29(nord);
}
//}}}
//{{{ 22
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL222(const int* p) /* -111-11 */
{
  setxi(p);
  setsij(p);
  setaij(p);

  const TreeValue phase = -i_*pow(a14, 3)/(a12*a15*a23*a45);
//  c1234 = T(0.);
  TreeValue t1 = x1 + x2;
  TreeValue t2 = -T(1.) + x5;
  const TreeValue t3 = x2 + x3;
  const TreeValue t4 = x2*x3;
  TreeValue t5 = x1*t3;
  TreeValue t6 = t4 + t5;
  t6 = T(1.)/t6;
  TreeValue t7 = t6*t6;
  const TreeValue t8 = t6*t7;
  TreeValue t9 = t1*t1;
  const TreeValue t10 = x3*x3;
  const TreeValue t11 = t10*t10;
  TreeValue t12 = x3*t10;
  TreeValue t13 = x1*T(0.5);
  TreeValue t14 = t13*t1*t9;
  const TreeValue c1235 = -t14*t11*t2*t8;
  const TreeValue t15 = x4 - x5;
  const TreeValue c1245 = -t4*t15*t2*T(0.5);
  const TreeValue t16 = -T(1.) + x4;
  const TreeValue t17 = x3*t16;
  TreeValue t18 = t4*t15;
  TreeValue t19 = x1*(t17 + x2*x4);
  TreeValue t20 = t19 + t18;
  const TreeValue t21 = T(1.)/t3;
  TreeValue t22 = t21*t21;
  TreeValue t23 = t21*t22;
  TreeValue t24 = t12*T(0.5);
  const TreeValue c1345 = t24*t20*t15*t23;
  TreeValue t25 = x4*T(0.5);
  const TreeValue c2345 = t25*t20;
  const TreeValue c123 = -t14*t12*t8;
  t12 = x2*T(0.5);
  const TreeValue c124 = t12*x5;
  t14 = T(2.)*x3;
  const TreeValue t26 = x2*x2;
  const TreeValue t27 = t26*t26;
  const TreeValue t28 = x2*t26;
  TreeValue t29 = T(3.)*t4;
  TreeValue t30 = T(3.)*t10;
  const TreeValue t31 = t26 + t29 + t30;
  const TreeValue t32 = x1*x1;
  t29 = t30*t26 + t29*x1*(x2 + t14) + t32*t31;
  t30 = t4*t13;
  const TreeValue c125 = -t30*t29*t2*t8;
  const TreeValue c134 = t24*(x1 - x2*t15)*t23;
  t2 = T(3.)*x2;
  const TreeValue t33 = t3*t3;
  const TreeValue t34 = t26*t31;
  const TreeValue c135 = t24*x2*(T(3.)*t32*t33 + t2*x1*(t26 + (t2 + t14)*x3) + t34)*(t5*t16 + t18)*t23*t8;
  const TreeValue c145 = t34*t15*t23*T(0.5);
  const TreeValue c234 = -t25*x2;
  const TreeValue c235 = -t13*t29*(t4*t16 + t19)*t8;
//  c245 = T(0.);
  const TreeValue c345 = t31*t20*t23*T(0.5);
  t13 = T(3.)*x3;
  t14 = x2 + t13;
  t18 = x3*T(0.5);
  const TreeValue c2_4512_0 = t18*t14*t22;
  const TreeValue c2_4512_1 = -t10*x4*t22;
  t19 = T(2.)*t3;
  t19 = T(1.)/t19;
  t23 = x4*x4;
  t24 = x4*t23;
  const TreeValue c2_4512_2 = -x3*t23*t19;
//  c2_4512_3 = T(0.);
  t19 = T(2.)*x2;
  t25 = T(5.)*x3;
  const TreeValue c2_1534_0 = t30*t1*(t19*x1 + t25*t1)*t8;
  t29 = (-x1 + x2*t15)*x3 + t5*x4;
  t29 = T(1.)/t29;
  const TreeValue c2_1534_1 = -t32*t26*t1*x3*(x1*x2 + t13*t1)*t8*t29;
  t1 = t29*t29;
  const TreeValue c2_1534_2 = x1*t32*t28*t9*t10*t8*t1*T(0.5);
//  c2_1534_3 = T(0.);
  const TreeValue c2_1234_0 = T(3.)*T(0.25) - t12*(t2 + t25)*t22;
  const TreeValue c2_1234_1 = t17*x1*t28*(t5*t14 + t4*(t19 + t13))*t22*t7*t29;
  t5 = t16*t16;
  const TreeValue c2_1234_2 = -t18*t32*t27*t5*t21*t6*t1;
//  c2_1234_3 = T(0.);
  const TreeValue c2_tree = T(3.)*T(0.25);
  t1 = T(3.)*x5;
  t6 = T(4.)*x4;
  t7 = T(3.)*x4;
  t9 = T(5.)*x5;
  t12 = x5*x5;
  t13 = T(6.)*x5;
  t14 = T(2.)*x5;
  t19 = T(6.)*x4;
  t22 = T(1.)/t15;
  t20 = T(1.)/t20;
  t1 = t18*(-x1*t32*t32*t33*t33*t5*t16 + x2*t32*(t32*(t28*x4*t15 + (t26*(T(4.)*x5 + (-T(13.) - t13 + (T(15.)
    + x5 - t6)*x4)*x4 + T(3.)) + (x2*(T(6.) + t9 + (-T(23.) - T(8.)*x5 + (T(26.) + T(2.)*x5 - T(8.)*x4)*x4)*x4) - t17*(T(3.)
    + t14 + (-T(8.) - x5 + t6)*x4))*x3)*x3) + t26*x3*(t26*(-T(2.) + x4)*t15 + (t2*(x4*(-T(1.) + t7) + (T(1.) - t6
    + x5)*x5) + (T(1.) - T(4.)*t24 + t13 + T(3.)*t12 + T(3.)*t23*(T(5.) + x5) - t7*(T(3.) + t9))*x3)*x3)
    + t4*x1*(t26*(x5*(T(4.) + x5) + (-T(4.) - T(7.)*x5 + t19)*x4) + (x2*((T(9.) + t14)*x5 + (-T(18.) - T(19.)*x5 + (T(26.)
    + t1 - t19)*x4)*x4 + T(3.)) + (T(3.) - T(6.)*t24 + t13 + t12 + t23*(T(19.) + t1) - x4*(T(15.)
    + T(11.)*x5))*x3)*x3))*t3 + x2*t27*t11*t15*t15 - x1*t27*t10*t15*(-t26*t16 + (x2*(T(1.) - T(5.)*x4
    + t1) + (T(2.) + t1 + (-T(6.) + x4)*x4)*x3)*x3));
  const TreeValue rat = -t1/x2*t21*t8*t20*t22;
  EpsTriplet<T> amp, camp;
//  amp += c1234*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  amp += c124*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c134*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
//  amp += c245*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  amp += c2_4512_0*(njetan->L0(s45,s12));
  amp += c2_4512_1*(njetan->L1(s45,s12));
  amp += c2_4512_2*(njetan->L2hat(s45,s12));
//  amp += c2_4512_3*(njetan->L3hat(s45,s12));
  amp += c2_1534_0*(njetan->L0(s15,s34));
  amp += c2_1534_1*(njetan->L1(s15,s34));
  amp += c2_1534_2*(njetan->L2hat(s15,s34));
//  amp += c2_1534_3*(njetan->L3hat(s15,s34));
  amp += c2_1234_0*(njetan->L0(s12,s34));
  amp += c2_1234_1*(njetan->L1(s12,s34));
  amp += c2_1234_2*(njetan->L2hat(s12,s34));
//  amp += c2_1234_3*(njetan->L3hat(s12,s34));
  amp += c2_tree*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s12, T(0.), T(0.)));
  amp += rat;

//  camp += conj(c1234)*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  camp += conj(c124)*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c134)*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
//  camp += conj(c245)*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  camp += conj(c2_4512_0)*(njetan->L0(s45,s12));
  camp += conj(c2_4512_1)*(njetan->L1(s45,s12));
  camp += conj(c2_4512_2)*(njetan->L2hat(s45,s12));
//  camp += conj(c2_4512_3)*(njetan->L3hat(s45,s12));
  camp += conj(c2_1534_0)*(njetan->L0(s15,s34));
  camp += conj(c2_1534_1)*(njetan->L1(s15,s34));
  camp += conj(c2_1534_2)*(njetan->L2hat(s15,s34));
//  camp += conj(c2_1534_3)*(njetan->L3hat(s15,s34));
  camp += conj(c2_1234_0)*(njetan->L0(s12,s34));
  camp += conj(c2_1234_1)*(njetan->L1(s12,s34));
  camp += conj(c2_1234_2)*(njetan->L2hat(s12,s34));
//  camp += conj(c2_1234_3)*(njetan->L3hat(s12,s34));
  camp += conj(c2_tree)*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s12, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//{{{ 25
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL225(const int* p) /* 1-1-111 */
{
  const int nord[] = {p[2],p[1],p[0],p[4],p[3]};
  return hAL228(nord);
}
//}}}
//{{{ 27
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL227(const int* p) /* 11-111 */
{
  const int nord[] = {p[2],p[1],p[0],p[4],p[3]};
  return hAL230(nord);
}
//}}}
//{{{ 28
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL228(const int* p) /* -1-1111 */
{
  setxi(p);
  setsij(p);
  setaij(p);

  const TreeValue phase = -i_*a12*a12/(a15*a34*a45);
  TreeValue t1 = x1*T(0.5);
  TreeValue t2 = x2*x4;
  const TreeValue c1234 = -t2*t1;
//  c1235 = T(0.);
  const TreeValue t3 = x4 - x5;
  TreeValue t4 = -T(1.) + x5;
  const TreeValue t5 = x3*t4;
  TreeValue t6 = t5*T(0.5);
  const TreeValue c1245 = t6*x2*t3;
  const TreeValue t7 = -T(1.) + x4;
  const TreeValue t8 = x3*t7 + t2;
  TreeValue t9 = x2*x3;
  TreeValue t10 = t9*t3;
  const TreeValue c1345 = -(x1*t8*T(0.5) + t10*T(0.5))*t3;
//  c2345 = T(0.);
  const TreeValue c123 = t1;
//  c124 = T(0.);
  const TreeValue c125 = t6;
//  c134 = T(0.);
  t1 = x2 + x3;
  t6 = T(1.)/x2;
  const TreeValue c135 = (x1*t1*t7*T(0.5) + t10*T(0.5))*t6;
//  c145 = T(0.);
  const TreeValue c234 = t2*T(0.5);
//  c235 = T(0.);
  const TreeValue c245 = (T(1.) - x5)*x3*T(0.5) - t2*T(0.5);
  const TreeValue c345 = (x1*(-t1*x4 + x3)*T(0.5) - t9*t3*T(0.5))*t6;
  t1 = x1*x1;
  t9 = pow(x1, -2);
  const TreeValue c2_2315_0 = (T(3.)*T(0.25)*t1 - (-T(8.)*x1 + T(4.)*x2)*x2*T(0.125))*t9;
  t10 = x1 + x2;
  t4 = T(1.)/t4;
  const TreeValue t11 = T(1.)/x3;
  const TreeValue c2_2315_1 = t10*(-T(2.)*x1 + T(2.)*x2)*t8*t9*t11*t4*T(0.5);
  const TreeValue t12 = t8*t8;
  t10 = t10*t10;
  const TreeValue c2_2315_2 = -t10*t12*t9*t11*t11*t4*t4*T(0.5);
//  c2_2315_3 = T(0.);
  const TreeValue c2_tree = -T(3.)*T(0.25);
  const TreeValue t13 = T(2.)*x2;
  const TreeValue t14 = T(4.)*t5;
  const TreeValue t15 = T(6.)*x5;
  const TreeValue t16 = T(4.)*x4;
  const TreeValue t17 = x2*x2;
  t2 = t2 + t5;
  t2 = T(1.)/t2;
  const TreeValue rat = (-t10*t12*t4*T(0.5) - x3*x3*(t1*t8*(t13*(-T(3.)*x5 + (T(1.) + T(2.)*x4)*x4)
    + t14*t7) + t17*t3*t3*(T(6.)*t17*x4 + (x2*(t16 + t15) + t14)*x3) + x1*x2*t3*(T(10.)*t17*x4*x4
    + (t13*(-t15 + (-T(3.) + T(5.)*x5 + t16)*x4) + T(8.)*t5*t7)*x3))*t6*t6/(x4*T(12.)))*t9*t11*t2;
  EpsTriplet<T> amp, camp;
  amp += c1234*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
//  amp += c124*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c134*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
//  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  amp += c245*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  amp += c2_2315_0*(njetan->L0(s23,s15));
  amp += c2_2315_1*(njetan->L1(s23,s15));
  amp += c2_2315_2*(njetan->L2hat(s23,s15));
//  amp += c2_2315_3*(njetan->L3hat(s23,s15));
  amp += c2_tree*(njetan->I2(s23, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  amp += rat;

  camp += conj(c1234)*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
//  camp += conj(c124)*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c134)*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  camp += conj(c245)*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  camp += conj(c2_2315_0)*(njetan->L0(s23,s15));
  camp += conj(c2_2315_1)*(njetan->L1(s23,s15));
  camp += conj(c2_2315_2)*(njetan->L2hat(s23,s15));
//  camp += conj(c2_2315_3)*(njetan->L3hat(s23,s15));
  camp += conj(c2_tree)*(njetan->I2(s23, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//{{{ 30
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL230(const int* p) /* -11111 */
{
  setxi(p);
  setsij(p);
  setaij(p);
  const TreeValue phase = -i_/(a23*a24*a35*a45*b13);
  const TreeValue t1 = T(3.)*x2;
  const TreeValue t2 = -T(1.)/T(6.);
  const TreeValue rat = t2*(x1 + x2)*(x2 + x3)*(t1*x1*x4 + x3*(t1 + T(2.)*x3)*(x4 - x5))*(x1 + x2*x5)/(x1*x2*x3);
  EpsTriplet<T> amp;
  amp += rat;
  const LoopResult<T> res = {phase*amp, conj(phase*amp)};
  return res;
}
//}}}
//}}}

//{{{ L3
//{{{ 1
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL31(const int* p) /* 1-1-1-1-1 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*hAL330(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 3
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL33(const int* p) /* 11-1-1-1 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*hAL328(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 5
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL35(const int* p) /* 1-11-1-1 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*hAL326(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 7
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL37(const int* p) /* 111-1-1 */
{
  const int nord[] = {p[3],p[2],p[1],p[0],p[4]};
  return hAL314(nord);
}
//}}}
//{{{ 8
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL38(const int* p) /* -1-1-11-1 */
{
  const int nord[] = {p[3],p[2],p[1],p[0],p[4]};
  return hAL31(nord);
}
//}}}
//{{{ 10
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL310(const int* p) /* -11-11-1 */
{
  const int nord[] = {p[3],p[2],p[1],p[0],p[4]};
  return hAL35(nord);
}
//}}}
//{{{ 12
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL312(const int* p) /* -1-111-1 */
{
  const int nord[] = {p[3],p[2],p[1],p[0],p[4]};
  return hAL33(nord);
}
//}}}
//{{{ 14
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL314(const int* p) /* -1111-1 */
{
  setxi(p);
  setsij(p);
  setaij(p);

  const TreeValue phase = -i_*a15*a15/(a12*a23*a34);
  TreeValue t1 = -T(1.) + x5;
  const TreeValue t2 = x1*T(0.5);
  const TreeValue t3 = x3*t1;
  const TreeValue c1235 = -t3*t2;
  TreeValue t4 = -T(1.) + x4;
  const TreeValue t5 = x3*t4 + x2*x4;
  const TreeValue t6 = x4 - x5;
  TreeValue t7 = x1*t5;
  const TreeValue t8 = x2*x3;
  const TreeValue c1345 = (t7*T(0.5) + t8*t6*T(0.5))*t6;
  const TreeValue c123 = -t2;
  const TreeValue c125 = x3*(T(1.) - x5)*T(0.5);
  const TreeValue t9 = -t6;
  const TreeValue c134 = x1*T(0.5) + x2*t9*T(0.5);
  const TreeValue c145 = x2*t6*T(0.5);
  const TreeValue t10 = T(1.)/x2;
  const TreeValue c235 = (t8*t4*T(0.5) + t7*T(0.5))*t10;
  t4 = -x2 - x3;
  const TreeValue c345 = (x1*(t4*x4 + x3)*T(0.5) + t8*t9*T(0.5))*t10;
  const TreeValue c2_4512_0 = T(3.)*T(0.25);
  t7 = T(1.)/x3;
  const TreeValue c2_4512_1 = t4*x4*t7;
  t7 = t7*t7;
  const TreeValue c2_4512_2 = -t4*t4*x4*x4*t7*T(0.5);
//  c2_4512_3 = T(0.);
  const TreeValue c2_tree = T(3.)*T(0.25);
  t1 = T(1.)/t1;
  t4 = T(1.)/t6;
  const TreeValue rat = t2*x4*t5*(t3 + x2*x5)*t10*t7*t4*t1;
  EpsTriplet<T> amp, camp;
  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c134*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  amp += c2_4512_0*(njetan->L0(s45,s12));
  amp += c2_4512_1*(njetan->L1(s45,s12));
  amp += c2_4512_2*(njetan->L2hat(s45,s12));
//  amp += c2_4512_3*(njetan->L3hat(s45,s12));
  amp += c2_tree*(njetan->I2(s45, T(0.), T(0.))+njetan->I2(s12, T(0.), T(0.)));
  amp += rat;

  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c134)*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  camp += conj(c2_4512_0)*(njetan->L0(s45,s12));
  camp += conj(c2_4512_1)*(njetan->L1(s45,s12));
  camp += conj(c2_4512_2)*(njetan->L2hat(s45,s12));
//  camp += conj(c2_4512_3)*(njetan->L3hat(s45,s12));
  camp += conj(c2_tree)*(njetan->I2(s45, T(0.), T(0.))+njetan->I2(s12, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//{{{ 17
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL317(const int* p) /* 1-1-1-11 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*hAL314(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 19
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL319(const int* p) /* 11-1-11 */
{
  const int nord[] = {p[3],p[2],p[1],p[0],p[4]};
  return hAL328(nord);
}
//}}}
//{{{ 21
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL321(const int* p) /* 1-11-11 */
{
  const int nord[] = {p[3],p[2],p[1],p[0],p[4]};
  return hAL326(nord);
}
//}}}
//{{{ 23
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL323(const int* p) /* 111-11 */
{
  const int nord[] = {p[3],p[2],p[1],p[0],p[4]};
  return hAL330(nord);
}
//}}}
//{{{ 24
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL324(const int* p) /* -1-1-111 */
{
  const int nord[] = {p[3],p[2],p[1],p[0],p[4]};
  return hAL317(nord);
}
//}}}
//{{{ 26
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL326(const int* p) /* -11-111 */
{
  setxi(p);
  setsij(p);
  setaij(p);

  const TreeValue phase = i_*pow(a13, 3)/(a12*a15*a23*a45);
//  c1234 = T(0.);
  TreeValue t1 = -T(1.) + x5;
  const TreeValue t2 = x2 + x3;
  TreeValue t3 = x1*t2;
  const TreeValue t4 = x2*x3;
  TreeValue t5 = t4 + t3;
  t5 = T(1.)/t5;
  TreeValue t6 = t5*t5;
  const TreeValue t7 = t5*t6;
  const TreeValue t8 = x3*x3;
  const TreeValue t9 = t8*t8;
  const TreeValue t10 = x3*t8;
  TreeValue t11 = x2*x2;
  const TreeValue t12 = t11*t11;
  TreeValue t13 = t11*t12;
  const TreeValue t14 = x2*t11;
  const TreeValue t15 = x1*T(0.5);
  TreeValue t16 = t15*t14;
  const TreeValue c1235 = -t16*t9*t1*t7;
  TreeValue t17 = x4 - x5;
  TreeValue t18 = t4*T(0.5);
  const TreeValue c1245 = -t18*t17*t1;
//  c1345 = T(0.);
  const TreeValue t19 = -T(1.) + x4;
  TreeValue t20 = x2*x4;
  const TreeValue t21 = x3*t19 + t20;
  const TreeValue t22 = t4*t17;
  TreeValue t23 = x1*t21;
  TreeValue t24 = t23 + t22;
  TreeValue t25 = x1 + x2;
  const TreeValue t26 = T(1.)/t25;
  TreeValue t27 = t26*t26;
  TreeValue t28 = t26*t27;
  const TreeValue c2345 = t14*x4*t24*t28*T(0.5);
  const TreeValue c123 = -t16*t10*t7;
  TreeValue t29 = x2*T(0.5);
  const TreeValue c124 = t29*x5;
  const TreeValue t30 = t2*t2;
  const TreeValue t31 = x1*x1;
  const TreeValue t32 = x1*t31;
  const TreeValue t33 = T(3.)*t11;
  const TreeValue t34 = t33*t8;
  const TreeValue t35 = t3*t4;
  const TreeValue t36 = t31*t30;
  const TreeValue t37 = x3*t1;
  const TreeValue c125 = -t37*t15*t2*(t34 + T(3.)*t35 + t36)*t7;
//  c134 = T(0.);
  const TreeValue t38 = t11*T(0.5);
  const TreeValue c135 = t38*t10*(t3*t19 + t22)*t7;
  const TreeValue c145 = t29*t17;
  const TreeValue c234 = -t12*x4*t28*T(0.5);
  t29 = T(3.)*t4;
  const TreeValue c235 = -t16*(t34 + t29*x1*(x2 + T(2.)*x3) + t31*(t11 + t29 + T(3.)*t8))*(t4*t19 + t23)*t28*t7;
  t16 = x1*x2;
  const TreeValue c245 = t15*(t31 + T(3.)*t16 + t33)*(t20 + t37)*t28;
  const TreeValue c345 = -t38*t24*t28;
//  c2_2315_0 = T(0.);
  t20 = T(1.)/x3;
  t1 = T(1.)/t1;
  const TreeValue c2_2315_1 = t23*(x1 + T(2.)*x2)*t27*t20*t1;
  t23 = t21*t21;
  const TreeValue c2_2315_2 = t15*t23*t26*t20*t20*t1*t1;
//  c2_2315_3 = T(0.);
  const TreeValue c2_1534_0 = (-T(3.)*T(0.25)*t14 + (t11*(T(1.) + T(10.)*t11*t5)
    + (x2*(T(7.) - T(16.)*t12*t6) + (T(3.) + T(6.)*t13*t7)*x1)*x1)*x1*T(0.25))*t28;
  t17 = (-x1 + x2*t17)*x3 + t3*x4;
  t17 = T(1.)/t17;
  const TreeValue c2_1534_1 = -t31*x2*t12*x3*(t16 + T(3.)*t25*x3)*t27*t7*t17;
  t16 = t17*t17;
  const TreeValue c2_1534_2 = t32*t13*t8*t26*t7*t16*T(0.5);
//  c2_1534_3 = T(0.);
//  c2_1234_0 = T(0.);
  const TreeValue c2_1234_1 = t35*(T(2.)*t4 + t3)*t19*t6*t17;
  t3 = t19*t19;
  t6 = t3*t19;
  const TreeValue c2_1234_2 = -t36*t18*t3*t5*t16;
//  c2_1234_3 = T(0.);
  const TreeValue c2_tree = T(3.)*T(0.25);
  t3 = T(2.)*x5;
  t5 = T(2.)*x4;
  t11 = t11*x4;
  t13 = T(6.)*x4;
  t16 = T(3.)*x5;
  t17 = t11*x3;
  t18 = x2*t8*t19;
  t25 = t14*x4*x4;
  t27 = T(4.)*x4;
  t28 = T(12.)*x4;
  t29 = T(4.)*t10*t6;
  t24 = T(1.)/t24;
  const TreeValue rat = t15*(t31*t31*t2*t30*t21*t23 + t12*t9*(t8*t6 + t5*t22*t19 + t11*(x5
    + (T(1.) - t3 + x4)*x4 - T(1.))) + t34*t31*t2*(T(2.)*t10*t6 + t25*(t5 - x5) + t18*(T(1.)
    + (-T(5.) - t3 + t13)*x4) + t17*(t3 + (-T(5.) - t16 + t13)*x4)) + t4*t32*t30*(t29 + t25*(t27 - x5)
    + t18*(T(1.) + (-T(11.) - t3 + t28)*x4) + t17*(t3 + (-T(11.) - t16 + t28)*x4))
    + x1*t14*t10*(t11*(x2*(t3 + (T(1.) - T(4.)*x5 + t27)*x4 - T(2.)) + x3*(-T(1.) + T(7.)*x5
    + (-T(8.) - T(10.)*x5 + t28)*x4)) + t29 + T(3.)*t18*(T(1.) + (-T(3.) - t3 + t27)*x4)))/x2*t26*t20*t7/x4*t24*t1;
  EpsTriplet<T> amp, camp;
//  amp += c1234*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  amp += c124*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c134*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  amp += c245*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
//  amp += c2_2315_0*(njetan->L0(s23,s15));
  amp += c2_2315_1*(njetan->L1(s23,s15));
  amp += c2_2315_2*(njetan->L2hat(s23,s15));
//  amp += c2_2315_3*(njetan->L3hat(s23,s15));
  amp += c2_1534_0*(njetan->L0(s15,s34));
  amp += c2_1534_1*(njetan->L1(s15,s34));
  amp += c2_1534_2*(njetan->L2hat(s15,s34));
//  amp += c2_1534_3*(njetan->L3hat(s15,s34));
//  amp += c2_1234_0*(njetan->L0(s12,s34));
  amp += c2_1234_1*(njetan->L1(s12,s34));
  amp += c2_1234_2*(njetan->L2hat(s12,s34));
//  amp += c2_1234_3*(njetan->L3hat(s12,s34));
  amp += c2_tree*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  amp += rat;

//  camp += conj(c1234)*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  camp += conj(c124)*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c134)*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  camp += conj(c245)*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
//  camp += conj(c2_2315_0)*(njetan->L0(s23,s15));
  camp += conj(c2_2315_1)*(njetan->L1(s23,s15));
  camp += conj(c2_2315_2)*(njetan->L2hat(s23,s15));
//  camp += conj(c2_2315_3)*(njetan->L3hat(s23,s15));
  camp += conj(c2_1534_0)*(njetan->L0(s15,s34));
  camp += conj(c2_1534_1)*(njetan->L1(s15,s34));
  camp += conj(c2_1534_2)*(njetan->L2hat(s15,s34));
//  camp += conj(c2_1534_3)*(njetan->L3hat(s15,s34));
//  camp += conj(c2_1234_0)*(njetan->L0(s12,s34));
  camp += conj(c2_1234_1)*(njetan->L1(s12,s34));
  camp += conj(c2_1234_2)*(njetan->L2hat(s12,s34));
//  camp += conj(c2_1234_3)*(njetan->L3hat(s12,s34));
  camp += conj(c2_tree)*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//{{{ 28
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL328(const int* p) /* -1-1111 */
{
  setxi(p);
  setsij(p);
  setaij(p);

  const TreeValue phase = i_*a12*a12*a24/(a15*a23*a34*a45);
  TreeValue t1 = T(2.)*x1 + T(2.)*x2;
  TreeValue t2 = T(1.)/t1;
  TreeValue t3 = x2*x2;
  const TreeValue c1234 = x1*t3*x4*t2;
//  c1235 = T(0.);
  const TreeValue t4 = x4 - x5;
  TreeValue t5 = -T(1.) + x5;
  TreeValue t6 = x2*x3;
  TreeValue t7 = t6*T(0.5);
  const TreeValue c1245 = -t7*t4*t5;
  const TreeValue t8 = -T(1.) + x4;
  TreeValue t9 = x2*x4;
  TreeValue t10 = x3*t8 + t9;
  TreeValue t11 = t6*t4;
  TreeValue t12 = x1*t10 + t11;
  t1 = t1*T(0.5);
  TreeValue t13 = x2 + x3;
  const TreeValue t14 = T(1.)/t1;
  TreeValue t15 = T(1.)/t13;
  const TreeValue c1345 = t3*t12*t4*t14*t15*T(0.5);
//  c2345 = T(0.);
  TreeValue t16 = x1*x2*t2;
  const TreeValue c123 = -t16;
  const TreeValue c124 = t16*x5;
  t16 = x3*t5;
  const TreeValue c125 = -t16*T(0.5);
  const TreeValue c134 = t7*(-x1 + x2*t4)*t14*t15;
  t7 = x1*t13;
  t13 = x2*T(0.5);
  const TreeValue c135 = -t13*(t7*t8 + t11)*t14*t15;
  const TreeValue c145 = t13*(t6 + t7)*t4*t14*t15;
  const TreeValue c234 = -t3*x4*t2;
//  c235 = T(0.);
  const TreeValue c245 = t9*T(0.5) + t16*T(0.5);
  const TreeValue c345 = -t13*t12*t14*t15;
  t2 = x1*x1;
  t7 = pow(x1, -2);
  const TreeValue c2_2315_0 = (-T(3.)*T(0.25)*t2 + (-T(8.)*x1 + T(4.)*x2)*x2*T(0.125))*t7;
  t9 = T(1.)/x3;
  t5 = T(1.)/t5;
  const TreeValue c2_2315_1 = -t1*(-T(2.)*x1 + T(2.)*x2)*t10*t7*t9*t5*T(0.5);
  t10 = t10*t10;
  const TreeValue c2_2315_2 = t1*t1*t10*t7*t9*t9*t5*t5*T(0.5);
//  c2_2315_3 = T(0.);
  const TreeValue c2_tree = T(3.)*T(0.25);
  t1 = T(2.)*x4;
  t11 = x4*x4;
  t12 = x3*x3;
  t6 = t6*x4;
  t13 = t6*(-T(1.) + t1 - x5);
  t3 = t3*t11;
  t15 = T(2.) + x5;
  t16 = T(3.)*x5;
  const TreeValue t17 = T(3.)*t3;
  const TreeValue rat = (x1*t2*t10*T(0.5) + (t2*(t17 + T(2.)*t6*(-T(2.) + T(3.)*x4 - x5)
    + t12*t8*(-t16 + x4*t15)) + (x1*(t17 + T(3.)*t13 + t12*(t11*(T(1.) + T(2.)*x5) + (-t1*t15 + t16)*x5))
    + (t3 + t13 + t12*t4*t4*x5)*x2)*x2)*x2*T(0.5))*t7/x2*t14*t9/x4*t5;
  EpsTriplet<T> amp, camp;
  amp += c1234*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  amp += c124*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c134*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  amp += c245*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  amp += c2_2315_0*(njetan->L0(s23,s15));
  amp += c2_2315_1*(njetan->L1(s23,s15));
  amp += c2_2315_2*(njetan->L2hat(s23,s15));
//  amp += c2_2315_3*(njetan->L3hat(s23,s15));
  amp += c2_tree*(njetan->I2(s23, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  amp += rat;

  camp += conj(c1234)*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  camp += conj(c124)*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c134)*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  camp += conj(c245)*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  camp += conj(c2_2315_0)*(njetan->L0(s23,s15));
  camp += conj(c2_2315_1)*(njetan->L1(s23,s15));
  camp += conj(c2_2315_2)*(njetan->L2hat(s23,s15));
//  camp += conj(c2_2315_3)*(njetan->L3hat(s23,s15));
  camp += conj(c2_tree)*(njetan->I2(s23, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//{{{ 30
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL330(const int* p) /* -11111 */
{
  setxi(p);
  setsij(p);
  setaij(p);
  const TreeValue phase = i_/(a23*a25*a34*a45*b14);
  const TreeValue t1 = -T(1.)*T(0.5);
  const TreeValue rat = t1*(x2*x3 + x1*(x2 + x3))*(x1*x4 + x3*(x4 - x5))*(x3*(-T(1.) + x5) + x2*x5)/(x1*x3);
  EpsTriplet<T> amp;
  amp += rat;
  const LoopResult<T> res = {phase*amp, conj(phase*amp)};
  return res;
}
//}}}
//}}}

//{{{ L4
//{{{ 1
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL41(const int* p) /* 1-1-1-1-1 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*hAL430(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 3
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL43(const int* p) /* 11-1-1-1 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*hAL428(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 5
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL45(const int* p) /* 1-11-1-1 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*hAL426(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 7
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL47(const int* p) /* 111-1-1 */
{
  const int nord[] = {p[4],p[3],p[2],p[1],p[0]};
  return hAL428(nord);
}
//}}}
//{{{ 9
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL49(const int* p) /* 1-1-11-1 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*hAL422(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 11
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL411(const int* p) /* 11-11-1 */
{
  const int nord[] = {p[4],p[3],p[2],p[1],p[0]};
  return hAL426(nord);
}
//}}}
//{{{ 13
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL413(const int* p) /* 1-111-1 */
{
  const int nord[] = {p[4],p[3],p[2],p[1],p[0]};
  return hAL422(nord);
}
//}}}
//{{{ 15
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL415(const int* p) /* 1111-1 */
{
  const int nord[] = {p[4],p[3],p[2],p[1],p[0]};
  return hAL430(nord);
}
//}}}
//{{{ 16
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL416(const int* p) /* -1-1-1-11 */
{
  const int nord[] = {p[4],p[3],p[2],p[1],p[0]};
  return hAL41(nord);
}
//}}}
//{{{ 18
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL418(const int* p) /* -11-1-11 */
{
  const int nord[] = {p[4],p[3],p[2],p[1],p[0]};
  return hAL49(nord);
}
//}}}
//{{{ 20
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL420(const int* p) /* -1-11-11 */
{
  const int nord[] = {p[4],p[3],p[2],p[1],p[0]};
  return hAL45(nord);
}
//}}}
//{{{ 22
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL422(const int* p) /* -111-11 */
{
  setxi(p);
  setsij(p);
  setaij(p);

  const TreeValue phase = i_*pow(a14, 3)/(a12*a15*a23*a34);
  TreeValue t1 = x1 + x2;
  TreeValue t2 = -T(1.) + x5;
  const TreeValue t3 = x2 + x3;
  const TreeValue t4 = x2*x3;
  TreeValue t5 = x1*t3;
  TreeValue t6 = t4 + t5;
  t6 = T(1.)/t6;
  const TreeValue t7 = x3*x3;
  const TreeValue t8 = t7*t7;
  const TreeValue t9 = x3*t7;
  TreeValue t10 = t1*t1;
  TreeValue t11 = t1*t10;
  TreeValue t12 = t6*t6;
  const TreeValue t13 = t6*t12;
  TreeValue t14 = x1*t11*T(0.5);
  const TreeValue c1235 = -t14*t8*t2*t13;
  const TreeValue t15 = -T(1.) + x4;
  TreeValue t16 = x3*t15;
  const TreeValue t17 = x4 - x5;
  TreeValue t18 = x1*(t16 + x2*x4);
  TreeValue t19 = t4*t17;
  const TreeValue t20 = t18 + t19;
  const TreeValue t21 = T(1.)/t3;
  const TreeValue t22 = t21*t21;
  TreeValue t23 = t21*t22;
  TreeValue t24 = t9*T(0.5);
  TreeValue t25 = t24*t20;
  const TreeValue c1345 = t25*t17*t23;
  const TreeValue c123 = -t14*t9*t13;
  t14 = T(2.)*x2;
  const TreeValue t26 = x2*x2;
  const TreeValue t27 = t26*t26;
  const TreeValue t28 = x2*t26;
  const TreeValue t29 = x1*x1;
  const TreeValue t30 = T(3.)*t4;
  const TreeValue t31 = T(3.)*t7;
  const TreeValue t32 = x3*T(0.5);
  const TreeValue c125 = -t32*(t28*t9 + t31*x1*t26*(t14 + x3) + t30*t29*(T(2.)*t26 + (T(4.)*x2
    + x3)*x3) + x1*t29*(T(2.)*t28 + (T(6.)*t26 + (T(6.)*x2 + x3)*x3)*x3))*t2*t13;
  const TreeValue c134 = t24*(x1 - x2*t17)*t23;
  const TreeValue t33 = T(3.)*x2;
  const TreeValue t34 = t3*t3;
  TreeValue t35 = t24*x2;
  const TreeValue c135 = t35*(T(3.)*t29*t34 + t33*x1*(t26 + (t33 + T(2.)*x3)*x3) + t26*(t26 + t30
    + t31))*(t5*t15 + t19)*t23*t13;
  const TreeValue c145 = -t35*t17*t23;
  t19 = T(1.)/x2;
  t24 = t24*t11;
  const TreeValue c235 = t24*(t4*t15 + t18)*t19*t13;
  const TreeValue c345 = -t25*t19*t23;
  t18 = T(3.)*x3;
  t23 = x2 + t18;
  const TreeValue c2_4512_0 = t32*t23*t22;
  t25 = t7*x4;
  const TreeValue c2_4512_1 = -t25*t22;
  t35 = T(2.)*t3;
  t35 = T(1.)/t35;
  const TreeValue t36 = x4*x4;
  const TreeValue c2_4512_2 = -x3*t36*t35;
//  c2_4512_3 = T(0.);
  t35 = t29*t26;
  const TreeValue c2_1534_0 = T(3.)*T(0.5)*x1*x2*(t35 + t30*x1*t1 + t31*t10)*t13;
  t1 = (-x1 + x2*t17)*x3 + t5*x4;
  t1 = T(1.)/t1;
  const TreeValue c2_1534_1 = t14*x1*t11*t9*t13*t1;
  t10 = t1*t1;
  const TreeValue c2_1534_2 = -t35*t24*t13*t10;
//  c2_1534_3 = T(0.);
  const TreeValue c2_1234_0 = T(3.)*T(0.25) - x2*(t33 + T(5.)*x3)*t22*T(0.5);
  const TreeValue c2_1234_1 = t16*x1*t28*(t5*t23 + t4*(t14 + t18))*t22*t12*t1;
  t1 = t15*t15;
  const TreeValue c2_1234_2 = -t32*t29*t27*t1*t21*t6*t10;
//  c2_1234_3 = T(0.);
  const TreeValue c2_tree = T(3.)*T(0.25);
  t5 = T(2.)*x4;
  t6 = T(3.)*x5;
  t10 = t16*x4;
  t11 = T(4.)*x4;
  t12 = T(2.)*x5;
  t2 = t2*x5;
  t14 = T(3.)*x4;
  t16 = T(1.)/t20;
  t18 = T(1.)/t17;
  const TreeValue rat = (x1*t29*t29*x3*t34*t34*t1*x4*T(0.5) + x2*t29*(t29*(t27*x4*t17 + (t28*(-T(1.)
    + t14)*t17 + (t26*(x5 + (T(2.) - x5 + (-T(5.) - x5 + t11)*x4)*x4) + (x2*(x5 + (T(5.) + t12
    + (-T(14.) - T(2.)*x5 + T(8.)*x4)*x4)*x4) + t10*(-T(3.) + t11 - x5))*x3)*x3)*x3)
    + t26*t7*(t25*t15*(-T(1.) + t11 - t6) + T(3.)*t26*(t2 + (T(1.) - t12 + x4)*x4) + t30*(t15*x4
    + (T(1.) - t5 + x5)*x5)) + t4*x1*(t28*(t2 + (T(1.) - T(4.)*x5 + t14)*x4) + (t26*(T(5.)*t36 + (-T(7.)*x4
    + t12)*x5) + (x2*(x5*(T(3.) + x5) + (x5 + (-T(8.) - t6 + T(6.)*x4)*x4)*x4) + T(3.)*t10*(-T(1.) + t5
    - x5))*x3)*x3))*t3*T(0.5) + x1*t27*t9*(t26*(T(1.) + t5 - t6) + (x2*(-T(1.) + T(3.)*t17) + t10)*x3)*t17*T(0.5)
    + t26*t27*t8*t17*t17*T(0.5))*t19*t21*t13*t16*t18;
  EpsTriplet<T> amp, camp;
  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c134*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  amp += c2_4512_0*(njetan->L0(s45,s12));
  amp += c2_4512_1*(njetan->L1(s45,s12));
  amp += c2_4512_2*(njetan->L2hat(s45,s12));
//  amp += c2_4512_3*(njetan->L3hat(s45,s12));
  amp += c2_1534_0*(njetan->L0(s15,s34));
  amp += c2_1534_1*(njetan->L1(s15,s34));
  amp += c2_1534_2*(njetan->L2hat(s15,s34));
//  amp += c2_1534_3*(njetan->L3hat(s15,s34));
  amp += c2_1234_0*(njetan->L0(s12,s34));
  amp += c2_1234_1*(njetan->L1(s12,s34));
  amp += c2_1234_2*(njetan->L2hat(s12,s34));
//  amp += c2_1234_3*(njetan->L3hat(s12,s34));
  amp += c2_tree*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s12, T(0.), T(0.)));
  amp += rat;

  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c134)*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  camp += conj(c2_4512_0)*(njetan->L0(s45,s12));
  camp += conj(c2_4512_1)*(njetan->L1(s45,s12));
  camp += conj(c2_4512_2)*(njetan->L2hat(s45,s12));
//  camp += conj(c2_4512_3)*(njetan->L3hat(s45,s12));
  camp += conj(c2_1534_0)*(njetan->L0(s15,s34));
  camp += conj(c2_1534_1)*(njetan->L1(s15,s34));
  camp += conj(c2_1534_2)*(njetan->L2hat(s15,s34));
//  camp += conj(c2_1534_3)*(njetan->L3hat(s15,s34));
  camp += conj(c2_1234_0)*(njetan->L0(s12,s34));
  camp += conj(c2_1234_1)*(njetan->L1(s12,s34));
  camp += conj(c2_1234_2)*(njetan->L2hat(s12,s34));
//  camp += conj(c2_1234_3)*(njetan->L3hat(s12,s34));
  camp += conj(c2_tree)*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s12, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//{{{ 24
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL424(const int* p) /* -1-1-111 */
{
  const int nord[] = {p[4],p[3],p[2],p[1],p[0]};
  return hAL43(nord);
}
//}}}
//{{{ 26
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL426(const int* p) /* -11-111 */
{
  setxi(p);
  setsij(p);
  setaij(p);

  const TreeValue phase = i_*pow(a13, 3)*a35/(a12*a15*a23*a34*a45);
  TreeValue t1 = -T(1.) + x5;
  TreeValue t2 = x2 + x3;
  TreeValue t3 = x1*t2;
  const TreeValue t4 = x2*x3;
  TreeValue t5 = t4 + t3;
  t5 = T(1.)/t5;
  TreeValue t6 = t5*t5;
  const TreeValue t7 = t5*t6;
  const TreeValue t8 = x3*x3;
  const TreeValue t9 = t8*t8;
  const TreeValue t10 = x3*t8;
  TreeValue t11 = x2*x2;
  const TreeValue t12 = t11*t11;
  TreeValue t13 = x2*t11;
  TreeValue t14 = x2*t12;
  TreeValue t15 = x1*T(0.5);
  TreeValue t16 = t15*t13;
  const TreeValue c1235 = -t16*t9*t1*t7;
  const TreeValue t17 = x4 - x5;
  TreeValue t18 = T(2.)*t2;
  t18 = T(1.)/t18;
  TreeValue t19 = x2*t17;
  const TreeValue c1245 = -t19*t8*t1*t18;
  TreeValue t20 = -T(1.) + x4;
  const TreeValue t21 = x2*x4;
  const TreeValue t22 = x3*t20 + t21;
  TreeValue t23 = x1*t22;
  TreeValue t24 = t19*x3;
  const TreeValue t25 = t23 + t24;
  TreeValue t26 = x1 + x2;
  const TreeValue t27 = T(1.)/t2;
  t26 = T(1.)/t26;
  TreeValue t28 = t26*t26;
  TreeValue t29 = t26*t28;
  TreeValue t30 = t13*x3*T(0.5);
  const TreeValue c2345 = t30*x4*t25*t29*t27;
  const TreeValue c123 = -t16*t10*t7;
  const TreeValue c124 = t4*x5*t18;
  t16 = T(2.)*x2;
  TreeValue t31 = t16 + x3;
  TreeValue t32 = T(3.)*x2;
  const TreeValue t33 = T(2.)*t11;
  const TreeValue t34 = t2*t2;
  const TreeValue t35 = t2*t34;
  const TreeValue t36 = x1*x1;
  const TreeValue t37 = t36*t36;
  const TreeValue t38 = x1*t37;
  const TreeValue t39 = x1*t36;
  const TreeValue t40 = T(3.)*t11;
  const TreeValue t41 = T(3.)*t4*t36;
  const TreeValue t42 = x3*t1;
  const TreeValue c125 = -t42*(t12*t10 + t41*t34*t31 + t39*t35*t31 + t40*x1*t8*(t33 + (t32 + x3)*x3))*t27*t7*T(0.5);
  t31 = t3*t20;
  const TreeValue t43 = t11*T(0.5);
  const TreeValue c135 = t43*t10*(t31 + t24)*t7;
  const TreeValue c145 = -t24*t18;
  const TreeValue c234 = -t12*x3*x4*t29*t27*T(0.5);
  const TreeValue c235 = t30*(t13*t8 - t41*t2 - t39*(t11 + (t32 + T(2.)*x3)*x3))*(t4*t20 + t23)*t29*t27*t7;
  const TreeValue c245 = t15*(t36 + t32*x1 + t40)*x3*(t21 + t42)*t29*t27;
  const TreeValue c345 = -t43*x3*t25*t29*t27;
//  c2_2315_0 = T(0.);
  t18 = T(1.)/t1;
  const TreeValue c2_2315_1 = t23*(x1 + t16)*t28*t27*t18;
  const TreeValue c2_2315_2 = t15*t22*t22*t26/x3*t27*t18*t18;
//  c2_2315_3 = T(0.);
  t15 = T(6.)*x3;
  t16 = T(1.)*T(0.25);
  const TreeValue c2_1534_0 = t16*(x1*t12*(T(9.)*x2 - T(5.)*x3)*t8 - T(3.)*t14*t10 + T(3.)*t38*t35
    + t37*x2*t34*(T(6.)*x2 +
 T(13.)*x3) + t40*t39*t2*(t11 + T(6.)*t4 + T(6.)*t8) + t36*t13*x3*(T(9.)*t11 + (T(22.)*x2 + t15)*x3))*t28*t7;
  t16 = (-x1 + t19)*x3 + t3*x4;
  t16 = T(1.)/t16;
  const TreeValue c2_1534_1 = x1*t14*t8*(t4*x1 + t33*x3 - t36*t2)*t28*t27*t7*t16;
  t14 = t16*t16;
  t19 = t36*T(0.5);
  const TreeValue c2_1534_2 = -t19*t13*t12*t10*t26*t27*t7*t14;
//  c2_1534_3 = T(0.);
//  c2_1234_0 = T(0.);
  const TreeValue c2_1234_1 = t31*t4*(T(2.)*t4 + t3)*t6*t16;
  t3 = t20*t20;
  t6 = t3*t20;
  const TreeValue c2_1234_2 = -t19*t4*t34*t3*t5*t14;
//  c2_1234_3 = T(0.);
  const TreeValue c2_tree = T(3.)*T(0.25);
  t5 = T(2.)*x4;
  t11 = t11*x4;
  t14 = T(3.)*x5;
  t16 = T(4.)*x5;
  t13 = t13*x4;
  t19 = T(4.)*x4;
  t20 = T(11.)*x5;
  t23 = T(12.)*x4;
  t24 = T(4.)*t6*x3;
  t28 = T(3.)*x4;
  t29 = x4*x4;
  t30 = x5*x5;
  t31 = T(18.)*x4;
  t32 = t37*t34*(t12*t29*t1 + (t13*(x4*(-T(6.) + t19) + (T(1.) + T(5.)*x4 - x5)*x5)
    + (t11*(T(4.) - T(2.)*t30 + (-T(21.) + T(7.)*x5 + t23)*x4) + (x2*(-T(1.) + (T(16.) - (T(1.) + x5)*x5
    + (-T(28.) + t14 + t23)*x4)*x4) + t24)*x3)*x3)*x3);
  t1 = t32 + (t39*x3*t2*(t12*x4*(T(1.) + t28 - x5)*t1 + (t13*(-T(2.) + T(6.)*t29
    + T(8.)*x5 - T(6.)*t30 + x4*(-T(13.) + T(10.)*x5)) + (t11*((-T(35.) + t20 + t31)*x4
    + (T(10.) - T(9.)*x5)*x5 + T(5.)) + (x2*(-T(3.) + (T(24.) - T(4.)*t1*x5 + (-T(43.) + t16
    + t31)*x4)*x4) + t15*t6)*x3)*x3)*x3) + (t36*t8*(t28*t12*(T(1.) + x4 - x5)*t1 + (t13*(x4*(-T(13.)
    + t19) + (T(18.) + T(10.)*x4 - T(12.)*x5)*x5 - T(6.)) + (t11*((-T(29.) + t20 + t23)*x4
    + (T(20.) - T(15.)*x5)*x5 + T(1.)) + (x2*(-T(3.) + (T(16.) + (T(8.) - T(6.)*x5)*x5 + (-T(31.) + t16
    + t23)*x4)*x4) + t24)*x3)*x3)*x3) + (x1*t10*(t10*t6 + t13*(T(1.) + t5 - t14)*t1 + t21*t8*((-T(7.) + t14 + t5)*x4
    + (T(5.) - t16)*x5 + T(1.)) + t11*x3*((-T(6.) + T(5.)*x5 + x4)*x4 + (T(9.) - T(7.)*x5)*x5 - T(2.)))
    + t11*t9*t2*t17*t1)*x2)*x2)*x2;
  t2 = T(1.)/t25;
  const TreeValue rat = (t38*t35*t22*(t8*t3 + t11*(-T(1.) + x4 + x5) + t4*x4*(-T(3.) + t5
    + x5))*T(0.5) + t1*x2*T(0.5))/x2*t26*t27*t7/x4*t2*t18;
  EpsTriplet<T> amp, camp;
  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  amp += c124*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  amp += c245*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
//  amp += c2_2315_0*(njetan->L0(s23,s15));
  amp += c2_2315_1*(njetan->L1(s23,s15));
  amp += c2_2315_2*(njetan->L2hat(s23,s15));
//  amp += c2_2315_3*(njetan->L3hat(s23,s15));
  amp += c2_1534_0*(njetan->L0(s15,s34));
  amp += c2_1534_1*(njetan->L1(s15,s34));
  amp += c2_1534_2*(njetan->L2hat(s15,s34));
//  amp += c2_1534_3*(njetan->L3hat(s15,s34));
//  amp += c2_1234_0*(njetan->L0(s12,s34));
  amp += c2_1234_1*(njetan->L1(s12,s34));
  amp += c2_1234_2*(njetan->L2hat(s12,s34));
//  amp += c2_1234_3*(njetan->L3hat(s12,s34));
  amp += c2_tree*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  amp += rat;

  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  camp += conj(c124)*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  camp += conj(c245)*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
//  camp += conj(c2_2315_0)*(njetan->L0(s23,s15));
  camp += conj(c2_2315_1)*(njetan->L1(s23,s15));
  camp += conj(c2_2315_2)*(njetan->L2hat(s23,s15));
//  camp += conj(c2_2315_3)*(njetan->L3hat(s23,s15));
  camp += conj(c2_1534_0)*(njetan->L0(s15,s34));
  camp += conj(c2_1534_1)*(njetan->L1(s15,s34));
  camp += conj(c2_1534_2)*(njetan->L2hat(s15,s34));
//  camp += conj(c2_1534_3)*(njetan->L3hat(s15,s34));
//  camp += conj(c2_1234_0)*(njetan->L0(s12,s34));
  camp += conj(c2_1234_1)*(njetan->L1(s12,s34));
  camp += conj(c2_1234_2)*(njetan->L2hat(s12,s34));
//  camp += conj(c2_1234_3)*(njetan->L3hat(s12,s34));
  camp += conj(c2_tree)*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//{{{ 28
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL428(const int* p) /* -1-1111 */
{
  setxi(p);
  setsij(p);
  setaij(p);

  const TreeValue phase = i_*a12*a12*a25/(a15*a23*a34*a45);
  TreeValue t1 = x2 + x3;
  TreeValue t2 = x2*x3;
  TreeValue t3 = T(2.)*t2 + T(2.)*x1*t1;
  TreeValue t4 = T(1.)/t3;
  const TreeValue t5 = x2*x2;
  TreeValue t6 = x1*t5*x3;
  const TreeValue c1234 = t6*x4*t4;
  TreeValue t7 = x1 + x2;
  const TreeValue t8 = x4 - x5;
  TreeValue t9 = -T(1.) + x5;
  const TreeValue t10 = x3*x3;
  TreeValue t11 = x2*t8;
  TreeValue t12 = t11*t7;
  const TreeValue c1245 = -t12*t10*t9*t4;
  TreeValue t13 = t2*x1*t4;
  const TreeValue c123 = -t13;
  const TreeValue c124 = t13*x5;
  t13 = x3*t9;
  const TreeValue c125 = -t13*(t2 + x1*(T(2.)*x2 + x3))*t4;
  const TreeValue c134 = t2*(-x1 + t11)*t4;
  const TreeValue c145 = -t12*x3*t4;
  const TreeValue c234 = -t5*x3*x4*t4;
  t11 = x2*x4;
  const TreeValue c245 = t7*x3*(t11 + t13)*t4;
  t4 = x1*x1;
  t12 = x1*t4;
  t3 = t3*T(0.5);
  t3 = T(1.)/t3;
  t13 = pow(x1, -2);
  const TreeValue c2_2315_0 = (-T(7.)*T(0.25)*t2*t4 - t6*T(0.5) + x2*t5*x3*T(0.5) - T(3.)*T(0.25)*t12*t1)*t13*t3;
  t1 = -T(1.) + x4;
  t6 = x3*t1 + t11;
  t9 = T(1.)/t9;
  t11 = t7*t7;
  const TreeValue c2_2315_1 = -t11*(-T(2.)*x1 + T(2.)*x2)*t6*t13*t3*t9*T(0.5);
  const TreeValue c2_2315_2 = t11*t7*t6*t6*t13/x3*t3*t9*t9*T(0.5);
//  c2_2315_3 = T(0.);
  const TreeValue c2_tree = T(3.)*T(0.25);
  t6 = T(2.)*x4;
  t7 = x4*x4;
  t2 = t2*x4;
  t11 = t5*t7;
  const TreeValue t14 = T(2.) + x5;
  const TreeValue t15 = T(3.)*x5;
  const TreeValue t16 = x4*(T(5.) + x5);
  const TreeValue t17 = T(2.)*x5;
  const TreeValue t18 = T(3.)*t11;
  const TreeValue rat = (t12*(t10*t1*t1 + t5*x4*(-T(1.) + x4 + x5) + t2*(-T(3.) + t6 + x5))*T(0.5)
    + (t4*(t18 + t10*t1*(-t15 + x4*t14) + t2*(-T(4.) - t17 + t16)) + (x1*(t18 + t2*(-T(3.) + t16 - t14*x5) + t10*(t7*(T(1.) + t17)
    + (-t6*t14 + t15)*x5)) + (t11 + t2*(-T(1.) + t6 - x5) + t10*t8*t8*x5)*x2)*x2)*x2*T(0.5))*t13/x2*t3/x4*t9;
  EpsTriplet<T> amp, camp;
  amp += c1234*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  amp += c124*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c134*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
  amp += c245*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
  amp += c2_2315_0*(njetan->L0(s23,s15));
  amp += c2_2315_1*(njetan->L1(s23,s15));
  amp += c2_2315_2*(njetan->L2hat(s23,s15));
//  amp += c2_2315_3*(njetan->L3hat(s23,s15));
  amp += c2_tree*(njetan->I2(s23, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  amp += rat;

  camp += conj(c1234)*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  camp += conj(c124)*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c134)*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
  camp += conj(c245)*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
  camp += conj(c2_2315_0)*(njetan->L0(s23,s15));
  camp += conj(c2_2315_1)*(njetan->L1(s23,s15));
  camp += conj(c2_2315_2)*(njetan->L2hat(s23,s15));
//  camp += conj(c2_2315_3)*(njetan->L3hat(s23,s15));
  camp += conj(c2_tree)*(njetan->I2(s23, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//{{{ 30
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAL430(const int* p) /* -11111 */
{
  setxi(p);
  setsij(p);
  setaij(p);

  const TreeValue phase = -i_/(a24*a25*a34*a35*b15);
  const TreeValue t1 = x2 + x3;
  const TreeValue t2 = -T(1.)*T(0.5);
  const TreeValue rat = t2*(x1 + x2)*t1*(x2*x3 + x1*t1)*(x1*x4 + x3*(x4 - x5))*(-T(1.) + x5)/(x1*x2*x2);
  EpsTriplet<T> amp;
  amp += rat;
  const LoopResult<T> res = {phase*amp, conj(phase*amp)};
  return res;
}
//}}}
//}}}

//{{{ f1
//{{{ 1
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAf11(const int* p) /* 1-1-1-1-1 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*hAf130(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 2
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAf12(const int* p) /* -11-1-1-1 */
{
  const int nord[] = {p[1],p[0],p[4],p[3],p[2]};
  return hAf11(nord);
}
//}}}
//{{{ 5
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAf15(const int* p) /* 1-11-1-1 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*hAf126(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 6
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAf16(const int* p) /* -111-1-1 */
{
  const int nord[] = {p[1],p[0],p[4],p[3],p[2]};
  return hAf117(nord);
}
//}}}
//{{{ 9
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAf19(const int* p) /* 1-1-11-1 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*hAf122(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 10
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAf110(const int* p) /* -11-11-1 */
{
  const int nord[] = {p[1],p[0],p[4],p[3],p[2]};
  return hAf19(nord);
}
//}}}
//{{{ 13
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAf113(const int* p) /* 1-111-1 */
{
  const int nord[] = {p[1],p[0],p[4],p[3],p[2]};
  return hAf126(nord);
}
//}}}
//{{{ 14
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAf114(const int* p) /* -1111-1 */
{
  setxi(p);
  setsij(p);
  setaij(p);

  const TreeValue phase = -i_*a15*a15*a25/(a12*a23*a34*a45);
  TreeValue t1 = x2 + x3;
  TreeValue t2 = x2*x3;
  TreeValue t3 = T(3.)*t2 + T(3.)*x1*t1;
  TreeValue t4 = T(1.)/t3;
  const TreeValue c2_4512_0 = T(2.)*t2*t4;
  t2 = t3/T(3.);
  t2 = T(1.)/t2;
  const TreeValue c2_4512_1 = -x2*t1*x4*t2;
  t3 = T(1.)/x3;
  t4 = x4*x4;
  TreeValue t5 = t1*t1;
  const TreeValue c2_4512_2 = x2*t5*t4*t3*t2;
  t3 = t3*t3;
  const TreeValue c2_4512_3 = -T(2.)/T(3.)*x2*t1*t5*x4*t4*t3*t2;
//  c2_tree = T(0.);
  t1 = x4 - x5;
  t4 = -T(1.) + x5;
  t5 = x2*x2;
  const TreeValue t6 = T(1.)/t1;
  const TreeValue t7 = T(1.)/t4;
  const TreeValue rat = x1*x4*(x2*t5*x4*x5 + (t1*t5 + (-T(3.)*x2*x4*t4 + (-T(1.) - T(2.)*x4*t4
    + x5)*x3)*x3)*x3)*t3*t2*t6*t7/T(3.);
  EpsTriplet<T> amp, camp;
  amp += c2_4512_0*(njetan->L0(s45,s12));
  amp += c2_4512_1*(njetan->L1(s45,s12));
  amp += c2_4512_2*(njetan->L2hat(s45,s12));
  amp += c2_4512_3*(njetan->L3hat(s45,s12));
//  amp += c2_tree*(njetan->I2(s45, T(0.), T(0.))+njetan->I2(s12, T(0.), T(0.)));
  amp += rat;

  camp += conj(c2_4512_0)*(njetan->L0(s45,s12));
  camp += conj(c2_4512_1)*(njetan->L1(s45,s12));
  camp += conj(c2_4512_2)*(njetan->L2hat(s45,s12));
  camp += conj(c2_4512_3)*(njetan->L3hat(s45,s12));
//  camp += conj(c2_tree)*(njetan->I2(s45, T(0.), T(0.))+njetan->I2(s12, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//{{{ 17
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAf117(const int* p) /* 1-1-1-11 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*hAf114(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 18
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAf118(const int* p) /* -11-1-11 */
{
  const int nord[] = {p[1],p[0],p[4],p[3],p[2]};
  return hAf15(nord);
}
//}}}
//{{{ 21
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAf121(const int* p) /* 1-11-11 */
{
  const int nord[] = {p[1],p[0],p[4],p[3],p[2]};
  return hAf122(nord);
}
//}}}
//{{{ 22
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAf122(const int* p) /* -111-11 */
{
  setxi(p);
  setsij(p);
  setaij(p);

  const TreeValue phase = -i_*pow(a14, 3)*a24/(a12*a15*a23*a34*a45);
  const TreeValue t1 = x3*x3;
  const TreeValue t2 = x2*x2;
  TreeValue t3 = t2*t2;
  const TreeValue t4 = x2*t2;
  TreeValue t5 = t2 + t1;
  const TreeValue t6 = -T(1.) + x4;
  TreeValue t7 = x2*x4;
  const TreeValue t8 = x4 - x5;
  const TreeValue t9 = x2*t8;
  TreeValue t10 = t9*x3;
  TreeValue t11 = x1*(x3*t6 + t7) + t10;
  TreeValue t12 = x1 + x2;
  TreeValue t13 = x2 + x3;
  t12 = T(1.)/t12;
  const TreeValue t14 = T(1.)/t13;
  const TreeValue t15 = t14*t14;
  TreeValue t16 = t15*t15;
  const TreeValue t17 = t14*t15;
  TreeValue t18 = t2*x3*t5*T(0.5);
  const TreeValue c1345 = -t18*t11*t8*t12*t16;
  const TreeValue c134 = -t18*(x1 - x2*t8)*t12*t16;
  t13 = x1*t13;
  t18 = x2*x3*t5*T(0.5);
  const TreeValue c135 = t18*(t13*t6 + t10)*t12*t16;
  const TreeValue c145 = t4*x3*t5*t8*t12*t16*T(0.5);
  const TreeValue c345 = t18*t11*t12*t16;
  t5 = x2 + T(2.)*x3;
  const TreeValue c2_4512_0 = x2*x3*(T(5.)*t2 + t5*x3)*t12*t17/T(3.);
  t10 = T(2.)*x2;
  t16 = t10 + x3;
  const TreeValue c2_4512_1 = -t7*x3*(T(3.)*t2 + t16*x3)*t12*t17;
  t7 = x4*x4;
  t18 = x2*x3;
  const TreeValue c2_4512_2 = t18*t16*t7*t12*t15;
  const TreeValue c2_4512_3 = -T(2.)/T(3.)*t18*x4*t7*t12*t14;
//  c2_1534_0 = T(0.);
//  c2_1534_1 = T(0.);
//  c2_1534_2 = T(0.);
//  c2_1534_3 = T(0.);
  const TreeValue c2_1234_0 = -t2*(T(2.)*t2 + (x2 + T(5.)*x3)*x3)*t12*t17/T(3.);
  t7 = (-x1 + t9)*x3 + t13*x4;
  t7 = T(1.)/t7;
  const TreeValue c2_1234_1 = x1*t4*(t2 + (t10 + T(3.)*x3)*x3)*t6*t12*t17*t7;
  t10 = t7*t7;
  t13 = t6*t6;
  t16 = x1*x1;
  const TreeValue c2_1234_2 = -t16*t3*t5*t13*t12*t15*t10;
  const TreeValue c2_1234_3 = T(2.)/T(3.)*x1*t16*x2*t3*t13*t6*t12*t14*t7*t10;
//  c2_tree = T(0.);
  t3 = T(2.)*x4;
  t5 = T(3.)*x4;
  t7 = (T(5.) - t5)*x4;
  t10 = T(1.)/t11;
  t11 = T(1.)/t8;
  const TreeValue rat = (-t16*x3*x4*(t2*(t5*t6 + T(1.)) + (x2*(T(2.) + (-T(6.) + T(5.)*x4)*x4)
    + (T(1.) + (-T(3.) + t3)*x4)*x3)*x3)/T(3.) + t9*x1*(t4*(T(2.) + (-T(3.) + x4)*x4)
    + (t2*(T(5.) - t7) + (x2*(-T(3.) + t7) + (T(1.) - t3)*x4*x3)*x3)*x3)/T(3.) + t4*t1*t8*t8)*t12*t15*t10*t11;
  EpsTriplet<T> amp, camp;
  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
  amp += c134*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  amp += c2_4512_0*(njetan->L0(s45,s12));
  amp += c2_4512_1*(njetan->L1(s45,s12));
  amp += c2_4512_2*(njetan->L2hat(s45,s12));
  amp += c2_4512_3*(njetan->L3hat(s45,s12));
//  amp += c2_1534_0*(njetan->L0(s15,s34));
//  amp += c2_1534_1*(njetan->L1(s15,s34));
//  amp += c2_1534_2*(njetan->L2hat(s15,s34));
//  amp += c2_1534_3*(njetan->L3hat(s15,s34));
  amp += c2_1234_0*(njetan->L0(s12,s34));
  amp += c2_1234_1*(njetan->L1(s12,s34));
  amp += c2_1234_2*(njetan->L2hat(s12,s34));
  amp += c2_1234_3*(njetan->L3hat(s12,s34));
//  amp += c2_tree*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s12, T(0.), T(0.)));
  amp += rat;

  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c134)*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  camp += conj(c2_4512_0)*(njetan->L0(s45,s12));
  camp += conj(c2_4512_1)*(njetan->L1(s45,s12));
  camp += conj(c2_4512_2)*(njetan->L2hat(s45,s12));
  camp += conj(c2_4512_3)*(njetan->L3hat(s45,s12));
//  camp += conj(c2_1534_0)*(njetan->L0(s15,s34));
//  camp += conj(c2_1534_1)*(njetan->L1(s15,s34));
//  camp += conj(c2_1534_2)*(njetan->L2hat(s15,s34));
//  camp += conj(c2_1534_3)*(njetan->L3hat(s15,s34));
  camp += conj(c2_1234_0)*(njetan->L0(s12,s34));
  camp += conj(c2_1234_1)*(njetan->L1(s12,s34));
  camp += conj(c2_1234_2)*(njetan->L2hat(s12,s34));
  camp += conj(c2_1234_3)*(njetan->L3hat(s12,s34));
//  camp += conj(c2_tree)*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s12, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//{{{ 25
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAf125(const int* p) /* 1-1-111 */
{
  const int nord[] = {p[1],p[0],p[4],p[3],p[2]};
  return hAf114(nord);
}
//}}}
//{{{ 26
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAf126(const int* p) /* -11-111 */
{
  setxi(p);
  setsij(p);
  setaij(p);

  const TreeValue phase = -i_*pow(a13, 3)/(a12*a15*a34*a45);
//  c2_2315_0 = T(0.);
//  c2_2315_1 = T(0.);
//  c2_2315_2 = T(0.);
//  c2_2315_3 = T(0.);
//  c2_1534_0 = T(0.);
//  c2_1534_1 = T(0.);
//  c2_1534_2 = T(0.);
//  c2_1534_3 = T(0.);
  const TreeValue c2_1234_0 = -T(2.)/T(3.);
  TreeValue t1 = x2 + x3;
  const TreeValue t2 = -T(1.) + x4;
  TreeValue t3 = x2*(x4 - x5);
  TreeValue t4 = x1*t1;
  TreeValue t5 = (-x1 + t3)*x3 + t4*x4;
  t5 = T(1.)/t5;
  const TreeValue c2_1234_1 = t4*t2*t5;
  t4 = t2*t2;
  TreeValue t6 = t1*t1;
  TreeValue t7 = t5*t5;
  const TreeValue t8 = x1*x1;
  const TreeValue c2_1234_2 = -t8*t6*t4*t7;
  const TreeValue c2_1234_3 = T(2.)/T(3.)*x1*t8*t1*t6*t4*t2*t5*t7;
//  c2_tree = T(0.);
  t1 = x3*x3;
  t5 = x2*x2;
  t6 = x3*t2;
  t7 = x2*x4;
  t3 = x1*(t6 + t7) + t3*x3;
  t3 = T(1.)/t3;
  const TreeValue rat = x1*t2*(x3*t1*t4 + x2*t5*(-T(2.) + x4)*x4 + T(3.)*t6*t5*x4
    + T(3.)*t7*t1*t2)/(T(3.)*x2*x2*x4)*t3;
  EpsTriplet<T> amp, camp;
//  amp += c2_2315_0*(njetan->L0(s23,s15));
//  amp += c2_2315_1*(njetan->L1(s23,s15));
//  amp += c2_2315_2*(njetan->L2hat(s23,s15));
//  amp += c2_2315_3*(njetan->L3hat(s23,s15));
//  amp += c2_1534_0*(njetan->L0(s15,s34));
//  amp += c2_1534_1*(njetan->L1(s15,s34));
//  amp += c2_1534_2*(njetan->L2hat(s15,s34));
//  amp += c2_1534_3*(njetan->L3hat(s15,s34));
  amp += c2_1234_0*(njetan->L0(s12,s34));
  amp += c2_1234_1*(njetan->L1(s12,s34));
  amp += c2_1234_2*(njetan->L2hat(s12,s34));
  amp += c2_1234_3*(njetan->L3hat(s12,s34));
//  amp += c2_tree*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  amp += rat;

//  camp += conj(c2_2315_0)*(njetan->L0(s23,s15));
//  camp += conj(c2_2315_1)*(njetan->L1(s23,s15));
//  camp += conj(c2_2315_2)*(njetan->L2hat(s23,s15));
//  camp += conj(c2_2315_3)*(njetan->L3hat(s23,s15));
//  camp += conj(c2_1534_0)*(njetan->L0(s15,s34));
//  camp += conj(c2_1534_1)*(njetan->L1(s15,s34));
//  camp += conj(c2_1534_2)*(njetan->L2hat(s15,s34));
//  camp += conj(c2_1534_3)*(njetan->L3hat(s15,s34));
  camp += conj(c2_1234_0)*(njetan->L0(s12,s34));
  camp += conj(c2_1234_1)*(njetan->L1(s12,s34));
  camp += conj(c2_1234_2)*(njetan->L2hat(s12,s34));
  camp += conj(c2_1234_3)*(njetan->L3hat(s12,s34));
//  camp += conj(c2_tree)*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//{{{ 29
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAf129(const int* p) /* 1-1111 */
{
  const int nord[] = {p[1],p[0],p[4],p[3],p[2]};
  return hAf130(nord);
}
//}}}
//{{{ 30
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAf130(const int* p) /* -11111 */
{
  setxi(p);
  setsij(p);
  setaij(p);

  const TreeValue phase = i_/(a23*a25*a34*a45*b12);
  const TreeValue t1 = x2 + x3;
  const TreeValue t2 = x2*x3;
  const TreeValue t3 = -T(1.) + x4;
  const TreeValue t4 = x4 - x5;
  const TreeValue t5 = -T(1.)/T(3.);
  const TreeValue rat = t5*(t2 + x1*t1)*(x1*(x2*x2*x4 + (-x2*t3*t3 + t4*x3)*x3) + t2*t1*t4)/(x1*x2*x3);
  EpsTriplet<T> amp;
  amp += rat;
  const LoopResult<T> res = {phase*amp, conj(phase*amp)};
  return res;
}
//}}}
//}}}

//{{{ f2
//{{{ 1
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAf21(const int* p) /* 1-1-1-1-1 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*hAf230(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 3
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAf23(const int* p) /* 11-1-1-1 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*hAf228(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 4
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAf24(const int* p) /* -1-11-1-1 */
{
  const int nord[] = {p[2],p[1],p[0],p[4],p[3]};
  return hAf21(nord);
}
//}}}
//{{{ 6
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAf26(const int* p) /* -111-1-1 */
{
  const int nord[] = {p[2],p[1],p[0],p[4],p[3]};
  return hAf23(nord);
}
//}}}
//{{{ 9
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAf29(const int* p) /* 1-1-11-1 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*hAf222(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 11
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAf211(const int* p) /* 11-11-1 */
{
  const int nord[] = {p[2],p[1],p[0],p[4],p[3]};
  return hAf222(nord);
}
//}}}
//{{{ 12
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAf212(const int* p) /* -1-111-1 */
{
  const int nord[] = {p[2],p[1],p[0],p[4],p[3]};
  return hAf217(nord);
}
//}}}
//{{{ 14
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAf214(const int* /*p*/) /* -1111-1 */
{
  return LoopResult<T>();
}
//}}}
//{{{ 17
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAf217(const int* p) /* 1-1-1-11 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*hAf214(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 19
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAf219(const int* p) /* 11-1-11 */
{
  const int nord[] = {p[2],p[1],p[0],p[4],p[3]};
  return hAf214(nord);
}
//}}}
//{{{ 20
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAf220(const int* p) /* -1-11-11 */
{
  const int nord[] = {p[2],p[1],p[0],p[4],p[3]};
  return hAf29(nord);
}
//}}}
//{{{ 22
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAf222(const int* /*p*/) /* -111-11 */
{
  return LoopResult<T>();
}
//}}}
//{{{ 25
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAf225(const int* p) /* 1-1-111 */
{
  const int nord[] = {p[2],p[1],p[0],p[4],p[3]};
  return hAf228(nord);
}
//}}}
//{{{ 27
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAf227(const int* p) /* 11-111 */
{
  const int nord[] = {p[2],p[1],p[0],p[4],p[3]};
  return hAf230(nord);
}
//}}}
//{{{ 28
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAf228(const int* p) /* -1-1111 */
{
  setxi(p);
  setsij(p);
  setaij(p);

  const TreeValue phase = -i_*a12*a12/(a15*a34*a45);
  const TreeValue t1 = -T(1.) + x4;
  const TreeValue t2 = x2*(x4 - x5);
  const TreeValue rat = -x3*(x1*t1 + t2)*(x1*(x3*t1 + x2*x4) + t2*x3)/(x1*x1*x2*x2*x4*T(3.));
  EpsTriplet<T> amp, camp;
  amp += rat;
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//{{{ 30
template <typename T>
LoopResult<T> Amp2q3g_a<T>::hAf230(const int* p) /* -11111 */
{
  setxi(p);
  setsij(p);
  setaij(p);
  const TreeValue phase = -i_/(a23*a24*a35*a45*b13);
  const TreeValue t1 = T(1.)/T(3.);
  const TreeValue rat = t1*(x1 + x2)*x3*(x2 + x3)*(-x4 + x5)*(x1 + x2*x5)/(x1*x2);
  EpsTriplet<T> amp;
  amp += rat;
  const LoopResult<T> res = {phase*amp, conj(phase*amp)};
  return res;
}
//}}}
//}}}

#ifdef USE_SD
  template class Amp2q3g_a<double>;
#endif
#ifdef USE_DD
  template class Amp2q3g_a<dd_real>;
#endif
#ifdef USE_QD
  template class Amp2q3g_a<qd_real>;
#endif
#ifdef USE_VC
  template class Amp2q3g_a<Vc::double_v>;
#endif
