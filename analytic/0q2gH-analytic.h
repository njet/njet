/*
* analytic/0q2gH-analytic.h
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef ANALYTIC_0Q2GH_ANALYTIC_H
#define ANALYTIC_0Q2GH_ANALYTIC_H

#include "../chsums/0q2gH.h"

template <typename T>
class Amp0q2gH_a : public Amp0q2gH<T>
{
    typedef Amp0q2gH<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    typedef TreeValue (Amp0q2gH_a::*HelAmp)(const int* ord);

    Amp0q2gH_a(const T scalefactor);

  protected:
    using BaseClass::mhel;
    using BaseClass::mhelint;

    using BaseClass::njetan;
    using BaseClass::callTree;
    using BaseClass::callLoop;
    using BaseClass::sA;
    using BaseClass::sB;
    using BaseClass::HelicityOrder;

    virtual TreeValue A0(int p0, int p1);

    HelAmp hA0[4];

    TreeValue hA00(const int* ord);
    TreeValue hA03(const int* ord);
};

#endif /* ANALYTIC_0Q2GH_ANALYTIC_H */
