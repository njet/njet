/*
* analytic/0q2gH-analytic.cpp
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>
#include "0q2gH-analytic.h"

template <typename T>
Amp0q2gH_a<T>::Amp0q2gH_a(const T scalefactor)
  : BaseClass(scalefactor), hA0()
{
  njetan = new NJetAnalytic<T>(scalefactor, BaseClass::legsMOM(), -1);
//   BaseClass::clearNG();

  hA0[0] = &Amp0q2gH_a::hA00;
  hA0[3] = &Amp0q2gH_a::hA03;
}

template <typename T>
typename Amp0q2gH_a<T>::TreeValue
Amp0q2gH_a<T>::A0(int p0, int p1)
{
  const int ord[] = {p0, p1, BaseClass::NN};
  const int hel = HelicityOrder(4^mhelint, ord);
  HelAmp const hamp = hA0[hel];
  if (hamp) {
    return i_*callTree(this, hamp, ord);
  } else {
    return TreeValue(0.);
  }
}

template <typename T>
typename Amp0q2gH_a<T>::TreeValue
Amp0q2gH_a<T>::hA00(const int* p)
{
  return -pow(sA(p[0], p[1]), 2);
}
template <typename T>
typename Amp0q2gH_a<T>::TreeValue
Amp0q2gH_a<T>::hA03(const int* p)
{
  return -pow(sB(p[0], p[1]), 2);
}

#ifdef USE_SD
  template class Amp0q2gH_a<double>;
#endif
#ifdef USE_DD
  template class Amp0q2gH_a<dd_real>;
#endif
#ifdef USE_QD
  template class Amp0q2gH_a<qd_real>;
#endif
#ifdef USE_VC
  template class Amp0q2gH_a<Vc::double_v>;
#endif
