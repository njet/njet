/*
* analytic/2q3g-ds-analytic.h
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef ANALYTIC_2Q3G_DS_ANALYTIC_H
#define ANALYTIC_2Q3G_DS_ANALYTIC_H

#include "2q3g-analytic.h"

// desymmetrized Amp2q3g_ds3_a

template <typename T>
class Amp2q3g_ds3_a : public Amp2q3g_a<T>
{
    typedef Amp2q3g_a<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;
    typedef typename BaseClass::ST ST;

    Amp2q3g_ds3_a(const T scalefactor,
                  const int mFC=1, const NJetAmpTables& tables=amptables());

    void setNc(const ST Nc_);

    LoopValue virt() { return BaseClass::virt_dstricksum(); }
    LoopValue virt(const int* h) { return BaseClass::virtds(h); }

  protected:
    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp2q3g_ds3Static>();
    }

    void initNc();

    bool setLoopType(int type, int norderL_, int norderF_)
    {
      return BaseClass::setLoopTypeNONE(type, norderL_, norderF_);
    }

    void getfvpart1ds(const int fv, LoopValue* fvpart);
    void getfvpart1ds(const int fv, LoopResult<T>* fvpart);
};

#endif /* ANALYTIC_2Q3G_DS_ANALYTIC_H */
