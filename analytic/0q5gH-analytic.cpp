/*
* analytic/0q5gH-analytic.cpp
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>
#include "0q5gH-analytic.h"

template <typename T>
Amp0q5gH_a<T>::Amp0q5gH_a(const T scalefactor)
  : BaseClass(scalefactor), hA0()
{
  njetan = new NJetAnalytic<T>(scalefactor, BaseClass::legsMOM(), -1);
  njetan->setNAB(5);
//   BaseClass::clearNG();

  hA0[0] = &Amp0q5gH_a::hA00;
  hA0[1] = &Amp0q5gH_a::hA01;
  hA0[2] = &Amp0q5gH_a::hA02;
  hA0[3] = &Amp0q5gH_a::hA03;
  hA0[4] = &Amp0q5gH_a::hA04;
  hA0[5] = &Amp0q5gH_a::hA05;
  hA0[6] = &Amp0q5gH_a::hA06;
  hA0[7] = &Amp0q5gH_a::hA07;
  hA0[8] = &Amp0q5gH_a::hA08;
  hA0[9] = &Amp0q5gH_a::hA09;
  hA0[10] = &Amp0q5gH_a::hA010;
  hA0[11] = &Amp0q5gH_a::hA011;
  hA0[12] = &Amp0q5gH_a::hA012;
  hA0[13] = &Amp0q5gH_a::hA013;
  hA0[14] = &Amp0q5gH_a::hA014;
  hA0[15] = &Amp0q5gH_a::hA015;
  hA0[16] = &Amp0q5gH_a::hA016;
  hA0[17] = &Amp0q5gH_a::hA017;
  hA0[18] = &Amp0q5gH_a::hA018;
  hA0[19] = &Amp0q5gH_a::hA019;
  hA0[20] = &Amp0q5gH_a::hA020;
  hA0[21] = &Amp0q5gH_a::hA021;
  hA0[22] = &Amp0q5gH_a::hA022;
  hA0[23] = &Amp0q5gH_a::hA023;
  hA0[24] = &Amp0q5gH_a::hA024;
  hA0[25] = &Amp0q5gH_a::hA025;
  hA0[26] = &Amp0q5gH_a::hA026;
  hA0[27] = &Amp0q5gH_a::hA027;
  hA0[28] = &Amp0q5gH_a::hA028;
  hA0[29] = &Amp0q5gH_a::hA029;
  hA0[30] = &Amp0q5gH_a::hA030;
  hA0[31] = &Amp0q5gH_a::hA031;
}

template <typename T>
typename Amp0q5gH_a<T>::TreeValue
Amp0q5gH_a<T>::A0(int p0, int p1, int p2, int p3, int p4)
{
  const int ord[] = {p0, p1, p2, p3, p4, BaseClass::NN};
  const int hel = HelicityOrder(32^mhelint, ord);
  HelAmp const hamp = hA0[hel];
  if (hamp) {
    return 0.5*i_*callTree(this, hamp, ord);
  } else {
    return TreeValue(0.);
  }
}

template <typename T>
typename Amp0q5gH_a<T>::TreeValue
Amp0q5gH_a<T>::hA00(const int* p)
{
  XX[1] = T(1.)/(sB(p[0],p[1])*sB(p[0],p[4])*sB(p[1],p[2])*sB(p[2],p[3])*sB(p[3],p[4]));
  XX[2] = lS(5,5);

  return  - pow(XX[2],2)*XX[1];
};
template <typename T>
typename Amp0q5gH_a<T>::TreeValue
Amp0q5gH_a<T>::hA01(const int* p)
{
  const int np[] = {p[1],p[2],p[3],p[4],p[0]};
  return hA016(np);
};
template <typename T>
typename Amp0q5gH_a<T>::TreeValue
Amp0q5gH_a<T>::hA02(const int* p)
{
  const int np[] = {p[2],p[3],p[4],p[0],p[1]};
  return hA016(np);
};
template <typename T>
typename Amp0q5gH_a<T>::TreeValue
Amp0q5gH_a<T>::hA03(const int* p)
{
  const int np[] = {p[2],p[3],p[4],p[0],p[1]};
  return hA024(np);
};
template <typename T>
typename Amp0q5gH_a<T>::TreeValue
Amp0q5gH_a<T>::hA04(const int* p)
{
  const int np[] = {p[3],p[4],p[0],p[1],p[2]};
  return hA016(np);
};
template <typename T>
typename Amp0q5gH_a<T>::TreeValue
Amp0q5gH_a<T>::hA05(const int* p)
{
  const int np[] = {p[1],p[2],p[3],p[4],p[0]};
  return hA018(np);
};
template <typename T>
typename Amp0q5gH_a<T>::TreeValue
Amp0q5gH_a<T>::hA06(const int* p)
{
  const int np[] = {p[3],p[4],p[0],p[1],p[2]};
  return hA024(np);
};
template <typename T>
typename Amp0q5gH_a<T>::TreeValue
Amp0q5gH_a<T>::hA07(const int* p)
{
  const int np[] = {p[3],p[4],p[0],p[1],p[2]};
  return hA028(np);
};
template <typename T>
typename Amp0q5gH_a<T>::TreeValue
Amp0q5gH_a<T>::hA08(const int* p)
{
  const int np[] = {p[4],p[0],p[1],p[2],p[3]};
  return hA016(np);
};
template <typename T>
typename Amp0q5gH_a<T>::TreeValue
Amp0q5gH_a<T>::hA09(const int* p)
{
  const int np[] = {p[1],p[2],p[3],p[4],p[0]};
  return hA020(np);
};
template <typename T>
typename Amp0q5gH_a<T>::TreeValue
Amp0q5gH_a<T>::hA010(const int* p)
{
  const int np[] = {p[4],p[0],p[1],p[2],p[3]};
  return hA020(np);
};
template <typename T>
typename Amp0q5gH_a<T>::TreeValue
Amp0q5gH_a<T>::hA011(const int* p)
{
  const int np[] = {p[2],p[3],p[4],p[0],p[1]};
  return hA026(np);
};
template <typename T>
typename Amp0q5gH_a<T>::TreeValue
Amp0q5gH_a<T>::hA012(const int* p)
{
  const int np[] = {p[4],p[0],p[1],p[2],p[3]};
  return hA024(np);
};
template <typename T>
typename Amp0q5gH_a<T>::TreeValue
Amp0q5gH_a<T>::hA013(const int* p)
{
  const int np[] = {p[1],p[2],p[3],p[4],p[0]};
  return hA022(np);
};
template <typename T>
typename Amp0q5gH_a<T>::TreeValue
Amp0q5gH_a<T>::hA014(const int* p)
{
  const int np[] = {p[4],p[0],p[1],p[2],p[3]};
  return hA028(np);
};
template <typename T>
typename Amp0q5gH_a<T>::TreeValue
Amp0q5gH_a<T>::hA015(const int* p)
{
  const int np[] = {p[4],p[0],p[1],p[2],p[3]};
  return hA030(np);
};
template <typename T>
typename Amp0q5gH_a<T>::TreeValue
Amp0q5gH_a<T>::hA016(const int* p)
{
  XX[1] = 1./(sA(p[0],p[4]));
  XX[2] = 1./(lS(5,p[1],p[2])-lS(5,5));
  XX[3] = 1./(sB(p[1],p[2]));
  XX[4] = 1./(sA(p[3],p[4]));
  XX[5] = 1./(sA(p[0],p[3])*sB(p[0],p[1]) + sA(p[3],p[4])*sB(p[1],p[4]));
  XX[6] = 1./(sA(p[0],p[3])*sB(p[2],p[3]) + sA(p[0],p[4])*sB(p[2],p[4]));
  XX[7] = sA(p[0],p[3]);
  XX[8] = 1./(sB(p[0],p[1]));
  XX[9] = 1./(sB(p[0],p[4]));
  XX[10] = 1./(sB(p[2],p[3]));
  XX[11] = 1./(sB(p[3],p[4]));
  XX[12] = (sA(p[0],p[4])*sB(p[0],p[4])*sAB(p[0],p[4]) + sA(p[0],p[4])*sB(p[1],p[4])*sAB(p[1],p[4]));
  XX[13] = 1./(sB(p[0],p[4])*sAB(p[0],p[2]) + sB(p[1],p[4])*sAB(p[1],p[2]));
  XX[14] = 1./(sB(p[2],p[4])*sAB(p[2],p[1]) + sB(p[3],p[4])*sAB(p[3],p[1]));
  XX[15] = 1./(sAB(p[3],p[2]));
  XX[16] = 1./(lS(5,p[3]));
  XX[17] = sAB(p[3],p[4]);
  XX[18] = 1./(lS(5,p[2]));
  XX[19] = 1./(lS(5,p[2],p[3])-lS(5,5));
  XX[20] = (sA(p[0],p[3])*sB(p[0],p[4]) + sA(p[1],p[3])*sB(p[1],p[4]));
  XX[21] = 1./(lS(5,p[0]));
  XX[22] = 1./(sAB(p[0],p[1]));
  XX[23] = sAB(p[0],p[4]);
  XX[24] = 1./(lS(5,p[0],p[1])-lS(5,5));
  XX[25] = 1./(lS(5,p[1]));
  XX[26] = (sA(p[0],p[2])*sB(p[2],p[4]) + sA(p[0],p[3])*sB(p[3],p[4]));
  XX[27] =  XX[8]*XX[9];
  XX[28] =  XX[27]*XX[13];
  XX[29] =  XX[1]*XX[12];
  XX[29] =  - pow(XX[29],3)*XX[28];
  XX[30] = pow(lS(5,5),2);
  XX[31] =  XX[6]*XX[25]*XX[24]*XX[22]*pow(XX[26],4)*XX[30];
  XX[29] =  XX[31] +  XX[29];
  XX[29] =  XX[29]*XX[14];
  XX[31] =  XX[3]*XX[21]*pow(XX[23],3)*XX[22];
  XX[29] =  XX[31] +  XX[29];
  XX[29] =  XX[10]*XX[11]*XX[29];
  XX[28] =  XX[19]*XX[18]*XX[15]*pow(XX[20],4)*XX[28];
  XX[31] =  -  XX[6]*XX[1]*XX[4]*XX[2]*XX[3]*pow(XX[7],4);
  XX[28] =  XX[28] +  XX[31];
  XX[28] =  XX[5]*XX[30]*XX[28];
  XX[27] =  -  XX[3]*XX[16]*pow(XX[17],3)*XX[27]*XX[15];
  XX[27] =  XX[28] +  XX[27] +  XX[29];
  return  XX[27];
};
template <typename T>
typename Amp0q5gH_a<T>::TreeValue
Amp0q5gH_a<T>::hA017(const int* p)
{
  const int np[] = {p[1],p[2],p[3],p[4],p[0]};
  return hA024(np);
};
template <typename T>
typename Amp0q5gH_a<T>::TreeValue
Amp0q5gH_a<T>::hA018(const int* p)
{
  const int np[] = {p[2],p[3],p[4],p[0],p[1]};
  return hA020(np);
};
template <typename T>
typename Amp0q5gH_a<T>::TreeValue
Amp0q5gH_a<T>::hA019(const int* p)
{
  const int np[] = {p[2],p[3],p[4],p[0],p[1]};
  return hA028(np);
};
template <typename T>
typename Amp0q5gH_a<T>::TreeValue
Amp0q5gH_a<T>::hA020(const int* p)
{
  XX[1] = 1./(sA(p[0],p[4]));
  XX[2] = 1./(sA(p[1],p[2]));
  XX[3] = 1./(sA(p[3],p[4]));
  XX[4] = 1./(sA(p[0],p[1])*sAB(p[0],p[1]) + sA(p[0],p[2])*sAB(p[0],p[2]));
  XX[5] = 1./(sA(p[0],p[1])*sAB(p[3],p[1]) + sA(p[0],p[2])*sAB(p[3],p[2]));
  XX[6] = 1./(sA(p[0],p[3])*sAB(p[2],p[3]) + sA(p[0],p[4])*sAB(p[2],p[4]));
  XX[7] = sA(p[0],p[1]);
  XX[8] = sA(p[0],p[3]);
  XX[9] = 1./(lS(5,p[1],p[2])-lS(5,5));
  XX[10] = 1./(sAB(p[2],p[1]));
  XX[11] = 1./(sA(p[0],p[3])*sB(p[0],p[1]) + sA(p[3],p[4])*sB(p[1],p[4]));
  XX[12] = lS(5,p[2]);
  XX[13] = 1./(sB(p[3],p[4]));
  XX[14] = 1./(sA(p[0],p[4])*sB(p[3],p[4]) - sAB(p[0],p[3]));
  XX[15] = 1./(sA(p[2],p[3])*sAB(p[0],p[3]) + sA(p[2],p[4])*sAB(p[0],p[4]));
  XX[16] = sAB(p[0],p[4]);
  XX[17] = 1./(lS(5,p[3]));
  XX[18] = 1./(lS(5,p[3],p[4])-lS(5,5));
  XX[19] = 1./(sA(p[2],p[3])*sB(p[3],p[4]) + sAB(p[2],p[4]));
  XX[20] = sAB(p[3],p[4]);
  XX[21] = 1./(sB(p[0],p[1]));
  XX[22] = 1./(sB(p[0],p[4]));
  XX[23] = 1./(sB(p[1],p[2]));
  XX[24] = 1./(sB(p[2],p[3]));
  XX[25] = sB(p[2],p[4]);
  XX[26] = 1./(lS(5,p[2],p[3])-lS(5,5));
  XX[27] = 1./(sA(p[2],p[3]));
  XX[28] = (sA(p[0],p[3])*sB(p[0],p[4]) + sA(p[1],p[3])*sB(p[1],p[4]));
  XX[29] = 1./(sA(p[0],p[2])*sB(p[0],p[4]) + sA(p[1],p[2])*sB(p[1],p[4]));
  XX[30] = 1./(sAB(p[0],p[1]));
  XX[31] = 1./(lS(5,p[1]));
  XX[32] = 1./(lS(5,p[0]));
  XX[33] = 1./(lS(5,p[0],p[1])-lS(5,5));
  XX[34] = (sA(p[0],p[3])*lS(5,p[0]) + sA(p[1],p[3])*sAB(p[0],p[1]));
  XX[35] = 1./(sA(p[0],p[4])*sB(p[0],p[1]) - sAB(p[4],p[1]));
  XX[36] = 1./(sA(p[0],p[2])*lS(5,p[0]) + sA(p[1],p[2])*sAB(p[0],p[1]));
  XX[37] = 1./(lS(5,p[0],p[4])-lS(5,5));
  XX[38] = (sA(p[0],p[4])*sB(p[2],p[4]) - sAB(p[0],p[2]));
  XX[39] =  XX[24]*XX[23];
  XX[40] =  XX[14]*XX[37]*XX[1]*pow(XX[38],4)*XX[39];
  XX[41] =  XX[27]*XX[30];
  XX[42] =  XX[32]*XX[33]*XX[36]*pow(XX[34],4)*XX[3]*XX[41];
  XX[40] =  XX[42] +  XX[40];
  XX[40] =  XX[35]*XX[40];
  XX[42] = pow(lS(5,5),2);
  XX[41] =  XX[31]*XX[42]*XX[41];
  XX[43] =  -  XX[9]*XX[6]*XX[11]*pow(XX[12],3);
  XX[41] =  XX[43] +  XX[41];
  XX[43] = pow(XX[8],4)*XX[1]*XX[3];
  XX[41] =  XX[10]*XX[41]*XX[43];
  XX[39] =  -  XX[13]*pow(XX[25],4)*XX[39];
  XX[44] =  XX[29]*XX[27]*XX[11]*XX[26]*pow(XX[28],4);
  XX[39] =  XX[44] +  XX[39];
  XX[39] =  XX[39]*XX[22]*XX[21];
  XX[42] =  -  XX[6]*XX[5]*XX[42]*XX[43];
  XX[43] =  -  XX[13]*XX[15]*XX[14]*pow(XX[16],3);
  XX[42] =  XX[43] +  XX[42];
  XX[42] =  XX[4]*XX[42];
  XX[43] =  -  XX[17]*XX[19]*XX[5]*XX[18]*pow(XX[20],3);
  XX[42] =  XX[43] +  XX[42];
  XX[42] =  XX[2]*XX[42]*pow(XX[7],3);
  XX[39] =  XX[42] +  XX[39] +  XX[41] +  XX[40];
  return  XX[39];
};
template <typename T>
typename Amp0q5gH_a<T>::TreeValue
Amp0q5gH_a<T>::hA021(const int* p)
{
  const int np[] = {p[1],p[2],p[3],p[4],p[0]};
  return hA026(np);
};
template <typename T>
typename Amp0q5gH_a<T>::TreeValue
Amp0q5gH_a<T>::hA022(const int* p)
{
  XX[1] = 1./(sA(p[0],p[1]));
  XX[2] = 1./(sA(p[0],p[4]));
  XX[3] = 1./(sA(p[1],p[2]));
  XX[4] = 1./(sA(p[2],p[3]));
  XX[5] = 1./(sA(p[3],p[4]));
  XX[6] = sA(p[0],p[3]);
  XX[7] = 1./(lS(5,p[3],p[4])-lS(5,5));
  XX[8] = 1./(sB(p[3],p[4]));
  XX[9] = (sA(p[0],p[3])*sB(p[3],p[4]) + sAB(p[0],p[4]));
  XX[10] = 1./(sA(p[0],p[4])*sB(p[3],p[4]) - sAB(p[0],p[3]));
  XX[11] = 1./(sA(p[2],p[3])*sB(p[3],p[4]) + sAB(p[2],p[4]));
  XX[12] = 1./(lS(5,p[1],p[2])-lS(5,5));
  XX[13] = 1./(sA(p[0],p[3])*sB(p[0],p[1]) + sA(p[3],p[4])*sB(p[1],p[4]));
  XX[14] = 1./(sA(p[0],p[3])*sB(p[2],p[3]) + sA(p[0],p[4])*sB(p[2],p[4]));
  XX[15] = sB(p[1],p[2]);
  XX[16] = 1./(sB(p[0],p[1]));
  XX[17] = 1./(sB(p[0],p[4]));
  XX[18] = 1./(sA(p[0],p[2])*sB(p[0],p[4]) + sA(p[1],p[2])*sB(p[1],p[4]));
  XX[19] = 1./(sB(p[1],p[2])*sAB(p[2],p[4]) + sB(p[1],p[3])*sAB(p[3],p[4]));
  XX[20] = 1./(sA(p[0],p[4])*sB(p[0],p[4])*sAB(p[0],p[4]) + sA(p[0],p[4])*sB(p[1],p[4])*sAB(p[1],p[4]));
  XX[21] = sA(p[0],p[4]);
  XX[22] = sB(p[1],p[4]);
  XX[23] = sAB(p[3],p[4]);
  XX[24] = 1./(sB(p[2],p[3]));
  XX[25] = 1./(sB(p[0],p[4])*sAB(p[0],p[2]) + sB(p[1],p[4])*sAB(p[1],p[2]));
  XX[26] = 1./(sB(p[2],p[4])*sAB(p[2],p[1]) + sB(p[3],p[4])*sAB(p[3],p[1]));
  XX[27] = sB(p[2],p[4]);
  XX[28] = 1./(lS(5,p[2]));
  XX[29] = 1./(lS(5,p[2],p[3])-lS(5,5));
  XX[30] = sAB(p[3],p[2]);
  XX[31] = 1./(lS(5,p[0],p[1])-lS(5,5));
  XX[32] = 1./(lS(5,p[1]));
  XX[33] = sAB(p[0],p[1]);
  XX[34] = 1./(lS(5,p[0],p[4])-lS(5,5));
  XX[35] = 1./(lS(5,p[4]));
  XX[36] = 1./(sB(p[0],p[1])*sAB(p[0],p[4]) - sB(p[1],p[4])*lS(5,p[4]));
  XX[37] = sAB(p[0],p[4]);
  XX[38] =  XX[5]*XX[2]*pow(XX[6],4);
  XX[39] =  XX[3]*XX[1];
  XX[40] =  XX[39]*XX[38];
  XX[41] = pow(XX[22],4)*XX[17]*XX[16];
  XX[42] =  XX[21]*XX[20];
  XX[43] =  -  XX[18]*XX[19]*pow(XX[23],3)*XX[42]*XX[41];
  XX[40] =  XX[40] +  XX[43];
  XX[40] =  XX[4]*XX[40];
  XX[43] = pow(XX[15],3);
  XX[38] =  -  XX[13]*XX[12]*XX[43]*XX[38];
  XX[44] =  XX[8]*pow(XX[27],4)*XX[24]*XX[26];
  XX[45] =  XX[32]*XX[31]*pow(XX[33],3)*XX[44];
  XX[38] =  XX[45] +  XX[38];
  XX[38] =  XX[14]*XX[38];
  XX[39] =  -  XX[7]*XX[11]*XX[8]*pow(XX[9],4)*XX[39];
  XX[43] =  -  XX[24]*XX[34]*XX[36]*XX[35]*pow(XX[37],3)*XX[43];
  XX[39] =  XX[39] +  XX[43];
  XX[39] =  XX[10]*XX[39];
  XX[42] =  - pow(lS(5,5),2)*XX[42]*XX[44];
  XX[43] =  XX[13]*XX[29]*XX[28]*pow(XX[30],3);
  XX[42] =  XX[43] +  XX[42];
  XX[41] =  XX[25]*XX[42]*XX[41];
  XX[38] =  XX[40] +  XX[41] +  XX[39] +  XX[38];
  return  XX[38];
};
template <typename T>
typename Amp0q5gH_a<T>::TreeValue
Amp0q5gH_a<T>::hA023(const int* p)
{
  const int np[] = {p[3],p[4],p[0],p[1],p[2]};
  return hA030(np);
};
template <typename T>
typename Amp0q5gH_a<T>::TreeValue
Amp0q5gH_a<T>::hA024(const int* p)
{
  XX[1] = 1./(sA(p[0],p[4]));
  XX[2] = 1./(lS(5,p[1],p[2])-lS(5,5));
  XX[3] = 1./(sAB(p[2],p[1]));
  XX[4] = 1./(lS(5,p[2]));
  XX[5] = 1./(sA(p[3],p[4]));
  XX[6] = (sA(p[0],p[3])*sAB(p[2],p[3]) + sA(p[0],p[4])*sAB(p[2],p[4]));
  XX[7] = 1./(sA(p[0],p[3])*sB(p[0],p[1]) + sA(p[3],p[4])*sB(p[1],p[4]));
  XX[8] = 1./(sB(p[0],p[1]));
  XX[9] = 1./(sB(p[0],p[4]));
  XX[10] = 1./(sB(p[1],p[2]));
  XX[11] = 1./(sB(p[2],p[3]));
  XX[12] = sB(p[3],p[4]);
  XX[13] = 1./(lS(5,p[2],p[3])-lS(5,5));
  XX[14] = 1./(sA(p[2],p[3]));
  XX[15] = (sA(p[0],p[2])*sB(p[0],p[4]) + sA(p[1],p[2])*sB(p[1],p[4]));
  XX[16] = 1./(sAB(p[0],p[1]));
  XX[17] = 1./(lS(5,p[1]));
  XX[18] = sA(p[0],p[2]);
  XX[19] = 1./(lS(5,p[0]));
  XX[20] = 1./(lS(5,p[0],p[1])-lS(5,5));
  XX[21] = (sA(p[0],p[2])*lS(5,p[0]) + sA(p[1],p[2])*sAB(p[0],p[1]));
  XX[22] = 1./(sA(p[0],p[4])*sB(p[0],p[1]) - sAB(p[4],p[1]));
  XX[23] = 1./(lS(5,p[0],p[4])-lS(5,5));
  XX[24] = (sA(p[0],p[4])*sB(p[3],p[4]) - sAB(p[0],p[3]));
  XX[25] =  XX[19]*XX[22]*XX[20]*pow(XX[21],3);
  XX[26] =  XX[3]*XX[1];
  XX[27] =  XX[17]*pow(lS(5,5),2)*pow(XX[18],4)*XX[26];
  XX[25] =  XX[27] +  XX[25];
  XX[25] =  XX[25]*XX[14]*XX[16];
  XX[26] =  -  XX[2]*XX[4]*XX[7]*pow(XX[6],3)*XX[26];
  XX[25] =  XX[26] +  XX[25];
  XX[25] =  XX[5]*XX[25];
  XX[26] =  XX[8]*XX[9];
  XX[27] =  - pow(XX[12],3)*XX[26];
  XX[28] =  XX[23]*XX[22]*XX[1]*pow(XX[24],3);
  XX[27] =  XX[27] +  XX[28];
  XX[27] =  XX[10]*XX[11]*XX[27];
  XX[26] =  XX[7]*XX[13]*XX[14]*pow(XX[15],3)*XX[26];
  XX[25] =  XX[26] +  XX[27] +  XX[25];
  return  XX[25];
};
template <typename T>
typename Amp0q5gH_a<T>::TreeValue
Amp0q5gH_a<T>::hA025(const int* p)
{
  const int np[] = {p[1],p[2],p[3],p[4],p[0]};
  return hA028(np);
};
template <typename T>
typename Amp0q5gH_a<T>::TreeValue
Amp0q5gH_a<T>::hA026(const int* p)
{
  XX[1] = 1./(sA(p[0],p[1]));
  XX[2] = 1./(sA(p[0],p[4]));
  XX[3] = 1./(sA(p[1],p[2]));
  XX[4] = 1./(sA(p[2],p[3]));
  XX[5] = 1./(sA(p[3],p[4]));
  XX[6] = sA(p[0],p[2]);
  XX[7] = 1./(lS(5,p[3],p[4])-lS(5,5));
  XX[8] = 1./(sA(p[0],p[4])*sB(p[3],p[4]) - sAB(p[0],p[3]));
  XX[9] = 1./(sA(p[2],p[3])*sB(p[3],p[4]) + sAB(p[2],p[4]));
  XX[10] = sB(p[3],p[4]);
  XX[11] = 1./(lS(5,p[1],p[2])-lS(5,5));
  XX[12] = 1./(sB(p[1],p[2]));
  XX[13] = (sA(p[0],p[3])*sB(p[1],p[3]) + sA(p[0],p[4])*sB(p[1],p[4]));
  XX[14] = 1./(sA(p[0],p[3])*sB(p[0],p[1]) + sA(p[3],p[4])*sB(p[1],p[4]));
  XX[15] = 1./(sA(p[0],p[3])*sB(p[2],p[3]) + sA(p[0],p[4])*sB(p[2],p[4]));
  XX[16] = 1./(sB(p[0],p[1]));
  XX[17] = 1./(sB(p[0],p[4]));
  XX[18] = 1./(sB(p[1],p[2])*sAB(p[2],p[1]) + sB(p[1],p[3])*sAB(p[3],p[1]));
  XX[19] = 1./(sB(p[2],p[4])*sAB(p[2],p[1]) + sB(p[3],p[4])*sAB(p[3],p[1]));
  XX[20] = sB(p[1],p[4]);
  XX[21] = sAB(p[2],p[1]);
  XX[22] = 1./(sB(p[2],p[3]));
  XX[23] = 1./(sB(p[0],p[1])*sAB(p[0],p[3]) - sB(p[1],p[4])*sAB(p[4],p[3]));
  XX[24] = 1./(sB(p[1],p[2])*sAB(p[2],p[4]) + sB(p[1],p[3])*sAB(p[3],p[4]));
  XX[25] = sB(p[1],p[3]);
  XX[26] = 1./(lS(5,p[2],p[3])-lS(5,5));
  XX[27] = 1./(lS(5,p[3]));
  XX[28] = 1./(sA(p[0],p[2])*sB(p[0],p[4]) + sA(p[1],p[2])*sB(p[1],p[4]));
  XX[29] = sAB(p[2],p[3]);
  XX[30] = 1./(lS(5,p[0],p[1])-lS(5,5));
  XX[31] = 1./(lS(5,p[1]));
  XX[32] = sAB(p[0],p[1]);
  XX[33] = 1./(lS(5,p[0],p[4])-lS(5,5));
  XX[34] = 1./(lS(5,p[4]));
  XX[35] = 1./(sB(p[0],p[1])*sAB(p[0],p[4]) - sB(p[1],p[4])*lS(5,p[4]));
  XX[36] = sAB(p[0],p[4]);
  XX[37] = pow(XX[6],4)*XX[3]*XX[1];
  XX[38] =  XX[4]*XX[37];
  XX[39] =  -  XX[11]*XX[15]*XX[12]*XX[14]*pow(XX[13],4);
  XX[38] =  XX[38] +  XX[39];
  XX[38] =  XX[38]*XX[5]*XX[2];
  XX[39] =  XX[31]*XX[15]*XX[19]*XX[30]*XX[22]*pow(XX[32],3);
  XX[37] =  -  XX[7]*XX[9]*XX[8]*XX[37];
  XX[37] =  XX[39] +  XX[37];
  XX[37] =  XX[37]*pow(XX[10],3);
  XX[39] = pow(XX[25],4)*XX[12]*XX[22];
  XX[40] =  -  XX[24]*XX[18]*pow(lS(5,5),2)*XX[39];
  XX[41] =  -  XX[28]*XX[27]*XX[26]*pow(XX[29],3);
  XX[40] =  XX[41] +  XX[40];
  XX[40] =  XX[40]*XX[23];
  XX[41] =  XX[4]*XX[19]*XX[14]*pow(XX[21],3)*XX[18];
  XX[40] =  XX[41] +  XX[40];
  XX[40] =  XX[17]*pow(XX[20],4)*XX[16]*XX[40];
  XX[39] =  -  XX[34]*XX[33]*XX[35]*XX[8]*pow(XX[36],3)*XX[39];
  XX[37] =  XX[40] +  XX[37] +  XX[39] +  XX[38];
  return  XX[37];
};
template <typename T>
typename Amp0q5gH_a<T>::TreeValue
Amp0q5gH_a<T>::hA027(const int* p)
{
  const int np[] = {p[2],p[3],p[4],p[0],p[1]};
  return hA030(np);
};
template <typename T>
typename Amp0q5gH_a<T>::TreeValue
Amp0q5gH_a<T>::hA028(const int* p)
{
  XX[1] = 1./(sA(p[0],p[4]));
  XX[2] = 1./(sA(p[1],p[2]));
  XX[3] = 1./(sA(p[2],p[3]));
  XX[4] = 1./(sA(p[3],p[4]));
  XX[5] = sA(p[0],p[1]);
  XX[6] = 1./(lS(5,p[1],p[2])-lS(5,5));
  XX[7] = 1./(sB(p[1],p[2]));
  XX[8] = (sA(p[0],p[3])*sB(p[2],p[3]) + sA(p[0],p[4])*sB(p[2],p[4]));
  XX[9] = 1./(sA(p[0],p[3])*sB(p[0],p[1]) + sA(p[3],p[4])*sB(p[1],p[4]));
  XX[10] = 1./(lS(5,p[3],p[4])-lS(5,5));
  XX[11] = 1./(sA(p[0],p[4])*sB(p[3],p[4]) - sAB(p[0],p[3]));
  XX[12] = 1./(sA(p[2],p[3])*sB(p[3],p[4]) + sAB(p[2],p[4]));
  XX[13] = sB(p[3],p[4]);
  XX[14] = 1./(sB(p[0],p[1]));
  XX[15] = 1./(sB(p[0],p[4]));
  XX[16] = 1./(sAB(p[3],p[4]));
  XX[17] = (sA(p[0],p[4])*sB(p[0],p[4])*sAB(p[0],p[4]) + sA(p[0],p[4])*sB(p[1],p[4])*sAB(p[1],p[4]));
  XX[18] = 1./(sA(p[0],p[2])*sB(p[0],p[4]) + sA(p[1],p[2])*sB(p[1],p[4]));
  XX[19] = 1./(sB(p[1],p[2])*sAB(p[2],p[4]) + sB(p[1],p[3])*sAB(p[3],p[4]));
  XX[20] = 1./(sAB(p[3],p[2]));
  XX[21] = 1./(lS(5,p[3]));
  XX[22] = sB(p[2],p[4]);
  XX[23] = 1./(lS(5,p[2]));
  XX[24] = 1./(lS(5,p[2],p[3])-lS(5,5));
  XX[25] = (sB(p[0],p[4])*sAB(p[0],p[2]) + sB(p[1],p[4])*sAB(p[1],p[2]));
  XX[26] = 1./(lS(5,p[0],p[4])-lS(5,5));
  XX[27] = 1./(lS(5,p[4]));
  XX[28] = 1./(sB(p[0],p[1])*sAB(p[0],p[4]) - sB(p[1],p[4])*lS(5,p[4]));
  XX[29] = sB(p[2],p[3]);
  XX[30] = sAB(p[0],p[4]);
  XX[31] =  XX[4]*XX[1];
  XX[32] =  -  XX[6]*XX[9]*pow(XX[8],3)*XX[31];
  XX[33] =  XX[30]*XX[29];
  XX[33] =  -  XX[26]*XX[27]*XX[28]*XX[11]*pow(XX[33],3);
  XX[32] =  XX[32] +  XX[33];
  XX[32] =  XX[7]*XX[32];
  XX[33] =  -  XX[21]*XX[7]*XX[16]*pow(lS(5,5),2)*pow(XX[22],4);
  XX[34] =  XX[23]*XX[24]*XX[9]*pow(XX[25],3);
  XX[33] =  XX[34] +  XX[33];
  XX[33] =  XX[20]*XX[33];
  XX[34] =  XX[17]*XX[1];
  XX[34] =  -  XX[18]*XX[3]*XX[19]*XX[16]*pow(XX[34],3);
  XX[33] =  XX[34] +  XX[33];
  XX[33] =  XX[33]*XX[14]*XX[15];
  XX[34] =  -  XX[10]*XX[11]*XX[12]*pow(XX[13],3);
  XX[31] =  XX[3]*XX[31];
  XX[31] =  XX[34] +  XX[31];
  XX[31] = pow(XX[5],3)*XX[2]*XX[31];
  XX[31] =  XX[31] +  XX[33] +  XX[32];
  return  XX[31];
};
template <typename T>
typename Amp0q5gH_a<T>::TreeValue
Amp0q5gH_a<T>::hA029(const int* p)
{
  const int np[] = {p[1],p[2],p[3],p[4],p[0]};
  return hA030(np);
};
template <typename T>
typename Amp0q5gH_a<T>::TreeValue
Amp0q5gH_a<T>::hA030(const int* p)
{
  XX[1] = 1./(sA(p[0],p[1]));
  XX[2] = 1./(sA(p[0],p[4]));
  XX[3] = 1./(sA(p[1],p[2]));
  XX[4] = 1./(sA(p[3],p[4]));
  XX[5] = (sA(p[0],p[1])*sAB(p[0],p[1]) + sA(p[0],p[2])*sAB(p[0],p[2]));
  XX[6] = 1./(sA(p[0],p[1])*sAB(p[3],p[1]) + sA(p[0],p[2])*sAB(p[3],p[2]));
  XX[7] = 1./(sA(p[0],p[3])*sAB(p[2],p[3]) + sA(p[0],p[4])*sAB(p[2],p[4]));
  XX[8] = 1./(sA(p[2],p[3]));
  XX[9] = 1./(sAB(p[3],p[4]));
  XX[10] = 1./(lS(5,p[4]));
  XX[11] = sAB(p[0],p[4]);
  XX[12] = 1./(lS(5,p[3]));
  XX[13] = 1./(lS(5,p[3],p[4])-lS(5,5));
  XX[14] = (sA(p[0],p[3])*sB(p[3],p[4]) + sAB(p[0],p[4]));
  XX[15] = 1./(sA(p[2],p[3])*sB(p[3],p[4]) + sAB(p[2],p[4]));
  XX[16] = 1./(lS(5,p[1]));
  XX[17] = 1./(sAB(p[2],p[1]));
  XX[18] = sAB(p[0],p[1]);
  XX[19] = 1./(lS(5,p[1],p[2])-lS(5,5));
  XX[20] = 1./(lS(5,p[2]));
  XX[21] = (sA(p[0],p[3])*sB(p[1],p[3]) + sA(p[0],p[4])*sB(p[1],p[4]));
  XX[22] = 1./(sA(p[0],p[3])*sB(p[0],p[1]) + sA(p[3],p[4])*sB(p[1],p[4]));
  XX[23] = 1./(sB(p[0],p[1]));
  XX[24] = 1./(sB(p[0],p[4]));
  XX[25] = 1./(lS(5,p[2],p[3])-lS(5,5));
  XX[26] = 1./(sA(p[0],p[2])*sB(p[0],p[4]) + sA(p[1],p[2])*sB(p[1],p[4]));
  XX[27] = sB(p[1],p[4]);
  XX[28] = pow(lS(5,5),2);
  XX[29] =  XX[28]*XX[22];
  XX[30] =  XX[4]*XX[2];
  XX[31] =  XX[30]*XX[17];
  XX[32] =  -  XX[7]*XX[19]*XX[20]*pow(XX[21],4)*XX[31]*XX[29];
  XX[29] =  XX[26]*XX[23]*XX[24]*XX[25]*pow(XX[27],4)*XX[29];
  XX[31] =  XX[16]*pow(XX[18],3)*XX[31];
  XX[29] =  XX[29] +  XX[31];
  XX[29] =  XX[8]*XX[29];
  XX[28] =  -  XX[13]*XX[12]*XX[15]*XX[9]*pow(XX[14],4)*XX[28];
  XX[30] =  - pow(XX[5],3)*XX[30]*XX[7];
  XX[28] =  XX[28] +  XX[30];
  XX[28] =  XX[28]*XX[6];
  XX[30] =  -  XX[8]*XX[10]*pow(XX[11],3)*XX[9];
  XX[28] =  XX[30] +  XX[28];
  XX[28] =  XX[3]*XX[1]*XX[28];
  XX[28] =  XX[28] +  XX[32] +  XX[29];
  return  XX[28];
};
template <typename T>
typename Amp0q5gH_a<T>::TreeValue
Amp0q5gH_a<T>::hA031(const int* p)
{
  XX[1] = T(1.)/(sA(p[0],p[1])*sA(p[0],p[4])*sA(p[1],p[2])*sA(p[2],p[3])*sA(p[3],p[4]));
  XX[2] = lS(5,5);
  return pow(XX[2],2)*XX[1];
};

#ifdef USE_SD
  template class Amp0q5gH_a<double>;
#endif
#ifdef USE_DD
  template class Amp0q5gH_a<dd_real>;
#endif
#ifdef USE_QD
  template class Amp0q5gH_a<qd_real>;
#endif
#ifdef USE_VC
  template class Amp0q5gH_a<Vc::double_v>;
#endif
