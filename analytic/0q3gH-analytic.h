/*
* analytic/0q3gH-analytic.h
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef ANALYTIC_0Q3GH_ANALYTIC_H
#define ANALYTIC_0Q3GH_ANALYTIC_H

#include "../chsums/0q3gH.h"

template <typename T>
class Amp0q3gH_a : public Amp0q3gH<T>
{
    typedef Amp0q3gH<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    typedef TreeValue (Amp0q3gH_a::*HelAmp)(const int* ord);

    Amp0q3gH_a(const T scalefactor);

  protected:
    using BaseClass::mhel;
    using BaseClass::mhelint;

    using BaseClass::njetan;
    using BaseClass::callTree;
    using BaseClass::callLoop;
    using BaseClass::lS;
    using BaseClass::sA;
    using BaseClass::sB;
    using BaseClass::HelicityOrder;

    virtual TreeValue A0(int p0, int p1, int p2);

    HelAmp hA0[8];

    TreeValue hA00(const int* ord);
    TreeValue hA01(const int* ord);
    TreeValue hA02(const int* ord);
    TreeValue hA03(const int* ord);
    TreeValue hA04(const int* ord);
    TreeValue hA05(const int* ord);
    TreeValue hA06(const int* ord);
    TreeValue hA07(const int* ord);
};

#endif /* ANALYTIC_0Q3GH_ANALYTIC_H */
