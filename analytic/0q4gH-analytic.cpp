/*
* analytic/0q4gH-analytic.cpp
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>
#include "0q4gH-analytic.h"

template <typename T>
Amp0q4gH_a<T>::Amp0q4gH_a(const T scalefactor)
  : BaseClass(scalefactor), hA0()
{
  njetan = new NJetAnalytic<T>(scalefactor, BaseClass::legsMOM(), -1);
  njetan->setNAB(4);
//   BaseClass::clearNG();

  hA0[0] = &Amp0q4gH_a::hA00;
  hA0[1] = &Amp0q4gH_a::hA01;
  hA0[2] = &Amp0q4gH_a::hA02;
  hA0[3] = &Amp0q4gH_a::hA03;
  hA0[4] = &Amp0q4gH_a::hA04;
  hA0[5] = &Amp0q4gH_a::hA05;
  hA0[6] = &Amp0q4gH_a::hA06;
  hA0[7] = &Amp0q4gH_a::hA07;
  hA0[8] = &Amp0q4gH_a::hA08;
  hA0[9] = &Amp0q4gH_a::hA09;
  hA0[10] = &Amp0q4gH_a::hA010;
  hA0[11] = &Amp0q4gH_a::hA011;
  hA0[12] = &Amp0q4gH_a::hA012;
  hA0[13] = &Amp0q4gH_a::hA013;
  hA0[14] = &Amp0q4gH_a::hA014;
  hA0[15] = &Amp0q4gH_a::hA015;
}

template <typename T>
typename Amp0q4gH_a<T>::TreeValue
Amp0q4gH_a<T>::A0(int p0, int p1, int p2, int p3)
{
  const int ord[] = {p0, p1, p2, p3, BaseClass::NN};
  const int hel = HelicityOrder(16^mhelint, ord);
  HelAmp const hamp = hA0[hel];
  if (hamp) {
    return 0.5*i_*callTree(this, hamp, ord);
  } else {
    return TreeValue(0.);
  }
}

template <typename T>
typename Amp0q4gH_a<T>::TreeValue
Amp0q4gH_a<T>::hA00(const int* p)
{
  return pow(lS(4, 4), 2)/(sB(p[0], p[1])*sB(p[1], p[2])*sB(p[2], p[3])*sB(p[0], p[3]));
}
template <typename T>
typename Amp0q4gH_a<T>::TreeValue
Amp0q4gH_a<T>::hA01(const int* p)
{
  const int np[] = {p[1], p[2], p[3], p[0]};
  return hA08(np);
}
template <typename T>
typename Amp0q4gH_a<T>::TreeValue
Amp0q4gH_a<T>::hA02(const int* p)
{
  const int np[] = {p[2], p[3], p[0], p[1]};
  return hA08(np);
}
template <typename T>
typename Amp0q4gH_a<T>::TreeValue
Amp0q4gH_a<T>::hA03(const int* p)
{
  const int np[] = {p[2], p[3], p[0], p[1]};
  return hA012(np);
}
template <typename T>
typename Amp0q4gH_a<T>::TreeValue
Amp0q4gH_a<T>::hA04(const int* p)
{
  const int np[] = {p[3], p[0], p[1], p[2]};
  return hA08(np);
}
template <typename T>
typename Amp0q4gH_a<T>::TreeValue
Amp0q4gH_a<T>::hA05(const int* p)
{
  const int np[] = {p[1], p[2], p[3], p[0]};
  return hA010(np);
}
template <typename T>
typename Amp0q4gH_a<T>::TreeValue
Amp0q4gH_a<T>::hA06(const int* p)
{
  const int np[] = {p[3], p[0], p[1], p[2]};
  return hA012(np);
}
template <typename T>
typename Amp0q4gH_a<T>::TreeValue
Amp0q4gH_a<T>::hA07(const int* p)
{
  const int np[] = {p[3], p[0], p[1], p[2]};
  return hA014(np);
}
template <typename T>
typename Amp0q4gH_a<T>::TreeValue
Amp0q4gH_a<T>::hA08(const int* p)
{
  return  (
            pow(lS(4, 4), 2)*pow(sA(p[0], p[2]), 4)/(sAB(p[2], p[1])*lS(p[1], 4)*sA(p[0], p[3])*sA(p[2], p[3]))
          - pow(sAB(p[0], p[3]), 3)/(lS(p[0], 4)*sB(p[1], p[2])*sB(p[2], p[3]))
          )/sAB(p[0], p[1])
          + pow(sAB(p[2], p[3]), 3)/(sAB(p[2], p[1])*lS(p[2], 4)*sB(p[0], p[1])*sB(p[0], p[3]));
}
template <typename T>
typename Amp0q4gH_a<T>::TreeValue
Amp0q4gH_a<T>::hA09(const int* p)
{
  const int np[] = {p[1], p[2], p[3], p[0]};
  return hA012(np);
}
template <typename T>
typename Amp0q4gH_a<T>::TreeValue
Amp0q4gH_a<T>::hA010(const int* p)
{
  return   pow(sB(p[1], p[3]), 4)/(sB(p[0], p[1])*sB(p[1], p[2])*sB(p[2], p[3])*sB(p[0], p[3]))
         + pow(sA(p[0], p[2]), 4)/(sA(p[0], p[1])*sA(p[1], p[2])*sA(p[2], p[3])*sA(p[0], p[3]));
}
template <typename T>
typename Amp0q4gH_a<T>::TreeValue
Amp0q4gH_a<T>::hA011(const int* p)
{
  const int np[] = {p[2], p[3], p[0], p[1]};
  return hA014(np);
}
template <typename T>
typename Amp0q4gH_a<T>::TreeValue
Amp0q4gH_a<T>::hA012(const int* p)
{
  return   pow(sB(p[2], p[3]), 4)/(sB(p[0], p[1])*sB(p[1], p[2])*sB(p[2], p[3])*sB(p[0], p[3]))
         + pow(sA(p[0], p[1]), 4)/(sA(p[0], p[1])*sA(p[1], p[2])*sA(p[2], p[3])*sA(p[0], p[3]));
}
template <typename T>
typename Amp0q4gH_a<T>::TreeValue
Amp0q4gH_a<T>::hA013(const int* p)
{
  const int np[] = {p[1], p[2], p[3], p[0]};
  return hA014(np);
}
template <typename T>
typename Amp0q4gH_a<T>::TreeValue
Amp0q4gH_a<T>::hA014(const int* p)
{
  return  (
            pow(lS(4, 4), 2)*pow(sB(p[1], p[3]), 4)/(sAB(p[2], p[3])*lS(p[2], 4)*sB(p[0], p[1])*sB(p[0], p[3]))
          + pow(sAB(p[0], p[1]), 3)/ (lS(p[1], 4)*sA(p[0], p[3])*sA(p[2], p[3]))
          )/sAB(p[2], p[1])
          - pow(sAB(p[0], p[3]), 3)/ (sAB(p[2], p[3])*lS(p[3], 4)*sA(p[0], p[1])*sA(p[1], p[2]));
}
template <typename T>
typename Amp0q4gH_a<T>::TreeValue
Amp0q4gH_a<T>::hA015(const int* p)
{
  return pow(lS(4, 4), 2)/(sA(p[0], p[1])*sA(p[1], p[2])*sA(p[2], p[3])*sA(p[0], p[3]));
}

#ifdef USE_SD
  template class Amp0q4gH_a<double>;
#endif
#ifdef USE_DD
  template class Amp0q4gH_a<dd_real>;
#endif
#ifdef USE_QD
  template class Amp0q4gH_a<qd_real>;
#endif
#ifdef USE_VC
  template class Amp0q4gH_a<Vc::double_v>;
#endif
