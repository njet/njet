/*
* analytic/0q5g-analytic.h
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef ANALYTIC_0Q6G_ANALYTIC_H
#define ANALYTIC_0Q6G_ANALYTIC_H

#include "../chsums/0q6g.h"

template <typename T>
class Amp0q6g_a : public Amp0q6g<T>
{
    typedef Amp0q6g<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    typedef TreeValue (Amp0q6g_a::*HelAmp)(const int* ord);

    Amp0q6g_a(const T scalefactor);

  protected:
    using BaseClass::mhel;
    using BaseClass::mhelint;

    using BaseClass::njetan;
    using BaseClass::callTree;
    using BaseClass::callLoop;
    using BaseClass::sA;
    using BaseClass::sB;
    using BaseClass::lS;
    using BaseClass::CyclicSpinorsA;
    using BaseClass::CyclicSpinorsB;
    using BaseClass::HelicityOrder;

    virtual TreeValue A0(int p0, int p1, int p2, int p3, int p4, int p5);

    HelAmp hA0[64];

    TreeValue hA03(const int* ord);
    TreeValue hA05(const int* ord);
    TreeValue hA06(const int* ord);
    TreeValue hA07(const int* ord);
    TreeValue hA09(const int* ord);
    TreeValue hA010(const int* ord);
    TreeValue hA011(const int* ord);
    TreeValue hA012(const int* ord);
    TreeValue hA013(const int* ord);
    TreeValue hA014(const int* ord);
    TreeValue hA015(const int* ord);
    TreeValue hA017(const int* ord);
    TreeValue hA018(const int* ord);
    TreeValue hA019(const int* ord);
    TreeValue hA020(const int* ord);
    TreeValue hA021(const int* ord);
    TreeValue hA022(const int* ord);
    TreeValue hA023(const int* ord);
    TreeValue hA024(const int* ord);
    TreeValue hA025(const int* ord);
    TreeValue hA026(const int* ord);
    TreeValue hA027(const int* ord);
    TreeValue hA028(const int* ord);
    TreeValue hA029(const int* ord);
    TreeValue hA030(const int* ord);
    TreeValue hA033(const int* ord);
    TreeValue hA034(const int* ord);
    TreeValue hA035(const int* ord);
    TreeValue hA036(const int* ord);
    TreeValue hA037(const int* ord);
    TreeValue hA038(const int* ord);
    TreeValue hA039(const int* ord);
    TreeValue hA040(const int* ord);
    TreeValue hA041(const int* ord);
    TreeValue hA042(const int* ord);
    TreeValue hA043(const int* ord);
    TreeValue hA044(const int* ord);
    TreeValue hA045(const int* ord);
    TreeValue hA046(const int* ord);
    TreeValue hA048(const int* ord);
    TreeValue hA049(const int* ord);
    TreeValue hA050(const int* ord);
    TreeValue hA051(const int* ord);
    TreeValue hA052(const int* ord);
    TreeValue hA053(const int* ord);
    TreeValue hA054(const int* ord);
    TreeValue hA056(const int* ord);
    TreeValue hA057(const int* ord);
    TreeValue hA058(const int* ord);
    TreeValue hA060(const int* ord);

    std::complex<T> XX[16];

};

#endif /* ANALYTIC_0Q6G_ANALYTIC_H */
