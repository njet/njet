/*
* analytic/0q5g-ds-analytic.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012, 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include "0q5g-ds-analytic.h"

// class Amp0q5g_ds3_a

template <typename T>
Amp0q5g_ds3_a<T>::Amp0q5g_ds3_a(const T scalefactor,
                                const int mFC, const NJetAmpTables& tables)
  : BaseClass(scalefactor, mFC, tables)
{
  BaseClass::initNc3();
}

template <typename T>
void Amp0q5g_ds3_a<T>::setNc(const ST Nc_)
{
  BaseClass::setNc(Nc_);
  BaseClass::initNc3();
}

template <typename T>
void Amp0q5g_ds3_a<T>::getfvpart1ds(const int fv, LoopValue* fvpart)
{
  return BaseClass::getfvpart1ds3_(fv, fvpart);
}

template <typename T>
void Amp0q5g_ds3_a<T>::getfvpart1ds(const int fv, LoopResult<T>* fvpart)
{
  return BaseClass::getfvpart1ds3_(fv, fvpart);
}

#ifdef USE_SD
  template class Amp0q5g_ds3_a<double>;
#endif
#ifdef USE_DD
  template class Amp0q5g_ds3_a<dd_real>;
#endif
#ifdef USE_QD
  template class Amp0q5g_ds3_a<qd_real>;
#endif
#ifdef USE_VC
  template class Amp0q5g_ds3_a<Vc::double_v>;
#endif
