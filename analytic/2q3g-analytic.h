/*
* analytic/2q3g-analytic.h
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef ANALYTIC_2Q3G_ANALYTIC_H
#define ANALYTIC_2Q3G_ANALYTIC_H

#include "../chsums/2q3g.h"

template <typename T>
class Amp2q3g_a : public Amp2q3g<T>
{
    typedef Amp2q3g<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    typedef TreeValue (Amp2q3g_a::*HelAmp)(const int* ord);
    typedef LoopResult<T> (Amp2q3g_a::*HelAmpLoop)(const int* ord);

    Amp2q3g_a(const T scalefactor, const int mFC=1,
              const NJetAmpTables& tables=BaseClass::amptables());

  protected:
    using BaseClass::mhel;
    using BaseClass::mhelint;

    using BaseClass::njetan;
    using BaseClass::getFlav;
    using BaseClass::callTree;
    using BaseClass::callLoop;
    using BaseClass::sA;
    using BaseClass::sB;
    using BaseClass::lS;
    using BaseClass::CyclicSpinorsA;
    using BaseClass::CyclicSpinorsB;
    using BaseClass::HelicityOrder;

    T MuR2() { return njetan->getMuR2(); }
    T sign(T x) { return (x < T(0.)) ? -T(1.) : T(1.); }

    virtual TreeValue A0(int p0, int p1, int p2, int p3, int p4);
    virtual LoopResult<T> AL(int p0, int p1, int p2, int p3, int p4);
    virtual LoopResult<T> AF(int p0, int p1, int p2, int p3, int p4);

    virtual LoopResult<T> AL(int p0, int p1, int p2, int p3, int p4, int /*pos*/) {
      return AL(p0, p1, p2, p3, p4);
    }
    virtual LoopResult<T> AF(int p0, int p1, int p2, int p3, int p4, int /*pos*/, int /*posR*/) {
      return AF(p0, p1, p2, p3, p4);
    }

    HelAmp hA0[32];

    HelAmpLoop hAL1[32];
    HelAmpLoop hAL2[32];
    HelAmpLoop hAL3[32];
    HelAmpLoop hAL4[32];

    HelAmpLoop hAf1[32];
    HelAmpLoop hAf2[32];

    TreeValue hA05(const int* p);
    TreeValue hA06(const int* p);
    TreeValue hA09(const int* p);
    TreeValue hA010(const int* p);
    TreeValue hA013(const int* p);
    TreeValue hA014(const int* p);
    TreeValue hA017(const int* p);
    TreeValue hA018(const int* p);
    TreeValue hA021(const int* p);
    TreeValue hA022(const int* p);
    TreeValue hA025(const int* p);
    TreeValue hA026(const int* p);

    LoopResult<T> hAL11(const int* p);
    LoopResult<T> hAL12(const int* p);
    LoopResult<T> hAL15(const int* p);
    LoopResult<T> hAL16(const int* p);
    LoopResult<T> hAL19(const int* p);
    LoopResult<T> hAL110(const int* p);
    LoopResult<T> hAL113(const int* p);
    LoopResult<T> hAL114(const int* p);
    LoopResult<T> hAL117(const int* p);
    LoopResult<T> hAL118(const int* p);
    LoopResult<T> hAL121(const int* p);
    LoopResult<T> hAL122(const int* p);
    LoopResult<T> hAL125(const int* p);
    LoopResult<T> hAL126(const int* p);
    LoopResult<T> hAL129(const int* p);
    LoopResult<T> hAL130(const int* p);

    LoopResult<T> hAL21(const int* p);
    LoopResult<T> hAL23(const int* p);
    LoopResult<T> hAL24(const int* p);
    LoopResult<T> hAL26(const int* p);
    LoopResult<T> hAL29(const int* p);
    LoopResult<T> hAL211(const int* p);
    LoopResult<T> hAL212(const int* p);
    LoopResult<T> hAL214(const int* p);
    LoopResult<T> hAL217(const int* p);
    LoopResult<T> hAL219(const int* p);
    LoopResult<T> hAL220(const int* p);
    LoopResult<T> hAL222(const int* p);
    LoopResult<T> hAL225(const int* p);
    LoopResult<T> hAL227(const int* p);
    LoopResult<T> hAL228(const int* p);
    LoopResult<T> hAL230(const int* p);

    LoopResult<T> hAL31(const int* p);
    LoopResult<T> hAL33(const int* p);
    LoopResult<T> hAL35(const int* p);
    LoopResult<T> hAL37(const int* p);
    LoopResult<T> hAL38(const int* p);
    LoopResult<T> hAL310(const int* p);
    LoopResult<T> hAL312(const int* p);
    LoopResult<T> hAL314(const int* p);
    LoopResult<T> hAL317(const int* p);
    LoopResult<T> hAL319(const int* p);
    LoopResult<T> hAL321(const int* p);
    LoopResult<T> hAL323(const int* p);
    LoopResult<T> hAL324(const int* p);
    LoopResult<T> hAL326(const int* p);
    LoopResult<T> hAL328(const int* p);
    LoopResult<T> hAL330(const int* p);

    LoopResult<T> hAL41(const int* p);
    LoopResult<T> hAL43(const int* p);
    LoopResult<T> hAL45(const int* p);
    LoopResult<T> hAL47(const int* p);
    LoopResult<T> hAL49(const int* p);
    LoopResult<T> hAL411(const int* p);
    LoopResult<T> hAL413(const int* p);
    LoopResult<T> hAL415(const int* p);
    LoopResult<T> hAL416(const int* p);
    LoopResult<T> hAL418(const int* p);
    LoopResult<T> hAL420(const int* p);
    LoopResult<T> hAL422(const int* p);
    LoopResult<T> hAL424(const int* p);
    LoopResult<T> hAL426(const int* p);
    LoopResult<T> hAL428(const int* p);
    LoopResult<T> hAL430(const int* p);

    LoopResult<T> hAf11(const int* p);
    LoopResult<T> hAf12(const int* p);
    LoopResult<T> hAf15(const int* p);
    LoopResult<T> hAf16(const int* p);
    LoopResult<T> hAf19(const int* p);
    LoopResult<T> hAf110(const int* p);
    LoopResult<T> hAf113(const int* p);
    LoopResult<T> hAf114(const int* p);
    LoopResult<T> hAf117(const int* p);
    LoopResult<T> hAf118(const int* p);
    LoopResult<T> hAf121(const int* p);
    LoopResult<T> hAf122(const int* p);
    LoopResult<T> hAf125(const int* p);
    LoopResult<T> hAf126(const int* p);
    LoopResult<T> hAf129(const int* p);
    LoopResult<T> hAf130(const int* p);

    LoopResult<T> hAf21(const int* p);
    LoopResult<T> hAf23(const int* p);
    LoopResult<T> hAf24(const int* p);
    LoopResult<T> hAf26(const int* p);
    LoopResult<T> hAf29(const int* p);
    LoopResult<T> hAf211(const int* p);
    LoopResult<T> hAf212(const int* p);
    LoopResult<T> hAf214(const int* p);
    LoopResult<T> hAf217(const int* p);
    LoopResult<T> hAf219(const int* p);
    LoopResult<T> hAf220(const int* p);
    LoopResult<T> hAf222(const int* p);
    LoopResult<T> hAf225(const int* p);
    LoopResult<T> hAf227(const int* p);
    LoopResult<T> hAf228(const int* p);
    LoopResult<T> hAf230(const int* p);

    // spinor product variables
    std::complex<T> a12, a13, a14, a15, a23, a24, a25, a34, a35, a45;
    std::complex<T> b12, b13, b14, b15, b23, b24, b25, b34, b35, b45;
    // momentum twistor variables
    std::complex<T> x1, x2, x3, x4, x5;
    // 2-particle invariants
    T s12, s23, s34, s45, s15;

    void setxi(const int *ord);
    void setsij(const int *ord);
    void setaij(const int *ord);
};

#endif /* ANALYTIC_2Q3G_ANALYTIC_H */
