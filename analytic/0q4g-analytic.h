/*
* analytic/0q4g-analytic.h
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef ANALYTIC_0Q4G_ANALYTIC_H
#define ANALYTIC_0Q4G_ANALYTIC_H

#include "../chsums/0q4g.h"

template <typename T>
class Amp0q4g_a : public Amp0q4g<T>
{
    typedef Amp0q4g<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    typedef TreeValue (Amp0q4g_a::*HelAmp)(const int* ord);
    typedef LoopResult<T> (Amp0q4g_a::*HelAmpLoop)(const int* ord);

    Amp0q4g_a(const T scalefactor);

  protected:
    using BaseClass::mhel;
    using BaseClass::mhelint;

    using BaseClass::njetan;
    using BaseClass::callTree;
    using BaseClass::callLoop;
    using BaseClass::sA;
    using BaseClass::sB;
    using BaseClass::CyclicSpinorsA;
    using BaseClass::CyclicSpinorsB;
    using BaseClass::HelicityOrder;
    using BaseClass::lS;

    using BaseClass::Nc;

    T MuR2() { return njetan->getMuR2(); }

    virtual TreeValue A0(int p0, int p1, int p2, int p3);
    virtual LoopResult<T> AL(int p0, int p1, int p2, int p3, int pos=0);
    virtual LoopResult<T> AF(int p0, int p1, int p2, int p3, int pos=0);

    HelAmp hA0[16];
    HelAmpLoop hAg[16];
    HelAmpLoop hAf[16];

    TreeValue hA03(const int* ord);
    TreeValue hA05(const int* ord);
    TreeValue hA06(const int* ord);
    TreeValue hA09(const int* ord);
    TreeValue hA010(const int* ord);
    TreeValue hA012(const int* ord);

    LoopResult<T> hAg0(const int* ord);
    LoopResult<T> hAg1(const int* ord);
    LoopResult<T> hAg2(const int* ord);
    LoopResult<T> hAg3(const int* ord);
    LoopResult<T> hAg4(const int* ord);
    LoopResult<T> hAg5(const int* ord);
    LoopResult<T> hAg6(const int* ord);
    LoopResult<T> hAg7(const int* ord);
    LoopResult<T> hAg8(const int* ord);
    LoopResult<T> hAg9(const int* ord);
    LoopResult<T> hAg10(const int* ord);
    LoopResult<T> hAg11(const int* ord);
    LoopResult<T> hAg12(const int* ord);
    LoopResult<T> hAg13(const int* ord);
    LoopResult<T> hAg14(const int* ord);
    LoopResult<T> hAg15(const int* ord);

    LoopResult<T> hAf0(const int* ord);
    LoopResult<T> hAf1(const int* ord);
    LoopResult<T> hAf2(const int* ord);
    LoopResult<T> hAf3(const int* ord);
    LoopResult<T> hAf4(const int* ord);
    LoopResult<T> hAf5(const int* ord);
    LoopResult<T> hAf6(const int* ord);
    LoopResult<T> hAf7(const int* ord);
    LoopResult<T> hAf8(const int* ord);
    LoopResult<T> hAf9(const int* ord);
    LoopResult<T> hAf10(const int* ord);
    LoopResult<T> hAf11(const int* ord);
    LoopResult<T> hAf12(const int* ord);
    LoopResult<T> hAf13(const int* ord);
    LoopResult<T> hAf14(const int* ord);
    LoopResult<T> hAf15(const int* ord);

};

#endif /* ANALYTIC_0Q4G_ANALYTIC_H */
