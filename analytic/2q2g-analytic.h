/*
* analytic/2q2g-analytic.h
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef ANALYTIC_2Q2G_ANALYTIC_H
#define ANALYTIC_2Q2G_ANALYTIC_H

#include "../chsums/2q2g.h"

template <typename T>
class Amp2q2g_a : public Amp2q2g<T>
{
    typedef Amp2q2g<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    typedef TreeValue (Amp2q2g_a::*HelAmp)(const int* ord);
    typedef LoopResult<T> (Amp2q2g_a::*HelAmpLoop)(const int* ord);

    Amp2q2g_a(const T scalefactor);

  protected:
    using BaseClass::mhel;
    using BaseClass::mhelint;

    using BaseClass::njetan;
    using BaseClass::callTree;
    using BaseClass::callLoop;
    using BaseClass::sA;
    using BaseClass::sB;
    using BaseClass::CyclicSpinorsA;
    using BaseClass::CyclicSpinorsB;
    using BaseClass::HelicityOrder;
    using BaseClass::lS;
    using BaseClass::getFlav;

    using BaseClass::Nc;

    T MuR2() { return njetan->getMuR2(); }

    virtual TreeValue A0(int p0, int p1, int p2, int p3);
    virtual LoopResult<T> AL(int p0, int p1, int p2, int p3);
    virtual LoopResult<T> AF(int p0, int p1, int p2, int p3);

    HelAmp hA0[16];
    HelAmpLoop hAL1[16];
    HelAmpLoop hAL2[16];
    HelAmpLoop hAL3[16];
    HelAmpLoop hAf1[16];

    TreeValue hA05(const int* ord);
    TreeValue hA06(const int* ord);
    TreeValue hA09(const int* ord);
    TreeValue hA010(const int* ord);

    LoopResult<T> hAL11(const int* ord);
    LoopResult<T> hAL12(const int* ord);
    LoopResult<T> hAL15(const int* ord);
    LoopResult<T> hAL16(const int* ord);
    LoopResult<T> hAL19(const int* ord);
    LoopResult<T> hAL110(const int* ord);
    LoopResult<T> hAL113(const int* ord);
    LoopResult<T> hAL114(const int* ord);

    LoopResult<T> hAL21(const int* ord);
    LoopResult<T> hAL23(const int* ord);
    LoopResult<T> hAL24(const int* ord);
    LoopResult<T> hAL26(const int* ord);
    LoopResult<T> hAL29(const int* ord);
    LoopResult<T> hAL211(const int* ord);
    LoopResult<T> hAL212(const int* ord);
    LoopResult<T> hAL214(const int* ord);

    LoopResult<T> hAL31(const int* ord);
    LoopResult<T> hAL33(const int* ord);
    LoopResult<T> hAL35(const int* ord);
    LoopResult<T> hAL37(const int* ord);
    LoopResult<T> hAL38(const int* ord);
    LoopResult<T> hAL310(const int* ord);
    LoopResult<T> hAL312(const int* ord);
    LoopResult<T> hAL314(const int* ord);

    LoopResult<T> hAf11(const int* ord);
    LoopResult<T> hAf12(const int* ord);
    LoopResult<T> hAf15(const int* ord);
    LoopResult<T> hAf16(const int* ord);
    LoopResult<T> hAf19(const int* ord);
    LoopResult<T> hAf110(const int* ord);
    LoopResult<T> hAf113(const int* ord);
    LoopResult<T> hAf114(const int* ord);

};

#endif /* ANALYTIC_2Q2G_ANALYTIC_H */
