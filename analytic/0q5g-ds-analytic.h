/*
* analytic/0q5g-ds-analytic.h
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef ANALYTIC_0Q5G_DS_ANALYTIC_H
#define ANALYTIC_0Q5G_DS_ANALYTIC_H

#include "0q5g-analytic.h"

// desymmetrized Amp0q5g_ds3_a

template <typename T>
class Amp0q5g_ds3_a : public Amp0q5g_a<T>
{
    typedef Amp0q5g_a<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;
    typedef typename BaseClass::ST ST;

    Amp0q5g_ds3_a(const T scalefactor,
                  const int mFC=1, const NJetAmpTables& tables=amptables());

    void setNc(const ST Nc_);

  protected:
    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp0q5g_ds3Static>();
    }

    void getfvpart1ds(const int fv, LoopValue* fvpart);
    void getfvpart1ds(const int fv, LoopResult<T>* fvpart);
};

#endif /* ANALYTIC_0Q5G_DS_ANALYTIC_H */
