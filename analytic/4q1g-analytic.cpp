/*
* analytic/4q1g-analytic.cpp
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

// vim: set foldmethod=marker:

#include <cassert>
#include "4q1g-analytic.h"

template <typename T>
Amp4q1g_a<T>::Amp4q1g_a(const T scalefactor, const int mFC,
                        const NJetAmpTables& tables)
  : BaseClass(scalefactor, mFC, tables), hA013()
{
  njetan = new NJetAnalytic<T>(scalefactor, BaseClass::legsMOM());
  assert(int(sizeof(hA013)/sizeof(hA013[0])) >= (1 << njetan->legsMOM()));
  assert(int(sizeof(hA014)/sizeof(hA014[0])) >= (1 << njetan->legsMOM()));
  assert(int(sizeof(hAL13)/sizeof(hAL13[0])) >= (1 << njetan->legsMOM()));
  assert(int(sizeof(hAL14)/sizeof(hAL14[0])) >= (1 << njetan->legsMOM()));
//   BaseClass::clearNG();

  hA013[5] = &Amp4q1g_a<T>::hA0135;
  hA013[6] = &Amp4q1g_a<T>::hA0136;
  hA013[9] = &Amp4q1g_a<T>::hA0139;
  hA013[10] = &Amp4q1g_a<T>::hA01310;
  hA013[21] = &Amp4q1g_a<T>::hA01321;
  hA013[22] = &Amp4q1g_a<T>::hA01322;
  hA013[25] = &Amp4q1g_a<T>::hA01325;
  hA013[26] = &Amp4q1g_a<T>::hA01326;

  hA014[5] = &Amp4q1g_a<T>::hA0145;
  hA014[6] = &Amp4q1g_a<T>::hA0146;
  hA014[13] = &Amp4q1g_a<T>::hA01413;
  hA014[14] = &Amp4q1g_a<T>::hA01414;
  hA014[17] = &Amp4q1g_a<T>::hA01417;
  hA014[18] = &Amp4q1g_a<T>::hA01418;
  hA014[25] = &Amp4q1g_a<T>::hA01425;
  hA014[26] = &Amp4q1g_a<T>::hA01426;

  hAL13[5] = &Amp4q1g_a<T>::hAL135;
  hAL13[6] = &Amp4q1g_a<T>::hAL136;
  hAL13[9] = &Amp4q1g_a<T>::hAL139;
  hAL13[10] = &Amp4q1g_a<T>::hAL1310;
  hAL13[21] = &Amp4q1g_a<T>::hAL1321;
  hAL13[22] = &Amp4q1g_a<T>::hAL1322;
  hAL13[25] = &Amp4q1g_a<T>::hAL1325;
  hAL13[26] = &Amp4q1g_a<T>::hAL1326;

  hAL14[5] = &Amp4q1g_a<T>::hAL145;
  hAL14[6] = &Amp4q1g_a<T>::hAL146;
  hAL14[13] = &Amp4q1g_a<T>::hAL1413;
  hAL14[14] = &Amp4q1g_a<T>::hAL1414;
  hAL14[17] = &Amp4q1g_a<T>::hAL1417;
  hAL14[18] = &Amp4q1g_a<T>::hAL1418;
  hAL14[25] = &Amp4q1g_a<T>::hAL1425;
  hAL14[26] = &Amp4q1g_a<T>::hAL1426;

  hAL32[3] = &Amp4q1g_a<T>::hAL323;
  hAL32[5] = &Amp4q1g_a<T>::hAL325;
  hAL32[10] = &Amp4q1g_a<T>::hAL3210;
  hAL32[12] = &Amp4q1g_a<T>::hAL3212;
  hAL32[19] = &Amp4q1g_a<T>::hAL3219;
  hAL32[21] = &Amp4q1g_a<T>::hAL3221;
  hAL32[26] = &Amp4q1g_a<T>::hAL3226;
  hAL32[28] = &Amp4q1g_a<T>::hAL3228;

  hAL42[3] = &Amp4q1g_a<T>::hAL423;
  hAL42[5] = &Amp4q1g_a<T>::hAL425;
  hAL42[11] = &Amp4q1g_a<T>::hAL4211;
  hAL42[13] = &Amp4q1g_a<T>::hAL4213;
  hAL42[18] = &Amp4q1g_a<T>::hAL4218;
  hAL42[20] = &Amp4q1g_a<T>::hAL4220;
  hAL42[26] = &Amp4q1g_a<T>::hAL4226;
  hAL42[28] = &Amp4q1g_a<T>::hAL4228;

  hAL43[3] = &Amp4q1g_a<T>::hAL433;
  hAL43[7] = &Amp4q1g_a<T>::hAL437;
  hAL43[9] = &Amp4q1g_a<T>::hAL439;
  hAL43[13] = &Amp4q1g_a<T>::hAL4313;
  hAL43[18] = &Amp4q1g_a<T>::hAL4318;
  hAL43[22] = &Amp4q1g_a<T>::hAL4322;
  hAL43[24] = &Amp4q1g_a<T>::hAL4324;
  hAL43[28] = &Amp4q1g_a<T>::hAL4328;

  hAf13[5] = &Amp4q1g_a<T>::hAf135;
  hAf13[6] = &Amp4q1g_a<T>::hAf136;
  hAf13[9] = &Amp4q1g_a<T>::hAf139;
  hAf13[10] = &Amp4q1g_a<T>::hAf1310;
  hAf13[21] = &Amp4q1g_a<T>::hAf1321;
  hAf13[22] = &Amp4q1g_a<T>::hAf1322;
  hAf13[25] = &Amp4q1g_a<T>::hAf1325;
  hAf13[26] = &Amp4q1g_a<T>::hAf1326;

  hAf14[5] = &Amp4q1g_a<T>::hAf145;
  hAf14[6] = &Amp4q1g_a<T>::hAf146;
  hAf14[13] = &Amp4q1g_a<T>::hAf1413;
  hAf14[14] = &Amp4q1g_a<T>::hAf1414;
  hAf14[17] = &Amp4q1g_a<T>::hAf1417;
  hAf14[18] = &Amp4q1g_a<T>::hAf1418;
  hAf14[25] = &Amp4q1g_a<T>::hAf1425;
  hAf14[26] = &Amp4q1g_a<T>::hAf1426;

}

template <typename T>
void Amp4q1g_a<T>::setsij(const int *p)
{
  s12 = lS(p[0], p[1]);
  s23 = lS(p[1], p[2]);
  s34 = lS(p[2], p[3]);
  s45 = lS(p[3], p[4]);
  s15 = lS(p[4], p[0]);
}

template <typename T>
void Amp4q1g_a<T>::setaij(const int *p)
{
  a12 = sA(p[0], p[1]);
  a13 = sA(p[0], p[2]);
  a14 = sA(p[0], p[3]);
  a15 = sA(p[0], p[4]);
  a23 = sA(p[1], p[2]);
  a24 = sA(p[1], p[3]);
  a25 = sA(p[1], p[4]);
  a34 = sA(p[2], p[3]);
  a35 = sA(p[2], p[4]);
  a45 = sA(p[3], p[4]);

  b12 = sB(p[0], p[1]);
  b13 = sB(p[0], p[2]);
  b14 = sB(p[0], p[3]);
  b15 = sB(p[0], p[4]);
  b23 = sB(p[1], p[2]);
  b24 = sB(p[1], p[3]);
  b25 = sB(p[1], p[4]);
  b34 = sB(p[2], p[3]);
  b35 = sB(p[2], p[4]);
  b45 = sB(p[3], p[4]);
}

template <typename T>
void Amp4q1g_a<T>::setxi(const int *p)
{
  x1 = lS(p[0], p[1]);
  x2 = -sB(p[0], p[1])*sA(p[1],p[2])*sA(p[0],p[3])/sA(p[2],p[3]);
  x3 = -sB(p[0], p[1])*sA(p[1],p[2])*sA(p[0],p[3])*sA(p[0],p[4])/(sA(p[0],p[2])*sA(p[3], p[4]));
  x4 = sA(p[2], p[3])*sB(p[1], p[2])/(sA(p[0], p[3])*sB(p[0], p[1]));
  x5 = -sA(p[2],p[3])*(lS(p[1],p[2]) - lS(p[3],p[4]))/sB(p[0],p[1])/(sA(p[0],p[3])*sA(p[1],p[2]));
}

template <typename T>
typename Amp4q1g_a<T>::TreeValue
Amp4q1g_a<T>::A0(int p0, int p1, int p2, int p3, int p4)
{
  const int np0 = getFperm(mfv, p0);
  const int np1 = getFperm(mfv, p1);
  const int np2 = getFperm(mfv, p2);
  const int np3 = getFperm(mfv, p3);
  const int np4 = getFperm(mfv, p4);
  const int ord[] = {np0, np1, np2, np3, np4};
  const int hel = HelicityOrder(mhelint, ord);
  const int f0 = getFlav(0,p0);
  const int f1 = getFlav(0,p1);
  const int f2 = getFlav(0,p2);
  const int f3 = getFlav(0,p3);
  const int f4 = getFlav(0,p4);
  if (f0 == -1 && f1 == 1 && f3 == 2 && f4 == 0) {
    HelAmp const hamp = hA013[hel];
    if (hamp) {
      return i_*callTree(this, hamp, ord);
    } else {
      return TreeValue(0.);
    }
  } else if (f0 == -1 && f1 == 1 && f4 == 2 && f3 == 0) {
    HelAmp const hamp = hA014[hel];
    if (hamp) {
      return i_*callTree(this, hamp, ord);
    } else {
      return TreeValue(0.);
    }
  } else if (f0 == -1 && f1 == 1 && f4 == 2 && f2 == 0) {
    const int nord[] = {np3, np4, np0, np1, np2};
    const int nhel = HelicityOrder(mhelint, nord);
    HelAmp const hamp = hA013[nhel];
    if (hamp) {
      return i_*callTree(this, hamp, nord);
    } else {
      return TreeValue(0.);
    }
  } else {
    std::cout << "# fallback to numerical" << std::endl;
    return NJetAmp5<T>::A0(p0, p1, p2, p3, p4);
  }
}

template <typename T>
LoopResult<T> Amp4q1g_a<T>::AL(int p0, int p1, int p2, int p3, int p4)
{
  const int np0 = getFperm(mfv, p0);
  const int np1 = getFperm(mfv, p1);
  const int np2 = getFperm(mfv, p2);
  const int np3 = getFperm(mfv, p3);
  const int np4 = getFperm(mfv, p4);
  const int ord[] = {np0, np1, np2, np3, np4};
  const int hel = HelicityOrder(mhelint, ord);
  const int f0 = getFlav(0,p0);
  const int f1 = getFlav(0,p1);
  const int f2 = getFlav(0,p2);
  const int f3 = getFlav(0,p3);
  const int f4 = getFlav(0,p4);
  if (f0 == -1 && f1 == 1 && f3 == 2 && f4 == 0) {
    HelAmpLoop const hamp = hAL13[hel];
    if (hamp) {
      return callLoop(this, hamp, ord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -1 && f1 == 1 && f2 == 2 && f4 == 0) {
    HelAmpLoop const hamp = hAL13[hel];
    if (hamp) {
      return -callLoop(this, hamp, ord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -1 && f1 == 1 && f4 == 2 && f2 == 0) {
    const int nord[] = {np3, np4, np0, np1, np2};
    const int nhel = HelicityOrder(mhelint, nord);
    HelAmpLoop const hamp = hAL13[nhel];
    if (hamp) {
      return callLoop(this, hamp, nord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -1 && f1 == 1 && f3 == 2 && f2 == 0) {
    const int nord[] = {np3, np4, np0, np1, np2};
    const int nhel = HelicityOrder(mhelint, nord);
    HelAmpLoop const hamp = hAL13[nhel];
    if (hamp) {
      return -callLoop(this, hamp, nord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -1 && f1 == 1 && f4 == 2 && f3 == 0) {
    HelAmpLoop const hamp = hAL14[hel];
    if (hamp) {
      return callLoop(this, hamp, ord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -1 && f1 == 1 && f2 == 2 && f3 == 0) {
    HelAmpLoop const hamp = hAL14[hel];
    if (hamp) {
      return -callLoop(this, hamp, ord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -1 && f2 == 1 && f4 == 2 && f1 == 0) {
    const int nord[] = {np3, np4, np0, np1, np2};
    const int nhel = HelicityOrder(mhelint, nord);
    HelAmpLoop const hamp = hAL14[nhel];
    if (hamp) {
      return callLoop(this, hamp, nord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -1 && f2 == 1 && f3 == 2 && f1 == 0) {
    const int nord[] = {np3, np4, np0, np1, np2};
    const int nhel = HelicityOrder(mhelint, nord);
    HelAmpLoop const hamp = hAL14[nhel];
    if (hamp) {
      return -callLoop(this, hamp, nord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -1 && f3 == 1 && f2 == 2 && f4 == 0) {
    HelAmpLoop const hamp = hAL32[hel];
    if (hamp) {
      return callLoop(this, hamp, ord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -1 && f3 == 1 && f1 == 2 && f4 == 0) {
    HelAmpLoop const hamp = hAL32[hel];
    if (hamp) {
      return -callLoop(this, hamp, ord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -2 && f3 == 2 && f2 == 1 && f4 == 0) {
    HelAmpLoop const hamp = hAL32[hel];
    if (hamp) {
      return callLoop(this, hamp, ord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -2 && f3 == 2 && f1 == 1 && f4 == 0) {
    HelAmpLoop const hamp = hAL32[hel];
    if (hamp) {
      return -callLoop(this, hamp, ord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -1 && f4 == 1 && f2 == 2 && f3 == 0) {
    HelAmpLoop const hamp = hAL42[hel];
    if (hamp) {
      return callLoop(this, hamp, ord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -1 && f4 == 1 && f1 == 2 && f3 == 0) {
    HelAmpLoop const hamp = hAL42[hel];
    if (hamp) {
      return -callLoop(this, hamp, ord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -2 && f4 == 2 && f2 == 1 && f3 == 0) {
    HelAmpLoop const hamp = hAL42[hel];
    if (hamp) {
      return callLoop(this, hamp, ord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -2 && f4 == 2 && f1 == 1 && f3 == 0) {
    HelAmpLoop const hamp = hAL42[hel];
    if (hamp) {
      return -callLoop(this, hamp, ord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -1 && f4 == 1 && f3 == 2 && f2 == 0) {
    HelAmpLoop const hamp = hAL43[hel];
    if (hamp) {
      return callLoop(this, hamp, ord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -1 && f4 == 1 && f1 == 2 && f2 == 0) {
    HelAmpLoop const hamp = hAL43[hel];
    if (hamp) {
      return -callLoop(this, hamp, ord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -2 && f4 == 2 && f3 == 1 && f2 == 0) {
    HelAmpLoop const hamp = hAL43[hel];
    if (hamp) {
      return callLoop(this, hamp, ord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -2 && f4 == 2 && f1 == 1 && f2 == 0) {
    HelAmpLoop const hamp = hAL43[hel];
    if (hamp) {
      return -callLoop(this, hamp, ord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -1 && f4 == 1 && f3 == 2 && f1 == 0) {
    const int nord[] = {np4, np3, np2, np1, np0};
    const int nhel = HelicityOrder(mhelint, nord);
    HelAmpLoop const hamp = hAL42[nhel];
    if (hamp) {
      return -callLoop(this, hamp, nord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -1 && f4 == 1 && f2 == 2 && f1 == 0) {
    const int nord[] = {np4, np3, np2, np1, np0};
    const int nhel = HelicityOrder(mhelint, nord);
    HelAmpLoop const hamp = hAL42[nhel];
    if (hamp) {
      return callLoop(this, hamp, nord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -2 && f4 == 2 && f3 == 1 && f1 == 0) {
    const int nord[] = {np4, np3, np2, np1, np0};
    const int nhel = HelicityOrder(mhelint, nord);
    HelAmpLoop const hamp = hAL42[nhel];
    if (hamp) {
      return -callLoop(this, hamp, nord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -2 && f4 == 2 && f2 == 1 && f1 == 0) {
    const int nord[] = {np4, np3, np2, np1, np0};
    const int nhel = HelicityOrder(mhelint, nord);
    HelAmpLoop const hamp = hAL42[nhel];
    if (hamp) {
      return callLoop(this, hamp, nord);
    } else {
      return LoopResult<T>();
    }
  } else {
    std::cout << "# WARNING: falling back on numerical " << f0<<f1<<f2<<f3<<f4 << std::endl;
    return NJetAmp5<T>::AL(p0, p1, p2, p3, p4);
  }
}

template <typename T>
LoopResult<T> Amp4q1g_a<T>::AF(int p0, int p1, int p2, int p3, int p4)
{
  const int np0 = getFperm(mfv, p0);
  const int np1 = getFperm(mfv, p1);
  const int np2 = getFperm(mfv, p2);
  const int np3 = getFperm(mfv, p3);
  const int np4 = getFperm(mfv, p4);
  const int ord[] = {np0, np1, np2, np3, np4};
  const int hel = HelicityOrder(mhelint, ord);
  const int f0 = getFlav(0,p0);
  const int f1 = getFlav(0,p1);
  const int f2 = getFlav(0,p2);
  const int f3 = getFlav(0,p3);
  const int f4 = getFlav(0,p4);
  if (f0 == -1 && f1 == 1 && f3 == 2 && f4 == 0) {
    HelAmpLoop const hamp = hAf13[hel];
    if (hamp) {
      return callLoop(this, hamp, ord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -1 && f1 == 1 && f2 == 2 && f4 == 0) {
    HelAmpLoop const hamp = hAf13[hel];
    if (hamp) {
      return -callLoop(this, hamp, ord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -1 && f1 == 1 && f4 == 2 && f2 == 0) {
    const int nord[] = {np3, np4, np0, np1, np2};
    const int nhel = HelicityOrder(mhelint, nord);
    HelAmpLoop const hamp = hAf13[nhel];
    if (hamp) {
      return callLoop(this, hamp, nord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -1 && f1 == 1 && f3 == 2 && f2 == 0) {
    const int nord[] = {np3, np4, np0, np1, np2};
    const int nhel = HelicityOrder(mhelint, nord);
    HelAmpLoop const hamp = hAf13[nhel];
    if (hamp) {
      return -callLoop(this, hamp, nord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -1 && f1 == 1 && f4 == 2 && f3 == 0) {
    HelAmpLoop const hamp = hAf14[hel];
    if (hamp) {
      return callLoop(this, hamp, ord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -1 && f1 == 1 && f2 == 2 && f3 == 0) {
    HelAmpLoop const hamp = hAf14[hel];
    if (hamp) {
      return -callLoop(this, hamp, ord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -1 && f2 == 1 && f4 == 2 && f1 == 0) {
    const int nord[] = {np3, np4, np0, np1, np2};
    const int nhel = HelicityOrder(mhelint, nord);
    HelAmpLoop const hamp = hAf14[nhel];
    if (hamp) {
      return callLoop(this, hamp, nord);
    } else {
      return LoopResult<T>();
    }
  } else if (f0 == -1 && f2 == 1 && f3 == 2 && f1 == 0) {
    const int nord[] = {np3, np4, np0, np1, np2};
    const int nhel = HelicityOrder(mhelint, nord);
    HelAmpLoop const hamp = hAf14[nhel];
    if (hamp) {
      return -callLoop(this, hamp, nord);
    } else {
      return LoopResult<T>();
    }
  } else {
    std::cout << "# WARNING: falling back on numerical " << f0<<f1<<f2<<f3<<f4 << std::endl;
    return NJetAmp5<T>::AF(p0, p1, p2, p3, p4);
  }
}

//{{{ tree A013
//{{{ 5
template <typename T>
typename Amp4q1g_a<T>::TreeValue
Amp4q1g_a<T>::hA0135(const int* p) /* 1-11-1-1 */
{
  setaij(p);
  const TreeValue tree = b13*b13*b14/(b12*b15*b34*b45);
  return tree;
}
//}}}
//{{{ 6
template <typename T>
typename Amp4q1g_a<T>::TreeValue
Amp4q1g_a<T>::hA0136(const int* p) /* -111-1-1 */
{
  setaij(p);
  const TreeValue tree = -b14*b23*b23/(b12*b15*b34*b45);
  return tree;
}
//}}}
//{{{ 9
template <typename T>
typename Amp4q1g_a<T>::TreeValue
Amp4q1g_a<T>::hA0139(const int* p) /* 1-1-11-1 */
{
  setaij(p);

  const TreeValue tree = -pow(b14, 3)/(b12*b15*b34*b45);
  return tree;
}
//}}}
//{{{ 10
template <typename T>
typename Amp4q1g_a<T>::TreeValue
Amp4q1g_a<T>::hA01310(const int* p) /* -11-11-1 */
{
  setaij(p);
  const TreeValue tree = b14*b24*b24/(b12*b15*b34*b45);
  return tree;
}
//}}}
//{{{ 21
template <typename T>
typename Amp4q1g_a<T>::TreeValue
Amp4q1g_a<T>::hA01321(const int* p) /* 1-11-11 */
{
  setaij(p);
  const TreeValue tree = -a14*a24*a24/(a12*a15*a34*a45);
  return tree;
}
//}}}
//{{{ 22
template <typename T>
typename Amp4q1g_a<T>::TreeValue
Amp4q1g_a<T>::hA01322(const int* p) /* -111-11 */
{
  setaij(p);

  const TreeValue tree = pow(a14, 3)/(a12*a15*a34*a45);
  return tree;
}
//}}}
//{{{ 25
template <typename T>
typename Amp4q1g_a<T>::TreeValue
Amp4q1g_a<T>::hA01325(const int* p) /* 1-1-111 */
{
  setaij(p);
  const TreeValue tree = a14*a23*a23/(a12*a15*a34*a45);
  return tree;
}
//}}}
//{{{ 26
template <typename T>
typename Amp4q1g_a<T>::TreeValue
Amp4q1g_a<T>::hA01326(const int* p) /* -11-111 */
{
  setaij(p);
  const TreeValue tree = -a13*a13*a14/(a12*a15*a34*a45);
  return tree;
}
//}}}
//}}}

//{{{ tree A014
//{{{ 5
template <typename T>
typename Amp4q1g_a<T>::TreeValue
Amp4q1g_a<T>::hA0145(const int* p) /* 1-11-1-1 */
{
  setaij(p);
  const TreeValue tree = b13*b13/(b12*b34*b45);
  return tree;
}
//}}}
//{{{ 6
template <typename T>
typename Amp4q1g_a<T>::TreeValue
Amp4q1g_a<T>::hA0146(const int* p) /* -111-1-1 */
{
  setaij(p);
  const TreeValue tree = -b23*b23/(b12*b34*b45);
  return tree;
}
//}}}
//{{{ 13
template <typename T>
typename Amp4q1g_a<T>::TreeValue
Amp4q1g_a<T>::hA01413(const int* p) /* 1-111-1 */
{
  setaij(p);
  const TreeValue tree = -a25*a25/(a12*a34*a45);
  return tree;
}
//}}}
//{{{ 14
template <typename T>
typename Amp4q1g_a<T>::TreeValue
Amp4q1g_a<T>::hA01414(const int* p) /* -1111-1 */
{
  setaij(p);
  const TreeValue tree = a15*a15/(a12*a34*a45);
  return tree;
}
//}}}
//{{{ 17
template <typename T>
typename Amp4q1g_a<T>::TreeValue
Amp4q1g_a<T>::hA01417(const int* p) /* 1-1-1-11 */
{
  setaij(p);
  const TreeValue tree = -b15*b15/(b12*b34*b45);
  return tree;
}
//}}}
//{{{ 18
template <typename T>
typename Amp4q1g_a<T>::TreeValue
Amp4q1g_a<T>::hA01418(const int* p) /* -11-1-11 */
{
  setaij(p);
  const TreeValue tree = b25*b25/(b12*b34*b45);
  return tree;
}
//}}}
//{{{ 25
template <typename T>
typename Amp4q1g_a<T>::TreeValue
Amp4q1g_a<T>::hA01425(const int* p) /* 1-1-111 */
{
  setaij(p);
  const TreeValue tree = a23*a23/(a12*a34*a45);
  return tree;
}
//}}}
//{{{ 26
template <typename T>
typename Amp4q1g_a<T>::TreeValue
Amp4q1g_a<T>::hA01426(const int* p) /* -11-111 */
{
  setaij(p);
  const TreeValue tree = -a13*a13/(a12*a34*a45);
  return tree;
}
//}}}
//}}}

//{{{ AL13
//{{{ 5
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL135(const int* p) /* 1-11-1-1 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*sign(lS(2,3))*hAL1326(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 6
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL136(const int* p) /* -111-1-1 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*sign(lS(2,3))*hAL1325(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 9
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL139(const int* p) /* 1-1-11-1 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*sign(lS(2,3))*hAL1322(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 10
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL1310(const int* p) /* -11-11-1 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*sign(lS(2,3))*hAL1321(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 21
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL1321(const int* p) /* 1-11-11 */
{
  setxi(p);
  setsij(p);
  setaij(p);

  const TreeValue phase = -i_*a14*a24*a24/(a12*a15*a34*a45);
  TreeValue t1 = x1 + x2;
  const TreeValue t2 = x2*x2;
  TreeValue t3 = t2*t2;
  const TreeValue t4 = x2*t2;
  const TreeValue t5 = pow(t1, -2);
  TreeValue t6 = x1*T(0.5);
  const TreeValue c1234 = -t6*t4*x4*t5;
  TreeValue t7 = -T(1.) + x5;
  const TreeValue t8 = x2 + x3;
  const TreeValue t9 = x2*x3;
  TreeValue t10 = x1*t8;
  TreeValue t11 = T(2.)*t9 + T(2.)*t10;
  TreeValue t12 = T(1.)/t11;
  const TreeValue t13 = x3*x3;
  const TreeValue c1235 = x1*t1*t13*t7*t12;
  t1 = x4 - x5;
  const TreeValue c1245 = t9*t1*t7*T(0.5);
  TreeValue t14 = -T(1.) + x4;
  const TreeValue t15 = x3*t14;
  TreeValue t16 = x1*(t15 + x2*x4);
  TreeValue t17 = t9*t1;
  const TreeValue t18 = t16 + t17;
  const TreeValue t19 = T(1.)/t8;
  const TreeValue t20 = t19*t19;
  TreeValue t21 = t19*t20;
  const TreeValue c1345 = -x2*t3*t18*t1*t5*t21*T(0.5);
  const TreeValue c2345 = -x4*t18*T(0.5);
  const TreeValue t22 = x1*x1;
  const TreeValue t23 = x1*t22;
  TreeValue t24 = T(2.)*t2;
  const TreeValue t25 = x1*t2;
  t11 = t11*T(0.5);
  const TreeValue t26 = T(1.)/t11;
  const TreeValue c123 = t6*(t25*(x2 + T(4.)*x3) + (t23 + (T(3.)*t22 + t24)*x2)*x3)*t5*t26;
  const TreeValue t27 = T(2.)*x2;
  t6 = t6*x2*(x1 + t27);
  const TreeValue c124 = -t6*x5*t5;
  const TreeValue t28 = t9*x1;
  const TreeValue c125 = t28*t7*t12;
  t7 = T(3.)*t2;
  const TreeValue t29 = T(3.)*t9;
  const TreeValue t30 = t7 + t29 + t13;
  const TreeValue t31 = x2*t1;
  const TreeValue t32 = (-x1 + t31)*x3;
  const TreeValue c134 = -t32*t2*t30*t5*t21*T(0.5);
  const TreeValue t33 = T(3.)*x3;
  const TreeValue t34 = T(2.)*t4;
  const TreeValue t35 = t8*t8;
  const TreeValue t36 = t8*t35;
  const TreeValue t37 = T(3.)*x2;
  const TreeValue t38 = t22*t36;
  const TreeValue t39 = T(1.)/x2;
  const TreeValue c135 = -(t25*(-t3 + (t34 + (T(9.)*t2 + (T(9.)*x2 + t33)*x3)*x3)*x3)*T(0.5)
    + (t38*(x1 + t37) + t4*t30*x3)*x3*T(0.5))*(t10*t14 + t17)*t39*t5*t21*t26;
  t3 = t27*x1*t36;
  t17 = t2*x3;
  const TreeValue c145 = -t31*(t38 + t3 + t17*t30)*t5*t21*T(0.5);
  const TreeValue c234 = -t6*x4*t5;
  const TreeValue c235 = x1*(t9*t14 + t16)*t12;
//  c245 = T(0.);
  t6 = t37 + x3;
  const TreeValue c345 = (t38*T(0.5) + t3*T(0.5) + t2*(t34 + (t7 + t6*x3)*x3)*T(0.5))*t18*t39*t5*t21;
  const TreeValue c2_4512_0 = -(T(4.)/T(3.)*t22*t35 + T(5.)/T(3.)*x1*x2*t35
    + t2*(t24 + (T(19.)*x2 + T(11.)*x3)*x3)/T(6.))*t5*t20;
  t3 = T(2.)*x3;
  const TreeValue c2_4512_1 = t17*(t37 + t3)*x4*t5*t20;
  t12 = x4*x4;
  const TreeValue c2_4512_2 = -t17*t12*t5*t19*T(0.5);
//  c2_4512_3 = T(0.);
  t16 = x1*x2*t5;
  const TreeValue c2_2345_0 = -t16;
  t17 = T(1.)/t1;
  t21 = t22*t5;
  const TreeValue c2_2345_1 = t21*t17;
//  c2_2345_2 = T(0.);
//  c2_2345_3 = T(0.);
  t17 = t22*t35;
  t24 = pow(x3, -2);
  const TreeValue c2_1234_0 = x2*(T(11.)*t28*t35 + t7*t13*t6 + t17*(x2 + T(8.)*x3))*t5*t24*t20/T(6.);
  t6 = t32 + t10*x4;
  t6 = T(1.)/t6;
  const TreeValue c2_1234_1 = -t16*(T(3.)*t28*t35*(x2 + t3) + t17*(t2 + t29 + T(3.)*t13)
    + t2*t13*(T(4.)*t2 + (T(6.)*x2 + t33)*x3))*t14*t24*t20*t6;
  t3 = t14*t14;
  t10 = t6*t6;
  const TreeValue c2_1234_2 = t21*t2*t11*(T(3.)*x1*t35 + t9*(t27 + t33))*t3*t24*t19*t10*T(0.5);
  const TreeValue c2_1234_3 = -T(2.)/T(3.)*t23*t4*t11*t11*t14*t3*t5*t24*t6*t10;
  const TreeValue c2_tree = T(1.)/T(3.);
  t3 = T(23.)*x4;
  t6 = T(2.)*x4;
  t10 = T(6.)*x5;
  t11 = T(1.)/t18;
  t14 = T(1.)/T(18.);
  const TreeValue rat = t14*(-t23*t8*(T(3.)*t4*(T(7.) + (-T(9.) + t6)*x4) + (t7*(T(11.) + (-T(13.)
    + t6)*x4) + (x2*(T(9.) - t3) - T(14.)*t15)*x3)*x3) + t4*x3*t13*(T(14.)*x2 + T(5.)*x3)*t1
    + t25*t13*(t2*(-T(12.) + t3 + (T(9.) - t10)*x5) + (x2*(-T(35.) + T(56.)*x4 + (-T(1.) - T(15.)*x5)*x5)
    + (-T(5.) + T(15.)*x4 + T(9.)*t12 - T(10.)*x5 - T(9.)*x5*x5)*x3)*x3) - t9*t22*t8*(t2*(T(6.)*x4*(x4 - T(6.) + x5) + T(33.) - T(9.)*x5)
    + (x2*(T(33.) - T(55.)*x4 - t10) + (T(10.) + T(23.)*x5 + (-T(15.) - T(9.)*x5 - T(9.)*x4)*x4)*x3)*x3))*t5*t24*t19*t11;

  EpsTriplet<T> amp, camp;
  amp += c1234*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  amp += c124*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c134*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
//  amp += c245*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  amp += c2_4512_0*(njetan->L0(s45,s12));
  amp += c2_4512_1*(njetan->L1(s45,s12));
  amp += c2_4512_2*(njetan->L2hat(s45,s12));
//  amp += c2_4512_3*(njetan->L3hat(s45,s12));
  amp += c2_2345_0*(njetan->L0(s23,s45));
  amp += c2_2345_1*(njetan->L1(s23,s45));
//  amp += c2_2345_2*(njetan->L2hat(s23,s45));
//  amp += c2_2345_3*(njetan->L3hat(s23,s45));
  amp += c2_1234_0*(njetan->L0(s12,s34));
  amp += c2_1234_1*(njetan->L1(s12,s34));
  amp += c2_1234_2*(njetan->L2hat(s12,s34));
  amp += c2_1234_3*(njetan->L3hat(s12,s34));
  amp += c2_tree*(njetan->I2(s45, T(0.), T(0.))+njetan->I2(s12, T(0.), T(0.)));
  amp += rat;

  camp += conj(c1234)*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  camp += conj(c124)*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c134)*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
//  camp += conj(c245)*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  camp += conj(c2_4512_0)*(njetan->L0(s45,s12));
  camp += conj(c2_4512_1)*(njetan->L1(s45,s12));
  camp += conj(c2_4512_2)*(njetan->L2hat(s45,s12));
//  camp += conj(c2_4512_3)*(njetan->L3hat(s45,s12));
  camp += conj(c2_2345_0)*(njetan->L0(s23,s45));
  camp += conj(c2_2345_1)*(njetan->L1(s23,s45));
//  camp += conj(c2_2345_2)*(njetan->L2hat(s23,s45));
//  camp += conj(c2_2345_3)*(njetan->L3hat(s23,s45));
  camp += conj(c2_1234_0)*(njetan->L0(s12,s34));
  camp += conj(c2_1234_1)*(njetan->L1(s12,s34));
  camp += conj(c2_1234_2)*(njetan->L2hat(s12,s34));
  camp += conj(c2_1234_3)*(njetan->L3hat(s12,s34));
  camp += conj(c2_tree)*(njetan->I2(s45, T(0.), T(0.))+njetan->I2(s12, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//{{{ 22
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL1322(const int* p) /* -111-11 */
{
  setxi(p);
  setsij(p);
  setaij(p);

  const TreeValue phase = i_*pow(a14, 3)/(a12*a15*a34*a45);
  TreeValue t1 = x1*x2;
  TreeValue t2 = t1*T(0.5);
  const TreeValue c1234 = -t2*x4;
  TreeValue t3 = x1 + x2;
  TreeValue t4 = -T(1.) + x5;
  const TreeValue t5 = x2 + x3;
  const TreeValue t6 = x2*x3;
  TreeValue t7 = x1*t5;
  TreeValue t8 = t6 + t7;
  t8 = T(1.)/t8;
  TreeValue t9 = x3*x3;
  const TreeValue t10 = t9*t9;
  const TreeValue t11 = x3*t9;
  TreeValue t12 = t8*t8;
  const TreeValue t13 = t8*t12;
  TreeValue t14 = t3*t3;
  TreeValue t15 = x1*T(0.5);
  const TreeValue c1235 = t15*t3*t14*t10*t4*t13;
  const TreeValue t16 = x4 - x5;
  TreeValue t17 = t6*t16*T(0.5);
  const TreeValue c1245 = t17*t4;
  const TreeValue t18 = -T(1.) + x4;
  const TreeValue t19 = t6*t16;
  const TreeValue t20 = x1*(x3*t18 + x2*x4);
  const TreeValue t21 = t20 + t19;
  const TreeValue t22 = T(1.)/t5;
  const TreeValue t23 = x2*x2;
  const TreeValue t24 = t23*t23;
  const TreeValue t25 = x2*t23;
  const TreeValue t26 = t22*t22;
  const TreeValue t27 = t22*t26;
  const TreeValue t28 = t25*T(0.5);
  const TreeValue c1345 = -t28*t21*t16*t27;
  const TreeValue c2345 = -x4*t21*T(0.5);
  TreeValue t29 = T(2.)*x3;
  TreeValue t30 = x2 + t29;
  const TreeValue t31 = T(3.)*x2;
  t29 = t31 + t29;
  const TreeValue t32 = T(3.)*t23;
  const TreeValue t33 = x1*x1;
  const TreeValue t34 = x1*t33;
  const TreeValue t35 = t32*x1*t9;
  const TreeValue t36 = T(3.)*t6;
  const TreeValue t37 = t36*t33;
  const TreeValue t38 = T(2.)*t25;
  const TreeValue c123 = t15*(t38*t11 + t35*t30 + t37*(t23 + T(2.)*t6 + T(2.)*t9) + t34*(t25 + (t32 + t29*x3)*x3))*t13;
//  c124 = T(0.);
  const TreeValue t39 = T(3.)*t9;
  const TreeValue t40 = T(3.)*x3;
  const TreeValue t41 = t32*t9;
  t30 = t41 + t40*t1*t30 + t33*(t23 + t36 + t39);
  t2 = t2*x3;
  const TreeValue c125 = t2*t30*t4*t13;
  t4 = t32 + t36 + t9;
  const TreeValue t42 = x3*T(0.5);
  const TreeValue c134 = t42*t4*(x1 - x2*t16)*t27;
  const TreeValue t43 = -t31 - x3;
  const TreeValue t44 = t43*x3;
  const TreeValue t45 = t5*t5;
  const TreeValue t46 = T(1.)/x2;
  const TreeValue c135 = (-t25*t10*t4*T(0.5) + t34*t5*t45*(t25 - t11)*T(0.5)
    + t37*t45*(t25 - t5*t9)*T(0.5) + t35*(t24 + (-t32 + t44)*t9)*T(0.5))*(t7*t18 + t19)*t46*t27*t13;
  const TreeValue c145 = -t17*t4*t27;
//  c234 = T(0.);
  const TreeValue c235 = t15*t30*(t6*t18 + t20)*t13;
//  c245 = T(0.);
  const TreeValue c345 = (t38*T(0.5) + (t32 - t44)*x3*T(0.5))*t21*t46*t27;
  t4 = T(5.)*x2;
  const TreeValue c2_4512_0 = -t42*(t4 + t40)*t26;
  const TreeValue c2_4512_1 = t29*x3*x4*t26;
  t15 = T(2.)*t5;
  t15 = T(1.)/t15;
  const TreeValue c2_4512_2 = -x3*x4*x4*t15;
//  c2_4512_3 = T(0.);
  t15 = T(5.)*x3;
  const TreeValue c2_1534_0 = -t2*t3*(T(2.)*t1 + t15*t3)*t13;
  t2 = (-x1 + x2*t16)*x3 + t7*x4;
  t2 = T(1.)/t2;
  t17 = t33*t23;
  const TreeValue c2_1534_1 = t17*t3*x3*(t1 + t40*t3)*t13*t2;
  t3 = t2*t2;
  const TreeValue c2_1534_2 = -t28*t34*t14*t9*t13*t3;
//  c2_1534_3 = T(0.);
  const TreeValue c2_1234_0 = T(1.)/T(3.) - x2*t43*t26*T(0.5);
  t14 = T(2.)*t23;
  const TreeValue c2_1234_1 = -t1*(t33*t45*(t14 + t36 + t39) + t23*t9*(T(4.)*t23 + (T(6.)*x2
    + t40)*x3) + t1*x3*t5*(T(5.)*t23 + (T(9.)*x2 + T(6.)*x3)*x3))*t18*t26*t12*t2;
  t1 = t18*t18;
  const TreeValue c2_1234_2 = t17*(t7*(x2 + t40) + t6*(T(2.)*x2 + t40))*t1*t22*t8*t3*T(0.5);
  const TreeValue c2_1234_3 = -T(2.)/T(3.)*t34*t25*t18*t1*t2*t3;
  const TreeValue c2_tree = T(1.)/T(3.);
  t1 = T(3.)*x4;
  t2 = T(6.)*x4;
  t3 = T(14.)*x5;
  t7 = T(15.)*x4;
  t8 = T(4.)*x4;
  t9 = T(1.)/t21;
  t12 = -T(1.)/T(18.);
  const TreeValue rat = t12*(t33*t33*t5*(t24*(T(3.) + (-T(23.) + t2)*x4) + (t25*(T(32.) + (-T(92.)
    + T(27.)*x4)*x4) + (t32*(T(26.) + (-T(43.) + T(12.)*x4)*x4) + (t4*(T(9.) + (-T(13.)
    + t1)*x4) - t15*t18)*x3)*x3)*x3) - t24*t10*(T(14.)*x2 + t15)*t16 + t41*t33*t5*(t23*(T(9.)
    + t3 + (-T(40.) + t2)*x4) + (x2*(T(35.) + T(19.)*x5 + (-T(74.) + t7)*x4) + t15*(T(1.) - T(2.)*x4 + x5))*x3)
    + x1*t25*t11*(t14*(T(6.) + T(21.)*x5 + (-T(37.) + t1)*x4) + (x2*(T(35.)
    + T(57.)*x5 + (-T(112.) + t7)*x4) + t15*(T(1.) - t8 + T(3.)*x5))*x3) + t6*t34*t5*(t25*(T(9.) + t3
    + (-T(83.) + T(18.)*x4)*x4) + (t32*(T(41.) + T(11.)*x5 + (-T(86.) + T(21.)*x4)*x4) + (t31*(T(40.)
    + T(8.)*x5 + (-T(68.) + t7)*x4) + t15*(T(3.) - t8 + x5))*x3)*x3))*t22*t13*t9;

  EpsTriplet<T> amp, camp;
  amp += c1234*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
//  amp += c124*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c134*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
//  amp += c245*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  amp += c2_4512_0*(njetan->L0(s45,s12));
  amp += c2_4512_1*(njetan->L1(s45,s12));
  amp += c2_4512_2*(njetan->L2hat(s45,s12));
//  amp += c2_4512_3*(njetan->L3hat(s45,s12));
  amp += c2_1534_0*(njetan->L0(s15,s34));
  amp += c2_1534_1*(njetan->L1(s15,s34));
  amp += c2_1534_2*(njetan->L2hat(s15,s34));
//  amp += c2_1534_3*(njetan->L3hat(s15,s34));
  amp += c2_1234_0*(njetan->L0(s12,s34));
  amp += c2_1234_1*(njetan->L1(s12,s34));
  amp += c2_1234_2*(njetan->L2hat(s12,s34));
  amp += c2_1234_3*(njetan->L3hat(s12,s34));
  amp += c2_tree*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s12, T(0.), T(0.)));
  amp += rat;

  camp += conj(c1234)*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
//  camp += conj(c124)*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c134)*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
//  camp += conj(c245)*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  camp += conj(c2_4512_0)*(njetan->L0(s45,s12));
  camp += conj(c2_4512_1)*(njetan->L1(s45,s12));
  camp += conj(c2_4512_2)*(njetan->L2hat(s45,s12));
//  camp += conj(c2_4512_3)*(njetan->L3hat(s45,s12));
  camp += conj(c2_1534_0)*(njetan->L0(s15,s34));
  camp += conj(c2_1534_1)*(njetan->L1(s15,s34));
  camp += conj(c2_1534_2)*(njetan->L2hat(s15,s34));
//  camp += conj(c2_1534_3)*(njetan->L3hat(s15,s34));
  camp += conj(c2_1234_0)*(njetan->L0(s12,s34));
  camp += conj(c2_1234_1)*(njetan->L1(s12,s34));
  camp += conj(c2_1234_2)*(njetan->L2hat(s12,s34));
  camp += conj(c2_1234_3)*(njetan->L3hat(s12,s34));
  camp += conj(c2_tree)*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s12, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//{{{ 25
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL1325(const int* p) /* 1-1-111 */
{
  setxi(p);
  setsij(p);
  setaij(p);

  const TreeValue phase = i_*a14*a23*a23/(a12*a15*a34*a45);
  TreeValue t1 = x2*x4;
  const TreeValue c1234 = -t1*x1*T(0.5);
  const TreeValue t2 = -T(1.) + x5;
  TreeValue t3 = x2 + x3;
  const TreeValue t4 = x2*x3;
  const TreeValue t5 = x1*t3;
  TreeValue t6 = T(2.)*t4 + T(2.)*t5;
  TreeValue t7 = T(1.)/t6;
  const TreeValue t8 = x3*x3;
  const TreeValue c1235 = x1*(x1 + x2)*t8*t2*t7;
  const TreeValue t9 = x4 - x5;
  const TreeValue c1245 = t4*t9*t2*T(0.5);
  const TreeValue t10 = -T(1.) + x4;
  t1 = x1*(x3*t10 + t1);
  TreeValue t11 = t4*t9;
  TreeValue t12 = t1 + t11;
  TreeValue t13 = T(2.)*t3;
  TreeValue t14 = T(1.)/t13;
  const TreeValue c1345 = -x2*t12*t9*t14;
  const TreeValue c2345 = -x4*t12*T(0.5);
  const TreeValue t15 = T(2.)*x3;
  const TreeValue c123 = x1*(T(2.)*t4 + x1*(x2 + t15))*t7;
//  c124 = T(0.);
  const TreeValue t16 = t4*x1;
  const TreeValue c125 = t16*t2*t7;
  const TreeValue c134 = x3*(x1 - x2*t9)*t14;
  const TreeValue t17 = x2*x2;
  t6 = t6*T(0.5);
  const TreeValue t18 = T(1.)/x2;
  TreeValue t19 = T(1.)/t6;
  TreeValue t20 = T(1.)/t3;
  const TreeValue c135 = (-x2*t8*T(0.5) + x1*(t17 - t8)*T(0.5))*(t5*t10 + t11)*t18*t20*t19;
  const TreeValue c145 = -t11*t14;
//  c234 = T(0.);
  const TreeValue c235 = x1*(t4*t10 + t1)*t7;
//  c245 = T(0.);
  t1 = T(2.)*x2;
  t7 = t1 + x3;
  const TreeValue c345 = t7*t12*t18*t20*T(0.5);
  t11 = T(6.)*x3;
  t14 = x1*x1;
  t19 = x1*t14;
  t20 = t14*t3;
  const TreeValue t21 = T(1.)/T(12.);
  const TreeValue t22 = t18*t18;
  const TreeValue t23 = pow(x3, -2);
  const TreeValue c2_1234_0 = t21*(T(22.)*t17*t8 + t20*(t1 + t11) + t16*(T(22.)*x2 + T(24.)*x3))*t22*t23;
  t1 = T(2.)*t7;
  t7 = t3*t3;
  t12 = T(1.)/t12;
  const TreeValue c2_1234_1 = -x1*(t14*t7*t13 + T(2.)*t17*t8*t1 + T(6.)*t16*t3*t3)*t10*t22*t23*t12*T(0.5);
  t3 = t10*t10;
  t13 = t12*t12;
  const TreeValue c2_1234_2 = t20*t6*(t4*t1 + t5*(T(6.)*x2 + t15))*t3*t22*t23*t13*T(0.25);
  const TreeValue c2_1234_3 = -T(2.)/T(3.)*t19*t7*t6*t6*t10*t3*t18*t23*t12*t13;
  const TreeValue c2_tree = T(1.)/T(3.);
  t1 = T(2.)*x4;
  t3 = x3*(-T(5.) + t1);
  const TreeValue rat = (T(7.)/T(9.)*x2*t17*x3*t8*t9 + (-t11*t20*(x2*(t1*(x4 - T(6.) + x5)
    + T(11.) - T(3.)*x5) + t3*(-T(2.) + x4 + x5)) + T(2.)*x1*t8*(t17*(-T(12.) + T(23.)*x4
    + (T(9.) - T(6.)*x5)*x5) + (x2*(-T(29.)
    + T(14.)*x4 + (T(30.) - T(15.)*x5)*x5) - T(9.)*t2*t2*x3)*x3))*x2/T(36.) - t19*t7*t10*(x2*(-T(7.)
    + t1) + t3)/T(6.))*t22*t23*t12;

  EpsTriplet<T> amp, camp;
  amp += c1234*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
//  amp += c124*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c134*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
//  amp += c245*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  amp += c2_1234_0*(njetan->L0(s12,s34));
  amp += c2_1234_1*(njetan->L1(s12,s34));
  amp += c2_1234_2*(njetan->L2hat(s12,s34));
  amp += c2_1234_3*(njetan->L3hat(s12,s34));
  amp += c2_tree*(njetan->I2(s12, T(0.), T(0.))+njetan->I2(s34, T(0.), T(0.)));
  amp += rat;

  camp += conj(c1234)*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
//  camp += conj(c124)*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c134)*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
//  camp += conj(c245)*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  camp += conj(c2_1234_0)*(njetan->L0(s12,s34));
  camp += conj(c2_1234_1)*(njetan->L1(s12,s34));
  camp += conj(c2_1234_2)*(njetan->L2hat(s12,s34));
  camp += conj(c2_1234_3)*(njetan->L3hat(s12,s34));
  camp += conj(c2_tree)*(njetan->I2(s12, T(0.), T(0.))+njetan->I2(s34, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//{{{ 26
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL1326(const int* p) /* -11-111 */
{
  setxi(p);
  setsij(p);
  setaij(p);

  const TreeValue phase = -i_*a13*a13*a14/(a12*a15*a34*a45);
  TreeValue t1 = x1*x2;
  TreeValue t2 = t1*x4*T(0.5);
  const TreeValue c1234 = -t2;
  TreeValue t3 = x1 + x2;
  TreeValue t4 = -T(1.) + x5;
  TreeValue t5 = x2 + x3;
  const TreeValue t6 = x2*x3;
  TreeValue t7 = x1*t5;
  TreeValue t8 = t6 + t7;
  t8 = T(1.)/t8;
  const TreeValue t9 = x2*x2;
  TreeValue t10 = t9*t9;
  const TreeValue t11 = x2*t10;
  const TreeValue t12 = x2*t9;
  TreeValue t13 = x3*x3;
  const TreeValue t14 = t13*t13;
  const TreeValue t15 = x3*t13;
  const TreeValue t16 = t8*t8;
  const TreeValue t17 = t8*t16;
  TreeValue t18 = t9*t14;
  const TreeValue c1235 = t18*x1*t3*t4*t17*T(0.5);
  const TreeValue t19 = x4 - x5;
  const TreeValue c1245 = t6*t19*t4*T(0.5);
  const TreeValue t20 = -T(1.) + x4;
  const TreeValue t21 = x2*x4;
  const TreeValue t22 = t6*t19;
  const TreeValue t23 = x1*(x3*t20 + t21);
  const TreeValue t24 = t23 + t22;
  TreeValue t25 = T(2.)*t5;
  t25 = T(1.)/t25;
  const TreeValue t26 = x2*t19;
  const TreeValue c1345 = -t26*t24*t25;
  const TreeValue t27 = T(1.)/t3;
  const TreeValue t28 = t27*t27;
  const TreeValue t29 = t9*T(0.5);
  const TreeValue c2345 = -t29*x4*t24*t28;
  TreeValue t30 = T(3.)*x2;
  const TreeValue t31 = x1*x1;
  const TreeValue t32 = t31*t31;
  const TreeValue t33 = x1*t31;
  const TreeValue t34 = t5*t5;
  const TreeValue t35 = t5*t34;
  const TreeValue t36 = t33*t35;
  const TreeValue t37 = T(3.)*t6;
  const TreeValue t38 = x1*T(0.5);
  const TreeValue c123 = t38*(T(2.)*t12*t15 + t37*t31*t34 + t36 + x1*t9*t13*(t30 + T(4.)*x3))*t17;
//  c124 = T(0.);
  const TreeValue t39 = T(2.)*x3;
  t30 = t30 + t39;
  const TreeValue t40 = T(3.)*x3;
  const TreeValue t41 = t40*t1;
  const TreeValue t42 = t9*t13;
  const TreeValue t43 = x3*t4;
  const TreeValue c125 = t43*t38*(t41*t34 + t31*t35 + t42*t30)*t17;
  const TreeValue c134 = x3*(x1 - x2*t19)*t25;
  const TreeValue t44 = T(2.)*x2;
  const TreeValue t45 = t40*t31;
  const TreeValue t46 = x1*t13;
  const TreeValue t47 = T(1.)/t5;
  const TreeValue c135 = (-t18*T(0.5) + (t45*t34 + t46*(T(3.)*t9 + (t44 - x3)*x3))*x2*T(0.5)
    + t36*T(0.5))*(t7*t20 + t22)*t47*t17;
  const TreeValue c145 = -t22*t25;
  t18 = x1 + t44;
  const TreeValue c234 = -t2*t18*t28;
  t2 = T(3.)*t42;
  const TreeValue c235 = t29*x1*(t2 + t41*(x2 + t39) + t31*(t9 + t37 + T(3.)*t13))*(t6*t20 + t23)*t28*t17;
  const TreeValue c245 = -t38*t18*(t21 + t43)*t28;
  t18 = t44 + x3;
  const TreeValue c345 = ((T(2.)*x1 + t18)*x2*T(0.5) + t31*T(0.5))*t24*t28*t47;
//  c2_2315_0 = T(0.);
  t4 = T(1.)/t4;
  const TreeValue c2_2315_1 = x1*(x3 - t5*x4)*t27*t4/x3;
//  c2_2315_2 = T(0.);
//  c2_2315_3 = T(0.);
  t4 = -T(1.)/T(6.);
  const TreeValue c2_1534_0 = t4*(T(2.)*t10*t15 + T(8.)*t32*t35 + T(7.)*t46*t12*t30 + t44*t33*t34*(x2
    + T(13.)*x3) + t45*t9*(T
(0.4e1)*t9 + (T(17.)*x2 + T(10.)*x3)*x3))*t27*t17;
  t4 = (-x1 + t26)*x3 + t7*x4;
  t4 = T(1.)/t4;
  const TreeValue c2_1534_1 = t31*t10*x3*(t1 + t40*t3)*t27*t17*t4;
  t3 = t4*t4;
  const TreeValue c2_1534_2 = -t33*t11*t13*t17*t3*T(0.5);
//  c2_1534_3 = T(0.);
  const TreeValue c2_1234_0 = T(13.)/T(6.);
  const TreeValue c2_1234_1 = -x1*(T(2.)*t42*t18 + t31*t34*t18 + t1*x3*t5*(T(5.)*x2 + t40))*t20*t16*t4;
  t1 = t20*t20;
  const TreeValue c2_1234_2 = t31*t5*(x1*t34 + t6*t18)*t1*t8*t3*T(0.5);
  const TreeValue c2_1234_3 = -T(2.)/T(3.)*t33*x2*t34*t20*t1*t4*t3;
  const TreeValue c2_tree = T(1.)/T(3.);
  t3 = T(6.)*x4;
  t4 = T(15.)*x4;
  t5 = T(9.)*t1*x3;
  t7 = T(3.)*x4;
  t8 = T(14.)*x5;
  t10 = T(1.)/t24;
  t13 = -T(1.)/T(18.);
  const TreeValue rat = t13*(t32*t35*(t9*(T(3.) + (-T(23.) + t3)*x4) + (x2*(T(29.) + (-T(44.)
    + t4)*x4) + t5)*x3) - T(14.)*t11*t14*t19 + t2*t31*(t12*(T(9.) + t8 + (-T(40.) + t3)*x4)
    + (T(7.)*t9*(T(5.) + T(4.)*x5 + (-T(14.) + t7)*x4) + (t44*(T(19.) + T(7.)*x5 + (-T(38.) + T(12.)*x4)*x4)
    + t5)*x3)*x3) + t6*t33*t34*(t9*(T(9.) + t8 + (-T(83.) + T(18.)*x4)*x4) + (x2*(T(87.)
    + t8 + (-T(146.) + T(45.)*x4)*x4) + T(27.)*t1*x3)*x3) + x1*t12*t15*(T(2.)*t9*(T(6.)
    + T(21.)*x5 + (-T(37.) + t7)*x4) + (x2*(T(29.) + T(42.)*x5 + (-T(86.) + t4)*x4) + t5)*x3))*t17*t10/x2;

  EpsTriplet<T> amp, camp;
  amp += c1234*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
//  amp += c124*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c134*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  amp += c245*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
//  amp += c2_2315_0*(njetan->L0(s23,s15));
  amp += c2_2315_1*(njetan->L1(s23,s15));
//  amp += c2_2315_2*(njetan->L2hat(s23,s15));
//  amp += c2_2315_3*(njetan->L3hat(s23,s15));
  amp += c2_1534_0*(njetan->L0(s15,s34));
  amp += c2_1534_1*(njetan->L1(s15,s34));
  amp += c2_1534_2*(njetan->L2hat(s15,s34));
//  amp += c2_1534_3*(njetan->L3hat(s15,s34));
  amp += c2_1234_0*(njetan->L0(s12,s34));
  amp += c2_1234_1*(njetan->L1(s12,s34));
  amp += c2_1234_2*(njetan->L2hat(s12,s34));
  amp += c2_1234_3*(njetan->L3hat(s12,s34));
  amp += c2_tree*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  amp += rat;

  camp += conj(c1234)*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
//  camp += conj(c124)*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c134)*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  camp += conj(c245)*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
//  camp += conj(c2_2315_0)*(njetan->L0(s23,s15));
  camp += conj(c2_2315_1)*(njetan->L1(s23,s15));
//  camp += conj(c2_2315_2)*(njetan->L2hat(s23,s15));
//  camp += conj(c2_2315_3)*(njetan->L3hat(s23,s15));
  camp += conj(c2_1534_0)*(njetan->L0(s15,s34));
  camp += conj(c2_1534_1)*(njetan->L1(s15,s34));
  camp += conj(c2_1534_2)*(njetan->L2hat(s15,s34));
//  camp += conj(c2_1534_3)*(njetan->L3hat(s15,s34));
  camp += conj(c2_1234_0)*(njetan->L0(s12,s34));
  camp += conj(c2_1234_1)*(njetan->L1(s12,s34));
  camp += conj(c2_1234_2)*(njetan->L2hat(s12,s34));
  camp += conj(c2_1234_3)*(njetan->L3hat(s12,s34));
  camp += conj(c2_tree)*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//}}}

//{{{ AL14
//{{{ 5
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL145(const int* p) /* 1-11-1-1 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*sign(lS(2,3))*hAL1426(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 6
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL146(const int* p) /* -111-1-1 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*sign(lS(2,3))*hAL1425(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 13
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL1413(const int* p) /* 1-111-1 */
{
  setxi(p);
  setsij(p);
  setaij(p);

  const TreeValue phase = i_*a25*a25/(a12*a34*a45);
  TreeValue t1 = x2 + x3;
  TreeValue t2 = x1*t1;
  TreeValue t3 = x2*x3;
  TreeValue t4 = t3 + t2;
  const TreeValue t5 = x3*x3;
  t4 = pow(t4, -2);
  const TreeValue t6 = x2*x2;
  TreeValue t7 = x1*T(0.5);
  const TreeValue c1234 = t7*x2*t6*t5*x4*t4;
  const TreeValue t8 = -T(1.) + x5;
  TreeValue t9 = x3*t8;
  TreeValue t10 = t9*t7;
  const TreeValue c1235 = -t10;
  const TreeValue t11 = x1 + x2;
  const TreeValue t12 = x4 - x5;
  TreeValue t13 = x2*t12;
  const TreeValue t14 = t13*t11*t11*T(0.5);
  const TreeValue c1245 = -t14*x3*t5*t8*t4;
//  c1345 = T(0.);
  const TreeValue t15 = -T(1.) + x4;
  const TreeValue t16 = x2*x4;
  const TreeValue t17 = t13*x3;
  const TreeValue t18 = x1*(x3*t15 + t16);
  const TreeValue c2345 = x4*(t18 + t17)*T(0.5);
  const TreeValue t19 = x1*x1;
  const TreeValue t20 = t1*t1;
  TreeValue t21 = T(2.)*x2;
  const TreeValue t22 = t6*t5;
  const TreeValue t23 = t19*t20;
  const TreeValue c123 = -t7*(T(2.)*t22 + t2*t21*x3 + t23)*t4;
  t7 = t7*x2;
  const TreeValue c124 = t7*(x1 + t21)*t5*x5*t4;
  const TreeValue t24 = T(2.)*x3;
  t21 = t21*x3;
  const TreeValue t25 = t21 + x1*(x2 + t24);
  const TreeValue c125 = -t10*x2*t25*t4;
  const TreeValue c134 = t22*(-x1 + t13)*t4*T(0.5);
  t10 = T(1.)/x2;
  const TreeValue c135 = (t2*t15*T(0.5) + t17*T(0.5))*t10;
  const TreeValue c145 = -t14*t5*t4;
  t13 = t1*x4;
  const TreeValue c234 = t13*t7*(t21 + t2)*t4;
//  c235 = T(0.);
  const TreeValue c245 = -t7*t25*(t16 + t9)*t4;
  t7 = -t12;
  const TreeValue c345 = (x1*(-t13 + x3)*T(0.5) + t3*t7*T(0.5))*t10;
  t3 = t3*t2;
  const TreeValue c2_4512_0 = (T(13.)/T(6.)*t22 + T(7.)/T(3.)*t3 + T(5.)/T(3.)*t23)*t4;
  const TreeValue c2_4512_1 = -t24*t13*t6*t4;
  const TreeValue c2_4512_2 = t6*t20*x4*x4*t4*T(0.5);
//  c2_4512_3 = T(0.);
  t9 = T(1.)/T(3.);
  const TreeValue c2_2345_0 = t9*(t22 + T(5.)*t3 + t19*t1*(T(4.)*x2 + x3))*t4;
  t1 = T(1.)/t7;
  const TreeValue c2_2345_1 = t19*t5*t4*t1;
//  c2_2345_2 = T(0.);
//  c2_2345_3 = T(0.);
  t1 = t2*x2;
  const TreeValue c2_2315_0 = -t1*t11*t4;
  t2 = T(1.)/t8;
  const TreeValue c2_2315_1 = t18*t6*t11*t4*t2/x3;
//  c2_2315_2 = T(0.);
//  c2_2315_3 = T(0.);
  const TreeValue c2_tree = -T(1.)/T(3.);
  t2 = T(9.);
  t2 = (x4 + x5)*t2;
  const TreeValue rat = (-T(5.)/T(18.)*t22 - T(7.)/T(9.)*t23 - t1*(t2*x2 + x3*(t2 + T(10.)))/T(18.))*t4;

  EpsTriplet<T> amp, camp;
  amp += c1234*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  amp += c124*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c134*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  amp += c245*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  amp += c2_4512_0*(njetan->L0(s45,s12));
  amp += c2_4512_1*(njetan->L1(s45,s12));
  amp += c2_4512_2*(njetan->L2hat(s45,s12));
//  amp += c2_4512_3*(njetan->L3hat(s45,s12));
  amp += c2_2345_0*(njetan->L0(s23,s45));
  amp += c2_2345_1*(njetan->L1(s23,s45));
//  amp += c2_2345_2*(njetan->L2hat(s23,s45));
//  amp += c2_2345_3*(njetan->L3hat(s23,s45));
  amp += c2_2315_0*(njetan->L0(s23,s15));
  amp += c2_2315_1*(njetan->L1(s23,s15));
//  amp += c2_2315_2*(njetan->L2hat(s23,s15));
//  amp += c2_2315_3*(njetan->L3hat(s23,s15));
  amp += c2_tree*(njetan->I2(s23, T(0.), T(0.))+njetan->I2(s45, T(0.), T(0.)));
  amp += rat;

  camp += conj(c1234)*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  camp += conj(c124)*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c134)*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  camp += conj(c245)*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  camp += conj(c2_4512_0)*(njetan->L0(s45,s12));
  camp += conj(c2_4512_1)*(njetan->L1(s45,s12));
  camp += conj(c2_4512_2)*(njetan->L2hat(s45,s12));
//  camp += conj(c2_4512_3)*(njetan->L3hat(s45,s12));
  camp += conj(c2_2345_0)*(njetan->L0(s23,s45));
  camp += conj(c2_2345_1)*(njetan->L1(s23,s45));
//  camp += conj(c2_2345_2)*(njetan->L2hat(s23,s45));
//  camp += conj(c2_2345_3)*(njetan->L3hat(s23,s45));
  camp += conj(c2_2315_0)*(njetan->L0(s23,s15));
  camp += conj(c2_2315_1)*(njetan->L1(s23,s15));
//  camp += conj(c2_2315_2)*(njetan->L2hat(s23,s15));
//  camp += conj(c2_2315_3)*(njetan->L3hat(s23,s15));
  camp += conj(c2_tree)*(njetan->I2(s23, T(0.), T(0.))+njetan->I2(s45, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//{{{ 14
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL1414(const int* p) /* -1111-1 */
{
  setxi(p);
  setsij(p);
  setaij(p);

  const TreeValue phase = -i_*a15*a15/(a12*a34*a45);
  TreeValue t1 = x2*x4;
  const TreeValue c1234 = t1*x1*T(0.5);
  TreeValue t2 = -T(1.) + x5;
  const TreeValue c1235 = -x1*x3*t2*T(0.5);
  TreeValue t3 = x4 - x5;
  TreeValue t4 = x2*t3;
  const TreeValue t5 = t4*T(0.5);
  const TreeValue c1245 = -t5*x3*t2;
//  c1345 = T(0.);
  t2 = -T(1.) + x4;
  const TreeValue t6 = t4*x3;
  const TreeValue c2345 = x4*(x1*(x3*t2 + t1) + t6)*T(0.5);
  const TreeValue c123 = -x1;
//  c124 = T(0.);
//  c125 = T(0.);
  const TreeValue c134 = -x1*T(0.5) + t4*T(0.5);
  t1 = x2 + x3;
  t4 = T(1.)/x2;
  const TreeValue c135 = (t1*x1*t2*T(0.5) + t6*T(0.5))*t4;
  const TreeValue c145 = -t5;
//  c234 = T(0.);
//  c235 = T(0.);
//  c245 = T(0.);
  t2 = t1*x4;
  const TreeValue c345 = (x1*(-t2 + x3)*T(0.5) - x2*x3*t3*T(0.5))*t4;
  const TreeValue c2_4512_0 = T(11.)/T(6.);
  t3 = T(1.)/x3;
  const TreeValue c2_4512_1 = -T(2.)*t2*t3;
  const TreeValue c2_4512_2 = t1*t1*x4*x4*t3*t3*T(0.5);
//  c2_4512_3 = T(0.);
  const TreeValue c2_tree = -T(1.)/T(3.);
  const TreeValue rat = -T(5.)/T(18.);

  EpsTriplet<T> amp, camp;
  amp += c1234*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
//  amp += c124*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
//  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c134*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
//  amp += c245*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  amp += c2_4512_0*(njetan->L0(s45,s12));
  amp += c2_4512_1*(njetan->L1(s45,s12));
  amp += c2_4512_2*(njetan->L2hat(s45,s12));
//  amp += c2_4512_3*(njetan->L3hat(s45,s12));
  amp += c2_tree*(njetan->I2(s45, T(0.), T(0.))+njetan->I2(s12, T(0.), T(0.)));
  amp += rat;

  camp += conj(c1234)*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
//  camp += conj(c124)*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
//  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c134)*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
//  camp += conj(c245)*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  camp += conj(c2_4512_0)*(njetan->L0(s45,s12));
  camp += conj(c2_4512_1)*(njetan->L1(s45,s12));
  camp += conj(c2_4512_2)*(njetan->L2hat(s45,s12));
//  camp += conj(c2_4512_3)*(njetan->L3hat(s45,s12));
  camp += conj(c2_tree)*(njetan->I2(s45, T(0.), T(0.))+njetan->I2(s12, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//{{{ 17
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL1417(const int* p) /* 1-1-1-11 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*sign(lS(2,3))*hAL1414(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 18
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL1418(const int* p) /* -11-1-11 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*sign(lS(2,3))*hAL1413(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 25
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL1425(const int* p) /* 1-1-111 */
{
  setxi(p);
  setsij(p);
  setaij(p);

  const TreeValue phase = -i_*a23*a23/(a12*a34*a45);
  TreeValue t1 = x2*x4;
  const TreeValue c1234 = t1*x1*T(0.5);
  const TreeValue t2 = -T(1.) + x5;
  TreeValue t3 = x1*T(0.5);
  const TreeValue c1235 = -t3*x3*t2;
  const TreeValue t4 = x4 - x5;
  TreeValue t5 = x2*x3;
  const TreeValue c1245 = -t5*t4*t2*T(0.5);
//  c1345 = T(0.);
  TreeValue t6 = -T(1.) + x4;
  TreeValue t7 = t5*t4;
  t1 = x1*(x3*t6 + t1) + t7;
  const TreeValue c2345 = x4*t1*T(0.5);
  const TreeValue c123 = -x1;
//  c124 = T(0.);
//  c125 = T(0.);
  TreeValue t8 = x2*t4;
  const TreeValue c134 = -x1*T(0.5) + t8*T(0.5);
  const TreeValue t9 = x2 + x3;
  TreeValue t10 = x1*t9;
  t7 = t10*t6 + t7;
  TreeValue t11 = T(1.)/x2;
  const TreeValue c135 = t7*t11*T(0.5);
  const TreeValue c145 = -t8*T(0.5);
//  c234 = T(0.);
//  c235 = T(0.);
//  c245 = T(0.);
  const TreeValue c345 = (x1*(-t9*x4 + x3)*T(0.5) - t5*t4*T(0.5))*t11;
  t8 = x1*x1;
  const TreeValue t12 = x2*x2;
  const TreeValue t13 = t9*t9;
  const TreeValue t14 = pow(x3, -2);
  t11 = t11*t11;
  const TreeValue c2_1234_0 = (-T(11.)/T(6.)*t12*x3*x3 - T(2.)*t10*t5 - t8*t13*T(0.5))*t11*t14;
  const TreeValue t15 = t5 + t10;
  t10 = T(2.)*t10;
  t1 = T(1.)/t1;
  const TreeValue c2_1234_1 = t3*t9*t15*(T(4.)*t5 + t10)*t6*t11*t14*t1;
  t3 = t15*t15;
  t5 = t6*t6;
  const TreeValue c2_1234_2 = -t8*t13*t3*t5*t11*t14*t1*t1*T(0.5);
//  c2_1234_3 = T(0.);
  const TreeValue c2_tree = -T(1.)/T(3.);
  t6 = T(1.)/t7;
  const TreeValue rat = (-t8*t13*t3*t5*t14*t1*T(0.5) - x2*(T(10.)*t12*x3*t4 + t10*(-T(9.)*x3*t2*t2
    + x2*(-T(14.) + T(5.)*x4 + (T(18.) - T(9.)*x5)*x5)))/T(36.))*t11*t6;

  EpsTriplet<T> amp, camp;
  amp += c1234*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
//  amp += c124*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
//  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c134*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
//  amp += c245*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  amp += c2_1234_0*(njetan->L0(s12,s34));
  amp += c2_1234_1*(njetan->L1(s12,s34));
  amp += c2_1234_2*(njetan->L2hat(s12,s34));
//  amp += c2_1234_3*(njetan->L3hat(s12,s34));
  amp += c2_tree*(njetan->I2(s12, T(0.), T(0.))+njetan->I2(s34, T(0.), T(0.)));
  amp += rat;

  camp += conj(c1234)*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
//  camp += conj(c124)*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
//  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c134)*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
//  camp += conj(c245)*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  camp += conj(c2_1234_0)*(njetan->L0(s12,s34));
  camp += conj(c2_1234_1)*(njetan->L1(s12,s34));
  camp += conj(c2_1234_2)*(njetan->L2hat(s12,s34));
//  camp += conj(c2_1234_3)*(njetan->L3hat(s12,s34));
  camp += conj(c2_tree)*(njetan->I2(s12, T(0.), T(0.))+njetan->I2(s34, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//{{{ 26
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL1426(const int* p) /* -11-111 */
{
  setxi(p);
  setsij(p);
  setaij(p);

  const TreeValue phase = i_*a13*a13/(a12*a34*a45);
  TreeValue t1 = x2*x4;
  TreeValue t2 = t1*x1*T(0.5);
  const TreeValue c1234 = t2;
  TreeValue t3 = -T(1.) + x5;
  const TreeValue t4 = x2 + x3;
  const TreeValue t5 = x1*t4;
  const TreeValue t6 = x2*x3;
  TreeValue t7 = t6 + t5;
  t7 = T(1.)/t7;
  const TreeValue t8 = t7*t7;
  const TreeValue t9 = x3*x3;
  const TreeValue t10 = x2*x2;
  const TreeValue t11 = x1*T(0.5);
  const TreeValue t12 = t11*t10;
  const TreeValue c1235 = -t12*x3*t9*t3*t8;
  const TreeValue t13 = x4 - x5;
  const TreeValue c1245 = -t6*t13*t3*T(0.5);
//  c1345 = T(0.);
  const TreeValue t14 = -T(1.) + x4;
  const TreeValue t15 = x1*(x3*t14 + t1);
  TreeValue t16 = t6*t13;
  const TreeValue t17 = t15 + t16;
  TreeValue t18 = x1 + x2;
  t18 = T(1.)/t18;
  const TreeValue t19 = t18*t18;
  const TreeValue c2345 = t10*x4*t17*t19*T(0.5);
  const TreeValue t20 = x1*x1;
  const TreeValue t21 = t4*t4;
  const TreeValue t22 = t20*t21;
  const TreeValue c123 = -t11*(T(2.)*t10*t9 + T(2.)*t5*t6 + t22)*t8;
//  c124 = T(0.);
  const TreeValue t23 = T(2.)*t6;
  const TreeValue t24 = t23 + t5;
  const TreeValue t25 = x3*t3;
  const TreeValue c125 = -t25*t11*t4*t24*t8;
  const TreeValue t26 = -x1 + x2*t13;
  const TreeValue c134 = t26*T(0.5);
  const TreeValue t27 = t5*t14;
  const TreeValue t28 = x2*T(0.5);
  const TreeValue c135 = t28*t9*(t27 + t16)*t8;
  const TreeValue c145 = -t28*t13;
  t16 = x1 + T(2.)*x2;
  const TreeValue c234 = t2*t16*t19;
  const TreeValue c235 = -t12*(t23 + x1*(x2 + T(2.)*x3))*(t6*t14 + t15)*t19*t8;
  const TreeValue c245 = t11*t16*(t1 + t25)*t19;
  const TreeValue c345 = -t28*t17*t19;
//  c2_2315_0 = T(0.);
  t1 = T(1.)/t3;
  const TreeValue c2_2315_1 = t15*t18*t1/x3;
//  c2_2315_2 = T(0.);
//  c2_2315_3 = T(0.);
  t1 = T(9.)*x3;
  t2 = T(1.)/T(3.);
  const TreeValue c2_1534_0 = t2*(x2*t10*t9 + T(4.)*x1*t20*t21 + x1*t10*x3*(T(5.)*x2 + T(6.)*x3)
    + t20*x2*t4*(x2 + t1))*t18*t8;
  t2 = t26*x3 + t5*x4;
  t2 = T(1.)/t2;
  const TreeValue c2_1534_1 = -t20*t10*t10*x3*t18*t8*t2;
//  c2_1534_2 = T(0.);
//  c2_1534_3 = T(0.);
  const TreeValue c2_1234_0 = -T(13.)/T(6.);
  const TreeValue c2_1234_1 = t27*t24*t7*t2;
  t3 = t14*t14;
  const TreeValue c2_1234_2 = -t22*t3*t2*t2*T(0.5);
//  c2_1234_3 = T(0.);
  const TreeValue c2_tree = -T(1.)/T(3.);
  t2 = T(1.)/t17;
  const TreeValue rat = (x1*(t10*(T(9.) + (-T(23.) + T(9.)*x4)*x4) + (x2*(T(23.) + (-T(41.)
    + T(18.)*x4)*x4) + t1*t3)*x3)/T(18.) - T(5.)/T(18.)*t10*x3*t13)*t2/x2;

  EpsTriplet<T> amp, camp;
  amp += c1234*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
//  amp += c124*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  amp += c134*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  amp += c245*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
//  amp += c2_2315_0*(njetan->L0(s23,s15));
  amp += c2_2315_1*(njetan->L1(s23,s15));
//  amp += c2_2315_2*(njetan->L2hat(s23,s15));
//  amp += c2_2315_3*(njetan->L3hat(s23,s15));
  amp += c2_1534_0*(njetan->L0(s15,s34));
  amp += c2_1534_1*(njetan->L1(s15,s34));
//  amp += c2_1534_2*(njetan->L2hat(s15,s34));
//  amp += c2_1534_3*(njetan->L3hat(s15,s34));
  amp += c2_1234_0*(njetan->L0(s12,s34));
  amp += c2_1234_1*(njetan->L1(s12,s34));
  amp += c2_1234_2*(njetan->L2hat(s12,s34));
//  amp += c2_1234_3*(njetan->L3hat(s12,s34));
  amp += c2_tree*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  amp += rat;

  camp += conj(c1234)*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
//  camp += conj(c124)*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c134)*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  camp += conj(c245)*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
//  camp += conj(c2_2315_0)*(njetan->L0(s23,s15));
  camp += conj(c2_2315_1)*(njetan->L1(s23,s15));
//  camp += conj(c2_2315_2)*(njetan->L2hat(s23,s15));
//  camp += conj(c2_2315_3)*(njetan->L3hat(s23,s15));
  camp += conj(c2_1534_0)*(njetan->L0(s15,s34));
  camp += conj(c2_1534_1)*(njetan->L1(s15,s34));
//  camp += conj(c2_1534_2)*(njetan->L2hat(s15,s34));
//  camp += conj(c2_1534_3)*(njetan->L3hat(s15,s34));
  camp += conj(c2_1234_0)*(njetan->L0(s12,s34));
  camp += conj(c2_1234_1)*(njetan->L1(s12,s34));
  camp += conj(c2_1234_2)*(njetan->L2hat(s12,s34));
//  camp += conj(c2_1234_3)*(njetan->L3hat(s12,s34));
  camp += conj(c2_tree)*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//}}}

//{{{ AL32
//{{{ 3
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL323(const int* p) /* 11-1-1-1 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*sign(lS(2,3))*hAL3228(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 5
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL325(const int* p) /* 1-11-1-1 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*sign(lS(2,3))*hAL3226(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 10
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL3210(const int* p) /* -11-11-1 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*sign(lS(2,3))*hAL3221(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 12
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL3212(const int* p) /* -1-111-1 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*sign(lS(2,3))*hAL3219(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 19
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL3219(const int* p) /* 11-1-11 */
{
  setxi(p);
  setsij(p);
  setaij(p);

  const TreeValue phase = i_*a34*a34/(a15*a23*a45);
//  c1234 = T(0.);
//  c1235 = T(0.);
  TreeValue t1 = x4 - x5;
  TreeValue t2 = -T(1.) + x5;
  TreeValue t3 = x2*T(0.5);
  const TreeValue c1245 = t3*x3*t1*t2;
//  c1345 = T(0.);
//  c2345 = T(0.);
//  c123 = T(0.);
  const TreeValue c124 = -t3*x5;
  const TreeValue c125 = x3*t2*T(0.5);
//  c134 = T(0.);
//  c135 = T(0.);
  const TreeValue c145 = -t3*t1;
//  c234 = T(0.);
//  c235 = T(0.);
  const TreeValue c245 = (T(1.) - x5)*x3*T(0.5) - x2*x4*T(0.5);
//  c345 = T(0.);
  const TreeValue c2_2345_0 = T(3.)*T(0.25);
  t2 = T(2.)*t1;
  t3 = T(1.)/t2;
  const TreeValue c2_2345_1 = T(2.)*t3;
  t1 = pow(t1, -2);
  const TreeValue c2_2345_2 = -t1*T(0.5);
//  c2_2345_3 = T(0.);
  const TreeValue c2_tree = -T(3.)*T(0.25);
  t1 = t2*x4;
  t1 = T(1.)/t1;
  const TreeValue rat = t1;

  EpsTriplet<T> amp, camp;
//  amp += c1234*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  amp += c124*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c134*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  amp += c245*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  amp += c2_2345_0*(njetan->L0(s23,s45));
  amp += c2_2345_1*(njetan->L1(s23,s45));
  amp += c2_2345_2*(njetan->L2hat(s23,s45));
//  amp += c2_2345_3*(njetan->L3hat(s23,s45));
  amp += c2_tree*(njetan->I2(s23, T(0.), T(0.))+njetan->I2(s45, T(0.), T(0.)));
  amp += rat;

//  camp += conj(c1234)*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  camp += conj(c124)*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c134)*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  camp += conj(c245)*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  camp += conj(c2_2345_0)*(njetan->L0(s23,s45));
  camp += conj(c2_2345_1)*(njetan->L1(s23,s45));
  camp += conj(c2_2345_2)*(njetan->L2hat(s23,s45));
//  camp += conj(c2_2345_3)*(njetan->L3hat(s23,s45));
  camp += conj(c2_tree)*(njetan->I2(s23, T(0.), T(0.))+njetan->I2(s45, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//{{{ 21
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL3221(const int* p) /* 1-11-11 */
{
  setxi(p);
  setsij(p);
  setaij(p);

  const TreeValue phase = -i_*a24*a24/(a15*a23*a45);
//  c1234 = T(0.);
//  c1235 = T(0.);
  TreeValue t1 = x4 - x5;
  TreeValue t2 = -T(1.) + x5;
  TreeValue t3 = x2*T(0.5);
  const TreeValue c1245 = t3*x3*t1*t2;
//  c1345 = T(0.);
//  c2345 = T(0.);
//  c123 = T(0.);
  const TreeValue c124 = -t3*x5;
  const TreeValue c125 = x3*t2*T(0.5);
//  c134 = T(0.);
//  c135 = T(0.);
  const TreeValue c145 = -t3*t1;
//  c234 = T(0.);
//  c235 = T(0.);
  const TreeValue c245 = (T(1.) - x5)*x3*T(0.5) - x2*x4*T(0.5);
//  c345 = T(0.);
  const TreeValue c2_4512_0 = -T(3.)*T(0.25);
//  c2_4512_1 = T(0.);
//  c2_4512_2 = T(0.);
//  c2_4512_3 = T(0.);
  t2 = T(2.)*x1;
  TreeValue t4 = x1 + x2;
  t4 = pow(t4, -2);
  const TreeValue c2_2345_0 = -t3*(t2 + T(3.)*x2)*t4;
  t3 = T(1.)/t1;
  const TreeValue c2_2345_1 = x1*(x1 + T(2.)*x2)*t4*t3;
  const TreeValue t5 = x1*x1;
  const TreeValue c2_2345_2 = -t5*t4*t3*t3*T(0.5);
//  c2_2345_3 = T(0.);
//  c2_1234_0 = T(0.);
//  c2_1234_1 = T(0.);
//  c2_1234_2 = T(0.);
//  c2_1234_3 = T(0.);
  const TreeValue c2_tree = -T(3.)*T(0.25);
  t1 = -t1;
  const TreeValue rat = (t5*T(0.5) + t2*x2*t1*T(0.5) + x2*x2*x5*t1*T(0.5))*t4*t3/x4;

  EpsTriplet<T> amp, camp;
//  amp += c1234*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  amp += c124*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c134*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  amp += c245*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  amp += c2_4512_0*(njetan->L0(s45,s12));
//  amp += c2_4512_1*(njetan->L1(s45,s12));
//  amp += c2_4512_2*(njetan->L2hat(s45,s12));
//  amp += c2_4512_3*(njetan->L3hat(s45,s12));
  amp += c2_2345_0*(njetan->L0(s23,s45));
  amp += c2_2345_1*(njetan->L1(s23,s45));
  amp += c2_2345_2*(njetan->L2hat(s23,s45));
//  amp += c2_2345_3*(njetan->L3hat(s23,s45));
//  amp += c2_1234_0*(njetan->L0(s12,s34));
//  amp += c2_1234_1*(njetan->L1(s12,s34));
//  amp += c2_1234_2*(njetan->L2hat(s12,s34));
//  amp += c2_1234_3*(njetan->L3hat(s12,s34));
  amp += c2_tree*(njetan->I2(s45, T(0.), T(0.))+njetan->I2(s12, T(0.), T(0.)));
  amp += rat;

//  camp += conj(c1234)*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  camp += conj(c124)*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c134)*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  camp += conj(c245)*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  camp += conj(c2_4512_0)*(njetan->L0(s45,s12));
//  camp += conj(c2_4512_1)*(njetan->L1(s45,s12));
//  camp += conj(c2_4512_2)*(njetan->L2hat(s45,s12));
//  camp += conj(c2_4512_3)*(njetan->L3hat(s45,s12));
  camp += conj(c2_2345_0)*(njetan->L0(s23,s45));
  camp += conj(c2_2345_1)*(njetan->L1(s23,s45));
  camp += conj(c2_2345_2)*(njetan->L2hat(s23,s45));
//  camp += conj(c2_2345_3)*(njetan->L3hat(s23,s45));
//  camp += conj(c2_1234_0)*(njetan->L0(s12,s34));
//  camp += conj(c2_1234_1)*(njetan->L1(s12,s34));
//  camp += conj(c2_1234_2)*(njetan->L2hat(s12,s34));
//  camp += conj(c2_1234_3)*(njetan->L3hat(s12,s34));
  camp += conj(c2_tree)*(njetan->I2(s45, T(0.), T(0.))+njetan->I2(s12, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//{{{ 26
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL3226(const int* p) /* -11-111 */
{
  setxi(p);
  setsij(p);
  setaij(p);

  const TreeValue phase = -i_*a13*a13/(a15*a23*a45);
//  c1234 = T(0.);
//  c1235 = T(0.);
  TreeValue t1 = x4 - x5;
  TreeValue t2 = -T(1.) + x5;
  TreeValue t3 = x2*T(0.5);
  const TreeValue c1245 = t3*x3*t1*t2;
//  c1345 = T(0.);
//  c2345 = T(0.);
//  c123 = T(0.);
  const TreeValue c124 = -t3*x5;
  const TreeValue c125 = x3*t2*T(0.5);
//  c134 = T(0.);
//  c135 = T(0.);
  const TreeValue c145 = -t3*t1;
//  c234 = T(0.);
//  c235 = T(0.);
  t1 = x2*x4;
  const TreeValue c245 = (T(1.) - x5)*x3*T(0.5) - t1*T(0.5);
//  c345 = T(0.);
//  c2_2315_0 = T(0.);
  t3 = T(1.)/x3;
  t2 = T(1.)/t2;
  const TreeValue c2_2315_1 = (x3 - (x2 + x3)*x4)*t3*t2;
  t1 = x3*(-T(1.) + x4) + t1;
  t1 = t1*t1*T(0.5);
  const TreeValue c2_2315_2 = -t1*t3*t3*t2*t2;
//  c2_2315_3 = T(0.);
  const TreeValue c2_1534_0 = -T(3.)*T(0.25);
//  c2_1534_1 = T(0.);
//  c2_1534_2 = T(0.);
//  c2_1534_3 = T(0.);
//  c2_1234_0 = T(0.);
//  c2_1234_1 = T(0.);
//  c2_1234_2 = T(0.);
//  c2_1234_3 = T(0.);
  const TreeValue c2_tree = -T(3.)*T(0.25);
  const TreeValue rat = -t1*t3*t2/(x2*x4);

  EpsTriplet<T> amp, camp;
//  amp += c1234*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  amp += c124*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c134*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  amp += c245*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
//  amp += c2_2315_0*(njetan->L0(s23,s15));
  amp += c2_2315_1*(njetan->L1(s23,s15));
  amp += c2_2315_2*(njetan->L2hat(s23,s15));
//  amp += c2_2315_3*(njetan->L3hat(s23,s15));
  amp += c2_1534_0*(njetan->L0(s15,s34));
//  amp += c2_1534_1*(njetan->L1(s15,s34));
//  amp += c2_1534_2*(njetan->L2hat(s15,s34));
//  amp += c2_1534_3*(njetan->L3hat(s15,s34));
//  amp += c2_1234_0*(njetan->L0(s12,s34));
//  amp += c2_1234_1*(njetan->L1(s12,s34));
//  amp += c2_1234_2*(njetan->L2hat(s12,s34));
//  amp += c2_1234_3*(njetan->L3hat(s12,s34));
  amp += c2_tree*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  amp += rat;

//  camp += conj(c1234)*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  camp += conj(c124)*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c134)*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  camp += conj(c245)*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
//  camp += conj(c2_2315_0)*(njetan->L0(s23,s15));
  camp += conj(c2_2315_1)*(njetan->L1(s23,s15));
  camp += conj(c2_2315_2)*(njetan->L2hat(s23,s15));
//  camp += conj(c2_2315_3)*(njetan->L3hat(s23,s15));
  camp += conj(c2_1534_0)*(njetan->L0(s15,s34));
//  camp += conj(c2_1534_1)*(njetan->L1(s15,s34));
//  camp += conj(c2_1534_2)*(njetan->L2hat(s15,s34));
//  camp += conj(c2_1534_3)*(njetan->L3hat(s15,s34));
//  camp += conj(c2_1234_0)*(njetan->L0(s12,s34));
//  camp += conj(c2_1234_1)*(njetan->L1(s12,s34));
//  camp += conj(c2_1234_2)*(njetan->L2hat(s12,s34));
//  camp += conj(c2_1234_3)*(njetan->L3hat(s12,s34));
  camp += conj(c2_tree)*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//{{{ 28
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL3228(const int* p) /* -1-1111 */
{
  setxi(p);
  setsij(p);
  setaij(p);

  const TreeValue phase = i_*a12*a12/(a15*a23*a45);
//  c1234 = T(0.);
//  c1235 = T(0.);
  TreeValue t1 = x4 - x5;
  TreeValue t2 = -T(1.) + x5;
  TreeValue t3 = x3*t2;
  TreeValue t4 = t3*T(0.5);
  const TreeValue c1245 = t4*x2*t1;
//  c1345 = T(0.);
//  c2345 = T(0.);
//  c123 = T(0.);
  TreeValue t5 = x2*T(0.5);
  const TreeValue c124 = -t5*x5;
  const TreeValue c125 = t4;
//  c134 = T(0.);
//  c135 = T(0.);
  const TreeValue c145 = -t5*t1;
//  c234 = T(0.);
//  c235 = T(0.);
  t4 = x2*x4;
  const TreeValue c245 = (T(1.) - x5)*x3*T(0.5) - t4*T(0.5);
//  c345 = T(0.);
  t5 = pow(x1, -2);
  const TreeValue c2_2315_0 = (T(3.)*T(0.25)*x1*x1 - (-T(8.)*x1 + T(4.)*x2)*x2*T(0.125))*t5;
  TreeValue t6 = x1 + x2;
  const TreeValue t7 = x3*(-T(1.) + x4) + t4;
  t2 = T(1.)/t2;
  const TreeValue t8 = T(1.)/x3;
  const TreeValue c2_2315_1 = t6*(-T(2.)*x1 + T(2.)*x2)*t7*t5*t8*t2*T(0.5);
  const TreeValue t9 = t7*t7;
  t6 = t6*t6;
  const TreeValue c2_2315_2 = -t6*t9*t5*t8*t8*t2*t2*T(0.5);
//  c2_2315_3 = T(0.);
  const TreeValue c2_tree = -T(3.)*T(0.25);
  t1 = x1*t7 + x2*x3*t1;
  t3 = t4 + t3;
  t3 = T(1.)/t3;
  const TreeValue rat = -(t1*t1/(x2*x4)*T(0.5) + t6*t9*t8*t2*T(0.5))*t5*t3;

  EpsTriplet<T> amp, camp;
//  amp += c1234*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  amp += c124*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c134*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  amp += c245*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  amp += c2_2315_0*(njetan->L0(s23,s15));
  amp += c2_2315_1*(njetan->L1(s23,s15));
  amp += c2_2315_2*(njetan->L2hat(s23,s15));
//  amp += c2_2315_3*(njetan->L3hat(s23,s15));
  amp += c2_tree*(njetan->I2(s23, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  amp += rat;

//  camp += conj(c1234)*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  camp += conj(c124)*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c134)*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  camp += conj(c245)*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  camp += conj(c2_2315_0)*(njetan->L0(s23,s15));
  camp += conj(c2_2315_1)*(njetan->L1(s23,s15));
  camp += conj(c2_2315_2)*(njetan->L2hat(s23,s15));
//  camp += conj(c2_2315_3)*(njetan->L3hat(s23,s15));
  camp += conj(c2_tree)*(njetan->I2(s23, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//}}}

//{{{ AL42
//{{{ 3
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL423(const int* p) /* 11-1-1-1 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*sign(lS(2,3))*hAL4228(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 5
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL425(const int* p) /* 1-11-1-1 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*sign(lS(2,3))*hAL4226(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 11
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL4211(const int* p) /* 11-11-1 */
{
  setxi(p);
  setsij(p);
  setaij(p);

  const TreeValue phase = -i_*pow(a35, 3)/(a15*a23*a34*a45);
//  c1234 = T(0.);
//  c1235 = T(0.);
  TreeValue t1 = x4 - x5;
  TreeValue t2 = -T(1.) + x5;
  TreeValue t3 = x2 + x3;
  TreeValue t4 = T(1.)/t3;
  TreeValue t5 = x3*x3;
  const TreeValue t6 = x3*t5;
  const TreeValue t7 = t4*t4;
  t4 = t4*t7;
  TreeValue t8 = x2*T(0.5);
  const TreeValue c1245 = -t8*t5*t5*t1*t2*t4;
//  c1345 = T(0.);
//  c2345 = T(0.);
//  c123 = T(0.);
  t5 = t8*t6;
  const TreeValue c124 = t5*x5*t4;
  const TreeValue t9 = x2*x2;
  const TreeValue t10 = x2*t9;
  const TreeValue t11 = x3*t2;
  const TreeValue c125 = -t11*(T(2.)*t10 + (T(6.)*t9 + (T(6.)*x2 + x3)*x3)*x3)*t4*T(0.5);
//  c134 = T(0.);
//  c135 = T(0.);
  const TreeValue c145 = -t5*t1*t4;
//  c234 = T(0.);
//  c235 = T(0.);
  t5 = x2*x4;
  const TreeValue t12 = t6*T(0.5);
  const TreeValue c245 = t12*(t5 + t11)*t4;
//  c345 = T(0.);
  const TreeValue c2_2315_0 = T(3.)*T(0.25) - t8*(T(3.)*x2 + T(5.)*x3)*t7;
  t8 = -T(1.) + x4;
  t2 = T(1.)/t2;
  const TreeValue c2_2315_1 = -x2*(x2 + T(2.)*x3)*(x3*t8 + t5)*t4*t2;
  t3 = x3 - t3*x4;
  const TreeValue c2_2315_2 = t9*t3*t3*t4*t2*t2*T(0.5)/x3;
//  c2_2315_3 = T(0.);
//  c2_1534_0 = T(0.);
//  c2_1534_1 = T(0.);
//  c2_1534_2 = T(0.);
//  c2_1534_3 = T(0.);
  const TreeValue c2_2345_0 = -x3*(x2 + T(3.)*x3)*t7*T(0.5);
  t3 = -T(1.)/t1;
  const TreeValue c2_2345_1 = t6*t4*t3;
  t1 = T(1.)/t1;
  const TreeValue c2_2345_2 = t12*t4*t1*t1;
//  c2_2345_3 = T(0.);
  const TreeValue c2_tree = T(3.)*T(0.25);
  const TreeValue rat = (t10*x4*(t8*x4 + (T(1.) - x5)*x5)*T(0.5) + (T(2.)*t9*x4*(-(-T(2.) + x5)*x5
    + (-T(2.) + x4)*x4) + (x2*(-x5 + (T(1.) + (T(3.) - x5)*x5 + (-T(3.) + x4)*x4)*x4) - t11)*x3)*x3*T(0.5))*t4*t1*t2/x4;

  EpsTriplet<T> amp, camp;
//  amp += c1234*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  amp += c124*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c134*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  amp += c245*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
//  amp += c2_1534_0*(njetan->L0(s15,s34));
//  amp += c2_1534_1*(njetan->L1(s15,s34));
//  amp += c2_1534_2*(njetan->L2hat(s15,s34));
//  amp += c2_1534_3*(njetan->L3hat(s15,s34));
  amp += c2_2345_0*(njetan->L0(s23,s45));
  amp += c2_2345_1*(njetan->L1(s23,s45));
  amp += c2_2345_2*(njetan->L2hat(s23,s45));
//  amp += c2_2345_3*(njetan->L3hat(s23,s45));
  amp += c2_2315_0*(njetan->L0(s23,s15));
  amp += c2_2315_1*(njetan->L1(s23,s15));
  amp += c2_2315_2*(njetan->L2hat(s23,s15));
//  amp += c2_2315_3*(njetan->L3hat(s23,s15));
  amp += c2_tree*(njetan->I2(s23, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  amp += rat;

//  camp += conj(c1234)*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  camp += conj(c124)*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c134)*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  camp += conj(c245)*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
//  camp += conj(c2_1534_0)*(njetan->L0(s15,s34));
//  camp += conj(c2_1534_1)*(njetan->L1(s15,s34));
//  camp += conj(c2_1534_2)*(njetan->L2hat(s15,s34));
//  camp += conj(c2_1534_3)*(njetan->L3hat(s15,s34));
  camp += conj(c2_2345_0)*(njetan->L0(s23,s45));
  camp += conj(c2_2345_1)*(njetan->L1(s23,s45));
  camp += conj(c2_2345_2)*(njetan->L2hat(s23,s45));
//  camp += conj(c2_2345_3)*(njetan->L3hat(s23,s45));
  camp += conj(c2_2315_0)*(njetan->L0(s23,s15));
  camp += conj(c2_2315_1)*(njetan->L1(s23,s15));
  camp += conj(c2_2315_2)*(njetan->L2hat(s23,s15));
//  camp += conj(c2_2315_3)*(njetan->L3hat(s23,s15));
  camp += conj(c2_tree)*(njetan->I2(s23, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//{{{ 13
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL4213(const int* p) /* 1-111-1 */
{
  setxi(p);
  setsij(p);
  setaij(p);

  const TreeValue phase = i_*a25*a25*a35/(a15*a23*a34*a45);
//  c1234 = T(0.);
//  c1235 = T(0.);
  TreeValue t1 = x1 + x2;
  const TreeValue t2 = x4 - x5;
  const TreeValue t3 = -T(1.) + x5;
  TreeValue t4 = x2 + x3;
  TreeValue t5 = x2*x3 + x1*t4;
  t4 = T(1.)/t4;
  const TreeValue t6 = x3*x3;
  const TreeValue t7 = x3*t6;
  t5 = pow(t5, -2);
  t1 = t1*t1;
  TreeValue t8 = x2*T(0.5);
  TreeValue t9 = t8*t1;
  const TreeValue c1245 = -t9*t6*t6*t2*t3*t4*t5;
//  c1345 = T(0.);
//  c2345 = T(0.);
//  c123 = T(0.);
  t9 = t9*t7;
  const TreeValue c124 = t9*x5*t4*t5;
  TreeValue t10 = T(2.)*x2;
  TreeValue t11 = x2*x2;
  const TreeValue t12 = x2*t11;
  TreeValue t13 = T(4.)*x2;
  const TreeValue t14 = x1*x1;
  TreeValue t15 = t10*x1*x3;
  const TreeValue t16 = t11*t6;
  const TreeValue t17 = x3*t3;
  const TreeValue c125 = -t17*(t16*(t10 + x3) + t15*(T(2.)*t11 + (t13 + x3)*x3) + t14*(T(2.)*t12
    + (T(6.)*t11 + (T(6.)*x2 + x3)*x3)*x3))*t4*t5*T(0.5);
//  c134 = T(0.);
//  c135 = T(0.);
  const TreeValue c145 = -t9*t2*t4*t5;
//  c234 = T(0.);
//  c235 = T(0.);
  t9 = x2*x4;
  const TreeValue t18 = t1*T(0.5);
  const TreeValue c245 = t18*t7*(t9 + t17)*t4*t5;
//  c345 = T(0.);
//  c2_4512_0 = T(0.);
//  c2_4512_1 = T(0.);
//  c2_4512_2 = T(0.);
//  c2_4512_3 = T(0.);
  const TreeValue t19 = T(3.)*x2;
  t13 = t13*x3;
  const TreeValue t20 = T(1.)*T(0.25);
  const TreeValue c2_2345_0 = t20*(t15*(t19 - x3) + T(3.)*t16 + t14*(T(3.)*t11 - T(3.)*t6 + t13))*t5;
  t15 = -T(1.)/t2;
  const TreeValue c2_2345_1 = x1*(x1 + t10)*t7*t4*t5*t15;
  t10 = T(1.)/t2;
  const TreeValue c2_2345_2 = t14*t7*t4*t5*t10*t10*T(0.5);
//  c2_2345_3 = T(0.);
  const TreeValue c2_2315_0 = t8*(-t13*x1 + t11*x3 - t14*(t19 + T(5.)*x3))*t5;
  t8 = T(2.)*x3;
  t13 = -T(1.) + x4;
  t9 = t13*x3 + t9;
  t15 = T(1.)/t3;
  const TreeValue c2_2315_1 = -x2*t1*(x2 + t8)*t9*t4*t5*t15;
  const TreeValue c2_2315_2 = t18*t11*t9*t9*t4*t5*t15*t15/x3;
//  c2_2315_3 = T(0.);
  const TreeValue c2_tree = T(3.)*T(0.25);
  t1 = x5*x5;
  t9 = t11*x4;
  t11 = t12*x4*x4;
  const TreeValue rat = -(t14*(t12*x4*(t3*x5 + (T(1.) - x4)*x4) + (T(2.)*t9*((-T(2.) + x5)*x5
    + (T(2.) - x4)*x4) + (x2*(x5 + (-T(1.) + (-T(3.) + x5)*x5 + (T(3.) - x4)*x4)*x4) + t17)*x3)*x3)*T(0.5)
    + (-T(2.)*x1*((t8*t9*t13 + t11 + t7*t3)*t2 + x2*t6*(-t1 + (T(3.)*x5 + (-T(2.) - x5 + x4)*x4)*x4))
    - t2*(t11 + (t9*(-T(1.) + T(2.)*x4 - x5) + (x2*(t1 + (-T(1.) - x5 + x4)*x4) + t17*x5)*x3)*x3)*x2)*x2*T(0.5))*t4*t5*t10*t15/x4;

  EpsTriplet<T> amp, camp;
//  amp += c1234*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  amp += c124*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c134*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  amp += c245*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
//  amp += c2_4512_0*(njetan->L0(s45,s12));
//  amp += c2_4512_1*(njetan->L1(s45,s12));
//  amp += c2_4512_2*(njetan->L2hat(s45,s12));
//  amp += c2_4512_3*(njetan->L3hat(s45,s12));
  amp += c2_2345_0*(njetan->L0(s23,s45));
  amp += c2_2345_1*(njetan->L1(s23,s45));
  amp += c2_2345_2*(njetan->L2hat(s23,s45));
//  amp += c2_2345_3*(njetan->L3hat(s23,s45));
  amp += c2_2315_0*(njetan->L0(s23,s15));
  amp += c2_2315_1*(njetan->L1(s23,s15));
  amp += c2_2315_2*(njetan->L2hat(s23,s15));
//  amp += c2_2315_3*(njetan->L3hat(s23,s15));
  amp += c2_tree*(njetan->I2(s23, T(0.), T(0.))+njetan->I2(s45, T(0.), T(0.)));
  amp += rat;

//  camp += conj(c1234)*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  camp += conj(c124)*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c134)*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  camp += conj(c245)*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
//  camp += conj(c2_4512_0)*(njetan->L0(s45,s12));
//  camp += conj(c2_4512_1)*(njetan->L1(s45,s12));
//  camp += conj(c2_4512_2)*(njetan->L2hat(s45,s12));
//  camp += conj(c2_4512_3)*(njetan->L3hat(s45,s12));
  camp += conj(c2_2345_0)*(njetan->L0(s23,s45));
  camp += conj(c2_2345_1)*(njetan->L1(s23,s45));
  camp += conj(c2_2345_2)*(njetan->L2hat(s23,s45));
//  camp += conj(c2_2345_3)*(njetan->L3hat(s23,s45));
  camp += conj(c2_2315_0)*(njetan->L0(s23,s15));
  camp += conj(c2_2315_1)*(njetan->L1(s23,s15));
  camp += conj(c2_2315_2)*(njetan->L2hat(s23,s15));
//  camp += conj(c2_2315_3)*(njetan->L3hat(s23,s15));
  camp += conj(c2_tree)*(njetan->I2(s23, T(0.), T(0.))+njetan->I2(s45, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//{{{ 18
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL4218(const int* p) /* -11-1-11 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*sign(lS(2,3))*hAL4213(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 20
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL4220(const int* p) /* -1-11-11 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*sign(lS(2,3))*hAL4211(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 26
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL4226(const int* p) /* -11-111 */
{
  setxi(p);
  setsij(p);
  setaij(p);

  const TreeValue phase = i_*a13*a13*a35/(a15*a23*a34*a45);
//  c1234 = T(0.);
//  c1235 = T(0.);
  TreeValue t1 = x4 - x5;
  TreeValue t2 = -T(1.) + x5;
  TreeValue t3 = T(2.)*x2 + T(2.)*x3;
  TreeValue t4 = T(1.)/t3;
  const TreeValue t5 = x3*x3;
  const TreeValue c1245 = -x2*t5*t1*t2*t4;
//  c1345 = T(0.);
//  c2345 = T(0.);
//  c123 = T(0.);
  const TreeValue t6 = x2*x3;
  const TreeValue c124 = t6*x5*t4;
  const TreeValue t7 = x3*t2;
  const TreeValue c125 = -t7*(T(2.)*x2 + x3)*t4;
//  c134 = T(0.);
//  c135 = T(0.);
  const TreeValue c145 = -t6*t1*t4;
//  c234 = T(0.);
//  c235 = T(0.);
  t1 = x2*x4;
  const TreeValue c245 = x3*(t1 + t7)*t4;
//  c345 = T(0.);
//  c2_2315_0 = T(0.);
  t4 = -T(1.) + x4;
  t1 = x3*t4 + t1;
  t3 = t3*T(0.5);
  t3 = T(1.)/t3;
  t2 = T(1.)/t2;
  const TreeValue c2_2315_1 = t1*t3*t2;
  const TreeValue c2_2315_2 = t1*t1*t3*t2*t2*T(0.5)/x3;
//  c2_2315_3 = T(0.);
  const TreeValue c2_1534_0 = T(3.)*T(0.25);
//  c2_1534_1 = T(0.);
//  c2_1534_2 = T(0.);
//  c2_1534_3 = T(0.);
//  c2_1234_0 = T(0.);
//  c2_1234_1 = T(0.);
//  c2_1234_2 = T(0.);
//  c2_1234_3 = T(0.);
  const TreeValue c2_tree = T(3.)*T(0.25);
  const TreeValue rat = (t5*t4*t4*T(0.5) + x2*x2*x4*(-T(1.) + x4 + x5)*T(0.5) + t6*x4*(-T(3.)
    + T(2.)*x4 + x5)*T(0.5))*t3*t2/(x2*x4);

  EpsTriplet<T> amp, camp;
//  amp += c1234*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  amp += c124*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c134*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  amp += c245*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
//  amp += c2_2315_0*(njetan->L0(s23,s15));
  amp += c2_2315_1*(njetan->L1(s23,s15));
  amp += c2_2315_2*(njetan->L2hat(s23,s15));
//  amp += c2_2315_3*(njetan->L3hat(s23,s15));
  amp += c2_1534_0*(njetan->L0(s15,s34));
//  amp += c2_1534_1*(njetan->L1(s15,s34));
//  amp += c2_1534_2*(njetan->L2hat(s15,s34));
//  amp += c2_1534_3*(njetan->L3hat(s15,s34));
//  amp += c2_1234_0*(njetan->L0(s12,s34));
//  amp += c2_1234_1*(njetan->L1(s12,s34));
//  amp += c2_1234_2*(njetan->L2hat(s12,s34));
//  amp += c2_1234_3*(njetan->L3hat(s12,s34));
  amp += c2_tree*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  amp += rat;

//  camp += conj(c1234)*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  camp += conj(c124)*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c134)*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  camp += conj(c245)*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
//  camp += conj(c2_2315_0)*(njetan->L0(s23,s15));
  camp += conj(c2_2315_1)*(njetan->L1(s23,s15));
  camp += conj(c2_2315_2)*(njetan->L2hat(s23,s15));
//  camp += conj(c2_2315_3)*(njetan->L3hat(s23,s15));
  camp += conj(c2_1534_0)*(njetan->L0(s15,s34));
//  camp += conj(c2_1534_1)*(njetan->L1(s15,s34));
//  camp += conj(c2_1534_2)*(njetan->L2hat(s15,s34));
//  camp += conj(c2_1534_3)*(njetan->L3hat(s15,s34));
//  camp += conj(c2_1234_0)*(njetan->L0(s12,s34));
//  camp += conj(c2_1234_1)*(njetan->L1(s12,s34));
//  camp += conj(c2_1234_2)*(njetan->L2hat(s12,s34));
//  camp += conj(c2_1234_3)*(njetan->L3hat(s12,s34));
  camp += conj(c2_tree)*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//{{{ 28
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL4228(const int* p) /* -1-1111 */
{
  setxi(p);
  setsij(p);
  setaij(p);

  const TreeValue phase = -i_*a12*a12*a35/(a15*a23*a34*a45);
//  c1234 = T(0.);
//  c1235 = T(0.);
  const TreeValue t1 = x4 - x5;
  TreeValue t2 = -T(1.) + x5;
  TreeValue t3 = T(2.)*x2 + T(2.)*x3;
  TreeValue t4 = T(1.)/t3;
  const TreeValue t5 = x3*x3;
  const TreeValue c1245 = -x2*t5*t1*t2*t4;
//  c1345 = T(0.);
//  c2345 = T(0.);
//  c123 = T(0.);
  TreeValue t6 = x2*x3;
  const TreeValue c124 = t6*x5*t4;
  TreeValue t7 = x3*t2;
  const TreeValue c125 = -t7*(T(2.)*x2 + x3)*t4;
//  c134 = T(0.);
//  c135 = T(0.);
  const TreeValue c145 = -t6*t1*t4;
//  c234 = T(0.);
//  c235 = T(0.);
  const TreeValue t8 = x2*x4;
  const TreeValue c245 = x3*(t8 + t7)*t4;
//  c345 = T(0.);
  t3 = t3*T(0.5);
  t4 = x1*x1;
  t7 = x2*x2;
  const TreeValue t9 = T(1.)/t3;
  const TreeValue t10 = pow(x1, -2);
  const TreeValue c2_2315_0 = (-t6*x1 + t7*x3*T(0.5) - T(3.)*T(0.25)*t4*t3)*t10*t9;
  t3 = x1 + x2;
  const TreeValue t11 = -T(1.) + x4;
  const TreeValue t12 = x3*t11 + t8;
  t2 = T(1.)/t2;
  const TreeValue c2_2315_1 = -t3*(-T(2.)*x1 + T(2.)*x2)*t12*t10*t9*t2*T(0.5);
  const TreeValue c2_2315_2 = t3*t3*t12*t12*t10*t9*t2*t2*T(0.5)/x3;
//  c2_2315_3 = T(0.);
  const TreeValue c2_tree = T(3.)*T(0.25);
  t3 = T(2.)*x4;
  t6 = t6*x4;
  const TreeValue rat = (t4*(t5*t11*t11 + t7*x4*(-T(1.) + x4 + x5) + t6*(-T(3.) + t3 + x5))*T(0.5)
    + (T(2.)*x1*t12*(t8 + x3*t1) + (t7*x4*x4 + t5*t1*t1 + t6*(-T(1.) + t3 - x5))*x2)*x2*T(0.5))*t10*t9*t2/(x2*x4);

  EpsTriplet<T> amp, camp;
//  amp += c1234*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  amp += c124*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c134*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  amp += c245*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  amp += c2_2315_0*(njetan->L0(s23,s15));
  amp += c2_2315_1*(njetan->L1(s23,s15));
  amp += c2_2315_2*(njetan->L2hat(s23,s15));
//  amp += c2_2315_3*(njetan->L3hat(s23,s15));
  amp += c2_tree*(njetan->I2(s23, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  amp += rat;

//  camp += conj(c1234)*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
  camp += conj(c124)*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c134)*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
  camp += conj(c245)*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  camp += conj(c2_2315_0)*(njetan->L0(s23,s15));
  camp += conj(c2_2315_1)*(njetan->L1(s23,s15));
  camp += conj(c2_2315_2)*(njetan->L2hat(s23,s15));
//  camp += conj(c2_2315_3)*(njetan->L3hat(s23,s15));
  camp += conj(c2_tree)*(njetan->I2(s23, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//}}}

//{{{ AL43
//{{{ 3
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL433(const int* p) /* 11-1-1-1 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*sign(lS(2,3))*hAL4328(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 7
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL437(const int* p) /* 111-1-1 */
{
  setxi(p);
  setsij(p);
  setaij(p);
  const TreeValue phase = -i_*a45*a45/(a15*a23*a34);
//  c1234 = T(0.);
//  c1235 = T(0.);
//  c1245 = T(0.);
//  c1345 = T(0.);
//  c2345 = T(0.);
//  c123 = T(0.);
//  c124 = T(0.);
  const TreeValue c125 = (T(1.) - x5)*x3;
//  c134 = T(0.);
//  c135 = T(0.);
//  c145 = T(0.);
//  c234 = T(0.);
//  c235 = T(0.);
//  c245 = T(0.);
//  c345 = T(0.);
  const TreeValue c2_1534_0 = T(3.)*T(0.25);
//  c2_1534_1 = T(0.);
//  c2_1534_2 = T(0.);
//  c2_1534_3 = T(0.);
  const TreeValue c2_tree = T(3.)*T(0.25);
  const TreeValue rat = T(1.)*T(0.5);

  EpsTriplet<T> amp, camp;
//  amp += c1234*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
//  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
//  amp += c124*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c134*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
//  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
//  amp += c245*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  amp += c2_1534_0*(njetan->L0(s15,s34));
//  amp += c2_1534_1*(njetan->L1(s15,s34));
//  amp += c2_1534_2*(njetan->L2hat(s15,s34));
//  amp += c2_1534_3*(njetan->L3hat(s15,s34));
  amp += c2_tree*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  amp += rat;

//  camp += conj(c1234)*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
//  camp += conj(c124)*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c134)*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
//  camp += conj(c245)*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  camp += conj(c2_1534_0)*(njetan->L0(s15,s34));
//  camp += conj(c2_1534_1)*(njetan->L1(s15,s34));
//  camp += conj(c2_1534_2)*(njetan->L2hat(s15,s34));
//  camp += conj(c2_1534_3)*(njetan->L3hat(s15,s34));
  camp += conj(c2_tree)*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//{{{ 9
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL439(const int* p) /* 1-1-11-1 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*sign(lS(2,3))*hAL4322(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 13
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL4313(const int* p) /* 1-111-1 */
{
  setxi(p);
  setsij(p);
  setaij(p);
  const TreeValue phase = i_*a25*a25/(a15*a23*a34);
//  c1234 = T(0.);
//  c1235 = T(0.);
//  c1245 = T(0.);
//  c1345 = T(0.);
//  c2345 = T(0.);
//  c123 = T(0.);
//  c124 = T(0.);
  const TreeValue c125 = (T(1.) - x5)*x3;
//  c134 = T(0.);
//  c135 = T(0.);
//  c145 = T(0.);
//  c234 = T(0.);
//  c235 = T(0.);
//  c245 = T(0.);
//  c345 = T(0.);
//  c2_4512_0 = T(0.);
//  c2_4512_1 = T(0.);
//  c2_4512_2 = T(0.);
//  c2_4512_3 = T(0.);
  const TreeValue c2_2345_0 = T(3.)*T(0.25);
//  c2_2345_1 = T(0.);
//  c2_2345_2 = T(0.);
//  c2_2345_3 = T(0.);
  const TreeValue c2_2315_0 = -T(3.)*T(0.5);
//  c2_2315_1 = T(0.);
//  c2_2315_2 = T(0.);
//  c2_2315_3 = T(0.);
  const TreeValue c2_tree = T(3.)*T(0.25);
  const TreeValue rat = T(1.)*T(0.5);

  EpsTriplet<T> amp, camp;
//  amp += c1234*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
//  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
//  amp += c124*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c134*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
//  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
//  amp += c245*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
//  amp += c2_4512_0*(njetan->L0(s45,s12));
//  amp += c2_4512_1*(njetan->L1(s45,s12));
//  amp += c2_4512_2*(njetan->L2hat(s45,s12));
//  amp += c2_4512_3*(njetan->L3hat(s45,s12));
  amp += c2_2345_0*(njetan->L0(s23,s45));
//  amp += c2_2345_1*(njetan->L1(s23,s45));
//  amp += c2_2345_2*(njetan->L2hat(s23,s45));
//  amp += c2_2345_3*(njetan->L3hat(s23,s45));
  amp += c2_2315_0*(njetan->L0(s23,s15));
//  amp += c2_2315_1*(njetan->L1(s23,s15));
//  amp += c2_2315_2*(njetan->L2hat(s23,s15));
//  amp += c2_2315_3*(njetan->L3hat(s23,s15));
  amp += c2_tree*(njetan->I2(s23, T(0.), T(0.))+njetan->I2(s45, T(0.), T(0.)));
  amp += rat;

//  camp += conj(c1234)*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
//  camp += conj(c124)*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c134)*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
//  camp += conj(c245)*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
//  camp += conj(c2_4512_0)*(njetan->L0(s45,s12));
//  camp += conj(c2_4512_1)*(njetan->L1(s45,s12));
//  camp += conj(c2_4512_2)*(njetan->L2hat(s45,s12));
//  camp += conj(c2_4512_3)*(njetan->L3hat(s45,s12));
  camp += conj(c2_2345_0)*(njetan->L0(s23,s45));
//  camp += conj(c2_2345_1)*(njetan->L1(s23,s45));
//  camp += conj(c2_2345_2)*(njetan->L2hat(s23,s45));
//  camp += conj(c2_2345_3)*(njetan->L3hat(s23,s45));
  camp += conj(c2_2315_0)*(njetan->L0(s23,s15));
//  camp += conj(c2_2315_1)*(njetan->L1(s23,s15));
//  camp += conj(c2_2315_2)*(njetan->L2hat(s23,s15));
//  camp += conj(c2_2315_3)*(njetan->L3hat(s23,s15));
  camp += conj(c2_tree)*(njetan->I2(s23, T(0.), T(0.))+njetan->I2(s45, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//{{{ 18
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL4318(const int* p) /* -11-1-11 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*sign(lS(2,3))*hAL4313(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 22
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL4322(const int* p) /* -111-11 */
{
  setxi(p);
  setsij(p);
  setaij(p);
  const TreeValue phase = i_*a14*a14/(a15*a23*a34);
//  c1234 = T(0.);
//  c1235 = T(0.);
//  c1245 = T(0.);
//  c1345 = T(0.);
//  c2345 = T(0.);
//  c123 = T(0.);
//  c124 = T(0.);
  const TreeValue c125 = (T(1.) - x5)*x3;
//  c134 = T(0.);
//  c135 = T(0.);
//  c145 = T(0.);
//  c234 = T(0.);
//  c235 = T(0.);
//  c245 = T(0.);
//  c345 = T(0.);
//  c2_4512_0 = T(0.);
//  c2_4512_1 = T(0.);
//  c2_4512_2 = T(0.);
//  c2_4512_3 = T(0.);
  const TreeValue c2_1534_0 = T(3.)*T(0.5);
//  c2_1534_1 = T(0.);
//  c2_1534_2 = T(0.);
//  c2_1534_3 = T(0.);
  const TreeValue c2_1234_0 = -T(3.)*T(0.25);
//  c2_1234_1 = T(0.);
//  c2_1234_2 = T(0.);
//  c2_1234_3 = T(0.);
  const TreeValue c2_tree = T(3.)*T(0.25);
  const TreeValue rat = T(1.)*T(0.5);

  EpsTriplet<T> amp, camp;
//  amp += c1234*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
//  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
//  amp += c124*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c134*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
//  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
//  amp += c245*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
//  amp += c2_4512_0*(njetan->L0(s45,s12));
//  amp += c2_4512_1*(njetan->L1(s45,s12));
//  amp += c2_4512_2*(njetan->L2hat(s45,s12));
//  amp += c2_4512_3*(njetan->L3hat(s45,s12));
  amp += c2_1534_0*(njetan->L0(s15,s34));
//  amp += c2_1534_1*(njetan->L1(s15,s34));
//  amp += c2_1534_2*(njetan->L2hat(s15,s34));
//  amp += c2_1534_3*(njetan->L3hat(s15,s34));
  amp += c2_1234_0*(njetan->L0(s12,s34));
//  amp += c2_1234_1*(njetan->L1(s12,s34));
//  amp += c2_1234_2*(njetan->L2hat(s12,s34));
//  amp += c2_1234_3*(njetan->L3hat(s12,s34));
  amp += c2_tree*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s12, T(0.), T(0.)));
  amp += rat;

//  camp += conj(c1234)*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
//  camp += conj(c124)*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c134)*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
//  camp += conj(c245)*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
//  camp += conj(c2_4512_0)*(njetan->L0(s45,s12));
//  camp += conj(c2_4512_1)*(njetan->L1(s45,s12));
//  camp += conj(c2_4512_2)*(njetan->L2hat(s45,s12));
//  camp += conj(c2_4512_3)*(njetan->L3hat(s45,s12));
  camp += conj(c2_1534_0)*(njetan->L0(s15,s34));
//  camp += conj(c2_1534_1)*(njetan->L1(s15,s34));
//  camp += conj(c2_1534_2)*(njetan->L2hat(s15,s34));
//  camp += conj(c2_1534_3)*(njetan->L3hat(s15,s34));
  camp += conj(c2_1234_0)*(njetan->L0(s12,s34));
//  camp += conj(c2_1234_1)*(njetan->L1(s12,s34));
//  camp += conj(c2_1234_2)*(njetan->L2hat(s12,s34));
//  camp += conj(c2_1234_3)*(njetan->L3hat(s12,s34));
  camp += conj(c2_tree)*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s12, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//{{{ 24
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL4324(const int* p) /* -1-1-111 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*sign(lS(2,3))*hAL437(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 28
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAL4328(const int* p) /* -1-1111 */
{
  setxi(p);
  setsij(p);
  setaij(p);
  const TreeValue phase = -i_*a12*a12/(a15*a23*a34);
//  c1234 = T(0.);
//  c1235 = T(0.);
//  c1245 = T(0.);
//  c1345 = T(0.);
//  c2345 = T(0.);
//  c123 = T(0.);
//  c124 = T(0.);
  const TreeValue c125 = (T(1.) - x5)*x3;
//  c134 = T(0.);
//  c135 = T(0.);
//  c145 = T(0.);
//  c234 = T(0.);
//  c235 = T(0.);
//  c245 = T(0.);
//  c345 = T(0.);
  const TreeValue c2_2315_0 = -T(3.)*T(0.25);
//  c2_2315_1 = T(0.);
//  c2_2315_2 = T(0.);
//  c2_2315_3 = T(0.);
  const TreeValue c2_tree = T(3.)*T(0.25);
  const TreeValue rat = T(1.)*T(0.5);

  EpsTriplet<T> amp, camp;
//  amp += c1234*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
//  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
//  amp += c124*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c134*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
//  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
//  amp += c245*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  amp += c2_2315_0*(njetan->L0(s23,s15));
//  amp += c2_2315_1*(njetan->L1(s23,s15));
//  amp += c2_2315_2*(njetan->L2hat(s23,s15));
//  amp += c2_2315_3*(njetan->L3hat(s23,s15));
  amp += c2_tree*(njetan->I2(s23, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  amp += rat;

//  camp += conj(c1234)*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
//  camp += conj(c124)*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c134)*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
//  camp += conj(c245)*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  camp += conj(c2_2315_0)*(njetan->L0(s23,s15));
//  camp += conj(c2_2315_1)*(njetan->L1(s23,s15));
//  camp += conj(c2_2315_2)*(njetan->L2hat(s23,s15));
//  camp += conj(c2_2315_3)*(njetan->L3hat(s23,s15));
  camp += conj(c2_tree)*(njetan->I2(s23, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//}}}

//{{{ Af13
//{{{ 5
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAf135(const int* p) /* 1-11-1-1 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*sign(lS(2,3))*hAf1326(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 6
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAf136(const int* p) /* -111-1-1 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*sign(lS(2,3))*hAf1325(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 9
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAf139(const int* p) /* 1-1-11-1 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*sign(lS(2,3))*hAf1322(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 10
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAf1310(const int* p) /* -11-11-1 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*sign(lS(2,3))*hAf1321(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 21
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAf1321(const int* p) /* 1-11-11 */
{
  setxi(p);
  setsij(p);
  setaij(p);

  const TreeValue phase = -i_*a14*a24*a24/(a12*a15*a34*a45);
//  c1234 = T(0.);
//  c1235 = T(0.);
//  c1245 = T(0.);
//  c1345 = T(0.);
//  c2345 = T(0.);
//  c123 = T(0.);
//  c124 = T(0.);
//  c125 = T(0.);
//  c134 = T(0.);
//  c135 = T(0.);
//  c145 = T(0.);
//  c234 = T(0.);
//  c235 = T(0.);
//  c245 = T(0.);
//  c345 = T(0.);
  const TreeValue c2_4512_0 = -T(1.)/T(3.);
//  c2_4512_1 = T(0.);
//  c2_4512_2 = T(0.);
//  c2_4512_3 = T(0.);
//  c2_2345_0 = T(0.);
//  c2_2345_1 = T(0.);
//  c2_2345_2 = T(0.);
//  c2_2345_3 = T(0.);
  TreeValue t1 = x1 + x2;
  TreeValue t2 = T(2.)*x1*x2;
  TreeValue t3 = t1*x3;
  TreeValue t4 = t2 + t3;
  const TreeValue t5 = pow(x3, -2);
  const TreeValue t6 = pow(t1, -2);
  const TreeValue c2_1234_0 = x1*x2*t4*t6*t5/T(3.);
  const TreeValue t7 = x3*x3;
  const TreeValue t8 = x1*x1;
  const TreeValue t9 = x1*t8;
  const TreeValue t10 = x2*x2;
  const TreeValue t11 = x2*t10;
  TreeValue t12 = -T(1.) + x4;
  const TreeValue t13 = x4 - x5;
  TreeValue t14 = x1*(x2 + x3);
  TreeValue t15 = (-x1 + x2*t13)*x3 + t14*x4;
  t15 = T(1.)/t15;
  const TreeValue c2_1234_1 = -x1*x2*(T(2.)*t8*t10 + t3*t2 + t1*t1*t7)*t12*t6*t5*t15;
  t1 = x2*x3;
  t2 = t1 + t14;
  t3 = t12*t12;
  t14 = t15*t15;
  const TreeValue c2_1234_2 = t8*t10*t4*t2*t3*t6*t5*t14;
  const TreeValue c2_1234_3 = -T(2.)/T(3.)*t9*t11*t2*t2*t12*t3*t6*t5*t15*t14;
  const TreeValue c2_tree = T(1.)/T(3.);
  t2 = x3*t12 + x2*x4;
  t3 = T(3.)*x5;
  t4 = T(3.)*x4;
  t12 = x1*t2 + t1*t13;
  t12 = T(1.)/t12;
  t14 = T(1.)/T(9.);
  const TreeValue rat = t14*(-t9*(T(3.)*t10*(x2*(T(5.) + (-T(6.) + x4)*x4) + x3*(T(4.) + (-T(5.)
    + x4)*x4)) + T(2.)*t7*t2) - T(2.)*t11*x3*t7*t13 + t1*t8*(-T(3.)*t10*(T(7.) - t3 + x4*(x4 - T(6.)
    + x5)) + (x2*(-T(3.) - T(4.)*x4 + t3) + (T(4.) - T(2.)*t4 + T(2.)*x5)*x3)*x3)
    + x1*t10*t7*(T(2.)*x3*(T(1.) - t4 + T(2.)*x5) - x2*(T(6.) + T(2.)*x4
    + (-T(9.) + t3)*x5)))*t6*t5*t12;

  EpsTriplet<T> amp, camp;
//  amp += c1234*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
//  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
//  amp += c124*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
//  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c134*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
//  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
//  amp += c245*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  amp += c2_4512_0*(njetan->L0(s45,s12));
//  amp += c2_4512_1*(njetan->L1(s45,s12));
//  amp += c2_4512_2*(njetan->L2hat(s45,s12));
//  amp += c2_4512_3*(njetan->L3hat(s45,s12));
//  amp += c2_2345_0*(njetan->L0(s23,s45));
//  amp += c2_2345_1*(njetan->L1(s23,s45));
//  amp += c2_2345_2*(njetan->L2hat(s23,s45));
//  amp += c2_2345_3*(njetan->L3hat(s23,s45));
  amp += c2_1234_0*(njetan->L0(s12,s34));
  amp += c2_1234_1*(njetan->L1(s12,s34));
  amp += c2_1234_2*(njetan->L2hat(s12,s34));
  amp += c2_1234_3*(njetan->L3hat(s12,s34));
  amp += c2_tree*(njetan->I2(s45, T(0.), T(0.))+njetan->I2(s12, T(0.), T(0.)));
  amp += rat;

//  camp += conj(c1234)*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
//  camp += conj(c124)*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
//  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c134)*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
//  camp += conj(c245)*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  camp += conj(c2_4512_0)*(njetan->L0(s45,s12));
//  camp += conj(c2_4512_1)*(njetan->L1(s45,s12));
//  camp += conj(c2_4512_2)*(njetan->L2hat(s45,s12));
//  camp += conj(c2_4512_3)*(njetan->L3hat(s45,s12));
//  camp += conj(c2_2345_0)*(njetan->L0(s23,s45));
//  camp += conj(c2_2345_1)*(njetan->L1(s23,s45));
//  camp += conj(c2_2345_2)*(njetan->L2hat(s23,s45));
//  camp += conj(c2_2345_3)*(njetan->L3hat(s23,s45));
  camp += conj(c2_1234_0)*(njetan->L0(s12,s34));
  camp += conj(c2_1234_1)*(njetan->L1(s12,s34));
  camp += conj(c2_1234_2)*(njetan->L2hat(s12,s34));
  camp += conj(c2_1234_3)*(njetan->L3hat(s12,s34));
  camp += conj(c2_tree)*(njetan->I2(s45, T(0.), T(0.))+njetan->I2(s12, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//{{{ 22
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAf1322(const int* p) /* -111-11 */
{
  setxi(p);
  setsij(p);
  setaij(p);

  const TreeValue phase = i_*pow(a14, 3)/(a12*a15*a34*a45);
//  c1234 = T(0.);
//  c1235 = T(0.);
//  c1245 = T(0.);
//  c1345 = T(0.);
//  c2345 = T(0.);
//  c123 = T(0.);
//  c124 = T(0.);
//  c125 = T(0.);
//  c134 = T(0.);
//  c135 = T(0.);
//  c145 = T(0.);
//  c234 = T(0.);
//  c235 = T(0.);
//  c245 = T(0.);
//  c345 = T(0.);
//  c2_4512_0 = T(0.);
//  c2_4512_1 = T(0.);
//  c2_4512_2 = T(0.);
//  c2_4512_3 = T(0.);
//  c2_1534_0 = T(0.);
//  c2_1534_1 = T(0.);
//  c2_1534_2 = T(0.);
//  c2_1534_3 = T(0.);
  const TreeValue c2_1234_0 = T(1.)/T(3.);
  TreeValue t1 = -T(1.) + x4;
  const TreeValue t2 = x4 - x5;
  TreeValue t3 = x2*t2;
  TreeValue t4 = (-x1 + t3)*x3 + x1*(x2 + x3)*x4;
  t4 = T(1.)/t4;
  const TreeValue c2_1234_1 = -x1*x2*t1*t4;
  const TreeValue t5 = t1*t1;
  const TreeValue t6 = x1*x1;
  const TreeValue t7 = t4*t4;
  const TreeValue t8 = x2*x2;
  const TreeValue c2_1234_2 = t6*t8*t5*t7;
  const TreeValue c2_1234_3 = -T(2.)/T(3.)*x1*t6*x2*t8*t1*t5*t4*t7;
  const TreeValue c2_tree = T(1.)/T(3.);
  t1 = x3*t1;
  t3 = T(9.)*x1*(t1 + x2*x4) + T(9.)*t3*x3;
  t3 = T(1.)/t3;
  const TreeValue rat = (x1*(-T(2.)*t1 + x2*(-T(6.) + (T(7.) - T(3.)*x4)*x4)) - T(2.)*x2*x3*t2)*t3;

  EpsTriplet<T> amp, camp;
//  amp += c1234*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
//  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
//  amp += c124*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
//  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c134*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
//  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
//  amp += c245*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
//  amp += c2_4512_0*(njetan->L0(s45,s12));
//  amp += c2_4512_1*(njetan->L1(s45,s12));
//  amp += c2_4512_2*(njetan->L2hat(s45,s12));
//  amp += c2_4512_3*(njetan->L3hat(s45,s12));
//  amp += c2_1534_0*(njetan->L0(s15,s34));
//  amp += c2_1534_1*(njetan->L1(s15,s34));
//  amp += c2_1534_2*(njetan->L2hat(s15,s34));
//  amp += c2_1534_3*(njetan->L3hat(s15,s34));
  amp += c2_1234_0*(njetan->L0(s12,s34));
  amp += c2_1234_1*(njetan->L1(s12,s34));
  amp += c2_1234_2*(njetan->L2hat(s12,s34));
  amp += c2_1234_3*(njetan->L3hat(s12,s34));
  amp += c2_tree*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s12, T(0.), T(0.)));
  amp += rat;

//  camp += conj(c1234)*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
//  camp += conj(c124)*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
//  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c134)*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
//  camp += conj(c245)*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
//  camp += conj(c2_4512_0)*(njetan->L0(s45,s12));
//  camp += conj(c2_4512_1)*(njetan->L1(s45,s12));
//  camp += conj(c2_4512_2)*(njetan->L2hat(s45,s12));
//  camp += conj(c2_4512_3)*(njetan->L3hat(s45,s12));
//  camp += conj(c2_1534_0)*(njetan->L0(s15,s34));
//  camp += conj(c2_1534_1)*(njetan->L1(s15,s34));
//  camp += conj(c2_1534_2)*(njetan->L2hat(s15,s34));
//  camp += conj(c2_1534_3)*(njetan->L3hat(s15,s34));
  camp += conj(c2_1234_0)*(njetan->L0(s12,s34));
  camp += conj(c2_1234_1)*(njetan->L1(s12,s34));
  camp += conj(c2_1234_2)*(njetan->L2hat(s12,s34));
  camp += conj(c2_1234_3)*(njetan->L3hat(s12,s34));
  camp += conj(c2_tree)*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s12, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//{{{ 25
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAf1325(const int* p) /* 1-1-111 */
{
  setxi(p);
  setsij(p);
  setaij(p);

  const TreeValue phase = i_*a14*a23*a23/(a12*a15*a34*a45);
//  c1234 = T(0.);
//  c1235 = T(0.);
//  c1245 = T(0.);
//  c1345 = T(0.);
//  c2345 = T(0.);
//  c123 = T(0.);
//  c124 = T(0.);
//  c125 = T(0.);
//  c134 = T(0.);
//  c135 = T(0.);
//  c145 = T(0.);
//  c234 = T(0.);
//  c235 = T(0.);
//  c245 = T(0.);
//  c345 = T(0.);
  const TreeValue t1 = x2 + x3;
  const TreeValue t2 = x1*x1;
  const TreeValue t3 = x1*t2;
  const TreeValue t4 = x2*x3;
  TreeValue t5 = T(1.)/x2;
  const TreeValue t6 = t5*t5;
  const TreeValue t7 = pow(x3, -2);
  const TreeValue c2_1234_0 = x2*(T(4.)*t4*(x1 + x3) + T(8.)*t2*t1)*t6*t7/T(12.);
  const TreeValue t8 = x2*x2;
  TreeValue t9 = T(4.)*x2;
  TreeValue t10 = T(4.)*t2;
  TreeValue t11 = T(8.)*x1;
  const TreeValue t12 = -T(1.) + x4;
  const TreeValue t13 = x3*t12;
  const TreeValue t14 = x4 - x5;
  TreeValue t15 = x1*(t13 + x2*x4) + t4*t14;
  t15 = T(1.)/t15;
  const TreeValue c2_1234_1 = -x1*x2*(t10*t8 + (x1*x2*(t11 + t9) + (t10 + t9*x1
    + T(2.)*t8)*x3)*x3)*t12*t6*t7*t15*T(0.5);
  t9 = t4 + x1*t1;
  t10 = t12*t12;
  const TreeValue t16 = t15*t15;
  const TreeValue c2_1234_2 = t2*t1*x2*(t11*t1 + T(4.)*t4)*t9*t10*t6*t7*t16*T(0.25);
  t11 = t1*t1;
  const TreeValue c2_1234_3 = -T(2.)/T(3.)*t3*t11*t9*t9*t12*t10*t5*t7*t15*t16;
  const TreeValue c2_tree = T(1.)/T(3.);
  t5 = T(3.)*x5;
  t9 = T(2.)*x4;
  t10 = x3*x3;
  const TreeValue rat = (-t3*t11*(x2*(-T(5.) + x4) + t13)*t12/T(3.) - T(2.)/T(9.)*x2*t8*x3*t10*t14 - t4*t2*t1*(x2*(T(7.)
    - t5 + (x4 - T(6.) + x5)*x4) + t13*(-T(2.) + x4 + x5))/T(3.) - x1*t8*t10*(x2*(T(6.) + t9 + (-T(9.) + t5)*x5)
    + x3*(T(1.) + t9 + (-T(6.) + t5)*x5))/T(9.))*t6*t7*t15;

  EpsTriplet<T> amp, camp;
//  amp += c1234*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
//  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
//  amp += c124*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
//  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c134*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
//  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
//  amp += c245*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  amp += c2_1234_0*(njetan->L0(s12,s34));
  amp += c2_1234_1*(njetan->L1(s12,s34));
  amp += c2_1234_2*(njetan->L2hat(s12,s34));
  amp += c2_1234_3*(njetan->L3hat(s12,s34));
  amp += c2_tree*(njetan->I2(s12, T(0.), T(0.))+njetan->I2(s34, T(0.), T(0.)));
  amp += rat;

//  camp += conj(c1234)*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
//  camp += conj(c124)*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
//  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c134)*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
//  camp += conj(c245)*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  camp += conj(c2_1234_0)*(njetan->L0(s12,s34));
  camp += conj(c2_1234_1)*(njetan->L1(s12,s34));
  camp += conj(c2_1234_2)*(njetan->L2hat(s12,s34));
  camp += conj(c2_1234_3)*(njetan->L3hat(s12,s34));
  camp += conj(c2_tree)*(njetan->I2(s12, T(0.), T(0.))+njetan->I2(s34, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//{{{ 26
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAf1326(const int* p) /* -11-111 */
{
  setxi(p);
  setsij(p);
  setaij(p);

  const TreeValue phase = -i_*a13*a13*a14/(a12*a15*a34*a45);
//  c1234 = T(0.);
//  c1235 = T(0.);
//  c1245 = T(0.);
//  c1345 = T(0.);
//  c2345 = T(0.);
//  c123 = T(0.);
//  c124 = T(0.);
//  c125 = T(0.);
//  c134 = T(0.);
//  c135 = T(0.);
//  c145 = T(0.);
//  c234 = T(0.);
//  c235 = T(0.);
//  c245 = T(0.);
//  c345 = T(0.);
//  c2_2315_0 = T(0.);
//  c2_2315_1 = T(0.);
//  c2_2315_2 = T(0.);
//  c2_2315_3 = T(0.);
  const TreeValue c2_1534_0 = -T(1.)/T(3.);
//  c2_1534_1 = T(0.);
//  c2_1534_2 = T(0.);
//  c2_1534_3 = T(0.);
  const TreeValue c2_1234_0 = T(2.)/T(3.);
  TreeValue t1 = -T(1.) + x4;
  TreeValue t2 = x2 + x3;
  const TreeValue t3 = x4 - x5;
  const TreeValue t4 = x2*t3;
  TreeValue t5 = (-x1 + t4)*x3 + x1*t2*x4;
  t5 = T(1.)/t5;
  const TreeValue c2_1234_1 = -x1*x2*t1*t5;
  const TreeValue t6 = t5*t5;
  const TreeValue t7 = x1*x1;
  const TreeValue t8 = t1*t1;
  const TreeValue c2_1234_2 = t7*x2*t2*t8*t6;
  const TreeValue c2_1234_3 = -T(2.)/T(3.)*x1*t7*x2*t2*t2*t1*t8*t5*t6;
  const TreeValue c2_tree = T(1.)/T(3.);
  t2 = T(3.)*x4;
  t1 = T(9.)*x1*(x3*t1 + x2*x4) + T(9.)*t4*x3;
  t1 = T(1.)/t1;
  const TreeValue rat = (-x1*(x2*(T(6.) + (-T(7.) + t2)*x4) + x3*(T(1.) + (-T(4.) + t2)*x4)) - T(2.)*x2*x3*t3)*t1;

  EpsTriplet<T> amp, camp;
//  amp += c1234*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
//  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
//  amp += c124*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
//  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c134*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
//  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
//  amp += c245*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
//  amp += c2_2315_0*(njetan->L0(s23,s15));
//  amp += c2_2315_1*(njetan->L1(s23,s15));
//  amp += c2_2315_2*(njetan->L2hat(s23,s15));
//  amp += c2_2315_3*(njetan->L3hat(s23,s15));
  amp += c2_1534_0*(njetan->L0(s15,s34));
//  amp += c2_1534_1*(njetan->L1(s15,s34));
//  amp += c2_1534_2*(njetan->L2hat(s15,s34));
//  amp += c2_1534_3*(njetan->L3hat(s15,s34));
  amp += c2_1234_0*(njetan->L0(s12,s34));
  amp += c2_1234_1*(njetan->L1(s12,s34));
  amp += c2_1234_2*(njetan->L2hat(s12,s34));
  amp += c2_1234_3*(njetan->L3hat(s12,s34));
  amp += c2_tree*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  amp += rat;

//  camp += conj(c1234)*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
//  camp += conj(c124)*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
//  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c134)*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
//  camp += conj(c245)*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
//  camp += conj(c2_2315_0)*(njetan->L0(s23,s15));
//  camp += conj(c2_2315_1)*(njetan->L1(s23,s15));
//  camp += conj(c2_2315_2)*(njetan->L2hat(s23,s15));
//  camp += conj(c2_2315_3)*(njetan->L3hat(s23,s15));
  camp += conj(c2_1534_0)*(njetan->L0(s15,s34));
//  camp += conj(c2_1534_1)*(njetan->L1(s15,s34));
//  camp += conj(c2_1534_2)*(njetan->L2hat(s15,s34));
//  camp += conj(c2_1534_3)*(njetan->L3hat(s15,s34));
  camp += conj(c2_1234_0)*(njetan->L0(s12,s34));
  camp += conj(c2_1234_1)*(njetan->L1(s12,s34));
  camp += conj(c2_1234_2)*(njetan->L2hat(s12,s34));
  camp += conj(c2_1234_3)*(njetan->L3hat(s12,s34));
  camp += conj(c2_tree)*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//}}}

//{{{ Af14
//{{{ 5
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAf145(const int* p) /* 1-11-1-1 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*sign(lS(2,3))*hAf1426(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 6
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAf146(const int* p) /* -111-1-1 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*sign(lS(2,3))*hAf1425(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 13
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAf1413(const int* p) /* 1-111-1 */
{
  setxi(p);
  setsij(p);
  setaij(p);
  const TreeValue phase = i_*a25*a25/(a12*a34*a45);
//  c1234 = T(0.);
//  c1235 = T(0.);
//  c1245 = T(0.);
//  c1345 = T(0.);
//  c2345 = T(0.);
//  c123 = T(0.);
//  c124 = T(0.);
//  c125 = T(0.);
//  c134 = T(0.);
//  c135 = T(0.);
//  c145 = T(0.);
//  c234 = T(0.);
//  c235 = T(0.);
//  c245 = T(0.);
//  c345 = T(0.);
  const TreeValue c2_4512_0 = T(2.)/T(3.);
//  c2_4512_1 = T(0.);
//  c2_4512_2 = T(0.);
//  c2_4512_3 = T(0.);
  const TreeValue c2_2345_0 = T(1.)/T(3.);
//  c2_2345_1 = T(0.);
//  c2_2345_2 = T(0.);
//  c2_2345_3 = T(0.);
//  c2_2315_0 = T(0.);
//  c2_2315_1 = T(0.);
//  c2_2315_2 = T(0.);
//  c2_2315_3 = T(0.);
  const TreeValue c2_tree = -T(1.)/T(3.);
  const TreeValue rat = T(2.)/T(9.);

  EpsTriplet<T> amp, camp;
//  amp += c1234*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
//  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
//  amp += c124*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
//  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c134*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
//  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
//  amp += c245*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  amp += c2_4512_0*(njetan->L0(s45,s12));
//  amp += c2_4512_1*(njetan->L1(s45,s12));
//  amp += c2_4512_2*(njetan->L2hat(s45,s12));
//  amp += c2_4512_3*(njetan->L3hat(s45,s12));
  amp += c2_2345_0*(njetan->L0(s23,s45));
//  amp += c2_2345_1*(njetan->L1(s23,s45));
//  amp += c2_2345_2*(njetan->L2hat(s23,s45));
//  amp += c2_2345_3*(njetan->L3hat(s23,s45));
//  amp += c2_2315_0*(njetan->L0(s23,s15));
//  amp += c2_2315_1*(njetan->L1(s23,s15));
//  amp += c2_2315_2*(njetan->L2hat(s23,s15));
//  amp += c2_2315_3*(njetan->L3hat(s23,s15));
  amp += c2_tree*(njetan->I2(s23, T(0.), T(0.))+njetan->I2(s45, T(0.), T(0.)));
  amp += rat;

//  camp += conj(c1234)*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
//  camp += conj(c124)*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
//  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c134)*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
//  camp += conj(c245)*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  camp += conj(c2_4512_0)*(njetan->L0(s45,s12));
//  camp += conj(c2_4512_1)*(njetan->L1(s45,s12));
//  camp += conj(c2_4512_2)*(njetan->L2hat(s45,s12));
//  camp += conj(c2_4512_3)*(njetan->L3hat(s45,s12));
  camp += conj(c2_2345_0)*(njetan->L0(s23,s45));
//  camp += conj(c2_2345_1)*(njetan->L1(s23,s45));
//  camp += conj(c2_2345_2)*(njetan->L2hat(s23,s45));
//  camp += conj(c2_2345_3)*(njetan->L3hat(s23,s45));
//  camp += conj(c2_2315_0)*(njetan->L0(s23,s15));
//  camp += conj(c2_2315_1)*(njetan->L1(s23,s15));
//  camp += conj(c2_2315_2)*(njetan->L2hat(s23,s15));
//  camp += conj(c2_2315_3)*(njetan->L3hat(s23,s15));
  camp += conj(c2_tree)*(njetan->I2(s23, T(0.), T(0.))+njetan->I2(s45, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//{{{ 14
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAf1414(const int* p) /* -1111-1 */
{
  setxi(p);
  setsij(p);
  setaij(p);
  const TreeValue phase = -i_*a15*a15/(a12*a34*a45);
//  c1234 = T(0.);
//  c1235 = T(0.);
//  c1245 = T(0.);
//  c1345 = T(0.);
//  c2345 = T(0.);
//  c123 = T(0.);
//  c124 = T(0.);
//  c125 = T(0.);
//  c134 = T(0.);
//  c135 = T(0.);
//  c145 = T(0.);
//  c234 = T(0.);
//  c235 = T(0.);
//  c245 = T(0.);
//  c345 = T(0.);
  const TreeValue c2_4512_0 = T(1.)/T(3.);
//  c2_4512_1 = T(0.);
//  c2_4512_2 = T(0.);
//  c2_4512_3 = T(0.);
  const TreeValue c2_tree = -T(1.)/T(3.);
  const TreeValue rat = T(2.)/T(9.);

  EpsTriplet<T> amp, camp;
//  amp += c1234*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
//  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
//  amp += c124*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
//  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c134*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
//  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
//  amp += c245*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  amp += c2_4512_0*(njetan->L0(s45,s12));
//  amp += c2_4512_1*(njetan->L1(s45,s12));
//  amp += c2_4512_2*(njetan->L2hat(s45,s12));
//  amp += c2_4512_3*(njetan->L3hat(s45,s12));
  amp += c2_tree*(njetan->I2(s45, T(0.), T(0.))+njetan->I2(s12, T(0.), T(0.)));
  amp += rat;

//  camp += conj(c1234)*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
//  camp += conj(c124)*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
//  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c134)*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
//  camp += conj(c245)*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  camp += conj(c2_4512_0)*(njetan->L0(s45,s12));
//  camp += conj(c2_4512_1)*(njetan->L1(s45,s12));
//  camp += conj(c2_4512_2)*(njetan->L2hat(s45,s12));
//  camp += conj(c2_4512_3)*(njetan->L3hat(s45,s12));
  camp += conj(c2_tree)*(njetan->I2(s45, T(0.), T(0.))+njetan->I2(s12, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//{{{ 17
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAf1417(const int* p) /* 1-1-1-11 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*sign(lS(2,3))*hAf1414(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 18
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAf1418(const int* p) /* -11-1-11 */
{
  const LoopResult<T> camp = -sign(lS(0,1))*sign(lS(2,3))*hAf1413(p);
  const LoopResult<T> res = {camp.loopC, camp.loop};
  return res;
}
//}}}
//{{{ 25
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAf1425(const int* p) /* 1-1-111 */
{
  setxi(p);
  setsij(p);
  setaij(p);
  const TreeValue phase = -i_*a23*a23/(a12*a34*a45);
//  c1234 = T(0.);
//  c1235 = T(0.);
//  c1245 = T(0.);
//  c1345 = T(0.);
//  c2345 = T(0.);
//  c123 = T(0.);
//  c124 = T(0.);
//  c125 = T(0.);
//  c134 = T(0.);
//  c135 = T(0.);
//  c145 = T(0.);
//  c234 = T(0.);
//  c235 = T(0.);
//  c245 = T(0.);
//  c345 = T(0.);
  const TreeValue c2_1234_0 = -T(1.)/T(3.);
//  c2_1234_1 = T(0.);
//  c2_1234_2 = T(0.);
//  c2_1234_3 = T(0.);
  const TreeValue c2_tree = -T(1.)/T(3.);
  const TreeValue rat = T(2.)/T(9.);

  EpsTriplet<T> amp, camp;
//  amp += c1234*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
//  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
//  amp += c124*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
//  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c134*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
//  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
//  amp += c245*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  amp += c2_1234_0*(njetan->L0(s12,s34));
//  amp += c2_1234_1*(njetan->L1(s12,s34));
//  amp += c2_1234_2*(njetan->L2hat(s12,s34));
//  amp += c2_1234_3*(njetan->L3hat(s12,s34));
  amp += c2_tree*(njetan->I2(s12, T(0.), T(0.))+njetan->I2(s34, T(0.), T(0.)));
  amp += rat;

//  camp += conj(c1234)*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
//  camp += conj(c124)*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
//  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c134)*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
//  camp += conj(c245)*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
  camp += conj(c2_1234_0)*(njetan->L0(s12,s34));
//  camp += conj(c2_1234_1)*(njetan->L1(s12,s34));
//  camp += conj(c2_1234_2)*(njetan->L2hat(s12,s34));
//  camp += conj(c2_1234_3)*(njetan->L3hat(s12,s34));
  camp += conj(c2_tree)*(njetan->I2(s12, T(0.), T(0.))+njetan->I2(s34, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//{{{ 26
template <typename T>
LoopResult<T> Amp4q1g_a<T>::hAf1426(const int* p) /* -11-111 */
{
  setxi(p);
  setsij(p);
  setaij(p);
  const TreeValue phase = i_*a13*a13/(a12*a34*a45);
//  c1234 = T(0.);
//  c1235 = T(0.);
//  c1245 = T(0.);
//  c1345 = T(0.);
//  c2345 = T(0.);
//  c123 = T(0.);
//  c124 = T(0.);
//  c125 = T(0.);
//  c134 = T(0.);
//  c135 = T(0.);
//  c145 = T(0.);
//  c234 = T(0.);
//  c235 = T(0.);
//  c245 = T(0.);
//  c345 = T(0.);
//  c2_2315_0 = T(0.);
//  c2_2315_1 = T(0.);
//  c2_2315_2 = T(0.);
//  c2_2315_3 = T(0.);
  const TreeValue c2_1534_0 = T(1.)/T(3.);
//  c2_1534_1 = T(0.);
//  c2_1534_2 = T(0.);
//  c2_1534_3 = T(0.);
  const TreeValue c2_1234_0 = -T(2.)/T(3.);
//  c2_1234_1 = T(0.);
//  c2_1234_2 = T(0.);
//  c2_1234_3 = T(0.);
  const TreeValue c2_tree = -T(1.)/T(3.);
  const TreeValue rat = T(2.)/T(9.);

  EpsTriplet<T> amp, camp;
//  amp += c1234*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  amp += c1235*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
//  amp += c1245*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  amp += c1345*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  amp += c2345*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c123*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
//  amp += c124*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
//  amp += c125*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  amp += c134*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  amp += c135*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
//  amp += c145*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  amp += c234*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  amp += c235*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
//  amp += c245*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  amp += c345*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
//  amp += c2_2315_0*(njetan->L0(s23,s15));
//  amp += c2_2315_1*(njetan->L1(s23,s15));
//  amp += c2_2315_2*(njetan->L2hat(s23,s15));
//  amp += c2_2315_3*(njetan->L3hat(s23,s15));
  amp += c2_1534_0*(njetan->L0(s15,s34));
//  amp += c2_1534_1*(njetan->L1(s15,s34));
//  amp += c2_1534_2*(njetan->L2hat(s15,s34));
//  amp += c2_1534_3*(njetan->L3hat(s15,s34));
  amp += c2_1234_0*(njetan->L0(s12,s34));
//  amp += c2_1234_1*(njetan->L1(s12,s34));
//  amp += c2_1234_2*(njetan->L2hat(s12,s34));
//  amp += c2_1234_3*(njetan->L3hat(s12,s34));
  amp += c2_tree*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  amp += rat;

//  camp += conj(c1234)*njetan->I4(s12, s23, T(0.), T(0.), T(0.), s45, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1235)*njetan->I4(s15, s12, T(0.), T(0.), T(0.), s34, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1245)*njetan->I4(s45, s15, T(0.), T(0.), T(0.), s23, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c1345)*njetan->I4(s34, s45, T(0.), T(0.), T(0.), s12, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c2345)*njetan->I4(s23, s34, T(0.), T(0.), T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c123)*njetan->I3(T(0.), T(0.), s12, T(0.), T(0.), T(0.));
//  camp += conj(c124)*njetan->I3(T(0.), s23, s45, T(0.), T(0.), T(0.));
//  camp += conj(c125)*njetan->I3(T(0.), s15, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c134)*njetan->I3(s12, T(0.), s45, T(0.), T(0.), T(0.));
//  camp += conj(c135)*njetan->I3(s12, s34, T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c145)*njetan->I3(s45, T(0.), T(0.), T(0.), T(0.), T(0.));
//  camp += conj(c234)*njetan->I3(T(0.), T(0.), s23, T(0.), T(0.), T(0.));
//  camp += conj(c235)*njetan->I3(T(0.), s34, s15, T(0.), T(0.), T(0.));
//  camp += conj(c245)*njetan->I3(s23, T(0.), s15, T(0.), T(0.), T(0.));
//  camp += conj(c345)*njetan->I3(T(0.), T(0.), s34, T(0.), T(0.), T(0.));
//  camp += conj(c2_2315_0)*(njetan->L0(s23,s15));
//  camp += conj(c2_2315_1)*(njetan->L1(s23,s15));
//  camp += conj(c2_2315_2)*(njetan->L2hat(s23,s15));
//  camp += conj(c2_2315_3)*(njetan->L3hat(s23,s15));
  camp += conj(c2_1534_0)*(njetan->L0(s15,s34));
//  camp += conj(c2_1534_1)*(njetan->L1(s15,s34));
//  camp += conj(c2_1534_2)*(njetan->L2hat(s15,s34));
//  camp += conj(c2_1534_3)*(njetan->L3hat(s15,s34));
  camp += conj(c2_1234_0)*(njetan->L0(s12,s34));
//  camp += conj(c2_1234_1)*(njetan->L1(s12,s34));
//  camp += conj(c2_1234_2)*(njetan->L2hat(s12,s34));
//  camp += conj(c2_1234_3)*(njetan->L3hat(s12,s34));
  camp += conj(c2_tree)*(njetan->I2(s34, T(0.), T(0.))+njetan->I2(s15, T(0.), T(0.)));
  camp += conj(rat);

  const LoopResult<T> res = {phase*amp, conj(phase)*camp};
  return res;
}
//}}}
//}}}

#ifdef USE_SD
  template class Amp4q1g_a<double>;
#endif
#ifdef USE_DD
  template class Amp4q1g_a<dd_real>;
#endif
#ifdef USE_QD
  template class Amp4q1g_a<qd_real>;
#endif
#ifdef USE_VC
  template class Amp4q1g_a<Vc::double_v>;
#endif
