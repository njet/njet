/*
* analytic/0q5g-analytic.h
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef ANALYTIC_0Q5G_ANALYTIC_H
#define ANALYTIC_0Q5G_ANALYTIC_H

#include "../chsums/0q5g.h"

template <typename T>
class Amp0q5g_a : public Amp0q5g<T>
{
    typedef Amp0q5g<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    typedef TreeValue (Amp0q5g_a::*HelAmp)(const int* ord);
    typedef LoopResult<T> (Amp0q5g_a::*HelAmpLoop)(const int* ord);

    Amp0q5g_a(const T scalefactor, const int mFC=1,
              const NJetAmpTables& tables=BaseClass::amptables());

  protected:
    using BaseClass::mhel;
    using BaseClass::mhelint;

    using BaseClass::njetan;
    using BaseClass::callTree;
    using BaseClass::callLoop;
    using BaseClass::sA;
    using BaseClass::sB;
    using BaseClass::lS;
    using BaseClass::CyclicSpinorsA;
    using BaseClass::CyclicSpinorsB;
    using BaseClass::HelicityOrder;

    T MuR2() { return njetan->getMuR2(); }

    virtual TreeValue A0(int p0, int p1, int p2, int p3, int p4);
    virtual LoopResult<T> AL(int p0, int p1, int p2, int p3, int p4);
    virtual LoopResult<T> AF(int p0, int p1, int p2, int p3, int p4);

    virtual LoopResult<T> AL(int p0, int p1, int p2, int p3, int p4, int /*pos*/) {
      return AL(p0, p1, p2, p3, p4);
    }
    virtual LoopResult<T> AF(int p0, int p1, int p2, int p3, int p4, int /*pos*/) {
      return AF(p0, p1, p2, p3, p4);
    }

    HelAmp hA0[32];
    HelAmpLoop hAg[32];
    HelAmpLoop hAf[32];

    TreeValue hA03(const int* ord);
    TreeValue hA05(const int* ord);
    TreeValue hA06(const int* ord);
    TreeValue hA07(const int* ord);
    TreeValue hA09(const int* ord);
    TreeValue hA010(const int* ord);
    TreeValue hA011(const int* ord);
    TreeValue hA012(const int* ord);
    TreeValue hA013(const int* ord);
    TreeValue hA014(const int* ord);
    TreeValue hA017(const int* ord);
    TreeValue hA018(const int* ord);
    TreeValue hA019(const int* ord);
    TreeValue hA020(const int* ord);
    TreeValue hA021(const int* ord);
    TreeValue hA022(const int* ord);
    TreeValue hA024(const int* ord);
    TreeValue hA025(const int* ord);
    TreeValue hA026(const int* ord);
    TreeValue hA028(const int* ord);

    LoopResult<T> hAg0(const int* p);
    LoopResult<T> hAg1(const int* p);
    LoopResult<T> hAg2(const int* p);
    LoopResult<T> hAg3(const int* p);
    LoopResult<T> hAg4(const int* p);
    LoopResult<T> hAg5(const int* p);
    LoopResult<T> hAg6(const int* p);
    LoopResult<T> hAg7(const int* p);
    LoopResult<T> hAg8(const int* p);
    LoopResult<T> hAg9(const int* p);
    LoopResult<T> hAg10(const int* p);
    LoopResult<T> hAg11(const int* p);
    LoopResult<T> hAg12(const int* p);
    LoopResult<T> hAg13(const int* p);
    LoopResult<T> hAg14(const int* p);
    LoopResult<T> hAg15(const int* p);
    LoopResult<T> hAg16(const int* p);
    LoopResult<T> hAg17(const int* p);
    LoopResult<T> hAg18(const int* p);
    LoopResult<T> hAg19(const int* p);
    LoopResult<T> hAg20(const int* p);
    LoopResult<T> hAg21(const int* p);
    LoopResult<T> hAg22(const int* p);
    LoopResult<T> hAg23(const int* p);
    LoopResult<T> hAg24(const int* p);
    LoopResult<T> hAg25(const int* p);
    LoopResult<T> hAg26(const int* p);
    LoopResult<T> hAg27(const int* p);
    LoopResult<T> hAg28(const int* p);
    LoopResult<T> hAg29(const int* p);
    LoopResult<T> hAg30(const int* p);
    LoopResult<T> hAg31(const int* p);

    LoopResult<T> hAf0(const int* p);
    LoopResult<T> hAf1(const int* p);
    LoopResult<T> hAf2(const int* p);
    LoopResult<T> hAf3(const int* p);
    LoopResult<T> hAf4(const int* p);
    LoopResult<T> hAf5(const int* p);
    LoopResult<T> hAf6(const int* p);
    LoopResult<T> hAf7(const int* p);
    LoopResult<T> hAf8(const int* p);
    LoopResult<T> hAf9(const int* p);
    LoopResult<T> hAf10(const int* p);
    LoopResult<T> hAf11(const int* p);
    LoopResult<T> hAf12(const int* p);
    LoopResult<T> hAf13(const int* p);
    LoopResult<T> hAf14(const int* p);
    LoopResult<T> hAf15(const int* p);
    LoopResult<T> hAf16(const int* p);
    LoopResult<T> hAf17(const int* p);
    LoopResult<T> hAf18(const int* p);
    LoopResult<T> hAf19(const int* p);
    LoopResult<T> hAf20(const int* p);
    LoopResult<T> hAf21(const int* p);
    LoopResult<T> hAf22(const int* p);
    LoopResult<T> hAf23(const int* p);
    LoopResult<T> hAf24(const int* p);
    LoopResult<T> hAf25(const int* p);
    LoopResult<T> hAf26(const int* p);
    LoopResult<T> hAf27(const int* p);
    LoopResult<T> hAf28(const int* p);
    LoopResult<T> hAf29(const int* p);
    LoopResult<T> hAf30(const int* p);
    LoopResult<T> hAf31(const int* p);

    // momentum twistor variables
    std::complex<T> x1, x2, x3, x4, x5;
    // 2-particle invariants
    T s12, s23, s34, s45, s15;

    void setxi(const int *ord);
    void setsij(const int *ord);
};

#endif /* ANALYTIC_0Q5G_ANALYTIC_H */
