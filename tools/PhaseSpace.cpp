#include "PhaseSpace.h"
#include "PSgenerator.h"

template <typename T>
template <typename U>
PhaseSpace<T>::PhaseSpace(const int n_, const Flavour<U>* flavs, const int rseed_, double SqrtS_, double PtMin_, double EtaMax_, double DeltaR_) :
  n(n_), rseed(rseed_),
  SqrtS(SqrtS_), PtMin(PtMin_), EtaMax(EtaMax_), DeltaR(DeltaR_),
  rnd(n), decay(0), weight()
{
  Set(n, flavs);
  Banner();
}

template <typename T>
template <typename U>
PhaseSpace<T>::PhaseSpace(const std::vector<Flavour<U> >& flavs, const int rseed_, double SqrtS_, double PtMin_, double EtaMax_, double DeltaR_) :
  n(flavs.size()), rseed(rseed_),
  SqrtS(SqrtS_), PtMin(PtMin_), EtaMax(EtaMax_), DeltaR(DeltaR_),
  rnd(n), decay(0), weight()
{
  Set(flavs);
  Banner();
}

template <typename T>
PhaseSpace<T>::PhaseSpace(const int n_, const int rseed_, double SqrtS_, double PtMin_, double EtaMax_, double DeltaR_) :
  n(n_), rseed(rseed_),
  SqrtS(SqrtS_), PtMin(PtMin_), EtaMax(EtaMax_), DeltaR(DeltaR_),
  rnd(n), decay(0), weight()
{
  Initialize(n);
  Banner();
}

template <typename T>
void PhaseSpace<T>::Banner()
{
  DEBUG_MSG(0, "# PhaseSpace generator initialized with:")
  DEBUG_MSG(0, "# seed = " << rseed)
  DEBUG_MSG(0, "# SqrtS = " << SqrtS)
  DEBUG_MSG(0, "# PtMin = " << PtMin << "*SqrtS = " << PtMin*SqrtS)
  DEBUG_MSG(0, "# EtaMax = " << EtaMax)
  DEBUG_MSG(0, "# DeltaR = " << DeltaR)
}

template <typename T>
template <typename U>
void PhaseSpace<T>::Set(const int nflavs, const Flavour<U>*  flavs)
{
  std::vector<Flavour<U> > vflavs(n);
  for (int i = 0; i < nflavs; i++) {
    vflavs[i] = flavs[i];
  }
  Set(vflavs);
}

template <typename T>
template <typename U>
void PhaseSpace<T>::Set(const std::vector<Flavour<U> >& flavs)
{
  int nflavs = flavs.size();
  int m = nflavs;
  for (int i = 0; i < nflavs; i++) {
    if(isMassiveVector(flavs[i].idx())) m++;
  }
  Initialize(m);

  int j=0;
  for (int i = 0; i < nflavs; i++) {
    if(isMassiveVector(flavs[i].idx())) {
      mass[j] = T(0.);
      mass[j+1] = T(0.);
      j++;
    }
    mass[j] = flavs[i].Mass();
    j++;
  }
  DEBUG_MSG(0, "# PhaseSpace now has " << n << " particles")
}

template <typename T>
void PhaseSpace<T>::Initialize(int n_)
{
  n = n_;
  mom.resize(n);
  mass.resize(n);
  eta.resize(n);
  phi.resize(n);
  pT.resize(n);

  rnd.resize(n);

  haveinputleft = 1;
  if (rseed > 0) {
    rnd.init(rseed);
    haveinput = 0;
  } else {
    haveinput = 1;
  }

}

template <typename T>
void PhaseSpace<T>::setDecay()
{
  decay = 1;
  rnd.resize(n+1);

  SqrtS = to_double(mass[0]);
  if (SqrtS < 1e-10) {
    std::cout << "### can't decay massless particle ###" << std::endl;
    exit(0);
  }

}

template <typename T>
void PhaseSpace<T>::UpdateKinematics()
{
  for (int i=0; i<n; i++) {
    T pTsq = mom[i].x1*mom[i].x1 + mom[i].x2*mom[i].x2;
    pT[i] = sqrt(pTsq);

    if (pTsq < 1e-16) {
      phi[i] = T(0.);
    } else {
      phi[i] = acos(mom[i].x1/pT[i]);
      if (mom[i].x2 > 0.) {
        // FIXME M_PI replaced with higher precision version
        phi[i] -= 2.*M_PI;
        phi[i] *= -1.;
      }
    }

    T num = mom[i].x0 + mom[i].x3;
    T den = mom[i].x0 - mom[i].x3;
    if (abs(num) < 1e-16) {
      eta[i]= T(1.);
    }
    else if (abs(den) < 1e-16) {
      eta[i]  = T(1e16);
    }
    else {
      eta[i] = 0.5*log(num/den);
    }
  }
}

template <typename T>
void PhaseSpace<T>::Generate(const double* rn)
{
  std::vector<T> vfinal_moms((n-2)*4);
  T (*final_moms)[][4] = reinterpret_cast<T(*)[][4]>(&vfinal_moms[0]);

  const T s = SqrtS * SqrtS;
  const T m0 = mass[0];
  const T m1 = mass[1];
  const T x1 = (m0*m0 - m1*m1);
  const T x2 = (m0*m0 + m1*m1);
  const T beta = sqrt(1 - 2.*x2/s + x1*x1/s/s);

  const T tmp00 = -(1.+x1/s)*SqrtS*0.5;
  const T tmp10 = -(1.-x1/s)*SqrtS*0.5;
  const T tmp3 = SqrtS*0.5*beta;

  mom[0] = MOM<T>(tmp00, 0., 0.,  tmp3);
  mom[1] = MOM<T>(tmp10, 0., 0., -tmp3);

  std::vector<T> masses(n-2);

  for (int i=2; i<n; i++) {
    masses[i-2] = mass[i];
  }

  eventn<T>(n-2, SqrtS, rn, &masses[0], *final_moms, weight);
  // put back overall factor of (2*pi)^(4-3*nfinal)
  weight *= pow(2.*M_PI, 4.-3.*(n-2));

  for (int i=2; i<n; i++) {
    mom[i] = MOM<T>((*final_moms)[i-2][0], (*final_moms)[i-2][1], (*final_moms)[i-2][2], (*final_moms)[i-2][3]);
  }

}

template <typename T>
void PhaseSpace<T>::GenerateDecay(const double* rn)
{
  std::vector<T> vfinal_moms((n-1)*4);
  T (*final_moms)[][4] = reinterpret_cast<T(*)[][4]>(&vfinal_moms[0]);

  mom[0] = MOM<T>(-SqrtS, 0., 0., 0.);
  std::vector<T> masses(n-1);

  for (int i=1; i<n; i++) {
    masses[i-1] = mass[i];
  }

  eventn<T>(n-1, SqrtS, rn, &masses[0], *final_moms, weight);
  // put back overall factor of (2*pi)^(4-3*nfinal)
  weight *= pow(2.*M_PI, 4.-3.*(n-1));

  for (int i=1; i<n; i++) {
    mom[i] = MOM<T>((*final_moms)[i-1][0], (*final_moms)[i-1][1], (*final_moms)[i-1][2], (*final_moms)[i-1][3]);
  }
}

template <typename T>
bool PhaseSpace<T>::checkPS() {
  UpdateKinematics();
  int ninit = 2;
  if (decay) { ninit = 1; }

  for (int i = ninit; i < n; i++) {
    if (pT[i] < T(PtMin*SqrtS)) {
      DEBUG_MSG(0, "### cutting : Pt[" <<i<< "] = " << pT[i] << " < " << PtMin)
      return 1;
    }
    if (abs(eta[i]) > T(EtaMax)) {
      DEBUG_MSG(0, "### cutting : |eta[" <<i<< "]| = " << abs(eta[i]) << " > " << EtaMax)
      return 1;
    }
    for (int j = ninit; j < i; j++) {
      const T etaij = eta[i] - eta[j];
      const T phiij = phi[i] - phi[j];
      const T deltaR = sqrt(etaij*etaij + phiij*phiij);
      if (deltaR < T(DeltaR)) {
        DEBUG_MSG(0, "### cutting : DeltaR[" << i << "," << j << "] = " << deltaR << "< " << DeltaR)
        return 1;
      }
    }
  }
  return 0;
}

template <typename T>
std::vector<MOM<T> > PhaseSpace<T>::Jade(const std::vector<MOM<T> > jetmom, const T eps) {
  const int njets = jetmom.size();
  T rij;
  T rijmin = T(1e300);
  int min1, min2;
  for (int i = 0; i < njets-1; i++) {
    for (int j = i+1; j < njets; j++) {
      rij = dot(jetmom[i], jetmom[j]);
      if (rij < rijmin) {
        rijmin = rij;
        min1 = i;
        min2 = j;
      }
    }
  }

  std::vector<MOM<T> > newjetmom = jetmom;
  if (rijmin < eps*SqrtS*SqrtS) {
    newjetmom[min1] = jetmom[min1]+jetmom[min2];
    newjetmom.erase(newjetmom.begin()+min2);
  }
  return newjetmom;
}

template <typename T>
int PhaseSpace<T>::JadeJets(const T eps) {
  int ninit = 2;
  if (decay) { ninit = 1; }

  std::vector<MOM<T> > jetmom(n-ninit);
  for (int i = ninit; i < n; i++) {
    jetmom[i-ninit] = mom[i];
  }

  return JadeJets(eps, jetmom);
}

template <typename T>
int PhaseSpace<T>::JadeJets(const T eps, std::vector<MOM<T> > jetmom) {

  unsigned njets = jetmom.size()+1;
  while (jetmom.size() < njets) {
    njets = jetmom.size();
    jetmom = Jade(jetmom, eps);
  }

  return njets;
}

template <typename T>
void PhaseSpace<T>::FillMomArray(T output[][4])
{
  for (int i = 0; i < n; i++) {
    output[i][0] = mom[i].x0;
    output[i][1] = mom[i].x1;
    output[i][2] = mom[i].x2;
    output[i][3] = mom[i].x3;
  }
}

template <typename T>
void PhaseSpace<T>::skip(int sk)
{
  for (int i = 0; i < sk; i++) {
    Generate(rnd.next());
    while (checkPS()) {
      Generate(rnd.next());
    }
  }
}

template <typename T>
std::vector<MOM<T> > PhaseSpace<T>::getPSpoint()
{
  if (haveinput && haveinputleft) {
    int readseed;
    unsigned long int readseqnum;
    std::cin >> readseed >> readseqnum;
    if (std::cin.eof()) {
      haveinputleft = 0;
      DEBUG_MSG(0, "# I've run out of phase-space points...")
      return mom;
    }
    if (rnd.curseed() != readseed) {
      rnd.init(readseed);
    }
    do {
      rnd.next();
    } while (rnd.seqnum() != readseqnum);
    if (decay) {
      GenerateDecay(rnd.current());
    } else {
      Generate(rnd.current());
    }
  } else {
    if (decay) {
      GenerateDecay(rnd.next());
    } else {
      Generate(rnd.next());
    }
    while (checkPS()) {
      if (decay) {
        GenerateDecay(rnd.next());
      } else {
        Generate(rnd.next());
      }
    }
  }
  return mom;
}

template <typename T>
std::vector<MOM<T> > PhaseSpace<T>::getPSpoint(const double *rn)
{
  if (decay) {
    GenerateDecay(rn);
  } else {
    Generate(rn);
  }
  return mom;
}

template <typename T>
std::vector<MOM<T> > PhaseSpace<T>::Mom()
{
  return mom;
}

template <typename T>
void PhaseSpace<T>::showPSpoint()
{
  std::cout << "# rnd seed " << rnd.curseed() << ", seq " << rnd.seqnum() << std::endl;
  for (int i = 0; i < n; i++) {
    std::cout << "# p" << i << " = " << mom[i] << std::endl;
  }
}

template <typename T>
const double* PhaseSpace<T>::current() {
  return rnd.current();
}

template <typename T>
int PhaseSpace<T>::seqnum() {
  return rnd.seqnum();
}

template <typename T>
int PhaseSpace<T>::curseed() {
  return rnd.curseed();
}

template class PhaseSpace<double>;
template PhaseSpace<double>::PhaseSpace(const int n_, const Flavour<double>* flavs, const int rseed_, double SqrtS_, double PtMin_, double EtaMax_, double DeltaR_);
template PhaseSpace<double>::PhaseSpace(const std::vector<Flavour<double> >& flavs, const int rseed_, double SqrtS_, double PtMin_, double EtaMax_, double DeltaR_);
#ifdef USE_DD
  template class PhaseSpace<dd_real>;
  template PhaseSpace<dd_real>::PhaseSpace(const int n_, const Flavour<double>* flavs, const int rseed_, double SqrtS_, double PtMin_, double EtaMax_, double DeltaR_);
  template PhaseSpace<dd_real>::PhaseSpace(const std::vector<Flavour<double> >& flavs, const int rseed_, double SqrtS_, double PtMin_, double EtaMax_, double DeltaR_);
#endif
#ifdef USE_QD
  template class PhaseSpace<qd_real>;
  template PhaseSpace<qd_real>::PhaseSpace(const int n_, const Flavour<double>* flavs, const int rseed_, double SqrtS_, double PtMin_, double EtaMax_, double DeltaR_);
  template PhaseSpace<qd_real>::PhaseSpace(const std::vector<Flavour<double> >& flavs, const int rseed_, double SqrtS_, double PtMin_, double EtaMax_, double DeltaR_);
#endif
