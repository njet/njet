/*
* tools/PhaseSpace.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef TOOLS_PHASESPACE_H
#define TOOLS_PHASESPACE_H

#include <vector>

#include "random.h"
#include "../ngluon2/Mom.h"
#include "../ngluon2/Flavour.h"
#include "../ngluon2/debug.h"

template <typename T>
class PhaseSpace : public ModelBase
{
  public:
    PhaseSpace(const int n_ = 4, int rseed_ = 1, double SqrtS_ = 7e3, double PtMin_ = 0.003, double EtaMax_ = 2.8, double DeltaR_ = 0.4);
    template <typename U>
    PhaseSpace(int n_, const Flavour<U>* flav, int rseed_ = 1, double SqrtS_ = 7e3, double PtMin_ = 0.003, double EtaMax_ = 2.8, double DeltaR_ = 0.4);
    template <typename U>
    PhaseSpace(const std::vector<Flavour<U> >& flav, int rseed_ = 1, double SqrtS_ = 7e3, double PtMin_ = 0.003, double EtaMax_ = 2.8, double DeltaR_ = 0.4);

    void Initialize(int n);
    void Banner();
    template <typename U>
    void Set(const int nflavs, const Flavour<U>* flav);
    template <typename U>
    void Set(const std::vector<Flavour<U> >& flav);
    void setDecay();

    std::vector<MOM<T> > getPSpoint();
    std::vector<MOM<T> > getPSpoint(const double* rn);

    void showPSpoint();
    void skip(int sk);
    void FillMomArray(T output[][4]);

    int seqnum();
    int curseed();
    const double* current();

    std::vector<MOM<T> > Mom();
    inline
    const MOM<T>& operator[] (int i) const {
      return mom[i];
    }
    bool checkPS();
    std::vector<MOM<T> > Jade(const std::vector<MOM<T> > jetmom, const T eps);
    int JadeJets(const T eps);
    int JadeJets(const T eps, std::vector<MOM<T> > jetmom);
    T getWeight() { return weight; }

  protected:
    int n, rseed;

    double SqrtS, PtMin, EtaMax, DeltaR;

    RandNums rnd;
    bool haveinput, haveinputleft;
    std::vector<MOM<T> > mom;
    std::vector<T> mass;
    std::vector<T> eta;
    std::vector<T> phi;
    std::vector<T> pT;

    void UpdateKinematics();
    void Generate(const double* rn);
    void GenerateDecay(const double* rn);
    bool decay;
    T weight;

};

#endif /* TOOLS_PHASESPACE_H */
