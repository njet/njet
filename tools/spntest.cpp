#include <cstdlib>
#include <fstream>
#include <sstream>
#include <time.h>
#include "../ngluon2/Current.h"
#include "random.h"

using namespace std;


int main ()
{
  cout.setf(ios_base::scientific, ios_base::floatfield);
  cout.precision(16);

  const int N=4;
  RandNums rnd(N);

  int hh[]={-1,-1,+1,+1};
  int ff[]={0,0,0,0};
  PTLS<double> process(N);
  process.set(ff,hh);
//  process.getPSpoint(7.0,rnd.next());
  MOM<double> ppp[4] = {
    MOM<double>(8.8317608663278469  ,2.0000000000000000 ,5.0000000000000000 ,7.0000000000000000  ),
    MOM<double>(10.8166538263919679 ,4.0000000000000000 ,1.0000000000000000 ,10.0000000000000000 ),
    MOM<double>(-4.3129060386958936 ,-2.4900574624306367,-2.4900574624306367,-2.4900574624306367 ),
    MOM<double>(-15.3355086540239211,-3.5099425375693633,-3.5099425375693633,-14.5099425375693633)
  };
  process.setMoms(ppp);

  cout << process << endl;

  cout << "checking massless spin sums" << endl;
  cout << "real momenta:" << endl;
  MOM<double> p = process[3].mom;
  cout << "# v(-,-p).U(+,p) + v(+,-p).U(-,p) = i_*slash(p)" << endl;
  cout << (ContractMatrix(vspn(-1,-p),Uspn(1,p))+ContractMatrix(vspn(1,-p),Uspn(-1,p)) + i_*SpnMatrix<double>(p)).iszero() << endl;
  cout << "# v(-,p).U(+,-p) + v(+,p).U(-,-p) = i_*slash(p)" << endl;
  cout << (ContractMatrix(vspn(-1,p),Uspn(1,-p))+ContractMatrix(vspn(1,p),Uspn(-1,-p)) + i_*SpnMatrix<double>(p)).iszero() << endl;
  cout << "# u(-,p).U(-,p) + u(+,-p).U(+,p) = slash(p)" << endl;
  cout << (ContractMatrix(uspn(-1,p),Uspn(-1,p))+ContractMatrix(uspn(1,p),Uspn(1,p)) - SpnMatrix<double>(p)).iszero() << endl;
  cout << "# v(-,p).V(-,p) + v(+,p).V(+,p) = slash(p)" << endl;
  cout << (ContractMatrix(vspn(-1,p),Vspn(-1,p))+ContractMatrix(vspn(1,p),Vspn(1,p)) - SpnMatrix<double>(p)).iszero() << endl;

  cout << "complex momenta:" << endl;
  MOM<complex<double> > cp = process[3].mom;
  cout << "# v(-,-p).U(+,p) + v(+,-p).U(-,p) = -slash(p)" << endl;
  cout << (ContractMatrix(vspn(-1,-cp),Uspn(1,cp))+ContractMatrix(vspn(1,-cp),Uspn(-1,cp)) + SpnMatrix<double>(cp)).iszero() << endl;
  cout << "# v(-,p).U(+,-p) + v(+,p).U(-,-p) = slash(p)" << endl;
  cout << (ContractMatrix(vspn(-1,cp),Uspn(1,-cp))+ContractMatrix(vspn(1,cp),Uspn(-1,-cp)) - SpnMatrix<double>(cp)).iszero() << endl;
  cout << "# u(-,p).U(-,p) + u(+,-p).U(+,p) = slash(p)" << endl;
  cout << (ContractMatrix(uspn(-1,cp),Uspn(-1,cp))+ContractMatrix(uspn(1,cp),Uspn(1,cp)) - SpnMatrix<double>(cp)).iszero() << endl;
  cout << "# v(-,p).V(-,p) + v(+,p).V(+,p) = slash(p)" << endl;
  cout << (ContractMatrix(vspn(-1,cp),Vspn(-1,cp))+ContractMatrix(vspn(1,cp),Vspn(1,cp)) - SpnMatrix<double>(cp)).iszero() << endl;

  cout << "checking massive spin sums" << endl;
  cout << "real momenta:" << endl;
  MOM<double> q(4.,1.,2.,3.);
  MOM<double> e(3.,2.,2.,1.);
  double m = sqrt(2.);
  cout << "# v(-,-q).U(+,q) + v(+,-q).U(-,q) = -i_*(slash(q)+m)" << endl;
  cout << (ContractMatrix(vspn(-1,-q,e,m),Uspn(1,q,e,m))+ContractMatrix(vspn(1,-q,e,m),Uspn(-1,q,e,m))
       -i_*(SpnMatrix<double>(q)+SpnMatrix<double>(m) ) ).iszero() << endl;
  cout << "# v(-,q).U(+,-q) + v(+,q).U(-,-q) = i_*(slash(q)-m)" << endl;
  cout << (ContractMatrix(vspn(-1,q,e,m),Uspn(1,-q,e,m))+ContractMatrix(vspn(1,q,e,m),Uspn(-1,-q,e,m))
       -i_*(SpnMatrix<double>(q)-SpnMatrix<double>(m) ) ).iszero() << endl;
  cout << "# u(-,q).U(-,q) + u(+,-q).U(+,q) = slash(q)+m" << endl;
  cout << (ContractMatrix(uspn(-1,q,e,m),Uspn(-1,q,e,m))+ContractMatrix(uspn(1,q,e,m),Uspn(1,q,e,m))
      - (SpnMatrix<double>(q)+SpnMatrix<double>(m) ) ).iszero() << endl;
  cout << "# v(-,q).V(-,q) + v(+,q).V(+,q) = slash(q)-m" << endl;
  cout << (ContractMatrix(vspn(-1,q,e,m),Vspn(-1,q,e,m))+ContractMatrix(vspn(1,q,e,m),Vspn(1,q,e,m))
      - (SpnMatrix<double>(q)-SpnMatrix<double>(m) ) ).iszero() << endl;

  cout << "complex momenta:" << endl;
  MOM<complex<double> > cq(4.,1.,2.,3.);
  MOM<complex<double> > ce(3.,2.,2.,1.);
//  MOM<complex<double> > ce(3.,1.,3.,-i_);
  complex<double> cm = sqrt(2.);
  cout << "# v(-,-q).U(+,q) + v(+,-q).U(-,q) = -(slash(q)+m)" << endl;
  cout << (ContractMatrix(vspn(-1,-cq,ce,cm),Uspn(1,cq,ce,cm))+ContractMatrix(vspn(1,-cq,ce,cm),Uspn(-1,cq,ce,cm))
       +(SpnMatrix<double>(cq)+SpnMatrix<double>(cm) ) ).iszero() << endl;
  cout << "# v(-,q).U(+,-q) + v(+,q).U(-,-q) = -(slash(q)-m)" << endl;
  cout << (ContractMatrix(vspn(-1,cq,ce,cm),Uspn(1,-cq,ce,cm))+ContractMatrix(vspn(1,cq,ce,cm),Uspn(-1,-cq,ce,cm))
      - (SpnMatrix<double>(cq)-SpnMatrix<double>(cm) ) ).iszero() << endl;
  cout << "# u(-,q).U(-,q) + u(+,-q).U(+,q) = slash(q)+m" << endl;
  cout << (ContractMatrix(uspn(-1,cq,ce,cm),Uspn(-1,cq,ce,cm))+ContractMatrix(uspn(1,cq,ce,cm),Uspn(1,cq,ce,cm))
      - (SpnMatrix<double>(cq)+SpnMatrix<double>(cm) ) ).iszero() << endl;
  cout << "# v(-,q).V(-,q) + v(+,q).V(+,q) = slash(q)-m" << endl;
  cout << (ContractMatrix(vspn(-1,cq,ce,cm),Vspn(-1,cq,ce,cm))+ContractMatrix(vspn(1,cq,ce,cm),Vspn(1,cq,ce,cm))
      - (SpnMatrix<double>(cq)-SpnMatrix<double>(cm) ) ).iszero() << endl;

  cout << "Complex\n";
  cout << "U(-1).u(-1) - 2*m = " <<
  Contract(Uspn(-1,cq,ce,cm),uspn(-1,cq,ce,cm))-2.*cm << endl;
  cout << "U(+1).u(+1) - 2*m = " <<
  Contract(Uspn(1,cq,ce,cm),uspn(1,cq,ce,cm))-2.*cm << endl;
  cout << "V(-1).v(-1) + 2*m = " <<
  Contract(Vspn(-1,cq,ce,cm),vspn(-1,cq,ce,cm))+2.*cm << endl;
  cout << "V(+1).v(+1) + 2*m = " <<
  Contract(Vspn(1,cq,ce,cm),vspn(1,cq,ce,cm))+2.*cm << endl;

  cout << "Real\n";
  cout << "U(-1).u(-1) - 2*m = " <<
  Contract(Uspn(-1,q,e,m),uspn(-1,q,e,m))-2.*cm << endl;
  cout << "U(+1).u(+1) - 2*m = " <<
  Contract(Uspn(1,q,e,m),uspn(1,q,e,m))-2.*cm << endl;
  cout << "V(-1).v(-1) + 2*m = " <<
  Contract(Vspn(-1,q,e,m),vspn(-1,q,e,m))+2.*cm << endl;
  cout << "V(+1).v(+1) + 2*m = " <<
  Contract(Vspn(1,q,e,m),vspn(1,q,e,m))+2.*cm << endl;

  AMP<double> tree(N);
  complex<double> treeval;
  tree.Initialize( process );

  MOM<double> p0 = process[0].mom;
  MOM<double> p1 = process[1].mom;
  MOM<double> p2 = process[2].mom;
  MOM<double> p3 = process[3].mom;
  cout << "MHV(-,+,-,+) = " <<  i_*pow(spA(p0,p2),4.)/(spA(p0,p1)*spA(p1,p2)*spA(p2,p3)*spA(p3,p0)) << endl;
  cout << "MHV(-,-,+,+) = " <<  i_*pow(spA(p0,p1),4.)/(spA(p0,p1)*spA(p1,p2)*spA(p2,p3)*spA(p3,p0)) << endl;

  complex<double> tt1 = i_*pow(spA(p0,p2),3.)*spA(p1,p2)/(spA(p0,p1)*spA(p1,p2)*spA(p2,p3)*spA(p3,p0));
  complex<double> tt2 = i_*pow(spB(p0,p2),3.)*spB(p1,p2)/(spB(p0,p1)*spB(p1,p2)*spB(p2,p3)*spB(p3,p0));
  cout << tt1*tt2 << endl;

  MOM<complex<double> > cp0 = process[0].mom;
  MOM<complex<double> > cp1 = process[1].mom;
  MOM<complex<double> > cp2 = process[2].mom;
  MOM<complex<double> > cp3 = process[3].mom;
  complex<double> tt3 = i_*pow(xspA(cp0,cp2),3.)*xspA(cp1,cp2)/(xspA(cp0,cp1)*xspA(cp1,cp2)*xspA(cp2,cp3)*xspA(cp3,cp0));
  complex<double> tt4 = i_*pow(xspB(cp0,cp2),3.)*xspB(cp1,cp2)/(xspB(cp0,cp1)*xspB(cp1,cp2)*xspB(cp2,cp3)*xspB(cp3,cp0));
  cout << tt3*tt4 << endl;

  cout << spA(p1,p2)*spB(p1,p2)+S(p1+p2) << endl;
  cout << xspA(cp1,cp2)*xspB(cp2,cp1) << endl;
  cout << S(cp1+cp2) << endl;
  cout << endl;

  cout << "Real spinor products\n";
  cout << " A " << spA(p2,p3) << " " << spA(p3,p2) << endl;
  cout << " B " << spB(p2,p3) << " " << spB(p3,p2) << endl;

  cout << "xA " << xspA(p2,p3) << " " << xspA(p3,p2) << endl;
  cout << "xB " << xspB(p2,p3) << " " << xspB(p3,p2) << endl << endl;

  cout << endl;

  cout << " A " << spA(p2,-p3) << " " << spA(-p3,p2) << endl;
  cout << " B " << spB(p2,-p3) << " " << spB(-p3,p2) << endl;

  cout << "xA " << xspA(p2,-p3) << " " << xspA(-p3,p2) << endl;
  cout << "xB " << xspB(p2,-p3) << " " << xspB(-p3,p2) << endl << endl;

  cout << endl;

  cout << " A " << spA(-p2,p3) << " " << spA(p3,-p2) << endl;
  cout << " B " << spB(-p2,p3) << " " << spB(p3,-p2) << endl;

  cout << "xA " << xspA(-p2,p3) << " " << xspA(p3,-p2) << endl;
  cout << "xB " << xspB(-p2,p3) << " " << xspB(p3,-p2) << endl;

  MOM<complex<double> > l1(complex<double>(-1.1241008124329710e-15,6.5454524294871979e+00),complex<double>(-1.1102230246251565e-15,2.1405742254155156e+00),
      complex<double>(2.4147350785597155e-15,2.3163732215517370e+00),complex<double>(-2.0539125955565396e-15,5.9921222943665491e+00));
  MOM<complex<double> > l2(complex<double>(-1.0816653826391970e+01,6.5454524294871979e+00),complex<double>(-4.0000000000000009e+00,2.1405742254155156e+00),
      complex<double>(-9.9999999999999756e-01,2.3163732215517370e+00),complex<double>(-1.0000000000000002e+01,5.9921222943665491e+00));
  MOM<complex<double> > l0(complex<double>(8.8317608663278460e+00,6.5454524294871979e+00),complex<double>(1.9999999999999989e+00,2.1405742254155156e+00),
      complex<double>(5.0000000000000027e+00,2.3163732215517370e+00),complex<double>(6.9999999999999982e+00,5.9921222943665491e+00));

  complex<double> mt = process.getMass(6);
  MOM<complex<double> > l1f = l1 - mt*mt/(2.*dot(l1,ce))*ce;
  MOM<complex<double> > l2f = l2 - mt*mt/(2.*dot(l2,ce))*ce;
  MOM<complex<double> > l0f = l0 - mt*mt/(2.*dot(l0,ce))*ce;

  PTL<complex<double> > L1(6,1,l1,mt);
  PTL<complex<double> > L2(6,1,l2,mt);
  PTL<complex<double> > L0(6,-1,l0,mt);

  cout << -l2+l0+cp2+cp3 << endl;
  cout << dot(l1f,l1f) << endl;
  cout << dot(l2f,l2f) << endl;
  cout << dot(l0f,l0f) << endl;

  cout << "A(-l2_tb(-),2(+),3(+),l0_t(-)) = " << tree.Leval(-L2,2,3,L0) << endl;
  cout << "A(-l2_tb(-),2(+),3(+),l0_t(-)) = " << 
   i_*mt*xspA(-l2f,l0f)*xspB(cp2,cp3)/(xspA(cp2,cp3)*2.*dot(-l2,cp2)) << endl;
  cout << endl;
  
  int hh2[]={1,+1,+1,1};
  int ff2[]={17,0,0,17};
  PTLS<double> process2(4);
  process2.set(ff2,hh2);
  process2.getPSpoint(7.0,rnd.next());
  cout << process2 << endl;
  AMP<double> treeS(N);
  cout << treeS.eval(process2) << endl;

  MOM<double> q0 = process2[0].mom;
  MOM<double> q1 = process2[1].mom;
  MOM<double> q2 = process2[2].mom;
  MOM<double> q3 = process2[3].mom;
  double mt2 = process2.getMass(17);
  MOM<double> q0f = q0 - mt2*mt2/(2.*dot(q0,e))*e;
  MOM<double> q3f = q3 - mt2*mt2/(2.*dot(q3,e))*e;

  cout << "A(0s(0),1(+),2(+),3s(0))" << 
   -i_*mt2*mt2*spB(q1,q2)/(spA(q1,q2)*2.*dot(q0,q1)) << endl;
  cout << endl;

  int hh3[]={-1,+1,+1,-1};

  int ff3[]={6,0,0,-6};
  PTLS<double> process3(4);
  process3.set(ff3,hh3);
  process3.getPSpoint(7.0,rnd.next());
  cout << process3 << endl;
  AMP<double> treeQ(N);
  cout << treeQ.eval(process3) << endl;;
  treeQ.Initialize(process3);

  q0 = process3[0].mom;
  q1 = process3[1].mom;
  q2 = process3[2].mom;
  q3 = process3[3].mom;
  mt2 = process3.getMass(6);
  q0f = q0 - mt2*mt2/(2.*dot(q0,e))*e;
  q3f = q3 - mt2*mt2/(2.*dot(q3,e))*e;

  cout << "A(0t(-),1(+),2(+),3tb(-))" << 
   i_*mt2*spA(q0f,q3f)*spB(q1,q2)/(spA(q1,q2)*spAB(q1,q0,q1)) << endl;
  
  cout << "A(0t(+),1(+),2(-),3tb(-))" <<
   i_*spAB(q2,q0,q1)*( spA(e,q3f)*spAB(q2,q0,q1)-spB(q1,q2)*spA(q2,e)*spA(q2,q3f) )/(S(q1+q2)*spAB(q1,q0,q1)*spA(e,q0f))
   << endl;

//  MOM<complex<double> > l2 = -q0;
//  MOM<complex<double> > cq1 = q1;
//  MOM<complex<double> > cq2 = q2;
//  MOM<complex<double> > l0 = q3;
//  MOM<complex<double> > l2f = l2 - mt2*mt2/(2.*dot(l2,ce))*ce;
//  MOM<complex<double> > l0f = l0 - mt2*mt2/(2.*dot(l0,ce))*ce;

//  PTL<complex<double> > L2(6,-1,-l2,mt2);
//  PTL<complex<double> > L0(-6,-1,l0,mt2);

//  cout << -l2+l0+cq1+cq2 << endl;
//  cout << dot(l2f,l2f) << endl;
//  cout << dot(l0f,l0f) << endl;

//  cout << "A(-l2_tb(-),2(+),3(+),l0_t(-)) = " << endl << treeQ.Leval(L2,1,2,L0) << endl;
//  cout << "A(-l2_tb(-),2(+),3(+),l0_t(-)) = " << 
//   i_*mt2*xspA(-l2f,l0f)*xspB(cq1,cq2)/(xspA(cq1,cq2)*2.*dot(-l2,cq1)) << endl;
//  cout << endl;

  return 0;
}
