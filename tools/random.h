/*
* tools/random.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef TOOLS_RANDOM_H
#define TOOLS_RANDOM_H

#include <inttypes.h>
#include <iostream>

using std::istream;
using std::ostream;

class iodouble
{
  private:
    typedef union { double d64; uint64_t i64; } DI64;
    typedef union { uint64_t i64; double d64; } ID64;

  public:
    iodouble(double val_=double()) : val(val_) { }
    operator double() { return val; }

  private:
    double val;

    friend istream& operator>> (istream &stream, iodouble& ioval);
    friend ostream& operator<< (ostream &stream, iodouble ioval);
};

class RandNums
{
  typedef double* CVEC;
  public:
    RandNums(const int N_);
    ~RandNums();
    operator CVEC() { return dblvec; }

    static void init(int seed_);
    static int curseed() { return seed; }
    static unsigned long int seqnum() { return seqn; }

    void resize(int seed_);
    const double* next();
    const double* current();

    int N;

  private:
    int len;
    double *dblvec;

    static unsigned long int seqn;
    static int seed;
    static const int maxlen = 50; // 50 allows to generate up to 2->18
    static float floatbuf[maxlen];

    friend std::ostream& operator<< (std::ostream &stream, RandNums &rn);
    friend std::istream& operator>> (std::istream &stream, RandNums &rn);
};

#endif /* TOOLS_RANDOM_H */
