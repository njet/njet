/*
* tools/PSgenerator.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cmath>

#include "PSgenerator.h"
#include "../ngluon2/utility.h"

template <typename T>
static T lambda(const T& a, const T& b, const T& c)
{
  return a*a + b*b + c*c - 2.*a*b - 2.*b*c - 2.*a*c;
}

template <typename T>
static void boost(const T m, const T p[4], T mom[4])
{
  //  m = (m == 0.) ? p[0]*p[0]-p[1]*p[1]-p[2]*p[2]*p[3]*p[3] : m ;
  const T gamma = p[0] / m;
  const T invp0 = 1. / p[0];

  const T beta1 = p[1] * invp0;
  const T beta2 = p[2] * invp0;
  const T beta3 = p[3] * invp0;

  const T help = beta1 * mom[1] + beta2 * mom[2] + beta3 * mom[3];
  const T fac = (gamma / (T(1.) + gamma) * help + mom[0]) * gamma;

  mom[0] = (mom[0] + help) * gamma ;
  mom[1] += fac * beta1;
  mom[2] += fac * beta2;
  mom[3] += fac * beta3;

}

template <typename T>
int eventn(const unsigned int n, const T sqrts, const double* x,
           const T masses[], T kout[][4], T& jacobi)
{
  static const T PI = constant_traits<T>::pi();
  static const T TwoPi = 2.*PI;

  /*
   * Routine to generate a n-parton event with center-of-mass
   * energy squared given by s and the parton masses set through
   * the 4-momenta in kout[].
   * The momentum configuration is returned in kout[], the jacobian for
   * the transformation is returned through jacobi.
   * x should contain 3*n-4 random numbers
   * Algorithm is described in Byckling & Kajantie
   */

  int err = 0;

  typedef struct {
    T m;
    T MM;
    T MMQ;
    T mu;
    T P;
  } pinfo_type;
  pinfo_type* pinfo = new pinfo_type[n];

#define XRANDOM  *(x++)

  T msum = 0;

  for (unsigned int i = 0; i < n; i++) {
    pinfo[i].m = masses[i];
    msum += pinfo[i].m;
    pinfo[i].mu = msum;
  };

  /*
   * Set the invariant masses MMQ(i) = (p_0+...+p_i)^2
   */
  pinfo[n-1].MM  = sqrts;
  pinfo[n-1].MMQ = sqrts * sqrts;
  pinfo[0].MM    = pinfo[0].m;
  pinfo[0].MMQ   = pinfo[0].MM * pinfo[0].MM;

  for (unsigned int i = n-2; i > 0; i--) {
    pinfo[i].MM = pinfo[i].mu + XRANDOM * (pinfo[i+1].MM - pinfo[i+1].mu);
    pinfo[i].MMQ = pinfo[i].MM * pinfo[i].MM;
  }

  /*
   * Calculate the three momenta and the jacobian
   */
  pinfo[1].P = pinfo[1].m == 0. ? (pinfo[1].MM - pinfo[0].MM) *
               (pinfo[1].MM + pinfo[0].MM) / (2.*pinfo[1].MM) :
               sqrt(lambda(pinfo[1].MMQ, pinfo[0].MMQ, pinfo[1].m * pinfo[1].m))
               / (2.*pinfo[1].MM);
  jacobi = pow(TwoPi, static_cast<int>(n) - 1) / (2.*sqrts) * pinfo[1].P;


  for (unsigned int i = 2; i < n; i++) {
    pinfo[i].P =
      (pinfo[i].m == 0. ?
       (pinfo[i].MM - pinfo[i-1].MM) * (pinfo[i].MM + pinfo[i-1].MM)
       / (2.*pinfo[i].MM) :
       sqrt(lambda(pinfo[i].MMQ, pinfo[i-1].MMQ, pinfo[i].m * pinfo[i].m))
       / (2.*pinfo[i].MM));
    jacobi *= pinfo[i].P * (pinfo[i].MM - pinfo[i].mu);
  }

  /*
   * Finally reconstruct the event in the overall CMS:
   *
   * Calculate first the 4-momenta pset(rho,i) in the restframe of
   * p_1+...+p_i:
   */
  for (unsigned int i = 1; i < n; i++) {
#ifdef USE_SINCOS
    T sinphi_i, cosphi_i;
    sincos(TwoPi * XRANDOM, &sinphi_i, &cosphi_i);
#else
    /*
     *  Note: For phi we have to calculate the cosine explicitly!!!!
     *  dsqrt(1-sin^2) would only give cos() > 0
     */
    T phi_i = TwoPi * XRANDOM;
    T sinphi_i = sin(phi_i);
    T cosphi_i = cos(phi_i);
#endif
    T tmp = XRANDOM;
    T costheta_i = 2.*tmp - 1.;
    T sintheta_i = 2.*sqrt(tmp * (1. - tmp));
    //T sintheta_i = sqrt(1. - costheta_i*costheta_i);

    kout[i][1] = pinfo[i].P * cosphi_i * sintheta_i;
    kout[i][2] = pinfo[i].P * sinphi_i * sintheta_i;
    kout[i][3] = pinfo[i].P * costheta_i;
    kout[i][0] = pinfo[i].m == 0. ? pinfo[i].P :
                 sqrt(pinfo[i].m * pinfo[i].m + pinfo[i].P * pinfo[i].P);

  }

  kout[0][0] = pinfo[1].MM - kout[1][0];
  kout[0][1] = -kout[1][1];
  kout[0][2] = -kout[1][2];
  kout[0][3] = -kout[1][3];

  T bhelp[4];

  for (unsigned int i = 2; i < n; i++) {
    bhelp[0] = pinfo[i].MM - kout[i][0];
    bhelp[1] = -kout[i][1];
    bhelp[2] = -kout[i][2];
    bhelp[3] = -kout[i][3];

    for (unsigned int j = 0; j < i; j++) {
      boost(pinfo[i-1].MM, bhelp, &kout[j][0]);
    }
  }

  delete[] pinfo;
  return err;
#undef XRANDOM
}

template
int eventn<double>(const unsigned int n, const double sqrts, const double* x,
                   const double masses[], double kout[][4], double& jacobi);
#ifdef USE_DD
  #include <qd/qd_real.h>
  template
  int eventn<dd_real>(const unsigned int n, const dd_real sqrts, const double* x,
                      const dd_real masses[], dd_real kout[][4], dd_real& jacobi);
#endif
#ifdef USE_QD
  #include <qd/qd_real.h>
  template
  int eventn<qd_real>(const unsigned int n, const qd_real sqrts, const double* x,
                      const qd_real masses[], qd_real kout[][4], qd_real& jacobi);
#endif
