//#include <cstdlib>
//#include <fstream>
//#include <sstream>
//#include <time.h>
#include <getopt.h>
#include <cstdio>

int rseed = 1;
int maxpts = 1;
int skip = 0;
int debug = 0;
int version = 2;
int fix = 0;
int dd_prec = 0;
int qloop = 0;
float theta = 0.;
float sqrtS = 7000.;
float pTmin = 0.008;
float etaMax = 2.8;
float deltaR = 0.4;

#include "../src-nparton/NParton-multiprec.h"
static NJet_dd::Initialize global_init_dd;

#include "../ngluon2/NParton2.h"
#include "PhaseSpace.h"
#include "random.h"
#include "../ngluon2/Initialize.h"
#include "PhaseSpace.h"
#include "../ngluon2/Model.h"

Initialize global_init;

using namespace std;

int parseOptions(int argc, char **argv)
{
  static struct option long_options[] =
    {
      {"points", required_argument, 0, 'n'},
      {"seed", required_argument, 0, 's'},
      {"skip", required_argument, 0, 'k'},
      {"version", required_argument, 0, 'v'},
      {"angle", required_argument, 0, 't'},
      {"energy", required_argument, 0, 'e'},
      {"pT", required_argument, 0, 'p'},
      {"eta", required_argument, 0, 'r'},
      {"dR", required_argument, 0, 'd'},
      {"dd", no_argument, &dd_prec, 1},
      {"qloop", no_argument, &qloop, 1},
      {"fix", no_argument, &fix, 1},
      {"debug", no_argument, &debug, 1},
      {0, 0, 0, 0}
    };

  while(1){
    /*
     * getopt_long stores the option index here.
     */
    int option_index = 0;
    int c = getopt_long_only(argc, argv, "n:s:k:v:t:e:p:r:d:",
                         long_options, &option_index);

    /* Detect the end of the options. */
    if ( c == -1 ){
      if (optind < argc){
        cout << "Unrecognized additonal input in commandline: ";
        cout << argv[optind] << endl;
        cout << "Abort programm" << endl;
        return 1;
      }
      break;
    }
    switch (c){
    case 'n':
      sscanf(optarg, "%d", &maxpts);
      break;
    case 's':
      sscanf(optarg, "%d", &rseed);
      break;
    case 'k':
      sscanf(optarg, "%d", &skip);
      break;
    case 'v':
      sscanf(optarg, "%d", &version);
      break;
    case 't':
      sscanf(optarg, "%f", &theta);
      break;
    case 'e':
      sscanf(optarg, "%f", &sqrtS);
      break;
    case 'p':
      sscanf(optarg, "%f", &pTmin);
      break;
    case 'r':
      sscanf(optarg, "%f", &etaMax);
      break;
    case 'd':
      sscanf(optarg, "%f", &deltaR);
      break;
    }
  }
  return 0;
}

void TestPrimitive(const int N, int* hh, int* ff, int npts=1, int seed=1)
{
  if (debug) {
    cout.precision(16);
  } else {
    cout.precision(3);
  }

  PhaseSpace<double> ps(StandardModel::NGluon1compat(N, ff), seed, sqrtS, pTmin, etaMax, deltaR);
  PhaseSpace<dd_real> ps_dd(StandardModel::NGluon1compat(N, ff), seed, sqrtS, pTmin, etaMax, deltaR);
  ps.skip(skip);

  NParton2<double> loop;
  NParton2<dd_real> loop_dd;

  loop.setProcess(StandardModel::NGluon1compat(N, ff));
  loop_dd.setProcess(StandardModel::NGluon1compat(N, ff));

  loop.setMuR2(0.25*sqrtS*sqrtS);
  loop_dd.setMuR2(dd_real(0.25*sqrtS*sqrtS));

  NJet_sd::NParton prmtv(N);
  prmtv.setMuR2(0.25*sqrtS*sqrtS);
  std::vector<double> vmoms((N)*4);
  double (*moms)[][4] = reinterpret_cast<double(*)[][4]>(&vmoms[0]);

  NJet_dd::NParton prmtv_dd(N);
  prmtv_dd.setMuR2(dd_real(0.25*sqrtS*sqrtS));
  std::vector<dd_real> vmoms_dd((N)*4);
  dd_real (*moms_dd)[][4] = reinterpret_cast<dd_real(*)[][4]>(&vmoms_dd[0]);

  for (int pt=0; pt < npts; pt++) {
    ps.getPSpoint();

    EpsTriplet<double> ng2res;
    EpsTriplet<double> ng2err;
    EpsTriplet<double> ng2cc, ng2cc1, ng2cc2, ng2cc3;
    EpsTriplet<double> ng2ccerr, ng2ccerr1, ng2ccerr2, ng2ccerr3;
    complex<double>    ng2tree, ng2rat, ng2rat1, ng2rat2, ng2rat3;
    complex<double>    ng2raterr, ng2raterr1, ng2raterr2, ng2raterr3;

    EpsTriplet<double> ng1res;
    EpsTriplet<double> ng1err;
    EpsTriplet<double> ng1cc, ng1cc1, ng1cc2, ng1cc3;
    EpsTriplet<double> ng1ccerr, ng1ccerr1, ng1ccerr2, ng1ccerr3;
    complex<double>    ng1tree, ng1rat, ng1rat1, ng1rat2, ng1rat3;
    complex<double>    ng1raterr, ng1raterr1, ng1raterr2, ng1raterr3;

    EpsTriplet<dd_real> ng2res_dd;
    EpsTriplet<dd_real> ng2err_dd;
    EpsTriplet<dd_real> ng2cc_dd, ng2cc1_dd, ng2cc2_dd, ng2cc3_dd;
    EpsTriplet<dd_real> ng2ccerr_dd, ng2ccerr1_dd, ng2ccerr2_dd, ng2ccerr3_dd;
    complex<dd_real>    ng2tree_dd, ng2rat_dd, ng2rat1_dd, ng2rat2_dd, ng2rat3_dd;
    complex<dd_real>    ng2raterr_dd, ng2raterr1_dd, ng2raterr2_dd, ng2raterr3_dd;

    EpsTriplet<dd_real> ng1res_dd;
    EpsTriplet<dd_real> ng1err_dd;
    EpsTriplet<dd_real> ng1cc_dd, ng1cc1_dd, ng1cc2_dd, ng1cc3_dd;
    EpsTriplet<dd_real> ng1ccerr_dd, ng1ccerr1_dd, ng1ccerr2_dd, ng1ccerr3_dd;
    complex<dd_real>    ng1tree_dd, ng1rat_dd, ng1rat1_dd, ng1rat2_dd, ng1rat3_dd;
    complex<dd_real>    ng1raterr_dd, ng1raterr1_dd, ng1raterr2_dd, ng1raterr3_dd;

    dd_real ep2, ep1, ep0;
    dd_real cc0cc, cc0cc1, cc0cc2, cc0cc3;
    dd_real rr0rr, rr0rr1, rr0rr2, rr0rr3;
    dd_real cc0ep, cc0ep1, cc0ep2, cc0ep3;
    dd_real rr0ep, rr0ep1, rr0ep2, rr0ep3;
    if (version > 1) {
      if (!dd_prec) {
        loop.setMomenta(ps.Mom());
        loop.setHelicity(hh);
        loop.eval(qloop == 0 ? 0 : 1);

        ng2tree = loop.gettree();
        ng2res = loop.getres();
        ng2err = loop.geterr();

        ng2cc = loop.getcc();
        ng2cc1 = loop.getcc(1);
        ng2cc2 = loop.getcc(2);
        ng2cc3 = loop.getcc(3);
        ng2ccerr = loop.getccerr();
        ng2ccerr1 = loop.getccerr(1);
        ng2ccerr2 = loop.getccerr(2);
        ng2ccerr3 = loop.getccerr(3);

        ng2rat = loop.getrat();
        ng2rat1 = loop.getrat(1);
        ng2rat2 = loop.getrat(2);
        ng2rat3 = loop.getrat(3);
        ng2raterr = loop.getraterr();
        ng2raterr1 = loop.getraterr(1);
        ng2raterr2 = loop.getraterr(2);
        ng2raterr3 = loop.getraterr(3);

        ep2 = abs(ng2err.get2()/ng2res.get2());
        ep1 = abs(ng2err.get1()/ng2res.get1());
        ep0 = abs(ng2err.get0()/ng2res.get0());

        cc0cc = abs(ng2ccerr.get0()/ng2cc.get0());
        cc0cc1 = abs(ng2ccerr1.get0()/ng2cc1.get0());
        cc0cc2 = abs(ng2ccerr2.get0()/ng2cc2.get0());
        cc0cc3 = abs(ng2ccerr3.get0()/ng2cc3.get0());
        rr0rr = abs(ng2raterr/ng2rat);
        rr0rr1 = abs(ng2raterr1/ng2rat1);
        rr0rr2 = abs(ng2raterr2/ng2rat2);
        rr0rr3 = abs(ng2raterr3/ng2rat3);

        cc0ep = abs(ng2ccerr.get0()/ng2res.get0());
        cc0ep1 = abs(ng2ccerr1.get0()/ng2res.get0());
        cc0ep2 = abs(ng2ccerr2.get0()/ng2res.get0());
        cc0ep3 = abs(ng2ccerr3.get0()/ng2res.get0());
        rr0ep = abs(ng2raterr/ng2res.get0());
        rr0ep1 = abs(ng2raterr1/ng2res.get0());
        rr0ep2 = abs(ng2raterr2/ng2res.get0());
        rr0ep3 = abs(ng2raterr3/ng2res.get0());

        if (debug) {
          ps.showPSpoint();
          cout << "NG2 A1[m]    = " << ng2res << endl;
          cout << "NG2 (err)    = " << ng2err << endl;
          cout << "NG2 A1[m]/A0 = " << ng2res/ng2tree << endl;
        }
      }

      if ((fix && ep0 > 1e-5) || dd_prec) {
        if (fix) {
          cout << "# " << pt << " # switching to dd prec. : " << ep0 << " ###" << endl;
        }
        loop_dd.setMomenta(ps_dd.getPSpoint(ps.current()));
        loop_dd.setHelicity(hh);
        loop_dd.eval(qloop == 0 ? 0 : 1);

        ng2tree_dd = loop_dd.gettree();
        ng2res_dd = loop_dd.getres();
        ng2err_dd = loop_dd.geterr();

        ng2cc_dd = loop_dd.getcc();
        ng2cc1_dd = loop_dd.getcc(1);
        ng2cc2_dd = loop_dd.getcc(2);
        ng2cc3_dd = loop_dd.getcc(3);
        ng2ccerr_dd = loop_dd.getccerr();
        ng2ccerr1_dd = loop_dd.getccerr(1);
        ng2ccerr2_dd = loop_dd.getccerr(2);
        ng2ccerr3_dd = loop_dd.getccerr(3);

        ng2rat_dd = loop_dd.getrat();
        ng2rat1_dd = loop_dd.getrat(1);
        ng2rat2_dd = loop_dd.getrat(2);
        ng2rat3_dd = loop_dd.getrat(3);
        ng2raterr_dd = loop_dd.getraterr();
        ng2raterr1_dd = loop_dd.getraterr(1);
        ng2raterr2_dd = loop_dd.getraterr(2);
        ng2raterr3_dd = loop_dd.getraterr(3);

        ep2 = abs(ng2err_dd.get2()/ng2res_dd.get2());
        ep1 = abs(ng2err_dd.get1()/ng2res_dd.get1());
        ep0 = abs(ng2err_dd.get0()/ng2res_dd.get0());

        cc0cc = abs(ng2ccerr_dd.get0()/ng2cc_dd.get0());
        cc0cc1 = abs(ng2ccerr1_dd.get0()/ng2cc1_dd.get0());
        cc0cc2 = abs(ng2ccerr2_dd.get0()/ng2cc2_dd.get0());
        cc0cc3 = abs(ng2ccerr3_dd.get0()/ng2cc3_dd.get0());
        rr0rr = abs(ng2raterr_dd/ng2rat_dd);
        rr0rr1 = abs(ng2raterr1_dd/ng2rat1_dd);
        rr0rr2 = abs(ng2raterr2_dd/ng2rat2_dd);
        rr0rr3 = abs(ng2raterr3_dd/ng2rat3_dd);

        cc0ep = abs(ng2ccerr_dd.get0()/ng2res_dd.get0());
        cc0ep1 = abs(ng2ccerr1_dd.get0()/ng2res_dd.get0());
        cc0ep2 = abs(ng2ccerr2_dd.get0()/ng2res_dd.get0());
        cc0ep3 = abs(ng2ccerr3_dd.get0()/ng2res_dd.get0());
        rr0ep = abs(ng2raterr_dd/ng2res_dd.get0());
        rr0ep1 = abs(ng2raterr1_dd/ng2res_dd.get0());
        rr0ep2 = abs(ng2raterr2_dd/ng2res_dd.get0());
        rr0ep3 = abs(ng2raterr3_dd/ng2res_dd.get0());

        if (debug) {
          cout << "NG2 A1[m](dd) = " << ng2res_dd << endl;
          cout << "NG2 (err)     = " << ng2err_dd << endl;
          cout << "NG2 A1[m]/A0 = " << ng2res_dd/ng2tree_dd << endl;
        }
      }

      // NGluon2 output
      cout << ps.curseed() << " " << ps.seqnum() << " "
           << ep2 << " " << ep1 << " " << ep0 << " "
           << cc0cc << " " << rr0rr << " "
           << cc0ep << " " << rr0ep << " "
           << cc0cc1 << " " << cc0cc2 << " "<< cc0cc3 << " "
           << rr0rr1 << " " << rr0rr2 << " "<< rr0rr3 << " "
           << cc0ep1 << " " << cc0ep2 << " "<< cc0ep3 << " "
           << rr0ep1 << " " << rr0ep2 << " "<< rr0ep3 << " "
           << endl;
    }

    if ( version == 1 || version > 2 ) {
      if (!dd_prec) {
        ps.FillMomArray(*moms);
        prmtv.setAmp(*moms,ff);
        prmtv.eval(hh, qloop == 0 ? NJet_sd::NParton::mixed : NJet_sd::NParton::qloop);

        ng1res = EpsTriplet<double>(prmtv.getAmp(NJet_sd::NParton::eps0),
                                    prmtv.getAmp(NJet_sd::NParton::eps1),
                                    prmtv.getAmp(NJet_sd::NParton::eps2));
        ng1err = EpsTriplet<double>(prmtv.getAbsErr(NJet_sd::NParton::eps0),
                                    prmtv.getAbsErr(NJet_sd::NParton::eps1),
                                    prmtv.getAbsErr(NJet_sd::NParton::eps2));

        ng1tree = prmtv.getAmp(NJet_sd::NParton::tree);
        ep2 = abs(ng1err.get2()/ng1res.get2());
        ep1 = abs(ng1err.get1()/ng1res.get1());
        ep0 = abs(ng1err.get0()/ng1res.get0());

        if (debug) {
          cout << "# Process : " << prmtv.ProcessStr() << endl;
          cout << "NG1 A1[m]    = " << ng1res << endl;
          cout << "NG1 (err)    = " << ng1err << endl;
        }

        // in case both versions are used check thery're equal
        if ( version > 2 ) {
          double diff = abs(2.*(ng1res.get0()/ng1tree - ng2res.get0()/ng2tree)/(ng1res.get0()/ng1tree + ng2res.get0()/ng2tree));
          double rel1 = abs(ng1err.get0()/ng1res.get0());
          double rel2 = abs(ng2err.get0()/ng2res.get0());
          if (diff > 1e1*max(rel1, rel2)) {
            cout << "# fail " << pt << " diff = " << diff << " (" << rel1 << " != " << rel2 << ") " << diff/max(rel1, rel2) << endl;
          }
        }
      } else {
        ps_dd.getPSpoint(ps.current());
        ps_dd.FillMomArray(*moms_dd);
        prmtv_dd.setAmp(*moms_dd, ff);
        prmtv_dd.eval(hh, qloop == 0 ? NJet_sd::NParton::mixed : NJet_sd::NParton::qloop);

        ng1res_dd = EpsTriplet<dd_real>(prmtv_dd.getAmp(NJet_dd::NParton::eps0),
                                        prmtv_dd.getAmp(NJet_dd::NParton::eps1),
                                        prmtv_dd.getAmp(NJet_dd::NParton::eps2));
        ng1err_dd = EpsTriplet<dd_real>(prmtv_dd.getAbsErr(NJet_dd::NParton::eps0),
                                        prmtv_dd.getAbsErr(NJet_dd::NParton::eps1),
                                        prmtv_dd.getAbsErr(NJet_dd::NParton::eps2));

        ng1tree_dd = prmtv_dd.getAmp(NJet_dd::NParton::tree);
        ep2 = abs(ng1err_dd.get2()/ng1res_dd.get2());
        ep1 = abs(ng1err_dd.get1()/ng1res_dd.get1());
        ep0 = abs(ng1err_dd.get0()/ng1res_dd.get0());

        if (debug) {
          cout << "# Process : " << prmtv_dd.ProcessStr() << endl;
          cout << "NG1 A1[m](dd) = " << ng1res_dd << endl;
          cout << "NG1 (err)     = " << ng1err_dd << endl;
        }
        // in case both versions are used check thery're equal
        if ( version > 2 ) {
          dd_real diff = abs(2.*(ng1res_dd.get0()/ng1tree_dd - ng2res_dd.get0()/ng2tree_dd)/(ng1res_dd.get0()/ng1tree_dd + ng2res_dd.get0()/ng2tree_dd));
          dd_real rel1 = abs(ng1err_dd.get0()/ng1res_dd.get0());
          dd_real rel2 = abs(ng2err_dd.get0()/ng2res_dd.get0());
          if (diff > 1e1*max(rel1, rel2)) {
            cout << "# fail " << pt << " diff = " << diff << " (" << rel1 << " != " << rel2 << ") " << diff/max(rel1, rel2) << endl;
          }
        }
      }
      // NGluon 1 output
      cout << ps.curseed() << " " << ps.seqnum() << " "
           << ep2 << "  " << ep1 << "  " << ep0
           << endl;
    }

  }

  return;
}

int main(int argc, char **argv)
{
  cout.setf(ios_base::scientific, ios_base::floatfield);
//  cout.setf(ios_base::showpos);
  cout.precision(16);
  if (0 != parseOptions(argc, argv)) return 1;
  cout << "### random seed = " << rseed << " ###" << endl;
  if ( version < 1 ) {
    cout << "you should use at least one version of NGluon..." << endl;
    return 1;
  }

  int hh6[][6]={
    {-1,+1,-1,+1,-1,+1},
    {-1,+1,-1,+1,-1,+1},
    {-1,+1,-1,+1,-1,+1},
    {-1,+1,-1,+1,-1,+1}
  };
  int ff6[][6]={
    {0,0,0,0,0,0},
    {-1,0,0,1,0,0},
    {-1,-2,2,1,0,0},
    {1,0,0,-1,0,0},
  };

  for (int k=0; k<4; k++) {
    TestPrimitive(6, hh6[k], ff6[k], maxpts, rseed);
  }

  return 0;
}
