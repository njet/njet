/*
* tools/PSgenerator.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef TOOLS_PSGEN_H
#define TOOLS_PSGEN_H

// Peter Uwer's phasespace point generator
template <typename T>
static T lambda(const T& a, const T& b, const T& c);

template <typename T>
static void boost(const T m, const T p[4], T mom[4]);

template <typename T>
int eventn(const unsigned int n, const T sqrts, const double* x,
           const T masses[], T kout[][4], T& jacobi);

#endif /* TOOLS_PSGEN_H */
