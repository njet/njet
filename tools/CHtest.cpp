#include <iostream>
#include <getopt.h>
#include <cstdio>

#include "../ngluon2/Model.h"
#include "Timer.h"

#include "../chsums/2q0gV.h"
#include "../chsums/2q1gV.h"

#include "../chsums/0q4g.h"
#include "../analytic/0q4g-analytic.h"

#include "../chsums/2q2g.h"
#include "../chsums/2q2gV.h"

#include "../chsums/4q0g.h"
#include "../chsums/4q0gV.h"

#include "../chsums/0q5g.h"
#include "../analytic/0q5g-analytic.h"

#include "../chsums/2q3g.h"
#include "../chsums/2q3gV.h"

#include "../chsums/4q1g.h"
#include "../chsums/4q1gV.h"

#include "../chsums/0q6g.h"
#include "../analytic/0q6g-analytic.h"

#include "../chsums/2q4g.h"
#include "../chsums/2q4gV.h"
#include "../chsums/2q4g-ds.h"

#include "../chsums/4q2g.h"
#include "../chsums/4q2gV.h"

#include "../chsums/6q0g.h"
#include "../chsums/6q0gV.h"

#ifdef NJET_ENABLE_5
#include "../chsums/0q7g.h"

#include "../chsums/2q5g.h"
#include "../chsums/2q5gV.h"
#include "../chsums/2q5g-ds.h"

#include "../chsums/4q3g.h"
#include "../chsums/4q3gV.h"

#include "../chsums/6q1g.h"
#include "../chsums/6q1gV.h"
#endif

using std::cout;
using std::endl;

static Timer tm;

int do_help = 0;
int do_analytic = 0;
int do_pp2j = 0;
int do_pp3j = 0;
int do_pp4j = 0;
int do_pp5j = 0;
int do_ppV0j = 0;
int do_ppV1j = 0;
int do_ppV2j = 0;
int do_ppV3j = 0;
int do_ppV4j = 0;
int do_ppV5j = 0;
int do_BH_V3j = 0;

static struct option long_options[] = {
  {"help", no_argument, &do_help, 1},
  {"analytic", no_argument, &do_analytic, 1},
  {"pp2j", no_argument, &do_pp2j, 1},
  {"pp3j", no_argument, &do_pp3j, 1},
  {"pp4j", no_argument, &do_pp4j, 1},
  {"pp5j", no_argument, &do_pp5j, 1},
  {"ppV0j", no_argument, &do_ppV0j, 1},
  {"ppV1j", no_argument, &do_ppV1j, 1},
  {"ppV2j", no_argument, &do_ppV2j, 1},
  {"ppV3j", no_argument, &do_ppV3j, 1},
  {"ppV4j", no_argument, &do_ppV4j, 1},
  {"ppV5j", no_argument, &do_ppV5j, 1},
  {"BH_V3j", no_argument, &do_BH_V3j, 1},
  {0, 0, 0, 0}
};

int parseOptions(int argc, char **argv)
{
  while (1) {
    // getopt_long stores the option index here.
    int option_index = 0;
    int c = getopt_long_only(argc, argv, "n:", long_options, &option_index);

    // Detect the end of the options.
    if (c == -1) {
      if (optind < argc) {
        cout << "Unrecognized additonal input in commandline: ";
        cout << argv[optind] << endl;
        cout << "Abort programm" << endl;
        return 1;
      }
      break;
    }
  }
  return 0;
}

void help()
{
  cout << endl;
  cout << "List of valid options:" << endl;
  for (int i=0; long_options[i].name != 0; i++) {
    cout << "    --" << long_options[i].name << endl;
  }
  cout << endl;
}

// natural symmetric index: (i,j) in [1,2,...], "nis" in [1,2,...]
// non-diagonal symmetric index: i != j, (i,j) in [0,1,...], "nis" in [0,1,...]
inline int nis(int i, int j) {
  return ( i<=j ? i+j*(j-1)/2 : j+i*(i-1)/2 );
}

// renorm for uFDH->rCDR
template <typename T>
EpsTriplet<T> getrenorm(int ng, int nq, T Nc, T Nf, T born)
{
  T beta0 = (T(11.)*Nc-2.*Nf)/T(3.);
  T CF = 0.5*(Nc-T(1.)/Nc);
  EpsTriplet<T> res;
  res.set0((T(nq)-2.)*Nc/T(6.) - T(nq)*CF*0.5);
  res.set1(-(T(nq+ng)-2.)*0.5*beta0);
  return 2.*res*born;
}

// poles for rCDR
template <typename T>
EpsTriplet<T> getpoles(int ng, int nq, T Nc, T Nf, const MOM<T>* p, T MuR2, T born, const T* born_cc, int rscheme = 0)
{
  int NN = nq+ng;
  T beta0 = (T(11.)*Nc-2.*Nf)/T(3.);
  T CF = 0.5*(Nc-T(1.)/Nc);
  T CA = Nc;

  T pole2 = -(T(nq)*CF + T(ng)*CA)*born;
  T pole1 = T();

  for (int i=0; i<NN-1; i++) {
    for (int j=i+1; j<NN; j++) {
      pole1 += 2.*born_cc[nis(i,j)]*log(abs(S(p[i]+p[j])/MuR2));
    }
  }
  pole1 -= (beta0-nq*(0.5*beta0-T(1.5)*CF))*born;
  // renorm for rCDR
  if (rscheme == 0) {
    pole1 -= (T(NN)-2.)*0.5*beta0*born;
  }

  EpsTriplet<T> res(T(), pole1, pole2);
  res *= 2.;
  return res;
}

void print_stat(int n, double* x)
{
  double sum = 0.;
  double sum2 = 0.;
  for (int i=0; i<n; i++) {
    sum += x[i];
    sum2 += x[i]*x[i];
  }
  const double avg = sum/n;
  const double sig2 = (sum2 - n*avg*avg)/(n-1);  // (sum2 - sum*sum/n)/(n-1)
  const double sig = sqrt(sig2);
  cout << avg << "(" << sig/avg << ")";
}

//{{{ ppV0j
void ppV0j()
{
  double mW = 80.419002445756163;
  double gW = 2.0476;
  double mZ = 91.188;
  double gZ = 2.441404;
  double se2 = 0.22224648578577766; // sin(theta_EW)^2
  StandardModel::setWmass(mW, gW);
  StandardModel::setZmass(mZ, gZ);
  StandardModel::setSinThetaWSq(se2);

  double Nc = 3.;
  double as = 0.118;
  double ae = 7.54677111397888348E-003;
  double gs = sqrt(4.*M_PI*as);
  double ee = sqrt(4.*M_PI*ae);
  double se = sqrt(se2);

  double sqrtS = 1e3;
  double MuR2 = sqrtS*sqrtS*0.25;
  double Nf = 4.;
  int ng = 0;
  int nq = 2;

  Flavour<double> Wp = StandardModel::Wp(StandardModel::u(), StandardModel::ubar(), 1., mW, gW);
  Flavour<double> Zu = StandardModel::Zu(StandardModel::u(), StandardModel::ubar(), mZ, gZ);
  Flavour<double> Zd = StandardModel::Zd(StandardModel::u(), StandardModel::ubar(), mZ, gZ);

  const int namps = 4;
  const int legs = 4;
  std::string process[namps] = {
    "# tests for Drell-Yan u d~ > W+ > ve e+",
    "# tests for Drell-Yan d u~ > W- > ve~ e-",
    "# tests for Drell-Yan u u~ > Z/g* > ve e+",
    "# tests for Drell-Yan d d~ > Z/g* > ve e+"
  };

  NJetAmp<double>* amp[namps] = {
    new Amp2q0gV<double>(Wp, 1.),
    new Amp2q0gV<double>(Wp, 1.),
    new Amp2q0gZ<double>(Zu, 1.),
    new Amp2q0gZ<double>(Zd, 1.)
  };

  double prefactor0[namps] = {
    pow(ee/se,4.)/(Nc*Nc*4.),
    pow(ee/se,4.)/(Nc*Nc*4.),
    pow(ee,4.)/(Nc*Nc*4.),
    pow(ee,4.)/(Nc*Nc*4.)
  };

  MOM<double> Momenta[legs] = {
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00, -0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00,  0.5000000000000000E+03),
      MOM<double>( 0.4999999999999998E+03,  0.1109242844438328E+03,  0.4448307894881214E+03, -0.1995529299308788E+03),
      MOM<double>( 0.5000000000000000E+03, -0.1109242844438328E+03, -0.4448307894881214E+03,  0.1995529299308787E+03)
  };

  const double ML50[] = {
    0.138759448068366E-02,
    0.752259666545266E-02,
    0.945497305199473E-03,
    0.470398442663523E-03
  };
  const EpsTriplet<double> ML51[] = {
    EpsTriplet<double>(0.547556723836849E+01, -0.303215037013634E+00, -0.266666666666667E+01),
    EpsTriplet<double>(0.547556723836830E+01, -0.303215037013629E+00, -0.266666666666667E+01),
    EpsTriplet<double>(0.547556723836826E+01, -0.303215037013630E+00, -0.266666666666667E+01),
    EpsTriplet<double>(0.547556723836872E+01, -0.303215037013630E+00, -0.266666666666667E+01)
  };

  int ord[namps][legs] = {
    {0, 1, 2, 3},
    {1, 0, 2, 3},
    {0, 1, 2, 3},
    {0, 1, 2, 3}
  };

  for (int i=0; i<namps; i++) {
    if (not amp[i]) continue;
    cout << process[i] << endl;
    const double prefactor1 = pow(gs, 2.)*prefactor0[i]/(16.*M_PI*M_PI);
    amp[i]->setMuR2(MuR2);
    amp[i]->setNc(Nc);
    amp[i]->setNf(Nf);

    MOM<double> mm[legs];
    for (int j=0; j<4; j++) mm[j] = Momenta[ord[i][j]];
    amp[i]->setMomenta(mm);

    EpsTriplet<double> loop = amp[i]->virt();
    double tree = amp[i]->born();
    double tree_cc[legs*(legs-1)/2] = {0.};
    amp[i]->born_cc(tree_cc);

    EpsTriplet<double> poles = getpoles<double>(ng, nq, Nc, Nf, mm, MuR2, tree, tree_cc);

    double ans0 = prefactor0[i]*tree;
    cout << "A0    " << ans0 << endl;
    cout << "ML50  " << ML50[i] << endl;
    EpsTriplet<double> ans1 = prefactor1*(loop+getrenorm<double>(ng, nq, Nc, Nf, tree));
    cout << "A1    " << ans1 << endl;
    cout << "poles " << prefactor1*poles << endl;
    EpsTriplet<double> ans1_ratio = ans1/ans0*0.5*(16.*M_PI*M_PI)/(gs*gs);
    cout << "A1/A0 = " << ans1_ratio << endl;
    cout << "ML51  = " << ML51[i] << endl;
    cout << "diff  = " << ML51[i] -ans1_ratio << endl;
    cout << endl;
  }

  for (int n=0; n<namps; n++) {
    if (amp[n]) {
      delete amp[n];
      amp[n] = 0;
    }
  }
}
//}}}

//{{{ ppV1j
void ppV1j()
{
  double mW = 80.419002445756163;
  double gW = 2.0476;
  double mZ = 91.188;
  double gZ = 2.441404;
  double se2 = 0.22224648578577766; // sin(theta_EW)^2
  StandardModel::setWmass(mW, gW);
  StandardModel::setZmass(mZ, gZ);
  StandardModel::setSinThetaWSq(se2);

  double Nc = 3.;
  double as = 0.118;
  double ae = 7.54677111397888348E-003;
  double gs = sqrt(4.*M_PI*as);
  double ee = sqrt(4.*M_PI*ae);
  double se = sqrt(se2);

  const int namps = 4;
  std::string process[namps] = {
    "# tests for u d~ > g W+ > ve e+",
    "# tests for d u~ > g W- > ve~ e-",
    "# tests for u u~ > g Z/g* > ve e+",
    "# tests for d d~ > g Z/g* > ve e+"
  };

  double prefactor0[namps] = {
    pow(gs, 2.)*pow(ee/se, 4.)/(Nc*Nc*4.),
    pow(gs, 2.)*pow(ee/se, 4.)/(Nc*Nc*4.),
    pow(gs, 2.)*pow(ee, 4.)/(Nc*Nc*4.),
    pow(gs, 2.)*pow(ee, 4.)/(Nc*Nc*4.)
  };

  Flavour<double> Wp = StandardModel::Wp(StandardModel::u(), StandardModel::ubar(), 1., mW, gW);
  Flavour<double> Wm = StandardModel::Wm(StandardModel::u(), StandardModel::ubar(), 1., mW, gW);
  Flavour<double> Zu = StandardModel::Zu(StandardModel::u(), StandardModel::ubar(), mZ, gZ);
  Flavour<double> Zd = StandardModel::Zd(StandardModel::u(), StandardModel::ubar(), mZ, gZ);

  NJetAmp<double>* amp[namps] = {
    new Amp2q1gV<double>(Wp, 1.),
    new Amp2q1gV<double>(Wm, 1.),
    new Amp2q1gZ<double>(Zu, 1.),
    new Amp2q1gZ<double>(Zd, 1.),
  };

  const int legs = 5;
  const MOM<double> MomentaW[legs] = {
    MOM<double>(-0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00,-0.5000000000000000E+03),
    MOM<double>(-0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00, 0.5000000000000000E+03),
    MOM<double>( 0.4585787878854402E+03, 0.1694532203096798E+03, 0.3796536620781987E+03,-0.1935024746502525E+03),
    MOM<double>( 0.3640666207368177E+03,-0.1832986929319185E+02,-0.3477043013193671E+03, 0.1063496077587081E+03),
    MOM<double>( 0.1773545913777421E+03,-0.1511233510164880E+03,-0.3194936075883156E+02, 0.8715286689154436E+02)
  };

  const double ML50[] = {
    1.1310217621016346E-006,
    1.1310217621016346E-006,
    4.8640822999840484E-007,
    2.7712010975873983E-007
  };
  const EpsTriplet<double> ML51[] = {
    EpsTriplet<double>(-3.4336982669789315, -13.432637493700025, -5.6666666666668588),
    EpsTriplet<double>(-3.4336982669789315, -13.432637493700025, -5.6666666666668588),
    EpsTriplet<double>(-1.6630637712678193, -13.432637493700030, -5.6666666666666883),
    EpsTriplet<double>(-2.5117845834692600, -13.432637493699994, -5.6666666666666829)
  };

  double Nf = 4.;
  int nq = 2;
  int ng = 1;
  double sqrtS = 1e3;
  double MuR2 = sqrtS*sqrtS;
  for (int i=0; i<namps; i++) {
    if (not amp[i]) continue;
    cout << process[i] << endl;
    const double prefactor1 = pow(gs, 2.)*prefactor0[i]/(16.*M_PI*M_PI);
    amp[i]->setMuR2(MuR2);
    amp[i]->setNc(Nc);
    amp[i]->setNf(Nf);

    amp[i]->setMomenta(MomentaW);
    EpsTriplet<double> loop = amp[i]->virt();
    double tree = amp[i]->born();
    loop += getrenorm<double>(ng, nq, Nc, Nf, tree);
    double tree_cc[legs*(legs-1)/2] = {0.};
    amp[i]->born_cc(tree_cc);

    cout << "A0    " << prefactor0[i]*tree << endl;
    cout << "ML50  " << ML50[i] << endl;
    cout << "A1    " << prefactor1*loop << endl;
    cout << "poles " << prefactor1*getpoles<double>(ng, nq, Nc, Nf, MomentaW, MuR2, tree, tree_cc) << endl;
    cout << "A1/A0 " << loop/tree*0.5 << endl;
    cout << "ML51  " << ML51[i] << endl;
    cout << "diff = " << ML51[i] - loop/tree*0.5 << endl;
    cout << endl;
  }

  for (int n=0; n<namps; n++) {
    if (amp[n]) {
      delete amp[n];
      amp[n] = 0;
    }
  }
}
//}}}

//{{{ ppV2j
void ppV2j()
{
  double mW = 80.419002445756163;
  double gW = 2.0476;
  double mZ = 91.188;
  double gZ = 2.441404;
  double se2 = 0.22224648578577766; // sin(theta_EW)^2
  StandardModel::setWmass(mW, gW);
  StandardModel::setZmass(mZ, gZ);
  StandardModel::setSinThetaWSq(se2);

  double Nc = 3.;
  double Nf = 4.;
  double as = 0.118;
  double ae = 7.54677111397888348E-003;
  double gs = sqrt(4.*M_PI*as);
  double ee = sqrt(4.*M_PI*ae);
  double se = sqrt(se2);

  const int legs = 6;
  const int npoints = 5;

  const MOM<double> MG5moms[npoints][legs] = {
    {
      MOM<double>(   -0.500000000000000E+03,    -0.000000000000000E+00,    -0.000000000000000E+00,    -0.500000000000000E+03),
      MOM<double>(   -0.500000000000000E+03,    -0.000000000000000E+00,    -0.000000000000000E+00,     0.500000000000000E+03),
      MOM<double>(    0.885513330545030E+02,    -0.221006902876900E+02,     0.400803531916853E+02,    -0.758054309569366E+02),
      MOM<double>(    0.328329419227099E+03,    -0.103849611883456E+03,    -0.301933755389540E+03,     0.764949213871659E+02),
      MOM<double>(    0.152358109467431E+03,    -0.105880959666592E+03,    -0.977096383269757E+02,     0.495483852267928E+02),
      MOM<double>(    0.430761138250968E+03,     0.231831261837738E+03,     0.359563040524831E+03,    -0.502378756570221E+02)
    },{
      MOM<double>(   -0.500000000000000E+03,    -0.000000000000000E+00,    -0.000000000000000E+00,    -0.500000000000000E+03),
      MOM<double>(   -0.500000000000000E+03,    -0.000000000000000E+00,    -0.000000000000000E+00,     0.500000000000000E+03),
      MOM<double>(    0.117747708137171E+03,    -0.607059218463615E+02,     0.712310458623266E+02,     0.714524452324687E+02),
      MOM<double>(    0.350954173077969E+03,    -0.317881667026108E+02,     0.839394286931734E+02,     0.339282354933456E+03),
      MOM<double>(    0.349332228573790E+03,     0.184009303995763E+03,    -0.515277979392370E+02,    -0.292435408257719E+03),
      MOM<double>(    0.181965890211070E+03,    -0.915152154467912E+02,    -0.103642676616263E+03,    -0.118299391908206E+03)
    },{
      MOM<double>(   -0.500000000000000E+03,    -0.000000000000000E+00,    -0.000000000000000E+00,    -0.500000000000000E+03),
      MOM<double>(   -0.500000000000000E+03,    -0.000000000000000E+00,    -0.000000000000000E+00,     0.500000000000000E+03),
      MOM<double>(    0.258692822424728E+03,     0.132468399481517E+03,    -0.169618879017114E+03,    -0.143539316543778E+03),
      MOM<double>(    0.108457863834848E+03,    -0.573577369439504E+02,    -0.216250529683915E+02,    -0.894737689180533E+02),
      MOM<double>(    0.400574546148629E+03,    -0.158077685562145E+03,     0.356319809463483E+03,     0.922366832307132E+02),
      MOM<double>(    0.232274767591795E+03,     0.829670230245788E+02,    -0.165075877477977E+03,     0.140776402231118E+03)
    },{
      MOM<double>(   -0.500000000000000E+03,    -0.000000000000000E+00,    -0.000000000000000E+00,    -0.500000000000000E+03),
      MOM<double>(   -0.500000000000000E+03,    -0.000000000000000E+00,    -0.000000000000000E+00,     0.500000000000000E+03),
      MOM<double>(    0.159571663754514E+03,    -0.691788160250290E+02,    -0.139519030356374E+03,    -0.348115994343293E+02),
      MOM<double>(    0.224778227571495E+03,    -0.136015435853364E+03,     0.165035779308367E+03,    -0.691971411828716E+02),
      MOM<double>(    0.250876533501086E+03,     0.144787822276561E+01,     0.249985686262268E+03,    -0.210735705127735E+02),
      MOM<double>(    0.364773575172904E+03,     0.203746373655628E+03,    -0.275502435214260E+03,     0.125082311129974E+03)
    },{
      MOM<double>(   -0.500000000000000E+03,    -0.000000000000000E+00,    -0.000000000000000E+00,    -0.500000000000000E+03),
      MOM<double>(   -0.500000000000000E+03,    -0.000000000000000E+00,    -0.000000000000000E+00,     0.500000000000000E+03),
      MOM<double>(    0.412858745690407E+03,    -0.871915070082442E+01,     0.138691324708477E+03,    -0.388768615959056E+03),
      MOM<double>(    0.796870803484754E+02,     0.273730894687734E+02,     0.698144057227168E+02,    -0.269572532165795E+02),
      MOM<double>(    0.391861899972489E+03,    -0.362186885048401E+02,    -0.101066839786417E+03,     0.376867946565670E+03),
      MOM<double>(    0.115592273988629E+03,     0.175647497368911E+02,    -0.107438890644778E+03,     0.388579226099660E+02),
    }
  };

  const double ML50_udx_ggepve[] = {
    5.3285934480530457E-010,
    2.4356314465029407E-008,
    8.2300262180867264E-010,
    1.3947578701223943E-010,
    2.2899312387707756E-009
  };
  const EpsTriplet<double> ML51_udx_ggepve[] = {
    EpsTriplet<double>(  -40.346566952928100,       -36.248321709626545,       -8.6666666666665968 ),
    EpsTriplet<double>(  -64.421060458355427,       -42.260793299003623,       -8.6666666666666092 ),
    EpsTriplet<double>(  -27.857260196973016,       -35.621258115716095,       -8.6666666666666643 ),
    EpsTriplet<double>(  -1.2104817339082803,       -28.951104748278279,       -8.6666666666666163 ),
    EpsTriplet<double>(  -56.792557770082823,       -40.697575389113787,       -8.6666666666668668 )
  };

  const double ML50_udx_sxsepve[] = {
    4.7249227429293764E-012,
    4.8314131703131530E-010,
    3.2504810293499303E-011,
    1.3827579482591743E-011,
    4.7636025972372016E-011
  };
  const EpsTriplet<double> ML51_udx_sxsepve[] = {
    EpsTriplet<double>(  -25.324873896407652,       -22.398545365303324,       -5.3333333333333064      ),
    EpsTriplet<double>(  -16.133899073062555,       -23.232396252944270,       -5.3333333333332975      ),
    EpsTriplet<double>(  0.11231309135122966,       -18.985472610246756,       -5.3333333333333020      ),
    EpsTriplet<double>(   2.2444786420508889,       -17.257841896288333,       -5.3333333333333002      ),
    EpsTriplet<double>(  -14.812959374421123,       -22.808426887694392,       -5.3333333333334449      )
  };

  const double ML50_udx_dxdepve[] = {
    6.1490070622480334E-011,
    4.6311725074183677E-010,
    4.1506347543267170E-011,
    2.0530434819006563E-011,
    1.0440939984252139E-009
  };
  const EpsTriplet<double> ML51_udx_dxdepve[] = {
    EpsTriplet<double>(   20.377560625603746,       -14.221222581207863,       -5.3333333333333623      ),
    EpsTriplet<double>(  -17.307963292213373,       -23.552961041877694,       -5.3333333333332762      ),
    EpsTriplet<double>(   3.4532680996221603,       -19.132336781668791,       -5.3333333333333064      ),
    EpsTriplet<double>(   10.501276113464323,       -16.619290832364435,       -5.3333333333333082      ),
    EpsTriplet<double>(   19.066225304836749,       -17.036260508432456,       -5.3333333333334645      ),
  };

  const double ML50_udx_uxuepve[] = {
    5.3570798087374086E-012,
    4.3502005021090470E-009,
    3.5070542975875295E-011,
    1.4951730563113969E-011,
    6.8124804797944237E-011
  };
  const EpsTriplet<double> ML51_udx_uxuepve[] = {
    EpsTriplet<double>(  -20.297854554298880,       -21.326729117836653,       -5.3333333333333153      ),
    EpsTriplet<double>(   15.612983622425819,       -18.345776175862049,       -5.3333333333333481      ),
    EpsTriplet<double>( -0.37924089650747123,       -19.037520595798089,       -5.3333333333333108      ),
    EpsTriplet<double>(   2.9527733754031136,       -17.169356217730574,       -5.3333333333333020      ),
    EpsTriplet<double>(  -12.253540982592517,       -21.014373087544602,       -5.3333333333334085      )
  };

  const double ML50_uux_ggepem[] = {
    2.8099109212963674E-010,
    1.0168027220918070E-008,
    3.9075272252099338E-010,
    6.3093435253554534E-011,
    2.0386347772106033E-009
  };
  const EpsTriplet<double> ML51_uux_ggepem[] = {
    EpsTriplet<double>(  -39.691914266512391,       -36.248697286812202,       -8.6666666666665790 ),
    EpsTriplet<double>(  -61.673668580052315,       -42.261263529159123,       -8.6666666666666003 ),
    EpsTriplet<double>(  -27.074673379477765,       -35.621040603480580,       -8.6666666666666661 ),
    EpsTriplet<double>(  -2.5396505698868070,       -28.954492056390574,       -8.6666666666666146 ),
    EpsTriplet<double>(  -57.123713714616621,       -40.756343865855953,       -8.6666666666667709 )
  };

  const double ML50_uux_cxcepem[] = {
    0.238686266029046E-11,
    0.184733527664505E-09,
    0.158032877303210E-10,
    0.649040547586803E-11,
    0.330047906221948E-10
  };
  const EpsTriplet<double> ML51_uux_cxcepem[] = {
    EpsTriplet<double>(   -0.232802724668137E+02,   -0.223985453653041E+02,   -0.533333333333330E+01),
    EpsTriplet<double>(   -0.117671736458246E+02,   -0.232323962529440E+02,   -0.533333333333328E+01),
    EpsTriplet<double>(   -0.346300730415821E+00,   -0.189854726102466E+02,   -0.533333333333331E+01),
    EpsTriplet<double>(    0.185558696464288E+01,   -0.172578418962882E+02,   -0.533333333333331E+01),
    EpsTriplet<double>(   -0.158751130858586E+02,   -0.228084268876941E+02,   -0.533333333333351E+01)
  };

  const double ML50_uux_dxdepem[] = {
    2.6953359363471435E-012,
    2.1679547192032063E-010,
    1.4919954987254625E-011,
    6.1136776650285313E-012,
    4.3441672863840614E-011
  };
  const EpsTriplet<double> ML51_uux_dxdepem[] = {
    EpsTriplet<double>(  -22.033325045420455,       -22.398545365303423,       -5.3333333333332895      ),
    EpsTriplet<double>(  -12.177419442293317,       -23.232396252945104,       -5.3333333333332771      ),
    EpsTriplet<double>(  0.27985939804419363,       -18.985472610246212,       -5.3333333333333002      ),
    EpsTriplet<double>(   1.7969000479057522,       -17.257841896288273,       -5.3333333333333002      ),
    EpsTriplet<double>(  -16.581815597055751,       -22.808426887693717,       -5.3333333333334512      )
  };
  const double ML50_uux_uxuepem[] = {
    3.1010488271348408E-011,
    1.8612685218941586E-009,
    2.3404948921904824E-011,
    1.1006986614266400E-011,
    6.5179042403816918E-010
  };
  const EpsTriplet<double> ML51_uux_uxuepem[] = {
    EpsTriplet<double>(   20.638859871510935,       -14.232208617535859,       -5.3333333333333357      ),
    EpsTriplet<double>(   18.561249683257280,       -18.275617725286580,       -5.3333333333333375      ),
    EpsTriplet<double>(   2.4246875528086433,       -19.051031346626420,       -5.3333333333333153      ),
    EpsTriplet<double>(   10.580628366423591,       -16.482862119128402,       -5.3333333333333082      ),
    EpsTriplet<double>(   16.956099589262056,       -17.049305608391148,       -5.3333333333334689      )
  };
  const double ML50_uux_ggepemA[] = {
    0.293607738738820E-09,
    0.442702678918472E-08,
    0.269822504355210E-09,
    0.326929234359730E-10,
    0.744587473578547E-08,
  };
  const EpsTriplet<double> ML51_uux_ggepemA[] = {
    EpsTriplet<double>(   -0.402772678432324E+02,   -0.362497973252645E+02,   -0.866666666666652E+01),
    EpsTriplet<double>(   -0.598673173304195E+02,   -0.422659141188673E+02,   -0.866666666666663E+01),
    EpsTriplet<double>(   -0.274377261370544E+02,   -0.356199773714885E+02,   -0.866666666666665E+01),
    EpsTriplet<double>(   -0.322617677963711E+01,   -0.289775953258567E+02,   -0.866666666666666E+01),
    EpsTriplet<double>(   -0.569342955047659E+02,   -0.407962005991480E+02,   -0.866666666666688E+01),
  };
  const double ML50_uux_cxcepemA[] = {
    0.264417540075374E-11,
    0.856425703219001E-10,
    0.103435126017173E-10,
    0.326253009912192E-11,
    0.118706290674402E-09,
  };
  const EpsTriplet<double> ML51_uux_cxcepemA[] = {
    EpsTriplet<double>(   -0.224605560721704E+02,   -0.223985453653043E+02,   -0.533333333333327E+01),
    EpsTriplet<double>(   -0.872476138089226E+01,   -0.232323962529435E+02,   -0.533333333333326E+01),
    EpsTriplet<double>(   -0.936235409464604E+00,   -0.189854726102465E+02,   -0.533333333333330E+01),
    EpsTriplet<double>(    0.214281899062025E+01,   -0.172578418962882E+02,   -0.533333333333333E+01),
    EpsTriplet<double>(   -0.158766395252593E+02,   -0.228084268876938E+02,   -0.533333333333344E+01),
  };
  const double ML50_uux_uxuepemA[] = {
    0.232531666995349E-10,
    0.786838936893495E-09,
    0.174329791214036E-10,
    0.577820569905169E-11,
    0.129405630339454E-08,
  };
  const EpsTriplet<double> ML51_uux_uxuepemA[] = {
    EpsTriplet<double>(    0.189096328547372E+02,   -0.146042173291913E+02,   -0.533333333333330E+01),
    EpsTriplet<double>(    0.193866525460619E+02,   -0.183387008412940E+02,   -0.533333333333327E+01),
    EpsTriplet<double>(    0.260284049836323E+01,   -0.189490006411049E+02,   -0.533333333333331E+01),
    EpsTriplet<double>(    0.105357911933761E+02,   -0.164740272295041E+02,   -0.533333333333334E+01),
    EpsTriplet<double>(    0.111770405020137E+02,   -0.173173718218678E+02,   -0.533333333333341E+01),
  };

  Flavour<double> Gstar = StandardModel::Gstar(StandardModel::u(), StandardModel::ubar());
  Flavour<double> W = StandardModel::Wp(StandardModel::u(), StandardModel::ubar(), 1., mW, gW);
  Flavour<double> Zu = StandardModel::Zu(StandardModel::u(), StandardModel::ubar(), mZ, gZ);

  const int namps = 11;
  std::string process[namps] = {
    "# tests for u d~ > g g W+ > e+ ve",
    "# tests for u d~ > s~ s W+ > e+ ve",
    "# tests for u d~ > d~ d W+ > e+ ve",
    "# tests for u d~ > u~ u W+ > e+ ve",
    "# tests for u u~ > g g Z/g* > e+ e-",
    "# tests for u u~ > c~ c Z/g* > e+ e-",
    "# tests for u u~ > d~ d Z/g* > e+ e-",
    "# tests for u u~ > u~ u Z/g* > e+ e-",
    "# tests for u u~ > g g A > e+ e-",
    "# tests for u u~ > c~ c A > e+ e-",
    "# tests for u u~ > u~ u A > e+ e-",
  };

  double sqrtS = 1e3;
  NJetAmp<double>* amp[namps] = {
    new Amp2q2gV<double>(W, 1.),
    new Amp4q0gV<double>(W, 1.),
    new Amp4q0gV2<double>(W, 1.),
    new Amp4q0gV2b<double>(W, 1.),
    new Amp2q2gZ<double>(Zu, 1.),
    new Amp4q0gZ<double>(Zu, 1.),
    new Amp4q0gZd<double>(Zu, 1.),
    new Amp4q0gZ2<double>(Zu, 1.),
    new Amp2q2gZ<double>(Gstar, 1.),
    new Amp4q0gZ<double>(Gstar, 1.),
    new Amp4q0gZ2<double>(Gstar, 1.),
  };

  double times[namps][npoints];

  int ord[namps][legs] = {
    {0, 1, 2, 3, 5, 4},
    {0, 1, 2, 3, 5, 4},
    {0, 1, 2, 3, 5, 4},
    {0, 1, 2, 3, 5, 4},
    {0, 1, 2, 3, 5, 4},
    {0, 1, 2, 3, 5, 4},
    {0, 1, 2, 3, 5, 4},
    {0, 1, 2, 3, 5, 4},
    {0, 1, 2, 3, 5, 4},
    {0, 1, 2, 3, 5, 4},
    {0, 1, 2, 3, 5, 4},
  };

  const double Qu = 2./3.;
  double prefactor0[] = {
    pow(ee/se, 4.)*pow(gs, 4.)/(Nc*Nc*4.*2.),
    pow(ee/se, 4.)*pow(gs, 4.)/(Nc*Nc*4.),
    pow(ee/se, 4.)*pow(gs, 4.)/(Nc*Nc*4.),
    pow(ee/se, 4.)*pow(gs, 4.)/(Nc*Nc*4.),
    pow(ee, 4.)*pow(gs, 4.)/(Nc*Nc*4.*2.),
    pow(ee, 4.)*pow(gs, 4.)/(Nc*Nc*4.),
    pow(ee, 4.)*pow(gs, 4.)/(Nc*Nc*4.),
    pow(ee, 4.)*pow(gs, 4.)/(Nc*Nc*4.),
    (Qu*Qu)*pow(ee, 4.)*pow(gs, 4.)/(Nc*Nc*4.*2.),
    (Qu*Qu)*pow(ee, 4.)*pow(gs, 4.)/(Nc*Nc*4.),
    (Qu*Qu)*pow(ee, 4.)*pow(gs, 4.)/(Nc*Nc*4.),
  };

  const double* ML5res0[namps] = {
    ML50_udx_ggepve,
    ML50_udx_sxsepve,
    ML50_udx_dxdepve,
    ML50_udx_uxuepve,
    ML50_uux_ggepem,
    ML50_uux_cxcepem,
    ML50_uux_dxdepem,
    ML50_uux_uxuepem,
    ML50_uux_ggepemA,
    ML50_uux_cxcepemA,
    ML50_uux_uxuepemA,
  };
  const EpsTriplet<double>* ML5res1[namps] = {
    ML51_udx_ggepve,
    ML51_udx_sxsepve,
    ML51_udx_dxdepve,
    ML51_udx_uxuepve,
    ML51_uux_ggepem,
    ML51_uux_cxcepem,
    ML51_uux_dxdepem,
    ML51_uux_uxuepem,
    ML51_uux_ggepemA,
    ML51_uux_cxcepemA,
    ML51_uux_uxuepemA,
  };

  int nq[namps] = {2, 4, 4, 4, 2, 4, 4, 4, 2, 4, 4};
  int ng[namps] = {2, 0, 0, 0, 2, 0, 0, 0, 2, 0, 0};
  double MuR2 = sqrtS*sqrtS;
  for (int i=0; i<namps; i++) {
    if (not amp[i]) continue;
    cout << process[i] << endl;
    amp[i]->setMuR2(MuR2);
    amp[i]->setNc(Nc);
    amp[i]->setNf(Nf);

    const double prefactor1 = pow(gs,2.)*prefactor0[i]/(16.*M_PI*M_PI);

    for (int pt=0; pt<npoints; pt++) {
      MOM<double> mm[legs];
      for (int j=0; j<legs; j++) {
        mm[j] = MG5moms[pt][ord[i][j]];
      }
      amp[i]->setMomenta(mm);

      tm.start();
      EpsTriplet<double> loop = amp[i]->virt();
      double tree = amp[i]->born();
      loop += getrenorm<double>(ng[i], nq[i], Nc, Nf, tree);
      times[i][pt] = static_cast<double>(tm.get());

      double tree_cc[legs*(legs-1)/2] = {0.};
      amp[i]->born_cc(tree_cc);

      double ans0 = prefactor0[i]*tree;
      cout << "A0    " << ans0 << "\n";
      cout << "ML0   " << ML5res0[i][pt] << " rel. diff("<< (ML5res0[i][pt]-ans0)/ans0 << ")" << endl;
      cout << "A1    " << prefactor1*loop << "\n";
      cout << "poles " << prefactor1*getpoles<double>(ng[i], nq[i], Nc, Nf, mm, MuR2, tree, tree_cc) << "\n";
      EpsTriplet<double> ans1_ratio = loop/tree*0.5;
      cout << "A1/A0 " << ans1_ratio << endl;
      cout << "ML1   " << ML5res1[i][pt] << "\n  diff("<< ML5res1[i][pt]-ans1_ratio << ")" << endl;
    }
    cout << "TIME ";
    print_stat(npoints, times[i]);
    cout << endl;
    cout << endl;
  }

  for (int n=0; n<namps; n++) {
    if (amp[n]) {
      delete amp[n];
      amp[n] = 0;
    }
  }
}
//}}}

//{{{ ppV3j
void ppV3j()
{
  double mW = 80.419002445756163;
  double gW = 2.0476;
  double mZ = 91.188;
  double gZ = 2.441404;
  double se2 = 0.22224648578577766; // sin(theta_EW)^2
  StandardModel::setWmass(mW, gW);
  StandardModel::setZmass(mZ, gZ);
  StandardModel::setSinThetaWSq(se2);

  double Nc = 3.;
  double Nf = 4.;
  double as = 0.118;
  double ae = 7.54677111397888348E-003;
  double gs = sqrt(4.*M_PI*as);
  double ee = sqrt(4.*M_PI*ae);
  double se = sqrt(se2);

  const int legs = 7;
  const int npoints = 5;

  const MOM<double> MG5moms[npoints][legs] = {
    {
      MOM<double>(-0.500000000000000E+03, -0.000000000000000E+00, -0.000000000000000E+00, -0.500000000000000E+03),
      MOM<double>(-0.500000000000000E+03, -0.000000000000000E+00, -0.000000000000000E+00,  0.500000000000000E+03),
      MOM<double>( 0.863540681437814E+02, -0.152133893202618E+02,  0.376335512949163E+02, -0.762187226821854E+02),
      MOM<double>( 0.280118181809376E+03, -0.831261116505822E+02, -0.263203856758651E+03,  0.477490851160266E+02),
      MOM<double>( 0.127522529569661E+03, -0.904490412959935E+02, -0.831783077030789E+02,  0.340930433392580E+02),
      MOM<double>( 0.414130068374543E+03,  0.232145564945939E+03,  0.332754436780819E+03, -0.829857518524426E+02),
      MOM<double>( 0.918751521026384E+02, -0.433570226791011E+02, -0.240058236140057E+02,  0.773623460793435E+02),
    },{
      MOM<double>(-0.500000000000000E+03, -0.000000000000000E+00, -0.000000000000000E+00, -0.500000000000000E+03),
      MOM<double>(-0.500000000000000E+03, -0.000000000000000E+00, -0.000000000000000E+00,  0.500000000000000E+03),
      MOM<double>( 0.399131606722457E+03, -0.861724378569117E+02,  0.713374954007305E+02,  0.383133543544020E+03),
      MOM<double>( 0.155569819713253E+03,  0.849184942870765E+02, -0.682995868362328E+02, -0.111022450754925E+03),
      MOM<double>( 0.127597475940391E+03, -0.955819920839683E+02, -0.827047427621708E+02, -0.174678041053229E+02),
      MOM<double>( 0.209925273473134E+03,  0.126492573956664E+03,  0.271543132220890E+02, -0.165320574793413E+03),
      MOM<double>( 0.107775824150766E+03, -0.296566383028609E+02,  0.525125209755841E+02, -0.893227138903587E+02),
    },{
      MOM<double>(-0.500000000000000E+03, -0.000000000000000E+00, -0.000000000000000E+00, -0.500000000000000E+03),
      MOM<double>(-0.500000000000000E+03, -0.000000000000000E+00, -0.000000000000000E+00,  0.500000000000000E+03),
      MOM<double>( 0.263497265970635E+03, -0.769341967950308E+01,  0.250649780024194E+03, -0.809092593060758E+02),
      MOM<double>( 0.203468135376530E+03,  0.122973498632152E+03, -0.156065316940634E+03,  0.438225694818240E+02),
      MOM<double>( 0.275945228903198E+03, -0.323043803391298E+02, -0.273578486342961E+03,  0.160314745309749E+02),
      MOM<double>( 0.123369604267851E+03, -0.110078538320951E+03,  0.533788267415079E+02, -0.159209143590817E+02),
      MOM<double>( 0.133719765481786E+03,  0.271028397074315E+02,  0.125615196517894E+03,  0.369761296523586E+02),
    },{
      MOM<double>(-0.500000000000000E+03, -0.000000000000000E+00, -0.000000000000000E+00, -0.500000000000000E+03),
      MOM<double>(-0.500000000000000E+03, -0.000000000000000E+00, -0.000000000000000E+00,  0.500000000000000E+03),
      MOM<double>( 0.276631880597796E+03,  0.122682822995529E+03, -0.183928023598235E+03,  0.166266666653808E+03),
      MOM<double>( 0.391696213280817E+03, -0.661079850209939E+02,  0.189606852489668E+03, -0.336310718389938E+03),
      MOM<double>( 0.712818707681210E+02,  0.774625132546946E+01,  0.634576356809854E+02, -0.315313996578822E+02),
      MOM<double>( 0.204664674114346E+03, -0.636547258071426E+02, -0.139304441927532E+02,  0.194014554704903E+03),
      MOM<double>( 0.557253612389200E+02, -0.666363492861476E+00, -0.552060203796661E+02,  0.756089668910973E+01),
    },{
      MOM<double>(-0.500000000000000E+03, -0.000000000000000E+00, -0.000000000000000E+00, -0.500000000000000E+03),
      MOM<double>(-0.500000000000000E+03, -0.000000000000000E+00, -0.000000000000000E+00,  0.500000000000000E+03),
      MOM<double>( 0.145275585603787E+03,  0.593390521511606E+02, -0.113674436439151E+03, -0.682788046363326E+02),
      MOM<double>( 0.137147952022567E+03,  0.555211967335753E+02,  0.187209897768859E+02,  0.124001943529244E+03),
      MOM<double>( 0.183495256172927E+03,  0.554674876006304E+02,  0.146281075954261E+03,  0.958942838489653E+02),
      MOM<double>( 0.182830232899459E+03, -0.436229997679493E+02, -0.287536680141779E+02,  0.175206034510934E+03),
      MOM<double>( 0.351250973301259E+03, -0.126704736717417E+03, -0.225739612778178E+02, -0.326823457252810E+03),
    }
  };

  const double ML50_udx_gggepve[] = {
    0.405540248387328E-11,
    0.575146988659830E-11,
    0.956515055983050E-13,
    0.289573341692037E-11,
    0.331057719598578E-14,
  };

  const EpsTriplet<double> ML51_udx_gggepve[] = {
    EpsTriplet<double>(   -0.911432123130513E+02,   -0.586891244440685E+02,   -0.116666666666667E+02),
    EpsTriplet<double>(   -0.445504670010842E+02,   -0.527223772522711E+02,   -0.116666666666669E+02),
    EpsTriplet<double>(   -0.187756011628975E+02,   -0.386090948560143E+02,   -0.116666666666667E+02),
    EpsTriplet<double>(   -0.584707957506837E+02,   -0.501973185001741E+02,   -0.116666666666668E+02),
    EpsTriplet<double>(   -0.555405335287168E+02,   -0.525974741467072E+02,   -0.116666666666667E+02),
  };

  const double ML50_udx_sxsgepve[] = {
    0.437886669926867E-13,
    0.500967110057145E-12,
    0.139803784850306E-13,
    0.806811638782506E-12,
    0.100411559728213E-15,
  };
  const EpsTriplet<double> ML51_udx_sxsgepve[] = {
    EpsTriplet<double>(   -0.655356993256244E+02,   -0.436199339685584E+02,   -0.833333333333355E+01),
    EpsTriplet<double>(   -0.106484969328800E+02,   -0.299675645956763E+02,   -0.833333333333325E+01),
    EpsTriplet<double>(   -0.970992011070450E+01,   -0.290146006126655E+02,   -0.833333333333340E+01),
    EpsTriplet<double>(   -0.410271419407710E+02,   -0.348241967023475E+02,   -0.833333333333340E+01),
    EpsTriplet<double>(   -0.159707216177040E+02,   -0.344624366949999E+02,   -0.833333333333329E+01),
  };

  const double ML50_udx_uxugepve[] = {
    0.583387442190979E-13,
    0.105999494050903E-11,
    0.235085156002289E-12,
    0.207989154051589E-11,
    0.207091079193539E-13,
  };
  const EpsTriplet<double> ML51_udx_uxugepve[] = {
    EpsTriplet<double>(   -0.535141739848194E+02,   -0.409662591439107E+02,   -0.833333333333341E+01),
    EpsTriplet<double>(    0.635162708009662E+01,   -0.298260487038394E+02,   -0.833333333333288E+01),
    EpsTriplet<double>(    0.142400759362160E+02,   -0.253940134810653E+02,   -0.833333333333338E+01),
    EpsTriplet<double>(   -0.282213100368388E+02,   -0.339875272285017E+02,   -0.833333333333347E+01),
    EpsTriplet<double>(    0.159155030406015E+02,   -0.297868208106220E+02,   -0.833333333333342E+01),
  };

  const double ML50_udx_dxdgepve[] = {
    0.333966192595395E-12,
    0.555234115283660E-12,
    0.807816953795643E-13,
    0.221814368388544E-11,
    0.553607805676006E-14,
  };
  const EpsTriplet<double> ML51_udx_dxdgepve[] = {
    EpsTriplet<double>(   -0.185919523500207E+02,   -0.349527208489157E+02,   -0.833333333333306E+01),
    EpsTriplet<double>(   -0.922184409736063E+01,   -0.299364490034428E+02,   -0.833333333333328E+01),
    EpsTriplet<double>(    0.222137422783084E+02,   -0.259247966026757E+02,   -0.833333333333313E+01),
    EpsTriplet<double>(   -0.183951019824886E+02,   -0.339348061704018E+02,   -0.833333333333351E+01),
    EpsTriplet<double>(    0.994988984113492E+01,   -0.299596191528006E+02,   -0.833333333333332E+01),
  };

   const double ML50_uux_gggepemA[] = {
    0.839207848460581E-12,
    0.529707334342161E-12,
    0.184399261788436E-13,
    0.699702393261845E-12,
    0.253737564789896E-12,
  };
  const EpsTriplet<double> ML51_uux_gggepemA[] = {
    EpsTriplet<double>(   -0.900181758006225E+02,   -0.586749701461529E+02,   -0.116666666666667E+02),
    EpsTriplet<double>(   -0.465109011527525E+02,   -0.526459359119806E+02,   -0.116666666666664E+02),
    EpsTriplet<double>(   -0.166042053397662E+02,   -0.386001967356590E+02,   -0.116666666666667E+02),
    EpsTriplet<double>(   -0.580435919878309E+02,   -0.509979057922910E+02,   -0.116666666666668E+02),
    EpsTriplet<double>(   -0.626966284018711E+02,   -0.555437651478206E+02,   -0.116666666666667E+02),
  };
  const double ML50_uux_cxcgepemA[] = {
    0.747889173015858E-14,
    0.951079320596959E-13,
    0.244585116996138E-14,
    0.390900512563798E-12,
    0.907592540829090E-15,
  };
  const EpsTriplet<double> ML51_uux_cxcgepemA[] = {
    EpsTriplet<double>(   -0.610107648102260E+02,   -0.435197641896715E+02,   -0.833333333333328E+01),
    EpsTriplet<double>(   -0.136244228687811E+02,   -0.298796672337736E+02,   -0.833333333333405E+01),
    EpsTriplet<double>(   -0.904518865815868E+01,   -0.291610626921860E+02,   -0.833333333333345E+01),
    EpsTriplet<double>(   -0.487954714709552E+02,   -0.347857119953966E+02,   -0.833333333333351E+01),
    EpsTriplet<double>(   -0.122430870339793E+02,   -0.338659743878677E+02,   -0.833333333333334E+01),
  };
  const double ML50_uux_uxugepemA[] = {
    0.136726612603337E-12,
    0.215807458818679E-12,
    0.624848991978500E-13,
    0.113592092146482E-11,
    0.468194703992618E-13,
  };
  const EpsTriplet<double> ML51_uux_uxugepemA[] = {
    EpsTriplet<double>(   -0.115587536695033E+02,   -0.342738214117557E+02,   -0.833333333333316E+01),
    EpsTriplet<double>(    0.328256489953356E+01,   -0.297929162228800E+02,   -0.833333333333324E+01),
    EpsTriplet<double>(    0.204712862463428E+02,   -0.253196554017370E+02,   -0.833333333333337E+01),
    EpsTriplet<double>(   -0.253389560274855E+02,   -0.339864010885884E+02,   -0.833333333333348E+01),
    EpsTriplet<double>(    0.139206828995181E+02,   -0.300542359265970E+02,   -0.833333333333348E+01),
  };

  const double ML50_uux_gggepem[] = {
    0.175788728200411E-11,
    0.191494889398919E-11,
    0.347031793386525E-13,
    0.983662631383924E-12,
    0.626964705048447E-13,
  };
  const EpsTriplet<double> ML51_uux_gggepem[] = {
    EpsTriplet<double>(   -0.909452862979931E+02,   -0.586875844173334E+02,   -0.116666666666667E+02),
    EpsTriplet<double>(   -0.460166838230663E+02,   -0.527204811091416E+02,   -0.116666666666668E+02),
    EpsTriplet<double>(   -0.177021764602699E+02,   -0.386085454025266E+02,   -0.116666666666668E+02),
    EpsTriplet<double>(   -0.584527013373294E+02,   -0.502485630848959E+02,   -0.116666666666668E+02),
    EpsTriplet<double>(   -0.610472419604610E+02,   -0.554827178988803E+02,   -0.116666666666667E+02),
  };

  const double ML50_uux_cxcgepem[] = {
    0.159831663864680E-13,
    0.194250761325306E-12,
    0.684512167855531E-14,
    0.612881428391730E-12,
    0.224336071727995E-15,
  };
  const EpsTriplet<double> ML51_uux_cxcgepem[] = {
    EpsTriplet<double>(   -0.623830354473497E+02,   -0.435467123488888E+02,   -0.833333333333317E+01),
    EpsTriplet<double>(   -0.119381837161207E+02,   -0.299362399243376E+02,   -0.833333333333334E+01),
    EpsTriplet<double>(   -0.125550424745844E+02,   -0.292654269102773E+02,   -0.833333333333345E+01),
    EpsTriplet<double>(   -0.490528432861703E+02,   -0.347884781301401E+02,   -0.833333333333346E+01),
    EpsTriplet<double>(   -0.112754401933783E+02,   -0.338454924827184E+02,   -0.833333333333331E+01),
  };

  const double ML50_uux_dxdgepem[] = {
    0.215513085218702E-13,
    0.179644887142777E-12,
    0.918672595302346E-14,
    0.508116506608127E-12,
    0.306237863762045E-15,
  };
  const EpsTriplet<double> ML51_uux_dxdgepem[] = {
    EpsTriplet<double>(   -0.667133515117397E+02,   -0.436462616629176E+02,   -0.833333333333334E+01),
    EpsTriplet<double>(   -0.113928654148330E+02,   -0.299466130488967E+02,   -0.833333333333335E+01),
    EpsTriplet<double>(   -0.104729905771314E+02,   -0.292576734571919E+02,   -0.833333333333341E+01),
    EpsTriplet<double>(   -0.499313716005706E+02,   -0.347935900095924E+02,   -0.833333333333349E+01),
    EpsTriplet<double>(   -0.129917401464320E+02,   -0.341211926501924E+02,   -0.833333333333333E+01),
  };

  const double ML50_uux_uxugepem[] = {
    0.181291339980982E-12,
    0.416453631805461E-12,
    0.128906488541556E-12,
    0.159029351977504E-11,
    0.242080307275889E-13,
  };
  const EpsTriplet<double> ML51_uux_uxugepem[] = {
    EpsTriplet<double>(   -0.146640776938934E+02,   -0.345314082521349E+02,   -0.833333333333318E+01),
    EpsTriplet<double>(    0.476499455214895E+01,   -0.298100077788201E+02,   -0.833333333333332E+01),
    EpsTriplet<double>(    0.175127808265078E+02,   -0.254085749764958E+02,   -0.833333333333340E+01),
    EpsTriplet<double>(   -0.268816515818101E+02,   -0.339805775560238E+02,   -0.833333333333344E+01),
    EpsTriplet<double>(    0.149766854453463E+02,   -0.299443366886585E+02,   -0.833333333333347E+01),
  };

  Flavour<double> Gstar = StandardModel::Gstar(StandardModel::u(), StandardModel::ubar());
  Flavour<double> W = StandardModel::Wp(StandardModel::u(), StandardModel::ubar(), 1., mW, gW);
  Flavour<double> Zu = StandardModel::Zu(StandardModel::u(), StandardModel::ubar(), mZ, gZ);

  const int namps = 11;
  std::string process[namps] = {
    "# tests for u d~ > g g g W+ > e+ ve",
    "# tests for u d~ > s~ s g W+ > e+ ve",
    "# tests for u d~ > u~ u g W+ > e+ ve",
    "# tests for u d~ > d~ d g W+ > e+ ve",
    "# tests for u u~ > g g g A > e+ e-",
    "# tests for u u~ > c~ c g A > e+ e-",
    "# tests for u u~ > u~ u g A > e+ e-",
    "# tests for u u~ > g g g Z/g* > e+ e-",
    "# tests for u u~ > c~ c g Z/g* > e+ e-",
    "# tests for u u~ > d~ d g Z/g* > e+ e-",
    "# tests for u u~ > u~ u g Z/g* > e+ e-",
  };

  double sqrtS = 1e3;
  NJetAmp<double>* amp[namps] = {
    new Amp2q3gV<double>(W, 1.),
    new Amp4q1gV<double>(W, 1.),
    new Amp4q1gV2b<double>(W, 1.),
    new Amp4q1gV2<double>(W, 1.),
    new Amp2q3gZ<double>(Gstar, 1.),
    new Amp4q1gZ<double>(Gstar, 1.),
    new Amp4q1gZ2<double>(Gstar, 1.),
    new Amp2q3gZ<double>(Zu, 1.),
    new Amp4q1gZ<double>(Zu, 1.),
    new Amp4q1gZd<double>(Zu, 1.),
    new Amp4q1gZ2<double>(Zu, 1.),
  };

  double times[namps][npoints];

  int ord[namps][legs] = {
    {0, 1, 2, 3, 4, 6, 5},
    {0, 1, 2, 3, 4, 6, 5},
    {0, 1, 2, 3, 4, 6, 5},
    {0, 1, 2, 3, 4, 6, 5},
    {0, 1, 2, 3, 4, 6, 5},
    {0, 1, 2, 3, 4, 6, 5},
    {0, 1, 2, 3, 4, 6, 5},
    {0, 1, 2, 3, 4, 6, 5},
    {0, 1, 2, 3, 4, 6, 5},
    {0, 1, 2, 3, 4, 6, 5},
    {0, 1, 2, 3, 4, 6, 5},
  };

  const double Qu = 2./3.;
  double prefactor0[] = {
    pow(ee/se, 4.)*pow(gs, 6.)/(Nc*Nc*4.*6.),
    pow(ee/se, 4.)*pow(gs, 6.)/(Nc*Nc*4.),
    pow(ee/se, 4.)*pow(gs, 6.)/(Nc*Nc*4.),
    pow(ee/se, 4.)*pow(gs, 6.)/(Nc*Nc*4.),
    (Qu*Qu)*pow(ee, 4.)*pow(gs, 6.)/(Nc*Nc*4.*6.),
    (Qu*Qu)*pow(ee, 4.)*pow(gs, 6.)/(Nc*Nc*4.),
    (Qu*Qu)*pow(ee, 4.)*pow(gs, 6.)/(Nc*Nc*4.),
    pow(ee, 4.)*pow(gs, 6.)/(Nc*Nc*4.*6.),
    pow(ee, 4.)*pow(gs, 6.)/(Nc*Nc*4.),
    pow(ee, 4.)*pow(gs, 6.)/(Nc*Nc*4.),
    pow(ee, 4.)*pow(gs, 6.)/(Nc*Nc*4.),
  };

  const double* ML5res0[namps] = {
    ML50_udx_gggepve,
    ML50_udx_sxsgepve,
    ML50_udx_uxugepve,
    ML50_udx_dxdgepve,
    ML50_uux_gggepemA,
    ML50_uux_cxcgepemA,
    ML50_uux_uxugepemA,
    ML50_uux_gggepem,
    ML50_uux_cxcgepem,
    ML50_uux_dxdgepem,
    ML50_uux_uxugepem,
  };
  const EpsTriplet<double>* ML5res1[namps] = {
    ML51_udx_gggepve,
    ML51_udx_sxsgepve,
    ML51_udx_uxugepve,
    ML51_udx_dxdgepve,
    ML51_uux_gggepemA,
    ML51_uux_cxcgepemA,
    ML51_uux_uxugepemA,
    ML51_uux_gggepem,
    ML51_uux_cxcgepem,
    ML51_uux_dxdgepem,
    ML51_uux_uxugepem,
  };

  int nq[namps] = {2, 4, 4, 4, 2, 4, 4, 2, 4, 4, 4};
  int ng[namps] = {3, 1, 1, 1, 3, 1, 1, 3, 1, 1, 1};
  double MuR2 = sqrtS*sqrtS;
  for (int i=0; i<namps; i++) {
    if (not amp[i]) continue;
    cout << process[i] << endl;
    amp[i]->setMuR2(MuR2);
    amp[i]->setNc(Nc);
    amp[i]->setNf(Nf);

    const double prefactor1 = pow(gs,2.)*prefactor0[i]/(16.*M_PI*M_PI);

    for (int pt=0; pt<npoints; pt++) {
      MOM<double> mm[legs];
      for (int j=0; j<legs; j++) {
        mm[j] = MG5moms[pt][ord[i][j]];
      }
      amp[i]->setMomenta(mm);

      tm.start();
      EpsTriplet<double> loop = amp[i]->virt();
      double tree = amp[i]->born();
      loop += getrenorm<double>(ng[i], nq[i], Nc, Nf, tree);
      times[i][pt] = static_cast<double>(tm.get());

      double tree_cc[legs*(legs-1)/2] = {0.};
      amp[i]->born_cc(tree_cc);

      double ans0 = prefactor0[i]*tree;
      cout << "A0    " << ans0 << "\n";
      cout << "ML0   " << ML5res0[i][pt] << " rel. diff("<< (ML5res0[i][pt]-ans0)/ans0 << ")" << endl;
      cout << "A1    " << prefactor1*loop << "\n";
      cout << "poles " << prefactor1*getpoles<double>(ng[i], nq[i], Nc, Nf, mm, MuR2, tree, tree_cc) << "\n";
      EpsTriplet<double> ans1_ratio = loop/tree*0.5;
      cout << "A1/A0 " << ans1_ratio << endl;
      cout << "ML1   " << ML5res1[i][pt] << "\n  diff("<< ML5res1[i][pt]-ans1_ratio << ")" << endl;
    }
    cout << "TIME ";
    print_stat(npoints, times[i]);
    cout << endl;
    cout << endl;
  }

  for (int n=0; n<namps; n++) {
    if (amp[n]) {
      delete amp[n];
      amp[n] = 0;
    }
  }
}
//}}}

//{{{ ppV4j
void ppV4j()
{
  double mW = 80.419002445756163;
  double gW = 2.0476;
  double mZ = 91.188;
  double gZ = 2.441404;
  double se2 = 0.22224648578577766; // sin(theta_EW)^2
  StandardModel::setWmass(mW, gW);
  StandardModel::setZmass(mZ, gZ);
  StandardModel::setSinThetaWSq(se2);

  double Nc = 3.;
  double Nf = 4.;
  double as = 0.118;
  double ae = 7.54677111397888348E-003;
  double gs = sqrt(4.*M_PI*as);
  double ee = sqrt(4.*M_PI*ae);
  double se = sqrt(se2);

  const int legs = 8;
  const int npoints = 5;

  const MOM<double> MG5moms[npoints][legs] = {
    {
      MOM<double>(-0.500000000000000E+03, -0.000000000000000E+00, -0.000000000000000E+00, -0.500000000000000E+03),
      MOM<double>(-0.500000000000000E+03, -0.000000000000000E+00, -0.000000000000000E+00,  0.500000000000000E+03),
      MOM<double>( 0.855312248384887E+02, -0.822193223977868E+01,  0.361637837682033E+02, -0.770725048002414E+02),
      MOM<double>( 0.181428811610043E+03, -0.578599829481937E+02, -0.171863734086635E+03, -0.561185898481311E+01),
      MOM<double>( 0.828493010774356E+02, -0.659095476235891E+02, -0.498952157196287E+02,  0.551413360058664E+01),
      MOM<double>( 0.381470385300815E+03,  0.190185277041519E+03,  0.292042940984587E+03, -0.155113300136598E+03),
      MOM<double>( 0.542314070117999E+02, -0.311330162081798E+02, -0.792796656791140E+01,  0.436912823611163E+02),
      MOM<double>( 0.214488870161418E+03, -0.270607980217775E+02, -0.985198083786150E+02,  0.188592247959950E+03),
    },{
      MOM<double>(-0.500000000000000E+03, -0.000000000000000E+00, -0.000000000000000E+00, -0.500000000000000E+03),
      MOM<double>(-0.500000000000000E+03, -0.000000000000000E+00, -0.000000000000000E+00,  0.500000000000000E+03),
      MOM<double>( 0.142512484904181E+03,  0.627068532890752E+02, -0.117797622129848E+03, -0.500137893455439E+02),
      MOM<double>( 0.157699691589175E+03, -0.999572562609543E+02, -0.117362768021287E+03,  0.332222866534579E+02),
      MOM<double>( 0.147456007565767E+03,  0.100113217019209E+03, -0.461024026015339E+02, -0.979550224320656E+02),
      MOM<double>( 0.686175565650728E+02, -0.326769687124127E+02,  0.139787473176467E+02, -0.586956506749722E+02),
      MOM<double>( 0.356755129080263E+03, -0.940386349317653E+02,  0.333278090933857E+03,  0.857710403850185E+02),
      MOM<double>( 0.126959130295541E+03,  0.638527895968482E+02, -0.659940454988357E+02,  0.876711354141052E+02),
    },{
      MOM<double>(-0.500000000000000E+03, -0.000000000000000E+00, -0.000000000000000E+00, -0.500000000000000E+03),
      MOM<double>(-0.500000000000000E+03, -0.000000000000000E+00, -0.000000000000000E+00,  0.500000000000000E+03),
      MOM<double>( 0.114365378707326E+03, -0.310495400121755E+02, -0.104841653154847E+03,  0.335200488612587E+02),
      MOM<double>( 0.913366925115006E+02, -0.683641065294493E+02,  0.591134458692615E+02,  0.132038197061459E+02),
      MOM<double>( 0.112326140251060E+03,  0.774669972859996E+01,  0.101929846963495E+03,  0.465559526270731E+02),
      MOM<double>( 0.326314507269921E+03,  0.129346883158691E+03, -0.220356095864801E+03,  0.202962391804899E+03),
      MOM<double>( 0.300927182883798E+03, -0.475590273434313E+02,  0.115170648347043E+03, -0.273917925802323E+03),
      MOM<double>( 0.547300983763950E+02,  0.987909099776504E+01,  0.489838078398485E+02, -0.223242871970539E+02),
    },{
      MOM<double>(-0.500000000000000E+03, -0.000000000000000E+00, -0.000000000000000E+00, -0.500000000000000E+03),
      MOM<double>(-0.500000000000000E+03, -0.000000000000000E+00, -0.000000000000000E+00,  0.500000000000000E+03),
      MOM<double>( 0.243711863908368E+03, -0.182418653739825E+03, -0.916363786236030E+02,  0.133122806048372E+03),
      MOM<double>( 0.155076610776720E+03, -0.298810245402782E+02, -0.126156699107185E+03, -0.850903452384391E+02),
      MOM<double>( 0.176947984257084E+03,  0.696960649045733E+02, -0.710538071839474E+02, -0.146302440697786E+03),
      MOM<double>( 0.106992928543509E+03,  0.754126861046720E+02,  0.626124576071744E+02,  0.428963131887164E+02),
      MOM<double>( 0.218407396303277E+03,  0.773368290710507E+02,  0.200398406914265E+03, -0.395130881536310E+02),
      MOM<double>( 0.988632162110418E+02, -0.101459018001928E+02,  0.258360203932959E+02,  0.948867548527678E+02),
    },{
      MOM<double>(-0.500000000000000E+03, -0.000000000000000E+00, -0.000000000000000E+00, -0.500000000000000E+03),
      MOM<double>(-0.500000000000000E+03, -0.000000000000000E+00, -0.000000000000000E+00,  0.500000000000000E+03),
      MOM<double>( 0.960382450700918E+02, -0.259742804793425E+02, -0.126714972949460E+02, -0.915866498242382E+02),
      MOM<double>( 0.223161583403806E+02, -0.270691766779795E+01,  0.190831370689339E+02,  0.112479953512163E+02),
      MOM<double>( 0.287806431902743E+03,  0.249778946314500E+03,  0.117301887074030E+03,  0.817513762054070E+02),
      MOM<double>( 0.140483066834692E+03,  0.133065560863879E+03, -0.268430018410707E+02,  0.361732198099961E+02),
      MOM<double>( 0.408718571084787E+03, -0.387854197318346E+03, -0.113026916873262E+03, -0.620073224245013E+02),
      MOM<double>( 0.446375267673055E+02,  0.336908882871081E+02,  0.161563918663151E+02,  0.244213808821201E+02),
    }
  };

  const double ML50_udx_sxsggepve[] = {
    0.801318883101803E-15,
    0.563324563268324E-16,
    0.107508829580660E-14,
    0.160131242011707E-16,
    0.176066972395041E-15,
  };
  const EpsTriplet<double> ML51_udx_sxsggepve[] = {
    EpsTriplet<double>(   -0.517630461000285E+02,   -0.503020433721874E+02,   -0.113333333333336E+02),
    EpsTriplet<double>(   -0.717214909624771E+02,   -0.587349115128235E+02,   -0.113333333333329E+02),
    EpsTriplet<double>(   -0.264587341117479E+02,   -0.478964282465181E+02,   -0.113333333333330E+02),
    EpsTriplet<double>(   -0.272567091212605E+02,   -0.479091072263666E+02,   -0.113333333333334E+02),
    EpsTriplet<double>(   -0.717363948769710E+02,   -0.568522161962970E+02,   -0.113333333333335E+02),
  };

  const double ML50_udx_dxdggepve[] = {
    0.295940509793629E-12,
    0.108199426222970E-15,
    0.119866023269639E-14,
    0.181598456918922E-16,
    0.843490637512056E-15,
  };
  const EpsTriplet<double> ML51_udx_dxdggepve[] = {
    EpsTriplet<double>(   -0.192200914230423E+02,   -0.470554379371587E+02,   -0.113333333333335E+02),
    EpsTriplet<double>(   -0.534939516896298E+02,   -0.567525737181903E+02,   -0.113333333333330E+02),
    EpsTriplet<double>(   -0.254966066182131E+02,   -0.478338163627090E+02,   -0.113333333333331E+02),
    EpsTriplet<double>(   -0.268884095267405E+02,   -0.484117315315052E+02,   -0.113333333333334E+02),
    EpsTriplet<double>(   -0.283264537306809E+02,   -0.508697562208510E+02,   -0.113333333333331E+02),
  };

  const double ML50_udx_uxuggepve[] = {
    0.141799119285090E-14,
    0.113115380621354E-15,
    0.394719790568295E-14,
    0.348603005165428E-16,
    0.251330947721312E-15,
  };
  const EpsTriplet<double> ML51_udx_uxuggepve[] = {
    EpsTriplet<double>(   -0.406981363263293E+02,   -0.493105563136728E+02,   -0.113333333333333E+02),
    EpsTriplet<double>(   -0.593944959821845E+02,   -0.569525119245734E+02,   -0.113333333333331E+02),
    EpsTriplet<double>(   -0.670470954242664E+01,   -0.471528784564456E+02,   -0.113333333333331E+02),
    EpsTriplet<double>(   -0.226565354124127E+02,   -0.494988337905963E+02,   -0.113333333333333E+02),
    EpsTriplet<double>(   -0.640359402173479E+02,   -0.541968426202120E+02,   -0.113333333333331E+02),
  };

  const double ML50_udx_sxscxcepve[] = {
    1.1040622692672241E-017,
    1.1194614384916828E-018,
    3.7941647980730374E-017,
    8.6800321559532048E-020,
    7.2670746266287411E-018,
  };
  const EpsTriplet<double> ML51_udx_sxscxcepve[] = {
    EpsTriplet<double>(   -0.213594657309862E+02,   -0.343346770996611E+02,   -0.800000000000038E+01),
    EpsTriplet<double>(   -0.148806372138429E+02,   -0.368400342723345E+02,   -0.800000000000025E+01),
    EpsTriplet<double>(   -0.102708763532512E+02,   -0.335598859389414E+02,   -0.799999999999965E+01),
    EpsTriplet<double>(    0.431712030279682E+01,   -0.310316668549278E+02,   -0.799999999999988E+01),
    EpsTriplet<double>(   -0.382653575605792E+02,   -0.391774535815008E+02,   -0.799999999999966E+01),
  };
  const double ML50_udx_sxssxsepve[] = {
    0.442350907408725E-16,
    0.377578218218646E-18,
    0.550854794745401E-16,
    0.465787256791831E-18,
    0.207525929685287E-17,
  };
  const EpsTriplet<double> ML51_udx_sxssxsepve[] = {
    EpsTriplet<double>(   -0.117043152589550E+02,   -0.320513819407658E+02,   -0.800000000000007E+01),
    EpsTriplet<double>(   -0.167554694692608E+02,   -0.370874432713068E+02,   -0.800000000000024E+01),
    EpsTriplet<double>(    0.375922681870472E+01,   -0.300767525996502E+02,   -0.799999999999990E+01),
    EpsTriplet<double>(   -0.886739705167051E+01,   -0.350709705417209E+02,   -0.800000000000001E+01),
    EpsTriplet<double>(   -0.420849633234995E+02,   -0.401265845226604E+02,   -0.799999999999980E+01),
  };
  const double ML50_udx_dxdcxcepve[] = {
    0.468323690736062E-15,
    0.173100913419703E-17,
    0.394474337512213E-16,
    0.974376665625653E-19,
    0.236807316952679E-16,
  };
  const EpsTriplet<double> ML51_udx_dxdcxcepve[] = {
    EpsTriplet<double>(   -0.689304900280042E+01,   -0.335639712671417E+02,   -0.800000000000002E+01),
    EpsTriplet<double>(   -0.448554241665524E+01,   -0.359356670466522E+02,   -0.800000000000036E+01),
    EpsTriplet<double>(   -0.949164271933764E+01,   -0.335153898411879E+02,   -0.799999999999964E+01),
    EpsTriplet<double>(    0.625202041459405E+01,   -0.312366713637768E+02,   -0.799999999999988E+01),
    EpsTriplet<double>(   -0.100880285013657E+01,   -0.338455549954682E+02,   -0.799999999999957E+01),
  };
  const double ML50_udx_dxddxdepve[] = {
    0.102832484567048E-14,
    0.808370319481956E-18,
    0.547050189589898E-16,
    0.119380501454295E-17,
    0.123236491330595E-16,
  };
  const EpsTriplet<double> ML51_udx_dxddxdepve[] = {
    EpsTriplet<double>(    0.138814763930723E+02,   -0.321092336186643E+02,   -0.799999999999995E+01),
    EpsTriplet<double>(   -0.306227894024144E+01,   -0.355169023005319E+02,   -0.800000000000014E+01),
    EpsTriplet<double>(    0.191081625333874E+01,   -0.303687431464420E+02,   -0.799999999999991E+01),
    EpsTriplet<double>(    0.412206363233707E+01,   -0.326885378017469E+02,   -0.800000000000000E+01),
    EpsTriplet<double>(   -0.118720445895805E+01,   -0.341804363873792E+02,   -0.800000000000010E+01),
  };
  const double ML50_udx_uxucxcepve[] = {
    0.120020722889003E-16,
    0.173811036918797E-17,
    0.576704434350406E-16,
    0.149785001733982E-18,
    0.965542820148765E-17,
  };
  const EpsTriplet<double> ML51_udx_uxucxcepve[] = {
    EpsTriplet<double>(   -0.178141669126697E+02,   -0.336977178558873E+02,   -0.800000000000039E+01),
    EpsTriplet<double>(   -0.870393441300811E+01,   -0.361302008510984E+02,   -0.800000000000011E+01),
    EpsTriplet<double>(   -0.261375953960378E-01,   -0.331940972934492E+02,   -0.799999999999974E+01),
    EpsTriplet<double>(    0.498833726106816E+01,   -0.325957181843501E+02,   -0.799999999999992E+01),
    EpsTriplet<double>(   -0.276400307695251E+02,   -0.369273610323348E+02,   -0.799999999999986E+01),
  };
  const double ML50_udx_uxuuxuepve[] = {
    0.437222088130779E-16,
    0.622697449031989E-18,
    0.747108966897553E-15,
    0.927162056282905E-18,
    0.468187423027337E-17,
  };
  const EpsTriplet<double> ML51_udx_uxuuxuepve[] = {
    EpsTriplet<double>(   -0.124663969994340E+02,   -0.321957985365125E+02,   -0.800000000000038E+01),
    EpsTriplet<double>(   -0.961505374000528E+01,   -0.360183503789054E+02,   -0.800000000000021E+01),
    EpsTriplet<double>(    0.115090369650494E+02,   -0.322871867840249E+02,   -0.799999999999991E+01),
    EpsTriplet<double>(    0.889748342941884E+01,   -0.331967780939149E+02,   -0.799999999999994E+01),
    EpsTriplet<double>(   -0.372468674243094E+02,   -0.402112862491418E+02,   -0.800000000000207E+01),
  };
  const double ML50_udx_dxduxuepve[] = {
    0.465590519866933E-15,
    0.179557111163866E-17,
    0.129692047852380E-14,
    0.195014852452016E-18,
    0.334248187973223E-16,
  };
  const EpsTriplet<double> ML51_udx_dxduxuepve[] = {
    EpsTriplet<double>(   -0.841450585605408E+01,   -0.337026388468914E+02,   -0.800000000000025E+01),
    EpsTriplet<double>(   -0.494764182307869E+01,   -0.360267815203671E+02,   -0.800000000000025E+01),
    EpsTriplet<double>(    0.163371015777327E+02,   -0.312852786560044E+02,   -0.799999999999984E+01),
    EpsTriplet<double>(    0.153053060011440E+02,   -0.303158311260512E+02,   -0.799999999999998E+01),
    EpsTriplet<double>(   -0.644243400780515E+01,   -0.357707399629467E+02,   -0.800000000000122E+01),
  };
  const double ML50_udx_ggggepve[] = {
    0.666594863110246E-13,
    0.368161637727752E-15,
    0.160335215872594E-13,
    0.900040498873078E-16,
    0.512190418948887E-13,
  };
  const EpsTriplet<double> ML51_udx_ggggepve[] = {
    EpsTriplet<double>(   -0.110557402332419E+03,   -0.699491483965170E+02,   -0.146666666666669E+02),
    EpsTriplet<double>(   -0.899198180689351E+02,   -0.735876444278290E+02,   -0.146666666666672E+02),
    EpsTriplet<double>(   -0.677670301747973E+02,   -0.649936141555878E+02,   -0.146666666666667E+02),
    EpsTriplet<double>(   -0.467057900182095E+02,   -0.632651275090085E+02,   -0.146666666666667E+02),
    EpsTriplet<double>(   -0.164600563901598E+03,   -0.850629865504956E+02,   -0.146666666666669E+02),
    EpsTriplet<double>(   -0.110560368273485E+03,   -0.699491483965197E+02,   -0.146666666666670E+02),
  };

  const double ML50_uux_ggggepem[] = {
    0.830462079664994E-14,
    0.172944796692993E-15,
    0.677870264352413E-14,
    0.349524189943924E-16,
    0.212991083031345E-13,
  };
  const EpsTriplet<double> ML51_uux_ggggepem[] = {
    EpsTriplet<double>(   -0.108582703402679E+03,   -0.699353915498041E+02,   -0.146666666666662E+02),
    EpsTriplet<double>(   -0.897113760077144E+02,   -0.735377541933529E+02,   -0.146666666666670E+02),
    EpsTriplet<double>(   -0.672590964373835E+02,   -0.649970406155596E+02,   -0.146666666666665E+02),
    EpsTriplet<double>(   -0.475314918193304E+02,   -0.632563145710780E+02,   -0.146666666666666E+02),
    EpsTriplet<double>(   -0.164451851596096E+03,   -0.850624157913074E+02,   -0.146666666666668E+02),
  };

  const double ML50_uux_cxcggepem[] = {
    0.167899984927919E-15,
    0.273244518449383E-16,
    0.535092655210627E-15,
    0.786982359057438E-17,
    0.701246340338453E-16,
  };
  const EpsTriplet<double> ML51_uux_cxcggepem[] = {
    EpsTriplet<double>(   -0.514971253586933E+02,   -0.506957340502218E+02,   -0.113333333333333E+02),
    EpsTriplet<double>(   -0.697949929745121E+02,   -0.587524062272763E+02,   -0.113333333333331E+02),
    EpsTriplet<double>(   -0.285291549616695E+02,   -0.480657027046915E+02,   -0.113333333333332E+02),
    EpsTriplet<double>(   -0.297807517289755E+02,   -0.479301185183702E+02,   -0.113333333333333E+02),
    EpsTriplet<double>(   -0.714513709898803E+02,   -0.569100146543955E+02,   -0.113333333333334E+02)
  };
  const double ML50_uux_dxdggepem[] = {
    0.882529541087756E-16,
    0.264312147451072E-16,
    0.418106450812153E-15,
    0.602014147795429E-17,
    0.755567598109355E-16,
  };
  const EpsTriplet<double> ML51_uux_dxdggepem[] = {
    EpsTriplet<double>(   -0.489586473975993E+02,   -0.503432399751651E+02,   -0.113333333333332E+02),
    EpsTriplet<double>(   -0.681709374572944E+02,   -0.587638614620553E+02,   -0.113333333333336E+02),
    EpsTriplet<double>(   -0.255031313551379E+02,   -0.478729627812250E+02,   -0.113333333333330E+02),
    EpsTriplet<double>(   -0.298123648778360E+02,   -0.478990096303600E+02,   -0.113333333333333E+02),
    EpsTriplet<double>(   -0.714826672500340E+02,   -0.568283435341183E+02,   -0.113333333333335E+02),
  };
  const double ML50_uux_uxuggepem[] = {
    0.421959407672558E-13,
    0.105518124108461E-15,
    0.211882376801090E-14,
    0.196697162040398E-16,
    0.370535685057901E-15,
  };
  const EpsTriplet<double> ML51_uux_uxuggepem[] = {
    EpsTriplet<double>(   -0.165171908277730E+02,   -0.471489790568643E+02,   -0.113333333333328E+02),
    EpsTriplet<double>(   -0.511269462209876E+02,   -0.559488221659668E+02,   -0.113333333333333E+02),
    EpsTriplet<double>(   -0.574205573507387E+01,   -0.471626154611109E+02,   -0.113333333333331E+02),
    EpsTriplet<double>(   -0.235490525623283E+02,   -0.496883878007492E+02,   -0.113333333333333E+02),
    EpsTriplet<double>(   -0.282063022985138E+02,   -0.505962020241898E+02,   -0.113333333333334E+02),
  };

  const double ML50_ddx_sxsbxbepem[] = {
    0.713727312645217E-18,
    0.303113724460874E-18,
    0.243727219713023E-16,
    0.229339353499610E-19,
    0.171123556288270E-17,
  };
  const EpsTriplet<double> ML51_ddx_sxsbxbepem[] = {
    EpsTriplet<double>(   -0.351049220149804E+02,   -0.343513406669945E+02,   -0.799999999999552E+01),
    EpsTriplet<double>(   -0.214966408455747E+02,   -0.368367515146468E+02,   -0.800000000000037E+01),
    EpsTriplet<double>(   -0.190859334143671E+02,   -0.333868744849025E+02,   -0.799999999999949E+01),
    EpsTriplet<double>(   -0.584909443852408E+01,   -0.310940463886519E+02,   -0.799999999999999E+01),
    EpsTriplet<double>(   -0.468379750193798E+02,   -0.392424518529333E+02,   -0.799999999999870E+01),
  };
  const double ML50_uux_dxdcxcepem[] = {
    0.857877332617655E-18,
    0.518711094431428E-18,
    0.207228569831085E-16,
    0.312137831454280E-19,
    0.309196926055646E-17,
  };
  const EpsTriplet<double> ML51_uux_dxdcxcepem[] = {
    EpsTriplet<double>(   -0.252713258649356E+02,   -0.349297899867398E+02,   -0.799999999999681E+01),
    EpsTriplet<double>(   -0.117976382241398E+02,   -0.368461684167099E+02,   -0.800000000000018E+01),
    EpsTriplet<double>(   -0.109454533865007E+02,   -0.332796974172131E+02,   -0.799999999999963E+01),
    EpsTriplet<double>(    0.723232321655009E+00,   -0.311927055979782E+02,   -0.800000000000000E+01),
    EpsTriplet<double>(   -0.380045653850212E+02,   -0.391185517443546E+02,   -0.800000000000063E+01),
  };
  const double ML50_uux_uxucxcepem[] = {
    0.436158494756149E-16,
    0.142157611437210E-17,
    0.362266798424936E-16,
    0.843572723239465E-19,
    0.173566522317808E-16,
  };
  const EpsTriplet<double> ML51_uux_uxucxcepem[] = {
    EpsTriplet<double>(    0.207260184308311E+00,   -0.331445769773098E+02,   -0.799999999999935E+01),
    EpsTriplet<double>(   -0.219992988438417E+01,   -0.355497966547841E+02,   -0.799999999999994E+01),
    EpsTriplet<double>(   -0.488407569408353E+01,   -0.332105335720036E+02,   -0.799999999999964E+01),
    EpsTriplet<double>(    0.481688022396514E+01,   -0.329572118078572E+02,   -0.800000000000000E+01),
    EpsTriplet<double>(    0.397093625385186E+01,   -0.328057934499768E+02,   -0.799999999999912E+01),
  };
  const double ML50_uux_uxudxdepem[] = {
    0.784433205661285E-16,
    0.136293980064297E-17,
    0.246819873829653E-16,
    0.908869193887442E-19,
    0.105314558875347E-16,
  };
  const EpsTriplet<double> ML51_uux_uxudxdepem[] = {
    EpsTriplet<double>(   -0.469899291493284E+01,   -0.334965985026957E+02,   -0.799999999999943E+01),
    EpsTriplet<double>(   -0.170978370552145E+01,   -0.354677705943153E+02,   -0.800000000000007E+01),
    EpsTriplet<double>(    0.376030442832424E+01,   -0.331171120308955E+02,   -0.799999999999964E+01),
    EpsTriplet<double>(    0.541240782727992E+01,   -0.326628375612171E+02,   -0.800000000000004E+01),
    EpsTriplet<double>(    0.235310759563128E+01,   -0.335252093418884E+02,   -0.799999999999895E+01),
  };
  const double ML50_uux_uxuuxuepem[] = {
    0.132828756750053E-15,
    0.688787792785236E-18,
    0.354503749765313E-15,
    0.867381742167771E-18,
    0.862239436088603E-17,
  };
  const EpsTriplet<double> ML51_uux_uxuuxuepem[] = {
    EpsTriplet<double>(    0.903370787833202E+01,   -0.319087997156145E+02,   -0.799999999999954E+01),
    EpsTriplet<double>(   -0.468066387428168E-01,   -0.349621761881102E+02,   -0.800000000000008E+01),
    EpsTriplet<double>(    0.118058285437717E+02,   -0.322384143114778E+02,   -0.799999999999984E+01),
    EpsTriplet<double>(    0.147578729880113E+02,   -0.319141741996268E+02,   -0.800000000000001E+01),
    EpsTriplet<double>(   -0.203408498027078E+01,   -0.347571443250118E+02,   -0.800000000000083E+01),
  };
  const double ML50_uux_dxdsxsepem[] = {
    0.218442193723769E-17,
    0.512195713309153E-18,
    0.106472983857989E-16,
    0.364192969223439E-19,
    0.304043720472100E-17,
  };
  const EpsTriplet<double> ML51_uux_dxdsxsepem[] = {
    EpsTriplet<double>(   -0.267581202517398E+02,   -0.348723747014952E+02,   -0.800000000000267E+01),
    EpsTriplet<double>(   -0.115611195723257E+02,   -0.368356578971086E+02,   -0.800000000000015E+01),
    EpsTriplet<double>(   -0.675031781468561E+01,   -0.337784982047074E+02,   -0.799999999999943E+01),
    EpsTriplet<double>(    0.197136011663010E+01,   -0.309302139354748E+02,   -0.800000000000006E+01),
    EpsTriplet<double>(   -0.372141403994176E+02,   -0.391411478457126E+02,   -0.799999999999940E+01),
  };

  Flavour<double> W = StandardModel::Wp(StandardModel::u(), StandardModel::ubar(), 1., mW, gW);
  Flavour<double> Zu = StandardModel::Zu(StandardModel::u(), StandardModel::ubar(), mZ, gZ);
  Flavour<double> Zd = StandardModel::Zd(StandardModel::u(), StandardModel::ubar(), mZ, gZ);

  const int namps = 21;
  std::string process[namps] = {
    "# tests for u d~ > g g g g W+ > e+ ve",
    "# tests for u d~ > s~ s g g W+ > e+ ve",
    "# tests for u d~ > d~ d g g W+ > e+ ve",
    "# tests for u d~ > u~ u g g W+ > e+ ve",
    "# tests for u d~ > s~ s c~ c W+ > e+ ve",
    "# tests for u d~ > s~ s s~ s W+ > e+ ve",
    "# tests for u d~ > d~ d c~ c W+ > e+ ve",
    "# tests for u d~ > d~ d d~ d W+ > e+ ve",
    "# tests for u d~ > u~ u c~ c W+ > e+ ve",
    "# tests for u d~ > u~ u u~ u W+ > e+ ve",
    "# tests for u d~ > d~ d u~ u W+ > e+ ve",
    "# tests for u u~ > g g g g Z/g* > e+ e-",
    "# tests for u u~ > c~ c g g Z/g* > e+ e-",
    "# tests for u u~ > d~ d g g Z/g* > e+ e-",
    "# tests for u u~ > u~ u g g Z/g* > e+ e-",
    "# tests for d d~ > s~ s b~ b Z/g* > e+ e-",
    "# tests for u u~ > d~ d c~ c Z/g* > e+ e-",
    "# tests for u u~ > u~ u c~ c Z/g* > e+ e-",
    "# tests for u u~ > u~ u d~ d Z/g* > e+ e-",
    "# tests for u u~ > u~ u u~ u Z/g* > e+ e-",
    // u<->d flip for Z amps
    "# tests for u u~ > d~ d s~ s Z/g* > e+ e-",
  };

  double sqrtS = 1e3;
  NJetAmp<double>* amp[namps] = {
    new Amp2q4gV<double>(W, 1.),
    new Amp4q2gV<double>(W, 1.),
    new Amp4q2gV2<double>(W, 1.),
    new Amp4q2gV2b<double>(W, 1.),
    new Amp6q0gV<double>(W, 1.),
    new Amp6q0gV2<double>(W, 1.),
    new Amp6q0gV2n<double>(W, 1.),
    new Amp6q0gV6n<double>(W, 1.),
    new Amp6q0gV2b<double>(W, 1.),
    new Amp6q0gV6b<double>(W, 1.),
    new Amp6q0gV4nb<double>(W, 1.),
    new Amp2q4gZ<double>(Zu, 1.),
    new Amp4q2gZ<double>(Zu, 1.),
    new Amp4q2gZd<double>(Zu, 1.),
    new Amp4q2gZ2<double>(Zu, 1.),
    new Amp6q0gZ<double>(Zd, 1.),
    new Amp6q0gZd<double>(Zu, 1.),
    new Amp6q0gZ2<double>(Zu, 1.),
    new Amp6q0gZ2d<double>(Zu, 1.),
    new Amp6q0gZ6<double>(Zu, 1.),
    // u<->d flip for Z amps
    new Amp6q0gZd<double>(Zd, 1.),
  };

  double times[namps][npoints];

  int ord[namps][legs] = {
    {0, 1, 2, 3, 4, 5, 7, 6},
    {0, 1, 2, 3, 4, 5, 7, 6},
    {0, 1, 2, 3, 4, 5, 7, 6},
    {0, 1, 2, 3, 4, 5, 7, 6},
    {0, 1, 2, 3, 4, 5, 7, 6},
    {0, 1, 2, 3, 4, 5, 7, 6},
    {0, 1, 2, 3, 4, 5, 7, 6},
    {0, 1, 2, 3, 4, 5, 7, 6},
    {0, 1, 2, 3, 4, 5, 7, 6},
    {0, 1, 2, 3, 4, 5, 7, 6},
    {0, 1, 2, 3, 4, 5, 7, 6},
    {0, 1, 2, 3, 4, 5, 7, 6},
    {0, 1, 2, 3, 4, 5, 7, 6},
    {0, 1, 2, 3, 4, 5, 7, 6},
    {0, 1, 2, 3, 4, 5, 7, 6},
    {0, 1, 2, 3, 4, 5, 7, 6},
    {0, 1, 4, 5, 2, 3, 7, 6},
    {0, 1, 2, 3, 4, 5, 7, 6},
    {0, 1, 2, 3, 4, 5, 7, 6},
    {0, 1, 2, 3, 4, 5, 7, 6},
    // u<->d flip for Z amps
    {4, 5, 2, 3, 0, 1, 7, 6},
  };

  double prefactor0[] = {
    pow(ee/se, 4.)*pow(gs, 8.)/(Nc*Nc*4.*24.),
    pow(ee/se, 4.)*pow(gs, 8.)/(Nc*Nc*4.*2.),
    pow(ee/se, 4.)*pow(gs, 8.)/(Nc*Nc*4.*2.),
    pow(ee/se, 4.)*pow(gs, 8.)/(Nc*Nc*4.*2.),
    pow(ee/se, 4.)*pow(gs, 8.)/(Nc*Nc*4.),
    pow(ee/se, 4.)*pow(gs, 8.)/(Nc*Nc*4.*4),
    pow(ee/se, 4.)*pow(gs, 8.)/(Nc*Nc*4.),
    pow(ee/se, 4.)*pow(gs, 8.)/(Nc*Nc*4.*4),
    pow(ee/se, 4.)*pow(gs, 8.)/(Nc*Nc*4.),
    pow(ee/se, 4.)*pow(gs, 8.)/(Nc*Nc*4.*4),
    pow(ee/se, 4.)*pow(gs, 8.)/(Nc*Nc*4.),
    pow(ee, 4.)*pow(gs, 8.)/(Nc*Nc*4.*24.),
    pow(ee, 4.)*pow(gs, 8.)/(Nc*Nc*4.*2.),
    pow(ee, 4.)*pow(gs, 8.)/(Nc*Nc*4.*2.),
    pow(ee, 4.)*pow(gs, 8.)/(Nc*Nc*4.*2.),
    pow(ee, 4.)*pow(gs, 8.)/(Nc*Nc*4.),
    pow(ee, 4.)*pow(gs, 8.)/(Nc*Nc*4.),
    pow(ee, 4.)*pow(gs, 8.)/(Nc*Nc*4.),
    pow(ee, 4.)*pow(gs, 8.)/(Nc*Nc*4.),
    pow(ee, 4.)*pow(gs, 8.)/(Nc*Nc*4.*4.),
    // u<->d flip for Z amps
    pow(ee, 4.)*pow(gs, 8.)/(Nc*Nc*4.),
  };

  const double* ML5res0[namps] = {
    ML50_udx_ggggepve,
    ML50_udx_sxsggepve,
    ML50_udx_dxdggepve,
    ML50_udx_uxuggepve,
    ML50_udx_sxscxcepve,
    ML50_udx_sxssxsepve,
    ML50_udx_dxdcxcepve,
    ML50_udx_dxddxdepve,
    ML50_udx_uxucxcepve,
    ML50_udx_uxuuxuepve,
    ML50_udx_dxduxuepve,
    ML50_uux_ggggepem,
    ML50_uux_cxcggepem,
    ML50_uux_dxdggepem,
    ML50_uux_uxuggepem,
    ML50_ddx_sxsbxbepem,
    ML50_uux_dxdcxcepem,
    ML50_uux_uxucxcepem,
    ML50_uux_uxudxdepem,
    ML50_uux_uxuuxuepem,
    // u<->d flip for Z amps
    ML50_uux_dxdsxsepem,
  };
  const EpsTriplet<double>* ML5res1[namps] = {
    ML51_udx_ggggepve,
    ML51_udx_sxsggepve,
    ML51_udx_dxdggepve,
    ML51_udx_uxuggepve,
    ML51_udx_sxscxcepve,
    ML51_udx_sxssxsepve,
    ML51_udx_dxdcxcepve,
    ML51_udx_dxddxdepve,
    ML51_udx_uxucxcepve,
    ML51_udx_uxuuxuepve,
    ML51_udx_dxduxuepve,
    ML51_uux_ggggepem,
    ML51_uux_cxcggepem,
    ML51_uux_dxdggepem,
    ML51_uux_uxuggepem,
    ML51_ddx_sxsbxbepem,
    ML51_uux_dxdcxcepem,
    ML51_uux_uxucxcepem,
    ML51_uux_uxudxdepem,
    ML51_uux_uxuuxuepem,
    // u<->d flip for Z amps
    ML51_uux_dxdsxsepem,
  };

  int nq[namps] = {2, 4, 4, 4, 6, 6, 6, 6, 6, 6, 6, 2, 4, 4, 4, 6, 6, 6, 6, 6, 6};
  int ng[namps] = {4, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 4, 2, 2, 2, 0, 0, 0, 0, 0, 0};
  double MuR2 = sqrtS*sqrtS;
  for (int i=0; i<namps; i++) {
    if (not amp[i]) continue;
    cout << process[i] << endl;
    amp[i]->setMuR2(MuR2);
    amp[i]->setNc(Nc);
    amp[i]->setNf(Nf);
    double xNf = Nf;
    if (i==15) {
      amp[15]->setNf(5.);
      xNf = 5.;
    }

    const double prefactor1 = pow(gs,2.)*prefactor0[i]/(16.*M_PI*M_PI);

    for (int pt=0; pt<npoints; pt++) {
      MOM<double> mm[legs];
      for (int j=0; j<legs; j++) {
        mm[j] = MG5moms[pt][ord[i][j]];
      }
      amp[i]->setMomenta(mm);

      tm.start();
      EpsTriplet<double> loop = amp[i]->virt();
      double tree = amp[i]->born();
      loop += getrenorm<double>(ng[i], nq[i], Nc, xNf, tree);
      times[i][pt] = static_cast<double>(tm.get());

      double tree_cc[legs*(legs-1)/2] = {0.};
      amp[i]->born_cc(tree_cc);

      double ans0 = prefactor0[i]*tree;
      cout << "A0    " << ans0 << "\n";
      cout << "ML0   " << ML5res0[i][pt] << " rel. diff("<< (ML5res0[i][pt]-ans0)/ans0 << ")" << endl;
      cout << "A1    " << prefactor1*loop << "\n";
      cout << "poles " << prefactor1*getpoles<double>(ng[i], nq[i], Nc, xNf, mm, MuR2, tree, tree_cc) << "\n";
      EpsTriplet<double> ans1_ratio = loop/tree*0.5;
      cout << "A1/A0 " << ans1_ratio << endl;
      cout << "ML1   " << ML5res1[i][pt] << "\n  diff("<< ML5res1[i][pt]-ans1_ratio << ")" << endl;
    }
    cout << "TIME ";
    print_stat(npoints, times[i]);
    cout << endl;
    cout << endl;
  }

  for (int n=0; n<namps; n++) {
    if (amp[n]) {
      delete amp[n];
      amp[n] = 0;
    }
  }
}
//}}}

//{{{ ppV5j
void ppV5j()
{
#ifdef NJET_ENABLE_5
  double mW = 80.419002445756163;
  double gW = 2.0476;
  double mZ = 91.188;
  double gZ = 2.441404;
  double se2 = 0.22224648578577766; // sin(theta_EW)^2
  StandardModel::setWmass(mW, gW);
  StandardModel::setZmass(mZ, gZ);
  StandardModel::setSinThetaWSq(se2);

  double Nc = 3.;
  double Nf = 4.;
  double as = 0.118;
  double ae = 7.54677111397888348E-003;
  double gs = sqrt(4.*M_PI*as);
  double ee = sqrt(4.*M_PI*ae);
  double se = sqrt(se2);

  const int legs = 9;
  const int npoints = 5;

  const MOM<double> MG5moms[npoints][legs] = {
    {
      MOM<double>(-0.500000000000000E+03,  0.000000000000000E+00,  0.000000000000000E+00, -0.500000000000000E+03),
      MOM<double>(-0.500000000000000E+03,  0.000000000000000E+00,  0.000000000000000E+00,  0.500000000000000E+03),
      MOM<double>( 0.728724247011202E+02, -0.116934135222199E+02,  0.397888035214641E+02, -0.599208267328575E+02),
      MOM<double>( 0.143846839649468E+03, -0.580194050700902E+02, -0.131478340772374E+03,  0.625362456467995E+01),
      MOM<double>( 0.710354311670830E+02, -0.607262108079718E+02, -0.353827125731215E+02,  0.103161743377581E+02),
      MOM<double>( 0.339320292356292E+03,  0.142305265922241E+03,  0.290082203738676E+03, -0.103632944422479E+03),
      MOM<double>( 0.510984252803030E+02, -0.295988212233645E+02, -0.191510389563769E+01,  0.416087878384234E+02),
      MOM<double>( 0.191820266071985E+03, -0.338212329032360E+02, -0.656538124964758E+02,  0.177033091780915E+03),
      MOM<double>( 0.130006320773749E+03,  0.515538176046410E+02, -0.954410375225311E+02, -0.716579073664396E+02),
    },{
      MOM<double>(-0.500000000000000E+03,  0.000000000000000E+00,  0.000000000000000E+00, -0.500000000000000E+03),
      MOM<double>(-0.500000000000000E+03,  0.000000000000000E+00,  0.000000000000000E+00,  0.500000000000000E+03),
      MOM<double>( 0.123297420499674E+03, -0.563233847105529E+02, -0.109509580352751E+03,  0.613041985386980E+01),
      MOM<double>( 0.168869892904542E+03,  0.124502306261835E+03, -0.505827818674322E+02, -0.102262400928335E+03),
      MOM<double>( 0.624692118408597E+02, -0.114867675719600E+02,  0.715824540789534E+01, -0.609853762917498E+02),
      MOM<double>( 0.268422550399921E+03, -0.165773142314150E+02,  0.266946109110419E+03,  0.227075548414983E+02),
      MOM<double>( 0.117771702890737E+03,  0.782484410266892E+02, -0.634724062137133E+02,  0.609803995404928E+02),
      MOM<double>( 0.142613365449090E+03, -0.352940726996950E+02, -0.128846001601895E+03,  0.499160125429567E+02),
      MOM<double>( 0.116555856015177E+03, -0.830692080749017E+02,  0.783064155174777E+02,  0.235133904412668E+02),
    },{
      MOM<double>(-0.500000000000000E+03,  0.000000000000000E+00,  0.000000000000000E+00, -0.500000000000000E+03),
      MOM<double>(-0.500000000000000E+03,  0.000000000000000E+00,  0.000000000000000E+00,  0.500000000000000E+03),
      MOM<double>( 0.102461061370725E+03, -0.213715379851305E+01,  0.988971196098230E+02,  0.267032096149016E+02),
      MOM<double>( 0.240534362898007E+03,  0.916891278110018E+02, -0.174868372001165E+03,  0.137371525614554E+03),
      MOM<double>( 0.323280798030722E+03, -0.718036691248330E+02,  0.129727415223704E+03, -0.287272527776271E+03),
      MOM<double>( 0.555840195595990E+02,  0.378408263917331E+01,  0.481649855077664E+02, -0.274845069087740E+02),
      MOM<double>( 0.174481373991418E+03, -0.614299327068123E+02, -0.258751905863757E+02,  0.161246977490086E+03),
      MOM<double>( 0.510944621194984E+02, -0.304168350648983E+01, -0.506502150402909E+02,  0.599565985521952E+01),
      MOM<double>( 0.525639220300308E+02,  0.429392286864731E+02, -0.253957427134622E+02, -0.165603378897159E+02),
    },{
      MOM<double>(-0.500000000000000E+03,  0.000000000000000E+00,  0.000000000000000E+00, -0.500000000000000E+03),
      MOM<double>(-0.500000000000000E+03,  0.000000000000000E+00,  0.000000000000000E+00,  0.500000000000000E+03),
      MOM<double>( 0.907645618997443E+02,  0.688283299094074E+00, -0.182375433883613E+02,  0.889107641071110E+02),
      MOM<double>( 0.955565102587877E+02, -0.586938801730482E+01,  0.593580443080499E+02,  0.746539986408209E+02),
      MOM<double>( 0.162320058018792E+03, -0.824934010863272E+02, -0.664232596379098E+02,  0.123006465649205E+03),
      MOM<double>( 0.269234903590361E+03, -0.164671738142012E+03, -0.868046882037881E+02, -0.194513747777484E+03),
      MOM<double>( 0.311995259361563E+02, -0.228096655552939E+02,  0.211952645420941E+02,  0.197239369477349E+01),
      MOM<double>( 0.228742403270457E+03,  0.167303795259330E+03,  0.141192266028075E+03, -0.663119232086220E+02),
      MOM<double>( 0.122182037025702E+03,  0.107852114242514E+03, -0.502800836481593E+02, -0.277179511058049E+02),
    },{
      MOM<double>(-0.500000000000000E+03,  0.000000000000000E+00,  0.000000000000000E+00, -0.500000000000000E+03),
      MOM<double>(-0.500000000000000E+03,  0.000000000000000E+00,  0.000000000000000E+00,  0.500000000000000E+03),
      MOM<double>( 0.299026102487866E+03, -0.228596189115547E+03, -0.105931457569442E+02, -0.192479031465521E+03),
      MOM<double>( 0.643252808336830E+02,  0.585429223825772E+02,  0.254023447455163E+02,  0.807396276122583E+01),
      MOM<double>( 0.105319956028235E+03,  0.948961746518037E+02, -0.455444256420548E+02, -0.356573515267504E+01),
      MOM<double>( 0.814652536470618E+02,  0.423923818022744E+02,  0.551578847590880E+02, -0.423919953035180E+02),
      MOM<double>( 0.219386882185242E+03,  0.365491888512753E+02, -0.981665500319213E+02,  0.192764336234962E+03),
      MOM<double>( 0.117200524603677E+03, -0.794631864110030E+02,  0.475258459304442E+01,  0.860173117021993E+02),
      MOM<double>( 0.113276000214235E+03,  0.756787078386191E+02,  0.689913073332717E+02, -0.484188487766728E+02),
    }
  };

  const double ML50_udx_sxsgggepve[] = {
    0.146302957179585E-19,
    0.122405556051350E-18,
    0.981971332280011E-17,
    0.193374344298969E-17,
    0.241964943435912E-19,
  };
  const EpsTriplet<double> ML51_udx_sxsgggepve[] = {
    EpsTriplet<double>(   -0.120941068342230E+03,   -0.774867620518936E+02,   -0.143333333333318E+02),
    EpsTriplet<double>(   -0.114758762799527E+03,   -0.717035867500180E+02,   -0.143333333333340E+02),
    EpsTriplet<double>(   -0.135886246479405E+03,   -0.818274988401141E+02,   -0.143333333333333E+02),
    EpsTriplet<double>(   -0.106686172098244E+03,   -0.788873975517961E+02,   -0.143333333333331E+02),
    EpsTriplet<double>(   -0.817378824663161E+02,   -0.730027344403829E+02,   -0.143333333333333E+02),
  };

  const double ML50_udx_dxdgggepve[] = {
    0.182600694579872E-17,
    0.265739811565734E-18,
    0.480330260827536E-16,
    0.199543094003562E-17,
    0.991566817151157E-19,
  };
  const EpsTriplet<double> ML51_udx_dxdgggepve[] = {
    EpsTriplet<double>(   -0.878434692256486E+02,   -0.728224057183632E+02,   -0.143333333333333E+02),
    EpsTriplet<double>(   -0.998457682137265E+02,   -0.722378244225080E+02,   -0.143333333333338E+02),
    EpsTriplet<double>(   -0.748171339678924E+02,   -0.727181279834981E+02,   -0.143333333333335E+02),
    EpsTriplet<double>(   -0.106134724445114E+03,   -0.788177680931825E+02,   -0.143333333333333E+02),
    EpsTriplet<double>(   -0.517458302263458E+02,   -0.699594194838704E+02,   -0.143333333333333E+02),
  };

  const double ML50_udx_uxugggepve[] = {
    0.118989423154989E-18,
    0.156245161272316E-18,
    0.495627663584779E-16,
    0.309692613845271E-17,
    0.969220202059294E-19,
  };
  const EpsTriplet<double> ML51_udx_uxugggepve[] = {
    EpsTriplet<double>(   -0.811713815568139E+02,   -0.738868632047726E+02,   -0.143333333333336E+02),
    EpsTriplet<double>(   -0.107782150770544E+03,   -0.715027273445976E+02,   -0.143333333333339E+02),
    EpsTriplet<double>(   -0.670337546530519E+02,   -0.728655695308605E+02,   -0.143333333333338E+02),
    EpsTriplet<double>(   -0.966182208368821E+02,   -0.777011671404942E+02,   -0.143333333333340E+02),
    EpsTriplet<double>(   -0.615099010521421E+02,   -0.706937319673502E+02,   -0.143333333333333E+02),
  };

  const double ML50_udx_sxscxcgepve[] = {
    0.463185534393613E-21,
    0.444473289224922E-21,
    0.360454136953329E-18,
    0.229715602511010E-18,
    0.146264995461294E-20,
  };
  const EpsTriplet<double> ML51_udx_sxscxcgepve[] = {
    EpsTriplet<double>(   -0.775979520410663E+02,   -0.600448800207454E+02,   -0.109999999999999E+02),
    EpsTriplet<double>(   -0.459245869643724E+02,   -0.497023197087513E+02,   -0.110000000000005E+02),
    EpsTriplet<double>(   -0.752518826873849E+02,   -0.599931651340864E+02,   -0.110000000000001E+02),
    EpsTriplet<double>(   -0.751695782576667E+02,   -0.594748851067672E+02,   -0.109999999999999E+02),
    EpsTriplet<double>(   -0.374982799211393E+02,   -0.534435851688062E+02,   -0.110000000000000E+02),
  };

  const double ML50_udx_sxssxsgepve[] = {
    0.236914075320467E-20,
    0.513081862320547E-21,
    0.110542445404345E-18,
    0.663687161504005E-19,
    0.250361386230713E-20,
  };
  // FIXME: these numbers are not updated after a bug fix in MadGraph5_v2.0_beta3
  const EpsTriplet<double> ML51_udx_sxssxsgepve[] = {
    EpsTriplet<double>(   -0.770400532241768E+02,   -0.583685732900407E+02,   -0.109999999999999E+02),
    EpsTriplet<double>(   -0.465490894571281E+02,   -0.527670103937502E+02,   -0.110000000000002E+02),
    EpsTriplet<double>(   -0.687610861986305E+02,   -0.586572734436967E+02,   -0.110000000000001E+02),
    EpsTriplet<double>(   -0.782886131263469E+02,   -0.600900491994917E+02,   -0.110000000000000E+02),
    EpsTriplet<double>(   -0.362297146870340E+02,   -0.538308338560740E+02,   -0.110000000000000E+02),
  };

  const double ML50_udx_dxdcxcgepve[] = {
    0.627092364912593E-20,
    0.932624573796150E-21,
    0.209312100979031E-17,
    0.229308451827054E-18,
    0.293224646654616E-20,
  };
  const EpsTriplet<double> ML51_udx_dxdcxcgepve[] = {
    EpsTriplet<double>(   -0.792575210302076E+02,   -0.597337713411430E+02,   -0.110000000000001E+02),
    EpsTriplet<double>(   -0.397709306358641E+02,   -0.514418840739291E+02,   -0.110000000000002E+02),
    EpsTriplet<double>(   -0.235990461772985E+02,   -0.527521370821501E+02,   -0.110000000000010E+02),
    EpsTriplet<double>(   -0.754677511890721E+02,   -0.594847991421082E+02,   -0.110000000000001E+02),
    EpsTriplet<double>(   -0.105800862471278E+02,   -0.502975523695691E+02,   -0.110000000000005E+02),
  };

  const double ML50_udx_dxddxdgepve[] = {
    0.314555456651790E-19,
    0.103013481884108E-19,
    0.425616668393923E-17,
    0.801801348349721E-19,
    0.505779857813357E-20,
  };
  const EpsTriplet<double> ML51_udx_dxddxdgepve[] = {
    EpsTriplet<double>(   -0.657442404484221E+02,   -0.588107090601028E+02,   -0.109999999999993E+02),
    EpsTriplet<double>(   -0.789434549736889E+01,   -0.482131604471644E+02,   -0.110000000000011E+02),
    EpsTriplet<double>(   -0.105664832360100E+01,   -0.482622422330778E+02,   -0.109999999999988E+02),
    EpsTriplet<double>(   -0.748866531940146E+02,   -0.603494022148779E+02,   -0.109999999999998E+02),
    EpsTriplet<double>(   -0.182744422168981E+02,   -0.518727688490555E+02,   -0.109999999999991E+02),
  };

  const double ML50_udx_uxucxcgepve[] = {
    0.761194944575872E-21,
    0.544069229990641E-21,
    0.249625408395891E-17,
    0.272847439102010E-18,
    0.363954085423331E-20,
  };
  const EpsTriplet<double> ML51_udx_uxucxcgepve[] = {
    EpsTriplet<double>(   -0.649362494611925E+02,   -0.580367041130905E+02,   -0.110000000000000E+02),
    EpsTriplet<double>(   -0.408774348570688E+02,   -0.496902116314560E+02,   -0.110000000000007E+02),
    EpsTriplet<double>(   -0.128087123246336E+02,   -0.527411148977368E+02,   -0.110000000000010E+02),
    EpsTriplet<double>(   -0.740453705647696E+02,   -0.594897354600087E+02,   -0.109999999999992E+02),
    EpsTriplet<double>(   -0.155077952419523E+02,   -0.507466790919699E+02,   -0.110000000000001E+02),
  };

  const double ML50_udx_uxuuxugepve[] = {
    0.615258994582363E-20,
    0.105223353852956E-20,
    0.118617786408389E-17,
    0.136756249432703E-18,
    0.393862975960839E-20,
  };
  const EpsTriplet<double> ML51_udx_uxuuxugepve[] = {
    EpsTriplet<double>(   -0.655628609383121E+02,   -0.580655557348208E+02,   -0.110000000000005E+02),
    EpsTriplet<double>(   -0.445908063535887E+02,   -0.532774728946658E+02,   -0.110000000000004E+02),
    EpsTriplet<double>(    0.245476426064113E+01,   -0.500510143803795E+02,   -0.109999999999998E+02),
    EpsTriplet<double>(   -0.692125402983642E+02,   -0.602824869268109E+02,   -0.109999999999993E+02),
    EpsTriplet<double>(   -0.266751090202718E+02,   -0.526630513291697E+02,   -0.110000000000000E+02),
  };

  const double ML50_udx_uxudxdgepve[] = {
    0.342853351791429E-20,
    0.247633555944821E-19,
    0.276767969945119E-17,
    0.324007330373931E-18,
    0.391073200802034E-20,
  };
  const EpsTriplet<double> ML51_udx_uxudxdgepve[] = {
    EpsTriplet<double>(   -0.678445556576493E+02,   -0.597125381763887E+02,   -0.109999999999999E+02),
    EpsTriplet<double>(   -0.494504831759304E+01,   -0.475688305662597E+02,   -0.110000000000021E+02),
    EpsTriplet<double>(   -0.742449485081977E+01,   -0.523478848340771E+02,   -0.109999999999965E+02),
    EpsTriplet<double>(   -0.713876066180817E+02,   -0.596622410305201E+02,   -0.109999999999994E+02),
    EpsTriplet<double>(   -0.148579549436251E+02,   -0.507547392053020E+02,   -0.110000000000001E+02),
  };
  const double ML50_udx_gggggepve[] = {
    0.701700338453752E-18,
    0.270753874466395E-18,
    0.146288083697239E-16,
    0.210841422874565E-16,
    0.240244805412811E-18,
  };
  const EpsTriplet<double> ML51_udx_gggggepve[] = {
  };

  const double ML50_uux_gggggepem[] = {
    0.508005054701506E-18,
    0.110491095595903E-18,
    0.651425374114060E-17,
    0.788196799908441E-17,
    0.994863317129295E-19,
  };
  const EpsTriplet<double> ML51_uux_gggggepem[] = {
  };
  const double ML50_uux_cxcgggepem[] = {
    0.112482982154153E-19,
    0.542318003071866E-19,
    0.326876220241561E-17,
    0.729358940941196E-18,
    0.624503959213481E-20,
  };
  const EpsTriplet<double> ML51_uux_cxcgggepem[] = {
  };
  const double ML50_uux_dxdgggepem[] = {
    0.129975584621601E-19,
    0.508985524307942E-19,
    0.665087307285709E-17,
    0.715022880644170E-18,
    0.146604469602655E-19,
  };
  const EpsTriplet<double> ML51_uux_dxdgggepem[] = {
  };
  const double ML50_uux_uxugggepem[] = {
    0.110749372198141E-17,
    0.145354649897691E-18,
    0.287494438834039E-16,
    0.120633575842803E-17,
    0.917300522489418E-19,
  };
  const EpsTriplet<double> ML51_uux_uxugggepem[] = {
  };
  const double ML50_ddx_sxsbxbgepem[] = {
    0.194740545354392E-21,
    0.102071232304455E-21,
    0.410622159711174E-19,
    0.684092262695372E-19,
    0.272347305699973E-21,
  };
  const EpsTriplet<double> ML51_ddx_sxsbxbgepem[] = {
  };
  const double ML50_uux_dxdcxcgepem[] = {
    0.269421892314158E-21,
    0.155889371040276E-21,
    0.206109061058496E-18,
    0.124336686002351E-18,
    0.827288229640434E-21,
  };
  const EpsTriplet<double> ML51_uux_dxdcxcgepem[] = {
  };
  const double ML50_uux_uxucxcgepem[] = {
    0.412924479469829E-20,
    0.427019906072874E-21,
    0.120080168560471E-17,
    0.147856445499906E-18,
    0.254112973224269E-20,
  };
  const EpsTriplet<double> ML51_uux_uxucxcgepem[] = {
  };
  const double ML50_uux_uxudxdgepem[] = {
    0.426599911586520E-20,
    0.549042537542236E-21,
    0.139664466153737E-17,
    0.100988301454927E-18,
    0.263283946633247E-20,
  };
  const EpsTriplet<double> ML51_uux_uxudxdgepem[] = {
  };
  const double ML50_uux_uxuuxugepem[] = {
    0.174330718492398E-19,
    0.579878052203658E-20,
    0.186326719725040E-17,
    0.803385726467123E-19,
    0.278496102326536E-20,
  };
  const EpsTriplet<double> ML51_uux_uxuuxugepem[] = {
  };

  Flavour<double> W = StandardModel::Wp(StandardModel::u(), StandardModel::ubar(), 1., mW, gW);
  Flavour<double> Zu = StandardModel::Zu(StandardModel::u(), StandardModel::ubar(), mZ, gZ);
  Flavour<double> Zd = StandardModel::Zd(StandardModel::u(), StandardModel::ubar(), mZ, gZ);

  const int namps = 20;
  std::string process[namps] = {
    "# tests for u d~ > g g g g g W+ > e+ ve",
    "# tests for u d~ > s~ s g g g W+ > e+ ve [not updated for ML5v2_beta3]",
    "# tests for u d~ > d~ d g g g W+ > e+ ve [not updated for ML5v2_beta3]",
    "# tests for u d~ > u~ u g g g W+ > e+ ve [not updated for ML5v2_beta3]",
    "# tests for u d~ > s~ s c~ c g W+ > e+ ve",
    "# tests for u d~ > s~ s s~ s g W+ > e+ ve [not updated for ML5v2_beta3]",
    "# tests for u d~ > d~ d c~ c g W+ > e+ ve",
    "# tests for u d~ > d~ d d~ d g W+ > e+ ve",
    "# tests for u d~ > u~ u c~ c g W+ > e+ ve",
    "# tests for u d~ > u~ u u~ u g W+ > e+ ve",
    "# tests for u d~ > u~ u d~ d g W+ > e+ ve",
    // Z amps
    "# tests for u u~ > g g g g g Z/g* > e+ e-",
    "# tests for u u~ > c~ c g g g Z/g* > e+ e-",
    "# tests for u u~ > d~ d g g g Z/g* > e+ e-",
    "# tests for u u~ > u~ u g g g Z/g* > e+ e-",
    "# tests for d d~ > s~ s b~ b g Z/g* > e+ e-",
    "# tests for u u~ > d~ d c~ c g Z/g* > e+ e-",
    "# tests for u u~ > u~ u c~ c g Z/g* > e+ e-",
    "# tests for u u~ > u~ u d~ d g Z/g* > e+ e-",
    "# tests for u u~ > u~ u u~ u g Z/g* > e+ e-",
  };

  double sqrtS = 1e3;
  NJetAmp<double>* amp[namps] = {
    0,//new Amp2q5gV<double>(W, 1.),
    new Amp4q3gV<double>(W, 1.),
    new Amp4q3gV2<double>(W, 1.),
    new Amp4q3gV2b<double>(W, 1.),
    new Amp6q1gV<double>(W, 1.),
    new Amp6q1gV2<double>(W, 1.),
    new Amp6q1gV2n<double>(W, 1.),
    new Amp6q1gV6n<double>(W, 1.),
    new Amp6q1gV2b<double>(W, 1.),
    new Amp6q1gV6b<double>(W, 1.),
    new Amp6q1gV4nb<double>(W, 1.),
    // Z amps
    new Amp2q5gZ<double>(Zu, 1.),
    new Amp4q3gZ<double>(Zu, 1.),
    new Amp4q3gZd<double>(Zu, 1.),
    new Amp4q3gZ2<double>(Zu, 1.),
    new Amp6q1gZ<double>(Zd, 1.),
    new Amp6q1gZd<double>(Zu, 1.),
    new Amp6q1gZ2<double>(Zu, 1.),
    new Amp6q1gZ2d<double>(Zu, 1.),
    new Amp6q1gZ6<double>(Zu, 1.),
  };

  double times[namps][npoints];

  int ord[namps][legs] = {
    // W amps
    {0, 1, 2, 3, 4, 5, 6, 8, 7},
    {0, 1, 2, 3, 4, 5, 6, 8, 7},
    {0, 1, 2, 3, 4, 5, 6, 8, 7},
    {0, 1, 2, 3, 4, 5, 6, 8, 7},
    {0, 1, 2, 3, 4, 5, 6, 8, 7},
    {0, 1, 2, 3, 4, 5, 6, 8, 7},
    {0, 1, 2, 3, 4, 5, 6, 8, 7},
    {0, 1, 2, 3, 4, 5, 6, 8, 7},
    {0, 1, 2, 3, 4, 5, 6, 8, 7},
    {0, 1, 2, 3, 4, 5, 6, 8, 7},
    {0, 1, 4, 5, 2, 3, 6, 8, 7},
    // Z amps
    {0, 1, 2, 3, 4, 5, 6, 8, 7},
    {0, 1, 2, 3, 4, 5, 6, 8, 7},
    {0, 1, 2, 3, 4, 5, 6, 8, 7},
    {0, 1, 2, 3, 4, 5, 6, 8, 7},
    {0, 1, 2, 3, 4, 5, 6, 8, 7},
    {0, 1, 4, 5, 2, 3, 6, 8, 7},
    {0, 1, 2, 3, 4, 5, 6, 8, 7},
    {0, 1, 2, 3, 4, 5, 6, 8, 7},
    {0, 1, 2, 3, 4, 5, 6, 8, 7},
  };

  double prefactor0[] = {
    // W amps
    pow(ee/se, 4.)*pow(gs, 10.)/(Nc*Nc*4.*120.),
    pow(ee/se, 4.)*pow(gs, 10.)/(Nc*Nc*4.*6.),
    pow(ee/se, 4.)*pow(gs, 10.)/(Nc*Nc*4.*6.),
    pow(ee/se, 4.)*pow(gs, 10.)/(Nc*Nc*4.*6.),
    pow(ee/se, 4.)*pow(gs, 10.)/(Nc*Nc*4.),
    pow(ee/se, 4.)*pow(gs, 10.)/(Nc*Nc*4.*4),
    pow(ee/se, 4.)*pow(gs, 10.)/(Nc*Nc*4.),
    pow(ee/se, 4.)*pow(gs, 10.)/(Nc*Nc*4.*4),
    pow(ee/se, 4.)*pow(gs, 10.)/(Nc*Nc*4.),
    pow(ee/se, 4.)*pow(gs, 10.)/(Nc*Nc*4.*4),
    pow(ee/se, 4.)*pow(gs, 10.)/(Nc*Nc*4.),
    // Z amps
    pow(ee, 4.)*pow(gs, 10.)/(Nc*Nc*4.*120.),
    pow(ee, 4.)*pow(gs, 10.)/(Nc*Nc*4.*6.),
    pow(ee, 4.)*pow(gs, 10.)/(Nc*Nc*4.*6.),
    pow(ee, 4.)*pow(gs, 10.)/(Nc*Nc*4.*6.),
    pow(ee, 4.)*pow(gs, 10.)/(Nc*Nc*4.),
    pow(ee, 4.)*pow(gs, 10.)/(Nc*Nc*4.),
    pow(ee, 4.)*pow(gs, 10.)/(Nc*Nc*4.),
    pow(ee, 4.)*pow(gs, 10.)/(Nc*Nc*4.),
    pow(ee, 4.)*pow(gs, 10.)/(Nc*Nc*4.*4.),
  };

  const double* ML5res0[namps] = {
    ML50_udx_gggggepve,
    ML50_udx_sxsgggepve,
    ML50_udx_dxdgggepve,
    ML50_udx_uxugggepve,
    ML50_udx_sxscxcgepve,
    ML50_udx_sxssxsgepve,
    ML50_udx_dxdcxcgepve,
    ML50_udx_dxddxdgepve,
    ML50_udx_uxucxcgepve,
    ML50_udx_uxuuxugepve,
    ML50_udx_uxudxdgepve,
    // Z amps
    ML50_uux_gggggepem,
    ML50_uux_cxcgggepem,
    ML50_uux_dxdgggepem,
    ML50_uux_uxugggepem,
    ML50_ddx_sxsbxbgepem,
    ML50_uux_dxdcxcgepem,
    ML50_uux_uxucxcgepem,
    ML50_uux_uxudxdgepem,
    ML50_uux_uxuuxugepem,
  };
  const EpsTriplet<double>* ML5res1[namps] = {
    ML51_udx_gggggepve,
    ML51_udx_sxsgggepve,
    ML51_udx_dxdgggepve,
    ML51_udx_uxugggepve,
    ML51_udx_sxscxcgepve,
    ML51_udx_sxssxsgepve,
    ML51_udx_dxdcxcgepve,
    ML51_udx_dxddxdgepve,
    ML51_udx_uxucxcgepve,
    ML51_udx_uxuuxugepve,
    ML51_udx_uxudxdgepve,
    // Z amps
    ML51_uux_gggggepem,
    ML51_uux_cxcgggepem,
    ML51_uux_dxdgggepem,
    ML51_uux_uxugggepem,
    ML51_ddx_sxsbxbgepem,
    ML51_uux_dxdcxcgepem,
    ML51_uux_uxucxcgepem,
    ML51_uux_uxudxdgepem,
    ML51_uux_uxuuxugepem,
  };

  int nq[namps] = {2, 4, 4, 4, 6, 6, 6, 6, 6, 6, 6, 2, 4, 4, 4, 6, 6, 6, 6, 6};
  int ng[namps] = {5, 3, 3, 3, 1, 1, 1, 1, 1, 1, 1, 5, 3, 3, 3, 1, 1, 1, 1, 1};
  double MuR2 = sqrtS*sqrtS;
  for (int i=0; i<namps; i++) {
    if (not amp[i]) continue;
    cout << process[i] << endl;
    amp[i]->setMuR2(MuR2);
    amp[i]->setNc(Nc);
    amp[i]->setNf(Nf);
    double xNf = Nf;
    if (i==15) {
      amp[15]->setNf(5.);
      xNf = 5.;
    }

    const double prefactor1 = pow(gs,2.)*prefactor0[i]/(16.*M_PI*M_PI);

    for (int pt=0; pt<npoints; pt++) {
      MOM<double> mm[legs];
      for (int j=0; j<legs; j++) {
        mm[j] = MG5moms[pt][ord[i][j]];
      }
      amp[i]->setMomenta(mm);

      tm.start();
      EpsTriplet<double> loop = amp[i]->virt();
      double tree = amp[i]->born();
      loop += getrenorm<double>(ng[i], nq[i], Nc, xNf, tree);
      times[i][pt] = static_cast<double>(tm.get());

      double tree_cc[legs*(legs-1)/2] = {0.};
      amp[i]->born_cc(tree_cc);

      double ans0 = prefactor0[i]*tree;
      cout << "A0    " << ans0 << "\n";
      cout << "ML0   " << ML5res0[i][pt] << " rel. diff("<< (ML5res0[i][pt]-ans0)/ans0 << ")" << endl;
      cout << "A1    " << prefactor1*loop << "\n";
      cout << "poles " << prefactor1*getpoles<double>(ng[i], nq[i], Nc, xNf, mm, MuR2, tree, tree_cc) << "\n";
      EpsTriplet<double> ans1_ratio = loop/tree*0.5;
      cout << "A1/A0 " << ans1_ratio << endl;
      cout << "ML1   " << ML5res1[i][pt] << "\n  diff("<< ML5res1[i][pt]-ans1_ratio << ")" << endl;
    }
    cout << "TIME ";
    print_stat(npoints, times[i]);
    cout << endl;
    cout << endl;
  }

  for (int n=0; n<namps; n++) {
    if (amp[n]) {
      delete amp[n];
      amp[n] = 0;
    }
  }
#endif
}
//}}}

//{{{ pp2j
void pp2j()
{
  const int legs = 4;
  const int npoints = 5;

  MOM<double> Momenta[npoints][legs] = {
    {
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00, -0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00,  0.5000000000000000E+03),
      MOM<double>( 0.4999999999999998E+03,  0.1109242844438328E+03,  0.4448307894881214E+03, -0.1995529299308788E+03),
      MOM<double>( 0.5000000000000000E+03, -0.1109242844438328E+03, -0.4448307894881214E+03,  0.1995529299308787E+03)
    },{
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00, -0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00,  0.5000000000000000E+03),
      MOM<double>( 0.5000000000000000E+03, -0.2819155058093908E+03, -0.3907909666011574E+03,  0.1334393795218208E+03),
      MOM<double>( 0.5000000000000002E+03,  0.2819155058093908E+03,  0.3907909666011575E+03, -0.1334393795218208E+03)
    },{
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00, -0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00,  0.5000000000000000E+03),
      MOM<double>( 0.4999999999999990E+03, -0.2406210290937675E+03,  0.2804720059911282E+03, -0.3368040590805997E+03),
      MOM<double>( 0.5000000000000012E+03,  0.2406210290937674E+03, -0.2804720059911282E+03,  0.3368040590805997E+03)
    },{
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00, -0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00,  0.5000000000000000E+03),
      MOM<double>( 0.5000000000000001E+03,  0.4510352131035046E+03,  0.6047968715179463E+02, -0.2071459485065961E+03),
      MOM<double>( 0.5000000000000001E+03, -0.4510352131035046E+03, -0.6047968715179463E+02,  0.2071459485065960E+03)
    },{
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00, -0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00,  0.5000000000000000E+03),
      MOM<double>( 0.5000000000000007E+03,  0.4557289838719782E+03, -0.2036894622960882E+03, -0.2866524391218178E+02),
      MOM<double>( 0.5000000000000007E+03, -0.4557289838719784E+03,  0.2036894622960884E+03,  0.2866524391218240E+02)
    }
  };

  const int namps = 4;

  const char* amp_names[namps] = {
    "Amp0q4g",
    "Amp2q2g",
    "Amp4q0g",
    "Amp4q0g2",
  };

  NJetAmp<double>* amps[namps] = {
    new Amp0q4g<double>(1.),
    new Amp2q2g<double>(1.),
    new Amp4q0g<double>(1.),
    new Amp4q0g2<double>(1.),
  };

  double times[namps][3][npoints];

  EpsTriplet<double> ng1[namps][npoints] = {
    {
    // 21, 21 -> [21, 21]
      EpsTriplet<double>(-1.2230265530030334e+02, 8.3053972665090072e+01, -2.3999999999999748e+01), /* 1.2848783651156748e+04 */
      EpsTriplet<double>(-1.2843774498678530e+02, 8.4581839908772579e+01, -2.4000000000000011e+01), /* 9.6717567949927070e+03 */
      EpsTriplet<double>(-9.8508121325443042e+01, 7.6929415457929494e+01, -2.3999999999999684e+01), /* 3.9762908516269876e+04 */
      EpsTriplet<double>(-1.2141773064113453e+02, 8.2831618345906534e+01, -2.3999999999999964e+01), /* 1.3390680948289461e+04 */
      EpsTriplet<double>(-1.3300455887751090e+02, 8.5702524305959273e+01, -2.3999999999999950e+01), /* 7.8531242729325832e+03 */
    },
    {
    // 1, -1 -> [21, 21]
      EpsTriplet<double>(-1.0538399439023101e+02, 6.3561860178414413e+01, -1.7333333333333297e+01), /* 6.2022890923407459e+01 */
      EpsTriplet<double>(-1.1499192786669185e+02, 6.6489566031541102e+01, -1.7333333333333329e+01), /* 4.7002344718440632e+01 */
      EpsTriplet<double>(-7.6129521869968698e+01, 5.4501399554083598e+01, -1.7333333333333250e+01), /* 1.5731882176149344e+02 */
      EpsTriplet<double>(-1.0409252299587713e+02, 6.3168371679451880e+01, -1.7333333333333311e+01), /* 6.4457084813945727e+01 */
      EpsTriplet<double>(-1.2304535649562962e+02, 6.8954341492751965e+01, -1.7333333333333389e+01), /* 3.7738362057077254e+01 */
    },
    {
    // 1, -1 -> [-2, 2]
      EpsTriplet<double>(-7.0480600362226383e+01, 3.8247064150251440e+01, -1.0666666666666687e+01), /* 9.2742838990079264e+00 */
      EpsTriplet<double>(-8.9419188127109919e+01, 4.3485322601097231e+01, -1.0666666666666657e+01), /* 8.5697941762293866e+00 */
      EpsTriplet<double>(-5.3080359973739604e+01, 3.3028473176161292e+01, -1.0666666666666705e+01), /* 1.1629983174821390e+01 */
      EpsTriplet<double>(-6.9771380922211335e+01, 3.8036944970037389e+01, -1.0666666666666668e+01), /* 9.3731022074463120e+00 */
      EpsTriplet<double>(-8.2366944949768410e+01, 4.1702939840519008e+01, -1.0666666666666636e+01), /* 8.0262942786734541e+00 */
    },
    {
    // 1, -1 -> [-1, 1]
      EpsTriplet<double>(-5.2682992684644354e+01, 5.0436424610333660e+01, -1.0666666666666790e+01), /* 2.9063862073949383e+02 */
      EpsTriplet<double>(-5.4795595393468048e+01, 4.7179179141607435e+01, -1.0666666666666654e+01), /* 5.6066259873581977e+01 */
      EpsTriplet<double>(-4.5304262326066365e+01, 5.2075971616632430e+01, -1.0666666666665996e+01), /* 1.0788359448107431e+03 */
      EpsTriplet<double>(-5.2496408322662475e+01, 5.0518121689751069e+01, -1.0666666666666696e+01), /* 3.0743636847604847e+02 */
      EpsTriplet<double>(-5.4262025221152889e+01, 4.8717821572643729e+01, -1.0666666666666563e+01), /* 1.0650163080756060e+02 */
    },
  };

  int ng[] = {4, 2, 0, 0};
  int nq[] = {0, 2, 4, 4};

  const double mur = 91.188;
  const double Nc = 3.;
  const double Nf = 5.;
  for (int n=0; n<namps; n++) {
    cout << "----- Amp " << amp_names[n] << " ------"<< endl;
    NJetAmp<double>* amp1 = amps[n];
    NJetAmp<double>* amp1_lc = 0;
    NJetAmp<double>* amp1_slc = 0;
    if (not amp1) continue;
    const bool withLC = amp1_lc and amp1_slc;

    amp1->setMuR2(mur*mur);
    if (withLC) {
      amp1_lc->setMuR2(mur*mur);
      amp1_slc->setMuR2(mur*mur);
    }

    for (int i=0; i<npoints; i++) {
      cout << "----- POINT N " << i << " ------"<< endl;
      amp1->setMomenta(Momenta[i]);
      if (withLC) {
        amp1_lc->setMomenta(Momenta[i]);
        amp1_slc->setMomenta(Momenta[i]);
      }

      tm.start();
      EpsTriplet<double> sum_loop = amp1->virt();
      times[n][0][i] = static_cast<double>(tm.get());
      tm.start();
      EpsTriplet<double> sum_loop_lc = withLC ? amp1_lc->virt() : EpsTriplet<double>();
      times[n][1][i] = static_cast<double>(tm.get());
      tm.start();
      EpsTriplet<double> sum_loop_slc =  withLC ? amp1_slc->virt() : EpsTriplet<double>();
      times[n][2][i] = static_cast<double>(tm.get());

      double sum_tree_cc[legs*(legs-1)/2] = {};
      double sum_tree = amp1->born();
      amp1->born_cc(sum_tree_cc);

      cout << "A0    " << sum_tree << endl;
      cout << "A1    " << sum_loop << endl;
      cout << "poles " << getpoles<double>(ng[n], nq[n], Nc, Nf, Momenta[i], mur*mur, sum_tree, sum_tree_cc, 1) << endl;
      cout << "A1/A0 " << sum_loop/sum_tree << endl;
      cout << "NG1:  " << ng1[n][i] << endl;
      cout << "DIFF  " << (sum_loop/sum_tree-ng1[n][i]) << endl;
      if (withLC) {
        cout << endl;
        cout << "LC    " << sum_loop_lc/sum_tree << endl;
        cout << "SL    " << sum_loop_slc/sum_tree << endl;
        cout << "FULL  " << (sum_loop_lc+sum_loop_slc)/sum_tree << endl;
      }
      cout.flush();
    }
    cout << "TIME " << amp_names[n] << " FULL ";
    print_stat(npoints, times[n][0]);
    if (withLC) {
      cout << "\tLC ";
      print_stat(npoints, times[n][1]);
      cout << "\tSLC ";
      print_stat(npoints, times[n][2]);
    }
    cout << endl;
  }
  for (int n=0; n<namps; n++) {
    if (amps[n]) {
      delete amps[n];
      amps[n] = 0;
    }
  }
}
//}}}

//{{{ pp3j
void pp3j()
{
  const int npoints = 2;
  const int legs = 5;

  const MOM<double> Momenta[][legs] = {
    {
      MOM<double>(-0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00,-0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00, 0.5000000000000000E+03),
      MOM<double>( 0.4585787878854402E+03, 0.1694532203096798E+03, 0.3796536620781987E+03,-0.1935024746502525E+03),
      MOM<double>( 0.3640666207368177E+03,-0.1832986929319185E+02,-0.3477043013193671E+03, 0.1063496077587081E+03),
      MOM<double>( 0.1773545913777421E+03,-0.1511233510164880E+03,-0.3194936075883156E+02, 0.8715286689154436E+02)
    },
    {
      MOM<double>(-0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00,-0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00, 0.5000000000000000E+03),
      MOM<double>( 0.4951533920773834E+03, 0.1867229157692120E+03, 0.3196835780850242E+03,-0.3288066974913060E+03),
      MOM<double>( 0.1026779138750091E+03,-0.7062042730772224E+02,-0.4345595295696615E+02, 0.6055649756384902E+02),
      MOM<double>( 0.4021686940476072E+03,-0.1161024884614897E+03,-0.2762276251280580E+03, 0.2682501999274569E+03)
    }
  };

  const int namps = 4;

  const char* amp_names[namps] = {
    "Amp0q5g",
    "Amp2q3g",
    "Amp4q1g",
    "Amp4q1g2",
  };

  const int nq[namps] = {0, 2, 4, 4};
  const int ng[namps] = {5, 3, 1, 1};

  NJetAmp<double>* amps[namps] = {
    new Amp0q5g<double>(1.),
    new Amp2q3g<double>(1.),
    new Amp4q1g<double>(1.),
    new Amp4q1g2<double>(1.),
  };

  double times[namps][3][npoints];

  EpsTriplet<double> ng1[namps][npoints] = {
    {
    // 21 21 -> [21, 21, 21]
      EpsTriplet<double>(-9.5232959888216556e+01, 8.9506190702177108e+01, -2.9999999999999876e+01), /* 8.8287997984160089e+00 */
      EpsTriplet<double>(-8.1759250560802940e+01, 6.9153368752106061e+01, -2.9999999999999883e+01), /* 2.1912327096425668e+02 */
    },
    {
    // 1, -1 -> [21, 21, 21]
      EpsTriplet<double>(-8.4055257658727186e+01, 6.8836659295292989e+01, -2.3333333333333048e+01), /* 2.7563319178479928e-02 */
      EpsTriplet<double>(-6.2094456805702421e+01, 4.5707323165881697e+01, -2.3333333333333563e+01), /* 7.4884847219389628e-01 */
    },
    {
    // 1, -1 -> [-2, 2, 21]
      EpsTriplet<double>(-5.4068179665171975e+01, 4.6322077520713890e+01, -1.6666666666666778e+01), /* 1.8951516616064301e-03 */
      EpsTriplet<double>(-3.2238543563696858e+01, 3.4179506584854316e+01, -1.6666666666665520e+01), /* 5.1790813692580324e-03 */
    },
    {
    // 1, -1 -> [-1, 1, 21]
      EpsTriplet<double>(-2.8269700601094456e+01, 5.6175015808681323e+01, -1.6666666666666618e+01), /* 9.4722439699621380e-02 */
      EpsTriplet<double>(-1.8523451692087377e+01, 5.4416419208972364e+01, -1.6666666666666590e+01), /* 4.0461797002320932e-01 */
    },
  };

  const double mur = 91.188;
  const double Nc = 3.;
  const double Nf = 5.;

  EpsTriplet<double> sum_loop;
  EpsTriplet<double> sum_loop_lc;
  EpsTriplet<double> sum_loop_slc;

  bool have_lcslc;
  for (int n=0; n<namps; n++) {
    cout << "----- Amp " << amp_names[n] << " ------"<< endl;
    NJetAmp<double>* amp1 = amps[n];

    amp1->setMuR2(mur*mur);

    for (int i=0; i<npoints; i++) {
      cout << "----- POINT N " << i << " ------"<< endl;
      amp1->setMomenta(Momenta[i]);

      tm.start();
      have_lcslc = amp1->setLoopType(NJetAmpTables::COLOR_FULL);
      sum_loop = amp1->virt();
      times[n][0][i] = static_cast<double>(tm.get());

      if (have_lcslc) {
        tm.start();
        amp1->setLoopType(NJetAmpTables::COLOR_LC);
        sum_loop_lc = amp1->virt();
        times[n][1][i] = static_cast<double>(tm.get());

        tm.start();
        amp1->setLoopType(NJetAmpTables::COLOR_SLC);
        sum_loop_slc =  amp1->virt();
        times[n][2][i] = static_cast<double>(tm.get());
      }

      double sum_tree = amp1->born();
      double sum_tree_cc[legs*(legs-1)/2] = {};
      amp1->born_cc(sum_tree_cc);

      cout << "A0    " << sum_tree << endl;
      cout << "A1    " << sum_loop << endl;
      cout << "poles " << getpoles<double>(ng[n], nq[n], Nc, Nf, Momenta[i], mur*mur, sum_tree, sum_tree_cc, 1) << endl;
      cout << "A1/A0 " << sum_loop/sum_tree << endl;
      cout << "NG1:  " << ng1[n][i] << endl;
      cout << "DIFF  " << (sum_loop/sum_tree-ng1[n][i]) << endl;
      if (have_lcslc) {
        cout << endl;
        cout << "LC    " << sum_loop_lc/sum_tree << endl;
        cout << "SL    " << sum_loop_slc/sum_tree << endl;
        cout << "FULL  " << (sum_loop_lc+sum_loop_slc)/sum_tree << endl;
      }
      cout.flush();
    }
    cout << "TIME " << amp_names[n] << " FULL ";
    print_stat(npoints, times[n][0]);
    if (have_lcslc) {
      cout << "\tLC ";
      print_stat(npoints, times[n][1]);
      cout << "\tSLC ";
      print_stat(npoints, times[n][2]);
    }
    cout << endl;
  }
  for (int n=0; n<namps; n++) {
    if (amps[n]) {
      delete amps[n];
      amps[n] = 0;
    }
  }
}
//}}}

//{{{ pp4j
void pp4j()
{
  const int npoints = 4;
  const int legs = 6;

  const MOM<double> Momenta[][legs] = {
    {
      MOM<double>(-0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00,-0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00, 0.5000000000000000E+03),
      MOM<double>( 0.8855133305450298E+02,-0.2210069028768998E+02, 0.4008035319168533E+02,-0.7580543095693663E+02),
      MOM<double>( 0.3283294192270985E+03,-0.1038496118834563E+03,-0.3019337553895401E+03, 0.7649492138716589E+02),
      MOM<double>( 0.1523581094674306E+03,-0.1058809596665922E+03,-0.9770963832697571E+02, 0.4954838522679282E+02),
      MOM<double>( 0.4307611382509676E+03, 0.2318312618377385E+03, 0.3595630405248305E+03,-0.5023787565702211E+02)
    },
    {
      MOM<double>(-0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00,-0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00, 0.5000000000000000E+03),
      MOM<double>( 0.1177477081371708E+03,-0.6070592184636148E+02, 0.7123104586232657E+02, 0.7145244523246875E+02),
      MOM<double>( 0.3509541730779688E+03,-0.3178816670261076E+02, 0.8393942869317344E+02, 0.3392823549334560E+03),
      MOM<double>( 0.3493322285737900E+03, 0.1840093039957635E+03,-0.5152779793923704E+02,-0.2924354082577186E+03),
      MOM<double>( 0.1819658902110701E+03,-0.9151521544679123E+02,-0.1036426766162630E+03,-0.1182993919082062E+03)
    },
    {
      MOM<double>(-5.0000000000000000E+01, 0.0000000000000000E+00, 0.0000000000000000E+00,-5.0000000000000000E+01),
      MOM<double>(-5.0000000000000000E+01, 0.0000000000000000E+00, 0.0000000000000000E+00, 5.0000000000000000E+01),
      MOM<double>( 3.0830908416563354E+01,-1.5462861903492566E+01, 2.6617620401269029E+01, 1.7167118913730799E+00),
      MOM<double>( 2.5745672609866769E+01,-1.1924408694529273E+01,-8.6688190119874555E+00,-2.1106864105254559E+01),
      MOM<double>( 4.0645606131607479E+01, 2.8711125857142530E+01,-2.0051754806234289E+01, 2.0631618429288316E+01),
      MOM<double>( 2.7778128419623971E+00,-1.3238552591206938E+00, 2.1029534169527198E+00,-1.2414662154068357E+00)
    },
    {
      MOM<double>(-5.0000000000000000E+01, 0.0000000000000000E+00, 0.0000000000000000E+00,-5.0000000000000000E+01),
      MOM<double>(-5.0000000000000000E+01, 0.0000000000000000E+00, 0.0000000000000000E+00, 5.0000000000000000E+01),
      MOM<double>( 1.7658726533284433E+01, 1.3403174828466632E+01, 6.6749400858894896E+00, 9.3611271834468557E+00),
      MOM<double>( 3.9791943733796067E+01,-3.0687550194649063E+01,-2.4819226609719482E+01, 5.0674490288386460E+00),
      MOM<double>( 3.4133057638993364E+01, 2.2521276138666977E+01, 1.5476870081140326E+01,-2.0452976247095574E+01),
      MOM<double>( 8.4162720939261479E+00,-5.2369007724845442E+00, 2.6674164426896647E+00, 6.0244000348100739E+00)
    }
  };

  const int namps = 7;

  const char* amp_names[namps] = {
    "Amp0q6g",
    "Amp2q4g",
    "Amp4q2g",
    "Amp4q2g2",
    "Amp6q0g",
    "Amp6q0g2",
    "Amp6q0g6",
  };

  const int nq[namps] = {0, 2, 4, 4, 6, 6, 6};
  const int ng[namps] = {6, 4, 2, 2, 0, 0, 0};

  NJetAmp<double>* amps[namps] = {
    new Amp0q6g<double>(1.),
    new Amp2q4g<double>(1.),
    new Amp4q2g<double>(1.),
    new Amp4q2g2<double>(1.),
    new Amp6q0g<double>(1.),
    new Amp6q0g2<double>(1.),
    new Amp6q0g6<double>(1.),
  };

  const int ord[namps][legs] = {
    {0, 1, 2, 3, 4, 5},
    {0, 1, 2, 3, 4, 5},
    {0, 1, 2, 3, 4, 5},
    {0, 1, 2, 3, 4, 5},
    {0, 1, 2, 3, 4, 5},
    {4, 5, 0, 1, 2, 3},
    {0, 1, 2, 3, 4, 5},
  };

  double times[namps][3][npoints];

  const EpsTriplet<double> ng1[namps][npoints] = {
    {
    // 21, 21 -> [21, 21, 21, 21]
      EpsTriplet<double>(-8.3100840602237710e+01, 7.0992551348527527e+01, -3.6000000000000668e+01), /* 2.0244053477393464e-01 */
      EpsTriplet<double>(8.1451742970829741e+00, 5.9357109422333941e+01, -3.5999999999995453e+01), /* 1.0562143598284039e+00 */
      EpsTriplet<double>(-1.9640154223926834e+02, -1.0362806164692095e+02, -3.6000000000001755e+01), /* 9.3751737039745167e+03 */
      EpsTriplet<double>(-6.6484138498486871e+01, -8.3695388894941587e+01, -3.6000000000000099e+01), /* 5.0455584104309457e+02 */
    },
    {
    // 1, -1 -> [21, 21, 21, 21]
      EpsTriplet<double>(-7.5767617610453740e+01, 5.2031131371199734e+01, -2.9333333333333798e+01), /* 5.3097829544836866e-04 */
      EpsTriplet<double>(3.2197129030131793e+00, 3.8699080751763923e+01, -2.9333333333333396e+01), /* 3.4534309273034230e-04 */
      EpsTriplet<double>(-2.0926656272506983e+02, -9.1155283475598367e+01, -2.9333333333341951e+01), /* 1.3800796471994691e+01 */
      EpsTriplet<double>(-7.0098110700024350e+01, -6.8142986689314000e+01, -2.9333333333333215e+01), /* 7.4199337381469699e-01 */
    },
    {
    // 1, -1 -> [-2, 2, 21, 21]
      EpsTriplet<double>(-6.4779643514372566e+01, 5.1961256852207356e+01, -2.2666666666669823e+01), /* 5.5255710669467073e-07 */
      EpsTriplet<double>(-1.0648587840524801e+00, 4.2043716541670612e+01, -2.2666666666666739e+01), /* 4.7386211286579543e-06 */
      EpsTriplet<double>(-1.9778081874810036e+02, -7.0092439318777195e+01, -2.2666666666675091e+01), /* 3.3934297820320730e-01 */
      EpsTriplet<double>(-8.7559009999614403e+01, -6.1351644095419921e+01, -2.2666666666666682e+01), /* 3.2921923379594369e-02 */
    },
    {
    // 1, -1 -> [-1, 1, 21, 21]
      EpsTriplet<double>(-2.4856347635229167e+01, 5.8414758997573180e+01, -2.2666666666666551e+01), /* 1.5861909771771118e-04 */
      EpsTriplet<double>(3.3665705454808425e+01, 5.3734638420210779e+01, -2.2666666666666476e+01), /* 1.0216895964371288e-04 */
      EpsTriplet<double>(-1.4699330200548016e+02, -6.9747597139841787e+01, -2.2666666666666401e+01), /* 1.3168190419246417e+00 */
      EpsTriplet<double>(2.9776772722522442e+00, -4.9874316499405765e+01, -2.2666666666666764e+01), /* 2.1018368109888869e-01 */
    },
    {
    // 1, -1 -> [-2, 2, -3, 3]
      EpsTriplet<double>(-8.4369717237056960e+01, 4.6642719147788661e+01, -1.6000000000000799e+01), /* 3.8423777779042078e-09 */
      EpsTriplet<double>(-1.5889758669803514e+01, 3.2373425696263183e+01, -1.5999999999999988e+01), /* 2.2717319793766573e-07 */
      EpsTriplet<double>(-3.2452436847787709e+01, -2.4412116071581480e+01, -1.5999999999969132e+01), /* 1.6114989828912567e-04 */
      EpsTriplet<double>(-1.7757235047299272e+01, -2.5139616837581240e+01, -1.5999999999999934e+01), /* 6.9895280485059909e-05 */
    },
    {
    // 1, -1 -> [-1, 1, -2, 2]
      EpsTriplet<double>(-4.4631260623296122e+01, 4.8874027511580046e+01, -1.6000000000000234e+01), /* 1.3394179927079422e-07 */
      EpsTriplet<double>(6.5736023621350332e+00, 4.2669139534223902e+01, -1.5999999999999750e+01), /* 2.5377765534604810e-06 */
      EpsTriplet<double>(1.0451825561102238e+01, -2.4772226304269175e+01, -1.6000000000000480e+01), /* 6.6403418828252182e-04 */
      EpsTriplet<double>(1.5993901834787703e+01, -2.7806733596566119e+01, -1.5999999999999924e+01), /* 5.6837559517441245e-04 */
    },
    {
    // 1, -1 -> [-1, 1, -1, 1]
      EpsTriplet<double>(-4.9012879261452760e+01, 5.3891392302678604e+01, -1.6000000000000419e+01), /* 2.2172293515544216e-06 */
      EpsTriplet<double>(2.9068385616152966e+01, 5.2040756127686514e+01, -1.5999999999999975e+01), /* 6.0604946386506344e-05 */
      EpsTriplet<double>(6.2227220154161840e+00, -1.9907245260565016e+01, -1.5999999999999227e+01), /* 9.2963537678857813e-03 */
      EpsTriplet<double>(5.4480471405108837e+01, -2.0207390511798298e+01, -1.5999999999999931e+01), /* 2.7510356832112753e-02 */
    },
  };

  const double mur = 91.188;
  const double Nc = 3.;
  const double Nf = 5.;

  EpsTriplet<double> sum_loop;
  EpsTriplet<double> sum_loop_lc;
  EpsTriplet<double> sum_loop_slc;

  bool have_lcslc;
  for (int n=0; n<namps; n++) {
    cout << "----- Amp " << amp_names[n] << " ------"<< endl;
    NJetAmp<double>* amp1 = amps[n];

    amp1->setMuR2(mur*mur);

    for (int i=0; i<npoints; i++) {
      cout << "----- POINT N " << i << " ------"<< endl;
      MOM<double> OrdMomenta[legs];
      for (int j=0; j<legs; j++) {
        OrdMomenta[j] = Momenta[i][ord[n][j]];
      }
      amp1->setMomenta(OrdMomenta);

      tm.start();
      have_lcslc = amp1->setLoopType(NJetAmpTables::COLOR_FULL);
      sum_loop = amp1->virt();
      times[n][0][i] = static_cast<double>(tm.get());

      if (have_lcslc) {
        tm.start();
        amp1->setLoopType(NJetAmpTables::COLOR_LC);
        sum_loop_lc = amp1->virt();
        times[n][1][i] = static_cast<double>(tm.get());

        tm.start();
        amp1->setLoopType(NJetAmpTables::COLOR_SLC);
        sum_loop_slc =  amp1->virt();
        times[n][2][i] = static_cast<double>(tm.get());
      }

      double sum_tree = amp1->born();
      double sum_tree_cc[legs*(legs-1)/2] = {};
      amp1->born_cc(sum_tree_cc);

      cout << "A0    " << sum_tree << endl;
      cout << "A1    " << sum_loop << endl;
      cout << "poles " << getpoles<double>(ng[n], nq[n], Nc, Nf, Momenta[i], mur*mur, sum_tree, sum_tree_cc, 1) << endl;
      cout << "A1/A0 " << sum_loop/sum_tree << endl;
      cout << "NG1:  " << ng1[n][i] << endl;
      cout << "DIFF  " << (sum_loop/sum_tree-ng1[n][i]) << endl;
      if (have_lcslc) {
        cout << endl;
        cout << "LC    " << sum_loop_lc/sum_tree << endl;
        cout << "SL    " << sum_loop_slc/sum_tree << endl;
        cout << "FULL  " << (sum_loop_lc+sum_loop_slc)/sum_tree << endl;
      }
      cout.flush();
    }
    cout << "TIME " << amp_names[n] << " FULL ";
    print_stat(npoints, times[n][0]);
    if (have_lcslc) {
      cout << "\tLC ";
      print_stat(npoints, times[n][1]);
      cout << "\tSLC ";
      print_stat(npoints, times[n][2]);
    }
    cout << endl;
  }
  for (int n=0; n<namps; n++) {
    if (amps[n]) {
      delete amps[n];
      amps[n] = 0;
    }
  }
}
//}}}

//{{{ pp5j
void pp5j()
{
#ifdef NJET_ENABLE_5
  const int npoints = 4;
  const int legs = 7;

  const MOM<double> Momenta[][legs] = {
    {
      MOM<double>(-0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00,-0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00, 0.5000000000000000E+03),
      MOM<double>( 0.8635406814378138E+02,-0.1521338932026180E+02, 0.3763355129491627E+02,-0.7621872268218542E+02),
      MOM<double>( 0.2801181818093764E+03,-0.8312611165058223E+02,-0.2632038567586505E+03, 0.4774908511602658E+02),
      MOM<double>( 0.1275225295696605E+03,-0.9044904129599348E+02,-0.8317830770307893E+02, 0.3409304333925805E+02),
      MOM<double>( 0.4141300683745435E+03, 0.2321455649459386E+03, 0.3327544367808187E+03,-0.8298575185244263E+02),
      MOM<double>( 0.9187515210263842E+02,-0.4335702267910112E+02,-0.2400582361400566E+02, 0.7736234607934345E+02)
    },
    {
      MOM<double>(-0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00,-0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00, 0.5000000000000000E+03),
      MOM<double>( 0.3991316067224567E+03,-0.8617243785691173E+02, 0.7133749540073053E+02, 0.3831335435440197E+03),
      MOM<double>( 0.1555698197132530E+03, 0.8491849428707653E+02,-0.6829958683623279E+02,-0.1110224507549247E+03),
      MOM<double>( 0.1275974759403907E+03,-0.9558199208396833E+02,-0.8270474276217082E+02,-0.1746780410532292E+02),
      MOM<double>( 0.2099252734731342E+03, 0.1264925739566645E+03, 0.2715431322208903E+02,-0.1653205747934134E+03),
      MOM<double>( 0.1077758241507656E+03,-0.2965663830286092E+02, 0.5251252097558412E+02,-0.8932271389035866E+02)
    },
    {
      MOM<double>(-0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00,-0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00, 0.5000000000000000E+03),
      MOM<double>( 0.2634972659706347E+03,-0.7693419679503080E+01, 0.2506497800241936E+03,-0.8090925930607581E+02),
      MOM<double>( 0.2034681353765298E+03, 0.1229734986321523E+03,-0.1560653169406342E+03, 0.4382256948182397E+02),
      MOM<double>( 0.2759452289031980E+03,-0.3230438033912978E+02,-0.2735784863429613E+03, 0.1603147453097491E+02),
      MOM<double>( 0.1233696042678511E+03,-0.1100785383209509E+03, 0.5337882674150785E+02,-0.1592091435908169E+02),
      MOM<double>( 0.1337197654817863E+03, 0.2710283970743146E+02, 0.1256151965178942E+03, 0.3697612965235862E+02)
    },
    {
      MOM<double>(-0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00,-0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00, 0.5000000000000000E+03),
      MOM<double>( 0.2766318805977961E+03, 0.1226828229955286E+03,-0.1839280235982345E+03, 0.1662666666538077E+03),
      MOM<double>( 0.3916962132808166E+03,-0.6610798502099392E+02, 0.1896068524896684E+03,-0.3363107183899378E+03),
      MOM<double>( 0.7128187076812097E+02, 0.7746251325469461E+01, 0.6345763568098539E+02,-0.3153139965788217E+02),
      MOM<double>( 0.2046646741143462E+03,-0.6365472580714265E+02,-0.1393044419275318E+02, 0.1940145547049026E+03),
      MOM<double>( 0.5572536123891997E+02,-0.6663634928614757E+00,-0.5520602037966614E+02, 0.7560896689109725E+01)
    }
  };

  const int namps = 7;

  const char* amp_names[namps] = {
    "Amp0q7g",
    "Amp2q5g",
    "Amp4q3g",
    "Amp4q3g2",
    "Amp6q1g",
    "Amp6q1g2",
    "Amp6q1g6",
  };

  const int nq[namps] = {0, 2, 4, 4, 6, 6, 6};
  const int ng[namps] = {7, 5, 3, 3, 1, 1, 1};

  NJetAmp<double>* amps[namps] = {
    new Amp0q7g<double>(1.),
    new Amp2q5g<double>(1.),
    new Amp4q3g<double>(1.),
    new Amp4q3g2<double>(1.),
    new Amp6q1g<double>(1.),
    new Amp6q1g2<double>(1.),
    new Amp6q1g6<double>(1.),
  };

  double times[namps][3][npoints];

  const EpsTriplet<double> ng1[namps][npoints] = {
    {
    // 21, 21 -> [21, 21, 21, 21, 21]
      EpsTriplet<double>(-6.2714191125773425e+01, 5.8728029891370738e+01, -4.1999999999992156e+01), /* 2.8598759504772976e-03 */
      EpsTriplet<double>(4.2297511427755687e+01, 6.0500773960098023e+01, -4.1999999999999964e+01), /* 3.2535323788799083e-03 */
      EpsTriplet<double>(-5.3020160821590963e+01, 9.3868977363314130e+01, -4.1999999999999474e+01), /* 3.6838299988979460e-05 */
      EpsTriplet<double>(-2.8385876689226568e+00, 4.8624252828002348e+01, -4.1999999999997456e+01), /* 1.4175626006714697e-02 */
    },
    {
    // 1, -1 -> [21, 21, 21, 21, 21]
      EpsTriplet<double>(-5.9922877184145172e+01, 3.7726106373219615e+01, -3.5333333333332980e+01), /* 4.9541557442083899e-06 */
      EpsTriplet<double>(2.6082590871211028e+01, 4.0600013635066048e+01, -3.5333333333331943e+01), /* 6.8829741279954073e-07 */
      EpsTriplet<double>(-5.3249686514211163e+01, 6.8251197371370864e+01, -3.5333333333303074e+01), /* 8.9717967222425871e-08 */
      EpsTriplet<double>(-3.7744497720619128e+00, 2.8639894956242546e+01, -3.5333333333302498e+01), /* 7.2868517777641960e-06 */
    },
    {
    // 1, -1 -> [-2, 2, 21, 21, 21]
      EpsTriplet<double>(-4.3672734955284433e+01, 4.3035885769998124e+01, -2.8666666666670508e+01), /* 3.1347640576559260e-09 */
      EpsTriplet<double>(-3.5766642865012095e+01, 5.4157240597416035e+01, -2.8666666666665691e+01), /* 8.9511529178258408e-09 */
      EpsTriplet<double>(-2.6064548490243581e+01, 5.6383577850974135e+01, -2.8666666666668110e+01), /* 4.6253119519883282e-10 */
      EpsTriplet<double>(-4.6733329269117107e+01, 2.5104451765390646e+01, -2.8666666666649125e+01), /* 3.0198698869147212e-07 */
    },
    {
    // 1, -1 -> [-1, 1, 21, 21, 21]
      EpsTriplet<double>(-2.9661081080420595e+00, 5.3860788820630461e+01, -2.8666666666661769e+01), /* 8.4846225935105164e-07 */
      EpsTriplet<double>(3.4434518358182471e+00, 5.4577615270927723e+01, -2.8666666666666657e+01), /* 2.1897226396507634e-08 */
      EpsTriplet<double>(1.6499047574299446e+01, 6.1593872203467875e+01, -2.8666666666666643e+01), /* 3.4891538274312173e-08 */
      EpsTriplet<double>(-5.9845162818836295e+00, 2.8065911597883254e+01, -2.8666666666661094e+01), /* 7.7430547446030756e-07 */
    },
    {
    // 1, -1 -> [-2, 2, -3, 3, 21]
      EpsTriplet<double>(-6.4643441047696072e+01, 4.0432896214472677e+01, -2.2000000000001954e+01), /* 1.8630896382726942e-11 */
      EpsTriplet<double>(-5.7012039637243447e+01, 5.3285753655558381e+01, -2.2000000000012548e+01), /* 4.0011783244554573e-11 */
      EpsTriplet<double>(-6.9830225691023912e+01, 5.6289511057091005e+01, -2.2000000000001148e+01), /* 3.2268120062422571e-12 */
      EpsTriplet<double>(-5.1837921735454266e+01, 2.9583338114663590e+01, -2.2000000000000444e+01), /* 7.0077496578708075e-11 */
    },
    {
    // 1, -1 -> [-2, 2, -2, 2, 21]
      EpsTriplet<double>(-8.0578222489714165e+01, 3.9176817717489598e+01, -2.1999999999999876e+01), /* 3.7145949552900821e-10 */
      EpsTriplet<double>(-5.5089746278046057e+01, 5.0856813246417040e+01, -2.2000000000003666e+01), /* 1.0394588998592571e-10 */
      EpsTriplet<double>(-7.8976677373158907e+01, 5.9932330899305917e+01, -2.1999999999999606e+01), /* 1.3634049492789548e-11 */
      EpsTriplet<double>(-2.4618909882111950e+01, 2.9400686066640748e+01, -2.1999999999999936e+01), /* 8.6461168138151034e-10 */
    },
    {
    // 1, -1 -> [-1, 1, -1, 1, 21]
      EpsTriplet<double>(-3.3414369951466611e+01, 4.2513898074040839e+01, -2.2000000000000181e+01), /* 1.6796372520116263e-08 */
      EpsTriplet<double>(-1.3033344004512582e+01, 5.0659176050279640e+01, -2.2000000000003006e+01), /* 3.1560061594861623e-10 */
      EpsTriplet<double>(-1.2473584077403851e+01, 5.3818006065443448e+01, -2.1999999999999904e+01), /* 3.3775808382274552e-10 */
      EpsTriplet<double>(2.7271277749277878e+01, 4.4711652574079636e+01, -2.1999999999998998e+01), /* 3.6853925828753380e-08 */
    },
  };

  const double mur = 91.188;
  const double Nc = 3.;
  const double Nf = 5.;

  EpsTriplet<double> sum_loop;
  EpsTriplet<double> sum_loop_lc;
  EpsTriplet<double> sum_loop_slc;

  bool have_lcslc;
  for (int n=0; n<namps; n++) {
    cout << "----- Amp " << amp_names[n] << " ------"<< endl;
    NJetAmp<double>* amp1 = amps[n];

    amp1->setMuR2(mur*mur);

    for (int i=0; i<npoints; i++) {
      cout << "----- POINT N " << i << " ------"<< endl;
      amp1->setMomenta(Momenta[i]);

      tm.start();
      have_lcslc = amp1->setLoopType(NJetAmpTables::COLOR_FULL);
      sum_loop = amp1->virt();
      times[n][0][i] = static_cast<double>(tm.get());

      if (have_lcslc) {
        tm.start();
        amp1->setLoopType(NJetAmpTables::COLOR_LC);
        sum_loop_lc = amp1->virt();
        times[n][1][i] = static_cast<double>(tm.get());

        tm.start();
        amp1->setLoopType(NJetAmpTables::COLOR_SLC);
        sum_loop_slc =  amp1->virt();
        times[n][2][i] = static_cast<double>(tm.get());
      }

      double sum_tree = amp1->born();
      double sum_tree_cc[legs*(legs-1)/2] = {};
      amp1->born_cc(sum_tree_cc);

      cout << "A0    " << sum_tree << endl;
      cout << "A1    " << sum_loop << endl;
      cout << "poles " << getpoles<double>(ng[n], nq[n], Nc, Nf, Momenta[i], mur*mur, sum_tree, sum_tree_cc, 1) << endl;
      cout << "A1/A0 " << sum_loop/sum_tree << endl;
      cout << "NG1:  " << ng1[n][i] << endl;
      cout << "DIFF  " << (sum_loop/sum_tree-ng1[n][i]) << endl;
      if (have_lcslc) {
        cout << endl;
        cout << "LC    " << sum_loop_lc/sum_tree << endl;
        cout << "SL    " << sum_loop_slc/sum_tree << endl;
        cout << "FULL  " << (sum_loop_lc+sum_loop_slc)/sum_tree << endl;
      }
      cout.flush();
    }
    cout << "TIME " << amp_names[n] << " FULL ";
    print_stat(npoints, times[n][0]);
    if (have_lcslc) {
      cout << "\tLC ";
      print_stat(npoints, times[n][1]);
      cout << "\tSLC ";
      print_stat(npoints, times[n][2]);
    }
    cout << endl;
  }
  for (int n=0; n<namps; n++) {
    if (amps[n]) {
      delete amps[n];
      amps[n] = 0;
    }
  }
#endif
}
//}}}

//{{{ pp3jds
void pp3jds()
{
  const int npoints = 2;
  const int legs = 5;

  const MOM<double> Momenta[][legs] = {
    {
      MOM<double>(-0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00,-0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00, 0.5000000000000000E+03),
      MOM<double>( 0.4585787878854402E+03, 0.1694532203096798E+03, 0.3796536620781987E+03,-0.1935024746502525E+03),
      MOM<double>( 0.3640666207368177E+03,-0.1832986929319185E+02,-0.3477043013193671E+03, 0.1063496077587081E+03),
      MOM<double>( 0.1773545913777421E+03,-0.1511233510164880E+03,-0.3194936075883156E+02, 0.8715286689154436E+02)
    },
    {
      MOM<double>(-0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00,-0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00, 0.5000000000000000E+03),
      MOM<double>( 0.4951533920773834E+03, 0.1867229157692120E+03, 0.3196835780850242E+03,-0.3288066974913060E+03),
      MOM<double>( 0.1026779138750091E+03,-0.7062042730772224E+02,-0.4345595295696615E+02, 0.6055649756384902E+02),
      MOM<double>( 0.4021686940476072E+03,-0.1161024884614897E+03,-0.2762276251280580E+03, 0.2682501999274569E+03)
    }
  };

  const int namps = 2;

  const char* amp_names[namps] = {
    "Amp0q5g3",
    "Amp2q3g3",
  };

  double times[namps][npoints];

  const int perm5_3[][legs] = {
    {0, 1, 2, 3, 4},
    {0, 1, 2, 4, 3},
    {0, 1, 3, 2, 4},
    {0, 1, 4, 3, 2},
    {0, 1, 3, 4, 2},
    {0, 1, 4, 2, 3},
  };

  NJetAmp<double>* amps[namps] = {
    new Amp0q5g_ds3<double>(1.),
    new Amp2q3g_ds3<double>(1.),
  };

  const int* amp_perms[namps] = {
    &perm5_3[0][0],
    &perm5_3[0][0],
  };

  const int amp_nperms[namps] = {
    3,
    6,
  };

  EpsTriplet<double> ng1[namps][npoints] = {
    {
    // 21 21 -> [21, 21, 21]
      EpsTriplet<double>(-9.5232959888216556e+01, 8.9506190702177108e+01, -2.9999999999999876e+01), /* 8.8287997984160089e+00 */
      EpsTriplet<double>(-8.1759250560802940e+01, 6.9153368752106061e+01, -2.9999999999999883e+01), /* 2.1912327096425668e+02 */
    },
    {
    // 1, -1 -> [21, 21, 21]
      EpsTriplet<double>(-8.4055257658727186e+01, 6.8836659295292989e+01, -2.3333333333333048e+01), /* 2.7563319178479928e-02 */
      EpsTriplet<double>(-6.2094456805702421e+01, 4.5707323165881697e+01, -2.3333333333333563e+01), /* 7.4884847219389628e-01 */
    },
  };

  const double mur = 91.188;
  for (int n=0; n<namps; n++) {
    cout << "----- Amp " << amp_names[n] << " ------"<< endl;
    NJetAmp<double>* amp1 = amps[n];

    amp1->setMuR2(mur*mur);

    for (int i=0; i<npoints; i++) {
      cout << "----- POINT N " << i << " ------"<< endl;
      EpsTriplet<double> sum_loop = EpsTriplet<double>();
      double sum_tree = 0;

      tm.start();
      cout << "Permuting";
      for (int p=0; p<amp_nperms[n]; p++) {
        MOM<double> pMomenta[legs];
        for (int j=0; j<legs; j++) {
          pMomenta[j] = Momenta[i][amp_perms[n][legs*p+j]];
        }
        amp1->setMomenta(pMomenta);

        EpsTriplet<double> loop = amp1->virt();
        double tree = amp1->born();
        cout << ".";

        sum_loop += loop;
        sum_tree += tree;
      }
      times[n][i] = static_cast<double>(tm.get());
      sum_tree /= double(amp_nperms[n]);
      sum_loop /= double(amp_nperms[n]);
      cout << endl;
      cout << "A0    " << sum_tree << endl;
      cout << "A1    " << sum_loop << endl;
      cout << "A1/A0 " << sum_loop/sum_tree << endl;
      cout << "NG1:  " << ng1[n][i] << endl;
      cout << "DIFF  " << (sum_loop/sum_tree-ng1[n][i]) << endl;
      cout << endl;
      cout.flush();
    }
    cout << "TIME " << amp_names[n] << " ";
    print_stat(npoints, times[n]);
    cout << endl;
  }
  for (int n=0; n<namps; n++) {
    if (amps[n]) {
      delete amps[n];
      amps[n] = 0;
    }
  }
}
//}}}

//{{{ pp4jds
void pp4jds()
{
  const int npoints = 4;
  const int legs = 6;

  const MOM<double> Momenta[][legs] = {
    {
      MOM<double>(-0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00,-0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00, 0.5000000000000000E+03),
      MOM<double>( 0.8855133305450298E+02,-0.2210069028768998E+02, 0.4008035319168533E+02,-0.7580543095693663E+02),
      MOM<double>( 0.3283294192270985E+03,-0.1038496118834563E+03,-0.3019337553895401E+03, 0.7649492138716589E+02),
      MOM<double>( 0.1523581094674306E+03,-0.1058809596665922E+03,-0.9770963832697571E+02, 0.4954838522679282E+02),
      MOM<double>( 0.4307611382509676E+03, 0.2318312618377385E+03, 0.3595630405248305E+03,-0.5023787565702211E+02)
    },
    {
      MOM<double>(-0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00,-0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00, 0.5000000000000000E+03),
      MOM<double>( 0.1177477081371708E+03,-0.6070592184636148E+02, 0.7123104586232657E+02, 0.7145244523246875E+02),
      MOM<double>( 0.3509541730779688E+03,-0.3178816670261076E+02, 0.8393942869317344E+02, 0.3392823549334560E+03),
      MOM<double>( 0.3493322285737900E+03, 0.1840093039957635E+03,-0.5152779793923704E+02,-0.2924354082577186E+03),
      MOM<double>( 0.1819658902110701E+03,-0.9151521544679123E+02,-0.1036426766162630E+03,-0.1182993919082062E+03)
    },
    {
      MOM<double>(-5.0000000000000000E+01, 0.0000000000000000E+00, 0.0000000000000000E+00,-5.0000000000000000E+01),
      MOM<double>(-5.0000000000000000E+01, 0.0000000000000000E+00, 0.0000000000000000E+00, 5.0000000000000000E+01),
      MOM<double>( 3.0830908416563354E+01,-1.5462861903492566E+01, 2.6617620401269029E+01, 1.7167118913730799E+00),
      MOM<double>( 2.5745672609866769E+01,-1.1924408694529273E+01,-8.6688190119874555E+00,-2.1106864105254559E+01),
      MOM<double>( 4.0645606131607479E+01, 2.8711125857142530E+01,-2.0051754806234289E+01, 2.0631618429288316E+01),
      MOM<double>( 2.7778128419623971E+00,-1.3238552591206938E+00, 2.1029534169527198E+00,-1.2414662154068357E+00)
    },
    {
      MOM<double>(-5.0000000000000000E+01, 0.0000000000000000E+00, 0.0000000000000000E+00,-5.0000000000000000E+01),
      MOM<double>(-5.0000000000000000E+01, 0.0000000000000000E+00, 0.0000000000000000E+00, 5.0000000000000000E+01),
      MOM<double>( 1.7658726533284433E+01, 1.3403174828466632E+01, 6.6749400858894896E+00, 9.3611271834468557E+00),
      MOM<double>( 3.9791943733796067E+01,-3.0687550194649063E+01,-2.4819226609719482E+01, 5.0674490288386460E+00),
      MOM<double>( 3.4133057638993364E+01, 2.2521276138666977E+01, 1.5476870081140326E+01,-2.0452976247095574E+01),
      MOM<double>( 8.4162720939261479E+00,-5.2369007724845442E+00, 2.6674164426896647E+00, 6.0244000348100739E+00)
    }
  };
  const int perm6_3[][legs] = {
    {0, 1, 2, 3, 4, 5},
    {0, 1, 2, 3, 5, 4},
    {0, 1, 2, 4, 3, 5},
    {0, 1, 2, 4, 5, 3},
    {0, 1, 2, 5, 3, 4},
    {0, 1, 2, 5, 4, 3}
  };
  const int perm6_4[][legs] = {
    {0, 1, 2, 3, 4, 5},
    {0, 1, 2, 4, 5, 3},
    {0, 1, 3, 2, 4, 5},
    {0, 1, 2, 4, 3, 5},
    {0, 1, 3, 5, 2, 4},
    {0, 1, 2, 3, 5, 4},
    {0, 1, 4, 2, 3, 5},
    {0, 1, 2, 5, 3, 4},
    {0, 1, 3, 4, 2, 5},
    {0, 1, 2, 5, 4, 3},
    {0, 1, 4, 3, 2, 5},
    {0, 1, 3, 2, 5, 4},
    {0, 1, 3, 4, 5, 2},
    {0, 1, 3, 5, 4, 2},
    {0, 1, 4, 2, 5, 3},
    {0, 1, 4, 3, 5, 2},
    {0, 1, 4, 5, 2, 3},
    {0, 1, 4, 5, 3, 2},
    {0, 1, 5, 2, 3, 4},
    {0, 1, 5, 2, 4, 3},
    {0, 1, 5, 3, 2, 4},
    {0, 1, 5, 3, 4, 2},
    {0, 1, 5, 4, 2, 3},
    {0, 1, 5, 4, 3, 2}
  };


  const int namps = 3;

  const char* amp_names[namps] = {
    "Amp0q6g4",
    "Amp2q4g3",
    "Amp2q4g4",
  };

  double times[namps][npoints];

  NJetAmp<double>* amps[namps] = {
    new Amp0q6g_ds4<double>(1.),
    new Amp2q4g_ds3<double>(1.),
    new Amp2q4g_ds4<double>(1.),
  };

  const int* amp_perms[namps] = {
    &perm6_4[0][0],
    &perm6_3[0][0],
    &perm6_4[0][0],
  };
  const int amp_nperms[namps] = {
    12,
    6,
    24,
  };

  EpsTriplet<double> ng1[namps][npoints] = {
    {
    // 21, 21 -> [21, 21, 21, 21]
      EpsTriplet<double>(-8.3100840602237710e+01, 7.0992551348527527e+01, -3.6000000000000668e+01), /* 2.0244053477393464e-01 */
      EpsTriplet<double>(8.1451742970829741e+00, 5.9357109422333941e+01, -3.5999999999995453e+01), /* 1.0562143598284039e+00 */
      EpsTriplet<double>(-1.9640154223926834e+02, -1.0362806164692095e+02, -3.6000000000001755e+01), /* 9.3751737039745167e+03 */
      EpsTriplet<double>(-6.6484138498486871e+01, -8.3695388894941587e+01, -3.6000000000000099e+01), /* 5.0455584104309457e+02 */
    },
    {
    // 1, -1 -> [21, 21, 21, 21]
      EpsTriplet<double>(-7.5767617610453740e+01, 5.2031131371199734e+01, -2.9333333333333798e+01), /* 5.3097829544836866e-04 */
      EpsTriplet<double>(3.2197129030131793e+00, 3.8699080751763923e+01, -2.9333333333333396e+01), /* 3.4534309273034230e-04 */
      EpsTriplet<double>(-2.0926656272506983e+02, -9.1155283475598367e+01, -2.9333333333341951e+01), /* 1.3800796471994691e+01 */
      EpsTriplet<double>(-7.0098110700024350e+01, -6.8142986689314000e+01, -2.9333333333333215e+01), /* 7.4199337381469699e-01 */
    },
    {
    // 1, -1 -> [21, 21, 21, 21]
      EpsTriplet<double>(-7.5767617610453740e+01, 5.2031131371199734e+01, -2.9333333333333798e+01), /* 5.3097829544836866e-04 */
      EpsTriplet<double>(3.2197129030131793e+00, 3.8699080751763923e+01, -2.9333333333333396e+01), /* 3.4534309273034230e-04 */
      EpsTriplet<double>(-2.0926656272506983e+02, -9.1155283475598367e+01, -2.9333333333341951e+01), /* 1.3800796471994691e+01 */
      EpsTriplet<double>(-7.0098110700024350e+01, -6.8142986689314000e+01, -2.9333333333333215e+01), /* 7.4199337381469699e-01 */
    },
  };

  const double mur = 91.188;
  for (int n=0; n<namps; n++) {
    cout << "----- Amp " << amp_names[n] << " ------"<< endl;
    NJetAmp<double>* amp1 = amps[n];

    amp1->setMuR2(mur*mur);

    for (int i=0; i<npoints; i++) {
      cout << "----- POINT N " << i << " ------"<< endl;
      EpsTriplet<double> sum_loop = EpsTriplet<double>();
      double sum_tree = 0;

      tm.start();
      cout << "Permuting";
      for (int p=0; p<amp_nperms[n]; p++) {
        MOM<double> pMomenta[legs];
        for (int j=0; j<legs; j++) {
          pMomenta[j] = Momenta[i][amp_perms[n][legs*p+j]];
        }
        amp1->setMomenta(pMomenta);

        EpsTriplet<double> loop = amp1->virt();
        double tree = amp1->born();
        cout << ".";

        sum_loop += loop;
        sum_tree += tree;
      }
      times[n][i] = static_cast<double>(tm.get());
      sum_tree /= double(amp_nperms[n]);
      sum_loop /= double(amp_nperms[n]);
      cout << endl;
      cout << "A0    " << sum_tree << endl;
      cout << "A1    " << sum_loop << endl;
      cout << "A1/A0 " << sum_loop/sum_tree << endl;
      cout << "NG1:  " << ng1[n][i] << endl;
      cout << "DIFF  " << (sum_loop/sum_tree-ng1[n][i]) << endl;
      cout << endl;
      cout.flush();
    }
    cout << "TIME " << amp_names[n] << " ";
    print_stat(npoints, times[n]);
    cout << endl;
  }
  for (int n=0; n<namps; n++) {
    if (amps[n]) {
      delete amps[n];
      amps[n] = 0;
    }
  }
}
//}}}

//{{{ pp5jds
void pp5jds()
{
#ifdef NJET_ENABLE_5
  const int npoints = 4;
  const int legs = 7;

  const MOM<double> Momenta[][legs] = {
    {
      MOM<double>(-0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00,-0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00, 0.5000000000000000E+03),
      MOM<double>( 0.8635406814378138E+02,-0.1521338932026180E+02, 0.3763355129491627E+02,-0.7621872268218542E+02),
      MOM<double>( 0.2801181818093764E+03,-0.8312611165058223E+02,-0.2632038567586505E+03, 0.4774908511602658E+02),
      MOM<double>( 0.1275225295696605E+03,-0.9044904129599348E+02,-0.8317830770307893E+02, 0.3409304333925805E+02),
      MOM<double>( 0.4141300683745435E+03, 0.2321455649459386E+03, 0.3327544367808187E+03,-0.8298575185244263E+02),
      MOM<double>( 0.9187515210263842E+02,-0.4335702267910112E+02,-0.2400582361400566E+02, 0.7736234607934345E+02)
    },
    {
      MOM<double>(-0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00,-0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00, 0.5000000000000000E+03),
      MOM<double>( 0.3991316067224567E+03,-0.8617243785691173E+02, 0.7133749540073053E+02, 0.3831335435440197E+03),
      MOM<double>( 0.1555698197132530E+03, 0.8491849428707653E+02,-0.6829958683623279E+02,-0.1110224507549247E+03),
      MOM<double>( 0.1275974759403907E+03,-0.9558199208396833E+02,-0.8270474276217082E+02,-0.1746780410532292E+02),
      MOM<double>( 0.2099252734731342E+03, 0.1264925739566645E+03, 0.2715431322208903E+02,-0.1653205747934134E+03),
      MOM<double>( 0.1077758241507656E+03,-0.2965663830286092E+02, 0.5251252097558412E+02,-0.8932271389035866E+02)
    },
    {
      MOM<double>(-0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00,-0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00, 0.5000000000000000E+03),
      MOM<double>( 0.2634972659706347E+03,-0.7693419679503080E+01, 0.2506497800241936E+03,-0.8090925930607581E+02),
      MOM<double>( 0.2034681353765298E+03, 0.1229734986321523E+03,-0.1560653169406342E+03, 0.4382256948182397E+02),
      MOM<double>( 0.2759452289031980E+03,-0.3230438033912978E+02,-0.2735784863429613E+03, 0.1603147453097491E+02),
      MOM<double>( 0.1233696042678511E+03,-0.1100785383209509E+03, 0.5337882674150785E+02,-0.1592091435908169E+02),
      MOM<double>( 0.1337197654817863E+03, 0.2710283970743146E+02, 0.1256151965178942E+03, 0.3697612965235862E+02)
    },
    {
      MOM<double>(-0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00,-0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00, 0.5000000000000000E+03),
      MOM<double>( 0.2766318805977961E+03, 0.1226828229955286E+03,-0.1839280235982345E+03, 0.1662666666538077E+03),
      MOM<double>( 0.3916962132808166E+03,-0.6610798502099392E+02, 0.1896068524896684E+03,-0.3363107183899378E+03),
      MOM<double>( 0.7128187076812097E+02, 0.7746251325469461E+01, 0.6345763568098539E+02,-0.3153139965788217E+02),
      MOM<double>( 0.2046646741143462E+03,-0.6365472580714265E+02,-0.1393044419275318E+02, 0.1940145547049026E+03),
      MOM<double>( 0.5572536123891997E+02,-0.6663634928614757E+00,-0.5520602037966614E+02, 0.7560896689109725E+01)
    }
  };
  const int perm7_3[][legs] = {
    {0, 1, 2, 3, 4, 5, 6},
    {0, 1, 2, 3, 4, 6, 5},
    {0, 1, 2, 3, 5, 4, 6},
    {0, 1, 2, 3, 5, 6, 4},
    {0, 1, 2, 3, 6, 4, 5},
    {0, 1, 2, 3, 6, 5, 4}
  };
  const int perm7_4[][legs] = {
    {0,1,2,3,4,5,6},
    {0,1,2,3,4,6,5},
    {0,1,2,3,5,4,6},
    {0,1,2,3,5,6,4},
    {0,1,2,3,6,4,5},
    {0,1,2,3,6,5,4},
    {0,1,2,4,3,5,6},
    {0,1,2,4,3,6,5},
    {0,1,2,4,5,3,6},
    {0,1,2,4,5,6,3},
    {0,1,2,4,6,3,5},
    {0,1,2,4,6,5,3},
    {0,1,2,5,3,4,6},
    {0,1,2,5,3,6,4},
    {0,1,2,5,4,3,6},
    {0,1,2,5,4,6,3},
    {0,1,2,5,6,3,4},
    {0,1,2,5,6,4,3},
    {0,1,2,6,3,4,5},
    {0,1,2,6,3,5,4},
    {0,1,2,6,4,3,5},
    {0,1,2,6,4,5,3},
    {0,1,2,6,5,3,4},
    {0,1,2,6,5,4,3}
  };
  const int perm7_5[][legs] = {
    {0,1,2,3,4,5,6},
    {0,1,2,3,4,6,5},
    {0,1,2,3,5,4,6},
    {0,1,2,3,5,6,4},
    {0,1,2,3,6,4,5},
    {0,1,2,3,6,5,4},
    {0,1,2,4,3,5,6},
    {0,1,2,4,3,6,5},
    {0,1,2,4,5,3,6},
    {0,1,2,4,5,6,3},
    {0,1,2,4,6,3,5},
    {0,1,2,4,6,5,3},
    {0,1,2,5,3,4,6},
    {0,1,2,5,3,6,4},
    {0,1,2,5,4,3,6},
    {0,1,2,5,4,6,3},
    {0,1,2,5,6,3,4},
    {0,1,2,5,6,4,3},
    {0,1,2,6,3,4,5},
    {0,1,2,6,3,5,4},
    {0,1,2,6,4,3,5},
    {0,1,2,6,4,5,3},
    {0,1,2,6,5,3,4},
    {0,1,2,6,5,4,3},
    {0,1,3,2,4,5,6},
    {0,1,3,2,4,6,5},
    {0,1,3,2,5,4,6},
    {0,1,3,2,5,6,4},
    {0,1,3,2,6,4,5},
    {0,1,3,2,6,5,4},
    {0,1,3,4,2,5,6},
    {0,1,3,4,2,6,5},
    {0,1,3,4,5,2,6},
    {0,1,3,4,6,2,5},
    {0,1,3,5,2,4,6},
    {0,1,3,5,2,6,4},
    {0,1,3,5,4,2,6},
    {0,1,3,5,6,2,4},
    {0,1,3,6,2,4,5},
    {0,1,3,6,2,5,4},
    {0,1,3,6,4,2,5},
    {0,1,3,6,5,2,4},
    {0,1,4,2,3,5,6},
    {0,1,4,2,3,6,5},
    {0,1,4,2,5,3,6},
    {0,1,4,2,6,3,5},
    {0,1,4,3,2,5,6},
    {0,1,4,3,2,6,5},
    {0,1,4,3,5,2,6},
    {0,1,4,3,6,2,5},
    {0,1,4,5,2,3,6},
    {0,1,4,5,3,2,6},
    {0,1,4,6,2,3,5},
    {0,1,4,6,3,2,5},
    {0,1,5,2,3,4,6},
    {0,1,5,2,4,3,6},
    {0,1,5,3,2,4,6},
    {0,1,5,3,4,2,6},
    {0,1,5,4,2,3,6},
    {0,1,5,4,3,2,6},
    {0,1,3,4,5,6,2},
    {0,1,3,4,6,5,2},
    {0,1,3,5,4,6,2},
    {0,1,3,5,6,4,2},
    {0,1,3,6,4,5,2},
    {0,1,3,6,5,4,2},
    {0,1,4,2,5,6,3},
    {0,1,4,2,6,5,3},
    {0,1,4,3,5,6,2},
    {0,1,4,3,6,5,2},
    {0,1,4,5,2,6,3},
    {0,1,4,5,3,6,2},
    {0,1,4,5,6,2,3},
    {0,1,4,5,6,3,2},
    {0,1,4,6,2,5,3},
    {0,1,4,6,3,5,2},
    {0,1,4,6,5,2,3},
    {0,1,4,6,5,3,2},
    {0,1,5,2,3,6,4},
    {0,1,5,2,4,6,3},
    {0,1,5,2,6,3,4},
    {0,1,5,2,6,4,3},
    {0,1,5,3,2,6,4},
    {0,1,5,3,4,6,2},
    {0,1,5,3,6,2,4},
    {0,1,5,3,6,4,2},
    {0,1,5,4,2,6,3},
    {0,1,5,4,3,6,2},
    {0,1,5,4,6,2,3},
    {0,1,5,4,6,3,2},
    {0,1,5,6,2,3,4},
    {0,1,5,6,2,4,3},
    {0,1,5,6,3,2,4},
    {0,1,5,6,3,4,2},
    {0,1,5,6,4,2,3},
    {0,1,5,6,4,3,2},
    {0,1,6,2,3,4,5},
    {0,1,6,2,3,5,4},
    {0,1,6,2,4,3,5},
    {0,1,6,2,4,5,3},
    {0,1,6,2,5,3,4},
    {0,1,6,2,5,4,3},
    {0,1,6,3,2,4,5},
    {0,1,6,3,2,5,4},
    {0,1,6,3,4,2,5},
    {0,1,6,3,4,5,2},
    {0,1,6,3,5,2,4},
    {0,1,6,3,5,4,2},
    {0,1,6,4,2,3,5},
    {0,1,6,4,2,5,3},
    {0,1,6,4,3,2,5},
    {0,1,6,4,3,5,2},
    {0,1,6,4,5,2,3},
    {0,1,6,4,5,3,2},
    {0,1,6,5,2,3,4},
    {0,1,6,5,2,4,3},
    {0,1,6,5,3,2,4},
    {0,1,6,5,3,4,2},
    {0,1,6,5,4,2,3},
    {0,1,6,5,4,3,2}
  };

  const int namps = 4;

  const char* amp_names[namps] = {
    "Amp0q7g5",
    "Amp2q5g3",
    "Amp2q5g4",
    "Amp2q5g5",
  };

  double times[namps][npoints];

  NJetAmp<double>* amps[namps] = {
    new Amp0q7g_ds5<double>(1.),
    new Amp2q5g_ds3<double>(1.),
    new Amp2q5g_ds4<double>(1.),
    new Amp2q5g_ds5<double>(1.),
  };

  const int* amp_perms[namps] = {
    &perm7_5[0][0],
    &perm7_3[0][0],
    &perm7_4[0][0],
    &perm7_5[0][0],
  };
  const int amp_nperms[namps] = {
    60,
    6,
    24,
    120,
  };

  EpsTriplet<double> ng1[namps][npoints] = {
    {
    // 21, 21 -> [21, 21, 21, 21, 21]
      EpsTriplet<double>(-6.2714191125773425e+01, 5.8728029891370738e+01, -4.1999999999992156e+01), /* 2.8598759504772976e-03 */
      EpsTriplet<double>(4.2297511427755687e+01, 6.0500773960098023e+01, -4.1999999999999964e+01), /* 3.2535323788799083e-03 */
      EpsTriplet<double>(-5.3020160821590963e+01, 9.3868977363314130e+01, -4.1999999999999474e+01), /* 3.6838299988979460e-05 */
      EpsTriplet<double>(-2.8385876689226568e+00, 4.8624252828002348e+01, -4.1999999999997456e+01), /* 1.4175626006714697e-02 */
    },
    {
    // 1, -1 -> [21, 21, 21, 21, 21]
      EpsTriplet<double>(-5.9922877184145172e+01, 3.7726106373219615e+01, -3.5333333333332980e+01), /* 4.9541557442083899e-06 */
      EpsTriplet<double>(2.6082590871211028e+01, 4.0600013635066048e+01, -3.5333333333331943e+01), /* 6.8829741279954073e-07 */
      EpsTriplet<double>(-5.3249686514211163e+01, 6.8251197371370864e+01, -3.5333333333303074e+01), /* 8.9717967222425871e-08 */
      EpsTriplet<double>(-3.7744497720619128e+00, 2.8639894956242546e+01, -3.5333333333302498e+01), /* 7.2868517777641960e-06 */
    },
    {
    // 1, -1 -> [21, 21, 21, 21, 21]
      EpsTriplet<double>(-5.9922877184145172e+01, 3.7726106373219615e+01, -3.5333333333332980e+01), /* 4.9541557442083899e-06 */
      EpsTriplet<double>(2.6082590871211028e+01, 4.0600013635066048e+01, -3.5333333333331943e+01), /* 6.8829741279954073e-07 */
      EpsTriplet<double>(-5.3249686514211163e+01, 6.8251197371370864e+01, -3.5333333333303074e+01), /* 8.9717967222425871e-08 */
      EpsTriplet<double>(-3.7744497720619128e+00, 2.8639894956242546e+01, -3.5333333333302498e+01), /* 7.2868517777641960e-06 */
    },
    {
    // 1, -1 -> [21, 21, 21, 21, 21]
      EpsTriplet<double>(-5.9922877184145172e+01, 3.7726106373219615e+01, -3.5333333333332980e+01), /* 4.9541557442083899e-06 */
      EpsTriplet<double>(2.6082590871211028e+01, 4.0600013635066048e+01, -3.5333333333331943e+01), /* 6.8829741279954073e-07 */
      EpsTriplet<double>(-5.3249686514211163e+01, 6.8251197371370864e+01, -3.5333333333303074e+01), /* 8.9717967222425871e-08 */
      EpsTriplet<double>(-3.7744497720619128e+00, 2.8639894956242546e+01, -3.5333333333302498e+01), /* 7.2868517777641960e-06 */
    },
  };

  const double mur = 91.188;
  for (int n=0; n<namps; n++) {
    cout << "----- Amp " << amp_names[n] << " ------"<< endl;
    NJetAmp<double>* amp1 = amps[n];

    amp1->setMuR2(mur*mur);

    for (int i=0; i<npoints; i++) {
      cout << "----- POINT N " << i << " ------"<< endl;
      EpsTriplet<double> sum_loop = EpsTriplet<double>();
      double sum_tree = 0;

      tm.start();
      cout << "Permuting";
      for (int p=0; p<amp_nperms[n]; p++) {
        MOM<double> pMomenta[legs];
        for (int j=0; j<legs; j++) {
          pMomenta[j] = Momenta[i][amp_perms[n][legs*p+j]];
        }
        amp1->setMomenta(pMomenta);

        EpsTriplet<double> loop = amp1->virt();
        double tree = amp1->born();
        cout << ".";

        sum_loop += loop;
        sum_tree += tree;
      }
      times[n][i] = static_cast<double>(tm.get());
      sum_tree /= double(amp_nperms[n]);
      sum_loop /= double(amp_nperms[n]);
      cout << endl;
      cout << "A0    " << sum_tree << endl;
      cout << "A1    " << sum_loop << endl;
      cout << "A1/A0 " << sum_loop/sum_tree << endl;
      cout << "NG1:  " << ng1[n][i] << endl;
      cout << "DIFF  " << (sum_loop/sum_tree-ng1[n][i]) << endl;
      cout << endl;
      cout.flush();
    }
    cout << "TIME " << amp_names[n] << " ";
    print_stat(npoints, times[n]);
    cout << endl;
  }
  for (int n=0; n<namps; n++) {
    if (amps[n]) {
      delete amps[n];
      amps[n] = 0;
    }
  }
#endif
}
//}}}

//{{{ BH_V3j
template <typename T>
void BH_V3j()
{
  double mW = 80.419002445756163;
  double gW = 2.0476;
  double mZ = 91.1876;
  double gZ = 2.48;
  double se2 = 0.230; // sin(theta_EW)^2
  StandardModel::setWmass(mW, gW);
  StandardModel::setZmass(mZ, gZ);
  StandardModel::setSinThetaWSq(se2);

  T mu = T(7.);
  T theta = T(M_PI/4.);
  T phi = T(M_PI/6.);
  T alpha = T(M_PI/3.);
  T gamma = T(2.*M_PI/3.);
  T st = sin(theta);
  T ct = cos(theta);
  T sp = sin(phi);
  T cp = cos(phi);
  T sa = sin(alpha);
  T ca = cos(alpha);
  T sg = sin(gamma);
  T cg = cos(gamma);
  T cb = -T(37./128.);
  T sb = sin(acos(cb));

  MOM<T> k1 = -(mu/T(2.))*MOM<T>(T(1.), -st, -ct*sp, -ct*cp);
  MOM<T> k2 = -(mu/T(2.))*MOM<T>(T(1.), st, ct*sp, ct*cp);
  MOM<T> k3 =  (mu/T(3.))*MOM<T>(T(1.), T(1.), T(0.), T(0.));
  MOM<T> k4 =  (mu/T(8.))*MOM<T>(T(1.), cb, sb, T(0.));
  MOM<T> k5 =  (mu/T(10.))*MOM<T>(T(1.), ca*cb, ca*sb, sa);
  MOM<T> k6 =  (mu/T(12.))*MOM<T>(T(1.), cg*cb, cg*sb, sg);
  MOM<T> k7 = -k1-k2-k3-k4-k5-k6;
  MOM<T> BHtestmom[7] = {k1, k2, k3, k4, k5, k6, k7};

  double Nc = 3.;
  double Nf = 5.;

  cout << "# test the points from 0907.1984 and 1004.1659" << endl;
  Flavour<double> W = StandardModel::Wm(StandardModel::u(), StandardModel::ubar(), 1., mW, gW);
  Flavour<double> Z = StandardModel::Zu(StandardModel::u(), StandardModel::ubar(), mZ, gZ);

  const int namps = 7;

  EpsTriplet<T> BHcheck[namps] = {
    EpsTriplet<T>(T(-13.97991225), T(-42.34303628), T(-11.66666667)),
    EpsTriplet<T>(T(1.778061330), T(-32.37677210), T(-8.333333333)),
    EpsTriplet<T>(T(1.035000256), T(-32.40807165), T(-8.333333333)),
    EpsTriplet<T>(T( 0.4788030624), T(-32.50750136), T(-8.333333333)),
    EpsTriplet<T>(T(-15.2326082853), T(-42.3279266518), T(-11.66666667)),
    EpsTriplet<T>(T(5.2374716277), T(-32.3745606495), T(-8.3333333333)),
    EpsTriplet<T>(T(0.4387412994),T(-32.5180902998),T(-8.3333333333))
  };

  std::string process[namps] = {
    "### u~ g > g g d~ W- > e- ve~ ",
    "### u~ c > c d~ g W- > e- ve~ ",
    "### u~ u > u d~ g W- > e- ve~ ",
    "### u~ d > d d~ g W- > e- ve~ ",
    "### u~ g > g g u~ Z/g* > e- e+",
    "### u~ d > d u~ g Z/g* > e- e+",
    "### u~ u > u u~ g Z/g* > e- e+",
  };

  NJetAmp<T>* amp[namps] = {
    new Amp2q3gV<T>(W, 1.),
    new Amp4q1gV<T>(W, 1.),
    new Amp4q1gV2<T>(W, 1.),
    new Amp4q1gV2b<T>(W, 1.),
    new Amp2q3gZ<T>(Z, 1.),
    new Amp4q1gZd<T>(Z, 1.),
    new Amp4q1gZ2<T>(Z, 1.),
  };

  const int ord[namps][7] = {
    {4,0,1,2,3,5,6},
    {3,0,1,2,4,5,6},
    {3,0,1,2,4,5,6},
    {1,0,3,2,4,5,6},
    {4,0,1,2,3,5,6},
    {3,0,1,2,4,5,6},
    {3,0,1,2,4,5,6},
  };

  int nq[namps] = {2, 4, 4, 4, 2, 4, 4};
  int ng[namps] = {3, 1, 1, 1, 3, 1, 1};
  T MuR2 = mu*mu;
  for (int i=0; i<namps; i++) {
    cout << process[i] << endl;
    amp[i]->setMuR2(MuR2);
    amp[i]->setNc(Nc);
    amp[i]->setNf(Nf);

    MOM<T> mm[7];
    for (int j=0; j<7; j++) mm[j] = BHtestmom[ord[i][j]];
    amp[i]->setMomenta(mm);

    T tree = amp[i]->born();
    EpsTriplet<T> loop = amp[i]->virt() + getrenorm<T>(ng[i], nq[i], T(Nc), T(Nf), tree);
    T tree_cc[36] = {};
    amp[i]->born_cc(tree_cc);

    cout << "A0    " << tree << "\n";
    cout << "A1    " << loop << "\n";
    cout << "poles " << getpoles<T>(ng[i], nq[i], T(Nc), T(Nf), mm, MuR2, tree, tree_cc) << "\n";
    EpsTriplet<T> ans1_ratio = loop/tree*0.5;
    cout << "A1/A0 " << ans1_ratio << endl;
    cout << "BH    " << BHcheck[i] << "\n  diff("<< BHcheck[i]-ans1_ratio << ")" << endl;
  }
  cout << endl;

  for (int n=0; n<namps; n++) {
    if (amp[n]) {
      delete amp[n];
      amp[n] = 0;
    }
  }
}

//  MOM<T> BHtestmom[7] = {k5, k1, k2, k3, k4, k6, k7};
//  MOM<T> BHtestmom2[7] = {k4, k1, k2, k3, k5, k6, k7};
//  MOM<T> BHtestmom3[7] = {k4, k1, k2, k3, k5, k6, k7};
//  MOM<T> BHtestmom4[7] = {k2, k1, k4, k3, k5, k6, k7};
//  MOM<T> BHtestmom[7] = {k5, k1, k2, k3, k4, k6, k7};
//}}}

//{{{ analytic4
void analytic4()
{
  const int legs = 4;
  const int npoints = 5;

  MOM<double> Momenta[npoints][legs] = {
    {
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00, -0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00,  0.5000000000000000E+03),
      MOM<double>( 0.4999999999999998E+03,  0.1109242844438328E+03,  0.4448307894881214E+03, -0.1995529299308788E+03),
      MOM<double>( 0.5000000000000000E+03, -0.1109242844438328E+03, -0.4448307894881214E+03,  0.1995529299308787E+03)
    },{
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00, -0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00,  0.5000000000000000E+03),
      MOM<double>( 0.5000000000000000E+03, -0.2819155058093908E+03, -0.3907909666011574E+03,  0.1334393795218208E+03),
      MOM<double>( 0.5000000000000002E+03,  0.2819155058093908E+03,  0.3907909666011575E+03, -0.1334393795218208E+03)
    },{
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00, -0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00,  0.5000000000000000E+03),
      MOM<double>( 0.4999999999999990E+03, -0.2406210290937675E+03,  0.2804720059911282E+03, -0.3368040590805997E+03),
      MOM<double>( 0.5000000000000012E+03,  0.2406210290937674E+03, -0.2804720059911282E+03,  0.3368040590805997E+03)
    },{
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00, -0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00,  0.5000000000000000E+03),
      MOM<double>( 0.5000000000000001E+03,  0.4510352131035046E+03,  0.6047968715179463E+02, -0.2071459485065961E+03),
      MOM<double>( 0.5000000000000001E+03, -0.4510352131035046E+03, -0.6047968715179463E+02,  0.2071459485065960E+03)
    },{
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00, -0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00,  0.5000000000000000E+03),
      MOM<double>( 0.5000000000000007E+03,  0.4557289838719782E+03, -0.2036894622960882E+03, -0.2866524391218178E+02),
      MOM<double>( 0.5000000000000007E+03, -0.4557289838719784E+03,  0.2036894622960884E+03,  0.2866524391218240E+02)
    }
  };

  const int namps = 2;

  const char* amp_names[namps] = {
    "Amp0q4g",
    "Amp0q4g-analytic",
  };

  NJetAmp<double>* amps[namps] = {
    new Amp0q4g<double>(1.),
    new Amp0q4g_a<double>(1.),
  };

  double times[2][namps][npoints];

  int ng[] = {4, 4};
  int nq[] = {0, 0};

  const double mur = 91.188;
  const double Nc = 3.;
  const double Nf = 0.;
  for (int n=0; n<namps; n++) {
    cout << "----- Amp " << amp_names[n] << " ------"<< endl;
    NJetAmp<double>* amp1 = amps[n];
    amp1->setMuR2(mur*mur);
    amp1->setNf(Nf);
    amp1->setNc(Nc);

    for (int i=0; i<1; i++) {
      cout << "----- POINT N " << i << " ------"<< endl;
      amp1->setMomenta(Momenta[i]);
      tm.start();
      double sum_tree = amp1->born();
      times[0][n][i] = static_cast<double>(tm.get());
      tm.start();
      EpsTriplet<double> sum_virt = amp1->virt();
      times[1][n][i] = static_cast<double>(tm.get());


      cout << "A0    " << sum_tree << endl;
      cout << "A1    " << sum_virt << endl;
      cout << "A1/A0 " << sum_virt/sum_tree << endl;
      cout.flush();
    }

    cout << "TIME " << amp_names[n] << " BORN ";
    print_stat(npoints, times[0][n]);
    cout << endl;
    cout << "TIME " << amp_names[n] << " VIRT ";
    print_stat(npoints, times[1][n]);
    cout << endl;
  }
  for (int n=0; n<namps; n++) {
    if (amps[n]) {
      delete amps[n];
      amps[n] = 0;
    }
  }
}
//}}}

//{{{ analytic5
void analytic5()
{
  const int legs = 5;
  const int npoints = 5;

  MOM<double> Momenta[npoints][legs] = {
    {
      MOM<double>(-0.5000000000000000E+03,    0.0000000000000000E+00,    0.0000000000000000E+00,   -0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03,    0.0000000000000000E+00,    0.0000000000000000E+00,    0.5000000000000000E+03),
      MOM<double>( 0.4585787878854402E+03,    0.1694532203096798E+03,    0.3796536620781987E+03,   -0.1935024746502525E+03),
      MOM<double>( 0.3640666207368177E+03,   -0.1832986929319185E+02,   -0.3477043013193671E+03,    0.1063496077587081E+03),
      MOM<double>( 0.1773545913777421E+03,   -0.1511233510164880E+03,   -0.3194936075883156E+02,    0.8715286689154436E+02)
    },{
      MOM<double>(-0.5000000000000000E+03,    0.0000000000000000E+00,    0.0000000000000000E+00,   -0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03,    0.0000000000000000E+00,    0.0000000000000000E+00,    0.5000000000000000E+03),
      MOM<double>( 0.4951533920773834E+03,    0.1867229157692120E+03,    0.3196835780850242E+03,   -0.3288066974913060E+03),
      MOM<double>( 0.1026779138750091E+03,   -0.7062042730772224E+02,   -0.4345595295696615E+02,    0.6055649756384902E+02),
      MOM<double>( 0.4021686940476072E+03,   -0.1161024884614897E+03,   -0.2762276251280580E+03,    0.2682501999274569E+03)
    },{
      MOM<double>(-0.500000000000000E+03,    0.000000000000000E+00,    0.000000000000000E+00,   -0.500000000000000E+03),
      MOM<double>(-0.500000000000000E+03,    0.000000000000000E+00,    0.000000000000000E+00,    0.500000000000000E+03),
      MOM<double>( 0.201434736015050E+03,    0.146039268347216E+03,   -0.112739931833677E+03,   -0.808590919080865E+02),
      MOM<double>( 0.457701082882854E+03,   -0.380677189626372E+03,   -0.737760145061084E+02,    0.243171252934838E+03),
      MOM<double>( 0.340864181102097E+03,    0.234637921279156E+03,    0.186515946339786E+03,   -0.162312161026751E+03)
    },{
      MOM<double>(-0.500000000000000E+03,    0.000000000000000E+00,    0.000000000000000E+00,   -0.500000000000000E+03),
      MOM<double>(-0.500000000000000E+03,    0.000000000000000E+00,    0.000000000000000E+00,    0.500000000000000E+03),
      MOM<double>( 0.185061893090239E+03,   -0.510144415070667E+02,   -0.823496764130983E+02,   -0.157683105710546E+03),
      MOM<double>( 0.419238112025642E+03,   -0.116508760132031E+03,    0.402710898576657E+03,   -0.319930537826527E+01),
      MOM<double>( 0.395699994884119E+03,    0.167523201639097E+03,   -0.320361222163558E+03,    0.160882411088811E+03)
    },{
      MOM<double>(-0.500000000000000E+03,    0.000000000000000E+00,    0.000000000000000E+00,   -0.500000000000000E+03),
      MOM<double>(-0.500000000000000E+03,    0.000000000000000E+00,    0.000000000000000E+00,    0.500000000000000E+03),
      MOM<double>( 0.448701953177064E+03,    0.202867960574524E+02,   -0.448241389162388E+03,    0.124327487215541E+01),
      MOM<double>( 0.225360245607144E+03,   -0.154877907100298E+03,    0.155579800846739E+03,   -0.509411401603107E+02),
      MOM<double>( 0.325937801215793E+03,    0.134591111042846E+03,    0.292661588315649E+03,    0.496978652881552E+02)
      }
  };

  const int namps = 2;
  const char* amp_names[namps] = {
    "Amp0q5g",
    "Amp0q5g-analytic",
  };

  NJetAmp<double>* amps[namps] = {
    new Amp0q5g<double>(1.),
    new Amp0q5g_a<double>(1.),
  };

  double times[namps][npoints];

  int ng[] = {5, 5};
  int nq[] = {0, 0};

  const double mur = 91.188;
  const double Nc = 3.;
  const double Nf = 5.;
  for (int n=0; n<namps; n++) {
    cout << "----- Amp " << amp_names[n] << " ------"<< endl;
    NJetAmp<double>* amp1 = amps[n];
    amp1->setMuR2(mur*mur);
    amp1->setNf(Nf);

    for (int i=0; i<npoints; i++) {
      cout << "----- POINT N " << i << " ------"<< endl;
      amp1->setMomenta(Momenta[i]);
      tm.start();
      double sum_tree = amp1->born();
      times[n][i] = static_cast<double>(tm.get());

      cout << "A0    " << sum_tree << endl;
      cout.flush();
    }
    cout << "TIME " << amp_names[n] << " FULL ";
    print_stat(npoints, times[n]);
    cout << endl;
  }
  for (int n=0; n<namps; n++) {
    if (amps[n]) {
      delete amps[n];
      amps[n] = 0;
    }
  }
}
//}}}

//{{{ analytic6
void analytic6()
{
  const int legs = 6;
  const int npoints = 4;

  const MOM<double> Momenta[][legs] = {
    {
      MOM<double>(-0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00,-0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00, 0.5000000000000000E+03),
      MOM<double>( 0.8855133305450298E+02,-0.2210069028768998E+02, 0.4008035319168533E+02,-0.7580543095693663E+02),
      MOM<double>( 0.3283294192270985E+03,-0.1038496118834563E+03,-0.3019337553895401E+03, 0.7649492138716589E+02),
      MOM<double>( 0.1523581094674306E+03,-0.1058809596665922E+03,-0.9770963832697571E+02, 0.4954838522679282E+02),
      MOM<double>( 0.4307611382509676E+03, 0.2318312618377385E+03, 0.3595630405248305E+03,-0.5023787565702211E+02)
    },{
      MOM<double>(-0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00,-0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00, 0.5000000000000000E+03),
      MOM<double>( 0.1177477081371708E+03,-0.6070592184636148E+02, 0.7123104586232657E+02, 0.7145244523246875E+02),
      MOM<double>( 0.3509541730779688E+03,-0.3178816670261076E+02, 0.8393942869317344E+02, 0.3392823549334560E+03),
      MOM<double>( 0.3493322285737900E+03, 0.1840093039957635E+03,-0.5152779793923704E+02,-0.2924354082577186E+03),
      MOM<double>( 0.1819658902110701E+03,-0.9151521544679123E+02,-0.1036426766162630E+03,-0.1182993919082062E+03)
    },{
      MOM<double>(-5.0000000000000000E+01, 0.0000000000000000E+00, 0.0000000000000000E+00,-5.0000000000000000E+01),
      MOM<double>(-5.0000000000000000E+01, 0.0000000000000000E+00, 0.0000000000000000E+00, 5.0000000000000000E+01),
      MOM<double>( 3.0830908416563354E+01,-1.5462861903492566E+01, 2.6617620401269029E+01, 1.7167118913730799E+00),
      MOM<double>( 2.5745672609866769E+01,-1.1924408694529273E+01,-8.6688190119874555E+00,-2.1106864105254559E+01),
      MOM<double>( 4.0645606131607479E+01, 2.8711125857142530E+01,-2.0051754806234289E+01, 2.0631618429288316E+01),
      MOM<double>( 2.7778128419623971E+00,-1.3238552591206938E+00, 2.1029534169527198E+00,-1.2414662154068357E+00)
    },{
      MOM<double>(-5.0000000000000000E+01, 0.0000000000000000E+00, 0.0000000000000000E+00,-5.0000000000000000E+01),
      MOM<double>(-5.0000000000000000E+01, 0.0000000000000000E+00, 0.0000000000000000E+00, 5.0000000000000000E+01),
      MOM<double>( 1.7658726533284433E+01, 1.3403174828466632E+01, 6.6749400858894896E+00, 9.3611271834468557E+00),
      MOM<double>( 3.9791943733796067E+01,-3.0687550194649063E+01,-2.4819226609719482E+01, 5.0674490288386460E+00),
      MOM<double>( 3.4133057638993364E+01, 2.2521276138666977E+01, 1.5476870081140326E+01,-2.0452976247095574E+01),
      MOM<double>( 8.4162720939261479E+00,-5.2369007724845442E+00, 2.6674164426896647E+00, 6.0244000348100739E+00)
    }
  };

  const int namps = 2;
  const char* amp_names[namps] = {
    "Amp0q6g",
    "Amp0q6g-analytic",
  };

  NJetAmp<double>* amps[namps] = {
    new Amp0q6g<double>(1.),
    new Amp0q6g_a<double>(1.),
  };

  double times[namps][npoints];

  int ng[] = {6, 6};
  int nq[] = {0, 0};

  const int hel[] = {-1, -1, -1, +1, +1, +1};

  const double mur = 91.188;
  const double Nc = 3.;
  const double Nf = 5.;
  for (int n=0; n<namps; n++) {
    cout << "----- Amp " << amp_names[n] << " ------"<< endl;
    NJetAmp<double>* amp1 = amps[n];
    amp1->setMuR2(mur*mur);

    for (int i=0; i<npoints; i++) {
      cout << "----- POINT N " << i << " ------"<< endl;
      amp1->setMomenta(Momenta[i]);
      tm.start();
      double sum_tree = amp1->born();
      times[n][i] = static_cast<double>(tm.get());

      cout << "A0    " << sum_tree << endl;
      cout.flush();
    }
    cout << "TIME " << amp_names[n] << " FULL ";
    print_stat(npoints, times[n]);
    cout << endl;
  }
  for (int n=0; n<namps; n++) {
    if (amps[n]) {
      delete amps[n];
      amps[n] = 0;
    }
  }
}
//}}}

void analytic()
{
  analytic4();
//  analytic5();
//  analytic6();
}

int main(int argc, char **argv)
{
  cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
  cout.precision(16);

  bool do_anything = false;

  if (0 != parseOptions(argc, argv)) {
    do_help = true;
  }

  if (do_analytic) {
    analytic();
    do_anything = true;
  }
  if (do_pp2j) {
    pp2j();
    do_anything = true;
  }
  if (do_pp3j) {
    pp3jds();
    pp3j();
    do_anything = true;
  }
  if (do_pp4j) {
    pp4jds();
    pp4j();
    do_anything = true;
  }
  if (do_pp5j) {
    pp5jds();
    pp5j();
    do_anything = true;
  }
  if (do_ppV0j) {
    ppV0j();
    do_anything = true;
  }
  if (do_ppV1j) {
    ppV1j();
    do_anything = true;
  }
  if (do_ppV2j) {
    ppV2j();
    do_anything = true;
  }
  if (do_ppV3j) {
    ppV3j();
    do_anything = true;
  }
  if (do_ppV4j) {
    ppV4j();
    do_anything = true;
  }
  if (do_ppV5j) {
    ppV5j();
    do_anything = true;
  }
  if (do_BH_V3j) {
    BH_V3j<double>();
#ifdef USE_DD
    BH_V3j<dd_real>();
#endif
#ifdef USE_VC
    BH_V3j<Vc::double_v>();
#endif
    do_anything = true;
  }

  if (do_help or not do_anything) {
    help();
  }
}
