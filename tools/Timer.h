/*
* tools/Timer.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef TOOLS_TIMER_H
#define TOOLS_TIMER_H

#include <time.h>
#include <unistd.h>
#include <iostream>

#if defined(_POSIX_TIMERS) && _POSIX_TIMERS > 0

class Time : public timespec
{
#ifdef _POSIX_MONOTONIC_CLOCK
  static const clockid_t CLOCK_NJET = CLOCK_MONOTONIC;
#else
  static const clockid_t CLOCK_NJET = CLOCK_REALTIME;
#endif
  public:
    Time() : timespec() {}

    static Time current() {
      Time t;
      clock_gettime(CLOCK_NJET, &t);
      return t;
    }

    operator double() const {
      return tv_sec + tv_nsec/1.e9;
    }

    void normalize() {
      while ( tv_nsec > 1000000000 || (tv_nsec > 0 && tv_sec < 0) ) {
        tv_sec += 1;
        tv_nsec -= 1000000000;
      }
      while ( tv_nsec < -1000000000 || (tv_nsec < 0 && tv_sec > 0) ) {
        tv_sec -= 1;
        tv_nsec += 1000000000;
      }
    }

    Time& operator-= (const Time& t) {
      tv_sec -= t.tv_sec;
      tv_nsec -= t.tv_nsec;
      normalize();
      return *this;
    }

    Time& operator+= (const Time& t) {
      tv_sec += t.tv_sec;
      tv_nsec += t.tv_nsec;
      normalize();
      return *this;
    }
};

inline
Time operator- (const Time& t1, const Time& t2)
{
  Time t = t1;
  t -= t2;
  return t;
}

inline
Time operator+ (const Time& t1, const Time& t2)
{
  Time t = t1;
  t += t2;
  return t;
}

inline
std::ostream& operator<< (std::ostream& stream, const Time& t)
{
  stream << t.tv_sec << "." << t.tv_nsec;
  return stream;
}

class Timer
{
  public:
    Timer() {
      start();
    }

    void start() {
      begin = Time::current();
    }

    Time get() const {
      return Time::current()-begin;
    }

  protected:
    Time begin;
};

#else // _POSIX_TIMERS

#include <ctime>

class Timer
{
  public:
    Timer() {
      start();
    }
    
    void start() {
      begin = clock();
    }

    double get() const {
      return double(clock() - begin)/CLOCKS_PER_SEC;
    }

  protected:
    clock_t begin;
};

#endif // _POSIX_TIMERS

#endif // TOOLS_TIMER_H
