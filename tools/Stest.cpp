#include <iostream>
#include <getopt.h>
#include <cstdio>

#include "PhaseSpace.h"
#include "../ngluon2/Initialize.h"
#include "../ngluon2/Model.h"
#include "Timer.h"

#include "../chsums/NJetInterface.h"


template <typename T>
void test_2j()
{
  static NJetInterface<T> njet;

  const int legs = 4;
  const int npoints = 2;

  MOM<double> Momenta[npoints][legs] = {
    {
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00, -0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00,  0.5000000000000000E+03),
      MOM<double>( 0.4999999999999998E+03,  0.1109242844438328E+03,  0.4448307894881214E+03, -0.1995529299308788E+03),
      MOM<double>( 0.5000000000000000E+03, -0.1109242844438328E+03, -0.4448307894881214E+03,  0.1995529299308787E+03)
    },{
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00, -0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00,  0.5000000000000000E+03),
      MOM<double>( 0.5000000000000000E+03, -0.2819155058093908E+03, -0.3907909666011574E+03,  0.1334393795218208E+03),
      MOM<double>( 0.5000000000000002E+03,  0.2819155058093908E+03,  0.3907909666011575E+03, -0.1334393795218208E+03)
    }
  };

  NJetAccuracy<T>* amp = njet.getAmp(NJetInterface<T>::Edduu);

  for (int n=0; n<npoints; n++) {
    amp->setMomenta(Momenta[n]);
    std::cout << amp->born() << std::endl;
    std::cout << amp->born_error() << std::endl;
    std::cout << amp->virt() << std::endl;
    std::cout << amp->virt_error() << std::endl;
  }
}

int main()
{
  cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
  cout.precision(16);
  test_2j<double>();
#ifdef USE_VC
  test_2j<Vc::double_v>();
#endif
}
