#include <getopt.h>
#include <cstdio>

#include "../ngluon2/Current.h"
#include "random.h"
#include "PhaseSpace.h"
#include "../ngluon2/Model.h"

int rseed = 1;
int maxpts = 1;
int skip = 0;
int debug = 0;
int dd_prec = 0;
int version = 2;
float sqrtS = 70.;
float pTmin = 0.008;
float etaMax = 2.8;
float deltaR = 0.4;

#include <common.h>
#include <src-ngluon/Current.h>

using namespace std;

int parseOptions(int argc, char **argv)
{
  static struct option long_options[] =
    {
      {"points", required_argument, 0, 'n'},
      {"seed", required_argument, 0, 's'},
      {"skip", required_argument, 0, 'k'},
      {"version", required_argument, 0, 'v'},
      {"energy", required_argument, 0, 'e'},
      {"pT", required_argument, 0, 'p'},
      {"eta", required_argument, 0, 'r'},
      {"dR", required_argument, 0, 'd'},
      {"dd", no_argument, &dd_prec, 1},
      {"debug", no_argument, &debug, 1},
      {0, 0, 0, 0}
    };

  while(1){
    /*
     * getopt_long stores the option index here.
     */
    int option_index = 0;
    int c = getopt_long_only(argc, argv, "n:s:k:v:e:p:r:d:",
                         long_options, &option_index);

    /* Detect the end of the options. */
    if ( c == -1 ){
      if (optind < argc){
        cout << "Unrecognized additonal input in commandline: ";
        cout << argv[optind] << endl;
        cout << "Abort programm" << endl;
        return 1;
      }
      break;
    }
    switch (c){
    case 'n':
      sscanf(optarg, "%d", &maxpts);
      break;
    case 's':
      sscanf(optarg, "%d", &rseed);
      break;
    case 'k':
      sscanf(optarg, "%d", &skip);
      break;
    case 'v':
      sscanf(optarg, "%d", &version);
      break;
    case 'e':
      sscanf(optarg, "%f", &sqrtS);
      break;
    case 'p':
      sscanf(optarg, "%f", &pTmin);
      break;
    case 'r':
      sscanf(optarg, "%f", &etaMax);
      break;
    case 'd':
      sscanf(optarg, "%f", &deltaR);
      break;
    }
  }
  return 0;
}

void TestTree(const int N, int* hh, int* ff, int npts=1, int seed=1)
{
  PhaseSpace<double> ps(StandardModel::NGluon1compat(N, ff), seed, sqrtS, pTmin, etaMax, deltaR);
  PhaseSpace<dd_real> ps_dd(StandardModel::NGluon1compat(N, ff), seed, sqrtS, pTmin, etaMax, deltaR);
  ps.skip(skip);

  Current<double> NG2tree(StandardModel::NGluon1compat(N, ff));
  Current<dd_real> NG2tree_dd(StandardModel::NGluon1compat(N, ff));
  NG2tree.setHelicity(hh);
  NG2tree_dd.setHelicity(hh);

  std::vector<MOM<double> > vmoms(N);
  double (*moms)[][4] = reinterpret_cast<double(*)[][4]>(&vmoms[0]);
  NJet_sd::Current NG1tree(N, *moms, hh, ff);
  NJet_sd::Current::printFlavor(ff, N);

  for (int pt = 0; pt < npts; pt++) {

    NG2tree.setMomenta(ps.getPSpoint());

    if (debug) {
      ps.showPSpoint();
    }

    if (version >= 2)  {
      complex<double> tree2 = NG2tree.eval();
      cout << "# NG2     A0(" << NJet_sd::Current::helicity2string(N, hh) << ") = " << tree2 << endl;
    }

    if (version != 2)  {
      ps.FillMomArray(*moms);
      complex<double> tree1 = NG1tree.Atree();
      cout << "# NG1     A0(" << NJet_sd::Current::helicity2string(N, hh) << ") = " << tree1 << endl;
    }

    if (dd_prec && version >= 2) {
      NG2tree_dd.setMomenta(ps_dd.getPSpoint(ps.current()));
      ps_dd.showPSpoint();
      complex<dd_real> tree3 = NG2tree_dd.eval();
      cout << "# NG2(dd) A0(" << NJet_sd::Current::helicity2string(N, hh) << ") = " << tree3 << endl;
    }

  }

}

void TestVbosons()
{
  cout << "# checing V+qqb+gluons against analytic formula in hep-th/0507161" << endl;
  MOM<double> p1, p2, p3, p4, p5, p6, l, lb, pV;

  Model::RealFlavour ff4[] = {StandardModel::ubar(),
                          StandardModel::V_uubar(),
                          StandardModel::u(),
                          StandardModel::G()};

  PhaseSpace<double> ps(4, ff4, rseed, sqrtS, pTmin, etaMax, deltaR);
  Current<double> tree4(4, ff4);
  tree4.setMomenta(ps.getPSpoint());
  ps.showPSpoint();

  int hh4[][4] = {
    {+1, +1, -1, +1},
    {+1, +1, -1, -1}
  };

  l  = ps[1];
  lb = ps[2];
  pV = l+lb;
  p1 = ps[3];
  p2 = ps[4];
  p3 = ps[0];

  tree4.setHelicity(hh4[0]);
  cout << "            tree = " << tree4.eval() << endl;
  cout << "A(Z,1q-,2+,3qb+) = " << -i_*xspA(p1,l)*xspAB(p1,pV,lb)/xspA(p1,p2)/xspA(p2,p3)/S(pV) << endl;
  tree4.setHelicity(hh4[1]);
  cout << "            tree = " << tree4.eval() << endl;
  cout << "A(Z,1q-,2-,3qb+) = " << -i_*xspB(p3,lb)*xspAB(l,pV,p3)/xspB(p1,p2)/xspB(p2,p3)/S(pV) << endl;

  Model::RealFlavour ff5[] = {StandardModel::ubar(),
                          StandardModel::V_uubar(),
                          StandardModel::u(),
                          StandardModel::G(),
                          StandardModel::G()};
  Current<double> tree5(5, ff5);
  ps.Set(5, ff5);
  tree5.setMomenta(ps.getPSpoint());
  ps.showPSpoint();

  int hh5[][5] = {
    {+1,+1,-1,+1,+1},
    {+1,+1,-1,-1,+1},
    {+1,+1,-1,+1,-1}
  };

  l  = ps[1];
  lb = ps[2];
  pV = l+lb;
  p1 = ps[3];
  p2 = ps[4];
  p3 = ps[5];
  p4 = ps[0];

  tree5.setHelicity(hh5[0]);
  cout << "               tree = " << tree5.eval() << endl;
  cout << "A(Z,1q-,2+,3+,4qb+) = " << -i_*xspA(p1,l)*xspAB(p1,pV,lb)/xspA(p1,p2)/xspA(p2,p3)/xspA(p3,p4)/S(pV) << endl;
  tree5.setHelicity(hh5[1]);
  cout << "               tree = " << tree5.eval() << endl;
  cout << "A(Z,1q-,2-,3+,4qb+) = " <<
    - i_*xspB(p1,p3)*(xspB(p3,p1)*xspAB(p1,pV,lb)+xspB(p3,p2)*xspAB(p2,pV,lb))*xspAB(l,p1+p2,p3)/(S(p1+p2+p3)*xspB(p1,p2)*xspB(p2,p3)*xspAB(p4,p2+p3,p1))/S(pV)
    - i_*xspA(p2,p4)*xspAB(p2,p3+p4,lb)*(xspAB(l,pV,p3)*xspA(p3,p2)+xspAB(l,pV,p4)*xspA(p4,p2))/(S(p2+p3+p4)*xspA(p2,p3)*xspA(p3,p4)*xspAB(p4,p2+p3,p1))/S(pV)
    << endl;
  tree5.setHelicity(hh5[2]);
  cout << "               tree = " << tree5.eval() << endl;
  cout << "A(Z,1q-,2+,3-,4qb+) = " <<
    - i_*pow(xspB(p2,p4),3.)*xspA(p1,l)*xspBA(lb,pV,p1)/(S(p2+p3+p4)*xspB(p2,p3)*xspB(p3,p4)*xspAB(p1,p2+p3,p4))/S(pV)
    - i_*pow(xspA(p1,p3),3.)*xspBA(p4,pV,l)*xspB(lb,p4)/(S(p1+p2+p3)*xspA(p1,p2)*xspA(p2,p3)*xspAB(p1,p2+p3,p4))/S(pV)
    << endl;

}

const MOM<double> MG5moms[][5] = {
  {
    MOM<double>(-0.350000000000000E+04,  0.000000000000000E+00,  0.000000000000000E+00, -0.350000000000000E+04),
    MOM<double>(-0.350000000000000E+04,  0.000000000000000E+00,  0.000000000000000E+00,  0.350000000000000E+04),
    MOM<double>( 0.321005151519808E+04,  0.118617254216776E+04,  0.265757563454739E+04, -0.135451732255177E+04),
    MOM<double>( 0.254846634515772E+04, -0.128309085052343E+03, -0.243393010923557E+04,  0.744447254310957E+03),
    MOM<double>( 0.124148213964419E+04, -0.105786345711542E+04, -0.223645525311821E+03,  0.610070068240811E+03)
  },{
    MOM<double>(-0.350000000000000E+04,  0.000000000000000E+00,  0.000000000000000E+00, -0.350000000000000E+04),
    MOM<double>(-0.350000000000000E+04,  0.000000000000000E+00,  0.000000000000000E+00,  0.350000000000000E+04),
    MOM<double>( 0.346607374454168E+04,  0.130706041038448E+04,  0.223778504659517E+04, -0.230164688243914E+04),
    MOM<double>( 0.718745397125064E+03, -0.494342991154056E+03, -0.304191670698763E+03,  0.423895482946943E+03),
    MOM<double>( 0.281518085833325E+04, -0.812717419230428E+03, -0.193359337589641E+04,  0.187775139949220E+04)
  },{
    MOM<double>(-0.350000000000000E+04,  0.000000000000000E+00,  0.000000000000000E+00, -0.350000000000000E+04),
    MOM<double>(-0.350000000000000E+04,  0.000000000000000E+00,  0.000000000000000E+00,  0.350000000000000E+04),
    MOM<double>( 0.141004315210535E+04,  0.102227487843051E+04, -0.789179522835740E+03, -0.566013643356605E+03),
    MOM<double>( 0.320390758017998E+04, -0.266474032738460E+04, -0.516432101542759E+03,  0.170219877054386E+04),
    MOM<double>( 0.238604926771468E+04,  0.164246544895409E+04,  0.130561162437850E+04, -0.113618512718726E+04)
  },{
    MOM<double>(-0.350000000000000E+04,  0.000000000000000E+00,  0.000000000000000E+00, -0.350000000000000E+04),
    MOM<double>(-0.350000000000000E+04,  0.000000000000000E+00,  0.000000000000000E+00,  0.350000000000000E+04),
    MOM<double>( 0.129543325163167E+04, -0.357101090549467E+03, -0.576447734891688E+03, -0.110378173997382E+04),
    MOM<double>( 0.293466678417949E+04, -0.815561320924215E+03,  0.281897629003660E+04, -0.223951376478569E+02),
    MOM<double>( 0.276989996418883E+04,  0.117266241147368E+04, -0.224252855514491E+04,  0.112617687762168E+04)
  },{
    MOM<double>(-0.350000000000000E+04,  0.000000000000000E+00,  0.000000000000000E+00, -0.350000000000000E+04),
    MOM<double>(-0.350000000000000E+04,  0.000000000000000E+00,  0.000000000000000E+00,  0.350000000000000E+04),
    MOM<double>( 0.314091367223945E+04,  0.142007572402167E+03, -0.313768972413672E+04,  0.870292410508790E+01),
    MOM<double>( 0.157752171925001E+04, -0.108414534970209E+04,  0.108905860592717E+04, -0.356587981122175E+03),
    MOM<double>( 0.228156460851055E+04,  0.942137777299922E+03,  0.204863111820954E+04,  0.347885057017087E+03)
  },{
    MOM<double>(-0.500000000000000E+03,  0.000000000000000E+00,  0.000000000000000E+00, -0.500000000000000E+03),
    MOM<double>(-0.500000000000000E+03,  0.000000000000000E+00,  0.000000000000000E+00,  0.500000000000000E+03),
    MOM<double>( 0.448701953177064E+03,  0.202867960574524E+02, -0.448241389162388E+03,  0.124327487215541E+01),
    MOM<double>( 0.225360245607144E+03, -0.154877907100298E+03,  0.155579800846739E+03, -0.509411401603107E+02),
    MOM<double>( 0.325937801215793E+03,  0.134591111042846E+03,  0.292661588315649E+03,  0.496978652881552E+02)
  }
};

void Test_pp_Wj()
{
  cout << "# some hard coded check for pp > W+j versus MadGraph5" << endl;
  double mW = 80.419002445756163;
  double gW = 2.0476;
  double se2 = 0.22224648578577766; // sin(theta_EW)^2
  StandardModel::setWmass(mW, gW);
  StandardModel::setSinThetaWSq(se2);

  Flavour<double> ff4[] = {StandardModel::dbar(),
                          StandardModel::Wp_udbar(),
                          StandardModel::u(),
                          StandardModel::G()};

  Current<double> tree4(4, ff4);

  int hels[][4] = {
    {+1, +1, -1, +1},
    {+1, +1, -1, -1}
  };

  double as = 0.118;
  double ae = 7.54677111397888348E-003;

  double gs = sqrt(4.*M_PI*as);
  double ee = sqrt(4.*M_PI*ae);
  double se = sqrt(se2);

  cout << "gs = " << gs << endl;
  cout << "ee = " << ee << endl;
  double coupling = pow(ee/se,4.)*pow(gs,2.);

  double Nc = 3.;
  double Vc = Nc*Nc-1.;
  double chaverage;

  cout << "# checking u d~ > e+ ve g with MadGraph5" << endl;
  chaverage = Nc*Nc*4.;
  double MG5res[6] = {
    5.14392294499157428E-008,
    4.92468342583684061E-008,
    2.44681961583209803E-008,
    1.30668327720555130E-008,
    7.82409732490919984E-009,
    3.97730290683046040E-007
  };


  for (int pts = 0; pts < 6; pts++) {
    MOM<double> newmoms[5] = {
      MG5moms[pts][0],
      MG5moms[pts][3],
      MG5moms[pts][2],
      MG5moms[pts][1],
      MG5moms[pts][4]
    };
    tree4.setMomenta(newmoms);

    complex<double> hamp, amp;
    for (int h = 0; h < 2; h++) {
      tree4.setHelicity(hels[h]);
      hamp = tree4.eval();
      amp += hamp*conj(hamp);
    }

    double NJ2res = coupling*real(amp)*Vc/chaverage;

    cout << "2(NJ2-MG5)/(NJ2+MG5) = " << 2.*(NJ2res-MG5res[pts])/(NJ2res+MG5res[pts]) << endl;

  }

  cout << "# checking g u > e+ ve d~ with MadGraph5" << endl;
  chaverage = Nc*Vc*4.;
  double MG5res2[5] = {
    6.08706641869898317E-010,
    4.87219921331325745E-010,
    1.06493676602959006E-009,
    3.73097048671613216E-009,
    1.01771762248006472E-009
  };

  for (int pts = 0; pts < 5; pts++) {
    MOM<double> newmoms[5] = {
      MG5moms[pts][1],
      MG5moms[pts][3],
      MG5moms[pts][2],
      MG5moms[pts][4],
      MG5moms[pts][0]
    };
    tree4.setMomenta(newmoms);

    complex<double> hamp, amp;
    for (int h = 0; h < 2; h++) {
      tree4.setHelicity(hels[h]);
      hamp = tree4.eval();
      amp += hamp*conj(hamp);
    }

    double NJ2res = coupling*real(amp)*Vc/chaverage;

    cout << "2(NJ2-MG5)/(NJ2+MG5) = " << 2.*(NJ2res-MG5res2[pts])/(NJ2res+MG5res2[pts]) << endl;

  }

  cout << "# checking g d~ > e+ ve u with MadGraph5" << endl;
  chaverage = Nc*Vc*4.;
  double MG5res3[5] = {
    2.32977985318237862E-009,
    1.15483411596253621E-008,
    1.43017283180480163E-009,
    1.86953444324906017E-009,
    1.58709666369714273E-009
  };

  for (int pts = 0; pts < 5; pts++) {
    MOM<double> newmoms[5] = {
      MG5moms[pts][4],
      MG5moms[pts][3],
      MG5moms[pts][2],
      MG5moms[pts][1],
      MG5moms[pts][0]
    };
    tree4.setMomenta(newmoms);

    complex<double> hamp, amp;
    for (int h = 0; h < 2; h++) {
      tree4.setHelicity(hels[h]);
      hamp = tree4.eval();
      amp += hamp*conj(hamp);
    }

    double NJ2res = coupling*real(amp)*Vc/chaverage;

    cout << "2(NJ2-MG5)/(NJ2+MG5) = " << 2.*(NJ2res-MG5res3[pts])/(NJ2res+MG5res3[pts]) << endl;

  }

}

void Test_pp_Zj()
{
  cout << "# some hard coded check for pp > Z+j versus MadGraph5" << endl;
  double mZ = 91.188;
  double gZ = 2.441404;
  double se2 = 0.22224648578577766; // sin(theta_EW)^2
  StandardModel::setZmass(mZ, gZ);
  StandardModel::setSinThetaWSq(se2);

  Flavour<double> ffU[] = {StandardModel::ubar(),
                          StandardModel::Z_uubar(),
                          StandardModel::u(),
                          StandardModel::G()};

  Flavour<double> ffD[] = {StandardModel::dbar(),
                          StandardModel::Z_ddbar(),
                          StandardModel::d(),
                          StandardModel::G()};


  Current<double> treeU(4, ffU);
  Current<double> treeD(4, ffD);

  int hels[][4] = {
    {+1, +1, -1, -1},
    {+1, +1, -1, +1},
    {+1, -1, -1, -1},
    {+1, -1, -1, +1},
    {-1, +5, +1, -1},
    {-1, +5, +1, +1},
    {-1, +3, +1, -1},
    {-1, +3, +1, +1}
  };

  double as = 0.118;
  double ae = 7.54677111397888348E-003;

  double gs = sqrt(4.*M_PI*as);
  double ee = sqrt(4.*M_PI*ae);

  cout << "gs = " << gs << endl;
  cout << "ee = " << ee << endl;
  double coupling = pow(ee,4.)*pow(gs,2.);

  double Nc = 3.;
  double Vc = Nc*Nc-1.;
  double chaverage;

  cout << "# checking u u~ > e+ e- g with MadGraph5" << endl;
  chaverage = Nc*Nc*4.;
  double MG5res[5] = {
    2.33816673343700579E-008,
    2.19956842357527602E-008,
    1.10027415520580374E-008,
    6.17637916988714191E-009,
    3.90919124109193586E-009
  };

  for (int pts = 0; pts < 5; pts++) {
    MOM<double> newmoms[5] = {
      MG5moms[pts][0],
      MG5moms[pts][3],
      MG5moms[pts][2],
      MG5moms[pts][1],
      MG5moms[pts][4]
    };
    treeU.setMomenta(newmoms);

    complex<double> hamp, amp;
    for (int h = 0; h < 8; h++) {
      treeU.setHelicity(hels[h]);
      hamp = treeU.eval();

      amp += hamp*conj(hamp);
    }

    double NJ2res = coupling*real(amp)*Vc/chaverage;

    cout << "2(NJ2-MG5)/(NJ2+MG5) = " << 2.*(NJ2res-MG5res[pts])/(NJ2res+MG5res[pts]) << endl;
  }

  cout << "# checking d d~ > e+ e- g with MadGraph5" << endl;
  chaverage = Nc*Nc*4.;
  double MG5res2[6] = {
    1.25896860504396520E-008,
    1.18989778158246044E-008,
    5.94123820316795026E-009,
    3.29364379803934681E-009,
    2.05642313892372361E-009
  };

  for (int pts = 0; pts < 5; pts++) {
    MOM<double> newmoms[5] = {
      MG5moms[pts][0],
      MG5moms[pts][3],
      MG5moms[pts][2],
      MG5moms[pts][1],
      MG5moms[pts][4]
    };
    treeD.setMomenta(newmoms);

    complex<double> hamp, amp;
    for (int h = 0; h < 8; h++) {
      treeD.setHelicity(hels[h]);
      hamp = treeD.eval();

      amp += hamp*conj(hamp);
    }

    double NJ2res = coupling*real(amp)*Vc/chaverage;

    cout << "2(NJ2-MG5)/(NJ2+MG5) = " << 2.*(NJ2res-MG5res2[pts])/(NJ2res+MG5res2[pts]) << endl;
  }

}

int main(int argc, char **argv)
{
  cout.setf(ios_base::scientific, ios_base::floatfield);
//  cout.setf(ios_base::showpos);
  cout.precision(16);
  if (0 != parseOptions(argc, argv)) return 1;
  cout << "### random seed = " << rseed << " ###" << endl;
  if ( version < 1 ) {
    cout << "you should use at least one version of NGluon..." << endl;
    return 1;
  }

  int hh6[][6]={
    {-1,+1,-1,+1,-1,+1},
    {-1,+1,-1,+1,-1,+1},
    {-1,+1,-1,+1,-1,+1}
  };
  int ff6[][6]={
    {0,0,0,0,0,0},
    {-1,0,0,1,0,0},
    {-1,-2,2,1,0,0}
  };

  for (int k=0; k<=2; k++) {
    TestTree(6, hh6[k], ff6[k], maxpts, rseed);
  }

  TestVbosons();
  Test_pp_Wj();
  Test_pp_Zj();

  return 0;
}
