#include <iostream>
#include <getopt.h>
#include <cstdio>

#include "../ngluon2/Model.h"
#include "PhaseSpace.h"

#include "../chsums/0q5g.h"
#include "../analytic/0q5g-analytic.h"

using std::cout;
using std::endl;

int main(int argc, char **argv)
{
  cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
  cout.precision(16);

  const int nn = 5;
  Flavour<double> flavours[] = {
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G()
  };
  PhaseSpace<double> ps(nn, flavours);

  const int namps = 2;
  NJetAmp<double>* amps[namps] = {
    new Amp0q5g<double>(1.),
    new Amp0q5g_a<double>(1.),
  };

  const double MuR = 91.188;
  const double Nc = 3.;
  const double Nf = 0.;

  std::vector<MOM<double> > moms = ps.getPSpoint();

  for (int n=0; n<namps; n++) {
    NJetAmp<double>* amp1 = amps[n];
    amp1->setMuR2(MuR*MuR);
    amp1->setNc(Nc);
    amp1->setNf(Nf);
    amp1->setMomenta(moms);
    cout << amp1->born() << endl;
    cout << amp1->virt() << endl;
  }

}
