/*
* tools/random.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>
#include "random.h"

extern "C" {
#include "ranlxs.h"
}

using std::cout;
using std::endl;
using std::string;

istream& operator>> (istream& stream, iodouble& ioval)
{
  uint64_t ival;
  stream >> ival;
  const iodouble::ID64 idval = {ival};
  ioval.val = idval.d64;
  return stream;
}

ostream& operator<< (ostream& stream, iodouble ioval)
{
  const iodouble::DI64 dival = {ioval.val};
  stream << dival.i64;
  return stream;
}

ostream& operator<<(ostream& stream, RandNums& rn)
{
  int len = rn.len;
  stream << "R(" << len << " ";
  for (int k=0; k<len-1; k++ ) {
    stream << iodouble(rn.dblvec[k]) << " ";
  }
  stream << iodouble(rn.dblvec[len-1]) << ")";
  return stream;
}

istream& operator>>(istream& stream, RandNums& rn)
{
  int len = rn.len;
  char c;
  int lencheck;
  stream >> c >> lencheck;
  iodouble tmp;
  if (c=='(' && lencheck==len) {
    for (int k=0 ; k<len; k++ ) {
      stream >> tmp;
      rn.dblvec[k]=tmp;
    }
    stream >> c;
  } else {
    string s;
    stream >> s;
    cout << "error reading RandNums: |" << s << "|" << lencheck << "=" << len << endl;
    assert(0);
  }
  return stream;
}

float RandNums::floatbuf[RandNums::maxlen];
unsigned long int RandNums::seqn;
int RandNums::seed;

RandNums::RandNums(const int N_)
  : N(N_), len(3*(N-2)-4)
{
  assert(len<=50);
  if (len > 0) {
    dblvec = new double[len];
  }
  else {
    dblvec = new double[1];
  }
}

RandNums::~RandNums()
{
  delete[] dblvec;
}

void RandNums::resize(int N_)
{
  N = N_;
  len = 3*(N-2)-4;
  assert(len<=50);
  delete[] dblvec;
  dblvec = new double[len];
}

void RandNums::init(int seed_)
{
  seed = seed_;
  seqn = 0;
  rlxs_init(0, seed);
}

const double* RandNums::next()
{
  ranlxs(floatbuf, len);
  for (int i=0; i<len; i++) {
    dblvec[i] = floatbuf[i];
  }
  seqn++;
  return dblvec;
}

const double* RandNums::current()
{
  return dblvec;
}
