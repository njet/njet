#!/usr/bin/env python

import pylab as np
import math
import gzip
import os
import sys
import collections
import re
import sqlite3
import pickle
import cStringIO


CACHE = 'cache.db'
ACC_MIN = 1e-16
ACC_MAX = 1e4
#ACC_MIN = 1e-30
#ACC_MAX = 1e-1
PARTIAL = True
RANGE = (math.log10(ACC_MIN), math.log10(ACC_MAX))


def get_file(iname):
    if iname == '-':
        return sys.stdin
    elif iname[-3:] == '.gz':
        return gzip.open(iname, 'rb')
    else:
        return open(iname, 'rb')


def read_file(name):
    f = None
    sio = cStringIO.StringIO()
    try:
        f = get_file(name)
        state = 0
        for line in f:
            if re.match(r"^\s*#", line):
                continue
            elif re.match(r"^\d", line):
                if state == 0:
                    state = 1
                if state == 1:
                    sio.write(line)
            else:
                if state == 1:
                    break
    except IOError, e:
        if (PARTIAL
            and isinstance(e.args[0], str)
            and re.match('CRC check', e.args[0])):
            pass
        else:
            raise
    finally:
        if f:
            f.close()
    sio.seek(0)
    return np.loadtxt(sio, unpack=True)
    #line = sio.readline()
    #sio.seek(0)
    #sample = [x for x in line.split(' ') if x != '' and x != '\n']
    #if sample[0].count('.') == 0 and sample[1].count('.') == 0:
        #dtype = tuple([int, int] + [float]*(len(sample)-2))
        #return np.genfromtxt(sio, unpack=True, dtype=dtype)
    #else:
        #return np.loadtxt(sio, unpack=True)


def load_data(template, n1=1, n2=10**6, rng=RANGE, bins=20, log=True, usecache=True, type=None):
    data = []
    cache = sqlite3.connect(CACHE)
    try:
        cache.execute("SELECT * FROM hist WHERE size=0")
    except sqlite3.OperationalError:
        cache.execute("CREATE TABLE hist (size INTEGER, mtime REAL, fname TEXT, opts TEXT, obj TEXT)")
        cache.execute("CREATE INDEX hist_mtime_idx ON hist (mtime)")
        cache.commit()
    opts = pickle.dumps([rng, bins, log])
    for i in range(n1, n2):
        try:
            fname = template % i
            if not os.path.exists(fname) or os.path.getsize(fname) == 0:
                break
            key = {}
            key['fname'] = fname
            key['size'] = os.path.getsize(fname)
            key['mtime'] = os.path.getmtime(fname)
            key['opts'] = opts

            val = cache.execute("SELECT obj FROM hist WHERE size=:size \
                                                      AND mtime=:mtime \
                                                      AND fname=:fname \
                                                      AND opts=:opts", key).fetchone()
            if usecache and val:
                hist = pickle.loads(val[0])
                #print "Cached: %s" % fname
                sys.stdout.write(' C%d' % i)
                sys.stdout.flush()
            else:
                #print "Reading: %s" % fname
                sys.stdout.write(' R%d' % i)
                sys.stdout.flush()
                arrs = np.clip(read_file(fname), ACC_MIN, ACC_MAX)
                if log:
                    arrs = np.log10(arrs)
                #hist = np.apply_along_axis(lambda a: np.histogram(a, range=rng, bins=bins), 1, arrs)
                hist = np.array([np.histogram(a, range=rng, bins=bins) for a in arrs], dtype=object)
                if usecache:
                    key['obj'] = buffer(pickle.dumps(hist))
                    with cache:
                        cache.execute("DELETE FROM hist WHERE fname=:fname \
                                                          AND opts=:opts", key).rowcount
                        cache.execute("INSERT INTO hist VALUES (:size, :mtime, :fname, :opts, :obj)", key)
            data.append(hist)
        except IOError, e:
            if not hasattr(e, 'errno') or e.errno != 2:
                sys.stdout.write('S')
                sys.stdout.flush()
                continue
            else:
                print sys.exc_info()
                break
    cache.commit()
    cache.close()
    output = []
    ndata = np.array(data)
    bindata = np.sum(ndata[..., 0], axis=0)
    binedges = ndata[0, ..., 1]
    assert np.all(numpy.array_equal(binedges, a) for a in ndata[..., 1])
    print "\nBinned %d x %d points from up to %s" % (sum(bindata[0]), len(bindata), (template % i))
    return np.transpose([bindata, binedges])


def eps2plot_types(eps, type):
    x = (eps[1][:-1] + eps[1][1:])/2
    #print "Sum", sum(eps[0])
    val = 1.*eps[0]   # make it real number
    if type == 0:
        y = np.clip(val/sum(val), 0, 1)
    elif type == 1:
        y = np.clip(np.log10(val), 0, sum(val))
    elif type == 2:
        y = np.clip(np.log10(val)/math.log10(sum(val)), 0, 1)
    elif type == 3:
        y = np.log10(val/sum(val))
    elif type == 4:
        y = np.clip(np.log10(val/sum(val)), math.log10(1/sum(val)), 0)
    else:
        y = val
    return [x, y]


# column names in test files
column_names = { 3 : ['ep2', 'ep1', 'ep0'
                     ],
                 5 : ['seed', 'seqnum',
                       'ep2', 'ep1', 'ep0'
                     ],
                19 : ['ep2', 'ep1', 'ep0',
                      'cc0/cc', 'rr0/rr',
                      'cc0/ep', 'rr0/ep',
                      'cc0box/cc', 'cc0tri/cc', 'cc0bub/cc',
                      'rr0box/rr', 'rr0tri/rr', 'rr0bub/rr',
                      'cc0box/ep', 'cc0tri/ep', 'cc0bub/ep',
                      'rr0box/ep', 'rr0tri/ep', 'rr0bub/ep',
                      ],
                21 : ['seed', 'seqnum',
                      'ep2', 'ep1', 'ep0',
                      'cc0/cc', 'rr0/rr',
                      'cc0/ep', 'rr0/ep',
                      'cc0box/cc', 'cc0tri/cc', 'cc0bub/cc',
                      'rr0box/rr', 'rr0tri/rr', 'rr0bub/rr',
                      'cc0box/ep', 'cc0tri/ep', 'cc0bub/ep',
                      'rr0box/ep', 'rr0tri/ep', 'rr0bub/ep',
                     ],
                }
def label_columns(data):
    return dict(zip(column_names[len(data)], data))


def plot(param):
    type_names = collections.defaultdict(lambda: 'Plain', {0 : 'Plain Normalized',
                                                           1 : 'Log',
                                                           2 : 'Log Normalized',
                                                           3 : 'Normalized Log',
                                                           4 : 'Normalized Log Clipped',
                                                           })
    print "Type %s, Cache %s, Bins %d" % (type_names[param['type']], str( param['usecache']), param['bins'])

    eps2plot = lambda eps: eps2plot_types(eps, param['type'])
    load = lambda path: load_data(path, **param)

    plotopt = {'linewidth' : 2, 'markersize' : 5} # {'drawstyle' : 'steps-mid'}

    np.figure()
    np.grid(True)


    if 0:
        plot_all(load, eps2plot, plotopt, 'NG2-A', 'acc-test1-%d.gz', extended=1)
        plotopt['linestyle'] = 'dotted'
        plot_all(load, eps2plot, plotopt, 'NG2-B', 'acc-test2-%d.gz', extended=1)

    if 1:
        #name = 'extended-new-%d.gz'
        #name = 'extended-new2-%d.gz'
        #name = 'extended-new2e6-%d.gz'
        #name = 'extended-new2e6r6-%d.gz'
        #name = 'extended-new1e6r6-%d.gz'
        #name = 'extended-test-%d.gz'
        #name = 'extended-test5-%d.gz'

        name = 'acc-qloop1-%d.gz'

        #name = 'extended-new-bade4-%d.gz'

        #name = 'extended-new2-bade4-%d.gz'
        #name = 'extended-new2e6-bade4-%d.gz'

        plot_all(load, eps2plot, plotopt, 'NG2', name, extended=0)

        #name = 'ngluon-test5-%d.gz'
        name = 'acc-ng1-qloop1-%d.gz'
        plotopt['linestyle'] = 'dotted'
        plot_all(load, eps2plot, plotopt, 'NG1', name)

    if 0:
        n1eps2, n1eps1, n1eps0 = load('/lscr221/badger/gitrepos/njet-private/src-ngluon2/6g/errors_NG2_PT60_Eta2.8_DR0.4_%d.gz')
        np.plot(*eps2plot(n1eps2), label="NG2 (-2)", **plotopt)
        np.plot(*eps2plot(n1eps1), label="NG2 (-1)", **plotopt)
        np.plot(*eps2plot(n1eps0), label="NG2 ( 0)", **plotopt)

    if 0:
        n1 = label_columns(load('6g/errors_NG1_S7000.0_rot0.0_PT0.003_Eta2.8_DR0.4_%d.gz'))
        np.plot(*eps2plot(n1['ep2']),  label="NG2 (-2)", ls='dotted', **plotopt)
        np.plot(*eps2plot(n1['ep1']),  label="NG2 (-1)", ls='dotted', **plotopt)
        np.plot(*eps2plot(n1['ep0']),  label="NG2 ( 0)", ls='dotted', **plotopt)

    if 0:
        n2 = label_columns(load('6g/errors_NG2_S7000.0_rot0.0_PT0.003_Eta2.8_DR0.4_wsq1_%d.gz'))
        np.plot(*eps2plot(n2['ep2']),  label="NG2 (-2)", **plotopt)
        np.plot(*eps2plot(n2['ep1']),  label="NG2 (-1)", **plotopt)
        np.plot(*eps2plot(n2['ep0']),  label="NG2 ( 0)", **plotopt)

    if 0:
        n3 = label_columns(load('6g/errors_NG2_S7000.0_rot0.0_PT0.003_Eta2.8_DR0.4_130213_rndXX_%d.gz'))
        np.plot(*eps2plot(n3['ep2']),  label="NG2 (-2)", **plotopt)
        np.plot(*eps2plot(n3['ep1']),  label="NG2 (-1)", **plotopt)
        np.plot(*eps2plot(n3['ep0']),  label="NG2 ( 0)", **plotopt)

    np.legend();
    np.xlim(*RANGE)
#    np.ylim(ymin = -5)
    np.show()


def plot_all(load, eps2plot, plotopt, pref, names, extended=2):
    n1 = label_columns(load(names))
    total = sum(n1['ep2'][0])
    th = -5
    try:
        tx = [x for x in enumerate(n1['ep0'][1]) if x[1] > -5][0][0]
        for sub in ['ep0', 'cc0tri/ep', 'cc0bub/ep', 'rr0tri/ep', 'rr0bub/ep']:
            print ("Total", total, "Failed %s at (%d) %d %d" % (sub, th, sum(n1[sub][0][:tx]), sum(n1[sub][0][tx:])),
                    "Fractions", 1.*sum(n1[sub][0][:tx])/sum(n1[sub][0]), 1.*sum(n1[sub][0][tx:])/sum(n1[sub][0]))
    except KeyError:
        pass
    try:
        np.plot(*eps2plot(n1['ep2']), label=pref+"(-2)", color='blue', **plotopt)
        np.plot(*eps2plot(n1['ep1']), label=pref+"(-1)", color='teal', **plotopt)
        np.plot(*eps2plot(n1['ep0']), label=pref+"( 0)", color='brown', **plotopt)
        if extended >= 2:
            np.plot(*eps2plot(n1['cc0/cc']), label=pref+"cc/cc", color='blueviolet', **plotopt)
            np.plot(*eps2plot(n1['cc0box/cc']), label=pref+"ccX/cc", marker='d', markerfacecolor='blueviolet', ls='dotted', color='blueviolet', **plotopt)
            np.plot(*eps2plot(n1['cc0tri/cc']), label=pref+"ccT/cc", marker='^', markerfacecolor='blueviolet', ls='dotted', color='blueviolet', **plotopt)
            np.plot(*eps2plot(n1['cc0bub/cc']), label=pref+"ccB/cc", marker='o', markerfacecolor='blueviolet', ls='dotted', color='blueviolet', **plotopt)
            np.plot(*eps2plot(n1['rr0/rr']), label=pref+"rr/rr", color='gray', **plotopt)
            np.plot(*eps2plot(n1['rr0box/rr']), label=pref+"rrX/rr", marker='d', markerfacecolor='gray', ls='dotted', color='gray', **plotopt)
            np.plot(*eps2plot(n1['rr0tri/rr']), label=pref+"rrT/rr", marker='^', markerfacecolor='gray', ls='dotted', color='gray', **plotopt)
            np.plot(*eps2plot(n1['rr0bub/rr']), label=pref+"rrB/rr", marker='o', markerfacecolor='gray', ls='dotted', color='gray', **plotopt)
        if extended >= 1:
            np.plot(*eps2plot(n1['cc0/ep']), label=pref+"cc/ep", color='red', **plotopt)
            np.plot(*eps2plot(n1['cc0box/ep']), label=pref+"ccX/ep", marker='d', markerfacecolor='red', ls='dashed', color='red', **plotopt)
            np.plot(*eps2plot(n1['cc0tri/ep']), label=pref+"ccT/ep", marker='^', markerfacecolor='red', ls='dashed', color='red', **plotopt)
            np.plot(*eps2plot(n1['cc0bub/ep']), label=pref+"ccB/ep", marker='o', markerfacecolor='red', ls='dashed', color='red', **plotopt)
            np.plot(*eps2plot(n1['rr0/ep']), label=pref+"rr/ep", color='black', **plotopt)
            np.plot(*eps2plot(n1['rr0box/ep']), label=pref+"rrX/ep", marker='d', markerfacecolor='black', ls='dashed', color='black', **plotopt)
            np.plot(*eps2plot(n1['rr0tri/ep']), label=pref+"rrT/ep", marker='^', markerfacecolor='black', ls='dashed', color='black', **plotopt)
            np.plot(*eps2plot(n1['rr0bub/ep']), label=pref+"rrB/ep", marker='o', markerfacecolor='black', ls='dashed', color='black', **plotopt)
    except KeyError:
        pass


def filter_points(template, field, threshold, n1=1, n2=10**6):
    threshold = float(threshold)
    for i in range(n1, n2):
        try:
            fname = template % i
            if not os.path.exists(fname) or os.path.getsize(fname) == 0:
                break
            arrs = read_file(fname)
            np.clip(arrs[2:], ACC_MIN, ACC_MAX, out=arrs[2:])
            arrs = np.transpose(arrs)
            kind = len(arrs[0])
            idx = column_names[kind].index(field)
            assert column_names[kind].index('seed') == 0
            assert column_names[kind].index('seqnum') == 1
            data = np.array([(int(a[0]),int(a[1])) for a in arrs if a[idx] > threshold])
            for d in data:
                print d[0], d[1]
        except IOError, e:
            if not hasattr(e, 'errno') or e.errno != 2:
                continue
            else:
                print sys.exc_info()
                break


def dump():
    cache = sqlite3.connect(CACHE)
    cache.row_factory = sqlite3.Row
    try:
        rows = cache.execute("SELECT DISTINCT fname, opts FROM hist").fetchall()
        seen = {}
        for val in rows:
            d = dict(zip(val.keys(), val))
            opts = 'Range %s, Bins %s, Log %s' % tuple(map(str, pickle.loads(str(d['opts']))))
            name = re.sub(r'(.*?)(\d+)(.gz)', r'\1%d\3', d['fname'])
            if (name+opts) not in seen:
                print 'File %s %s' % (name, opts)
                seen[(name+opts)] = True
    except sqlite3.OperationalError:
        print "Cache is empty"


def usage():
    print """\
Usage: analyze.py [OPTION...]
Process events
  -t, --type=3              Type
  -c, --cache=yes           Enable or disable
  -b, --bins=40             Bin number
  -d, --dump                Show cache contents
  -f, --filter              Filter [name] [field] [threshold]

Other options:
  -h, --help                show this help message
"""


def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "t:c:b:hdf",
                                  ["type", "cache", "bins", "help", "dump", "filter"])
    except getopt.GetoptError, err:
        print str(err)
        usage()
        sys.exit(2)

    param = {'type' : 3,
             'usecache' : True,
             'bins' : 40,
            }

    action = ['plot']
    for op, oparg in opts:
        if op in ("-h", "--help"):
            usage()
            sys.exit()
        elif op in ("-t", "--type"):
            param['type'] = int(oparg)
        elif op in ("-b", "--bins"):
            param['bins'] = int(oparg)
        elif op in ("-c", "--cache"):
            param['usecache'] = oparg == 'yes' or oparg == 'enable'
        elif op in ("-d", "--dump"):
            if action[0] == 'plot':
                action = []
            action.append('dump')
        elif op in ("-f", "--filter"):
            if action[0] == 'plot':
                action = []
            action.append('filter')
        else:
            assert False, "unhandled option"

    if len(action) > 1:
        print "Select one of", action
    elif action[0] == 'plot':
        plot(param)
    elif action[0] == 'dump':
        dump()
    elif action[0] == 'filter':
        filter_points(*args)


if __name__ == '__main__':
    import getopt
    main()
