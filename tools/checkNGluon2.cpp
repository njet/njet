//#include <cstdlib>
//#include <fstream>
//#include <sstream>
//#include <time.h>
#include <getopt.h>
#include <cstdio>

#include "../src-nparton/NParton-multiprec.h"
#ifdef USE_DD
  static NJet_dd::Initialize global_init_dd;
#else
  static NJet_sd::Initialize global_init_sd;
#endif

#include "../ngluon2/NParton2.h"
#include "../ngluon2/Current.h"
#include "PhaseSpace.h"
#include "random.h"
#include "../ngluon2/Initialize.h"
#include "../ngluon2/LoopIntegrals.h"

Initialize global_init;

using namespace std;

int debug = 0;
int verifyNG2 = 0;

int parseOptions(int argc, char **argv)
{
  static struct option long_options[] =
    {
      {"debug", no_argument, &debug, 1},
      {"verify", no_argument, &verifyNG2, 1},
      {0, 0, 0, 0}
    };

  while(1){
    // getopt_long stores the option index here.
    int option_index = 0;
    int c = getopt_long_only(argc, argv, "n:", long_options, &option_index);

    // Detect the end of the options.
    if ( c == -1 ){
      if (optind < argc){
        cout << "Unrecognized additonal input in commandline: ";
        cout << argv[optind] << endl;
        cout << "Abort programm" << endl;
        return 1;
      }
      break;
    }
  }
  return 0;
}

complex<double> Llog(double x) {
  complex<double> ans = log(abs(x));
  if (x<0) {
    ans += complex<double>(0.,1.)*M_PI;
  }
  return ans;
}

void test_ppW()
{
  cout << "# check against hard coded analytic formulae" << endl;
  double sqrtS = 7e3;
  double mW = 80.419002445756163;
  double gW = 2.0476;
  double mZ = 91.188;
  double gZ = 2.441404;
  StandardModel::setWmass(mW, gW);
  StandardModel::setZmass(mZ, gZ);

  Flavour<double> ff3[] = {
                          StandardModel::dbar(),
                          StandardModel::Wp_udbar(),
                          StandardModel::u()
  };

  NParton2<double> loop;
  loop.setProcess(3,ff3);
  loop.setMuR2(mZ*mZ);

  int hels[][3] = {
    {+1, +1, -1}
  };

  NParton2<double> NG2loop;
  NG2loop.setMuR2(mZ*mZ);

  PhaseSpace<double> ps(3, ff3, 1, sqrtS);
  NG2loop.setProcess(3, ff3);
  NG2loop.setMomenta(ps.getPSpoint());
  NG2loop.setHelicity(hels[0]);

  NG2loop.eval(0);
  complex<double> tree = NG2loop.gettree();
  EpsTriplet<double> res = NG2loop.getres();

  cout << "          A1 = " << res/tree << endl;

  double s = S(ps[0]+ps[3]);
  double Mu2 = mZ*mZ;
  complex<double> Ls = Llog(-Mu2/s);
  EpsTriplet<double> I3s(0.5*Ls*Ls/s, Ls/s, 1./s);
  EpsTriplet<double> I2s(Ls+2., 1., 0.);
  EpsTriplet<double> check = -s*I3s - 3./2.*I2s + (-0.5);

  cout << "A1(analytic) = " << check << endl;
}

const MOM<double> MG5moms[][5] = {
  {
    MOM<double>( -0.500000000000000E+03,  0.000000000000000E+00,   0.000000000000000E+00, -0.500000000000000E+03),
    MOM<double>( -0.500000000000000E+03,  0.000000000000000E+00,   0.000000000000000E+00,  0.500000000000000E+03),
    MOM<double>(  0.419238112025642E+03, -0.116508760132031E+03,   0.402710898576657E+03, -0.319930537826527E+01),
    MOM<double>(  0.185061893090239E+03, -0.510144415070667E+02,  -0.823496764130983E+02, -0.157683105710546E+03),
    MOM<double>(  0.395699994884119E+03,  0.167523201639097E+03,  -0.320361222163558E+03,  0.160882411088811E+03)
  }
};

void test_ppWj()
{
  cout << "# check against numerical result from MadLoop" << endl;

  double mW = 80.419002445756163;
  double gW = 2.0476;
  double mZ = 91.188;
  double gZ = 2.441404;
  double se2 = 0.22224648578577766; // sin(theta_EW)^2
  StandardModel::setWmass(mW, gW);
  StandardModel::setZmass(mZ, gZ);
  StandardModel::setSinThetaWSq(se2);

  Flavour<double> ff4[] = {
                          StandardModel::dbar(),
                          StandardModel::u(),
                          StandardModel::Wp_udbar(),
                          StandardModel::G()};

  NParton2<double> loop;
  loop.setProcess(4, ff4);
  double sqrtS = 1e3;
  loop.setMuR2(sqrtS*sqrtS);

  int hels[][4] = {
    {+1, -1, +1, +1},
    {+1, -1, +1, -1}
  };

  double as = 0.118;
  double ae = 7.54677111397888348E-003;

  double gs = sqrt(4.*M_PI*as);
  double ee = sqrt(4.*M_PI*ae);
  double se = sqrt(se2);

  double coupling = pow(ee/se,4.)*pow(gs,2.);

  double Nc = 3.;
  double Vc = Nc*Nc-1.;
  double chaverage = Nc*Nc*4.;

  cout << "# checking u d~ > e+ ve g with MadLoop" << endl;

  for (int pts = 0; pts < 1; pts++) {
    loop.setMomenta(MG5moms[pts]);

    complex<double> Atree, amp0;
    EpsTriplet<double> AloopLC, AloopSLC, amp1;
    int porder[3][4] = {
      {0,2,1,3},
      {0,2,3,1},
      {0,3,2,1}
    };

    for (int h = 0; h < 2; h++) {
      loop.setHelicity(hels[h]);
      loop.eval(0, porder[0]);
      Atree = loop.gettree();
      AloopLC = loop.getres();
      loop.eval(0, porder[1]);
      AloopSLC = loop.getres();
      loop.eval(0, porder[2]);
      AloopSLC += loop.getres();

      amp0 += Atree*conj(Atree);
      amp1 += (Nc*AloopLC + AloopSLC/Nc)*conj(Atree);
    }

    complex<double> NJ2LO = coupling*amp0*Vc/chaverage;
    EpsTriplet<double> NJ2NLO = coupling*amp1*Vc/chaverage;

    double Nlf = 4.;
    double beta0 = (11.*Nc-2.*Nlf)/3.;
    EpsTriplet<double> renorm(
        0.5*Nc/3. - Nc/6. - 2./(4.*Nc)*Vc,
        -0.5*beta0,
        0.);
    renorm *= NJ2LO;
    NJ2NLO += renorm;

    cout << "NJ2 0     = " << NJ2LO << endl;
    cout << "NJ2 1     = " << NJ2NLO << endl;
    cout << "NJ2 1/0   = " << NJ2NLO/NJ2LO << endl;

    double checkMG5 = -2.9618560789176676;
    cout << "# check finite with MG5: rel. diff = " << (real(NJ2NLO.get0())/NJ2LO-checkMG5)/checkMG5 << endl;
  }
}

EpsTriplet<double> ppZj_qloops(Flavour<double>* ff0) {
//  // fermion loops with Z-bosons are a bit tricky...
  double mZ = 91.188;
  double gZ = 2.441404;
  double se2 = 0.22224648578577766; // sin(theta_EW)^2
  StandardModel::setZmass(mZ, gZ);
  StandardModel::setSinThetaWSq(se2);

  Current<double> tree;
  tree.setProcess(4, ff0);
  tree.setMomenta(MG5moms[0]);

  Flavour<double> ff[][4] = {
    {
      StandardModel::ubar(),
      StandardModel::u(),
      StandardModel::Z_LoopU(),
      StandardModel::G()
    },{
      StandardModel::ubar(),
      StandardModel::u(),
      StandardModel::Z_LoopD(),
      StandardModel::G()
    }
  };

  NParton2<double> loop;
  double sqrtS = 1e3;
  loop.setMuR2(sqrtS*sqrtS);

  int hels[][4] = {
    {+1, -1, -1, +1},
    {+1, -1, -1, -1},
    {+1, -1, +1, +1},
    {+1, -1, +1, -1},
    {-1, +1, +3, +1},
    {-1, +1, +3, -1},
    {-1, +1, +5, +1},
    {-1, +1, +5, -1}
  };

  int porder0[4] = {0, 2, 1, 3};

  int porder[][4] = {
    {0, 1, 2, 3},
    {0, 1, 3, 2}
  };

  complex<double> ans0;
  EpsTriplet<double> ans, ansL, ansR;

  loop.setProcess(4, ff[0]);
  loop.setMomenta(MG5moms[0]);

  for (int h = 0; h < 4; h++) {
    loop.setHelicity(hels[h]);
    tree.setHelicity(hels[h]);
    tree.setOrder(porder0);
    ans0 = tree.eval();
    ansL = loop.eval(3, porder[0]);
    ansL += loop.eval(3, porder[1]);
    cout << ansL/ans0 << endl;
    ans += ansL*conj(ans0);
//    ans += ans0*conj(ans0);
  }

  loop.setProcess(4, ff[1]);
  loop.setMomenta(MG5moms[0]);

  for (int h = 4; h < 8; h++) {
    loop.setHelicity(hels[h]);
    tree.setHelicity(hels[h]);
    tree.setOrder(porder0);
    ans0 = tree.eval();
    ansR = loop.eval(4, porder[0]);
    ansR += loop.eval(4, porder[1]);
    cout << ansR/ans0 << endl;
    ans += ansR*conj(ans0);
//    ans += ans0*conj(ans0);
  }

  return ans;
}

void test_ppZj()
{
  cout << "# check against numerical result from MadLoop" << endl;

  double mW = 80.419002445756163;
  double gW = 2.0476;
  double mZ = 91.188;
  double gZ = 2.441404;
  double se2 = 0.22224648578577766; // sin(theta_EW)^2
  StandardModel::setWmass(mW, gW);
  StandardModel::setZmass(mZ, gZ);
  StandardModel::setSinThetaWSq(se2);

  Flavour<double> ff[][4] = {
    {
      StandardModel::ubar(),
      StandardModel::u(),
      StandardModel::Z_uubar(),
      StandardModel::G()
    },{
      StandardModel::dbar(),
      StandardModel::d(),
      StandardModel::Z_ddbar(),
      StandardModel::G()
    }
  };

  NParton2<double> loop;

  double sqrtS = 1e3;
  loop.setMuR2(sqrtS*sqrtS);

  int hels[][4] = {
    {+1, -1, -1, +1},
    {+1, -1, -1, -1},
    {+1, -1, +1, +1},
    {+1, -1, +1, -1},
    {-1, +1, +3, +1},
    {-1, +1, +3, -1},
    {-1, +1, +5, +1},
    {-1, +1, +5, -1}
  };

  double as = 0.118;
  double ae = 7.54677111397888348E-003;

  double gs = sqrt(4.*M_PI*as);
  double ee = sqrt(4.*M_PI*ae);

  double coupling = pow(ee,4.)*pow(gs,2.);

  double Nc = 3.;
  double INc = 1./Nc;
  double Vc = Nc*Nc-1.;
  double chaverage = Nc*Nc*4.;

  string channel[2] = {
    "# checking u u~ > e+ e- g with MadLoop",
    "# checking d d~ > e+ e- g with MadLoop"
  };
  double checkMG5[2] = {-2.4678632460419778, -2.6604707119844582};

  complex<double> Atree;
  EpsTriplet<double> AloopLC, AloopSLC;
  int porder[3][4] = {
    {0, 2, 1, 3},
    {0, 2, 3, 1},
    {0, 3, 2, 1}
  };
  double Nf = 4.;
  // NO Nh term in beta0!
  double beta0 = (11.*Nc-2.*Nf)/3.;
  double nq = 2.;
  double ng = 1.;
  double n = ng + nq;
  EpsTriplet<double> renorm(
      (nq-2.)*Nc/6. - nq*Vc*INc*0.25,
      -(n-2.)*0.5*beta0,
      0.);

  for (int f = 0; f < 2; f++) {
    cout << channel[f] << endl;
    loop.setProcess(4, ff[f]);
    complex<double> amp0;
    EpsTriplet<double> amp1;
    for (int pts = 0; pts < 1; pts++) {
      loop.setMomenta(MG5moms[pts]);

      for (int h = 0; h < 8; h++) {
        loop.setHelicity(hels[h]);
        loop.eval(0, porder[0]);
        Atree = loop.gettree();
        AloopLC = loop.getres();
        loop.eval(0, porder[1]);
        AloopSLC = loop.getres();
        loop.eval(0, porder[2]);
        AloopSLC += loop.getres();

        amp0 += Atree*conj(Atree);
        amp1 += (Nc*AloopLC + AloopSLC/Nc)*conj(Atree);
      }

      complex<double> NJ2LO = coupling*amp0*Vc/chaverage;
      EpsTriplet<double> NJ2NLO = coupling*amp1*Vc/chaverage;

      NJ2NLO += renorm*NJ2LO;

      cout << "NJ2 0     = " << NJ2LO << endl;
      cout << "NJ2 1     = " << NJ2NLO << endl;
      cout << "NJ2 1/0   = " << NJ2NLO/NJ2LO << endl;

      cout << "# check finite with MG5: rel. diff = " << (real(NJ2NLO.get0())/NJ2LO-checkMG5[f])/checkMG5[f] << endl;
    }
  }
}

void test_ppWjjj()
{
  cout << "# check against primitive amplitudes from arXiv:0810.2762 [hep-ph]" << endl;

  EpsTriplet<double> EGKMZres[] = {

    // ud3gV [1]
    EpsTriplet<double>(5.993700 - i_*19.646278, -10.439578 - i_*9.424778, -4.00000),
    EpsTriplet<double>(-14.377555 - i_*37.219716, -10.439578 - i_*9.424778, -4.00000),
    EpsTriplet<double>(-1.039489 - i_*30.210418, -10.439578 - i_*9.424778, -4.00000),
    EpsTriplet<double>(-1.444709 - i_*26.101951, -10.439578 - i_*9.424778, -4.00000),

    EpsTriplet<double>(-1.423339 - i_*14.443863, -8.676830 - i_*6.283185, -3.00000),
    EpsTriplet<double>(-11.406265 - i_*16.485295, -8.676830 - i_*6.283185, -3.00000),
    EpsTriplet<double>(-5.430180 - i_*21.180247, -8.676830 - i_*6.283185, -3.00000),
    EpsTriplet<double>(-4.868668 - i_*21.036597, -8.676830 - i_*6.283185, -3.00000),

    EpsTriplet<double>(13.662096 - i_*25.637707, -7.835662 - i_*3.141593, -2.00000),
    EpsTriplet<double>(-9.177581 - i_*16.265480, -7.835662 - i_*3.141593, -2.00000),
    EpsTriplet<double>(-12.140461 - i_*15.924761, -7.835662 - i_*3.141593, -2.00000),
    EpsTriplet<double>(-13.465828 - i_*13.730719, -7.835662 - i_*3.141593, -2.00000),

    EpsTriplet<double>(11.973924 - i_*8.033958, -3.334232 - i_*0.000000, -1.00000),
    EpsTriplet<double>(-1.783190 + i_*1.552944, -3.334232 + i_*0.000000, -1.00000),
    EpsTriplet<double>(-5.654597 + i_*0.276608, -3.334232 - i_*0.000000, -1.00000),
    EpsTriplet<double>(-6.461431 + i_*1.451815, -3.334232 + i_*0.000000, -1.00000),

    // ud3gV [1/2]
    EpsTriplet<double>(-6.001512 - i_*26.601839, 0.000000 - i_*0.000000, 0.00000),
    EpsTriplet<double>(7.227836 - i_*4.090839, 0.000000 - i_*0.000000, 0.00000),
    EpsTriplet<double>(0.096288 - i_*0.114398, 0.000000 + i_*0.000000, 0.00000),
    EpsTriplet<double>(0.164410 - i_*0.134601, 0.000000 + i_*0.000000, 0.00000),

    EpsTriplet<double>(-0.127241 - i_*1.316987, 0.000000 - i_*0.000000, 0.00000),
    EpsTriplet<double>(0.000000 + i_*0.000000, 0.000000 + i_*0.000000, 0.00000),
    EpsTriplet<double>(-0.020783 - i_*0.001593, 0.000000 + i_*0.000000, 0.00000),
    EpsTriplet<double>(0.000000 - i_*0.000000, 0.000000 - i_*0.000000, 0.00000),

    EpsTriplet<double>(0.000000 - i_*0.000000, 0.000000 - i_*0.000000, 0.00000),
    EpsTriplet<double>(0.000000 - i_*0.000000, 0.000000 - i_*0.000000, 0.00000),
    EpsTriplet<double>(0.000000 - i_*0.000000, 0.000000 - i_*0.000000, 0.00000),
    EpsTriplet<double>(0.000000 - i_*0.000000, 0.000000 - i_*0.000000, 0.00000),
    EpsTriplet<double>(0.000000 - i_*0.000000, 0.000000 - i_*0.000000, 0.00000),
    EpsTriplet<double>(0.000000 - i_*0.000000, 0.000000 - i_*0.000000, 0.00000),
    EpsTriplet<double>(0.000000 - i_*0.000000, 0.000000 - i_*0.000000, 0.00000),
    EpsTriplet<double>(0.000000 - i_*0.000000, 0.000000 - i_*0.000000, 0.00000),

    // udQQgV [1]
    EpsTriplet<double>(12.513239 - i_*16.727811, -1.906388 - i_*6.283185, -2.00000),
    EpsTriplet<double>(12.452841 - i_*14.482266, -1.906388 - i_*6.283185, -2.00000),
    EpsTriplet<double>(9.466663 - i_*4.461718, -6.083408 - i_*3.141593, -3.00000),
    EpsTriplet<double>(7.337316 - i_*3.094966, -6.083408 - i_*3.141593, -3.00000),
    EpsTriplet<double>(14.470591 - i_*16.520704, -1.906388 - i_*6.283185, -2.00000),
    EpsTriplet<double>(10.478262 - i_*16.315474, -1.906388 - i_*6.283185, -2.00000),
    EpsTriplet<double>(10.012255 - i_*29.539947, -5.946351 - i_*9.424778, -3.00000),
    EpsTriplet<double>(9.462997 - i_*25.465941, -5.946351 - i_*9.424778, -3.00000),

    EpsTriplet<double>(7.650907 - i_*1.630983, -3.109446 + i_*0.000000, -2.00000),
    EpsTriplet<double>(7.388565 + i_*2.000057, -3.109446 + i_*0.000000, -2.00000),
    EpsTriplet<double>(8.835307 - i_*10.218599, -6.730261 - i_*3.141593, -3.00000),
    EpsTriplet<double>(2.417473 - i_*12.686072, -6.730261 - i_*3.141593, -3.00000),
    EpsTriplet<double>(11.636607 + i_*2.670357, -3.109446 + i_*0.000000, -2.00000),
    EpsTriplet<double>(3.098383 - i_*0.972210, -3.109446 + i_*0.000000, -2.00000),
    EpsTriplet<double>(9.563123 - i_*12.742650, -6.502556 - i_*3.141593, -3.00000),
    EpsTriplet<double>(7.832425 - i_*8.905177, -6.502556 - i_*3.141593, -3.00000),

    EpsTriplet<double>(-8.027321 - i_*0.968722, -3.334232 + i_*0.000000, -1.00000),
    EpsTriplet<double>(-4.397007 - i_*3.109084, -3.334232 + i_*0.000000, -1.00000),
    EpsTriplet<double>(-5.997531 - i_*0.856042, -3.334232 + i_*0.000000, -1.00000),
    EpsTriplet<double>(-6.462353 - i_*0.674696, -3.334232 + i_*0.000000, -1.00000),
    EpsTriplet<double>(-6.039221 - i_*0.200173, -3.334232 - i_*0.000000, -1.00000),
    EpsTriplet<double>(-6.632788 - i_*0.178166, -3.334232 + i_*0.000000, -1.00000),
    EpsTriplet<double>(-12.743438 - i_*16.092839, -7.835662 - i_*3.141593, -2.00000),
    EpsTriplet<double>(-14.463179 - i_*14.071065, -7.835662 - i_*3.141593, -2.00000),

    EpsTriplet<double>(-10.068901 - i_*0.284089, -3.826559 - i_*0.000000, -1.00000),
    EpsTriplet<double>(-9.118256 - i_*0.316181, -3.826559 - i_*0.000000, -1.00000),
    EpsTriplet<double>(-6.343258 - i_*9.293058, -5.954375 - i_*3.141593, -2.00000),
    EpsTriplet<double>(-8.567533 - i_*11.440611, -5.954375 - i_*3.141593, -2.00000),
    EpsTriplet<double>(-9.495931 + i_*0.100657, -3.826559 + i_*0.000000, -1.00000),
    EpsTriplet<double>(-9.538700 - i_*0.299195, -3.826559 - i_*0.000000, -1.00000),
    EpsTriplet<double>(-9.696279 + i_*0.000000, -3.826559 + i_*0.000000, -1.00000),
    EpsTriplet<double>(-9.696279 + i_*0.000000, -3.826559 + i_*0.000000, -1.00000),

    EpsTriplet<double>(-2.527058 - i_*0.104842, -0.666667 - i_*0.000000, 0.00000),
    EpsTriplet<double>(-2.366982 + i_*0.089047, -0.666667 + i_*0.000000, 0.00000),
    EpsTriplet<double>(-2.656572 + i_*0.000000, -0.66666 + i_*0.000000, 0.00000),
    EpsTriplet<double>(-2.656572 + i_*0.000000, -0.666667 + i_*0.000000, 0.00000),
    EpsTriplet<double>(-2.887087 - i_*0.164880, -0.666667 + i_*0.000000, 0.00000),
    EpsTriplet<double>(-2.749917 + i_*0.137009, -0.666667 + i_*0.000000, 0.00000),
    EpsTriplet<double>(-2.662151 + i_*0.000000, -0.666667 + i_*0.000000, 0.00000),
    EpsTriplet<double>(-2.662151 + i_*0.000000, -0.666667 + i_*0.000000, 0.00000),

    EpsTriplet<double>(0.000000 - i_*0.000000, 0.000000 - i_*0.000000, 0.00000),
    EpsTriplet<double>(0.000000 - i_*0.000000, 0.000000 - i_*0.000000, 0.00000),
    EpsTriplet<double>(0.000000 - i_*0.000000, 0.000000 - i_*0.000000, 0.00000),
    EpsTriplet<double>(0.000000 - i_*0.000000, 0.000000 - i_*0.000000, 0.00000),
    EpsTriplet<double>(0.000000 - i_*0.000000, 0.000000 - i_*0.000000, 0.00000),
    EpsTriplet<double>(0.000000 - i_*0.000000, 0.000000 - i_*0.000000, 0.00000),
    EpsTriplet<double>(0.000000 - i_*0.000000, 0.000000 - i_*0.000000, 0.00000),
    EpsTriplet<double>(0.000000 - i_*0.000000, 0.000000 - i_*0.000000, 0.00000),
    EpsTriplet<double>(0.000000 - i_*0.000000, 0.000000 - i_*0.000000, 0.00000),
    EpsTriplet<double>(0.000000 - i_*0.000000, 0.000000 - i_*0.000000, 0.00000),
    EpsTriplet<double>(0.000000 - i_*0.000000, 0.000000 - i_*0.000000, 0.00000),
    EpsTriplet<double>(0.000000 - i_*0.000000, 0.000000 - i_*0.000000, 0.00000),
    EpsTriplet<double>(0.000000 - i_*0.000000, 0.000000 - i_*0.000000, 0.00000),
    EpsTriplet<double>(0.000000 - i_*0.000000, 0.000000 - i_*0.000000, 0.00000),
    EpsTriplet<double>(0.000000 - i_*0.000000, 0.000000 - i_*0.000000, 0.00000),
    EpsTriplet<double>(0.000000 - i_*0.000000, 0.000000 - i_*0.000000, 0.00000),
    EpsTriplet<double>(0.000000 - i_*0.000000, 0.000000 - i_*0.000000, 0.00000),
    EpsTriplet<double>(0.000000 - i_*0.000000, 0.000000 - i_*0.000000, 0.00000),
    EpsTriplet<double>(0.000000 - i_*0.000000, 0.000000 - i_*0.000000, 0.00000),
    EpsTriplet<double>(0.000000 - i_*0.000000, 0.000000 - i_*0.000000, 0.00000),
    EpsTriplet<double>(0.000000 - i_*0.000000, 0.000000 - i_*0.000000, 0.00000),
    EpsTriplet<double>(0.000000 - i_*0.000000, 0.000000 - i_*0.000000, 0.00000),
    EpsTriplet<double>(0.000000 - i_*0.000000, 0.000000 - i_*0.000000, 0.00000),
    EpsTriplet<double>(0.000000 - i_*0.000000, 0.000000 - i_*0.000000, 0.00000)
  };

  double mW = 80.419002445756163;
  double gW = 2.0476;
  double mZ = 91.188;
  double gZ = 2.441404;
  double se2 = 0.22224648578577766; // sin(theta_EW)^2
  StandardModel::setWmass(mW, gW);
  StandardModel::setZmass(mZ, gZ);
  StandardModel::setSinThetaWSq(se2);

  NParton2<double> loop;

  Flavour<double> ud3gW[6] = {
      StandardModel::ubar(),
      StandardModel::u(),
      StandardModel::G(),
      StandardModel::G(),
      StandardModel::G(),
      StandardModel::V_uubar()
  };

  Flavour<double> udqqW[6] = {
      StandardModel::ubar(),
      StandardModel::u(),
      StandardModel::dbar(),
      StandardModel::d(),
      StandardModel::G(),
      StandardModel::V_uubar()
  };
  loop.setProcess(6, ud3gW);

  double mu = 7.;
  loop.setMuR2(mu*mu);
  double alpha = M_PI/3.;
  double gamma = 2.*M_PI/3.;
  double theta = M_PI/4.;
  double phi = M_PI/6.;

  double sa = sin(alpha);
  double ca = cos(alpha);
  double sg = sin(gamma);
  double cg = cos(gamma);
  double st = sin(theta);
  double ct = cos(theta);
  double sp = sin(phi);
  double cp = cos(phi);
  double cb = -37./128.;
  double sb = sin(acos(cb));

  MOM<double> p1 = 0.1*mu*MOM<double>(1, ca*cb, ca*sb, sa);
  MOM<double> p2 =  0.5*mu*MOM<double>(-1, st, ct*sp, ct*cp);
  MOM<double> p3 =  0.5*mu*MOM<double>(-1, -st, -ct*sp, -ct*cp);
  MOM<double> p4 =  mu/3.*MOM<double>(1., 1., 0., 0.);
  MOM<double> p5 =  mu/8.*MOM<double>(1., cb, sb, 0.);
  MOM<double> p6 =  mu/12.*MOM<double>(1., cg*cb, cg*sb, sg);
  MOM<double> p7 = -(p1+p2+p3+p4+p5+p6);

  MOM<double> CheckMoms[] = {p1, p2, p3, p4, p5, p7, p6};
  loop.setMomenta(CheckMoms);

  int hels[][6] = {
    {+1, -1, +1, +1, +1, +1},
    {+1, -1, +1, +1, -1, +1},
    {+1, -1, -1, +1, +1, +1},
    {+1, -1, -1, +1, -1, +1}
  };

  int order[][6] = {
    {0, 5, 1, 2, 3, 4},
    {0, 5, 2, 1, 3, 4},
    {0, 2, 5, 1, 3, 4},
    {0, 5, 2, 3, 1, 4},
    {0, 2, 5, 3, 1, 4},
    {0, 2, 3, 5, 1, 4},
    {0, 5, 2, 3, 4, 1},
    {0, 2, 3, 5, 4, 1},
    {0, 2, 3, 4, 5, 1},
    {0, 2, 5, 3, 4, 1}
  };

  int pos[] = {0, 1, 3, 6};
  int nps[] = {1, 2, 3, 4};

  int EGKMZ = 0;

  // NB overall minus sign on fermion loop primitives
  double sign[] = {1., -1.};
  for (int type=0; type<=1; type++) {
    for (int prim=0; prim<4; prim++) {
      for (int h=0; h<4; h++) {
        loop.setHelicity(hels[h]);
        EpsTriplet<double> ans;
        complex<double> tree;
        for (int p=pos[prim]; p<pos[prim]+nps[prim]; p++) {
          ans += loop.eval(type, order[p]);
          tree += loop.gettree();
        }
        cout << type << "," << prim << "," << h << " = " << ans/tree << endl;
        cout << "## diff. " << EGKMZres[EGKMZ++] - sign[type]*ans/tree << endl;
      }
    }
  }

  loop.setProcess(6, udqqW);
  loop.setMomenta(CheckMoms);

  int hels2[][6] = {
    {+1, -1, -1, +1, +1, +1},
    {+1, -1, -1, +1, -1, +1}
  };

  int order2[][6] = {
    // table 7
    {0, 4, 5, 1, 2, 3},
    {0, 5, 4, 1, 2, 3},
    {0, 5, 1, 4, 2, 3},
    {0, 5, 1, 2, 4, 3},
    {0, 5, 1, 2, 3, 4},
    // table 8
    {0, 4, 5, 1, 3, 2},
    {0, 5, 4, 1, 3, 2},
    {0, 5, 1, 4, 3, 2},
    {0, 5, 1, 3, 4, 2},
    {0, 5, 1, 3, 2, 4},
    // table 9
    {0, 5, 4, 3, 2, 1},
    {0, 4, 5, 3, 2, 1},
    {0, 4, 3, 2, 5, 1},
    {0, 5, 3, 4, 2, 1},
    {0, 3, 4, 2, 5, 1},
    {0, 5, 3, 2, 4, 1},
    {0, 3, 2, 5, 4, 1},
    {0, 3, 2, 4, 5, 1},
    {0, 5, 3, 2, 1, 4},
    {0, 3, 2, 5, 1, 4},
    // table 10
    {3, 4, 0, 5, 1, 2},
    {3, 0, 5, 1, 2, 4},
    {3, 0, 5, 1, 4, 2},
    {3, 0, 5, 4, 1, 2},
    {3, 0, 4, 5, 1, 2}
  };

  int pos2[] = {0, 2, 3, 4, 5, 7, 8, 9, 10, 13, 15, 18, 20, 21, 22, 23};
  int nps2[] = {2, 1, 1, 1, 2, 1, 1, 1, 3,  2,  3,  2,  1,  1,  1,  2};

  cout << "## udccgW amplitudes" << endl;

  for (int prim=0; prim<16; prim++) {
    for (int h=0; h<2; h++) {
      loop.setHelicity(hels2[h]);
      EpsTriplet<double> ans;
      complex<double> tree;
      for (int p=pos2[prim]; p<pos2[prim]+nps2[prim]; p++) {
        ans += loop.eval(0, order2[p]);
        tree += loop.gettree();
      }
      cout << "0," << prim << "," << h << " = " << ans/tree << endl;
      cout << "## diff. " << EGKMZres[EGKMZ++] - ans/tree << endl;
    }
  }

  int order3[][6] = {
    // table 11
    {0, 5, 1, 2, 3, 4},
    {0, 5, 1, 2, 4, 3},
    {0, 5, 1, 4, 2, 3},
    {0, 5, 4, 1, 2, 3},
    {0, 4, 5, 1, 2, 3}
  };

  int pos3[] = {0, 1, 2, 3};
  int nps3[] = {1, 1, 1, 2};

  for (int prim=0; prim<4; prim++) {
    for (int h=0; h<2; h++) {
      loop.setHelicity(hels2[h]);
      EpsTriplet<double> ans;
      complex<double> tree;
      for (int p=pos3[prim]; p<pos3[prim]+nps3[prim]; p++) {
        ans += loop.eval(1, order3[p]);
        tree += loop.gettree();
      }
      cout << "1," << prim << "," << h << " = " << ans/tree << endl;
      cout << "## diff. " << EGKMZres[EGKMZ++] + ans/tree << endl;
    }
  }
}

inline
string verify(complex<double> a, complex<double> b)
{
  stringstream ss;
  double val = 0.5*abs((a+b));
  double dif = abs((a-b));
  double threshold = 1e-7;
  if (val < threshold) {
    threshold = 10.;
  }
  if (dif > threshold*val ) {
    ss << "fail (" << dif/val << a << b << ")";
  } else {
    ss << "pass";
  }
  return ss.str();
}

void VerifyNG2(const int N, int *ff, int* hh)
{
  double sqrtS = 7e3;

#ifdef USE_VC
  NParton2<Vc::double_v> NG2loop;
#else
  NParton2<double> NG2loop;
#endif

  NG2loop.setMuR2(0.25*sqrtS*sqrtS);

  NJet_sd::NParton NG1loop(N);
  NG1loop.setMuR2(0.25*sqrtS*sqrtS);
  std::vector<double> vmoms((N)*4);
  double (*moms)[][4] = reinterpret_cast<double(*)[][4]>(&vmoms[0]);

  PhaseSpace<double> ps(StandardModel::NGluon1compat(N, ff), 1, sqrtS);
  NG2loop.setProcess(StandardModel::NGluon1compat(N, ff));

  NG2loop.setMomenta(ps.getPSpoint());
  NG2loop.setHelicity(hh);

  ps.FillMomArray(*moms);
  NG1loop.setAmp(*moms, ff);
  NG1loop.eval(hh, NJet_sd::NParton::mixed);
  complex<double> tree1 = NG1loop.getAmp(NJet_sd::NParton::tree);
  complex<double> res1m = NG1loop.getAmp(NJet_sd::NParton::eps0)/tree1;
  NG1loop.eval(hh, NJet_sd::NParton::qloop);
  complex<double> res1f = NG1loop.getAmp(NJet_sd::NParton::eps0)/tree1;

  NG2loop.eval(0);
  complex<double> tree2 = NG2loop.gettree();
  complex<double> res2m = NG2loop.getres().get0()/tree2;

  NG2loop.eval(1);
  complex<double> res2f = NG2loop.getres().get0()/tree2;

  cout << "# Process : " << NG1loop.ProcessStr() << " [m] " << verify(res1m, res2m) << endl;
  cout << "# Process : " << NG1loop.ProcessStr() << " [f] " << verify(res1f, res2f) << endl;
}


void test_4gtoploop()
{
  cout << "# check against analytic formulae of Bern and Mogan hep-ph/9511336" << endl;
  double sqrtS = 7e3;
  double mt = 174.3;
  NParton2<double> loop;
  double MuR2 = 0.25*sqrtS*sqrtS;
  loop.setMuR2(MuR2);

  int hels[][4] = {
    {+1, +1, +1, +1},
    {+1, -1, +1, +1},
    {+1, -1, -1, +1},
    {+1, -1, +1, -1}
  };

  Flavour<double> ff[4] = {
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G()
  };
  loop.setProcess(4, ff);
  loop.setHeavyQuarkMass(mt);

  PhaseSpace<double> ps(4, ff, 1, sqrtS);
  loop.setMomenta(ps.getPSpoint());

  MOM<double> p1 = ps[1];
  MOM<double> p2 = ps[2];
  MOM<double> p3 = ps[3];
  MOM<double> p4 = ps[0];
  double mtsq = mt*mt;

  complex<double> X0pppp = i_*xspB(p1,p2)*xspB(p3,p4)/(xspA(p1,p2)*xspA(p3,p4));
  complex<double> X0mppp = -i_*xspB(p2,p4)*xspB(p2,p3)*xspA(p1,p2)/(xspB(p4,p1)*xspA(p2,p4)*xspA(p3,p4));
  complex<double> X0mmpp = i_*pow(xspA(p1,p2),3.)/(xspA(p2,p3)*xspA(p3,p4)*xspA(p4,p1));
  complex<double> X0mpmp = i_*pow(xspA(p1,p3),4.)/(xspA(p1,p2)*xspA(p2,p3)*xspA(p3,p4)*xspA(p4,p1));
  double s12 = S(p1+p2);
  double s13 = S(p1+p3);
  double s14 = S(p1+p4);

  EpsTriplet<double> BMpppp;
  BMpppp += X0pppp * (
      + 1./3.
      );

  BMpppp += I4(s12,s14,0.,0.,0.,0.,mtsq,mtsq,mtsq,mtsq,MuR2)*X0pppp
    * (
        - 2*pow(mtsq,2.)
      );

  EpsTriplet<double> BMmmpp;
  BMmmpp += X0mmpp * (
      - 2./9.
      );

  BMmmpp += I2(s14,mtsq,mtsq,MuR2)*X0mmpp * (
      + 2./3.
      - 2.*mtsq*pow(s12,-1.)
      + 4./3.*mtsq*pow(s14,-1.)
      );

  BMmmpp += I2(0.,mtsq,mtsq,MuR2)*X0mmpp * (
      - 4./3.
      + 2.*mtsq*pow(s12,-1.)
      - 4./3.*mtsq*pow(s14,-1.)
      );

  BMmmpp += I4(s12,s14,0.,0.,0.,0.,mtsq,mtsq,mtsq,mtsq,MuR2)*X0mmpp
    * (
        - mtsq*s14
        + 2.*pow(mtsq,2)*pow(s12,-1)*s14
      );

  EpsTriplet<double> BMmppp;
  BMmppp += X0mppp * (
      + 1.
      - 1./3.*pow(s12,-1.)*s13
      + s14*pow(s13,-1.)
      + 1./3.*pow(s14,2.)*pow(s13,-2.)
      + 1./3.*s12*pow(s14,-1.)
      + 2./3.*s12*pow(s13,-1.)
      + 1./3.*s12*s14*pow(s13,-2.)
      );

  BMmppp += I2(s12,mtsq,mtsq,MuR2)*X0mppp * (
      - 4*mtsq*pow(s12,-2.)*s13
      - 2*mtsq*pow(s12,-1.)
      );

  BMmppp += I2(s14,mtsq,mtsq,MuR2)*X0mppp * (
      + 10*mtsq*pow(s14,-1.)
      + 12*mtsq*pow(s13,-1.)
      + 4*mtsq*s14*pow(s13,-2.)
      + 4*mtsq*s12*pow(s14,-2.)
      + 8*mtsq*s12*pow(s14,-1.)*pow(s13,-1.)
      + 4*mtsq*s12*pow(s13,-2.)
      );

  BMmppp += I2(0.,mtsq,mtsq,MuR2)*X0mppp * (
      + 4*mtsq*pow(s12,-2.)*s13
      + 2*mtsq*pow(s12,-1.)
      - 10*mtsq*pow(s14,-1.)
      - 12*mtsq*pow(s13,-1.)
      - 4*mtsq*s14*pow(s13,-2.)
      - 4*mtsq*s12*pow(s14,-2.)
      - 8*mtsq*s12*pow(s14,-1.)*pow(s13,-1.)
      - 4*mtsq*s12*pow(s13,-2.)
      );

  BMmppp += I3(s12,0.,0.,mtsq,mtsq,mtsq,MuR2)*X0mppp * (
      + 2*mtsq*pow(s12,-1.)*s14
      - 2*mtsq*s14*pow(s13,-1)
      );

  BMmppp += I3(s14,0.,0.,mtsq,mtsq,mtsq,MuR2)*X0mppp * (
      + 2*mtsq*s12*pow(s14,-1.)
      - 2*mtsq*s12*pow(s13,-1.)
      );

  BMmppp += I4(s12,s14,0.,0.,0.,0.,mtsq,mtsq,mtsq,mtsq,MuR2)*X0mppp
    * (
        - mtsq*s12*s14*pow(s13,-1.)
        - 2*pow(mtsq,2.)
      );

  EpsTriplet<double> BMmpmp;
  BMmpmp += X0mpmp * (
      - 2./9.
      - s14*pow(s13,-1.)
      - pow(s14,2.)*pow(s13,-2.)
      );

  BMmpmp += I2(s12,mtsq,mtsq,MuR2)*X0mpmp * (
      - 5./3.*s14*pow(s13,-1.)
      - 3.*pow(s14,2)*pow(s13,-2.)
      - 2.*pow(s14,3)*pow(s13,-3.)
      + 4./3.*mtsq*pow(s12,-1.)
      + 4./3.*mtsq*pow(s13,-1.)
      + 2.*mtsq*s14*pow(s13,-2.)
      );

  BMmpmp += I2(s14,mtsq,mtsq,MuR2)*X0mpmp * (
      + 2./3.
      + 5./3.*s14*pow(s13,-1.)
      + 3.*pow(s14,2.)*pow(s13,-2.)
      + 2.*pow(s14,3.)*pow(s13,-3.)
      + 4./3.*mtsq*pow(s14,-1.)
      - 2./3.*mtsq*pow(s13,-1.)
      - 2.*mtsq*s14*pow(s13,-2.)
      );

  BMmpmp += I2(0.,mtsq,mtsq,MuR2)*X0mpmp * (
      - 4./3.
      - 4./3.*mtsq*pow(s12,-1.)
      - 4./3.*mtsq*pow(s14,-1.)
      - 2./3.*mtsq*pow(s13,-1.)
      );

  BMmpmp += I3(s12,0.,0.,mtsq,mtsq,mtsq,MuR2)*X0mpmp * (
      + pow(s12,2.)*s14*pow(s13,-2.)
      - 2*pow(s12,3.)*pow(s14,2.)*pow(s13,-4.)
      + 2*mtsq*s12*s14*pow(s13,-2.)
      + 2*mtsq*s12*pow(s14,2.)*pow(s13,-3.)
      - 2*mtsq*pow(s12,2.)*s14*pow(s13,-3.)
      );

  BMmpmp += I3(s14,0.,0.,mtsq,mtsq,mtsq,MuR2)*X0mpmp * (
      + s12*pow(s14,2.)*pow(s13,-2.)
      - 2.*pow(s12,2.)*pow(s14,3.)*pow(s13,-4.)
      + 2.*mtsq*s12*s14*pow(s13,-2.)
      - 2.*mtsq*s12*pow(s14,2.)*pow(s13,-3.)
      + 2.*mtsq*pow(s12,2.)*s14*pow(s13,-3.)
      );

  BMmpmp += I4(s12,s14,0.,0.,0.,0.,mtsq,mtsq,mtsq,mtsq,MuR2)*X0mpmp
    * (
        - 1./2.*pow(s12,2)*pow(s14,2.)*pow(s13,-2.)
        + pow(s12,3.)*pow(s14,3.)*pow(s13,-4.)
        - mtsq*s12*s14*pow(s13,-1.)
        + 4*mtsq*pow(s12,2.)*pow(s14,2.)*pow(s13,-3.)
        + 2*pow(mtsq,2.)*s12*s14*pow(s13,-2.)
      );

  // "fix" Bern-Morgan, sort of like charge renorm...
  double logm = 2./3.*log(mtsq/MuR2);

  EpsTriplet<double> BMcheck[] = {-BMpppp, -BMmppp, -BMmmpp+X0mmpp*logm, -BMmpmp+X0mpmp*logm};
  for (int h=0; h<4; h++) {
    loop.setHelicity(hels[h]);
    loop.eval(2);
    complex<double> tree = loop.gettree();
    EpsTriplet<double> ng2res = loop.getres();
    EpsTriplet<double> ng2err = loop.geterr();
    cout << "NG2 A0       = " << tree << endl;
    cout << "NG2 A1[m]    = " << ng2res << endl;
    cout << "NG2 (err)    = " << ng2err << endl;
    cout << "Bern+Morgan  = " << BMcheck[h] << endl;
    cout << "abs. err.    = " << ng2res-BMcheck[h] << endl;
  }

}

void test_4gNLO()
{
  cout << "# checking full colour/hel. summed g g > g g including top loops" << endl;
  cout << "# the 'renormalization' corrections to get agreement with MadLoop" << endl;
  cout << "# still don't make much sense to me." << endl;

  MOM<double> MGmom[][4] = {
    {
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00, -0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00,  0.5000000000000000E+03),
      MOM<double>( 0.4999999999999998E+03,  0.1109242844438328E+03,  0.4448307894881214E+03, -0.1995529299308788E+03),
      MOM<double>( 0.5000000000000000E+03, -0.1109242844438328E+03, -0.4448307894881214E+03,  0.1995529299308787E+03)
    },{
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00, -0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00,  0.5000000000000000E+03),
      MOM<double>( 0.5000000000000000E+03, -0.2819155058093908E+03, -0.3907909666011574E+03,  0.1334393795218208E+03),
      MOM<double>( 0.5000000000000002E+03,  0.2819155058093908E+03,  0.3907909666011575E+03, -0.1334393795218208E+03)
    },{
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00, -0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00,  0.5000000000000000E+03),
      MOM<double>( 0.4999999999999990E+03, -0.2406210290937675E+03,  0.2804720059911282E+03, -0.3368040590805997E+03),
      MOM<double>( 0.5000000000000012E+03,  0.2406210290937674E+03, -0.2804720059911282E+03,  0.3368040590805997E+03)
    },{
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00, -0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00,  0.5000000000000000E+03),
      MOM<double>( 0.5000000000000001E+03,  0.4510352131035046E+03,  0.6047968715179463E+02, -0.2071459485065961E+03),
      MOM<double>( 0.5000000000000001E+03, -0.4510352131035046E+03, -0.6047968715179463E+02,  0.2071459485065960E+03)
    },{
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00, -0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00,  0.5000000000000000E+03),
      MOM<double>( 0.5000000000000007E+03,  0.4557289838719782E+03, -0.2036894622960882E+03, -0.2866524391218178E+02),
      MOM<double>( 0.5000000000000007E+03, -0.4557289838719784E+03,  0.2036894622960884E+03,  0.2866524391218240E+02)
    }
  };
  double MG5res[] = {
    -8.5722720530574943,
    -8.0266337579865574,
    -11.182654512934072,
    -8.6558371951782416,
    -7.6611972536224426
  };

  double sqrtS = 1e3;
  double mt = 173.0;
  double mb = 4.7;
  NParton2<double> loop;
  double MuR2 = sqrtS*sqrtS;
  loop.setMuR2(MuR2);

  // ignore helicities vanishing at tree-level
  int hels[6][4] = {
    {-1, -1, +1, +1},
    {-1, +1, -1, +1},
    {-1, +1, +1, -1},
    {+1, -1, -1, +1},
    {+1, -1, +1, -1},
    {+1, +1, -1, -1}
  };

  Flavour<double> ff[4] = {
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G()
  };
  loop.setProcess(4, ff);

  int odr0[2][4] = {
    {0,1,2,3},
    {0,1,3,2}
  };

  int odr1[3][4] = {
    {0,1,2,3},
    {0,2,1,3},
    {0,2,3,1}
  };

  double Col0[2][2] = {
    {4., 2.},
    {2., 4.}
  };

  double Col1g[2][3] = {
    {2., -2., 0.},
    {0., -2., 2.}
  };

  double Col1f[2][3] = {
    {-2., 2.,  0.},
    { 0., 2., -2.}
  };


  // parameters
  double Nc = 3.;
  double INc = 1/Nc;
  double Vc = Nc*Nc - 1.;
  double Nf = 4.;
  double Nh = 2.;
  double mh[2] = {mb, mt};
  double as = 0.118;
  double gs = sqrt(4.*M_PI*as);

  double chave = Vc*Vc*4.*2.;
  double prefactor0 = Vc*Nc*Nc*pow(gs, 4.)/chave;
  double prefactor1 = pow(gs, 2.)*prefactor0;

  for (int pt=0; pt<5; pt++) {
    loop.setMomenta(MGmom[pt]);

    complex<double> Amp0 = 0.;
    EpsTriplet<double> Amp1;

    for (int h=0; h<6; h++) {
      loop.setHelicity(hels[h]);

      complex<double> tamps[2];
      for (int p=0; p<2; p++) {
        tamps[p] = loop.evalTree(odr0[p]);
      }

      EpsTriplet<double> pamps0[3];
      EpsTriplet<double> pamps1[3];
      EpsTriplet<double> pamps2[3];
      for (int p=0; p<3; p++) {
        pamps0[p] = loop.eval(0, odr1[p]);
        pamps1[p] = loop.eval(1, odr1[p]);
        for (int HQ = 0; HQ < int(Nh); HQ++) {
          loop.setHeavyQuarkMass(mh[HQ]);
          pamps2[p] += loop.eval(2, odr1[p]);
        }
      }

      for (int i=0; i<2; i++) {
        complex<double> row = 0.;
        for (int j=0; j<2; j++) {
          row += Col0[i][j]*tamps[j];
        }
        Amp0 += conj(tamps[i])*row;
      }

      for (int i=0; i<2; i++) {
        EpsTriplet<double> rowg, rowf, rowh;
        for (int j=0; j<3; j++) {
          rowg += Col1g[i][j]*pamps0[j];
          rowf += Col1f[i][j]*pamps1[j];
          rowh += Col1f[i][j]*pamps2[j];
        }
        Amp1 += conj(tamps[i])*(Nc*rowg+Nf*rowf+rowh);
      }

    }

    // NO Nh term in beta0!
    double beta0 = (11.*Nc-2.*Nf)/3.;
    double nq = 0.;
    double ng = 4.;
    double n = ng + nq;
    EpsTriplet<double> renorm(
        (nq-2.)*Nc/6. - nq*Vc*INc*0.25,
        -(n-2.)*0.5*beta0,
        0.);

    cout << "|A0|^2           = " << prefactor0*Amp0 << endl;
    cout << "Re(A1.A0)        = " << prefactor1*(Amp1+renorm*Amp0) << endl;
    cout << "Re(A1.A0)/|A0|^2 = " << (Amp1+renorm*Amp0)/Amp0 << endl;
    cout << "check            = " << MG5res[pt] << "  diff(" << (real((Amp1+renorm*Amp0).get0())/Amp0-MG5res[pt])/MG5res[pt] << ")" << endl;
  }
}

void test_2q2gNLO()
{
  cout << "# checking full colour/hel. summed u u~ > g g including top loops" << endl;
  cout << "# the 'renormalization' corrections to get agreement with MadLoop" << endl;
  cout << "# still don't make much sense to me." << endl;

  MOM<double> MGmom[][4] = {
    {
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00, -0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00,  0.5000000000000000E+03),
      MOM<double>( 0.4999999999999998E+03,  0.1109242844438328E+03,  0.4448307894881214E+03, -0.1995529299308788E+03),
      MOM<double>( 0.5000000000000000E+03, -0.1109242844438328E+03, -0.4448307894881214E+03,  0.1995529299308787E+03)
    },{
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00, -0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00,  0.5000000000000000E+03),
      MOM<double>( 0.5000000000000000E+03, -0.2819155058093908E+03, -0.3907909666011574E+03,  0.1334393795218208E+03),
      MOM<double>( 0.5000000000000002E+03,  0.2819155058093908E+03,  0.3907909666011575E+03, -0.1334393795218208E+03)
    },{
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00, -0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00,  0.5000000000000000E+03),
      MOM<double>( 0.4999999999999990E+03, -0.2406210290937675E+03,  0.2804720059911282E+03, -0.3368040590805997E+03),
      MOM<double>( 0.5000000000000012E+03,  0.2406210290937674E+03, -0.2804720059911282E+03,  0.3368040590805997E+03)
    },{
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00, -0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00,  0.5000000000000000E+03),
      MOM<double>( 0.5000000000000001E+03,  0.4510352131035046E+03,  0.6047968715179463E+02, -0.2071459485065961E+03),
      MOM<double>( 0.5000000000000001E+03, -0.4510352131035046E+03, -0.6047968715179463E+02,  0.2071459485065960E+03)
    },{
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00, -0.5000000000000000E+03),
      MOM<double>(-0.5000000000000000E+03,  0.0000000000000000E+00,  0.0000000000000000E+00,  0.5000000000000000E+03),
      MOM<double>( 0.5000000000000007E+03,  0.4557289838719782E+03, -0.2036894622960882E+03, -0.2866524391218178E+02),
      MOM<double>( 0.5000000000000007E+03, -0.4557289838719784E+03,  0.2036894622960884E+03,  0.2866524391218240E+02)
    }
  };
  double MG5res[] = {
    -1.2158385428340144,
    0.99155829331183765,
    -8.2868830443031243,
    -1.5124416826020393,
    2.8675670510125828
  };

  double sqrtS = 1e3;
  double mt = 173.0;
  double mb = 4.7;
  NParton2<double> loop;
  double MuR2 = sqrtS*sqrtS;
  loop.setMuR2(MuR2);

  // ignore helicities vanishing at tree-level
  int hels[4][4] = {
    {-1, +1, -1, +1},
    {-1, +1, +1, -1},
    {+1, -1, -1, +1},
    {+1, -1, +1, -1}
  };

  Flavour<double> ff[4] = {
    StandardModel::ubar(),
    StandardModel::u(),
    StandardModel::G(),
    StandardModel::G()
  };
  loop.setProcess(4, ff);

  int odr[6][4] = {
    {0,1,2,3},
    {0,3,2,1},
    {0,1,3,2},
    {0,2,3,1},
    {0,2,1,3},
    {0,3,1,2}
  };

  // parameters
  double Nc = 3.;
  double Nf = 4.;
  double Nh = 2.;
  double mh[2] = {mb, mt};
  double Vc = Nc*Nc - 1.;
  double INc = 1./Nc;
  double as = 0.118;
  double gs = sqrt(4.*M_PI*as);

  double Col0[2][2] = {
    {Vc*INc, -INc},
    {-INc, Vc*INc}
  };

  double Col1g[2][3] = {
    {Vc*INc, -INc, 1.},
    {-INc, Vc*INc, 1.}
  };

  double Col1f[2][2] = {
    {Vc*INc, -INc},
    {-INc, Vc*INc}
  };

  double chave = Nc*Nc*4.*2.;
  double prefactor0 = Vc*pow(gs, 4.)/chave;
  double prefactor1 = pow(gs, 2.)*prefactor0;

  for (int pt=0; pt<5; pt++) {
    loop.setMomenta(MGmom[pt]);

    complex<double> Amp0 = 0.;
    EpsTriplet<double> Amp1;

    for (int h=0; h<4; h++) {
      loop.setHelicity(hels[h]);

      complex<double> A0123 = loop.evalTree(odr[0]);
      complex<double> A0132 = loop.evalTree(odr[2]);
      complex<double> tamps[2] = {A0123, A0132};

      EpsTriplet<double> M0123 = loop.eval(0, odr[0]);
      EpsTriplet<double> M0321 = loop.eval(0, odr[1]);
      EpsTriplet<double> M0132 = loop.eval(0, odr[2]);
      EpsTriplet<double> M0231 = loop.eval(0, odr[3]);
      EpsTriplet<double> M0213 = loop.eval(0, odr[4]);
      EpsTriplet<double> M0312 = loop.eval(0, odr[5]);
      EpsTriplet<double> F0123 = loop.eval(1, odr[0]);
      EpsTriplet<double> H0123;
      for (int HQ = 0; HQ < int(Nh); HQ++) {
        loop.setHeavyQuarkMass(mh[HQ]);
        H0123 += loop.eval(2, odr[0]);
      }

      EpsTriplet<double> pampsM[3] = {
        Nc*M0123 - INc*M0321,
        Nc*M0132 - INc*M0231,
        M0123 + M0132 + M0231 + M0321 + M0213 + M0312
      };
      EpsTriplet<double> pampsF[2] = {
        -F0123,
        F0123
      };
      EpsTriplet<double> pampsH[2] = {
        -H0123,
        H0123
      };

      for (int i=0; i<2; i++) {
        complex<double> row = 0.;
        EpsTriplet<double> rowf, rowh;
        for (int j=0; j<2; j++) {
          row += Col0[i][j]*tamps[j];
          rowf += Col1f[i][j]*pampsF[j];
          rowh += Col1f[i][j]*pampsH[j];
        }
        Amp0 += conj(tamps[i])*row;
        Amp1 += conj(tamps[i])*(Nf*rowf+rowh);
      }

      for (int i=0; i<2; i++) {
        EpsTriplet<double> rowg, rowf, rowh;
        for (int j=0; j<3; j++) {
          rowg += Col1g[i][j]*pampsM[j];
        }
        Amp1 += conj(tamps[i])*rowg;
      }

    }

    // No Nh term in beta0!
    double beta0 = (11.*Nc-2.*Nf)/3.;
    double nq = 2.;
    double ng = 2.;
    double n = ng + nq;
    EpsTriplet<double> renorm(
        (nq-2.)*Nc/6. - nq*Vc*INc*0.25,
        -(n-2.)*0.5*beta0,
        0.);

    cout << "|A0|^2           = " << prefactor0*Amp0 << endl;
    cout << "Re(A1.A0)        = " << prefactor1*(Amp1+renorm*Amp0) << endl;
    cout << "Re(A1.A0)/|A0|^2 = " << (Amp1+renorm*Amp0)/Amp0 << endl;
    cout << "check            = " << MG5res[pt] << "  diff(" << (real((Amp1+renorm*Amp0).get0())/Amp0-MG5res[pt])/MG5res[pt] << ")" << endl;
  }
}

int Flavs4[10][4] = {
  {0,0,0,0},
  {-1,1,0,0},
  {-1,0,1,0},
  {-1,0,0,1},
  {1,-1,0,0},
  {1,0,-1,0},
  {1,0,0,-1},
  {-1,-2,2,1},
  {1,-1,-2,2},
  {-1,1,-2,2}
};
int Hels4[10][4] = {
  {-1,1,1,-1},
  {-1,1,1,-1},
  {-1,1,1,-1},
  {-1,1,-1,1},
  {-1,1,1,-1},
  {-1,1,1,-1},
  {-1,1,-1,1},
  {-1,1,-1,1},
  {-1,1,1,-1},
  {-1,1,1,-1}
};

int Flavs6[2][6] = {
  {0,0,0,0,0,0},
  {-1,1,2,-3,3,-2}
};
int Hels6[2][6] = {
  {-1,1,-1,1,-1,1},
  {-1,1,-1,1,-1,1}
};

int Flavs7[2][7] = {
  {0,0,0,0,0,0,0},
  {-1,0,-2,0,2,1,0}
};
int Hels7[2][7] = {
  {-1,1,-1,1,-1,1,-1},
  {-1,1,1,-1,-1,1,-1}
};

int main(int argc, char **argv)
{
  cout.setf(ios_base::scientific, ios_base::floatfield);

  cout.precision(16);
  if (0 != parseOptions(argc, argv)) return 1;

  if (verifyNG2) {
    for (int k=0; k<10; k++) {
      VerifyNG2(4, Flavs4[k], Hels4[k]);
    }
    for (int k=0; k<2; k++) {
      VerifyNG2(6, Flavs6[k], Hels6[k]);
      VerifyNG2(7, Flavs7[k], Hels7[k]);
    }
  }

//  test_ppW();

//  test_ppWj();
  test_ppWjjj();

//  test_ppZj();
//  Flavour<double> ff[4] = {
//    StandardModel::ubar(),
//    StandardModel::u(),
//    StandardModel::Z_uubar(),
//    StandardModel::G()
//  };

//  ppZj_qloops(ff);

//  test_4gtoploop();

//  test_4gNLO();

//  test_2q2gNLO();

  return 0;
}
