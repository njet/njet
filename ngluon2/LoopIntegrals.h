/*
* ngluon2/LoopIntegrals.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef NGLUON2_LOOPINTEGRALS_H
#define NGLUON2_LOOPINTEGRALS_H

#include <limits>
#include <complex>
#include <iostream>

#include "../common.h"

#include "EpsTriplet.h"

#ifdef QCDLOOP

#ifndef F77_FUNC
  #define F77_FUNC(name,NAME) name ## _
#endif

#define qlprec F77_FUNC(ffprec,FFPREC)
#define qlflag F77_FUNC(ffflag,FFFLAG)
extern "C" {
  typedef struct {double re; double im;} fortran_complex;

  void F77_FUNC(qlinit,QLINIT)();
  void F77_FUNC(ffexi,FFEXI)();

#ifdef USE_F2C
  void F77_FUNC(qli1,QLI1)(fortran_complex *rslt, double *m1, double *mu2, int *ep);
  void F77_FUNC(qli2,QLI2)(fortran_complex *rslt,
                           double *p1, double *m1, double *m2, double *mu2, int *ep);
  void F77_FUNC(qli3,QLI3)(fortran_complex *rslt,
                           double *p1, double *p2, double *p3,
                           double *m1, double *m2, double *m3, double *mu2, int *ep);
  void F77_FUNC(qli4,QLI4)(fortran_complex *rslt,
                           double *p1, double *p2, double *p3, double *p4,
                           double *s12, double *s23,
                           double *m1, double *m2, double *m3, double *m4,
                           double *mu2, int *ep);
#else // USE_F2C
  fortran_complex F77_FUNC(qli1,QLI1)(double *m1, double *mu2, int *ep);
  fortran_complex F77_FUNC(qli2,QLI2)(double *p1, double *m1, double *m2, double *mu2, int *ep);
  fortran_complex F77_FUNC(qli3,QLI3)(double *p1, double *p2, double *p3,
      double *m1, double *m2, double *m3, double *mu2, int *ep);
  fortran_complex F77_FUNC(qli4,QLI4)(double *p1, double *p2, double *p3, double *p4,
      double *s12, double *s23,
      double *m1, double *m2, double *m3, double *m4,
      double *mu2, int *ep);
#endif // USE_F2C
  extern struct {
    double xloss,precx,precc,xalogm,xclogm,xalog2,xclog2,reqprc;
  } qlprec;
  extern struct {
    int lwrite, ltest, l4also, ldc3c4, lmem, lwarn, ldot,
        nevent, ner, id, idsub, nwidth, nschem, onshel, idot;
  } qlflag;
}
#endif

template <typename T>
EpsTriplet<T> I4(const T s, const T t,
                 const T M1, const T M2, const T M3, const T M4,
                 const T m1, const T m2, const T m3, const T m4, const T MUR2);

template <typename T>
EpsTriplet<T> I3(const T M1, const T M2, const T M3,
                 const T m1, const T m2, const T m3, const T MUR2);

template <typename T>
EpsTriplet<T> I2(const T s, const T m1, const T m2, const T MUR2);

void myqlinit();
void myqlexit();

#endif /* NGLUON2_LOOPINTEGRALS_H */
