/*
* ngluon2/Model.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef NGLUON2_MODEL_H
#define NGLUON2_MODEL_H

#include "ModelBase.h"
#include "Flavour.h"

class Model : public ModelBase
{
  public:
    typedef Flavour<double> RealFlavour;

};

class StandardModel : public Model
{
  public:
    enum SMPARTICLES {
      qU=LQuark, qD, qC, qS, qB, qT=HQuark, qIL=ILQuark
    };

    static RealFlavour::FlavourList NGluon1compat(const int n, const int* farr);

    static RealFlavour G() { return RealFlavour(Gluon); }
    static RealFlavour u() { return RealFlavour(qU); }
    static RealFlavour d() { return RealFlavour(qD); }
    static RealFlavour c() { return RealFlavour(qC); }
    static RealFlavour s() { return RealFlavour(qS); }
    static RealFlavour b() { return RealFlavour(qB); }

    static RealFlavour IL() { return RealFlavour(qIL); }

    static RealFlavour ubar() { return RealFlavour(-qU); }
    static RealFlavour dbar() { return RealFlavour(-qD); }
    static RealFlavour cbar() { return RealFlavour(-qC); }
    static RealFlavour sbar() { return RealFlavour(-qS); }
    static RealFlavour bbar() { return RealFlavour(-qB); }

    // to constuct massive anti-quarks use Flavour::C()
    static RealFlavour t(double m=Tmass) { return RealFlavour(qT, m); }

    static RealFlavour Wp_udbar() { return Wp(u(), d().C()); }
    static RealFlavour Wm_dubar() { return Wm(d(), u().C()); }
    static RealFlavour Z_uubar() { return Zu(u(), u().C()); }
    static RealFlavour Z_ddbar() { return Zd(d(), d().C()); }
    static RealFlavour V_uubar() { return Gstar(u(), u().C()); }

    static RealFlavour Z_LoopU() { return Zu(IL(), IL().C()); }
    static RealFlavour Z_LoopD() { return Zd(IL(), IL().C()); }

    static RealFlavour Higgs(double m=Hmass, double w=Hwidth) { return RealFlavour(ModelBase::Higgs, m, w); }

    static RealFlavour BosonFlip(const RealFlavour& ff);
    static RealFlavour BosonNext(const RealFlavour& ff);

    static int getNf() { return Nf; }
    static void setNf(int n) {
      Nf = n;
    }

    static double getHmass() { return Hmass; }

    static void setHmass(double m, double w=Hwidth) {
      Hmass = m;
      Hwidth = w;
    }

    static void setHwidth(double w) {
      Hwidth = w;
    }

    static void setTmass(double m) {
      Tmass = m;
    }

    static void setWmass(double m, double w=Wwidth) {
      Wmass = m;
      Wwidth = w;
    }

    static void setWwidth(double w) {
      Wwidth = w;
    }

    static void setZmass(double m, double w=Zwidth) {
      Zmass = m;
      Zwidth = w;
    }

    static void setZwidth(double w) {
      Zwidth = w;
    }

    static void setSinThetaWSq(double se2) {
      SinThetaWSq = se2;
    }

    static double getSinThetaWSq() {
      return SinThetaWSq;
    }

    static double getCKM(const RealFlavour& f1, const RealFlavour& f2) {
      double* ckmel = refCKM(f1, f2);
      if (ckmel) {
        return *ckmel;
      } else {
        return 0.;
      }
    }

    static void setCKM(const RealFlavour& f1, const RealFlavour& f2, double x) {
      double* ckmel = refCKM(f1, f2);
      if (ckmel) {
        *ckmel = x;
      }
    }

    static RealFlavour Wp(const RealFlavour& f1, const RealFlavour& f2,
                          double cpl=0., double m=Wmass, double w=Wwidth);
    static RealFlavour Wm(const RealFlavour& f1, const RealFlavour& f2,
                          double cpl=0., double m=Wmass, double w=Wwidth);

    static RealFlavour Zu(const RealFlavour& f1, const RealFlavour& f2,
                          double m=Zmass, double w=Zwidth);
    static RealFlavour Zd(const RealFlavour& f1, const RealFlavour& f2,
                          double m=Zmass, double w=Zwidth);

    static RealFlavour Gstar(const RealFlavour& f1, const RealFlavour& f2);

    static RealFlavour Au(const RealFlavour& f1, const RealFlavour& f2,
                          int flav=PhotonU1);
    static RealFlavour Ad(const RealFlavour& f1, const RealFlavour& f2,
                          int flav=PhotonD1);
    static RealFlavour Ax(const RealFlavour& f1, const RealFlavour& f2,
                          int flav=PhotonL);

  private:
    static int SU2Flip(int flav);

    static int upidx(const RealFlavour& f);
    static int downidx(const RealFlavour& f);
    static double* refCKM(const RealFlavour& f1, const RealFlavour& f2);

    static const int up[3];
    static const int down[3];
    static double CKM[3][3];
    static double CKMfull[3][3];

    // default parameters used by BLHA and NJetInterface
    static int Nf;                 // number of light flavours

    static double Tmass;
    static double Wmass, Wwidth;
    static double Zmass, Zwidth;
    static double Hmass, Hwidth;
    static double SinThetaWSq;
};

#endif /* NGLUON2_MODEL_H */
