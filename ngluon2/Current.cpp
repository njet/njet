/*
* ngluon2/Current.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <iostream>
#include <algorithm>

#include "Current.h"

#include "Current-vertices.cpp"

template <typename T>
Current<T>::Current()
{
}

template <typename T>
Current<T>::Current(const int n_, const RealFlavour* flavarr)
{
  setProcess(n_, flavarr);
}

template <typename T>
Current<T>::Current(const RealFlavourList& flavours)
{
  setProcess(flavours.size(), flavours.data());
}

template <typename T>
template <typename U>
Current<T>::Current(const int n_, const Flavour<U>* otherflavarr)
{
  setProcess(n_, otherflavarr);
}

template <typename T>
template <typename U>
Current<T>::Current(const std::vector<Flavour<U> >& otherflavours)
{
  setProcess(otherflavours.size(), otherflavours.data());
}

// setting the process
template <typename T>
void Current<T>::setProcess(const int n_, const RealFlavour* flavarr)
{
  initialize(n_, flavarr);
}

template <typename T>
void Current<T>::setProcess(const RealFlavourList& flavours)
{
  setProcess(flavours.size(), flavours.data());
}

template <typename T>
template <typename U>
void Current<T>::setProcess(const int n_, const Flavour<U>* otherflavarr)
{
  RealFlavourList flavours(n_);
  for (int i=0; i<n_; i++) {
    flavours[i] = RealFlavour(otherflavarr[i]);
  }
  setProcess(flavours.size(), flavours.data());
}

template <typename T>
template <typename U>
void Current<T>::setProcess(const std::vector<Flavour<U> >& otherflavours)
{
  setProcess(otherflavours.size(), otherflavours.data());
}

template <typename T>
void Current<T>::setFlavourAt(const RealFlavour& flavour, const unsigned int i)
{
  if (base_process.size() < i+1) {
    base_process.resize(i+1);
    loop_process.resize(i+1);
  }
  base_process[i] = RealParticle(flavour);
  flavmap[abs(base_process[i].idx())] = i;
  loop_process[i] = ComplexParticle(base_process[i].DimShift());
  loopmap[abs(loop_process[i].idx())] = i;
}

// initialize all internal variables
template <typename T>
void Current<T>::initialize(const int n_, const RealFlavour* flavarr)
{
  n = n_;
  base_process.reserve(n+5);
  loop_process.reserve(n+5);
  base_process.resize(n+1);  // n + null
  loop_process.resize(n+1);  // n + null
  process.resize(n);
  order.resize(n);

  ordered = false;
  precomputed = false;
  for (int i=0; i<n; i++) {
    order[i] = i;
  }

  // initialize flavour maps with n (NullParticle index)
  base_process[n] = RealParticle();
  loop_process[n] = ComplexParticle();
  for (int i=0; i<EndFlavour; i++) {
    flavmap[i] = n;
    loopmap[i] = n;
  }
  nullpos = n;
  heavypos = n;  // no heavy loop flavour by default

  legs = 0;
  for (int i=0; i<n; i++) {
    setFlavourAt(flavarr[i], i);
    legs += base_process[i].decayMultiplicity();
  }

  // make sure we have gluon
  if (flavmap[Gluon] == int(nullpos)) {
    const unsigned int pos = base_process.size();
    setFlavourAt(RealFlavour(Gluon), pos);
  }

  // initialize light quark-loop flavour (heavy loop flavour has to be added by setHeavyLoopFlavour)
  const unsigned int pos = base_process.size();
  setFlavourAt(RealFlavour(ILQuark), pos);
  lightpos = pos;

  init_v3table();  // call after base_process and loop_process are filled

  havehiggs = false;
  for (int i=0; i<n; i++) {
    havehiggs |= base_process[i].isHiggs();
  }

  J.resize(n);
  Jmom.resize(n);
  lJ.resize(n+1);
  lJmom.resize(n+1);
}

template <typename T>
void Current<T>::setHeavyLoopFlavour(const T mass)
{
  if (base_process[heavypos].isBlank()) {
    const unsigned int pos = base_process.size();
    setFlavourAt(RealFlavour(IHQuark, mass), pos);
    heavypos = pos;

    init_v3table();  // refresh vertex table
  } else {
    base_process[heavypos].setMass(mass);
  }
}

template <typename T>
void Current<T>::setMomenta(const RealMom* moms)
{
  int j = 0;
  for (int i=0; i<n; i++) {
    j += base_process[i].setMomentum(&moms[j]);
  }
  ordered = false;
  precomputed = false;
}

template <typename T>
void Current<T>::setMomenta(const std::vector<RealMom>& moms)
{
  setMomenta(moms.data());
}

template <typename T>
void Current<T>::setHelicity(const int* helicity)
{
  for (int i=0; i<n; i++) {
    base_process[i].setExtHelicity(helicity[i], base_process);
    process[i] = base_process[order[i]];  // avoid reordering afer setHelicity
  }
  precomputed = false;
}

template <typename T>
void Current<T>::setHelicity(const std::vector<int>& helicity)
{
  setHelicity(helicity.data());
}

template <typename T>
void Current<T>::setOrder(const int* ord)
{
  order.assign(&ord[0], &ord[n]);
  ordered = false;
  precomputed = false;
}

template <typename T>
void Current<T>::setOrder(const std::vector<int>& ord)
{
  setOrder(ord.data());
}

template <typename T>
void Current<T>::reorder()
{
  if (ordered) {
    return;
  }
  for (int i=0; i<n; i++) {
    process[i] = base_process[order[i]];
  }
  ordered = true;
}

template <typename T>
T Current<T>::getRealFlavourMass(const int flav) const
{
  return base_process[flavmap[abs(flav)]].Mass();
}

template <typename T>
complex<T> Current<T>::getComplexFlavourMass(const int flav) const
{
  return loop_process[loopmap[abs(flav)]].Mass();
}

template <typename T>
complex<T> Current<T>::getLoopFlavourMass(const int flav) const
{
  // TODO it may be better to put real flavour indices to loopmap and use setLoopMass(0) for CC part
  const unsigned int aflav = abs(flav);
  const unsigned int i = loopmap[aflav];
  if (i != nullpos) {
    return loop_process[i].Mass();
  } else {
    return base_process[flavmap[aflav]].Mass();
  }
}

template <typename T>
void Current<T>::setLoopMass(const complex<T>& mDsq)
{
  const complex<T> mD = sqrt(mDsq);
  const int N = base_process.size();
  for (int i=0; i<N; i++) {
    if (isMassiveFermion(base_process[i].idx())) {  // other massive particles cannot appear in a loop
      const T m = base_process[i].Mass();
      loop_process[i].setMass(sqrt(mDsq + m*m));
    } else {
      loop_process[i].setMass(mD);
    }
  }
}

template <typename T>
template <typename U1, typename U2>
SubCurrent<T> Current<T>::InvProp(const SubCurrent<T>& J1, const MOM<U1>& P, const U2 m) const
{
  switch (J1.type) {
    case 3: {
      return (1./S(P))*J1;
    }
    case 1: {
      SubCurrent<T> ans = (-SIGNqqg/(S(P) - m*m))*(ContractSlash(SubCurrent<T>(P), J1) - m*J1);
      ans.flav = J1.flav;
      return ans;
    }
    case -1: {
      SubCurrent<T> ans = (SIGNqqg/(S(P) - m*m))*(ContractSlash(SubCurrent<T>(P), J1) + m*J1);
      ans.flav = J1.flav;
      return ans;
    }
    case 2: {
      return (1./(S(P) - m*m))*J1;
    }
    case 0: {
      return J1;
    }
    default: {
      NJET_ERROR("Error: unknown propagator type " << J1.type);
    }
  }
}

template <typename T>
SubCurrent<T> Current<T>::getCurrent(int i1, int i2)
{
  SubCurrent<T> ans;

  if (i1 == i2) {
    ans = process[i1%n].getPol();
    Jmom(i1, i1) = process[i1%n].getMom();
  } else {
    if (i2 < i1) {
      NJET_ERROR("cyclic getCurrent calls not implemented")
    }

    for (int k1 = i1; k1 < i2; k1++) {
      if (J(i1, k1).type != 0 && J(k1+1, i2).type != 0) {
        ans += V3(J(i1, k1), J(k1+1, i2), Jmom(i1, k1), Jmom(k1+1, i2));
      }
    }

    for (int k1 = i1; k1 < i2-1; k1++) {
      for (int k2 = k1+1; k2 < i2; k2++) {
        if (J(i1, k1).type != 0 && J(k1+1, k2).type != 0 && J(k2+1, i2).type != 0) {
          ans += V4(J(i1, k1), J(k1+1, k2), J(k2+1, i2), Jmom(i1, k1), Jmom(k1+1, k2), Jmom(k2+1, i2));
        }
      }
    }

    if (havehiggs)
    for (int k1 = i1; k1 < i2-2; k1++) {
      for (int k2 = k1+1; k2 < i2-1; k2++) {
        for (int k3 = k2+1; k3 < i2; k3++) {
          if (J(i1, k1).type != 0 && J(k1+1, k2).type != 0 && J(k2+1, k3).type != 0 && J(k3+1, i2).type != 0) {
            ans += V5(J(i1, k1), J(k1+1, k2), J(k2+1, k3), J(k3+1, i2));
          }
        }
      }
    }

    Jmom(i1, i2) = process[i2%n].getMom() + Jmom(i1, i2-1);

    if (i2-i1 != n-2 && ans.flav != NullFlavour) {
      const T m = getRealFlavourMass(ans.flav);  // maybe move InvProp to Particle?
      ans = InvProp(ans, Jmom(i1, i2), m);
    }
  }

  return ans;
}

template <typename T>
complex<T> Current<T>::eval()
{
  if (not precomputed) {
    reorder();
    for (int s = 0; s < n-1; s++) {
      for (int i = 0; i < n-1-s; i++) {
        J(i, i+s) = getCurrent(i, i+s);
      }
    }
    J(n-1, n-1) = getCurrent(n-1, n-1);
  }

//   if (isGluon(process[n-1].idx()) and J(0, n-2).type != 0) {
//     cout << "# Gauge check: " << Contract(process[n-1].getMom(), J(0, n-2)) << endl;
//   }

  if (J(0, n-2).type == 0) {
    return T(0.);
  } else {
    return i_*Contract(process[n-1].getPol(), J(0, n-2));
  }
}

template <typename T>
void Current<T>::fillCurrents()
{
  reorder();
  if (precomputed) {
    return;
  }

  for (int s = 0; s < n-1; s++) {
    for (int i = 0; i < n; i++) {
      J(i, i+s) = getCurrent(i, i+s);
    }
  }

  precomputed = true;
}


template <typename T>
SubCurrent<T> Current<T>::Ceval(const ComplexParticle& L1, const SubCurrent<T>& PL1,
                                int i1, int i2)
{
#ifndef NDEBUG
  if (!precomputed) {
    NJET_ERROR("Error: call Current::fillCurrents() first");
  }
#endif

  const int I2 = i2 < i1 ? i2+n : i2;

  // avoid overcounting currents in multi-quark case
  int q1 = n;
  int q2 = 0;
  if (I2 >= n) {
    int qflav = NullFlavour;
    for (int i=0; i<n; i++) {
      if (isFermion(process[i].idx())) {
        if (q1 == n) {
          qflav = process[i].idx();
          q1 = i;
        } else if (process[i].idx() == -qflav) {
          q2 = i;
          if (q1 < i1 && q2 >= i1) {
            q1 += n;
            break;
          } else {
            q1 = n;
            q2 = 0;
          }
        }
      }
    }
  }

  const int m = (I2 - i1 + n) % n;

  lJ[0] = PL1;
  lJmom[0] = L1.getMom();

  for (int s = i1; s <= I2; s++) {
    SubCurrent<T> ans;

    // we skip tree currents with qq~ pair which was cut
    const int k1min = q1 <= s ? std::max(i1, q2+1) : i1;
    for (int k1 = k1min; k1 <= s; k1++) {
      if (lJ[k1-i1].type != 0 && J(k1, s).type != 0) {
        ans += V3(lJ[k1-i1], J(k1, s), lJmom[k1-i1], Jmom(k1, s));
      }
    }

    for (int k1 = i1; k1 <= s-1; k1++) {
      for (int k2 = k1; k2 < s; k2++) {
        if (lJ[k1-i1].type != 0 && J(k1, k2).type != 0 && J(k2+1, s).type != 0) {
          ans += V4(lJ[k1-i1], J(k1, k2), J(k2+1, s), lJmom[k1-i1], Jmom(k1, k2), Jmom(k2+1, s));
        }
      }
    }

    if (havehiggs)
    for (int k1 = i1; k1 < s-2; k1++) {
      for (int k2 = k1+1; k2 < s-1; k2++) {
        for (int k3 = k2+1; k3 < s; k3++) {
          if (lJ[k1-i1].type != 0 && J(k1, k2).type != 0 && J(k2+1, k3).type != 0 && J(k3+1, s).type != 0) {
            ans += V5(lJ[k1-i1], J(k1, k2), J(k2+1, k3), J(k3+1, s));
          }
        }
      }
    }

    int ms = s - i1 + 1;
    lJmom[ms] = lJmom[ms-1] + Jmom(s, s);

    if (s != I2) {
      const complex<T> mass = getLoopFlavourMass(ans.flav);
      ans = InvProp(ans, lJmom[ms], mass);
    }

    lJ[ms] = ans;
  }

  return lJ[m+1];
}

template <typename T>
complex<T> Current<T>::Leval(const ComplexParticle& L1, int i1, int i2, const ComplexParticle& L2)
{
  // attempt to get correct sign on cut-propagator
  // this should be looked at more carefully
  complex<T> propfactor = complex<T>(1.);

  if (isScalar(L2.idx())) {
    propfactor *= -1.;
  } else if (isFermion(L2.idx())) {
    propfactor *= SIGNqqg;
  }

  const complex<T> tree = propfactor * i_ * Contract(L2.getPol(), Ceval(L1, L1.getPol(), i1, i2));
  return tree;
}

template <typename T>
complex<T> Current<T>::Ceval5(const ComplexParticle& L0, const ComplexParticle& L1,
                              const ComplexParticle& L2, const ComplexParticle& L3,
                              const ComplexParticle& L4,
                              int i0, int i1, int i2, int i3, int i4, int n)
{
  const ComplexParticle mL0 = -L0;
  const ComplexParticle mL1 = -L1;
  const ComplexParticle mL2 = -L2;
  const ComplexParticle mL3 = -L3;
  const ComplexParticle mL4 = -L4;
  const SubCurrent<T> c0 = Ceval(mL0, mL0.getPol(), i0, i1-1);
  const SubCurrent<T> c0p = PolSum(c0, mL1.getMom(), mL1.Mass());
  const SubCurrent<T> c1 = Ceval(mL1, c0p, i1, i2-1);
  const SubCurrent<T> c1p = PolSum(c1, mL2.getMom(), mL2.Mass());
  const SubCurrent<T> c2 = Ceval(mL2, c1p, i2, i3-1);
  const SubCurrent<T> c2p = PolSum(c2, mL3.getMom(), mL3.Mass());
  const SubCurrent<T> c3 = Ceval(mL3, c2p, i3, i4-1);
  const SubCurrent<T> c3p = PolSum(c3, mL4.getMom(), mL4.Mass());
  const SubCurrent<T> c4 = Ceval(mL4, c3p, i4, (i0-1+n)%n);
  if (isDimFermion(L0.idx())) {
    return -i_*Contract(L0.getPol(), c4);
  } else {
    return i_*Contract(L0.getPol(), c4);
  }
}

template <typename T>
complex<T> Current<T>::Ceval4(const ComplexParticle& L0, const ComplexParticle& L1,
                              const ComplexParticle& L2, const ComplexParticle& L3,
                              int i0, int i1, int i2, int i3, int n)
{
  const ComplexParticle mL0 = -L0;
  const ComplexParticle mL1 = -L1;
  const ComplexParticle mL2 = -L2;
  const ComplexParticle mL3 = -L3;
  const SubCurrent<T> c0 = Ceval(mL0, mL0.getPol(), i0, i1-1);
  const SubCurrent<T> c0p = PolSum(c0, mL1.getMom(), mL1.Mass());
  const SubCurrent<T> c1 = Ceval(mL1, c0p, i1, i2-1);
  const SubCurrent<T> c1p = PolSum(c1, mL2.getMom(), mL2.Mass());
  const SubCurrent<T> c2 = Ceval(mL2, c1p, i2, i3-1);
  const SubCurrent<T> c2p = PolSum(c2, mL3.getMom(), mL3.Mass());
  const SubCurrent<T> c3 = Ceval(mL3, c2p, i3, (i0-1+n)%n);
  if (isDimFermion(L0.idx())) {
    return -i_*Contract(L0.getPol(), c3);
  } else {
    return i_*Contract(L0.getPol(), c3);
  }
}

template <typename T>
complex<T> Current<T>::Ceval3(const ComplexParticle& L0, const ComplexParticle& L1,
                              const ComplexParticle& L2, int i0, int i1, int i2, int n)
{
  const ComplexParticle mL0 = -L0;
  const ComplexParticle mL1 = -L1;
  const ComplexParticle mL2 = -L2;
  const SubCurrent<T> c0 = Ceval(mL0, mL0.getPol(), i0, i1-1);
  const SubCurrent<T> c0p = PolSum(c0, mL1.getMom(), mL1.Mass());
  const SubCurrent<T> c1 = Ceval(mL1, c0p, i1, i2-1);
  const SubCurrent<T> c1p = PolSum(c1, mL2.getMom(), mL2.Mass());
  const SubCurrent<T> c2 = Ceval(mL2, c1p, i2, (i0-1+n)%n);

  if (isDimFermion(L0.idx())) {
    return -i_*Contract(L0.getPol(), c2);
  } else {
    return i_*Contract(L0.getPol(), c2);
  }
}

template <typename T>
complex<T> Current<T>::Ceval2(const ComplexParticle& L0, const ComplexParticle& L1,
                              int i0, int i1, int n)
{
  const ComplexParticle mL0 = -L0;
  const ComplexParticle mL1 = -L1;
  const SubCurrent<T> c0 = Ceval(mL0, mL0.getPol(), i0, i1-1);
  const SubCurrent<T> c0p = PolSum(c0, mL1.getMom(), mL1.Mass());
  const SubCurrent<T> c1 = Ceval(mL1, c0p, i1, (i0-1+n)%n);
  if (isDimFermion(L0.idx())) {
    return -i_*Contract(L0.getPol(), c1);
  } else {
    return i_*Contract(L0.getPol(), c1);
  }
}

template <typename T>
complex<T> Current<T>::Peval5(const ComplexParticle& L0, const ComplexParticle& L1,
                              const ComplexParticle& L2, const ComplexParticle& L3,
                              const ComplexParticle& L4,
                              int i0, int i1, int i2, int i3, int i4, int n)
{
  const int NN = 5;
  ComplexParticle LL[NN] = {L0, L1, L2, L3, L4};
  ComplexParticle mLL[NN] = {-L0, -L1, -L2, -L3, -L4};
  const int states[NN] = {L0.spinStates(), L1.spinStates(),
                          L2.spinStates(), L3.spinStates(),
                          L4.spinStates()};
  const int idx[NN+1] = {i0-1, i1-1, i2-1, i3-1, i4-1, (i0-1+n)%n};

  complex<T> cc[NN][2][2] = {};
  for (int i=0; i<NN; i++) {
    for (int mh=0; mh<states[i]; mh++) {
      const int rmh = states[i]-1-mh;
      const SubCurrent<T> cur = Ceval(mLL[i], mLL[i].getPol(mh), idx[i]+1, idx[i+1]);
      const int j = (i+1)%NN;
      for (int h=0; h<states[j]; h++) {
        const complex<T> factor = isDimFermion(LL[j].idx()) ? -i_ : i_;
        cc[i][rmh][h] = factor*Contract(LL[j].getPol(h), cur);
      }
    }
  }

  complex<T> prod = T(0.);
  for (int a0=0; a0<L0.spinStates(); a0++) {
  for (int a1=0; a1<L1.spinStates(); a1++) {
  for (int a2=0; a2<L2.spinStates(); a2++) {
  for (int a3=0; a3<L3.spinStates(); a3++) {
  for (int a4=0; a4<L4.spinStates(); a4++) {
    prod += cc[0][a0][a1]*cc[1][a1][a2]*cc[2][a2][a3]*cc[3][a3][a4]*cc[4][a4][a0];
  } } } } }
  return prod;
}

template <typename T>
complex<T> Current<T>::Peval4(const ComplexParticle& L0, const ComplexParticle& L1,
                              const ComplexParticle& L2, const ComplexParticle& L3,
                              int i0, int i1, int i2, int i3, int n)
{
  const int NN = 4;
  ComplexParticle LL[NN] = {L0, L1, L2, L3};
  ComplexParticle mLL[NN] = {-L0, -L1, -L2, -L3};
  const int states[NN] = {L0.spinStates(), L1.spinStates(),
                          L2.spinStates(), L3.spinStates()};
  const int idx[NN+1] = {i0-1, i1-1, i2-1, i3-1, (i0-1+n)%n};

  complex<T> cc[NN][2][2] = {};
  for (int i=0; i<NN; i++) {
    for (int mh=0; mh<states[i]; mh++) {
      const int rmh = states[i]-1-mh;
      const SubCurrent<T> cur = Ceval(mLL[i], mLL[i].getPol(mh), idx[i]+1, idx[i+1]);
      const int j = (i+1)%NN;
      for (int h=0; h<states[j]; h++) {
        const complex<T> factor = isDimFermion(LL[j].idx()) ? -i_ : i_;
        cc[i][rmh][h] = factor*Contract(LL[j].getPol(h), cur);
      }
    }
  }

  complex<T> prod = T(0.);
  for (int a0=0; a0<L0.spinStates(); a0++) {
  for (int a1=0; a1<L1.spinStates(); a1++) {
  for (int a2=0; a2<L2.spinStates(); a2++) {
  for (int a3=0; a3<L3.spinStates(); a3++) {
    prod += cc[0][a0][a1]*cc[1][a1][a2]*cc[2][a2][a3]*cc[3][a3][a0];
  } } } }
  return prod;
}

template <typename T>
complex<T> Current<T>::Peval3(const ComplexParticle& L0, const ComplexParticle& L1,
                              const ComplexParticle& L2,
                              int i0, int i1, int i2, int n)
{
  const int NN = 3;
  ComplexParticle LL[NN] = {L0, L1, L2};
  ComplexParticle mLL[NN] = {-L0, -L1, -L2};
  const int states[NN] = {L0.spinStates(), L1.spinStates(),
                          L2.spinStates()};
  const int idx[NN+1] = {i0-1, i1-1, i2-1, (i0-1+n)%n};

  complex<T> cc[NN][2][2] = {};
  for (int i=0; i<NN; i++) {
    for (int mh=0; mh<states[i]; mh++) {
      const int rmh = states[i]-1-mh;
      const SubCurrent<T> cur = Ceval(mLL[i], mLL[i].getPol(mh), idx[i]+1, idx[i+1]);
      const int j = (i+1)%NN;
      for (int h=0; h<states[j]; h++) {
        const complex<T> factor = isDimFermion(LL[j].idx()) ? -i_ : i_;
        cc[i][rmh][h] = factor*Contract(LL[j].getPol(h), cur);
      }
    }
  }

  complex<T> prod = T(0.);
  for (int a0=0; a0<L0.spinStates(); a0++) {
  for (int a1=0; a1<L1.spinStates(); a1++) {
  for (int a2=0; a2<L2.spinStates(); a2++) {
    prod += cc[0][a0][a1]*cc[1][a1][a2]*cc[2][a2][a0];
  } } }
  return prod;
}

template <typename T>
complex<T> Current<T>::Peval2(const ComplexParticle& L0, const ComplexParticle& L1,
                              int i0, int i1, int n)
{
  const int NN = 2;
  ComplexParticle LL[NN] = {L0, L1};
  ComplexParticle mLL[NN] = {-L0, -L1};
  const int states[NN] = {L0.spinStates(), L1.spinStates()};
  const int idx[NN+1] = {i0-1, i1-1, (i0-1+n)%n};

  complex<T> cc[NN][2][2] = {};
  for (int i=0; i<NN; i++) {
    for (int mh=0; mh<states[i]; mh++) {
      const int rmh = states[i]-1-mh;
      const SubCurrent<T> cur = Ceval(mLL[i], mLL[i].getPol(mh), idx[i]+1, idx[i+1]);
      const int j = (i+1)%NN;
      for (int h=0; h<states[j]; h++) {
        const complex<T> factor = isDimFermion(LL[j].idx()) ? -i_ : i_;
        cc[i][rmh][h] = factor*Contract(LL[j].getPol(h), cur);
      }
    }
  }

  complex<T> prod = T(0.);
  for (int a0=0; a0<L0.spinStates(); a0++) {
  for (int a1=0; a1<L1.spinStates(); a1++) {
    prod += cc[0][a0][a1]*cc[1][a1][a0];
  } }
  return prod;
}

#ifdef USE_SD
  template class Current<double>;
#endif
#ifdef USE_DD
  template class Current<dd_real>;
  // converting constructors:
  template Current<dd_real>::Current(std::vector<Flavour<double> > const&);
  template Current<dd_real>::Current(const int, const Flavour<double>*);
  // converting setProcess:
  template void Current<dd_real>::setProcess(std::vector<Flavour<double> > const&);
  template void Current<dd_real>::setProcess(const int, const Flavour<double>*);
#endif
#ifdef USE_QD
  template class Current<qd_real>;
  // converting constructors:
  template Current<qd_real>::Current(std::vector<Flavour<double> > const&);
  template Current<qd_real>::Current(const int, const Flavour<double>*);
  // converting setProcess:
  template void Current<qd_real>::setProcess(std::vector<Flavour<double> > const&);
  template void Current<qd_real>::setProcess(const int, const Flavour<double>*);
#endif
#ifdef USE_VC
  template class Current<Vc::double_v>;
  // converting constructors:
  template Current<Vc::double_v>::Current(std::vector<Flavour<double> > const&);
  template Current<Vc::double_v>::Current(const int, const Flavour<double>*);
  // converting setProcess:
  template void Current<Vc::double_v>::setProcess(std::vector<Flavour<double> > const&);
  template void Current<Vc::double_v>::setProcess(const int, const Flavour<double>*);
#endif
