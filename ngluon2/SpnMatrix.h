/*
* ngluon2/SpnMatrix.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef NGLUON2_SPNMATRIX_H
#define NGLUON2_SPNMATRIX_H

#include "SubCurrent.h"

template <typename T>
class SpnMatrix
{
  public:
    typedef std::complex<T> complex;
    typedef T real;

    SpnMatrix() {}

    SpnMatrix(const MOM<real>& p) {
      pp[8] = p.x0 - p.x2;
      pp[12] = -p.x3 + i_ * p.x1;
      pp[9] = -p.x3 - i_ * p.x1;
      pp[13] = p.x0 + p.x2;
      pp[2] = p.x0 + p.x2;
      pp[6] = p.x3 - i_ * p.x1;
      pp[3] = p.x3 + i_ * p.x1;
      pp[7] = p.x0 - p.x2;
    }

    SpnMatrix(const MOM<complex>& p) {
      pp[8] = p.x0 - p.x2;
      pp[12] = -p.x3 + i_ * p.x1;
      pp[9] = -p.x3 - i_ * p.x1;
      pp[13] = p.x0 + p.x2;
      pp[2] = p.x0 + p.x2;
      pp[6] = p.x3 - i_ * p.x1;
      pp[3] = p.x3 + i_ * p.x1;
      pp[7] = p.x0 - p.x2;
    }

    SpnMatrix(const real& x) {
      pp[0] = x;
      pp[5] = x;
      pp[10] = x;
      pp[15] = x;
    }

    SpnMatrix(const complex& x) {
      pp[0] = x;
      pp[5] = x;
      pp[10] = x;
      pp[15] = x;
    }

    SpnMatrix(const real& x1, const real& x2,
              const real& x3, const real& x4) {
      pp[0] = x1;
      pp[5] = x2;
      pp[10] = x3;
      pp[15] = x4;
    }

    SpnMatrix(const SubCurrent<T>& s1, const SubCurrent<T>& s2) {
      for (int i1 = 0; i1 < 4; i1++) {
        for (int i2 = 0; i2 < 4; i2++) {
          (*this)(i1, i2) = s1.data(i1) * s2.data(i2);
        }
      }
    }

    complex trace() const {
      return pp[0] + pp[5] + pp[10] + pp[15];
    }

    const complex& data(int i) const {
      return pp[i];
    }

    complex& operator() (int i1, int i2) {
      return pp[i1 + 4*i2];
    }

    const complex& operator() (int i1, int i2) const {
      return pp[i1 + 4*i2];
    }

    SpnMatrix& operator+= (const SpnMatrix& m1) {
      for (int i = 0; i < 16; i++) {
        pp[i] += m1.pp[i];
      }
      return *this;
    }

    SpnMatrix& operator-= (const SpnMatrix& m1) {
      for (int i = 0; i < 16; i++) {
        pp[i] -= m1.pp[i];
      }
      return *this;
    }

    template <typename U>
    SpnMatrix& operator*= (const U& x) {
      for (int i = 0; i < 16; i++) {
        pp[i] *= x;
      }
      return *this;
    }

    SpnMatrix operator- () const {
      SpnMatrix tmp;
      for (int i = 0; i < 16; i++) {
        tmp.pp[i] = -pp[i];
      }
      return tmp;
    }

    template <typename TT>
    friend std::ostream& operator<< (std::ostream& stream, const SpnMatrix<TT>& sm);

  private:
    complex pp[16];
};

template <typename T>
std::ostream& operator<< (std::ostream& stream, const SpnMatrix<T>& sm)
{
  stream << "spinor matrix" << std::endl;
  stream << sm(0, 0) << "," << sm(0, 1) << "," << sm(0, 2) << "," << sm(0, 3) << std::endl;
  stream << sm(1, 0) << "," << sm(1, 1) << "," << sm(1, 2) << "," << sm(1, 3) << std::endl;
  stream << sm(2, 0) << "," << sm(2, 1) << "," << sm(2, 2) << "," << sm(2, 3) << std::endl;
  stream << sm(3, 0) << "," << sm(3, 1) << "," << sm(3, 2) << "," << sm(3, 3) << std::endl;
  return stream;
}

template <typename T>
SpnMatrix<T> operator* (const SpnMatrix<T>& m1, const SpnMatrix<T>& m2)
{
  SpnMatrix<T> ans;

  for (int i1 = 0; i1 < 4; i1++) {
    for (int i2 = 0; i2 < 4; i2++) {
      for (int i3 = 0; i3 < 4; i3++) {
        ans(i1, i2) += m1(i1, i3) * m2(i3, i2);
      }
    }
  }

  return ans;
}

template <typename T, typename U>
SpnMatrix<T> operator* (SpnMatrix<T> m1, const U& x)
{
  m1 *= x;
  return m1;
}

template <typename T, typename U>
SpnMatrix<T> operator* (const U& x, const SpnMatrix<T>& m1)
{
  return m1*x;
}

template <typename T>
SpnMatrix<T> operator+ (SpnMatrix<T> m1, const SpnMatrix<T>& m2)
{
  m1 += m2;
  return m1;
}

template <typename T>
SpnMatrix<T> operator- (SpnMatrix<T> m1, const SpnMatrix<T>& m2)
{
  m1 -= m2;
  return m1;
}

template <typename T>
SpnMatrix<T> ContractMatrix(const SubCurrent<T>& s1, const SubCurrent<T>& s2)
{
  return SpnMatrix<T>(s1, s2);
}

#endif /* NGLUON2_SPNMATRIX_H */
