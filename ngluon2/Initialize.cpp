
#include <iostream>
#include <cstring>
//#include <fenv.h>

#include "version.h"
#include "Initialize.h"
#include "LoopIntegrals.h"

bool Initialize::valid = false;

Initialize::Initialize()
{
  old_cw = 0;
  if (not valid) {
    initNJet();
    initFloat();
    initLoop();
//      feenableexcept(FE_DIVBYZERO|FE_OVERFLOW);
    valid = true;
  }
}

Initialize::~Initialize()
{
  if (valid) {
    uninitFloat();
    uninitLoop();
    valid = false;
  }
}

void Initialize::info(const char* s)
{
  std::cout << "## init: " << s << std::endl;
}

void Initialize::initFloat()
{
#ifdef EXTENDED_PREC
  fpu_fix_start(&old_cw);
  info("Switch floating point unit");
#endif
}

void Initialize::uninitFloat()
{
#ifdef EXTENDED_PREC
  fpu_fix_end(&old_cw);
  info("Restore floating point unit");
#endif
}

void Initialize::initNJet()
{
  std::cout << "#--------------------------------------------------------------------" << std::endl;
  std::cout << "#  NJet -- multi-leg one-loop matrix elements in the Standard Model " << std::endl;
  std::cout << "#  version : " << PACKAGE_VERSION;
#ifdef HAVE_VERSION_H
  if (strstr(PACKAGE_VERSION, "git")) {
    std::cout << " " << GIT_VERSION;
  }
#endif
  std::cout << std::endl;
  std::cout << "#  Authors : Simon Badger, Valery Yundin                             " << std::endl;
  std::cout << "#  Homepage: https://bitbucket.org/njet/njet                         " << std::endl;
  std::cout << "#--------------------------------------------------------------------" << std::endl;
}

void Initialize::initLoop()
{
#ifdef QCDLOOP
  info("FF and QCDLoop are used to calculate the scalar one-loop integrals");
  // QCDLoop takes good enough care of advertising itself in qlinit_
  /*
  info("[1] van Oldenborgh: FF: A Package To Evaluate One Loop Feynman Diagrams");
  info("    Comput.Phys.Commun.66:1-15,1991");
  info("[2] R.Keith Ellis, Giulia Zanderighi,  Scalar one-loop integrals for QCD, ");
  info("    JHEP 0802:002,2008");
  */
  myqlinit();
#else
  info("WARNING: all scalar loop integrals are set to 1.");
  info("Recompile the package with scalar integrals library enabled");
#endif
}

void Initialize::uninitLoop()
{
  myqlexit();
}
