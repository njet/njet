/*
* ngluon2/Current.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef NGLUON2_CURRENT_H
#define NGLUON2_CURRENT_H

#include "Mom.h"
#include "SubCurrent.h"
#include "Particle.h"
#include "Flavour.h"
#include "PMatrix.h"
#include "ModelBase.h"

using std::complex;

template <typename T>
class Current : public ModelBase
{
  public:
    static const T rt2;
    static const T Irt2;

    typedef MOM<T> RealMom;
    typedef MOM<complex<T> > ComplexMom;

    typedef Flavour<T> RealFlavour;
    typedef Flavour<complex<T> > ComplexFlavour;

    typedef typename RealFlavour::FlavourList RealFlavourList;
    typedef typename ComplexFlavour::FlavourList ComplexFlavourList;

    typedef Particle<T> RealParticle;
    typedef typename RealParticle::ParticleList RealParticleList;

    typedef Particle<complex<T> > ComplexParticle;
    typedef typename ComplexParticle::ParticleList ComplexParticleList;

    Current();
    Current(const int n_, const RealFlavour* flavours);
    Current(const RealFlavourList& flavours);

    void setProcess(const int n_, const RealFlavour* flavours);
    void setProcess(const RealFlavourList& flavours);

    // double -> extended conversion constructors
    template <typename U>
    Current(const int n_, const Flavour<U>* otherflavarr);
    template <typename U>
    Current(const std::vector<Flavour<U> >& otherflavours);

    // double -> extended converting setProcess
    template <typename U>
    void setProcess(const int n_, const Flavour<U>* otherflavarr);
    template <typename U>
    void setProcess(const std::vector<Flavour<U> >& otherflavours);

    void setHeavyLoopFlavour(const T mass);

    int Nexternal() const { return legs; }
    void setMomenta(const RealMom* moms);
    void setMomenta(const std::vector<RealMom>& moms);

    void setHelicity(const int* helicity);
    void setHelicity(const std::vector<int>& helicity);

    void setOrder(const int* ord);
    void setOrder(const std::vector<int>& ord);

    void fillCurrents();

    SubCurrent<T> Ceval(const ComplexParticle& L1, const SubCurrent<T>& PL1, int i1, int i2);
    complex<T> Leval(const ComplexParticle& L1, int i1, int i2, const ComplexParticle& L2);

    complex<T> Ceval5(const ComplexParticle& L0, const ComplexParticle& L1,
                      const ComplexParticle& L2, const ComplexParticle& L3,
                      const ComplexParticle& L4,
                      int i0, int i1, int i2, int i3, int i4, int n);
    complex<T> Ceval4(const ComplexParticle& L0, const ComplexParticle& L1,
                      const ComplexParticle& L2, const ComplexParticle& L3,
                      int i0, int i1, int i2, int i3, int n);
    complex<T> Ceval3(const ComplexParticle& L0, const ComplexParticle& L1,
                      const ComplexParticle& L2, int i0, int i1, int i2, int n);
    complex<T> Ceval2(const ComplexParticle& L0, const ComplexParticle& L1,
                      int i0, int i1, int n);

    complex<T> Peval5(const ComplexParticle& L0, const ComplexParticle& L1,
                      const ComplexParticle& L2, const ComplexParticle& L3,
                      const ComplexParticle& L4,
                      int i0, int i1, int i2, int i3, int i4, int n);
    complex<T> Peval4(const ComplexParticle& L0, const ComplexParticle& L1,
                      const ComplexParticle& L2, const ComplexParticle& L3,
                      int i0, int i1, int i2, int i3, int n);
    complex<T> Peval3(const ComplexParticle& L0, const ComplexParticle& L1,
                      const ComplexParticle& L2, int i0, int i1, int i2, int n);
    complex<T> Peval2(const ComplexParticle& L0, const ComplexParticle& L1,
                      int i0, int i1, int n);

    complex<T> eval();

    T getRealFlavourMass(const int flav) const;
    complex<T> getComplexFlavourMass(const int flav) const;
    complex<T> getLoopFlavourMass(const int flav) const;

    void setLoopMass(const complex<T>& mDsq);

    const RealMom& PropMoms(int i, int j) const { return Jmom(i, j); }
    const RealParticleList& Process() const { return process; }
    const RealParticleList& BaseProcess() const { return base_process; }

    const RealFlavour& getLightLoopFlavour() const { return base_process[lightpos]; }
    const RealFlavour& getHeavyLoopFlavour() const { return base_process[heavypos]; }

  private:
    enum VERTICES {
      v3_null = 0,
      v3_GG_G, v3_sG_s, v3_Gs_s, v3_ss_G,
      v3_qQ_G, v3_Qq_G, v3_qDQ_s, v3_QqD_s, v3_QDq_s, v3_qQD_s,
      v3_qG_q, v3_QG_Q, v3_Gq_q, v3_GQ_Q,
      v3_HQ_Q, v3_QH_Q, v3_qH_q, v3_Hq_q,
      v3_QDs_Q, v3_Qs_QD, v3_sQD_Q, v3_sQ_QD,
      v3_qDs_q, v3_qs_qD, v3_sqD_q, v3_sq_qD,
      v3_qV_q, v3_QV_Q, v3_Vq_q, v3_VQ_Q,
      v3_qDV_qD, v3_QDV_QD, v3_VqD_qD, v3_VQD_QD,
      v3_HG_G, v3_GH_G,
    };

    void setFlavourAt(const RealFlavour& flavour, const unsigned int i);
    void initialize(const int n_, const RealFlavour* flavours);
    void reorder();

    SubCurrent<T> getCurrent(int i1, int i2);

    template <typename U1, typename U2>
    SubCurrent<T> InvProp(const SubCurrent<T>& J1, const MOM<U1>& P, const U2 m) const;
    template <typename U1, typename U2>
    SubCurrent<T> PolSum(const SubCurrent<T>& J1, const MOM<U1>& P, const U2 m) const;
    template <typename U1, typename U2>
    SubCurrent<T> V3(const SubCurrent<T>& J1, const SubCurrent<T>& J2, const MOM<U1>& P1, const MOM<U2>& P2);
    template <typename U1, typename U2, typename U3>
    SubCurrent<T> V4(const SubCurrent<T>& J1, const SubCurrent<T>& J2, const SubCurrent<T>& J3,
                     const MOM<U1>& P1, const MOM<U2>& P2, const MOM<U3>& P3);
    SubCurrent<T> V5(const SubCurrent<T>& J1, const SubCurrent<T>& J2,
                     const SubCurrent<T>& J3, const SubCurrent<T>& J4);

    // variables
    int n, legs;
    unsigned int nullpos, lightpos, heavypos;
    bool havehiggs;

    RealParticleList base_process;
    ComplexParticleList loop_process;
    int flavmap[EndFlavour];
    int loopmap[EndFlavour];  // can have only flavmap, since they do not overlap

    RealParticleList process;
    std::vector<int> order;

    bool ordered;
    bool precomputed;

    PMatrix<SubCurrent<T> > J;
    PMatrix<RealMom> Jmom;

    bool init_v3table();
    int V3classify(const int flav1, const int flav2);
    int v3table[2*EndFlavour][2*EndFlavour];

    // arrays used in Leval and Ceval
    std::vector<SubCurrent<T> > lJ;
    std::vector<ComplexMom> lJmom;
};

#endif /* NGLUON2_CURRENT_H */
