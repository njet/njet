/*
* ngluon2/NGluon2.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include "NAmp.h"
#include "NGluon2.h"

template <typename T>
NAmp<T>::NAmp()
 : ngluons()
{
}

template <typename T>
NAmp<T>::~NAmp()
{
  clearNG();
}

template <typename T>
void NAmp<T>::clearNG()
{
  for (unsigned i=0; i<ngluons.size(); i++) {
    if (ngluons[i]) {
      ngalloc.destroy(ngluons[i]);
      ngalloc.deallocate(ngluons[i], 1);
      ngluons[i] = 0;
    }
  }
  ngluons.clear();
}

template <typename T>
void NAmp<T>::initNG(const T scalefactor, unsigned pos)
{
  if (pos >= ngluons.size()) {
    ngluons.resize(pos+1);
  }
  if (ngluons[pos]) {
    ngalloc.destroy(ngluons[pos]);
  } else {
    ngluons[pos] = ngalloc.allocate(1);
  }
  ngalloc.construct(ngluons[pos], NGluon2<T>(scalefactor));
}

template <typename T>
void NAmp<T>::setMuR2(const T rscale)
{
  for (unsigned i=0; i<ngluons.size(); i++) {
    ngluons[i]->setMuR2(rscale);
  }
}

template <typename T>
void NAmp<T>::setProcess(const int n_, const Flavour<T>* flavarr)
{
  for (unsigned i=0; i<ngluons.size(); i++) {
    setProcess(n_, flavarr, i);
  }
}

template <typename T>
void NAmp<T>::setProcess(const std::vector<Flavour<T> >& flavours)
{
  setProcess(flavours.size(), flavours.data());
}

template <typename T>
template <typename U>
void NAmp<T>::setProcess(const int n_, const Flavour<U>* otherflavarr)
{
  for (unsigned i=0; i<ngluons.size(); i++) {
    setProcess(n_, otherflavarr, i);
  }
}

template <typename T>
template <typename U>
void NAmp<T>::setProcess(const std::vector<Flavour<U> >& otherflavours)
{
  setProcess(otherflavours.size(), otherflavours.data());
}

template <typename T>
void NAmp<T>::setProcess(const int n_, const Flavour<T>* flavarr, const int pos)
{
  ngluons[pos]->setProcess(n_, flavarr);
}

template <typename T>
void NAmp<T>::setProcess(const std::vector<Flavour<T> >& flavours, const int pos)
{
  setProcess(flavours.size(), flavours.data(), pos);
}

template <typename T>
template <typename U>
void NAmp<T>::setProcess(const int n_, const Flavour<U>* otherflavarr, const int pos)
{
  ngluons[pos]->setProcess(n_, otherflavarr);
}

template <typename T>
template <typename U>
void NAmp<T>::setProcess(const std::vector<Flavour<U> >& otherflavours, const int pos)
{
  setProcess(otherflavours.size(), otherflavours.data(), pos);
}

template <typename T>
void NAmp<T>::setMomenta(const RealMom* moms)
{
  for (unsigned i=0; i<ngluons.size(); i++) {
    ngluons[i]->setMomenta(moms);
  }
}

template <typename T>
void NAmp<T>::setMomenta(const std::vector<RealMom>& moms)
{
  setMomenta(moms.data());
}

template <typename T>
template <typename U>
void NAmp<T>::setMomenta(const MOM<U>* othermoms)
{
  for (unsigned i=0; i<ngluons.size(); i++) {
    ngluons[i]->setMomenta(othermoms);
  }
}

template <typename T>
template <typename U>
void NAmp<T>::setMomenta(const std::vector<MOM<U> >& othermoms)
{
  setMomenta(othermoms.data());
}

template <typename T>
void NAmp<T>::setHelicity(const int* helicity)
{
  for (unsigned i=0; i<ngluons.size(); i++) {
    ngluons[i]->setHelicity(helicity);
  }
}

template <typename T>
void NAmp<T>::setHelicity(const std::vector<int>& helicity)
{
  setHelicity(helicity.data());
}

#ifdef USE_SD
  template class NAmp<double>;
#endif
#ifdef USE_DD
  template class NAmp<dd_real>;
  template void NAmp<dd_real>::setProcess(std::vector<Flavour<double> > const&);
  template void NAmp<dd_real>::setProcess(const int, const Flavour<double>*);
  template void NAmp<dd_real>::setProcess(std::vector<Flavour<double> > const&, const int);
  template void NAmp<dd_real>::setProcess(const int, const Flavour<double>*, const int);
#endif
#ifdef USE_QD
  template class NAmp<qd_real>;
  template void NAmp<qd_real>::setProcess(std::vector<Flavour<double> > const&);
  template void NAmp<qd_real>::setProcess(const int, const Flavour<double>*);
  template void NAmp<qd_real>::setProcess(std::vector<Flavour<double> > const&, const int);
  template void NAmp<qd_real>::setProcess(const int, const Flavour<double>*, const int);
#endif
#ifdef USE_VC
  template class NAmp<Vc::double_v>;
  template void NAmp<Vc::double_v>::setProcess(std::vector<Flavour<double> > const&);
  template void NAmp<Vc::double_v>::setProcess(const int, const Flavour<double>*);
  template void NAmp<Vc::double_v>::setProcess(std::vector<Flavour<double> > const&, const int);
  template void NAmp<Vc::double_v>::setProcess(const int, const Flavour<double>*, const int);
  template void NAmp<Vc::double_v>::setMomenta(const MOM<double>*);
  template void NAmp<Vc::double_v>::setMomenta(const std::vector<MOM<double> >&);
#endif
