/*
* ngluon2/Current-vertices.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

// fermion vertex conventions
#define SIGNqqg 1.

// higgs effective vertices prefactors
static const std::complex<double> HGG = 0.5;
static const std::complex<double> HGGG = 1./3.;
static const std::complex<double> HGGGG = 0.25;

template <typename T>
template <typename U1, typename U2>
SubCurrent<T> Current<T>::PolSum(const SubCurrent<T>& J1, const MOM<U1>& P, const U2 m) const
{
  switch (J1.type) {
    case 3: {
      return J1;
    }
    case 1: {
      SubCurrent<T> ans = -(ContractSlash(SubCurrent<T>(P), J1) - m*J1);  // TODO check
      ans.flav = J1.flav;
      return ans;
    }
    case -1: {
      SubCurrent<T> ans = (ContractSlash(SubCurrent<T>(P), J1) + m*J1);
      ans.flav = J1.flav;
      return ans;
    }
    case 2: {
      return J1;
    }
    case 0: {
      return J1;
    }
    default:
      NJET_ERROR("unknown propagator")
  }
}

template <typename T>
int Current<T>::V3classify(const int flav1, const int flav2)
{
  // gluon-gluon and gluon-scalar vertices
  if (isGluon(flav1) and isGluon(flav2)) {
    return v3_GG_G;
  }
  if (isScalar(flav1) and isGluon(flav2)) {
    return v3_sG_s;
  }
  if (isGluon(flav1) and isScalar(flav2)) {
    return v3_Gs_s;
  }
  if (isScalar(flav1) and isScalar(flav2)) {
    return v3_ss_G;
  }

  // fermion-antifermion vertices
  if (isQuark(flav1) and isAntiQuark(flav2)) {
    if (isQuark(flav1) and flav1 == -flav2) {  // NOTE same vertex for 4-dim and d-dim quarks
      return v3_qQ_G;
    }
    if (is4Quark(flav1) and DimShift(flav1) == -flav2) {
      return v3_qQD_s;
    }
    if (isDimQuark(flav1) and -flav1 == DimShift(flav2)) {
      return v3_qDQ_s;
    }
    return v3_null;
  }

  // antifermion-fermion vertices
  if (isAntiQuark(flav1) and isQuark(flav2)) {
    if (isAntiQuark(flav1) and flav1 == -flav2) {  // NOTE same vertex for 4-dim and d-dim quarks
      return v3_Qq_G;
    }
    if (is4AntiQuark(flav1) and DimShift(flav1) == -flav2) {
      return v3_QqD_s;
    }
    if (isDimAntiQuark(flav1) and -flav1 == DimShift(flav2)) {
      return v3_QDq_s;
    }
    return v3_null;
  }

  // fermion-gluon vertices
  // NOTE same vertices for 4-dim and d-dim quarks
  if (isFermion(flav1) and isGluon(flav2)) {
    if (isQuark(flav1)) {
      return v3_qG_q;
    }
    if (isAntiQuark(flav1)) {
      return v3_QG_Q;
    }
    return v3_null;
  }

  // gluon-fermion vertices
  // NOTE same vertices for 4-dim and d-dim quarks
  if (isGluon(flav1) and isFermion(flav2)) {
    if (isQuark(flav2)) {
      return v3_Gq_q;
    }
    if (isAntiQuark(flav2)) {
      return v3_GQ_Q;
    }
    return v3_null;
  }

  // fermion-scalar vertices
  if (isFermion(flav1) and isScalar(flav2)) {
    if (isQuark(flav1)) {
      return v3_qs_qD;
    }
    if (isAntiQuark(flav1)) {
      return v3_Qs_QD;
    }
    if (isDimQuark(flav1)) {
      return v3_qDs_q;
    }
    if (isDimAntiQuark(flav1)) {
      return v3_QDs_Q;
    }
    return v3_null;
  }

  // scalar-fermion vertices
  if (isScalar(flav1) and isFermion(flav2)) {
    if (isQuark(flav2)) {
      return v3_sq_qD;
    }
    if (isAntiQuark(flav2)) {
      return v3_sQ_QD;
    }
    if (isDimQuark(flav2)) {
      return v3_sqD_q;
    }
    if (isDimAntiQuark(flav2)) {
      return v3_sQD_Q;
    }
    return v3_null;
  }

  // fermion-vector vertices
  if (isFermion(flav1) and (isMassiveVector(flav2) or isLightVector(flav2))) {
    const RealFlavour& v = base_process[flavmap[flav2]];
    if (is4Quark(flav1) and v.Vq() == flav1) {
      return v3_qV_q;
    }
    if (isAntiQuark(flav1) and v.VQ() == flav1) {
      return v3_QV_Q;
    }
    if (isDimQuark(flav1) and DimShift(v.Vq()) == flav1) {
      return v3_qDV_qD;
    }
    if (isDimAntiQuark(flav1) and DimShift(v.VQ()) == flav1) {
      return v3_QDV_QD;
    }
    return v3_null;
  }

  // vector-fermion vertices
  if ((isMassiveVector(flav1) or isLightVector(flav1)) and isFermion(flav2)) {
    const RealFlavour& v = base_process[flavmap[flav1]];
    if (is4Quark(flav2) and v.Vq() == flav2) {
      return v3_Vq_q;
    }
    if (isAntiQuark(flav2) and v.VQ() == flav2) {
      return v3_VQ_Q;
    }
    if (isDimQuark(flav2) and DimShift(v.Vq()) == flav2) {
      return v3_VqD_qD;
    }
    if (isDimAntiQuark(flav2) and DimShift(v.VQ()) == flav2) {
      return v3_VQD_QD;
    }
    return v3_null;
  }

  // gluon-gluon-eff.higgs vertices
  if (isHiggs(flav1) and isGluon(flav2)) {
    return v3_HG_G;
  }
  if (isGluon(flav1) and isHiggs(flav2)) {
    return v3_GH_G;
  }

  return v3_null;
}

template <typename T>
bool Current<T>::init_v3table()
{
  for (int i = -EndFlavour; i < EndFlavour; i++) {
    for (int j = -EndFlavour; j < EndFlavour; j++) {
      const int type = V3classify(i, j);
      v3table[i + EndFlavour][j + EndFlavour] = type;
    }
  }
  return true;
}

template <typename T>
template <typename U1, typename U2>
SubCurrent<T> Current<T>::V3(const SubCurrent<T>& J1, const SubCurrent<T>& J2,
                             const MOM<U1>& P1, const MOM<U2>& P2)
{
  static const T rt2 = constant_traits<T>::rt2();
  static const T Irt2 = 1./rt2;

  const int flav1 = J1.flav;
  const int flav2 = J2.flav;
  const int type = v3table[flav1 + EndFlavour][flav2 + EndFlavour];
  switch (type) {
    case v3_null: {
      return SubCurrent<T>();
    }
    // gluon-gluon and gluon-scalar vertices
    case v3_GG_G: {
      SubCurrent<T> ans = 2.*Contract(P2, J1)*J2 - 2.*Contract(P1, J2)*J1 + Contract(J1, J2)*SubCurrent<T>(P1-P2);
      ans.flav = Gluon;
      return Irt2*ans;
    }
    case v3_sG_s: {
      SubCurrent<T> ans = Contract(P1, J2)*J1;
      ans.flav = flav1;
      return -rt2*ans;
    }
    case v3_Gs_s: {
      SubCurrent<T> ans = Contract(P2, J1)*J2;
      ans.flav = flav2;
      return rt2*ans;
    }
    case v3_ss_G: {
      SubCurrent<T> ans = SubCurrent<T>(P1-P2)*Contract(J1, J2);
      ans.flav = Gluon;
      return Irt2*ans;
    }
    // quark-quark -> gluon vertices
    case v3_qQ_G: {
      SubCurrent<T> ans = ContractMu(J1, J2);
      ans.flav = Gluon;
      return -SIGNqqg*Irt2*ans;
    }
    case v3_Qq_G: {
      SubCurrent<T> ans = ContractMu(J1, J2);
      ans.flav = Gluon;
      return SIGNqqg*Irt2*ans;
    }
    // dim-quark-quark -> scalar vertices
    case v3_qDQ_s: {
      SubCurrent<T> ans = Contract(J1, J2)*SubCurrent<T>::USC;
      ans.flav = Scalar;
      return -i_*Irt2*ans;
    }
    case v3_QqD_s: {
      SubCurrent<T> ans = Contract(J1, J2)*SubCurrent<T>::USC;
      ans.flav = Scalar;
      return i_*Irt2*ans;
    }
    case v3_QDq_s: {
      SubCurrent<T> ans = Contract(J1, J2)*SubCurrent<T>::USC;
      ans.flav = Scalar;
      return i_*Irt2*ans;
    }
    case v3_qQD_s: {
      SubCurrent<T> ans = Contract(J1, J2)*SubCurrent<T>::USC;
      ans.flav = Scalar;
      return -i_*Irt2*ans;
    }
    // quark-gluon -> quark vertices
    case v3_qG_q: {
      SubCurrent<T> ans = ContractSlash(J1, J2);
      ans.flav = flav1;
      return SIGNqqg*Irt2*ans;
    }
    case v3_QG_Q: {
      SubCurrent<T> ans = ContractSlash(J1, J2);
      ans.flav = flav1;
      return -SIGNqqg*Irt2*ans;
    }
    case v3_Gq_q: {
      SubCurrent<T> ans = ContractSlash(J1, J2);
      ans.flav = flav2;
      return -SIGNqqg*Irt2*ans;
    }
    case v3_GQ_Q: {
      SubCurrent<T> ans = ContractSlash(J1, J2);
      ans.flav = flav2;
      return SIGNqqg*Irt2*ans;
    }
    // higgs-quark vertices
    case v3_HQ_Q: {
      SubCurrent<T> ans = Contract(SubCurrent<T>::USC, J1)*J2;
      ans.flav = flav2;
      return Irt2*ans;
    }
    case v3_QH_Q: {
      SubCurrent<T> ans = Contract(SubCurrent<T>::USC, J2)*J1;
      ans.flav = flav1;
      return -Irt2*ans;
    }
    case v3_qH_q: {
      SubCurrent<T> ans = Contract(SubCurrent<T>::USC, J2)*J1;
      ans.flav = flav1;
      return Irt2*ans;
    }
    case v3_Hq_q: {
      SubCurrent<T> ans = Contract(SubCurrent<T>::USC, J1)*J2;
      ans.flav = flav2;
      return -Irt2*ans;
    }
    // dim-quark-scalar -> quark vertices
    case v3_QDs_Q: {
      SubCurrent<T> ans = Contract(SubCurrent<T>::USC, J2)*J1;
      ans.flav = DimAntiQuarkShift(flav1);
      return -i_*Irt2*ans;
    }
    case v3_Qs_QD: {
      SubCurrent<T> ans = Contract(SubCurrent<T>::USC,J2)*J1;
      ans.flav = AntiQuarkShift(flav1);
      return -i_*Irt2*ans;
    }
    case v3_sQD_Q: {
      SubCurrent<T> ans = Contract(SubCurrent<T>::USC, J1)*J2;
      ans.flav = DimAntiQuarkShift(flav2);
      return i_*Irt2*ans;
    }
    case v3_sQ_QD: {
      SubCurrent<T> ans = Contract(SubCurrent<T>::USC, J1)*J2;
      ans.flav = AntiQuarkShift(flav2);
      return i_*Irt2*ans;
    }
    case v3_qDs_q: {
      SubCurrent<T> ans = Contract(SubCurrent<T>::USC, J2)*J1;
      ans.flav = DimQuarkShift(flav1);
      return i_*Irt2*ans;
    }
    case v3_qs_qD: {
      SubCurrent<T> ans = Contract(SubCurrent<T>::USC, J2)*J1;
      ans.flav = QuarkShift(flav1);
      return i_*Irt2*ans;
    }
    case v3_sqD_q: {
      SubCurrent<T> ans = Contract(SubCurrent<T>::USC, J1)*J2;
      ans.flav = DimQuarkShift(flav2);
      return -i_*Irt2*ans;
    }
    case v3_sq_qD: {
      SubCurrent<T> ans = Contract(SubCurrent<T>::USC, J1)*J2;
      ans.flav = QuarkShift(flav2);
      return -i_*Irt2*ans;
    }
    // massive vector couplings
    case v3_qV_q: {
      const RealFlavour& v = base_process[flavmap[flav2]];
      SubCurrent<T> ans = ContractSlash(J1, J2);
      ans.flav = -v.VQ();
      return v.Coupling()*Irt2*ans;
    }
    case v3_QV_Q: {
      const RealFlavour& v = base_process[flavmap[flav2]];
      SubCurrent<T> ans = ContractSlash(J1, J2);
      ans.flav = -v.Vq();
      return v.Coupling()*Irt2*ans;
    }
    case v3_Vq_q: {
      const RealFlavour& v = base_process[flavmap[flav1]];
      SubCurrent<T> ans = ContractSlash(J1, J2);
      ans.flav = -v.VQ();
      return v.Coupling()*Irt2*ans;
    }
    case v3_VQ_Q: {
      const RealFlavour& v = base_process[flavmap[flav1]];
      SubCurrent<T> ans = ContractSlash(J1, J2);
      ans.flav = -v.Vq();
      return v.Coupling()*Irt2*ans;
    }
    // massive vector couplings to D-dim fermions
    case v3_qDV_qD: {
      const RealFlavour& v = base_process[flavmap[flav2]];
      SubCurrent<T> ans = ContractSlash(J1, J2);
      ans.flav = QuarkShift(-v.VQ());
      return v.Coupling()*Irt2*ans;
    }
    case v3_QDV_QD: {
      const RealFlavour& v = base_process[flavmap[flav2]];
      SubCurrent<T> ans = ContractSlash(J1, J2);
      ans.flav = AntiQuarkShift(-v.Vq());
      return v.Coupling()*Irt2*ans;
    }
    case v3_VqD_qD: {
      const RealFlavour& v = base_process[flavmap[flav1]];
      SubCurrent<T> ans = ContractSlash(J1, J2);
      ans.flav = QuarkShift(-v.VQ());
      return v.Coupling()*Irt2*ans;
    }
    case v3_VQD_QD: {
      const RealFlavour& v = base_process[flavmap[flav1]];
      SubCurrent<T> ans = ContractSlash(J1, J2);
      ans.flav = AntiQuarkShift(-v.Vq());
      return v.Coupling()*Irt2*ans;
    }
    case v3_HG_G: {
      const SubCurrent<T> P12 = SubCurrent<T>(P1+P2);
      SubCurrent<T> ans = Contract(P12, J2)*SubCurrent<T>(P2) - Contract(P12, P2)*J2;
      ans.flav = Gluon;
      return HGG*ans;
    }
    case v3_GH_G: {
      const SubCurrent<T> P12 = SubCurrent<T>(P1+P2);
      SubCurrent<T> ans = Contract(P12, J1)*SubCurrent<T>(P1) - Contract(P12, P1)*J1;
      ans.flav = Gluon;
      return HGG*ans;
    }
    default: {
      NJET_ERROR("unknown 3-vertex type " << type << " of " << flav1 << " and " << flav2)
    }
  }
}

template <typename T>
template <typename U1, typename U2, typename U3>
SubCurrent<T> Current<T>::V4(const SubCurrent<T>& J1, const SubCurrent<T>& J2, const SubCurrent<T>& J3,
                             const MOM<U1>& P1, const MOM<U2>& P2, const MOM<U3>& P3)
{
  static const T rt2 = constant_traits<T>::rt2();
  static const T Irt2 = 1./rt2;

  const int flav1 = J1.flav;
  const int flav2 = J2.flav;
  const int flav3 = J3.flav;
  if (isGluon(flav1)) {
    if (isGluon(flav2)) {
      if (isGluon(flav3)) {
        // G G G -> G
        SubCurrent<T> ans = 2.*Contract(J1, J3)*J2 - Contract(J1, J2)*J3 - Contract(J2, J3)*J1;
        ans.flav = flav1;
        return 0.5*ans;
      }
      else if (isScalar(flav3)) {
        // G G S -> S
        SubCurrent<T> ans = Contract(J1, J2)*J3;
        ans.flav = flav3;
        return -0.5*ans;
      }
      else if (isHiggs(flav3)) {
        // G G H -> G
        SubCurrent<T> ans = Contract(2.*P2+P3, J1)*J2 - Contract(2.*P1+P3, J2)*J1 + Contract(J1, J2)*SubCurrent<T>(P1-P2);
        ans.flav = Gluon;
        return Irt2*HGGG*ans;
      }
    }
    else if (isScalar(flav2)) {
      if (isGluon(flav3)) {
        // G S G -> S
        SubCurrent<T> ans = Contract(J3, J1)*J2;
        ans.flav = flav2;
        return ans;
      }
      else if (isScalar(flav3)) {
        // G S S -> G
        SubCurrent<T> ans = J1*Contract(J2, J3);
        ans.flav = flav1;
        return -0.5*ans;
      }
    }
    else if (isHiggs(flav2) and isGluon(flav3)) {
      // G H G -> G
      SubCurrent<T> ans = Contract(2.*P3+P2, J1)*J3 - Contract(2.*P1+P2, J3)*J1 + Contract(J1, J3)*SubCurrent<T>(P1-P3);
      ans.flav = Gluon;
      return Irt2*HGGG*ans;
    }
  }
  else if (isScalar(flav1)) {
    if (isGluon(flav2)) {
      if (isGluon(flav3)) {
        // S G G -> S
        SubCurrent<T> ans = Contract(J2, J3)*J1;
        ans.flav = J1.flav;
        return -0.5*ans;
      }
      else if (isScalar(flav3)) {
        // S G S -> G
        SubCurrent<T> ans = J2*Contract(J3, J1);
        ans.flav = flav2;
        return ans;
      }
    }
    else if (isScalar(flav2)) {
      if (isGluon(flav3)) {
        // S S G -> G
        SubCurrent<T> ans = J3*Contract(J1, J2);
        ans.flav = flav3;
        return -0.5*ans;
      }
      else if (isScalar(flav3)) {
        // S S S -> 0
        return SubCurrent<T>();
      }
    }
  }
  else if (isHiggs(flav1) and isGluon(flav2) and isGluon(flav3)) {
    // H G G -> G
    SubCurrent<T> ans = Contract(2.*P3+P1, J2)*J3 - Contract(2.*P2+P1, J3)*J2 + Contract(J2, J3)*SubCurrent<T>(P2-P3);
    ans.flav = Gluon;
    return Irt2*HGGG*ans;
  }

  return SubCurrent<T>();
}

template <typename T>
SubCurrent<T> Current<T>::V5(const SubCurrent<T>& J1, const SubCurrent<T>& J2,
                             const SubCurrent<T>& J3, const SubCurrent<T>& J4)
{
  const int flav1 = J1.flav;
  const int flav2 = J2.flav;
  const int flav3 = J3.flav;
  const int flav4 = J4.flav;
  if (isHiggs(flav4) and isGluon(flav1) and isGluon(flav2) and isGluon(flav3)) {
    // G G G H -> G
    SubCurrent<T> ans = 2.*Contract(J1, J3)*J2 - Contract(J1, J2)*J3 - Contract(J2, J3)*J1;
    ans.flav = Gluon;
    return 0.5*HGGGG*ans;
  }
  if (isHiggs(flav3) and isGluon(flav1) and isGluon(flav2) and isGluon(flav4)) {
    // G G H G -> G
    SubCurrent<T> ans = 2.*Contract(J1, J4)*J2 - Contract(J1, J2)*J4 - Contract(J2, J4)*J1;
    ans.flav = Gluon;
    return 0.5*HGGGG*ans;
  }
  if (isHiggs(flav2) and isGluon(flav1) and isGluon(flav3) and isGluon(flav4)) {
    // G H G G -> G
    SubCurrent<T> ans = 2.*Contract(J1, J4)*J3 - Contract(J1, J3)*J4 - Contract(J3, J4)*J1;
    ans.flav = Gluon;
    return 0.5*HGGGG*ans;
  }
  if (isHiggs(flav1) and isGluon(flav2) and isGluon(flav3) and isGluon(flav4)) {
    // H G G G -> G
    SubCurrent<T> ans = 2.*Contract(J2, J4)*J3 - Contract(J2, J3)*J4 - Contract(J3, J4)*J2;
    ans.flav = Gluon;
    return 0.5*HGGGG*ans;
  }
  return SubCurrent<T>();
}
