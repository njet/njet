/*
* ngluon2/LoopIntegrals.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include "LoopIntegrals.h"
#include "utility.h"

/*
 * For the scalar one-loop integrals we use....
 *
 * Refs: [1] van Oldenborgh: FF: A Package To Evaluate One Loop Feynman Diagrams
 *           Comput.Phys.Commun.66:1-15,1991
 *       [2] R.Keith Ellis, Giulia Zanderighi,  "Scalar one-loop integrals for QCD",
 *           JHEP 0802:002,2008.
 *
 */

template <typename T>
EpsTriplet<T> I4(const T s_, const T t_,
                 const T M1_, const T M2_, const T M3_, const T M4_,
                 const T m1_, const T m2_, const T m3_, const T m4_,
                 const T MUR2_)
{
  fortran_complex ep0 = {1., 0};
  fortran_complex ep1 = {1., 0};
  fortran_complex ep2 = {1., 0};

#ifdef QCDLOOP
  double s = to_double(s_);
  double t = to_double(t_);
  double M1 = to_double(M1_);
  double M2 = to_double(M2_);
  double M3 = to_double(M3_);
  double M4 = to_double(M4_);
  double m1 = to_double(m1_);
  double m2 = to_double(m2_);
  double m3 = to_double(m3_);
  double m4 = to_double(m4_);
  double MUR2 = to_double(MUR2_);

  int eps;
#ifdef USE_F2C
  eps = 0;
  F77_FUNC(qli4,QLI4)(&ep0, &M1, &M2, &M3, &M4, &s, &t, &m1, &m2, &m3, &m4, &MUR2, &eps);
  eps = -1;
  F77_FUNC(qli4,QLI4)(&ep1, &M1, &M2, &M3, &M4, &s, &t, &m1, &m2, &m3, &m4, &MUR2, &eps);
  eps = -2;
  F77_FUNC(qli4,QLI4)(&ep2, &M1, &M2, &M3, &M4, &s, &t, &m1, &m2, &m3, &m4, &MUR2, &eps);
#else // USE_F2C
  eps = 0;
  ep0 = F77_FUNC(qli4,QLI4)(&M1, &M2, &M3, &M4, &s, &t, &m1, &m2, &m3, &m4, &MUR2, &eps);
  eps = -1;
  ep1 = F77_FUNC(qli4,QLI4)(&M1, &M2, &M3, &M4, &s, &t, &m1, &m2, &m3, &m4, &MUR2, &eps);
  eps = -2;
  ep2 = F77_FUNC(qli4,QLI4)(&M1, &M2, &M3, &M4, &s, &t, &m1, &m2, &m3, &m4, &MUR2, &eps);
#endif // USE_F2C

#endif // QCDLOOP

  const std::complex<double> cep0(ep0.re, ep0.im);
  const std::complex<double> cep1(ep1.re, ep1.im);
  const std::complex<double> cep2(ep2.re, ep2.im);
  return EpsTriplet<T>(cep0, cep1, cep2);
}

template <typename T>
EpsTriplet<T> I3(const T M1_, const T M2_, const T M3_,
                 const T m1_, const T m2_, const T m3_,
                 const T MUR2_)
{
  fortran_complex ep0 = {1., 0};
  fortran_complex ep1 = {1., 0};
  fortran_complex ep2 = {1., 0};

#ifdef QCDLOOP
  double M1 = to_double(M1_);
  double M2 = to_double(M2_);
  double M3 = to_double(M3_);
  double m1 = to_double(m1_);
  double m2 = to_double(m2_);
  double m3 = to_double(m3_);
  double MUR2 = to_double(MUR2_);

  int eps;
#ifdef USE_F2C
  eps = 0;
  F77_FUNC(qli3,QLI3)(&ep0, &M1, &M2, &M3, &m1, &m2, &m3, &MUR2, &eps);
  eps = -1;
  F77_FUNC(qli3,QLI3)(&ep1, &M1, &M2, &M3, &m1, &m2, &m3, &MUR2, &eps);
  eps = -2;
  F77_FUNC(qli3,QLI3)(&ep2, &M1, &M2, &M3, &m1, &m2, &m3, &MUR2, &eps);
#else // USE_F2C
  eps = 0;
  ep0 = F77_FUNC(qli3,QLI3)(&M1, &M2, &M3, &m1, &m2, &m3, &MUR2, &eps);
  eps = -1;
  ep1 = F77_FUNC(qli3,QLI3)(&M1, &M2, &M3, &m1, &m2, &m3, &MUR2, &eps);
  eps = -2;
  ep2 = F77_FUNC(qli3,QLI3)(&M1, &M2, &M3, &m1, &m2, &m3, &MUR2, &eps);
#endif // USE_F2C

#endif // QCDLOOP

  const std::complex<double> cep0(ep0.re, ep0.im);
  const std::complex<double> cep1(ep1.re, ep1.im);
  const std::complex<double> cep2(ep2.re, ep2.im);
  return EpsTriplet<T>(cep0, cep1, cep2);
}

template <typename T>
EpsTriplet<T> I2(const T s_, const T m1_, const T m2_, const T MUR2_)
{
  fortran_complex ep0 = {1., 0};
  fortran_complex ep1 = {1., 0};
  fortran_complex ep2 = {1., 0};

#ifdef QCDLOOP
  double s = to_double(s_);
  double m1 = to_double(m1_);
  double m2 = to_double(m2_);
  double MUR2 = to_double(MUR2_);

  int eps;
#ifdef USE_F2C
  eps = 0;
  F77_FUNC(qli2,QLI2)(&ep0, &s, &m1, &m2, &MUR2, &eps);
  eps = -1;
  F77_FUNC(qli2,QLI2)(&ep1, &s, &m1, &m2, &MUR2, &eps);
  eps = -2;
  F77_FUNC(qli2,QLI2)(&ep2, &s, &m1, &m2, &MUR2, &eps);
#else // USE_F2C
  eps = 0;
  ep0 = F77_FUNC(qli2,QLI2)(&s, &m1, &m2, &MUR2, &eps);
  eps = -1;
  ep1 = F77_FUNC(qli2,QLI2)(&s, &m1, &m2, &MUR2, &eps);
  eps = -2;
  ep2 = F77_FUNC(qli2,QLI2)(&s, &m1, &m2, &MUR2, &eps);
#endif // USE_F2C

#endif // QCDLOOP

  const std::complex<double> cep0(ep0.re, ep0.im);
  const std::complex<double> cep1(ep1.re, ep1.im);
  const std::complex<double> cep2(ep2.re, ep2.im);
  return EpsTriplet<T>(cep0, cep1, cep2);
}

void myqlinit()
{
#ifdef QCDLOOP
  const double dbl_min=std::numeric_limits<double>::min();

  F77_FUNC(qlinit,QLINIT)();

  if (qlprec.xalogm < dbl_min) {
    qlprec.xalogm = dbl_min;
    qlprec.xalog2 = sqrt(dbl_min);
//         #ifndef NDEBUG
//         printf("Set xalogm to normalized value %e\n", qlprec.xalogm);
//         printf("Set xalog2 to normalized value %e\n", qlprec.xalog2);
//         #endif
  }
  if (qlprec.xclogm < dbl_min) {
    qlprec.xclogm = dbl_min;
    qlprec.xclog2 = sqrt(dbl_min);
//         #ifndef NDEBUG
//         printf("Set xclogm to normalized value %e\n", qlprec.xclogm);
//         printf("Set xclog2 to normalized value %e\n", qlprec.xclog2);
//         #endif
  }
  if (qlflag.lwarn) {
    qlflag.lwarn = 0;
//         #ifndef NDEBUG
//         printf("Disable FF warnings %d\n",qlflag.lwarn);
//         #endif
  }
#endif
}

void myqlexit()
{
#ifdef QCDLOOP
  F77_FUNC(ffexi,FFEXI)();
#endif
}

template
EpsTriplet<double> I4(const double s, const double t,
                      const double M1, const double M2, const double M3, const double M4,
                      const double m1, const double m2, const double m3, const double m4, const double MUR2);
template
EpsTriplet<double> I3(const double M1, const double M2, const double M3,
                      const double m1, const double m2, const double m3, const double MUR2);
template
EpsTriplet<double> I2(const double s, const double m1, const double m2, const double MUR2);

#ifdef USE_DD
  #include <qd/qd_real.h>
  template
  EpsTriplet<dd_real> I4(const dd_real s, const dd_real t,
                         const dd_real M1, const dd_real M2, const dd_real M3, const dd_real M4,
                         const dd_real m1, const dd_real m2, const dd_real m3, const dd_real m4, const dd_real MUR2);
  template
  EpsTriplet<dd_real> I3(const dd_real M1, const dd_real M2, const dd_real M3,
                         const dd_real m1, const dd_real m2, const dd_real m3, const dd_real MUR2);
  template
  EpsTriplet<dd_real> I2(const dd_real s, const dd_real m1, const dd_real m2, const dd_real MUR2);
#endif /* USE_DD */

#ifdef USE_QD
  #include <qd/qd_real.h>
  template
  EpsTriplet<qd_real> I4(const qd_real s, const qd_real t,
                         const qd_real M1, const qd_real M2, const qd_real M3, const qd_real M4,
                         const qd_real m1, const qd_real m2, const qd_real m3, const qd_real m4, const qd_real MUR2);
  template
  EpsTriplet<qd_real> I3(const qd_real M1, const qd_real M2, const qd_real M3,
                         const qd_real m1, const qd_real m2, const qd_real m3, const qd_real MUR2);
  template
  EpsTriplet<qd_real> I2(const qd_real s, const qd_real m1, const qd_real m2, const qd_real MUR2);
#endif /* USE_QD */

#ifdef USE_VC

template <>
EpsTriplet<Vc::double_v> I4(const Vc::double_v s_, const Vc::double_v t_,
                 const Vc::double_v M1_, const Vc::double_v M2_, const Vc::double_v M3_, const Vc::double_v M4_,
                 const Vc::double_v m1_, const Vc::double_v m2_, const Vc::double_v m3_, const Vc::double_v m4_,
                 const Vc::double_v MUR2_)
{
  Vc::double_v ep0r, ep0i;
  Vc::double_v ep1r, ep1i;
  Vc::double_v ep2r, ep2i;
  for (int i = 0; i < Vc::double_v::Size; ++i) {
    const EpsTriplet<double> val = I4(s_[i], t_[i], M1_[i], M2_[i], M3_[i], M4_[i],
                                      m1_[i], m2_[i], m3_[i], m4_[i], MUR2_[i]);
    ep0r[i] = val.get0().real();
    ep0i[i] = val.get0().imag();
    ep1r[i] = val.get1().real();
    ep1i[i] = val.get1().imag();
    ep2r[i] = val.get2().real();
    ep2i[i] = val.get2().imag();
  }

  return EpsTriplet<Vc::double_v>(std::complex<Vc::double_v>(ep0r, ep0i),
                                  std::complex<Vc::double_v>(ep1r, ep1i),
                                  std::complex<Vc::double_v>(ep2r, ep2i));
}

template <>
EpsTriplet<Vc::double_v> I3(const Vc::double_v M1_, const Vc::double_v M2_, const Vc::double_v M3_,
                            const Vc::double_v m1_, const Vc::double_v m2_, const Vc::double_v m3_,
                            const Vc::double_v MUR2_)
{
  Vc::double_v ep0r, ep0i;
  Vc::double_v ep1r, ep1i;
  Vc::double_v ep2r, ep2i;
  for (int i = 0; i < Vc::double_v::Size; ++i) {
    const EpsTriplet<double> val = I3(M1_[i], M2_[i], M3_[i],
                                      m1_[i], m2_[i], m3_[i], MUR2_[i]);
    ep0r[i] = val.get0().real();
    ep0i[i] = val.get0().imag();
    ep1r[i] = val.get1().real();
    ep1i[i] = val.get1().imag();
    ep2r[i] = val.get2().real();
    ep2i[i] = val.get2().imag();
  }

  return EpsTriplet<Vc::double_v>(std::complex<Vc::double_v>(ep0r, ep0i),
                                  std::complex<Vc::double_v>(ep1r, ep1i),
                                  std::complex<Vc::double_v>(ep2r, ep2i));
}


template <>
EpsTriplet<Vc::double_v> I2(const Vc::double_v s_,
                            const Vc::double_v m1_, const Vc::double_v m2_,
                            const Vc::double_v MUR2_)
{
  Vc::double_v ep0r, ep0i;
  Vc::double_v ep1r, ep1i;
  Vc::double_v ep2r, ep2i;
  for (int i = 0; i < Vc::double_v::Size; ++i) {
    const EpsTriplet<double> val = I2(s_[i], m1_[i], m2_[i], MUR2_[i]);
    ep0r[i] = val.get0().real();
    ep0i[i] = val.get0().imag();
    ep1r[i] = val.get1().real();
    ep1i[i] = val.get1().imag();
    ep2r[i] = val.get2().real();
    ep2i[i] = val.get2().imag();
  }

  return EpsTriplet<Vc::double_v>(std::complex<Vc::double_v>(ep0r, ep0i),
                                  std::complex<Vc::double_v>(ep1r, ep1i),
                                  std::complex<Vc::double_v>(ep2r, ep2i));
}

#endif /* USE_VC */
