/*
* ngluon2/EpsTriplet.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef NGLUON2_EPSTRIPLET_H
#define NGLUON2_EPSTRIPLET_H

#include <complex>
#include <iostream>

template <typename T>
class EpsTriplet
{
  public:
    typedef std::complex<T> complexT;
    EpsTriplet(complexT ep0_=complexT(),
               complexT ep1_=complexT(),
               complexT ep2_=complexT())
      : ep0(ep0_), ep1(ep1_), ep2(ep2_) {};

    complexT get0() const {
      return ep0;
    }
    complexT get1() const {
      return ep1;
    }
    complexT get2() const {
      return ep2;
    }

    EpsTriplet real() const {
      return EpsTriplet(ep0.real(), ep1.real(), ep2.real());
    }

    void set0(const complexT& x) {
      ep0 = x;
    }
    void set1(const complexT& x) {
      ep1 = x;
    }
    void set2(const complexT& x) {
      ep2 = x;
    }

    EpsTriplet operator- () const {
      return EpsTriplet(-ep0, -ep1, -ep2);
    }

    EpsTriplet conjugate() const {
      return EpsTriplet(conj(ep0), conj(ep1), conj(ep2));
    }

    template <typename U>
    EpsTriplet& operator*= (const U& c1) {
      ep0 *= c1;
      ep1 *= c1;
      ep2 *= c1;
      return *this;
    }

    template <typename U>
    EpsTriplet& operator/= (const U& c1) {
      ep0 /= c1;
      ep1 /= c1;
      ep2 /= c1;
      return *this;
    }

    EpsTriplet& operator+= (const EpsTriplet& R1) {
      ep0 += R1.ep0;
      ep1 += R1.ep1;
      ep2 += R1.ep2;
      return *this;
    }

    EpsTriplet& operator-= (const EpsTriplet& R1) {
      ep0 -= R1.ep0;
      ep1 -= R1.ep1;
      ep2 -= R1.ep2;
      return *this;
    }

    EpsTriplet& operator+= (const complexT R1) {
      ep0 += R1;
      return *this;
    }

    template <typename F>
    EpsTriplet<typename F::RType> apply(const F& func) const {
      return EpsTriplet<typename F::RType>(apply_to_obj(ep0, func),
                                           apply_to_obj(ep1, func),
                                           apply_to_obj(ep2, func));
    }

    template <typename TT>
    friend std::ostream& operator<< (std::ostream& stream, const EpsTriplet<TT>& R1);

    bool operator< (const EpsTriplet& amp) {
      return ep0.real() < amp.ep0.real();
    }

  private:
    complexT ep0, ep1, ep2;
};

template <typename T, typename U>
inline
EpsTriplet<T> operator* (EpsTriplet<T> ep3, const U& c1)
{
  ep3 *= c1;
  return ep3;
}

template <typename T, typename U>
inline
EpsTriplet<T> operator* (const U& c1, const EpsTriplet<T>& ep3)
{
  return ep3*c1;
}

template <typename T, typename U>
inline
EpsTriplet<T> operator/ (EpsTriplet<T> ep3, const U& c1)
{
  ep3 /= c1;
  return ep3;
}

template <typename T>
inline
EpsTriplet<T> operator+ (EpsTriplet<T> ep3a, const EpsTriplet<T>& ep3b)
{
  ep3a += ep3b;
  return ep3a;
}

template <typename T>
inline
EpsTriplet<T> operator- (EpsTriplet<T> ep3a, const EpsTriplet<T>& ep3b)
{
  ep3a -= ep3b;
  return ep3a;
}

template <typename T>
inline
EpsTriplet<T> operator+ (EpsTriplet<T> ep3a, const typename EpsTriplet<T>::complexT& x)
{
  ep3a += x;
  return ep3a;
}

template <typename T>
inline
EpsTriplet<T> conj(const EpsTriplet<T>& ep)
{
  return ep.conjugate();
}

template <typename T>
std::ostream& operator << (std::ostream& stream, const EpsTriplet<T>& R1)
{
 stream << R1.ep2 << "/e^2 + " << R1.ep1 << "/e^1 + " << R1.ep0;
 return stream;
}

// LoopResult
template <typename T>
struct LoopResult
{
  EpsTriplet<T> loop;
  EpsTriplet<T> loopC;

  operator EpsTriplet<T>() const {
    return loop;
  }

  LoopResult operator- () const {
    LoopResult ans = {-loop, -loopC};
    return ans;
  }

  template <typename U>
  LoopResult& operator*= (const U& c1) {
    loop *= c1;
    loopC *= c1;
    return *this;
  }

  template <typename U>
  LoopResult& operator/= (const U& c1) {
    loop /= c1;
    loopC /= c1;
    return *this;
  }

  LoopResult& operator+= (const LoopResult& x1) {
    loop += x1.loop;
    loopC += x1.loopC;
    return *this;
  }

  LoopResult& operator-= (const LoopResult& x1) {
    loop -= x1.loop;
    loopC -= x1.loopC;
    return *this;
  }
};

template <typename T, typename U>
inline
LoopResult<T> operator* (LoopResult<T> x1, const U& c1)
{
  x1 *= c1;
  return x1;
}

template <typename T, typename U>
inline
LoopResult<T> operator* (const U& c1, const LoopResult<T>& x1)
{
  return x1*c1;
}

template <typename T, typename U>
inline
LoopResult<T> operator/ (LoopResult<T> x1, const U& c1)
{
  x1 /= c1;
  return x1;
}

template <typename T>
inline
LoopResult<T> operator+ (LoopResult<T> x1, const LoopResult<T>& x2)
{
  x1 += x2;
  return x1;
}

template <typename T>
inline
LoopResult<T> operator- (LoopResult<T> x1, const LoopResult<T>& x2)
{
  x1 -= x2;
  return x1;
}

template <typename T>
std::ostream& operator << (std::ostream& stream, const LoopResult<T>& R1)
{
 stream << "[ " << R1.loop << " | " << R1.loopC << " ]";
 return stream;
}

#ifdef VC_DECLARE_ALLOCATOR_T
  VC_DECLARE_ALLOCATOR_T(EpsTriplet)
  VC_DECLARE_ALLOCATOR_T(LoopResult)
#endif

#endif /* NGLUON2_EPSTRIPLET_H */
