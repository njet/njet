/*
* ngluon2/Particle.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef NGLUON2_PARTICLE_H
#define NGLUON2_PARTICLE_H

#ifdef USE_VC
  #include "utility.h"
#endif

#include "Flavour.h"
#include "SubCurrent.h"
#include "Model.h"

template <typename T>
class Particle : public Flavour<T>
{
  public:
    typedef typename complex_traits<T>::real real;
    typedef typename complex_traits<T>::complex complex;

    typedef std::vector<Particle> ParticleList;
    typedef int hel_type;
    typedef MOM<T> Momentum;
    typedef MOM<real> RefMomentum;

    static const hel_type HINVAL = 0xFFFF;

    Particle()
      : Flavour<T>(), mom(), hel()
    {}

    Particle(const Flavour<T>& flav_)
      : Flavour<T>(flav_), mom(), hel()
    {}

    Particle(const Flavour<T>& flav_, const Momentum& m, const hel_type h=HINVAL)
      : Flavour<T>(flav_), mom(m), hel(h)
    {
      if (hel != HINVAL && hel != -HINVAL) {
        initPol();
      }
    }

    Particle operator- () const {
      return Particle(this->C(), -mom, -hel);
    }

    void setHelicity(hel_type h) {
      if (hel == HINVAL || hel == -HINVAL) {
        initPol();
      }
      hel = h;
    }

    void setExtHelicity(hel_type h, const ParticleList& process);

    const SubCurrent<real>& getPol() const {
      return pol[(hel+1)/2];
    }

    const SubCurrent<real>& getCachedPol(int h) const {
      return pol[(h+1)/2];
    }

    const SubCurrent<real>& getPol(int h) {
      setHelicity(h);
      return getPol();
    }

    const Momentum& getMom() const { return mom; }
    int setMomentum(const Momentum* moms);

    int decayMultiplicity() { return ModelBase::isMassiveVector(flav) ? 2 : 1; }

//     static const Momentum refvec;  // do we want per-particle refvectors?
    static const MOM<real> refvec; //do we want complex refvec for loop particles?

    static T getQu() { return T(2.)/3.; }
    static T getQd() { return T(-1.)/3.; }

  private:
    using Flavour<T>::flav;
    using Flavour<T>::mass;
    using Flavour<T>::width;
    using Flavour<T>::cf1;

    void initPol();
    void initPol(const Momentum& ref1, const Momentum& ref2);

    Momentum mom;
    SubCurrent<real> pol[4];  // pol[0] = -1 (minus), pol[1] = +1 (plus)
    hel_type hel;
};

template <typename T>
int Particle<T>::setMomentum(const Momentum* moms)
{
  if (ModelBase::isMassiveVector(flav)) {
    mom = moms[0] + moms[1];
    initPol(moms[0], moms[1]);
    return 2;
  } else {
    mom = moms[0];
    initPol();
    return 1;
  }
}

template <typename T>
void Particle<T>::setExtHelicity(hel_type h, const ParticleList& process)
{
  if (flav == ModelBase::ZbosonU || flav == ModelBase::ZbosonD) {
    // adjust Z-polarization in accordance with connected quark-line chirality
    for (unsigned i=0; i<process.size(); i++) {
      if (process[i].flav == Flavour<T>::VQ()) {
        h = h + 4*int(process[i].hel == -1);
        break;
      }
      if (process[i].flav == Flavour<T>::Vq()) {
        h = h + 4*int(process[i].hel == 1);
        break;
      }
    }
  }
  setHelicity(h);
}

template <typename T>
void Particle<T>::initPol()
{
  if (ModelBase::isGluon(flav)) {
    pol[0] = EPS(-1, mom, refvec);
    pol[1] = EPS( 1, mom, refvec);
  }
  else if (ModelBase::isSpinZero(flav)) {
    pol[0] = SubCurrent<real>::USC;
    pol[1] = SubCurrent<real>::USC;
  }
  else if (ModelBase::isLightFermion(flav)) {
    if (flav > 0) {
      pol[0] = vspn(-1, mom);
      pol[1] = vspn( 1, mom);
    }
    else {
      pol[0] = Uspn(-1, mom);
      pol[1] = Uspn( 1, mom);
    }
  }
  else if (ModelBase::isMassiveFermion(flav)) {
    if (flav > 0) {
      pol[0] = vspn(-1, mom, refvec, mass);
      pol[1] = vspn( 1, mom, refvec, mass);
    }
    else {
      pol[0] = Uspn(-1, mom, refvec, mass);
      pol[1] = Uspn( 1, mom, refvec, mass);
    }
  }
  else if (ModelBase::isLightVector(flav)) {
    T rt2 = constant_traits<T>::rt2();
    if (ModelBase::isUpPhoton(flav)) {
      rt2 *= getQu();
    }
    else if (ModelBase::isDownPhoton(flav)) {
      rt2 *= getQd();
    }
    pol[0] = rt2*EPS(-1, mom, refvec);
    pol[1] = rt2*EPS( 1, mom, refvec);
  }

  pol[0].flav = flav;
  pol[1].flav = flav;
}

template <typename T>
void Particle<T>::initPol(const Momentum& ref1, const Momentum& ref2)
{
  const T rt2 = constant_traits<T>::rt2();

  if (flav == ModelBase::Wplus || flav == ModelBase::Wminus) {
      pol[1] = pol[0] = CMOM(ref1, ref2)/((S(mom) - mass*mass + i_*width*mass)*rt2); // W+/-_LL
  }
  else if (flav == ModelBase::ZbosonU || flav == ModelBase::ZbosonD) {
    const T se2 = StandardModel::getSinThetaWSq();
    const T se = sqrt(se2);
    const T ce = sqrt(1. - se2);
    const T VeL = (-0.5 + se2)/(se*ce);
    const T VeR = se/ce;

    if (flav == ModelBase::ZbosonU) {
      const T Qu = getQu();
      const T VuL = (0.5 - Qu*se2)/(se*ce);
      const T VuR = -Qu*se/ce;
      pol[0] =
        - rt2*Qu*CMOM(ref1, ref2)/S(mom) // gamma_star
        + rt2*VeL*VuL*CMOM(ref1, ref2)/(S(mom) - mass*mass + i_*width*mass); // Z_LL
      pol[1] =
        - rt2*Qu*CMOM(ref2, ref1)/S(mom) // gamma_star
        + rt2*VeR*VuL*CMOM(ref2, ref1)/(S(mom) - mass*mass + i_*width*mass); // Z_RL
      pol[2] =
        - rt2*Qu*CMOM(ref1, ref2)/S(mom) // gamma_star
        + rt2*VeL*VuR*CMOM(ref1, ref2)/(S(mom) - mass*mass + i_*width*mass); // Z_LR
      pol[3] =
        - rt2*Qu*CMOM(ref2, ref1)/S(mom) // gamma_star
        + rt2*VeR*VuR*CMOM(ref2, ref1)/(S(mom) - mass*mass + i_*width*mass); // Z_RR
    }
    else {  // ModelBase::ZbosonD
      const T Qd = getQd();
      const T VdL = (-0.5 - Qd*se2)/(se*ce);
      const T VdR = -Qd*se/ce;
      pol[0] =
        - rt2*Qd*CMOM(ref1, ref2)/S(mom) // gamma_star
        + rt2*VeL*VdL*CMOM(ref1, ref2)/(S(mom) - mass*mass + i_*width*mass); // Z_LL
      pol[1] =
        - rt2*Qd*CMOM(ref2, ref1)/S(mom) // gamma_star
        + rt2*VeR*VdL*CMOM(ref2, ref1)/(S(mom) - mass*mass + i_*width*mass); // Z_RL
      pol[2] =
        - rt2*Qd*CMOM(ref1, ref2)/S(mom) // gamma_star
        + rt2*VeL*VdR*CMOM(ref1, ref2)/(S(mom) - mass*mass + i_*width*mass); // Z_LR
      pol[3] =
        - rt2*Qd*CMOM(ref2, ref1)/S(mom) // gamma_star
        + rt2*VeR*VdR*CMOM(ref2, ref1)/(S(mom) - mass*mass + i_*width*mass); // Z_RR
    }
  }
  else {  // ModelBase::GammaStar:
    pol[0] = -rt2*CMOM(ref1, ref2)/S(mom);
    pol[1] = -rt2*CMOM(ref2, ref1)/S(mom);
  }

  pol[0].flav = flav;
  pol[1].flav = flav;
  pol[2].flav = flav;
  pol[3].flav = flav;
}

template <typename T>
const typename Particle<T>::RefMomentum Particle<T>::refvec = RefMomentum(3., 2., 2., 1.);

#ifdef VC_DECLARE_ALLOCATOR_T
  VC_DECLARE_ALLOCATOR_T(Particle)
#endif

#endif /* NGLUON2_PARTICLE_H */
