/*
* ngluon2/NParton2.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef NGLUON2_NPARTON_H
#define NGLUON2_NPARTON_H

#include <vector>
#include <complex>

#include "NAmp.h"

template <typename T>
class NParton2 : public NAmp<T>
{
  public:
    typedef typename vector_traits<T>::scalar ST;
    using typename NAmp<T>::RealMom;

    NParton2();
    ~NParton2();

    EpsTriplet<ST> eval(const int primtype_, const int* ord=0);
    std::complex<ST> evalTree(const int* ord=0);

    std::complex<ST> gettree() const;
    std::complex<ST> getrat(int type=0) const;
    std::complex<ST> getraterr(int type=0) const;
    EpsTriplet<ST> getcc(int type=0) const;
    EpsTriplet<ST> getccerr(int type=0) const;
    EpsTriplet<ST> getres() const;
    EpsTriplet<ST> geterr() const;

    void setOrder(const int* ord);
    void setOrder(const std::vector<int>& ord);
    void setHeavyQuarkMass(const T m);

  protected:
    using NAmp<T>::ngluons;
    using NAmp<T>::initNG;
    AmpValue<ST> value;
    AmpValue<ST> error;
};

#endif /* NGLUON2_NPARTON_H */
