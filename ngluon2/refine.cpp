/*
* ngluon2/refine.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012, 2013 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include "Mom.h"

#include <cassert>
#include "refine.h"

// squared mass rounding threshold, 10^-13 gives ~2-3 digits margin
#define SQMASS_THRESHOLD  1e-13
#define MAX_ITERATIONS    16

// ----------------------------------------------------------------------------
// Version for plain arrays
// ----------------------------------------------------------------------------

template <typename SRC, typename DST>
void refine(const int NN, const SRC (*mom)[4], DST (*ddmom)[4], int np, int nq)
{
/*
({s0 + p0 + q0 == 0, s1 + p1 + q1 == 0, s2 + p2 + q2 == 0,
 s3 + p3 + q3 == 0, p0^2 - (p1^2 + p2^2 + p3^2) == 0,
 q0^2 - (q1^2 + q2^2 + q3^2) == 0}
 /. {p0 -> p0 + dp0, p3 -> p3 + dp3, q0 -> q0 + dq0, q3 -> q3 + dq3})
sol1 = Solve[%, {dp0, dp3, dq0, dq3}][[1]] // Simplify
*/
  if (np == -1 or nq == -1) {
    np = NN-2;
    nq = NN-1;
  }

  DST s0 = DST();
  DST s1 = DST();
  DST s2 = DST();
  DST s3 = DST();
  for (int n=0; n<NN; n++) {
    if (n == np or n == nq) continue;

    DST energy = sqrt( DST(mom[n][1])*mom[n][1]
                     + DST(mom[n][2])*mom[n][2]
                     + DST(mom[n][3])*mom[n][3] );
    if (mom[n][0] < 0.) {
      energy = -energy;
    }

    ddmom[n][0] = energy;
    ddmom[n][1] = mom[n][1];
    ddmom[n][2] = mom[n][2];
    ddmom[n][3] = mom[n][3];

    s0 += ddmom[n][0];
    s1 += ddmom[n][1];
    s2 += ddmom[n][2];
    s3 += ddmom[n][3];
  }

  const DST p1 = ddmom[np][1] = mom[np][1];
  const DST p2 = ddmom[np][2] = mom[np][2];
  const DST q1 = ddmom[nq][1] = -(mom[np][1] + s1);
  const DST q2 = ddmom[nq][2] = -(mom[np][2] + s2);

  const DST p0 = mom[np][0];
  const DST p3 = mom[np][3];
  const DST q0 = mom[nq][0];
  const DST q3 = mom[nq][3];

  const DST tmp1 = (p1 - q1)*(p1 + q1) + (p2 - q2)*(p2 + q2);
  const DST tmp2 = (s0 - s3)*(s0 + s3);
  const DST sqdis = s0*s0*(tmp1*tmp1 + tmp2*(tmp2 - 2.*(p1*p1 + p2*p2 + q1*q1 + q2*q2)));
  DST dis = sqrt(abs(sqdis));

  const DST den3 = 2.*tmp2;
  const DST den0 = s0*den3;

  const DST dp0_tmp = s0*(s0*tmp1 + (2.*p0 + s0)*tmp2);
  const DST dq0_tmp = s0*(s0*tmp1 - (2.*q0 + s0)*tmp2);
  const bool sign = (abs(dp0_tmp) > abs(dq0_tmp)) ? (dp0_tmp > 0.) : (dq0_tmp > 0.);
  if (sign ^ (dis*s3 <= 0.)) {
    dis = -dis;
  }
  const DST dp0 = (-dis*s3 - dp0_tmp)/den0;
  const DST dq0 = ( dis*s3 + dq0_tmp)/den0;
  const DST dp3 = (-dis - tmp1*s3 - (2.*p3 + s3)*tmp2)/den3;
  const DST dq3 = ( dis + tmp1*s3 - (2.*q3 + s3)*tmp2)/den3;

  ddmom[np][0] = p0 + dp0;
  ddmom[np][3] = p3 + dp3;
  ddmom[nq][0] = q0 + dq0;
  ddmom[nq][3] = q3 + dq3;
}

#ifdef USE_SD
  template void refine(const int NN, const double (*mom)[4], double (*ddmom)[4], int np, int nq);
#endif
#ifdef USE_DD
  template void refine(const int NN, const double (*mom)[4], dd_real (*ddmom)[4], int np, int nq);
  template void refine(const int NN, const dd_real (*mom)[4], dd_real (*ddmom)[4], int np, int nq);
#endif
#ifdef USE_QD
  template void refine(const int NN, const double (*mom)[4], qd_real (*ddmom)[4], int np, int nq);
  template void refine(const int NN, const dd_real (*mom)[4], qd_real (*ddmom)[4], int np, int nq);
  template void refine(const int NN, const qd_real (*mom)[4], qd_real (*ddmom)[4], int np, int nq);
#endif

// ----------------------------------------------------------------------------
// Version for MOM<T>
// ----------------------------------------------------------------------------

template <typename SRC, typename DST>
void refine(const int NN, const MOM<SRC>* mom, MOM<DST>* ddmom, int np, int nq)
{
/*
({s0 + p0 + q0 == 0, s1 + p1 + q1 == 0, s2 + p2 + q2 == 0,
 s3 + p3 + q3 == 0, p0^2 - (p1^2 + p2^2 + p3^2) == 0,
 q0^2 - (q1^2 + q2^2 + q3^2) == 0}
 /. {p0 -> p0 + dp0, p3 -> p3 + dp3, q0 -> q0 + dq0, q3 -> q3 + dq3})
sol1 = Solve[%, {dp0, dp3, dq0, dq3}][[1]] // Simplify
*/
  if (np == -1 or nq == -1) {
    np = NN-2;
    nq = NN-1;
  }

  DST s0 = DST();
  DST s1 = DST();
  DST s2 = DST();
  DST s3 = DST();
  for (int n=0; n<NN; n++) {
    if (n == np or n == nq) continue;

    DST energy = sqrt( DST(mom[n].x1)*mom[n].x1
                     + DST(mom[n].x2)*mom[n].x2
                     + DST(mom[n].x3)*mom[n].x3 );
    if (mom[n].x0 < 0.) {
      energy = -energy;
    }

    ddmom[n] = MOM<DST>(energy, mom[n].x1, mom[n].x2, mom[n].x3);

    s0 += ddmom[n].x0;
    s1 += ddmom[n].x1;
    s2 += ddmom[n].x2;
    s3 += ddmom[n].x3;
  }

  const DST p1 = ddmom[np].x1 = mom[np].x1;
  const DST p2 = ddmom[np].x2 = mom[np].x2;
  const DST q1 = ddmom[nq].x1 = -(mom[np].x1 + s1);
  const DST q2 = ddmom[nq].x2 = -(mom[np].x2 + s2);

  const DST p0 = mom[np].x0;
  const DST p3 = mom[np].x3;
  const DST q0 = mom[nq].x0;
  const DST q3 = mom[nq].x3;

  const DST tmp1 = (p1 - q1)*(p1 + q1) + (p2 - q2)*(p2 + q2);
  const DST tmp2 = (s0 - s3)*(s0 + s3);
  const DST sqdis = s0*s0*(tmp1*tmp1 + tmp2*(tmp2 - 2.*(p1*p1 + p2*p2 + q1*q1 + q2*q2)));
  DST dis = sqrt(abs(sqdis));

  const DST den3 = 2.*tmp2;
  const DST den0 = s0*den3;

  const DST dp0_tmp = s0*(s0*tmp1 + (2.*p0 + s0)*tmp2);
  const DST dq0_tmp = s0*(s0*tmp1 - (2.*q0 + s0)*tmp2);
  const bool sign = (abs(dp0_tmp) > abs(dq0_tmp)) ? (dp0_tmp > 0.) : (dq0_tmp > 0.);
  if (sign ^ (dis*s3 <= 0.)) {
    dis = -dis;
  }
  const DST dp0 = (-dis*s3 - dp0_tmp)/den0;
  const DST dq0 = ( dis*s3 + dq0_tmp)/den0;
  const DST dp3 = (-dis - tmp1*s3 - (2.*p3 + s3)*tmp2)/den3;
  const DST dq3 = ( dis + tmp1*s3 - (2.*q3 + s3)*tmp2)/den3;

  ddmom[np].x0 = p0 + dp0;
  ddmom[np].x3 = p3 + dp3;
  ddmom[nq].x0 = q0 + dq0;
  ddmom[nq].x3 = q3 + dq3;
}

template <typename SRC, typename DST>
void refine(const std::vector<MOM<SRC> >& mom, std::vector<MOM<DST> >& ddmom, int np, int nq)
{
  ddmom.resize(mom.size());
  refine(mom.size(), mom.data(), ddmom.data(), np, nq);
}

#ifdef USE_SD
  template void refine(const int NN, const MOM<double>* mom, MOM<double>* ddmom, int np, int nq);
  template void refine(const std::vector<MOM<double> >& mom, std::vector<MOM<double> >& ddmom, int np, int nq);
#endif
#ifdef USE_DD
  template void refine(const int NN, const MOM<double>* mom, MOM<dd_real>* ddmom, int np, int nq);
  template void refine(const int NN, const MOM<dd_real>* mom, MOM<dd_real>* ddmom, int np, int nq);
  template void refine(const std::vector<MOM<double> >& mom, std::vector<MOM<dd_real> >& ddmom, int np, int nq);
  template void refine(const std::vector<MOM<dd_real> >& mom, std::vector<MOM<dd_real> >& ddmom, int np, int nq);
#endif
#ifdef USE_QD
  template void refine(const int NN, const MOM<double>* mom, MOM<qd_real>* ddmom, int np, int nq);
  template void refine(const int NN, const MOM<dd_real>* mom, MOM<qd_real>* ddmom, int np, int nq);
  template void refine(const int NN, const MOM<qd_real>* mom, MOM<qd_real>* ddmom, int np, int nq);
  template void refine(const std::vector<MOM<double> >& mom, std::vector<MOM<qd_real> >& ddmom, int np, int nq);
  template void refine(const std::vector<MOM<dd_real> >& mom, std::vector<MOM<qd_real> >& ddmom, int np, int nq);
  template void refine(const std::vector<MOM<qd_real> >& mom, std::vector<MOM<qd_real> >& ddmom, int np, int nq);
#endif



// ----------------------------------------------------------------------------
// Version for MOM<T>
// ----------------------------------------------------------------------------

template <typename T, typename U>
static T truncate(const T val, const T acc, int numscales, const U* scales)
{
  T ans = val;
  bool known = false;
  for (int i=0; i<numscales; i++) {
    if (abs(val - T(scales[i])) < acc) {
      ans = T(scales[i]);
      known = true;
      break;
    }
  }
  if (not known) {
    NJET_WARN("unknown scale " << val);
  }
  return ans;
}


template <typename SRC, typename DST, typename SCT>
void refineM(const int NN, const MOM<SRC>* mom, MOM<DST>* ddmom, int numscales2, const SCT* scales2)
{
/*
F = {
  s0 + q0 + p0 == 0,
  s3 + q3 + p3 == 0,
  q0^2 - q12sq - q3^2 - qM^2,
  p0^2 - p12sq - p3^2 - pM^2}
  X = {q0, q3, p0, p3}
J = {{1, 0, 1, 0},
     {0, 1, 0, 1},
     {2 q0, -2 q3, 0, 0},
     {0, 0, 2 p0, -2 p3}}
J(X_n) (X_n+1 - X_n) = -F(X_n)
*/

  int np = NN-2;
  int nq = NN-1;

  SRC x1x2maxp = 0;
  SRC x1x2maxq = 0;

  DST scale = 0.;
  for (int n=0; n<NN; n++) {
    const DST x0abs = abs(mom[n].x0);
    if (x0abs > scale) {
      scale = x0abs;
    }
    const SRC x1x2abs = abs(mom[n].x1*mom[n].x2);
    if (x1x2abs > x1x2maxq) {
      if (nq != n) {
        x1x2maxp = x1x2maxq;
        np = nq;
      }
      if (np != n) {
        x1x2maxq = x1x2abs;
        nq = n;
      }
    } else if (x1x2abs > x1x2maxp) {
      if (nq != n) {
        x1x2maxp = x1x2abs;
        np = n;
      }
    }
  }
  assert(nq != np);

  const DST M2acc = (scale*scale)*SQMASS_THRESHOLD;

  DST s0 = DST();
  DST s1 = DST();
  DST s2 = DST();
  DST s3 = DST();

  for (int n=0; n<NN; n++) {
    if (n == np or n == nq) continue;

    const DST nP2 = DST(mom[n].x1)*mom[n].x1
                  + DST(mom[n].x2)*mom[n].x2
                  + DST(mom[n].x3)*mom[n].x3;
    DST nM2 = DST(mom[n].x0)*mom[n].x0 - nP2;
    nM2 = truncate(nM2, M2acc, numscales2, scales2);

    DST energy = sqrt(nP2 + nM2);
    if (mom[n].x0 < 0.) {
      energy = -energy;
    }
    ddmom[n] = MOM<DST>(energy, mom[n].x1, mom[n].x2, mom[n].x3);

    s0 += ddmom[n].x0;
    s1 += ddmom[n].x1;
    s2 += ddmom[n].x2;
    s3 += ddmom[n].x3;
  }

  const DST p1 = ddmom[np].x1 = mom[np].x1;
  const DST p2 = ddmom[np].x2 = mom[np].x2;
  const DST q1 = ddmom[nq].x1 = -(mom[np].x1 + s1);
  const DST q2 = ddmom[nq].x2 = -(mom[np].x2 + s2);

  DST p0 = mom[np].x0;
  DST p3 = mom[np].x3;
  DST q0 = mom[nq].x0;
  DST q3 = mom[nq].x3;

  const DST p12sq = p1*p1 + p2*p2;
  const DST q12sq = q1*q1 + q2*q2;

  DST pM2 = p0*p0 - (p12sq + p3*p3);
  DST qM2 = q0*q0 - (q12sq + q3*q3);

  pM2 = truncate(pM2, M2acc, numscales2, scales2);
  qM2 = truncate(qM2, M2acc, numscales2, scales2);

  DST dist = 1e30;
  DST prevdist = 1e31;
  int count = 0;

  DST newq0 = q0;
  DST newq3 = q3;
  DST newp0 = p0;
  DST newp3 = p3;

  do {
    q0 = newq0;
    q3 = newq3;
    p0 = newp0;
    p3 = newp3;

    const DST detJ = 4.*(q0*p3 - q3*p0);
    if (detJ == 0.) {
      NJET_WARN("det(J) == " << detJ << " expect nan");
    }

    const DST ex1 = 2.*p12sq - 2.*p3*p3 + 2.*pM2 + 4.*p0*s0 - 4.*p3*s3;
    const DST exq12 = 2.*q12sq + 2.*qM2;
    const DST exr12 = 2.*p12sq + 2.*pM2;
    const DST dq0 = (p3*(-2.*q0*q0 + exq12) + q3*(2.*p0*p0 - 2.*q3*p3 + 4.*q0*p0 + ex1))/detJ;
    const DST dq3 = (p0*( 2.*q3*q3 + exq12) + q0*(2.*p0*p0 + 2.*p0*q0 - 4.*q3*p3 + ex1))/detJ;
    const DST dp0 = (p3*(-2.*q0*q0 - exq12 - 4.*q0*p0 + 2.*q3*q3 - 4.*q0*s0)
                   + q3*(-exr12 + 2.*p0*p0 + 2.*p3*p3 + 4.*p3*s3))/detJ;
    const DST dp3 = (q0*(-2.*p0*p0 - exr12 - 2.*q0*p0 - 2.*p3*p3 - 4.*p0*s0)
                   + p0*(-exq12 + 2.*q3*q3 + 4.*q3*p3 + 4.*q3*s3))/detJ;
    newq0 = q0 + dq0;
    newq3 = q3 + dq3;
    newp0 = p0 + dp0;
    newp3 = p3 + dp3;
    prevdist = dist;
    dist = abs(s0 + newp0 + newq0) + abs(newp0*newp0 - (p12sq + newp3*newp3 + pM2))
         + abs(s3 + newp3 + newq3) + abs(newq0*newq0 - (q12sq + newq3*newq3 + qM2));
  } while (prevdist > dist and ++count < MAX_ITERATIONS);

  if (prevdist > dist and count == MAX_ITERATIONS) {
    NJET_WARN("failed to converge after " << count << " iterations");
  }

  ddmom[np].x0 = p0;
  ddmom[np].x3 = p3;
  ddmom[nq].x0 = q0;
  ddmom[nq].x3 = q3;
}

template <typename SRC, typename DST, typename SCT>
void refineM(const std::vector<MOM<SRC> >& mom, std::vector<MOM<DST> >& ddmom, const std::vector<SCT>& scales2)
{
  ddmom.resize(mom.size());
  refineM(mom.size(), mom.data(), ddmom.data(), scales2.size(), scales2.data());
}

#ifdef USE_SD
  template void refineM(const int NN, const MOM<double>* mom, MOM<double>* ddmom, int numscales2, const double* scales2);
  template void refineM(const std::vector<MOM<double> >& mom, std::vector<MOM<double> >& ddmom, const std::vector<double>& scales2);
#endif
#ifdef USE_DD
  template void refineM(const int NN, const MOM<double>* mom, MOM<dd_real>* ddmom, int numscales2, const double* scales2);
  template void refineM(const int NN, const MOM<dd_real>* mom, MOM<dd_real>* ddmom, int numscales2, const double* scales2);
  template void refineM(const std::vector<MOM<double> >& mom, std::vector<MOM<dd_real> >& ddmom, const std::vector<double>& scales2);
  template void refineM(const std::vector<MOM<dd_real> >& mom, std::vector<MOM<dd_real> >& ddmom, const std::vector<double>& scales2);
#endif
#ifdef USE_QD
  template void refineM(const int NN, const MOM<double>* mom, MOM<qd_real>* ddmom, int numscales2, const double* scales2);
  template void refineM(const int NN, const MOM<dd_real>* mom, MOM<qd_real>* ddmom, int numscales2, const double* scales2);
  template void refineM(const int NN, const MOM<qd_real>* mom, MOM<qd_real>* ddmom, int numscales2, const double* scales2);
  template void refineM(const std::vector<MOM<double> >& mom, std::vector<MOM<qd_real> >& ddmom, const std::vector<double>& scales2);
  template void refineM(const std::vector<MOM<dd_real> >& mom, std::vector<MOM<qd_real> >& ddmom, const std::vector<double>& scales2);
  template void refineM(const std::vector<MOM<qd_real> >& mom, std::vector<MOM<qd_real> >& ddmom, const std::vector<double>& scales2);
#endif
