/*
* ngluon2/Model.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include "Model.h"

#include <iostream>
#include <cstdlib>

using std::abs;

int StandardModel::Nf = 5;  // number of light flavours

double StandardModel::Tmass = 174.2;

double StandardModel::Wmass = 80.403;
double StandardModel::Wwidth = 2.14;

double StandardModel::Zmass = 91.1876;
double StandardModel::Zwidth = 2.4952;

double StandardModel::Hmass = 126.;
double StandardModel::Hwidth = 0.;

double StandardModel::SinThetaWSq = 0.22224648578577766;

const int StandardModel::up[3]   = {qU, qC, qT};
const int StandardModel::down[3] = {qD, qS, qB};

double StandardModel::CKMfull[3][3] = {  // CKM[up][down]
  {0.97427, 0.22534, 0.00351 },
  {0.22520, 0.97344, 0.0412  },
  {0.00867, 0.0404,  0.999146}
};

double StandardModel::CKM[3][3] = {  // CKM[up][down]
  {1., 0., 0.},
  {0., 1., 0.},
  {0., 0., 1.}
};

int StandardModel::upidx(const RealFlavour& f)
{
  int i = sizeof(up)/sizeof(up[0]);
  int fidx = f.idx();
  while (--i >= 0) {
    if (abs(fidx) == up[i]) break;
  }
  return i;
}

int StandardModel::downidx(const RealFlavour& f)
{
  int i = sizeof(down)/sizeof(down[0]);
  int fidx = f.idx();
  while (--i >= 0) {
    if (abs(fidx) == down[i]) break;
  }
  return i;
}

double* StandardModel::refCKM(const RealFlavour& f1, const RealFlavour& f2)
{
  int i, j;
  i = upidx(f1);
  j = downidx(f2);
  if (i >= 0 and j >= 0) {
    return &CKM[i][j];
  }
  i = upidx(f2);
  j = downidx(f1);
  if (i >= 0 and j >= 0) {
    return &CKM[i][j];
  }
  return 0;
}

Model::RealFlavour StandardModel::Wp(const RealFlavour& f1, const RealFlavour& f2,
                                     double cpl, double m, double w)
{
  const int flav1 = f1.idx();
  const int flav2 = f2.idx();
  if (not is4Quark(flav1) or not is4AntiQuark(flav2)) {
    std::cout << "Warning: Wplus(" << flav1 << ", " << flav2 << ")" << std::endl;
  }
  if (cpl == 0.) {
    return RealFlavour(Wplus, m, w, flav1, flav2, getCKM(f1, f2));
  } else {
    return RealFlavour(Wplus, m, w, flav1, flav2, cpl);
  }
}

Model::RealFlavour StandardModel::Wm(const RealFlavour& f1, const RealFlavour& f2,
                                     double cpl, double m, double w)
{
  const int flav1 = f1.idx();
  const int flav2 = f2.idx();
  if (not is4Quark(flav1) or not is4AntiQuark(flav2)) {
    std::cout << "Warning: Wminus(" << flav1 << ", " << flav2 << ")" << std::endl;
  }
  if (cpl == 0.) {
    return RealFlavour(Wminus, m, w, flav1, flav2, getCKM(f1, f2));
  } else {
    return RealFlavour(Wminus, m, w, flav1, flav2, cpl);
  }
}

Model::RealFlavour StandardModel::Zd(const RealFlavour& f1, const RealFlavour& f2,
                                    double m, double w)
{
  const int flav1 = f1.idx();
  const int flav2 = f2.idx();
  if (not is4Quark(flav1) or not is4AntiQuark(flav2) or flav1 != -flav2) {
    std::cout << "Warning: Z(" << flav1 << ", " << flav2 << ")" << std::endl;
  }
  return RealFlavour(ZbosonD, m, w, flav1, flav2);
}

Model::RealFlavour StandardModel::Zu(const RealFlavour& f1, const RealFlavour& f2,
                                    double m, double w)
{
  const int flav1 = f1.idx();
  const int flav2 = f2.idx();
  if (not is4Quark(flav1) or not is4AntiQuark(flav2) or flav1 != -flav2) {
    std::cout << "Warning: Z(" << flav1 << ", " << flav2 << ")" << std::endl;
  }
  return RealFlavour(ZbosonU, m, w, flav1, flav2);
}

Model::RealFlavour StandardModel::Gstar(const RealFlavour& f1, const RealFlavour& f2)
{
  const int flav1 = f1.idx();
  const int flav2 = f2.idx();
  if (not is4Quark(flav1) or not is4AntiQuark(flav2) or flav1 != -flav2) {
    std::cout << "Warning: GammaStar(" << flav1 << ", " << flav2 << ")" << std::endl;
  }
  return RealFlavour(GammaStar, 0., 0., flav1, flav2);
}

Model::RealFlavour StandardModel::Ad(const RealFlavour& f1, const RealFlavour& f2,
                                     int flav)
{
  const int flav1 = f1.idx();
  const int flav2 = f2.idx();
  if (not isDownPhoton(flav)) {
    std::cout << "Warning: Ad is not DOWN: " << flav << std::endl;
  }
  if (not is4Quark(flav1) or not is4AntiQuark(flav2) or flav1 != -flav2) {
    std::cout << "Warning: A(" << flav1 << ", " << flav2 << ")" << std::endl;
  }
  return RealFlavour(flav, 0., 0., flav1, flav2);
}

Model::RealFlavour StandardModel::Au(const RealFlavour& f1, const RealFlavour& f2,
                                     int flav)
{
  const int flav1 = f1.idx();
  const int flav2 = f2.idx();
  if (not isUpPhoton(flav)) {
    std::cout << "Warning: Au is not UP: " << flav << std::endl;
  }
  if (not is4Quark(flav1) or not is4AntiQuark(flav2) or flav1 != -flav2) {
    std::cout << "Warning: A(" << flav1 << ", " << flav2 << ")" << std::endl;
  }
  return RealFlavour(flav, 0., 0., flav1, flav2);
}

Model::RealFlavour StandardModel::Ax(const RealFlavour& f1, const RealFlavour& f2,
                                     int flav)
{
  const int flav1 = f1.idx();
  const int flav2 = f2.idx();
  if (not isPhoton(flav)) {
    std::cout << "Warning: Ax is not photon: " << flav << std::endl;
  }
  if (not is4Quark(flav1) or not is4AntiQuark(flav2) or flav1 != -flav2) {
    std::cout << "Warning: A(" << flav1 << ", " << flav2 << ")" << std::endl;
  }
  return RealFlavour(flav, 0., 0., flav1, flav2);
}

int StandardModel::SU2Flip(int flav)
{
  switch (flav) {
    case ModelBase::ZbosonU:  return ModelBase::ZbosonD;
    case ModelBase::ZbosonD:  return ModelBase::ZbosonU;

    case ModelBase::PhotonU1: return ModelBase::PhotonD1;
    case ModelBase::PhotonU2: return ModelBase::PhotonD2;
    case ModelBase::PhotonU3: return ModelBase::PhotonD3;

    case ModelBase::PhotonD1: return ModelBase::PhotonU1;
    case ModelBase::PhotonD2: return ModelBase::PhotonU2;
    case ModelBase::PhotonD3: return ModelBase::PhotonU3;
  }
  return flav;
}


Model::RealFlavour StandardModel::BosonFlip(const RealFlavour& ff)
{
  return ff.Change(SU2Flip(ff.idx()));
}

Model::RealFlavour StandardModel::BosonNext(const RealFlavour& ff)
{
  const int s1 = ff.pair1() > 0 ? 1 : -1;
  const int s2 = ff.pair2() > 0 ? 1 : -1;
  const int f = ModelBase::isPhoton(ff.idx()) ? 1 : 0;
  return RealFlavour(ff.idx()+f, ff.Mass(), ff.Width(), ff.pair1()+s1, ff.pair2()+s2);
}

StandardModel::RealFlavour::FlavourList StandardModel::NGluon1compat(const int n, const int* farr)
{
  RealFlavour::FlavourList flist(n);
  for (int i=0; i<n; i++) {
    switch (farr[i]) {
      case 0:
        flist[i] = G();
        break;
      case 1:
        flist[i] = u();
        break;
      case 2:
        flist[i] = d();
        break;
      case 3:
        flist[i] = c();
        break;
      case 4:
        flist[i] = s();
        break;
      case -1:
        flist[i] = u().C();
        break;
      case -2:
        flist[i] = d().C();
        break;
      case -3:
        flist[i] = c().C();
        break;
      case -4:
        flist[i] = s().C();
        break;
    }
  }
  return flist;
}
