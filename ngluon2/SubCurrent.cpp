/*
* ngluon2/SubCurrent.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

// #include "SubCurrent.h"

template <typename T>
const SubCurrent<T> SubCurrent<T>::USC = SubCurrent<T>(2, T(1.), T(0.), T(0.), T(0.));

template <typename T>
SubCurrent<T>::SubCurrent()
  : type(0), flav(0)
{
  J[0] = J[1] = J[2] = J[3] = real(0.);
}

template <typename T>
SubCurrent<T>::SubCurrent(const int type_,
                          SubCurrent<T>::complex_param j0,
                          SubCurrent<T>::complex_param j1,
                          SubCurrent<T>::complex_param j2,
                          SubCurrent<T>::complex_param j3)
  : type(type_), flav(0)
{
  J[0] = j0;
  J[1] = j1;
  J[2] = j2;
  J[3] = j3;
}

template <typename T>
template <typename U>
SubCurrent<T>::SubCurrent(const MOM<U>& mom)
  : type(3), flav(0)
{
  J[0] = mom.x0;
  J[1] = mom.x1;
  J[2] = mom.x2;
  J[3] = mom.x3;
}

template <typename T>
SubCurrent<T> SubCurrent<T>::operator- () const
{
  SubCurrent ans = SubCurrent(type, -J[0], -J[1], -J[2], -J[3]);
  ans.flav = flav;
  return ans;
}

template <typename T>
SubCurrent<T> SubCurrent<T>::conj() const
{
  SubCurrent ans = SubCurrent(type, std::conj(J[0]), std::conj(J[1]),
                                    std::conj(J[2]), std::conj(J[3]));
//   ans.flav = flav;
  return ans;
}

template <typename T>
SubCurrent<T> SubCurrent<T>::operator+= (const SubCurrent<T>& J1)
{
  J[0] += J1.J[0];
  J[1] += J1.J[1];
  J[2] += J1.J[2];
  J[3] += J1.J[3];
  type |= J1.type;
  flav |= J1.flav;
  return *this;
}

template <typename T>
SubCurrent<T> SubCurrent<T>::operator-= (const SubCurrent<T>& J1)
{
  J[0] -= J1.J[0];
  J[1] -= J1.J[1];
  J[2] -= J1.J[2];
  J[3] -= J1.J[3];
  type |= J1.type;
  flav |= J1.flav;
  return *this;
}

template <typename T>
template <typename U>
SubCurrent<T> SubCurrent<T>::operator*= (U x)
{
  J[0] *= x;
  J[1] *= x;
  J[2] *= x;
  J[3] *= x;
  return *this;
}

template <typename T>
template <typename U>
SubCurrent<T> SubCurrent<T>::operator/= (U x)
{
  J[0] /= x;
  J[1] /= x;
  J[2] /= x;
  J[3] /= x;
  return *this;
}

template <typename T>
SubCurrent<T> operator+ (SubCurrent<T> J1, const SubCurrent<T>& J2)
{
  J1 += J2;
  return J1;
}

template <typename T>
SubCurrent<T> operator- (SubCurrent<T> J1, const SubCurrent<T>& J2)
{
  J1 -= J2;
  return J1;
}

template <typename T, typename U>
SubCurrent<T> operator* (SubCurrent<T> J1, const U& x)
{
  J1 *= x;
  return J1;
}

template <typename T, typename U>
SubCurrent<T> operator* (SubCurrent<T> J1, const std::complex<U>& x)
{
  J1 *= x;
  return J1;
}

template <typename T, typename U>
SubCurrent<T> operator/ (SubCurrent<T> J1, const U& x)
{
  J1 /= x;
  return J1;
}

template <typename T, typename U>
SubCurrent<T> operator/ (SubCurrent<T> J1, const std::complex<U>& x)
{
  J1 /= x;
  return J1;
}

template <typename T>
std::ostream& operator<< (std::ostream& stream, const SubCurrent<T>& J1)
{
  stream << "[" << J1.type << "(" << J1.flav << "), ";
  stream << MOM<std::complex<T> >(J1.J[0], J1.J[1], J1.J[2], J1.J[3]);
  stream << "]";
  return stream;
}

template <typename T>
typename SubCurrent<T>::complex
SubCurrent<T>::Contract(const SubCurrent<T>& J1, const SubCurrent<T>& J2)
{
#define DEBUG_CURRENT_CONTRACT
#ifndef DEBUG_CURRENT_CONTRACT
  switch (J1.type) {
    case -1:
    case 1:
      return J1.J[0]*J2.J[0] + J1.J[1]*J2.J[1] + J1.J[2]*J2.J[2] + J1.J[3]*J2.J[3];
      break;
    case 2:
      return J1.J[0]*J2.J[0];
      break;
    case 3:
      return J1.J[0]*J2.J[0] - (J1.J[1]*J2.J[1] + J1.J[2]*J2.J[2] + J1.J[3]*J2.J[3]);
      break;
    default:
      return complex();
  }
#else
  if (J1.type == J2.type) {
    switch (J1.type) {
      case 3: {
        return J1.J[0]*J2.J[0] - (J1.J[1]*J2.J[1] + J1.J[2]*J2.J[2] + J1.J[3]*J2.J[3]);
      }
      break;
      case 2: {
        return J1.J[0]*J2.J[0];
      }
      break;
      default: {
        NJET_ERROR("don't know how to contract these currents (" << J1.type << ", " << J2.type << ")")
      }
    }
  } else if (J1.type == -J2.type) {
    switch (J1.type) {
      case 1: {
        return J1.J[0]*J2.J[0] + J1.J[1]*J2.J[1] + J1.J[2]*J2.J[2] + J1.J[3]*J2.J[3];
      }
      break;
      case -1: {
        return J1.J[0]*J2.J[0] + J1.J[1]*J2.J[1] + J1.J[2]*J2.J[2] + J1.J[3]*J2.J[3];
      }
      break;
      default: {
        NJET_ERROR("don't know how to contract these currents (" << J1.type << ", " << J2.type << ")")
      }
    }
  } else {
    NJET_ERROR("only same type currents can be contracted (" << J1.type << ", " << J2.type << ")")
  }
#endif
}

template <typename T>
template <typename U>
typename SubCurrent<T>::complex
SubCurrent<T>::Contract(const MOM<U>& P1, const SubCurrent<T>& J2)
{
  return Contract(SubCurrent(P1), J2);
}

template <typename T>
template <typename U>
typename SubCurrent<T>::complex
SubCurrent<T>::Contract(const SubCurrent<T>& J2, const MOM<U>& P1)
{
  return Contract(SubCurrent(P1), J2);
}


// computes slash(J1).J2
template <typename T>
SubCurrent<T> SubCurrent<T>::ContractSlash(const SubCurrent<T>& J1,
                                           const SubCurrent<T>& J2)
{
  if (J1.type == 3 && J2.type == 1) {
    const complex J1p = J1.J[0] + J1.J[2];
    const complex J1m = J1.J[0] - J1.J[2];
    const complex J1T = J1.J[3] + i_*J1.J[1];
    const complex J1Tb = J1.J[3] - i_*J1.J[1];

    const complex a1 = J1m*J2.J[2] - J1Tb*J2.J[3];
    const complex a2 = -J1T*J2.J[2] + J1p*J2.J[3];
    const complex a3 = J1p*J2.J[0] + J1Tb*J2.J[1];
    const complex a4 = J1T*J2.J[0] + J1m*J2.J[1];

    return SubCurrent(1, a1, a2, a3, a4);
  } else if (J1.type == 3 && J2.type == -1) {
    const complex J1p = J1.J[0] + J1.J[2];
    const complex J1m = J1.J[0] - J1.J[2];
    const complex J1T = J1.J[3] + i_*J1.J[1];
    const complex J1Tb = J1.J[3] - i_*J1.J[1];

    const complex a1 = J1p*J2.J[2] + J1T*J2.J[3];
    const complex a2 = J1Tb*J2.J[2] + J1m*J2.J[3];
    const complex a3 = J1m*J2.J[0] - J1T*J2.J[1];
    const complex a4 = -J1Tb*J2.J[0] + J1p*J2.J[1];

    return SubCurrent(-1, a1, a2, a3, a4);
  } else if (J1.type == 1 && J2.type == 3) {
    const complex J2p = J2.J[0] + J2.J[2];
    const complex J2m = J2.J[0] - J2.J[2];
    const complex J2T = J2.J[3] + i_*J2.J[1];
    const complex J2Tb = J2.J[3] - i_*J2.J[1];

    const complex a1 = J2m*J1.J[2] - J2Tb*J1.J[3];
    const complex a2 = -J2T*J1.J[2] + J2p*J1.J[3];
    const complex a3 = J2p*J1.J[0] + J2Tb*J1.J[1];
    const complex a4 = J2T*J1.J[0] + J2m*J1.J[1];

    return SubCurrent(1, a1, a2, a3, a4);
  } else if (J1.type == -1 && J2.type == 3) {
    const complex J2p = J2.J[0] + J2.J[2];
    const complex J2m = J2.J[0] - J2.J[2];
    const complex J2T = J2.J[3] + i_*J2.J[1];
    const complex J2Tb = J2.J[3] - i_*J2.J[1];

    const complex a1 = J2p*J1.J[2] + J2T*J1.J[3];
    const complex a2 = J2Tb*J1.J[2] + J2m*J1.J[3];
    const complex a3 = J2m*J1.J[0] - J2T*J1.J[1];
    const complex a4 = -J2Tb*J1.J[0] + J2p*J1.J[1];

    return SubCurrent(-1, a1, a2, a3, a4);
  } else {
    NJET_ERROR("only spinor currents can be slashed")
  }
}

// computes J1.gamma^mu.J2
template <typename T>
SubCurrent<T> SubCurrent<T>::ContractMu(const SubCurrent<T>& J1,
                                        const SubCurrent<T>& J2)
{
  if (J1.type == -1 && J2.type == 1) {
    const complex x02 = J1.J[0]*J2.J[2];
    const complex x03 = J1.J[0]*J2.J[3];
    const complex x20 = J1.J[2]*J2.J[0];
    const complex x30 = J1.J[3]*J2.J[0];
    const complex x12 = J1.J[1]*J2.J[2];
    const complex x13 = J1.J[1]*J2.J[3];
    const complex x21 = J1.J[2]*J2.J[1];
    const complex x31 = J1.J[3]*J2.J[1];

    const complex a1 = x02 + x20 + x13 + x31;
    const complex a2 = i_*(-x03 - x30 + x12 + x21);
    const complex a3 = x02 - x20 - x13 + x31;
    const complex a4 = x03 - x30 + x12 - x21;

    return SubCurrent(3, a1, a2, a3, a4);
  } else if (J1.type == 1 && J2.type == -1) {
    const complex x02 = J2.J[0]*J1.J[2];
    const complex x03 = J2.J[0]*J1.J[3];
    const complex x20 = J2.J[2]*J1.J[0];
    const complex x30 = J2.J[3]*J1.J[0];
    const complex x12 = J2.J[1]*J1.J[2];
    const complex x13 = J2.J[1]*J1.J[3];
    const complex x21 = J2.J[2]*J1.J[1];
    const complex x31 = J2.J[3]*J1.J[1];

    const complex a1 = x02 + x20 + x13 + x31;
    const complex a2 = i_*(-x03 - x30 + x12 + x21);
    const complex a3 = x02 - x20 - x13 + x31;
    const complex a4 = x03 - x30 + x12 - x21;

    return SubCurrent(3, a1, a2, a3, a4);
  } else {
    NJET_ERROR("only spinor currents can be slashed")
  }
}

template <typename T>
SubCurrent<T> EPS(int h, const MOM<T>& p, const MOM<T>& x)
{
  const double dh = static_cast<double>(h);

  const T ppp = pp(p);
  const T pmp = pm(p);
  const std::complex<T> pThp = h==1 ? pTb(p) : pT(p);

  const T pmx = pm(x);
  const std::complex<T> pThx = h==1 ? pTb(x) : pT(x);

  const std::complex<T> e0 = pThp*pmx + ppp*pThx;
  const std::complex<T> e1 = dh*i_*(pThp*pThx - ppp*pmx);
  const std::complex<T> e2 = ppp*pThx - pThp*pmx;
  const std::complex<T> e3 = ppp*pmx + pThp*pThx;

  const T rt2 = constant_traits<T>::rt2();
  const std::complex<T> norm = pThp/(rt2*ppp*(pThp*pmx - pmp*pThx));

  return SubCurrent<T>(3, norm*e0, norm*e1, norm*e2, norm*e3);
}

template <typename T>
SubCurrent<T> EPS(int h, const MOM<std::complex<T> >& p, const MOM<T>& x)
{
  const double dh = static_cast<double>(h);

  const std::complex<T> ppp = pp(p);
  const std::complex<T> pmp = pm(p);
  const std::complex<T> pThp = h==1 ? pTb(p) : pT(p);

  const T pmx = pm(x);
  const std::complex<T> pThx = h==1 ? pTb(x) : pT(x);

  const std::complex<T> e0 = pThp*pmx + ppp*pThx;
  const std::complex<T> e1 = dh*i_*(pThp*pThx - ppp*pmx);
  const std::complex<T> e2 = ppp*pThx - pThp*pmx;
  const std::complex<T> e3 = ppp*pmx + pThp*pThx;

  const T rt2 = constant_traits<T>::rt2();
  const std::complex<T> norm = pThp/(rt2*ppp*(pThp*pmx - pmp*pThx));

  return SubCurrent<T>(3, norm*e0, norm*e1, norm*e2, norm*e3);
}

template <typename T>
SubCurrent<T> EPS(int h, const MOM<std::complex<T> >& p, const MOM<std::complex<T> >& x)
{
  const double dh = static_cast<double>(h);

  const std::complex<T> ppp = pp(p);
  const std::complex<T> pmp = pm(p);
  const std::complex<T> pThp = h==1 ? pTb(p) : pT(p);

  const std::complex<T> pmx = pm(x);
  const std::complex<T> pThx = h==1 ? pTb(x) : pT(x);

  const std::complex<T> e0 = pThp*pmx + ppp*pThx;
  const std::complex<T> e1 = dh*i_*(pThp*pThx - ppp*pmx);
  const std::complex<T> e2 = ppp*pThx - pThp*pmx;
  const std::complex<T> e3 = ppp*pmx + pThp*pThx;

  const T rt2 = constant_traits<T>::rt2();
  const std::complex<T> norm = pThp/(rt2*ppp*(pThp*pmx - pmp*pThx));

  return SubCurrent<T>(3, norm*e0, norm*e1, norm*e2, norm*e3);
}

template <typename T>
SubCurrent<T> uspn(int h, const MOM<T>& p)
{
  if (h == 1) {
    const std::complex<T> norm = spNormA(p);
    return SubCurrent<T>(1, T(0.), T(0.), norm, norm*pm(p)/pTb(p));
  } else if (h == -1) {
    const std::complex<T> norm = spNormB(p);
    return SubCurrent<T>(1, norm, -norm*pp(p)/pTb(p), T(0.), T(0.));
  } else {
    NJET_ERROR("helicity " << h << " in uspn<T> not recognized")
  }
}

template <typename T>
SubCurrent<T> Uspn(int h, const MOM<T>& p)
{
  if (h == -1) {
    const std::complex<T> norm = 1./spNormB(p);
    return SubCurrent<T>(-1, T(0.), T(0.), norm*pm(p), -norm*pTb(p));
  } else if (h == 1) {
    const std::complex<T> norm = 1./spNormA(p);
    return SubCurrent<T>(-1, norm*pp(p), norm*pTb(p), T(0.), T(0.));
  } else {
    NJET_ERROR("helicity " << h << " in Uspn<T> not recognized")
  }
}

template <typename T>
SubCurrent<T> vspn(int h, const MOM<T>& p)
{
  return uspn(-h, p);
}

template <typename T>
SubCurrent<T> Vspn(int h, const MOM<T>& p)
{
  return Uspn(-h, p);
}

template <typename T>
SubCurrent<T> uspn(int h, const MOM<T>& p, const MOM<T>& e, T m)
{
  const MOM<T> pf = p - m * m / (2.*dot(p, e)) * e;

  if (h == -1) {
    return uspn(-1, pf) + m / xspA(pf, e) * uspn(1, e);
  } else if (h == 1) {
    return uspn(1, pf) + m / xspB(pf, e) * uspn(-1, e);
  } else {
    NJET_ERROR("helicity " << h << " in uspn<T> not recognized")
  }
}

template <typename T>
SubCurrent<T> Uspn(int h, const MOM<T>& p, const MOM<T>& e, T m)
{
  const MOM<T> pf = p - m * m / (2.*dot(p, e)) * e;

  if (h == -1) {
    return Uspn(-1, pf) + m / xspB(e, pf) * Uspn(1, e);
  } else if (h == 1) {
    return Uspn(1, pf) + m / xspA(e, pf) * Uspn(-1, e);
  } else {
    NJET_ERROR("helicity " << h << " in Uspn<T> not recognized")
  }
}

template <typename T>
SubCurrent<T> vspn(int h, const MOM<T>& p, const MOM<T>& e, T m)
{
  const MOM<T> pf = p - m * m / (2.*dot(p, e)) * e;

  if (h == -1) {
    return vspn(-1, pf) - m / xspB(pf, e) * vspn(1, e);
  } else if (h == 1) {
    return vspn(1, pf) - m / xspA(pf, e) * vspn(-1, e);
  } else {
    NJET_ERROR("helicity " << h << " in vspn<T> not recognized")
  }
}

template <typename T>
SubCurrent<T> Vspn(int h, const MOM<T>& p, const MOM<T>& e, T m)
{
  const MOM<T> pf = p - m * m / (2.*dot(p, e)) * e;

  if (h == -1) {
    return Vspn(-1, pf) - m / xspA(e, pf) * Vspn(1, e);
  } else if (h == 1) {
    return Vspn(1, pf) - m / xspB(e, pf) * Vspn(-1, e);
  } else {
    NJET_ERROR("helicity " << h << " in Vspn<T> not recognized")
  }
}


template <typename T>
SubCurrent<T> uspn(int h, const MOM<std::complex<T> >& p)
{
  if (h == 1) {
    const std::complex<T> norm = spNormA(p);
    return SubCurrent<T>(1, T(0.), T(0.), norm, norm*pm(p)/pTb(p));
  } else if (h == -1) {
    const std::complex<T> norm = spNormB(p);
    return SubCurrent<T>(1, norm, -norm*pp(p)/pTb(p), T(0.), T(0.));
  } else {
    NJET_ERROR("helicity " << h << " in uspn<C<T>> not recognized")
  }
}

template <typename T>
SubCurrent<T> Uspn(int h, const MOM<std::complex<T> >& p)
{
  if (h == -1) {
    const std::complex<T> norm = 1./spNormB(p);
    return SubCurrent<T>(-1, T(0.), T(0.), norm*pm(p), -norm*pTb(p));
  } else if (h == 1) {
    const std::complex<T> norm = 1./spNormA(p);
    return SubCurrent<T>(-1, norm*pp(p), norm*pTb(p), T(0.), T(0.));
  } else {
    NJET_ERROR("helicity " << h << " in Uspn<C<T>> not recognized")
  }
}

template <typename T>
SubCurrent<T> vspn(int h, const MOM<std::complex<T> >& p)
{
  return uspn(-h, p);
}

template <typename T>
SubCurrent<T> Vspn(int h, const MOM<std::complex<T> >& p)
{
  return Uspn(-h, p);
}

template <typename T>
SubCurrent<T> uspn(int h,
                   const MOM<std::complex<T> >& p,
                   const MOM<std::complex<T> >& e,
                   std::complex<T> m)
{
  const MOM<std::complex<T> > pf = p - m * m / (2.*dot(p, e)) * e;

  if (h == -1) {
    return uspn(-1, pf) + m / xspA(pf, e) * uspn(1, e);
  } else if (h == 1) {
    return uspn(1, pf) + m / xspB(pf, e) * uspn(-1, e);
  } else {
    NJET_ERROR("helicity " << h << " in uspn<C<T>> not recognized")
  }
}

template <typename T>
SubCurrent<T> Uspn(int h,
                   const MOM<std::complex<T> >& p,
                   const MOM<std::complex<T> >& e,
                   std::complex<T> m)
{
  const MOM<std::complex<T> > pf = p - m * m / (2.*dot(p, e)) * e;

  if (h == -1) {
    return Uspn(-1, pf) + m / xspB(e, pf) * Uspn(1, e);
  } else if (h == 1) {
    return Uspn(1, pf) + m / xspA(e, pf) * Uspn(-1, e);
  } else {
    NJET_ERROR("helicity in " << h << " Uspn<C<T>> not recognized")
  }
}

template <typename T>
SubCurrent<T> vspn(int h,
                   const MOM<std::complex<T> >& p,
                   const MOM<std::complex<T> >& e,
                   std::complex<T> m)
{
  const MOM<std::complex<T> > pf = p - m * m / (2.*dot(p, e)) * e;

  if (h == -1) {
    return vspn(-1, pf) - m / xspB(pf, e) * vspn(1, e);
  } else if (h == 1) {
    return vspn(1, pf) - m / xspA(pf, e) * vspn(-1, e);
  } else {
    NJET_ERROR("helicity " << h << " in vspn<C<T>> not recognized")
  }
}

template <typename T>
SubCurrent<T> Vspn(int h,
                   const MOM<std::complex<T> >& p,
                   const MOM<std::complex<T> >& e,
                   std::complex<T> m)
{
  const MOM<std::complex<T> > pf = p - m * m / (2.*dot(p, e)) * e;

  if (h == -1) {
    return Vspn(-1, pf) - m / xspA(e, pf) * Vspn(1, e);
  } else if (h == 1) {
    return Vspn(1, pf) - m / xspB(e, pf) * Vspn(-1, e);
  } else {
    NJET_ERROR("helicity " << h << " in Vspn<C<T>> not recognized")
  }
}
