/*
* ngluon2/PMatrix.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef NGLUON2_PMATRIX_H
#define NGLUON2_PMATRIX_H

#include <vector>

template <typename T>
class PMatrix
{
  public:
    PMatrix()
      : n(0), data()
    {}

    PMatrix(const int n_)
      : n(n_), data(n*(n-1))
    {}

    void resize(const int n_) {
      n = n_;
      data.resize(n*(n-1));
    }

    inline
    const T& operator() (int i1, int i2) const {
      return data[i2%n + n*((i2-i1+n)%n)];
    }

    inline
    T& operator() (int i1, int i2) {
      return data[i2%n + n*((i2-i1+n)%n)];
    }

    inline
    const T& operator[] (int i1) const {
      return data[i1];
    }

    inline
    T& operator[] (int i1) {
      return data[i1];
    }

  private:
    int n;
    std::vector<T> data;
};

#endif /* NGLUON2_PMATRIX_H */
