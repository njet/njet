/*
* ngluon2/debug.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef NGLUON2_DEBUG_H
#define NGLUON2_DEBUG_H

#include <iostream>

#if !defined(DEBUG_LEVEL)
  #define DEBUG_LEVEL 0
#endif

#if defined(DEBUG_LEVEL) && DEBUG_LEVEL != 0
  #undef NDEBUG
#endif

#ifdef NDEBUG
  #define DEBUG_MSG(lvl, msg) /* msg */
#else
  #define DEBUG_MSG(lvl, msg) \
    if (!lvl or lvl & DEBUG_LEVEL) { \
      std::cout << __FILE__ << ":" << __LINE__ << " " << msg << std::endl; \
    }
#endif

#endif /* NGLUON2_DEBUG_H */
