/*
* ngluon2/NGluon2-onshell.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include "NGluon2.h"


template <typename T>
//const MOM<T> NGluon2<T>::XiRef(3., 2., 1., 2.);
//const MOM<T> NGluon2<T>::XiRef(T(1.), T(3.)/sqrt(T(50.)), T(4.)/sqrt(T(50.)), T(5.)/sqrt(T(50.)));
//const MOM<complex<T> > NGluon2<T>::XiRef(complex<T>(3.,0.),complex<T>(2.,0.),complex<T>(2.,0.),complex<T>(1.,0.));
const typename NGluon2<T>::ComplexMom NGluon2<T>::XiRef(complex<T>(5.,0.),complex<T>(3.,-1.),complex<T>(3.,0.),complex<T>(3.,1.));

template <typename T>
void NGluon2<T>::initonshell()
{
  if (abs(S(XiRef)) > 1e-13) {
    NJET_ERROR("bad reference vector " << XiRef)
  }

  for (int i0=0; i0<n-1; i0++) {
    for (int i1=i0+1; i1<n; i1++) {
      const RealMom& K1 = PropMoms(i0, i1-1);
      const T m0 = propFlavour[i0].Mass();
      const T m1 = propFlavour[i1].Mass();
      const T m0sq = m0*m0;
      const T m1sq = m1*m1;
      const T S1 = PropMomsSq(i0, i1-1);  // S(K1);
      const T S1h = S1 - m0sq + m1sq;

      complex<T> rtS1 = sqrt(abs(S1));
      if (S1 < 0.) {
        rtS1 *= i_;
      }

      const complex<T> g1X = 2.*dot(K1, XiRef);
      const complex<T> rtg1X = sqrt(g1X);
      const complex<T> x1 = S1/g1X;
      const complex<T> rtx1 = sqrt(x1);
      const T x2 = S1h/S1;
      const T x3 = m1sq/S1;

      const ComplexMom K1f = K1 - x1*XiRef;
      const ComplexMom cv1X = 0.5*CMOM(K1f, XiRef);
      const ComplexMom cvX1 = 0.5*CMOM(XiRef, K1f);

      Momenta bubmom = bubM(i0, i1);
      bubmom[0] = -x2*K1f;  // 1
      bubmom[1] = K1f - x1*XiRef;  // tt1
      bubmom[2] = rtx1*cv1X;  // tt2
      bubmom[3] = -rtx1*cvX1;  // tt1^2/tt2
      bubmom[4] =  rtx1*x2*cvX1;  // tt1/tt2
      bubmom[5] = -rtx1*x3*cvX1;  // 1/tt2

      // spurious vectors:
      // the choice of normalization for the 3rd vector
      // means the scalar bubble coefficient is
      // explicitly independent of XiRef
      // it diverges for all on-shell bubbles
      bubmom[6] =     (cv1X - cvX1)/rtg1X;
      bubmom[7] =  i_*(cv1X + cvX1)/rtg1X;
      bubmom[8] = -i_*(K1f - x1*XiRef)/rtS1;

      Kinematics bubkin = bubK(i0, i1);
      bubkin[0] = S1;
      bubkin[1] = m0sq;
      bubkin[2] = m1sq;
      bubkin[3] = S1h;

      Momenta bubmomR = bubRM(i0, i1);
      bubmomR[0] = bubmom[0];
      bubmomR[1] = K1f - x1*XiRef;  // tt1
      bubmomR[2] = rtx1*cv1X;  // tt2
      bubmomR[3] = rtx1*cvX1;  // tt3

      Coefficients bubmu = bubMU(i0, i1);
      bubmu[0] = -m1sq;  // 1
      bubmu[1] = S1*x2;  // tt1
      bubmu[2] = -S1;  // tt1^2
      bubmu[3] = -S1;  // tt2*tt3

      for (int i2=i1+1; i2<n; i2++) {
        const RealMom& K2 = PropMoms(i1, i2-1);
        const T m2 = propFlavour[i2].Mass();
        const T m2sq = m2*m2;
        const T K1K2 = dot(K1, K2);
        const T S2 = PropMomsSq(i1, i2-1);  // S(K2);
        const T S2h = S2 + m1sq - m2sq;

        T g12 = K1K2;
        if (K1K2 < 0.) {
          g12 -= sqrt(K1K2*K1K2 - S1*S2);
        } else {
          g12 += sqrt(K1K2*K1K2 - S1*S2);
        }
        const T DEN  = g12*g12 - S1*S2;

        const RealMom K1f = g12*(g12*K1 - S1*K2)/DEN;
        const RealMom K2f = g12*(g12*K2 - S2*K1)/DEN;

        const ComplexMom cv12 = 0.5*CMOM(K1f, K2f);
        const ComplexMom cv21 = 0.5*CMOM(K2f, K1f);

        complex<T> rtg12 = sqrt(abs(g12));
        if (g12 < 0.) {
          rtg12 *= i_;
        }

        Momenta trimom = triM(i0, i1, i2);
        const T a = (S2h*g12 + S1h*S2)/DEN;
        const T b = -(S1h*g12 + S2h*S1)/DEN;
        const T G3 = a*b - m1sq/g12;
        const T G3h = a*b*g12 - m1sq;

        trimom[0] = a*K1f + b*K2f;  // 1
        trimom[1] = cv12;  // tt1
        trimom[2] = cv21;  // 1/tt1
        trimom[3] =    (cv12 - cv21)/rtg12;
        trimom[4] = i_*(cv12 + cv21)/rtg12;

        Kinematics trikin = triK(i0, i1, i2);
        trikin[0] = G3;
        trikin[1] = S1;
        trikin[2] = S2;
        trikin[3] = S(K1+K2);
        trikin[4] = m0sq;
        trikin[5] = m1sq;
        trikin[6] = m2sq;
        trikin[7] = G3h;
        trikin[8] = g12;

        // looks uneccessary...almost certainly is...
        Momenta trimomR = triRM(i0, i1, i2);
        trimomR[0] = trimom[0];  // 1
        trimomR[1] = trimom[1];  // tt1
        trimomR[2] = trimom[2];  // tt2

        Coefficients trimu = triMU(i0, i1, i2);
        trimu[0] = G3h;  // 1
        trimu[1] = -g12;  // tt1*tt2

        for (int i3=i2+1; i3<n; i3++) {
          const RealMom& K3 = PropMoms(i2, i3-1);
          const T m3 = propFlavour[i3].Mass();
          const T m3sq = m3*m3;
          const T S3 = PropMomsSq(i2, i3-1);
          const T S23 = S3 + 2.*dot(K2, K3);
          const T S23h = S23 + m2sq - m3sq;

          const complex<T> AB132 = spAB(K1f, K3, K2f);
          const complex<T> AB231 = spAB(K2f, K3, K1f);

          const T A = a*2.*dot(K1f, K3) + b*2.*dot(K2f, K3);

          const T AA = A - S23h;
          const T tr1323 = real(AB132*AB231);

          const T G42 = 4.*tr1323/g12;
          const T G41 = AA*AA - G3h*G42;

          if (G41 < 0.) {
            NJET_ERROR("G41 = " << G41 << " < 0, square root is complex")
          }
          if (G42 < 0.) {
            NJET_ERROR("G42 = " << G42 << " < 0, square root is complex")
          }
          const T rtG41 = sqrt(G41);
          const T rtG42 = sqrt(G42);

          const complex<T> c1 = -(AA - rtG41)/(2.*AB132);
          const complex<T> d1 = -(AA + rtG41)/(2.*AB231);

          Momenta boxmom = boxM(i0, i1, i2, i3);
          boxmom[0] = trimom[0] + c1*cv12 + d1*cv21;
          boxmom[1] = 2.*(AB231*cv12 - AB132*cv21)/(rtG42*g12);

          const complex<T> c2 = -(AA + rtG41)/(2.*AB132);
          const complex<T> d2 = -(AA - rtG41)/(2.*AB231);
          boxmom[2] = trimom[0] + c2*cv12 + d2*cv21;

          Kinematics boxkin = boxK(i0, i1, i2, i3);
          boxkin[0] = rtG42/rtG41;
          boxkin[1] = S1;
          boxkin[2] = S2;
          boxkin[3] = S3;
          boxkin[4] = S(K1+K2+K3);
          boxkin[5] = S(K1+K2);
          boxkin[6] = S(K2+K3);
          boxkin[7] = m0sq;
          boxkin[8] = m1sq;
          boxkin[9] = m2sq;
          boxkin[10] = m3sq;
          boxkin[11] = G42/G41;

          Momenta boxRmom = boxRM(i0, i1, i2, i3);
          boxRmom[0] = trimom[0] - 0.5*AA*(cv12/AB132 + cv21/AB231);  // 1
          boxRmom[1] = 0.5*rtG41*(cv12/AB132 - cv21/AB231);  // tt1
          Coefficients boxmu = boxMU(i0, i1, i2, i3);
          boxmu[0] = G3h - AA*AA/G42;  // 1
          boxmu[1] = G41/G42;  // tt1^2

          for (int i4=i3+1; i4<n; i4++) {
            const RealMom& K4 = PropMoms(i3, i4-1);
            const T m4 = propFlavour[i4].Mass();
            const T m4sq = m4*m4;
            const T S4 = PropMomsSq(i3, i4-1);
            const T S34 = S4 + 2.*dot(K4, K2+K3);
            const T S34h = S34 + m3sq - m4sq;

            const complex<T> AB142 = spAB(K1f, K4, K2f);
            const complex<T> AB241 = spAB(K2f, K4, K1f);
            const T B = a*2.*dot(K1f, K4) + b*2.*dot(K2f, K4);
            const T BB = B - S34h;
            const complex<T> tmp0 = AB132*AB241 - AB142*AB231;
            const complex<T> tmp1 = AB132*AB241 + AB142*AB231;
            const T tr1424 = real(AB142*AB241);
            const complex<T> c5 = ( BB*AB231 - AA*AB241)/tmp0;
            const complex<T> d5 = (-BB*AB132 + AA*AB142)/tmp0;

            Momenta pentmom = pentM(i0, i1, i2, i3, i4);
            Coefficients pentmu = pentMU(i0, i1, i2, i3, i4);
            pentmom[0] = trimom[0] + c5*cv12 + d5*cv21;
            pentmu[0] = g12*(G3 + (AA*AA*tr1424 + BB*BB*tr1323 - AA*BB*tmp1)/(tmp0*tmp0));
          }
        }
      }
    }
  }
}
