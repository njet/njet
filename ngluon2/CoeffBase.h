/*
* ngluon2/CoeffBase.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef NGLUON2_COEFFBASE_H
#define NGLUON2_COEFFBASE_H

#include <vector>

template <typename T>
class Coeff_Base
{
  public:
    Coeff_Base() {}

    Coeff_Base(int n_, int c_, int integrand_size)
      : n(n_), c(c_), isize(integrand_size)
    {
      ncfs = csize();
      cfs.resize(ncfs*isize);
    }

    void resize(int n_, int c_, int integrand_size)
    {
      n = n_;
      c = c_;
      isize = integrand_size;
      ncfs = csize();
      cfs.resize(ncfs*isize);
    }

    void clear() {
      cfs.assign(ncfs*isize, T());
    }

    T* operator() (int i1, int i2, int i3, int i4, int i5) {
      return &cfs[isize*key(i1, i2, i3, i4, i5)];
    }

    T* operator() (int i1, int i2, int i3, int i4) {
      return &cfs[isize*key(i1, i2, i3, i4)];
    }

    T* operator() (int i1, int i2, int i3) {
      return &cfs[isize*key(i1, i2, i3)];
    }

    T* operator() (int i1, int i2) {
      return &cfs[isize*key(i1, i2)];
    }

  private:

    inline
    int key(int i1, int i2, int i3, int i4, int i5) {
      return i1 + i2*(i2-1)/2 + i3*(i3-1)*(i3-2)/6 + i4*(i4-1)*(i4-2)*(i4-3)/24 + i5*(i5-1)*(i5-2)*(i5-3)*(i5-4)/120;
    }

    inline
    int key(int i1, int i2, int i3, int i4) {
      return i1 + i2*(i2-1)/2 + i3*(i3-1)*(i3-2)/6 + i4*(i4-1)*(i4-2)*(i4-3)/24;
    }

    inline
    int key(int i1, int i2, int i3) {
      return i1 + i2*(i2-1)/2 + i3*(i3-1)*(i3-2)/6;
    }

    inline
    int key(int i1, int i2) {
      return i1 + i2*(i2-1)/2;
    }

    int csize() {
      int num=1, den=1;
      for (int k=n; k>n-c; k--) {
        num *= k;
        den *= (n-k+1);
      }
      return num/den;
    }

    int n, c, ncfs, isize;
    std::vector<T> cfs;
};

#endif /* NGLUON2_COEFFBASE_H */
