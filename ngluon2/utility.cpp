
#include "utility.h"

#ifdef USE_VC

// namespace std
// {
// template <>
std::complex<Vc::double_v> sqrt(const std::complex<Vc::double_v>& z)
{
  Vc::double_v x = z.real();
  Vc::double_v y = z.imag();

  const Vc::double_m xnz = x != Vc::double_v::Zero();

  Vc::double_v t = sqrt(0.5 * abs(y));
  Vc::double_v xval = t;
  t(y < Vc::double_v::Zero()) = -t;
  Vc::double_v yval = t;

  if (not xnz.isEmpty()) {
    t(xnz) = sqrt(2. * (std::abs(z) + abs(x)));
    Vc::double_v u = 0.5 * t;
    const Vc::double_m xneg = x < Vc::double_v::Zero();
    xval(xnz) = u;
    yval(xnz) = y / t;
    xval(xneg) = abs(y) / t;
    u(y < Vc::double_v::Zero()) = -u;
    yval(xneg) = u;
  }

  return std::complex<Vc::double_v>(xval, yval);
}

Vc::double_v acos(const Vc::double_v& x)
{
  Vc::double_v ac;
  for (int i=0; i<Vc::double_v::Size; i++) {
    ac[i] = acos(x[i]);
  }
  return ac;
}

// }

static void checkVcStatus_fill()
{
  unsigned fill[Vc::double_v::Size*128];
  for (unsigned i=0; i<sizeof(fill)/sizeof(fill[0]); i++) {
    fill[i] = 0x55555555;
  }
}

static bool checkVcStatus_test()
{
  std::complex<Vc::double_v> zero1[16];
  std::complex<Vc::double_v> zero2[16] = {};
  std::complex<Vc::double_v> one1 = Vc::double_v(1.);
  for (unsigned i=0; i<sizeof(zero1)/sizeof(zero1[0]); i++) {
    if (zero1[i] != std::complex<Vc::double_v>(0., 0.)) {
      return false;
    }
  }
  for (unsigned i=0; i<sizeof(zero2)/sizeof(zero2[0]); i++) {
    if (zero2[i] != std::complex<Vc::double_v>(0., 0.)) {
      return false;
    }
  }
  if (one1 != std::complex<Vc::double_v>(1., 0.)) {
    return false;
  }
  // test default constructor in real->complex assignment
  one1 = std::complex<Vc::double_v>(1., 1.);
  one1 = Vc::double_v(1.);
  if (one1 != std::complex<Vc::double_v>(1., 0.)) {
    return false;
  }
  return true;
}

int checkVcStatus()
{
  if (Vc::double_v::Size == 1) {
    return 0;
  }
  checkVcStatus_fill();
  if (not checkVcStatus_test()) {
    return 0;
  }
  return Vc::double_v::Size;
}

#endif /* USE_VC */
