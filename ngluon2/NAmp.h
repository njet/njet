/*
* ngluon2/NAmp.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef NGLUON2_NAMP_H
#define NGLUON2_NAMP_H

#include <vector>

#include "Mom.h"
#include "Flavour.h"
#include "AmpValue.h"
#include "EpsTriplet.h"

template <typename T> class NGluon2;

template <typename T>
class NAmp
{
  public:
    typedef MOM<T> RealMom;

    NAmp();
    ~NAmp();

    void setMuR2(const T rscale);

    void setProcess(const int n_, const Flavour<T>* flavours);
    void setProcess(const std::vector<Flavour<T> >& flavours);

    template <typename U>
    void setProcess(const int n_, const Flavour<U>* otherflavarr);
    template <typename U>
    void setProcess(const std::vector<Flavour<U> >& otherflavours);

    void setMomenta(const RealMom* moms);
    void setMomenta(const std::vector<RealMom>& moms);

    template <typename U>
    void setMomenta(const MOM<U>* othermoms);
    template <typename U>
    void setMomenta(const std::vector<MOM<U> >& othermoms);

    void setHelicity(const int* helicity);
    void setHelicity(const std::vector<int>& helicity);

  protected:
    void initNG(const T scalefactor, unsigned pos);
    void clearNG();

    void setProcess(const int n_, const Flavour<T>* flavours, const int pos);
    void setProcess(const std::vector<Flavour<T> >& flavours, const int pos);

    template <typename U>
    void setProcess(const int n_, const Flavour<U>* otherflavarr, const int pos);
    template <typename U>
    void setProcess(const std::vector<Flavour<U> >& otherflavours, const int pos);

    std::vector<NGluon2<T>*> ngluons;
    std::allocator<NGluon2<T> > ngalloc;
};

#endif /* NGLUON2_NAMP_H */
