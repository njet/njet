/*
* ngluon2/Flavour.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef NGLUON2_FLAVOUR_H
#define NGLUON2_FLAVOUR_H

#include <vector>
#include <complex>

#ifdef USE_VC
  #include "utility.h"
#endif

#include "ModelBase.h"

template <typename T>
class Flavour
{
  public:
    typedef std::vector<Flavour> FlavourList;

    Flavour()
      : mass(), width(), coupling(), flav(ModelBase::NullFlavour), cf1(), cf2()
    {}

    Flavour(const int f, const T m=T(), const T w=T(),
            const int f1=0, const int f2=0, const T cpl=T(1.))
      : mass(m), width(w), coupling(cpl), flav(f), cf1(f1), cf2(f2)
    {}

    template <typename U>
    operator Flavour<U>() const {
      return Flavour<U>(flav, mass, width, cf1, cf2, coupling);
    }

    Flavour DimShift() const {
      const int Dflav = ModelBase::DimShift(flav);
      if (Dflav) {
        return Flavour(Dflav, mass, width, cf1, cf2, coupling);
      } else {
        return Flavour();
      }
    }

    Flavour Change(int f) const {
      return Flavour(f, mass, width, cf1, cf2, coupling);
    }

    Flavour C() const {
      if (ModelBase::isFermion(flav)) {
        return Flavour(-flav, mass, width);
      } else {
        return *this;
      }
    }

    Flavour& operator*= (const T x) {
      mass *= x;
      width *= x;
      return *this;
    }

    bool operator== (const Flavour& f) {
      return f.flav == flav;
    }

    int idx() const { return flav; }

    int pair1() const { return cf1; }
    int pair2() const { return cf2; }

    // mnemonic names
    int Vq() const { return cf1; }
    int VQ() const { return cf2; }

    void setMass(const T m) { mass = m; }

    T Mass() const { return mass; }
    T Width() const { return width; }
    T Coupling() const { return coupling; }

    // some shortcuts duplicating ModelBase functions
    bool isBlank() const { return ModelBase::isBlank(flav); }
    bool isParton() const { return ModelBase::isParton(flav); }
    bool isFermion() const { return ModelBase::isFermion(flav); }
    bool isGluon() const { return ModelBase::isGluon(flav); }
    bool isMassiveVector() const { return ModelBase::isMassiveVector(flav); }
    bool isLightVector() const { return ModelBase::isLightVector(flav); }
    bool isHiggs() const { return ModelBase::isHiggs(flav); }

    // analog of PropSpin and PropSpinD   // atm isScalar does the same
    int iterSpin() const { return ModelBase::isSpinZero(flav) ? 0 : -1; }
    int spinStates() const { return ModelBase::isSpinZero(flav) ? 1 : 2; }

  protected:
    T mass, width, coupling;
    int flav;
    int cf1, cf2;
};

template <typename T>
Flavour<T> operator* (Flavour<T> f, const T x)
{
  f *= x;
  return f;
}

template <typename T>
Flavour<T> operator* (const T x, Flavour<T> f)
{
  f *= x;
  return f;
}

#ifdef VC_DECLARE_ALLOCATOR_T
  VC_DECLARE_ALLOCATOR_T(Flavour)
#endif

#endif /* NGLUON2_FLAVOUR_H */
