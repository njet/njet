/*
* ngluon2/Mom.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef NGLUON2_MOM_H
#define NGLUON2_MOM_H

#include <iostream>

#include "utility.h"

template <typename T>
class SubCurrent;

template <typename T>
class MOM;

template <typename T>
class MOM
{
  public:
    typedef typename complex_traits<T>::complex complex;
    typedef typename call_traits<T>::param_type param_type;

    MOM()
      : x0(), x1(), x2(), x3() { }
    // MAYBE: pass by ref here?
    MOM(param_type x0_, param_type x1_, param_type x2_, param_type x3_)
      : x0(x0_), x1(x1_), x2(x2_), x3(x3_) { }

    operator MOM<complex>() const {
      return MOM<complex>(x0, x1, x2, x3);
    }

#ifdef USE_VC
    operator MOM<Vc::double_v>() const {
      return MOM<Vc::double_v>(Vc::double_v(x0), Vc::double_v(x1),
                               Vc::double_v(x2), Vc::double_v(x3));
    }
#endif

    MOM operator- () const;
    MOM conj() const;
    T mass() const;

    bool operator== (const MOM& m) const {
      return x0 == m.x0 && x1 == m.x1 && x2 == m.x2 && x3 == m.x3;
    }

    bool operator!= (const MOM& m) const {
      return x0 != m.x0 || x1 != m.x1 || x2 != m.x2 || x3 != m.x3;
    }

    template <typename U>
    MOM& operator+= (const MOM<U>& mm);

    template <typename U>
    MOM& operator-= (const MOM<U>& mm);

    template <typename U>
    MOM& operator*= (const U& x);

    template <typename U>
    MOM& operator/= (const U& x);

    template <typename U>
    static MOM opplus(MOM m1, const MOM<U>& m2);

    template <typename U>
    static MOM opminus(MOM m1, const MOM<U>& m2);

    template <typename U>
    T dotwith(const MOM<U>& m1) const;

    // no ref versions
    MOM& operator*= (const double x);
    MOM& operator/= (const double x);
    MOM& operator*= (const std::complex<double> x);
    MOM& operator/= (const std::complex<double> x);

    template <typename TT>
    friend TT S(const MOM<TT>& m);

    template <typename TT>
    friend std::ostream& operator<<(std::ostream& stream, const MOM<TT>& m);

    template <typename U> friend class MOM;
    template <typename U> friend class SubCurrent;

//   protected:
    T x0, x1, x2, x3;
};

template <typename T>
MOM<T> operator+ (const MOM<T>& m1, const MOM<T>& m2);

template <typename T>
MOM<T> operator- (const MOM<T>& m1, const MOM<T>& m2);

template <typename T, typename U>
MOM<T> operator* (MOM<T> m1, const U& x);
template <typename T, typename U>
MOM<T> operator* (const U& x, MOM<T> m1);

template <typename T, typename U>
MOM<T> operator/ (MOM<T> m1, const U& x);

// complex conversion (supposed to be inlined)
template <typename T>
MOM<std::complex<T> > operator+ (const MOM<std::complex<T> >& m1, const MOM<T>& m2);
template <typename T>
MOM<std::complex<T> > operator+ (const MOM<T>& m1, const MOM<std::complex<T> >& m2);

template <typename T>
MOM<std::complex<T> > operator- (const MOM<std::complex<T> >& m1, const MOM<T>& m2);
template <typename T>
MOM<std::complex<T> > operator- (const MOM<T>& m1, const MOM<std::complex<T> >& m2);

template <typename T>
MOM<std::complex<T> > operator* (const MOM<T>& m1, const std::complex<T>& x);
template <typename T>
MOM<std::complex<T> > operator* (const std::complex<T>& x, const MOM<T>& m1);
template <typename T>
MOM<std::complex<T> > operator/ (const MOM<T>& m1, const std::complex<T>& x);

// no ref versions of mul and div
template <typename T>
MOM<T> operator* (MOM<T> m1, const double x);
template <typename T>
MOM<T> operator* (const double x, MOM<T> m1);

template <typename T>
MOM<T> operator/ (MOM<T> m1, const double x);

template <typename T>
MOM<std::complex<T> > operator* (MOM<std::complex<T> > m1, const std::complex<double> x);
template <typename T>
MOM<std::complex<T> > operator* (const std::complex<double> x, MOM<std::complex<T> > m1);

template <typename T>
MOM<std::complex<T> > operator/ (MOM<std::complex<T> > m1, const std::complex<double> x);

// dot products, etc (supposed to be inlined)
template <typename T>
MOM<std::complex<T> > conj(const MOM<std::complex<T> >& m);

template <typename T>
T S(const MOM<T>& m);

template <typename T>
T dot(const MOM<T>& m1, const MOM<T>& m2);
template <typename T>
std::complex<T> dot(const MOM<std::complex<T> >& m1, const MOM<T>& m2);
template <typename T>
std::complex<T> dot(const MOM<T>& m1, const MOM<std::complex<T> >& m2);

// Old spinors

template <typename T>
std::complex<T> spNorm(const MOM<T>& p);

template <typename T>
std::complex<T> spNorm(const MOM<std::complex<T> >& p);

template <typename T>
std::complex<T> spA(const MOM<T>& p1, const MOM<T>& p2);
template <typename T>
std::complex<T> spB(const MOM<T>& p1, const MOM<T>& p2);
template <typename T>
std::complex<T> spAB(const MOM<T>& p1, const MOM<T>& p2, const MOM<T>& p3);

template <typename T>
std::complex<T> spA(const MOM<std::complex<T> >& p1, const MOM<std::complex<T> >& p2);
template <typename T>
std::complex<T> spB(const MOM<std::complex<T> >& p1, const MOM<std::complex<T> >& p2);
template <typename T>
std::complex<T> spAB(const MOM<std::complex<T> >& p1, const MOM<std::complex<T> >& p2, const MOM<std::complex<T> >& p3);

template <typename T>
MOM<std::complex<T> > CMOM(const MOM<T>& p1, const MOM<T>& p2);
template <typename T>
MOM<std::complex<T> > CMOM(const MOM<std::complex<T> >& p1, const MOM<std::complex<T> >& p2);

// New spinors

template <typename T>
std::complex<T> xspA(const MOM<T>& p, const MOM<T>& k);
template <typename T>
std::complex<T> xspB(const MOM<T>& p, const MOM<T>& k);
template <typename T>
std::complex<T> xspBA(const MOM<T>& p1, const MOM<T>& p2, const MOM<T>& p3);
template <typename T>
std::complex<T> xspAB(const MOM<T>& p1, const MOM<T>& p2, const MOM<T>& p3);

template <typename T>
std::complex<T> xspA(const MOM<std::complex<T> >& p, const MOM<std::complex<T> >& k);
template <typename T>
std::complex<T> xspB(const MOM<std::complex<T> >& p, const MOM<std::complex<T> >& k);
template <typename T>
std::complex<T> xspBA(const MOM<std::complex<T> >& p1, const MOM<std::complex<T> >& p2, const MOM<std::complex<T> >& p3);
template <typename T>
std::complex<T> xspAB(const MOM<std::complex<T> >& p1, const MOM<std::complex<T> >& p2, const MOM<std::complex<T> >& p3);

#include "Mom.cpp"

#ifdef VC_DECLARE_ALLOCATOR_T
  VC_DECLARE_ALLOCATOR_T(MOM)
#endif

#endif /* NGLUON2_MOM_H */
