/*
* ngluon2/Initialize.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef NGLUON2_INITIALIZE_H
#define NGLUON2_INITIALIZE_H

#if (defined(USE_DD) || defined(USE_QD)) && !defined(EXTENDED_PREC)
  #include <qd/qd_real.h>
  #define EXTENDED_PREC 1
#endif

class Initialize
{
  public:
    Initialize();
    ~Initialize();

  private:
    static bool valid;

    void info(const char* s);
    unsigned int old_cw;
    void initFloat();
    void uninitFloat();
    void initNJet();
    void initLoop();
    void uninitLoop();
};

#endif /* NGLUON2_INITIALIZE_H */
