/*
* ngluon2/ModelBase.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef NGLUON2_MODELBASE_H
#define NGLUON2_MODELBASE_H

class ModelBase
{
  public:
    enum PARAMETERS {
      NIf = 1,  // Number of internal loop flavours
      NLf = 6,  // Real light flavours (not including extra NIf light loop flavours)
      NHf = 3,  // Real heavy flavours (not including extra NIf heavy loop flavours)
    };
    /* LAYOUT:
     *      null-flavour, gluon, coloured scalar,
     *      [light quarks], [loop light quarks], [heavy quarks], [loop heavy quarks],
     *      [dim light quarks], [dim loop light quarks], [dim heavy quarks], [dim loop heavy quarks],
     *      heavy bosons, higgs
     */
    enum FLAVOURS {
      NullFlavour = 0, Gluon, Scalar,
      FirstQuark,
      LQuark = FirstQuark, ILQuark = LQuark + NLf,
      HQuark = ILQuark + NIf, IHQuark = HQuark + NHf,
      End4Quark = IHQuark + NIf,
      DLQuark = End4Quark, IDLQuark = DLQuark + NLf,
      DHQuark = IDLQuark + NIf, IDHQuark = DHQuark + NHf,
      EndQuark = IDHQuark + NIf,
      Wplus = EndQuark, Wminus, ZbosonU, ZbosonD, GammaStar,
      FirstPhoton, PhotonL = FirstPhoton,
      FirstUpPhoton, PhotonU1 = FirstUpPhoton, PhotonU2, PhotonU3,
      FirstDownPhoton, PhotonD1 = FirstDownPhoton, PhotonD2, PhotonD3,
      EndPhoton, Higgs = EndPhoton,
      EndFlavour
    };

    static const int QuarkShiftValue = End4Quark - FirstQuark;

    static bool isBlank(int flav) {return flav == NullFlavour; }
    static bool isNull(int flav) {return flav == NullFlavour; }

    static bool isGluon(int flav) { return flav == Gluon; }
    static bool isScalar(int flav) { return flav == Scalar; }
    static bool isHiggs(int flav) { return flav == Higgs; }
    static bool isSpinZero(int flav) { return flav == Scalar or flav == Higgs; }

    static bool isMassiveVector(int flav) { return flav >= Wplus and flav <= GammaStar; }
    static bool isLightVector(int flav) { return flav >= FirstPhoton and flav < EndPhoton; }
    static bool isWplus(int flav) { return flav == Wplus; }
    static bool isWminus(int flav) { return flav == Wminus; }
    static bool isZboson(int flav) { return flav == ZbosonU or flav == ZbosonD; }
    static bool isGammaStar(int flav) { return flav == GammaStar; }
    static bool isPhoton(int flav) { return flav >= FirstPhoton and flav < EndPhoton; }
    static bool isUpPhoton(int flav) { return flav >= FirstUpPhoton and flav < FirstDownPhoton; }
    static bool isDownPhoton(int flav) { return flav >= FirstDownPhoton and flav < EndPhoton; }

    // all quarks
    static bool isQuark(int flav) { return flav >= FirstQuark and flav < EndQuark; }
    static bool isAntiQuark(int flav) { return flav <= -FirstQuark and flav > -EndQuark; }
    static bool isFermion(int flav) { return isQuark(flav) or isAntiQuark(flav); }
    static bool isParton(int flav) { return isFermion(flav) or isGluon(flav); }

    // 4-dim light quarks (including loop flavours)
    static bool isLightQuark(int flav) { return flav >= LQuark and flav < HQuark; }
    static bool isLightAntiQuark(int flav) { return flav <= -LQuark and flav > -HQuark; }
    static bool isLightFermion(int flav) { return isLightQuark(flav) or isLightAntiQuark(flav); }

    // 4-dim heavy quarks (including loop flavours)
    static bool isHeavyQuark(int flav) { return flav >= HQuark and flav < DLQuark; }
    static bool isHeavyAntiQuark(int flav) { return flav <= -HQuark and flav > -DLQuark; }
    static bool isHeavyFermion(int flav) { return isHeavyQuark(flav) or isHeavyAntiQuark(flav); }

    // massive quarks (4-dim heavy and all d-dim)
    static bool isMassiveQuark(int flav) { return flav >= HQuark and flav < EndQuark; }
    static bool isMassiveAntiQuark(int flav) { return flav <= -HQuark and flav > -EndQuark; }
    static bool isMassiveFermion(int flav) { return isMassiveQuark(flav) or isMassiveAntiQuark(flav); }

    // all 4-dim quarks
    static bool is4Quark(int flav) { return flav >= LQuark and flav < End4Quark; }
    static bool is4AntiQuark(int flav) { return flav <= -LQuark and flav > -End4Quark; }
    static bool is4Fermion(int flav) { return is4Quark(flav) or is4AntiQuark(flav); }

    // all d-dim quarks
    static bool isDimQuark(int flav) { return flav >= DLQuark and flav < EndQuark; }
    static bool isDimAntiQuark(int flav) { return flav <= -DLQuark and flav > -EndQuark; }
    static bool isDimFermion(int flav) { return isDimQuark(flav) or isDimAntiQuark(flav); }

    // d-dim partners of light quarks
    static bool isDimLightQuark(int flav) { return flav >= DLQuark and flav < DHQuark; }
    static bool isDimLightAntiQuark(int flav) { return flav <= -DLQuark and flav > -DHQuark; }
    static bool isDimLightFermion(int flav) { return isDimLightQuark(flav) or isDimLightAntiQuark(flav); }

    // d-dim partners of heavy quarks
    static bool isDimHeavyQuark(int flav) { return flav >= DHQuark and flav < EndQuark; }
    static bool isDimHeavyAntiQuark(int flav) { return flav <= -DHQuark and flav > -EndQuark; }
    static bool isDimHeavyFermion(int flav) { return isDimHeavyQuark(flav) or isDimHeavyAntiQuark(flav); }

    static int DimShift(int flav) {
      if (is4Quark(flav)) {
        return flav + QuarkShiftValue;
      }
      if (is4AntiQuark(flav)) {
        return flav - QuarkShiftValue;
      }
      else if (flav == Gluon) {
        return Scalar;
      }
      return 0;
    }

    // NOTE the following functions do not check their arguments
    static int QuarkShift(int flav) { return flav + QuarkShiftValue; }
    static int AntiQuarkShift(int flav) { return flav - QuarkShiftValue; }
    static int DimQuarkShift(int flav) { return flav - QuarkShiftValue; }
    static int DimAntiQuarkShift(int flav) { return flav + QuarkShiftValue; }

  protected:
//     static int dshift[2*EndFlavour];
//     static bool dshift_valid;
};

#endif /* NGLUON2_MODELBASE_H */
