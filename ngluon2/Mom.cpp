/*
* ngluon2/Mom.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

// #include "Mom.h"

template <typename T>
std::ostream& operator<<(std::ostream& stream, const MOM<T>& m)
{
  stream << "(" << m.x0 << "," << m.x1 << "," << m.x2 << "," << m.x3 << ")";
  return stream;
}

template <typename T>
MOM<T> MOM<T>::operator-() const
{
  return MOM<T>(-x0, -x1, -x2, -x3);
}

template <typename T>
MOM<T> MOM<T>::conj() const
{
  return MOM<T>(std::conj(x0), std::conj(x1), std::conj(x2), std::conj(x3));
}

template <typename T>
T MOM<T>::mass() const
{
  return x0*x0 - (x1*x1 + x2*x2 + x3*x3);
}

template <typename T>
template <typename U>
MOM<T>& MOM<T>::operator+= (const MOM<U>& mm)
{
  x1 += mm.x1;
  x0 += mm.x0;
  x2 += mm.x2;
  x3 += mm.x3;
  return *this;
}

template <typename T>
template <typename U>
MOM<T>& MOM<T>::operator-= (const MOM<U>& mm)
{
  x0 -= mm.x0;
  x1 -= mm.x1;
  x2 -= mm.x2;
  x3 -= mm.x3;
  return *this;
}

template <typename T>
template <typename U>
MOM<T>& MOM<T>::operator*= (const U& x)
{
  x0 *= x;
  x1 *= x;
  x2 *= x;
  x3 *= x;
  return *this;
}

template <typename T>
template <typename U>
MOM<T>& MOM<T>::operator/= (const U& x)
{
  x0 /= x;
  x1 /= x;
  x2 /= x;
  x3 /= x;
  return *this;
}

// no ref versions
template <typename T>
MOM<T>& MOM<T>::operator*= (const double x)
{
  x0 *= x;
  x1 *= x;
  x2 *= x;
  x3 *= x;
  return *this;
}
template <typename T>
MOM<T>& MOM<T>::operator/= (const double x)
{
  x0 /= x;
  x1 /= x;
  x2 /= x;
  x3 /= x;
  return *this;
}
template <typename T>
MOM<T>& MOM<T>::operator*= (const std::complex<double> x)
{
  x0 *= x;
  x1 *= x;
  x2 *= x;
  x3 *= x;
  return *this;
}
template <typename T>
MOM<T>& MOM<T>::operator/= (const std::complex<double> x)
{
  x0 /= x;
  x1 /= x;
  x2 /= x;
  x3 /= x;
  return *this;
}

template <typename T>
template <typename U>
MOM<T> MOM<T>::opplus(MOM<T> m1, const MOM<U>& m2)
{
  m1 += m2;
  return m1;
}

template <typename T>
template <typename U>
MOM<T> MOM<T>::opminus(MOM<T> m1, const MOM<U>& m2)
{
  m1 -= m2;
  return m1;
}

template <typename T>
template <typename U>
T MOM<T>::dotwith(const MOM<U>& m1) const
{
  return x0*m1.x0 - (x1*m1.x1 + x2*m1.x2 + x3*m1.x3);
}

// Non-members

template <typename T>
MOM<T> operator+ (const MOM<T>& m1, const MOM<T>& m2)
{
  return MOM<T>::opplus(m1, m2);
}

template <typename T>
MOM<T> operator- (const MOM<T>& m1, const MOM<T>& m2)
{
  return MOM<T>::opminus(m1, m2);
}

template <typename T, typename U>
MOM<T> operator* (MOM<T> m1, const U& x)
{
  m1 *= x;
  return m1;
}

template <typename T, typename U>
MOM<T> operator* (const U& x, MOM<T> m1)
{
  return m1*x;
}

template <typename T, typename U>
MOM<T> operator/ (MOM<T> m1, const U& x)
{
  m1 /= x;
  return m1;
}

// complex conversion
template <typename T>
MOM<std::complex<T> > operator+ (const MOM<std::complex<T> >& m1, const MOM<T>& m2)
{
  return MOM<std::complex<T> >::opplus(m1, m2);
}
template <typename T>
MOM<std::complex<T> > operator+ (const MOM<T>& m1, const MOM<std::complex<T> >& m2)
{
  return MOM<std::complex<T> >::opplus(m2, m1);
}
template <typename T>
MOM<std::complex<T> > operator- (const MOM<std::complex<T> >& m1, const MOM<T>& m2)
{
  return MOM<std::complex<T> >::opminus(m1, m2);
}
template <typename T>
MOM<std::complex<T> > operator- (const MOM<T>& m1, const MOM<std::complex<T> >& m2)
{
  return MOM<std::complex<T> >::opminus(MOM<std::complex<T> >(m1), m2);
}

template <typename T>
MOM<std::complex<T> > operator* (const MOM<T>& m1, const std::complex<T>& x)
{
  return MOM<std::complex<T> >(m1)*x;
}
template <typename T>
MOM<std::complex<T> > operator* (const std::complex<T>& x, const MOM<T>& m1)
{
  return MOM<std::complex<T> >(m1)*x;
}
template <typename T>
MOM<std::complex<T> > operator/ (const MOM<T>& m1, const std::complex<T>& x)
{
  return MOM<std::complex<T> >(m1)/x;
}

// no ref versions
template <typename T>
MOM<T> operator* (MOM<T> m1, const double x)
{
  m1 *= x;
  return m1;
}
template <typename T>
MOM<T> operator* (const double x, MOM<T> m1)
{
  return m1*x;
}

template <typename T>
MOM<T> operator/ (MOM<T> m1, const double x)
{
  m1 /= x;
  return m1;
}

template <typename T>
MOM<std::complex<T> > operator* (MOM<std::complex<T> > m1, const std::complex<double> x)
{
  m1 *= x;
  return m1;
}
template <typename T>
MOM<std::complex<T> > operator* (const std::complex<double> x, MOM<std::complex<T> > m1)
{
  return m1*x;
}

template <typename T>
MOM<std::complex<T> > operator/ (MOM<std::complex<T> > m1, const std::complex<double> x)
{
  m1 /= x;
  return m1;
}

template <typename T>
MOM<std::complex<T> > conj(const MOM<std::complex<T> >& m)
{
  return m.conj();
}

template <typename T>
T S(const MOM<T>& m)
{
  return m.mass();
}

template <typename T>
T dot(const MOM<T>& m1, const MOM<T>& m2)
{
  return m1.dotwith(m2);
}

template <typename T>
std::complex<T> dot(const MOM<std::complex<T> >& m1, const MOM<T>& m2)
{
  return m1.dotwith(m2);
}

template <typename T>
std::complex<T> dot(const MOM<T>& m1, const MOM<std::complex<T> >& m2)
{
  return m2.dotwith(m1);
}

// spinors

template <typename T>
std::complex<T> spNorm(const MOM<T>& p)
{
  const T pp = p.x0 + p.x2;
  const std::complex<T> pTb = p.x3 - i_*p.x1;
  if (pp > 0.) {
    return sqrt(pp)/pTb;
  } else {
    return std::complex<T>(T(0.), sqrt(-pp))/pTb;
  }
}

template <typename T>
std::complex<T> spNorm(const MOM<std::complex<T> >& p)
{
  const std::complex<T> pp = p.x0 + p.x2;
  const std::complex<T> pTb = p.x3 - i_*p.x1;
  return sqrt(abs(pp))/pTb*exp(0.5*i_*arg(pp));
}

template <typename T>
std::complex<T> spA(const MOM<T>& p1, const MOM<T>& p2)
{
  const T pm1 = p1.x0 - p1.x2;
//   const std::complex<T> pTb1 = p1.x3 - i_*p1.x1;
  const std::complex<T> pTb1(p1.x3, -p1.x1);
  const T pm2 = p2.x0 - p2.x2;
//   const std::complex<T> pTb2 = p2.x3 - i_*p2.x1;
  const std::complex<T> pTb2(p2.x3, -p2.x1);

  return spNorm(p1)*spNorm(p2)*(pm1*pTb2 - pm2*pTb1);
}

template <typename T>
std::complex<T> spB(const MOM<T>& p1, const MOM<T>& p2)
{
  const T pp1 = p1.x0 + p1.x2;
//   const std::complex<T> pTb1 = p1.x3 - i_*p1.x1;
  const std::complex<T> pTb1(p1.x3, -p1.x1);
  const T pp2 = p2.x0 + p2.x2;
//   const std::complex<T> pTb2 = p2.x3 - i_*p2.x1;
  const std::complex<T> pTb2(p2.x3, -p2.x1);

  return (pp1*pTb2 - pp2*pTb1)/(spNorm(p1)*spNorm(p2)*pTb1*pTb2);
}

template <typename T>
std::complex<T> spAB(const MOM<T>& p1, const MOM<T>& p2, const MOM<T>& p3)
{
  const T pm1 = p1.x0 - p1.x2;
//   const std::complex<T> pTb1 = p1.x3 - i_*p1.x1;
  const std::complex<T> pTb1(p1.x3, -p1.x1);
  const T pp2 = p2.x0 + p2.x2;
  const T pm2 = p2.x0 - p2.x2;
//   const std::complex<T> pT2 = p2.x3 + i_*p2.x1;
  const std::complex<T> pT2(p2.x3, p2.x1);
//   const std::complex<T> pTb2 = p2.x3 - i_*p2.x1;
  const std::complex<T> pTb2(p2.x3, -p2.x1);
  const T pp3 = p3.x0 + p3.x2;
//   const std::complex<T> pTb3 = p3.x3 - i_*p3.x1;
  const std::complex<T> pTb3(p3.x3, -p3.x1);

  return (pm1*pp2 - pTb1*pT2 - pp3/pTb3*(pm1*pTb2 - pTb1*pm2))*spNorm(p1)/spNorm(p3);
}

template <typename T>
std::complex<T> spA(const MOM<std::complex<T> >& p1, const MOM<std::complex<T> >& p2)
{
  const std::complex<T> pm1 = p1.x0 - p1.x2;
  const std::complex<T> pTb1 = p1.x3 - i_*p1.x1;
  const std::complex<T> pm2 = p2.x0 - p2.x2;
  const std::complex<T> pTb2 = p2.x3 - i_*p2.x1;

  return spNorm(p1)*spNorm(p2)*(pm1*pTb2 - pm2*pTb1);
}

template <typename T>
std::complex<T> spB(const MOM<std::complex<T> >& p1, const MOM<std::complex<T> >& p2)
{
  const std::complex<T> pp1 = p1.x0 + p1.x2;
  const std::complex<T> pTb1 = p1.x3 - i_*p1.x1;
  const std::complex<T> pp2 = p2.x0 + p2.x2;
  const std::complex<T> pTb2 = p2.x3 - i_*p2.x1;

  return (pp1*pTb2 - pp2*pTb1)/(spNorm(p1)*spNorm(p2)*pTb1*pTb2);
}

template <typename T>
std::complex<T> spAB(const MOM<std::complex<T> >& p1, const MOM<std::complex<T> >& p2, const MOM<std::complex<T> >& p3)
{
  const std::complex<T> pm1 = p1.x0 - p1.x2;
  const std::complex<T> pTb1 = p1.x3 - i_*p1.x1;
  const std::complex<T> pp2 = p2.x0 + p2.x2;
  const std::complex<T> pm2 = p2.x0 - p2.x2;
  const std::complex<T> pT2 = p2.x3 + i_*p2.x1;
  const std::complex<T> pTb2 = p2.x3 - i_*p2.x1;
  const std::complex<T> pp3 = p3.x0 + p3.x2;
  const std::complex<T> pTb3 = p3.x3 - i_*p3.x1;

  return (pm1*pp2 - pTb1*pT2 - pp3/pTb3*(pm1*pTb2 - pTb1*pm2))*spNorm(p1)/spNorm(p3);
}

template <typename T>
MOM<std::complex<T> > CMOM(const MOM<T>& p1, const MOM<T>& p2)
{
  const T pm1 = p1.x0 - p1.x2;
//   const std::complex<T> pTb1 = p1.x3 - i_*p1.x1;
  const std::complex<T> pTb1(p1.x3, -p1.x1);
  const T pp2 = p2.x0 + p2.x2;
//   const std::complex<T> pTb2 = p2.x3 - i_*p2.x1;
  const std::complex<T> pTb2(p2.x3, -p2.x1);
  const std::complex<T> norm = spNorm(p1)/(spNorm(p2)*pTb2);

  const std::complex<T> a0 = (pm1*pTb2 + pTb1*pp2);
  const std::complex<T> a1 = -i_*(pm1*pp2 - pTb1*pTb2);
  const std::complex<T> a2 = -pm1*pTb2 + pTb1*pp2;
  const std::complex<T> a3 = pm1*pp2 + pTb1*pTb2;

  return MOM<std::complex<T> >(a0*norm, a1*norm, a2*norm, a3*norm);
}

template <typename T>
MOM<std::complex<T> > CMOM(const MOM<std::complex<T> >& p1, const MOM<std::complex<T> >& p2)
{
  const std::complex<T> pm1 = p1.x0 - p1.x2;
  const std::complex<T> pTb1 = p1.x3 - i_*p1.x1;
  const std::complex<T> pp2 = p2.x0 + p2.x2;
  const std::complex<T> pTb2 = p2.x3 - i_*p2.x1;
  const std::complex<T> norm = spNorm(p1)/(spNorm(p2)*pTb2);

  const std::complex<T> a0 = (pm1*pTb2 + pTb1*pp2);
  const std::complex<T> a1 = -i_*(pm1*pp2 - pTb1*pTb2);
  const std::complex<T> a2 = -pm1*pTb2 + pTb1*pp2;
  const std::complex<T> a3 = pm1*pp2 + pTb1*pTb2;

  return MOM<std::complex<T> >(a0*norm, a1*norm, a2*norm, a3*norm);
}

// z-spinors
// #define SPIN_AXIS 3
// #define X0 x0
// #define X1 x1
// #define X2 x2
// #define X3 x3

// y-spinors
#define SPIN_AXIS 2
#define X0 x0
#define X1 x3
#define X2 x1
#define X3 x2

template <typename T>
inline
T pp(const MOM<T>& p)
{
  return p.X0 + p.X3;
}

template <typename T>
inline
T pm(const MOM<T>& p)
{
  return p.X0 - p.X3;
}

template <typename T>
inline
std::complex<T> pTb(const MOM<T>& p)
{
  return std::complex<T>(p.X1, -p.X2);
}

template <typename T>
inline
std::complex<T> pT(const MOM<std::complex<T> >& p)
{
  return p.X1 + std::complex<T>(0.,1.)*p.X2;
}

template <typename T>
inline
std::complex<T> pT(const MOM<T>& p)
{
  return std::complex<T>(p.X1, p.X2);
}

template <typename T>
inline
std::complex<T> pTb(const MOM<std::complex<T> >& p)
{
  return p.X1 - std::complex<T>(0.,1.)*p.X2;
}

template <typename T>
inline
std::complex<T> spNormA(const MOM<T>& p)
{
  const T ppl = pp(p);
  if (ppl > 0.) {
    return sqrt(ppl);
  }
  else {
    return std::complex<T>(0., sqrt(-ppl));
//     return -sqrt(-ppl);
  }
}

template <typename T>
inline
std::complex<T> spNormB(const MOM<T>& p)
{
  const T ppl = pp(p);
  if (ppl > 0.) {
    return pTb(p)/sqrt(ppl);
  }
  else {
    return std::complex<T>(-p.X2/sqrt(-ppl), -p.X1/sqrt(-ppl)); // pTb(p)/std::complex<T>(0., sqrt(-ppl));
//     return -pTb(p)/sqrt(-ppl);
  }
}

template <typename T>
inline
std::complex<T> spNormA(const MOM<std::complex<T> >& p)
{
  return pTb(p)/sqrt(abs(pp(p)));
//  return sqrt(abs(pp(p)));
}

template <typename T>
inline
std::complex<T> spNormB(const MOM<std::complex<T> >& p)
{
  return pTb(p)/sqrt(abs(pp(p)));
/*
  // pseudo antisymmetric normalisation
  if (real(p.x0) != 0.) {
    return (real(p.x0) > 0.) ? sqrt(abs(pp(p))) : -sqrt(abs(pp(p)));
  } else {
    return (imag(p.x0) > 0.) ? sqrt(abs(pp(p))) : -sqrt(abs(pp(p)));
  }
*/
}

#undef X0
#undef X1
#undef X2
#undef X3

template <typename T>
std::complex<T> xspA(const MOM<T>& p, const MOM<T>& k)
{
  return spNormA(k)/spNormB(p)*(pm(p) - pm(k)*pTb(p)/pTb(k));
}

template <typename T>
std::complex<T> xspB(const MOM<T>& p, const MOM<T>& k)
{
  return spNormB(k)/spNormA(p)*(pp(p) - pp(k)*pTb(p)/pTb(k));
}

template <typename T>
std::complex<T> xspBA(const MOM<T>& p1, const MOM<T>& p2, const MOM<T>& p3)
{
  const std::complex<T> pTbp1 = pTb(p1);
  const std::complex<T> pTbp3 = pTb(p3);
  return spNormA(p3)/(spNormA(p1)*pTbp3)*(
     pm(p3)*(pp(p2)*pTbp1 - pp(p1)*pTb(p2))
    +pTbp3*(pm(p2)*pp(p1) - pT(p2)*pTbp1)
  );
}

template <typename T>
std::complex<T> xspAB(const MOM<T>& p1, const MOM<T>& p2, const MOM<T>& p3)
{
  const std::complex<T> pTbp3 = pTb(p3);
  const T ppp3 = pp(p3);
  return spNormB(p3)/(spNormB(p1)*pTbp3)*(
     pm(p1)*(pp(p2)*pTbp3 - ppp3*pTb(p2))
    +pTb(p1)*(pm(p2)*ppp3 - pT(p2)*pTbp3)
  );
}

template <typename T>
std::complex<T> xspA(const MOM<std::complex<T> >& p, const MOM<std::complex<T> >& k)
{
  return spNormA(k)/spNormB(p)*(pm(p) - pm(k)*pTb(p)/pTb(k));
}

template <typename T>
std::complex<T> xspB(const MOM<std::complex<T> >& p, const MOM<std::complex<T> >& k)
{
  return spNormB(k)/spNormA(p)*(pp(p) - pp(k)*pTb(p)/pTb(k));
}

template <typename T>
std::complex<T> xspBA(const MOM<std::complex<T> >& p1, const MOM<std::complex<T> >& p2, const MOM<std::complex<T> >& p3)
{
  const std::complex<T> pTbp1 = pTb(p1);
  const std::complex<T> pTbp3 = pTb(p3);
  return spNormA(p3)/(spNormA(p1)*pTbp3)*(
     pm(p3)*(pp(p2)*pTbp1 - pp(p1)*pTb(p2))
    +pTbp3*(pm(p2)*pp(p1) - pT(p2)*pTbp1)
  );
}

template <typename T>
std::complex<T> xspAB(const MOM<std::complex<T> >& p1, const MOM<std::complex<T> >& p2, const MOM<std::complex<T> >& p3)
{
  const std::complex<T> pTbp3 = pTb(p3);
  const std::complex<T> ppp3 = pp(p3);
  return spNormB(p3)/(spNormB(p1)*pTbp3)*(
     pm(p1)*(pp(p2)*pTbp3 - ppp3*pTb(p2))
    +pTb(p1)*(pm(p2)*ppp3 - pT(p2)*pTbp3)
  );
}
