/*
* ngluon2/AmpValue.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef NGLUON2_AMPVALUE_H
#define NGLUON2_AMPVALUE_H

#include "EpsTriplet.h"

template <typename T>
class AmpValue
{
  template<typename> friend class AmpValue;
  public:
    AmpValue() : data() {}

    const EpsTriplet<T>& tree() const { return data[TREE]; }
    const EpsTriplet<T>& loop() const { return data[LOOP]; }
    const EpsTriplet<T>& loopcc() const { return data[LOOPCC]; }
    const EpsTriplet<T>& looprat() const { return data[LOOPR]; }
    const EpsTriplet<T>& box() const { return data[BOX]; }
    const EpsTriplet<T>& tri() const { return data[TRI]; }
    const EpsTriplet<T>& bub() const { return data[BUB]; }
    const EpsTriplet<T>& boxR() const { return data[BOXR]; }
    const EpsTriplet<T>& triR() const { return data[TRIR]; }
    const EpsTriplet<T>& bubR() const { return data[BUBR]; }
    const EpsTriplet<T>& MassRenorm() const { return data[MASSR]; }

    EpsTriplet<T>& tree() { return data[TREE]; }
    EpsTriplet<T>& box() { return data[BOX]; }
    EpsTriplet<T>& tri() { return data[TRI]; }
    EpsTriplet<T>& bub() { return data[BUB]; }
    EpsTriplet<T>& boxR() { return data[BOXR]; }
    EpsTriplet<T>& triR() { return data[TRIR]; }
    EpsTriplet<T>& bubR() { return data[BUBR]; }
    EpsTriplet<T>& MassRenorm() { return data[MASSR]; }

    void update() {
      data[LOOP] =   (data[BOX] + data[BOXR])
                   + (data[TRI] + data[TRIR])
                   + (data[BUB] + data[BUBR]) + data[MASSR];
      data[LOOPCC] = data[BOX] + data[TRI] + data[BUB];
      data[LOOPR] = data[BOXR] + data[TRIR] + data[BUBR];
    }

    AmpValue& operator+= (const AmpValue& a1) {
      for (int i=0; i<LEN; i++) {
        data[i] += a1.data[i];
      }
      return *this;
    }

    AmpValue& operator-= (const AmpValue& a1) {
      for (int i=0; i<LEN; i++) {
        data[i] -= a1.data[i];
      }
      return *this;
    }

    AmpValue& operator*= (const T& x) {
      for (int i=0; i<LEN; i++) {
        data[i] *= x;
      }
      return *this;
    }

    AmpValue& operator/= (const T& x) {
      for (int i=0; i<LEN; i++) {
        data[i] /= x;
      }
      return *this;
    }

    template <typename F>
    AmpValue<typename F::RType> apply(const F& func) const {
      AmpValue<typename F::RType> amp;
      for (int i=0; i<LEN; i++) {
        amp.data[i] = data[i].apply(func);
      }
      return amp;
    }

    bool operator< (const AmpValue& amp) {
      return loop().get0().real() < amp.loop().get0().real();
    }

  private:
    enum ELEMENTS {TREE=0,
                   LOOP, LOOPCC, LOOPR,
                   BOX, TRI, BUB,
                   BOXR, TRIR, BUBR, MASSR, LEN};
    EpsTriplet<T> data[LEN];
};

template <typename T>
AmpValue<T> operator+ (AmpValue<T> a1, const AmpValue<T>& a2)
{
  a1 += a2;
  return a1;
}

template <typename T>
AmpValue<T> operator- (AmpValue<T> a1, const AmpValue<T>& a2)
{
  a1 -= a2;
  return a1;
}

template <typename T, typename U>
AmpValue<T> operator* (AmpValue<T> a1, const U& x)
{
  a1 *= T(x);
  return a1;
}

template <typename T, typename U>
AmpValue<T> operator* (const U& x, const AmpValue<T>& a1)
{
  return a1*x;
}

template <typename T, typename U>
AmpValue<T> operator/ (AmpValue<T> a1, const U& x)
{
  a1 /= T(x);
  return a1;
}

#ifdef VC_DECLARE_ALLOCATOR_T
  VC_DECLARE_ALLOCATOR_T(AmpValue)
#endif

#endif /* NGLUON2_AMPVALUE_H */
