/*
* ngluon2/SubCurrent.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef NGLUON2_SUBCURRENT_H
#define NGLUON2_SUBCURRENT_H

#include <iostream>

#include "utility.h"
#include "Mom.h"

template <typename T>
class SubCurrent
{
  public:
    typedef std::complex<T> complex;
    typedef T real;
    typedef typename call_traits<complex>::param_type complex_param;
    typedef typename call_traits<real>::param_type real_param;

    static const SubCurrent USC;

    SubCurrent();
    SubCurrent(const int type_,
               complex_param j0, complex_param j1,
               complex_param j2, complex_param j3);

    template <typename U>
    SubCurrent(const MOM<U>& mom);

    SubCurrent operator- () const;
    SubCurrent conj() const;
    SubCurrent operator+= (const SubCurrent& J1);
    SubCurrent operator-= (const SubCurrent& J1);

    template <typename U>
    SubCurrent operator*= (U x);

    template <typename U>
    SubCurrent operator/= (U x);

    // this function will need to know about all type combinations:
    static complex Contract(const SubCurrent& J1, const SubCurrent& J2);
    template <typename U>
    static complex Contract(const MOM<U>& P1, const SubCurrent& J2);
    template <typename U>
    static complex Contract(const SubCurrent& J2, const MOM<U>& P1);

    static SubCurrent ContractSlash(const SubCurrent& J1, const SubCurrent& J2);
    static SubCurrent ContractMu(const SubCurrent& J1, const SubCurrent& J2);

    template <typename U>
    friend std::ostream& operator<< (std::ostream& stream, const SubCurrent<U>& J1);

    const complex& data(int i) const { return J[i]; }

  private:
    complex J[4];

  public:
    int type; // e.g. 3 - MOM, 1 - v, (-1) - U=ub, 2 - Scalar
    int flav;
};

template <typename T>
class SubCurrent<std::complex<T> >;

template <typename T>
SubCurrent<T> operator+ (SubCurrent<T> J1, const SubCurrent<T>& J2);

template <typename T>
SubCurrent<T> operator- (SubCurrent<T> J1, const SubCurrent<T>& J2);

template <typename T, typename U>
SubCurrent<T> operator* (SubCurrent<T> J1, const U& x);

template <typename T, typename U>
SubCurrent<T> operator* (SubCurrent<T> J1, const std::complex<U>& x);

template <typename T, typename U>
SubCurrent<T> operator* (const U& x, const SubCurrent<T>& J1)
{
  return J1*x;
}

template <typename T, typename U>
SubCurrent<T> operator* (const std::complex<U>& x, const SubCurrent<T>& J1)
{
  return J1*x;
}

template <typename T, typename U>
SubCurrent<T> operator/ (SubCurrent<T> J1, const U& x);

template <typename T, typename U>
SubCurrent<T> operator/ (SubCurrent<T> J1, const std::complex<U>& x);


template <typename T>
SubCurrent<T> EPS(int h, const MOM<T>& p, const MOM<T>& x);
template <typename T>
SubCurrent<T> EPS(int h, const MOM<std::complex<T> >& p, const MOM<T>& x);
template <typename T>
SubCurrent<T> EPS(int h, const MOM<std::complex<T> >& p, const MOM<std::complex<T> >& x);

template <typename T>
SubCurrent<T> uspn(int h, const MOM<T>& p);
template <typename T>
SubCurrent<T> Uspn(int h, const MOM<T>& p);
template <typename T>
SubCurrent<T> vspn(int h, const MOM<T>& p);
template <typename T>
SubCurrent<T> Vspn(int h, const MOM<T>& p);

template <typename T>
SubCurrent<T> uspn(int h, const MOM<std::complex<T> >& p);
template <typename T>
SubCurrent<T> Uspn(int h, const MOM<std::complex<T> >& p);
template <typename T>
SubCurrent<T> vspn(int h, const MOM<std::complex<T> >& p);
template <typename T>
SubCurrent<T> Vspn(int h, const MOM<std::complex<T> >& p);

template <typename T>
SubCurrent<T> uspn(int h, const MOM<T>& p, const MOM<T>& e, T m);
template <typename T>
SubCurrent<T> Uspn(int h, const MOM<T>& p, const MOM<T>& e, T m);
template <typename T>
SubCurrent<T> vspn(int h, const MOM<T>& p, const MOM<T>& e, T m);
template <typename T>
SubCurrent<T> Vspn(int h, const MOM<T>& p, const MOM<T>& e, T m);

template <typename T>
SubCurrent<T> uspn(int h, const MOM<std::complex<T> >& p, const MOM<std::complex<T> >& e, std::complex<T> m);
template <typename T>
SubCurrent<T> Uspn(int h, const MOM<std::complex<T> >& p, const MOM<std::complex<T> >& e, std::complex<T> m);
template <typename T>
SubCurrent<T> vspn(int h, const MOM<std::complex<T> >& p, const MOM<std::complex<T> >& e, std::complex<T> m);
template <typename T>
SubCurrent<T> Vspn(int h, const MOM<std::complex<T> >& p, const MOM<std::complex<T> >& e, std::complex<T> m);

// TODO real ref vector can be cheaper to compute, but it might break complex spinors
template <typename T>
SubCurrent<T> uspn(int h, const MOM<std::complex<T> >& p, const MOM<T>& e, std::complex<T> m)
{
  return uspn(h, p, MOM<std::complex<T> >(e), m);
}

template <typename T>
SubCurrent<T> Uspn(int h, const MOM<std::complex<T> >& p, const MOM<T>& e, std::complex<T> m)
{
  return Uspn(h, p, MOM<std::complex<T> >(e), m);
}

template <typename T>

SubCurrent<T> vspn(int h, const MOM<std::complex<T> >& p, const MOM<T>& e, std::complex<T> m)
{
  return vspn(h, p, MOM<std::complex<T> >(e), m);
}
template <typename T>
SubCurrent<T> Vspn(int h, const MOM<std::complex<T> >& p, const MOM<T>& e, std::complex<T> m)
{
  return Vspn(h, p, MOM<std::complex<T> >(e), m);
}

template <typename T>
std::complex<T> Contract(const SubCurrent<T>& J1, const SubCurrent<T>& J2)
{
  return SubCurrent<T>::Contract(J1, J2);
}

template <typename T, typename U>
std::complex<T> Contract(const SubCurrent<T>& J1, const U& J2)
{
  return SubCurrent<T>::Contract(J1, J2);
}

template <typename T, typename U>
std::complex<T> Contract(const U& J1, const SubCurrent<T>& J2)
{
  return SubCurrent<T>::Contract(J1, J2);
}

template <typename T>
SubCurrent<T> ContractSlash(const SubCurrent<T>& J1, const SubCurrent<T>& J2)
{
  return SubCurrent<T>::ContractSlash(J1, J2);
}

template <typename T>
SubCurrent<T> ContractMu(const SubCurrent<T>& J1, const SubCurrent<T>& J2)
{
  return SubCurrent<T>::ContractMu(J1, J2);
}

#include "SubCurrent.cpp"

#ifdef VC_DECLARE_ALLOCATOR_T
  VC_DECLARE_ALLOCATOR_T(SubCurrent)
#endif

#endif /* NGLUON2_SUBCURRENT_H */
