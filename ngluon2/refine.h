/*
* ngluon2/refine.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012, 2013 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef NGLUON2_REFINE_H
#define NGLUON2_REFINE_H

template <typename SRC, typename DST>
void refine(const int NN, const SRC (*mom)[4], DST (*ddmom)[4], int np=-1, int nq=-1);

#endif // NGLUON2_REFINE_H

#ifdef NGLUON2_MOM_H
#ifndef NGLUON2_REFINE_MOM_H
#define NGLUON2_REFINE_MOM_H

#include <vector>

template <typename SRC, typename DST>
void refine(const int NN, const MOM<SRC>* mom, MOM<DST>* ddmom, int np=-1, int nq=-1);

template <typename SRC, typename DST>
void refine(const std::vector<MOM<SRC> >& mom, std::vector<MOM<DST> >& ddmom, int np=-1, int nq=-1);

template <typename SRC, typename DST, typename SCT>
void refineM(const int NN, const MOM<SRC>* mom, MOM<DST>* ddmom, int numscales2, const SCT* scales2);

template <typename SRC, typename DST, typename SCT>
void refineM(const std::vector<MOM<SRC> >& mom, std::vector<MOM<DST> >& ddmom, const std::vector<SCT>& scales2);

#endif // NGLUON2_REFINE_MOM_H
#endif // NGLUON2_MOM_H
