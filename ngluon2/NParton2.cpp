/*
* ngluon2/NGluon2.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include "NParton2.h"
#include "NGluon2.h"

template <typename T>
NParton2<T>::NParton2()
 : NAmp<T>()
{
  if (vector_traits<T>::isScalar) {
    initNG(SCALEFACTOR_A, 0);
    initNG(SCALEFACTOR_B, 1);
  } else {
    typename vector_traits<T>::scalar scale_arr[4] = {
      SCALEFACTOR_A, SCALEFACTOR_B, SCALEFACTOR_C, SCALEFACTOR_D
    };
    T scale_v;
    for (int i = 0; i < vector_traits<T>::Size; i++) {
      vector_traits<T>::set(scale_v, i, scale_arr[i]);
    }
    initNG(scale_v, 0);
  }
}

template <typename T>
NParton2<T>::~NParton2()
{
  // ngluons is cleared by NAmp
}

template <typename T>
EpsTriplet<typename NParton2<T>::ST> NParton2<T>::eval(const int primtype_, const int* ord/*=0*/)
{
  typename NGluon2<T>::PRIMTYPE primtype = static_cast<typename NGluon2<T>::PRIMTYPE>(primtype_);
  if (vector_traits<T>::isScalar) {
    ngluons[0]->eval(primtype, ord);
    ngluons[1]->eval(primtype, ord);
    AmpValue<T> amp1 = ngluons[0]->lastValue();
    AmpValue<T> amp2 = ngluons[1]->lastValue();
    value = get_average(amp1, amp2);
    error = get_error(amp1, amp2);
  } else {
    ngluons[0]->eval(primtype, ord);
    AmpValue<T> amps = ngluons[0]->lastValue();
    value = get_average(amps);
    error = get_error(amps);
  }
  return value.loop();
}

template <typename T>
complex<typename NParton2<T>::ST> NParton2<T>::evalTree(const int* ord/*=0*/)
{
  return get_average(ngluons[0]->evalTree(ord));
}

template <typename T>
complex<typename NParton2<T>::ST> NParton2<T>::gettree() const
{
  return value.tree().get0();
}

template <typename T>
EpsTriplet<typename NParton2<T>::ST> NParton2<T>::getres() const
{
  return value.loop();
}

template <typename T>
EpsTriplet<typename NParton2<T>::ST> NParton2<T>::geterr() const
{
  return error.loop();
}

template <typename T>
EpsTriplet<typename NParton2<T>::ST> NParton2<T>::getcc(int type) const
{
  switch (type) {
    case 0:
      return value.loopcc();
    case 1:
      return value.box();
    case 2:
      return value.tri();
    case 3:
      return value.bub();
  }
  return EpsTriplet<typename NParton2<T>::ST>();
}

template <typename T>
EpsTriplet<typename NParton2<T>::ST> NParton2<T>::getccerr(int type) const
{
  switch (type) {
    case 0:
      return error.loopcc();
    case 1:
      return error.box();
    case 2:
      return error.tri();
    case 3:
      return error.bub();
  }
  return EpsTriplet<typename NParton2<T>::ST>();
}

template <typename T>
complex<typename NParton2<T>::ST> NParton2<T>::getrat(int type) const
{
  switch (type) {
    case 0:
      return value.looprat().get0();
    case 1:
      return value.boxR().get0();
    case 2:
      return value.triR().get0();
    case 3:
      return value.bubR().get0();
  }
  return complex<ST>();
}

template <typename T>
complex<typename NParton2<T>::ST> NParton2<T>::getraterr(int type) const
{
  switch (type) {
    case 0:
      return error.looprat().get0();
    case 1:
      return error.boxR().get0();
    case 2:
      return error.triR().get0();
    case 3:
      return error.bubR().get0();
  }
  return complex<ST>();
}

template <typename T>
void NParton2<T>::setOrder(const int* ord)
{
  for (unsigned i=0; i<ngluons.size(); i++) {
    ngluons[i]->setOrder(ord);
  }
}

template <typename T>
void NParton2<T>::setOrder(const std::vector<int>& ord)
{
  setOrder(ord.data());
}

template<typename T>
void NParton2<T>::setHeavyQuarkMass(const T m)
{
  for (unsigned i=0; i<ngluons.size(); i++) {
    ngluons[i]->setHeavyQuarkMass(m);
  }
}

#ifdef USE_SD
  template class NParton2<double>;
#endif
#ifdef USE_DD
  template class NParton2<dd_real>;
#endif
#ifdef USE_QD
  template class NParton2<qd_real>;
#endif
#ifdef USE_VC
  template class NParton2<Vc::double_v>;
#endif
