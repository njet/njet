/*
* ngluon2/NGluon2.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef NGLUON2_H
#define NGLUON2_H

#include "Current.h"
#include "CoeffBase.h"
#include "AmpValue.h"

#include "debug.h"

using std::complex;

template <typename T>
class NGluon2
{
  public:
    typedef typename Current<T>::RealMom RealMom;
    typedef typename Current<T>::ComplexMom ComplexMom;

    typedef typename Current<T>::RealFlavour RealFlavour;
    typedef typename Current<T>::RealFlavourList RealFlavourList;
    typedef typename Current<T>::ComplexFlavour ComplexFlavour;

    typedef typename Current<T>::RealParticle RealParticle;
    typedef typename Current<T>::RealParticleList RealParticleList;
    typedef typename Current<T>::ComplexParticle ComplexParticle;

    typedef complex<T>* Coefficients;
    typedef T* Kinematics;
    typedef ComplexMom* Momenta;

    enum PRIMTYPE {MIXED=0, LIGHTQ=1, HEAVYQ=2, NONE=-1};

    NGluon2(const T scalefactor_);

    void setProcess(const int n_, const RealFlavour* flavours);
    void setProcess(const RealFlavourList& flavours);

    template <typename U>
    void setProcess(const int n_, const Flavour<U>* otherflavarr);
    template <typename U>
    void setProcess(const std::vector<Flavour<U> >& otherflavours);

    void setMomenta(const RealMom* moms);
    void setMomenta(const std::vector<RealMom>& moms);

    template <typename U>
    void setMomenta(const MOM<U>* othermoms);
    template <typename U>
    void setMomenta(const std::vector<MOM<U> >& othermoms);

    void setHelicity(const int* helicity);
    void setHelicity(const std::vector<int>& helicity);

    void setOrder(const int* ord);
    void setOrder(const std::vector<int>& ord);

    void setMuR2(const T rscale_);

    void setHeavyQuarkMass(const T m);

    LoopResult<T> eval(const PRIMTYPE primtype_, const int* ord=0, bool cconly=false);
    complex<T> evalTree(const int* ord=0);

    const AmpValue<T>& lastValue() const { return value; }
    const AmpValue<T>& lastValueC() const { return valueC; }

    const RealParticle& getExtParticle(int i) const { return trees.BaseProcess()[i]; }
    int Nexternal() const { return trees.Nexternal(); }

  private:
    const RealMom& PropMoms(int i, int j) const { return trees.PropMoms(i, j); }
    void initialize(const int n_, const RealFlavour* flavours);
    void clearCoefficients();

    void fillPropMomsSq();
    void fillPropFlavours(const RealFlavour& l0);
    void initonshell();

    void setLoopMass(const complex<T>& mDsq);

    void fillOnShell();
    int& isOnShell(int i0, int i1) { return isonshell[i0 + i1*(i1-1)/2]; }
    int isOnShell(int i0, int i1) const { return isonshell[i0 + i1*(i1-1)/2]; }

    void MassRenormalizeH(const T mH);

    // debugging stuff
    void print();

    // member variables
    int n;
    const T scalefactor;
    T amprescale;

    T MuR2;
    PRIMTYPE primtype;
    int validity;

    Current<T> trees;
    PMatrix<T> PropMomsSq;
    std::vector<int> isonshell;
    std::vector<int> PropSpin, PropSpinD;
    std::vector<RealFlavour> propFlavour;
    std::vector<ComplexFlavour> propFlavourD;

    Coeff_Base<complex<T> > box, tri, bub, pent, boxR, triR, bubR;
    Coeff_Base<T> boxK, triK, bubK;
    Coeff_Base<complex<T> > pentMU, boxMU, triMU, bubMU;
    Coeff_Base<ComplexMom> boxM, triM, bubM, pentM, boxRM, triRM, bubRM;

    AmpValue<T> value;
    AmpValue<T> valueC;

    complex<T> Integrand4(const ComplexMom& l1, int i0, int i1, int i2, int i3);
    complex<T> Integrand3(const ComplexMom& l1, int i0, int i1, int i2);

    complex<T> DIntegrand5(const ComplexMom& l1, const complex<T>& MUsq, int i0, int i1, int i2, int i3, int i4);
    complex<T> DIntegrand4(const ComplexMom& l1, const complex<T>& MUsq, int i0, int i1, int i2, int i3);
    complex<T> DIntegrand3(const ComplexMom& l1, const complex<T>& MUsq, int i0, int i1, int i2);

    complex<T> subtract3(const ComplexMom& l1, int i0, int i1, int i2, int skip=0);
    complex<T> subtract2(const ComplexMom& l1, int i0, int i1);

    complex<T> Dsubtract4(const ComplexMom& l1, const complex<T>& MUsq, int i0, int i1, int i2, int i3, int skip=0);
    complex<T> Dsubtract3(const ComplexMom& l1, const complex<T>& MUsq, int i0, int i1, int i2, int skip=0);
    complex<T> Dsubtract2(const ComplexMom& l1, const complex<T>& MUsq, int i0, int i1);

    void calculate_boxes();
    void calculate_triangles();
    void calculate_bubbles();

    void calculate_pentagons();
    void calculate_boxesR();
    void calculate_trianglesR();
    void calculate_bubblesR();

    static const ComplexMom XiRef;
    static const T PI;
};

#ifdef VC_DECLARE_ALLOCATOR_T
  VC_DECLARE_ALLOCATOR_T(NGluon2)
#endif

#endif /* NGLUON2_H */
