/*
* ngluon2/utility.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef NGLUON2_UTILITY_H
#define NGLUON2_UTILITY_H

#include <complex>

#include "../common.h"

#define i_ (std::complex<double>(0., 1.))

#define SCALEFACTOR_A double(1.)
#define SCALEFACTOR_B double(1.2857142857142855)
#define SCALEFACTOR_C double(1.1290825271086293)
#define SCALEFACTOR_D double(0.8136237542381588)

#include <cstdlib>
#include <cmath>

#define NJET_ERROR(msg) \
  std::cout << "NJet error at " << __FILE__ << ":" << __LINE__ \
            << " " << msg << std::endl; \
  exit(1);

#define NJET_WARN(msg) \
  std::cout << "NJet warning at " << __FILE__ << ":" << __LINE__ \
            << " " << msg << std::endl;

using std::abs;
using std::pow;
using std::sqrt;

// complex <> real properties
template <typename T>
struct complex_traits
{
  typedef std::complex<T> complex;
  typedef T real;
};

template <typename T>
struct complex_traits<std::complex<T> >
{
  typedef std::complex<T> complex;
  typedef T real;
};

// vector properties
template <typename T>
struct vector_traits
{
  typedef T scalar;
  static inline void set(T& v, const int /*i*/, const T x) { v = x; }
  static const int Size = 1;
  static const bool isScalar = true;
};

// numeric constants
template <typename T>
struct constant_traits
{
  static T pi() {
    static const T _pi = 4.*atan(T(1.));
    return _pi;
  }
  static T rt2() {
    static const T _rt2 = sqrt(T(2.));
    return _rt2;
  }
};

// simple replacement of <boost/call_traits.hpp> for double, etc
template <typename T>
struct call_traits
{
  typedef const T& param_type;
};

template <>
struct call_traits<double>
{
  typedef const double param_type;
};

template <>
struct call_traits<std::complex<double> >
{
  typedef const std::complex<double> param_type;
};

// for generic conversion to work
inline double to_double(const double x)
{
  return x;
}

template <typename T>
T njet_pow(T x, const int m)
{
  unsigned int n = m < 0 ? -m : m;
  T y = n % 2 ? x : T(1.);
  while (n >>= 1) {
    x = x*x;
    if (n % 2) {
      y = y*x;
    }
  }
  return m < 0 ? typename complex_traits<T>::real(1.)/y : y;
}

// ---------------------------------------------------------------------------
//   dd_real
// ---------------------------------------------------------------------------

#ifdef USE_DD
#include <qd/qd_real.h>

template <>
struct constant_traits<dd_real>
{
  static dd_real pi() {
    return dd_real::_pi;
  }
  static dd_real rt2() {
    static const dd_real _rt2 = sqrt(dd_real(2.));
    return _rt2;
  }
};

inline
std::complex<dd_real> operator* (const double x, const std::complex<dd_real>& y)
{
  return std::complex<dd_real>(x*y.real(), x*y.imag());
}

inline
std::complex<dd_real> operator* (const std::complex<dd_real>& y, const double x)
{
  return x*y;
}

inline
std::complex<dd_real> operator* (const std::complex<double> x, const std::complex<dd_real>& y)
{
  return std::complex<dd_real>(x.real()*y.real()-x.imag()*y.imag(), x.real()*y.imag()+x.imag()*y.real());
}

inline
std::complex<dd_real> operator* (const std::complex<dd_real>& y, const std::complex<double> x)
{
  return x*y;
}

inline
std::complex<dd_real> operator/ (const double x, const std::complex<dd_real>& y)
{
  return dd_real(x)/y;
}

inline
std::complex<dd_real> operator/ (const std::complex<dd_real>& y, const double x)
{
  return std::complex<dd_real>(y.real()/x, y.imag()/x);
}

inline
std::complex<dd_real> operator/ (const std::complex<double> x, const dd_real& y)
{
  return std::complex<dd_real>(x.real()/y, x.imag()/y);
}

inline
std::complex<dd_real> operator/ (const std::complex<double> x, const std::complex<dd_real>& y)
{
  return std::complex<dd_real>(dd_real(x.real()), dd_real(x.imag()))/y;
}

inline std::complex<dd_real> pow(std::complex<dd_real> x, const int m) { return njet_pow(x, m); }

#endif /* USE_DD */

// ---------------------------------------------------------------------------
//   qd_real
// ---------------------------------------------------------------------------

#ifdef USE_QD
#include <qd/qd_real.h>

template <>
struct constant_traits<qd_real>
{
  static qd_real pi() {
    return qd_real::_pi;
  }
  static qd_real rt2() {
    static const qd_real _rt2 = sqrt(qd_real(2.));
    return _rt2;
  }
};

inline
std::complex<qd_real> operator* (const double x, const std::complex<qd_real>& y)
{
  return std::complex<qd_real>(x*y.real(), x*y.imag());
}

inline
std::complex<qd_real> operator* (const std::complex<qd_real>& y, const double x)
{
  return x*y;
}

inline
std::complex<qd_real> operator* (const std::complex<double> x, const std::complex<qd_real>& y)
{
  return std::complex<qd_real>(x.real()*y.real()-x.imag()*y.imag(), x.real()*y.imag()+x.imag()*y.real());
}

inline
std::complex<qd_real> operator* (const std::complex<qd_real>& y, const std::complex<double> x)
{
  return x*y;
}

inline
std::complex<qd_real> operator/ (const double x, const std::complex<qd_real>& y)
{
  return qd_real(x)/y;
}

inline
std::complex<qd_real> operator/ (const std::complex<qd_real>& y, const double x)
{
  return std::complex<qd_real>(y.real()/x, y.imag()/x);
}

inline
std::complex<qd_real> operator/ (const std::complex<double> x, const qd_real& y)
{
  return std::complex<qd_real>(x.real()/y, x.imag()/y);
}

inline
std::complex<qd_real> operator/ (const std::complex<double> x, const std::complex<qd_real>& y)
{
  return std::complex<qd_real>(qd_real(x.real()), qd_real(x.imag()))/y;
}

inline std::complex<qd_real> pow(std::complex<qd_real> x, const int m) { return njet_pow(x, m); }

#endif /* USE_QD */

// ---------------------------------------------------------------------------
//   Vc::double_v
// ---------------------------------------------------------------------------

#ifdef USE_VC

#ifndef VC_IMPL
  #define VC_IMPL SSE
#endif

#include <Vc/Vc>
#include <Vc/IO>
#include <Vc/Allocator>

VC_DECLARE_ALLOCATOR(std::complex<Vc::double_v>)

#define VC_DECLARE_ALLOCATOR_T(Type) \
namespace std \
{ \
  template <typename T> \
  class allocator<Type<T> > : public ::Vc::Allocator<Type<T> > \
  { \
    public: \
      template <typename U> struct rebind { typedef ::std::allocator<U> other; }; \
  }; \
}

// namespace std
// {
// template <>
std::complex<Vc::double_v> sqrt(const std::complex<Vc::double_v>& z);
Vc::double_v acos(const Vc::double_v& x);
inline Vc::double_v pow(Vc::double_v x, const int m) { return njet_pow(x, m); }
inline std::complex<Vc::double_v> pow(std::complex<Vc::double_v> x, const int m)  { return njet_pow(x, m); }
// }

int checkVcStatus();

template <>
struct vector_traits<Vc::double_v>
{
  typedef double scalar;
  static inline void set(Vc::double_v& v, const int i, const double x) { v[i] = x; }
  static const int Size = Vc::double_v::Size;
  static const bool isScalar = false;
};

template <>
struct constant_traits<Vc::double_v>
{
  static inline Vc::double_v pi() { return Vc::double_v(constant_traits<double>::pi()); }
  static inline Vc::double_v rt2() { return Vc::double_v(constant_traits<double>::rt2()); }
};

inline
std::complex<Vc::double_v> operator* (const double x, const std::complex<Vc::double_v>& y)
{
  return std::complex<Vc::double_v>(x*y.real(), x*y.imag());
}

inline
std::complex<Vc::double_v> operator* (const std::complex<Vc::double_v>& y, const double x)
{
  return x*y;
}

inline
std::complex<Vc::double_v> operator* (const std::complex<double> x, const std::complex<Vc::double_v>& y)
{
  return std::complex<Vc::double_v>(x.real()*y.real()-x.imag()*y.imag(), x.real()*y.imag()+x.imag()*y.real());
}

inline
std::complex<Vc::double_v> operator* (const std::complex<Vc::double_v>& y, const std::complex<double> x)
{
  return x*y;
}

inline
std::complex<Vc::double_v> operator/ (const double x, const std::complex<Vc::double_v>& y)
{
  return Vc::double_v(x)/y;
}

inline
std::complex<Vc::double_v> operator/ (const std::complex<Vc::double_v>& y, const double x)
{
  return std::complex<Vc::double_v>(y.real()/x, y.imag()/x);
}

inline
std::complex<Vc::double_v> operator/ (const std::complex<double> x, const Vc::double_v& y)
{
  return std::complex<Vc::double_v>(x.real()/y, x.imag()/y);
}

inline
std::complex<Vc::double_v> operator/ (const std::complex<double> x, const std::complex<Vc::double_v>& y)
{
  return std::complex<Vc::double_v>(Vc::double_v(x.real()), Vc::double_v(x.imag()))/y;
}

#endif /* USE_VC */

// ---------------------------------------------------------------------------
//   Vc interoperability
// ---------------------------------------------------------------------------

template <typename T>
T get_average(const T x)
{
  return x;
}

template <typename T>
T get_error(const T /*x*/)
{
  return T();
}

template <typename T>
T get_average(const T x, const T y)
{
  return 0.5*(x + y);
}

template <typename T>
T get_error(const T x, const T y)
{
  return x - y;
}

#ifdef USE_VC

template <template<typename> class X, template<typename,typename,typename> class F, typename T, typename A, typename R>
X<R> apply_to_obj(const X<T>& obj, const F<T,A,R>& func) {
  return obj.apply(func);
}

template <template<typename,typename,typename> class F, typename T, typename A, typename R>
std::complex<R> apply_to_obj(const std::complex<T>& obj, const F<T,A,R>& func) {
  return std::complex<R>(func(obj.real()), func(obj.imag()));
}

template <typename T, typename A, typename R>
class FApply1
{
  public:
    typedef R RType;
    FApply1(const A arg1_) : arg1(arg1_) {}
    R operator()(const T /*x*/) const {
      return R();
    }
    const A arg1;
};

template <typename T, typename A, typename R>
class FApply1_rot : public FApply1<T,A,R>
{
  public:
    using FApply1<T,A,R>::arg1;
    FApply1_rot(const A arg1_) : FApply1<T,A,R>(arg1_) {}
    R operator()(const T x) const {
      return x.rotated(arg1);
    }
};

template <typename T, typename A, typename R>
class FApply1_sub : public FApply1<T,A,R>
{
  public:
    using FApply1<T,A,R>::arg1;
    FApply1_sub(const A arg1_) : FApply1<T,A,R>(arg1_) {}
    R operator()(const T x) const {
      return x[arg1];
    }
};

inline
double get_average(const Vc::double_v x)
{
  double sum = double();
  for (int i=0; i<Vc::double_v::Size; i++) {
    sum += x[i];
  }
  return sum/double(Vc::double_v::Size);
}

inline
double get_error(const Vc::double_v x)
{
  Vc::double_v diff[Vc::double_v::Size] = {};
  for (int i=1; i<Vc::double_v::Size; i++) {
    diff[i] = x - x.rotated(i);
  }
  double err = double();
  for (int i=1; i<Vc::double_v::Size; i++) {
    for (int j=0; j<Vc::double_v::Size; j++) {
      const double ampi = diff[i][j];
      if (err < ampi) {
        err = ampi;
      }
    }
  }
  return err;
}

inline
std::complex<double> get_error(const std::complex<Vc::double_v> x)
{
  return std::complex<double>(get_error(x.real()), get_error(x.imag()));
}

template <template<typename> class T>
T<double> get_average(const T<Vc::double_v> x)
{
  T<double> sum = T<double>();
  for (int i=0; i<Vc::double_v::Size; i++) {
    sum += apply_to_obj(x, FApply1_sub<Vc::double_v,int,double>(i));
  }
  return sum/double(Vc::double_v::Size);
}

template <template<typename> class T>
T<double> get_error(const T<Vc::double_v> x)
{
  T<Vc::double_v> diff[Vc::double_v::Size] = {};
  for (int i=1; i<Vc::double_v::Size; i++) {
    diff[i] = x - apply_to_obj(x, FApply1_rot<Vc::double_v,int,Vc::double_v>(i));
  }
  T<double> err = T<double>();
  for (int i=1; i<Vc::double_v::Size; i++) {
    for (int j=0; j<Vc::double_v::Size; j++) {
      const T<double> ampi = apply_to_obj(diff[i], FApply1_sub<Vc::double_v,int,double>(j));
      if (err < ampi) {
        err = ampi;
      }
    }
  }
  return err;
}

inline
double get_average(const Vc::double_v /*x*/, const Vc::double_v /*y*/)
{
  assert(0);
  return double();
}

inline
double get_error(const Vc::double_v /*x*/, const Vc::double_v /*y*/)
{
  assert(0);
  return double();
}

template <template<typename> class T>
T<double> get_average(const T<Vc::double_v> /*x*/, const T<Vc::double_v> /*y*/)
{
  assert(0);
  return T<double>();
}

template <template<typename> class T>
T<double> get_error(const T<Vc::double_v> /*x*/, const T<Vc::double_v> /*y*/)
{
  assert(0);
  return T<double>();
}

#endif /* USE_VC */

#endif /* NGLUON_UTILITY_H */
