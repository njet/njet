/*
* ngluon2/NGluon2-subtract.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include "NGluon2.h"


template<typename T>
complex<T> NGluon2<T>::Integrand4(const ComplexMom& l1,
                                  int i0, int i1, int i2, int i3)
{
  const Coefficients c = box(i0, i1, i2, i3);
  const Momenta w = boxM(i0, i1, i2, i3);
  const complex<T> ISP1 = 2.*dot(l1, w[1]);

  const complex<T> ans = c[0] + c[1]*ISP1;
  return ans;
}

template<typename T>
complex<T> NGluon2<T>::Integrand3(const ComplexMom& l1,
                                  int i0, int i1, int i2)
{
  const Coefficients c = tri(i0, i1, i2);
  const Momenta w = triM(i0, i1, i2);
  const complex<T> ISP1 = 2.*dot(l1, w[3]);
  const complex<T> ISP2 = 2.*dot(l1, w[4]);

  const complex<T> ans = c[0] + c[1]*ISP1 + c[2]*ISP2
         + c[3]*(ISP1*ISP1 - ISP2*ISP2)
         + c[4]*ISP1*ISP2
         + c[5]*ISP1*ISP1*ISP2
         + c[6]*ISP1*ISP2*ISP2;
  return ans;
}

template<typename T>
complex<T> NGluon2<T>::DIntegrand5(const ComplexMom& /*l1*/, const complex<T>& MUsq,
                                   int i0, int i1, int i2, int i3, int i4)
{
  const Coefficients c = pent(i0, i1, i2, i3, i4);
  const complex<T> ans = c[0]*MUsq;
  return ans;
}

template<typename T>
complex<T> NGluon2<T>::DIntegrand4(const ComplexMom& l1, const complex<T>& MUsq,
                                   int i0, int i1, int i2, int i3)
{
  const Coefficients c = boxR(i0, i1, i2, i3);
  const Momenta w = boxM(i0, i1, i2, i3);
  const complex<T> ISP1 = 2.*dot(l1, w[1]);

  const complex<T> ans = c[0] + c[1]*ISP1 + MUsq*((c[2] + c[3]*ISP1) + MUsq*c[4]);
  return ans;
}

template<typename T>
complex<T> NGluon2<T>::DIntegrand3(const ComplexMom& l1, const complex<T>& MUsq,
                                   int i0, int i1, int i2)
{
  const Coefficients c = triR(i0, i1, i2);
  const Momenta w = triM(i0, i1, i2);
  const complex<T> ISP1 = 2.*dot(l1, w[3]);
  const complex<T> ISP2 = 2.*dot(l1, w[4]);

  const complex<T> ans = c[0] + c[1]*ISP1 + c[2]*ISP2
         + c[3]*(ISP1*ISP1 - ISP2*ISP2)
         + c[4]*ISP1*ISP2
         + c[5]*ISP1*ISP1*ISP2
         + c[6]*ISP1*ISP2*ISP2
         + MUsq*(
            + c[7]*ISP1
            + c[8]*ISP2
            + c[9]
         );
  return ans;
}

/* Throughout the subtractions we use the off-shell form of the propagator
 * P^2-m^2. This is because the subtractions are isolated from each other
 * so we do not know whether "l1" is on-shell or not. It may be beneficial
 * to keep track of this in future as dot(l1, P)+S(P) may be more accurate. */

template<typename T>
complex<T> NGluon2<T>::Dsubtract4(const ComplexMom& l1, const complex<T>& MUsq,
                                  int i0, int i1, int i2, int i3, int skip)
{
  complex<T> ans = T(0.);
  complex<T> prop;
  T m;

  if (n < 5) {
    return T(0.);
  }

  if (skip < 1) {
    for (int k=0; k<i0; k++) {
      const RealMom& P1 = PropMoms(k, i0-1);
      const RealMom& P2 = PropMoms(i0, i1-1);
      const RealMom P = P1 + P2;
      m = propFlavour[k].Mass();
      prop = S(l1+P) - (MUsq+m*m);
      ans += DIntegrand5(l1+P2, MUsq, k, i0, i1, i2, i3)/prop;
    }
  }
  if (skip < 2) {
    for (int k=i0+1; k<i1; k++) {
      const RealMom& P = PropMoms(k, i1-1);
      m = propFlavour[k].Mass();
      prop = S(l1+P) - (MUsq+m*m);
      ans += DIntegrand5(l1+P, MUsq, i0, k, i1, i2, i3)/prop;
    }
  }
  if (skip < 3) {
    for (int k=i1+1; k<i2; k++) {
      const RealMom& P = PropMoms(i1, k-1);
      m = propFlavour[k].Mass();
      prop = S(l1-P) - (MUsq+m*m);
      ans += DIntegrand5(l1, MUsq, i0, i1, k, i2, i3)/prop;
    }
  }
  if (skip < 4) {
    for (int k=i2+1; k<i3; k++) {
      const RealMom& P = PropMoms(i1, k-1);
      m = propFlavour[k].Mass();
      prop = S(l1-P) - (MUsq+m*m);
      ans += DIntegrand5(l1, MUsq, i0, i1, i2, k, i3)/prop;
    }
  }
  if (skip < 5) {
    for (int k=i3+1; k<n; k++) {
      const RealMom& P = PropMoms(i1, k-1);
      m = propFlavour[k].Mass();
      prop = S(l1-P) - (MUsq+m*m);
      ans += DIntegrand5(l1, MUsq, i0, i1, i2, i3, k)/prop;
    }
  }
  return ans;
}

template<typename T>
complex<T> NGluon2<T>::Dsubtract3(const ComplexMom& l1, const complex<T>& MUsq,
                                  int i0, int i1, int i2, int skip)
{
  complex<T> ans = T(0.);
  complex<T> prop;
  T m;

  if (skip < 1) {
    for (int k=0; k<i0; k++) {
      const RealMom& P1 = PropMoms(k, i0-1);
      const RealMom& P2 = PropMoms(i0, i1-1);
      const RealMom P = P1 + P2;
      m = propFlavour[k].Mass();
      prop = S(l1+P) - (MUsq+m*m);
      ans += DIntegrand4(l1+P2, MUsq, k, i0, i1, i2)/prop;
      ans += Dsubtract4(l1+P2, MUsq, k, i0, i1, i2, 1)/prop;
    }
  }
  if (skip < 2) {
    for (int k=i0+1; k<i1; k++) {
      const RealMom& P = PropMoms(k, i1-1);
      m = propFlavour[k].Mass();
      prop = S(l1+P) - (MUsq+m*m);
      ans += DIntegrand4(l1+P, MUsq, i0, k, i1, i2)/prop;
      ans += Dsubtract4(l1+P, MUsq, i0, k, i1, i2, 2)/prop;
    }
  }
  if (skip < 3) {
    for (int k=i1+1; k<i2; k++) {
      const RealMom& P = PropMoms(i1, k-1);
      m = propFlavour[k].Mass();
      prop = S(l1-P) - (MUsq+m*m);
      ans += DIntegrand4(l1, MUsq, i0, i1, k, i2)/prop;
      ans += Dsubtract4(l1, MUsq, i0, i1, k, i2, 3)/prop;
    }
  }
  if (skip < 4) {
    for (int k=i2+1; k<n; k++) {
      const RealMom& P = PropMoms(i1, k-1);
      m = propFlavour[k].Mass();
      prop = S(l1-P) - (MUsq+m*m);
      ans += DIntegrand4(l1, MUsq, i0, i1, i2, k)/prop;
      ans += Dsubtract4(l1, MUsq, i0, i1, i2, k, 4)/prop;
    }
  }
  return ans;
}

template<typename T>
complex<T> NGluon2<T>::Dsubtract2(const ComplexMom& l1, const complex<T>& MUsq,
                                  int i0, int i1)
{
  complex<T> ans = T(0.);
  complex<T> prop;
  T m;

  for (int k=0; k<i0; k++) {
    const RealMom& P1 = PropMoms(k, i0-1);
    const RealMom& P2 = PropMoms(i0, i1-1);
    const RealMom P = P1 + P2;
    m = propFlavour[k].Mass();
    // prop = 2.*dot(l1, P)+S(P); // technically since l1 will ALWAYS be on-shell
                                 // we could use this for prop
                                 // the off-shell version is used
                                 // for consistency
    prop = S(l1+P) - (MUsq+m*m);
    ans += DIntegrand3(l1+P2, MUsq, k, i0, i1)/prop;
    ans += Dsubtract3(l1+P2, MUsq, k, i0, i1, 1)/prop;
  }
  for (int k=i0+1; k<i1; k++) {
    const RealMom& P = PropMoms(k, i1-1);
    m = propFlavour[k].Mass();
    prop = S(l1+P) - (MUsq+m*m);
    ans += DIntegrand3(l1+P, MUsq, i0, k, i1)/prop;
    ans += Dsubtract3(l1+P, MUsq, i0, k, i1, 2)/prop;
  }
  for (int k=i1+1; k<n; k++) {
    const RealMom& P = PropMoms(i1, k-1);
    m = propFlavour[k].Mass();
    prop = S(l1-P) - (MUsq+m*m);
    ans += DIntegrand3(l1, MUsq, i0, i1, k)/prop;
    ans += Dsubtract3(l1, MUsq, i0, i1, k, 3)/prop;
  }
  return ans;
}

template<typename T>
complex<T> NGluon2<T>::subtract3(const ComplexMom& l1,
                                 int i0, int i1, int i2, int skip)
{
  complex<T> ans = T(0.);
  complex<T> prop;
  T m;

  if (skip < 1) {
    for (int k=0; k<i0; k++) {
      const RealMom& P1 = PropMoms(k, i0-1);
      const RealMom& P2 = PropMoms(i0, i1-1);
      const RealMom P = P1 + P2;
      m = propFlavour[k].Mass();
      complex<T> prop = S(l1+P) - m*m;
      ans += Integrand4(l1+P2, k, i0, i1, i2)/prop;
    }
  }
  if (skip < 2) {
    for (int k=i0+1; k<i1; k++) {
      const RealMom& P = PropMoms(k, i1-1);
      m = propFlavour[k].Mass();
      prop = S(l1+P) - m*m;
      ans += Integrand4(l1+P, i0, k, i1, i2)/prop;
    }
  }
  if (skip < 3) {
    for (int k=i1+1; k<i2; k++) {
      const RealMom& P = PropMoms(i1, k-1);
      m = propFlavour[k].Mass();
      prop = S(l1-P) - m*m;
      ans += Integrand4(l1, i0, i1, k, i2)/prop;
    }
  }
  if (skip < 4) {
    for (int k=i2+1; k<n; k++) {
      const RealMom& P = PropMoms(i1, k-1);
      m = propFlavour[k].Mass();
      prop = S(l1-P) - m*m;
      ans += Integrand4(l1, i0, i1, i2, k)/prop;
    }
  }
  DEBUG_MSG(2, "box sub  = " << ans)
  return ans;
}

template<typename T>
complex<T> NGluon2<T>::subtract2(const ComplexMom& l1, int i0, int i1)
{
  complex<T> ans = T(0.);
  complex<T> prop;
  T m;

  for (int k=0; k<i0; k++) {
    const RealMom& P1 = PropMoms(k, i0-1);
    const RealMom& P2 = PropMoms(i0, i1-1);
    const RealMom P = P1 + P2;
    m = propFlavour[k].Mass();
    // prop = 2.*dot(l1, P) + S(P); // technically since l1 will ALWAYS be on-shell
                                 // we could use this for prop
                                 // the off-shell version is used
                                 // for consistency
    prop = S(l1+P) - m*m;
    ans += Integrand3(l1+P2, k, i0, i1)/prop;
    ans += subtract3(l1+P2, k, i0, i1, 1)/prop;
  }
  for (int k=i0+1; k<i1; k++) {
    const RealMom& P = PropMoms(k, i1-1);
    m = propFlavour[k].Mass();
    //prop = 2.*dot(l1, P) + S(P);
    prop = S(l1+P) - m*m;
    ans += Integrand3(l1+P, i0, k, i1)/prop;
    ans += subtract3(l1+P, i0, k, i1, 2)/prop;
  }
  for (int k=i1+1; k<n; k++) {
    const RealMom& P = PropMoms(i1, k-1);
    m = propFlavour[k].Mass();
    //prop = -2.*dot(l1, P) + S(P);
    prop = S(l1-P) - m*m;
    ans += Integrand3(l1, i0, i1, k)/prop;
    ans += subtract3(l1, i0, i1, k, 3)/prop;
  }
  DEBUG_MSG(2, "box+tri sub  = " << ans)
  return ans;
}
