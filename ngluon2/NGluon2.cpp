/*
* ngluon2/NGluon2.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include "NGluon2.h"
#include "Model.h"
#include "LoopIntegrals.h"

#define TriThreshold 1e-8

// #define DEBUG_CUT 1
// #define PEVAL 1

template <typename T>
NGluon2<T>::NGluon2(const T scalefactor_)
  : n(0), scalefactor(scalefactor_), MuR2(4.), primtype(NONE), validity(0)
{
  setMuR2(MuR2);
}

template <typename T>
const T NGluon2<T>::PI = constant_traits<T>::pi();

template <typename T>
void NGluon2<T>::setMuR2(const T rscale)
{
  MuR2 = scalefactor*scalefactor*rscale;
}

// setting the process
template <typename T>
void NGluon2<T>::setProcess(const int n_, const RealFlavour* flavarr)
{
  initialize(n_, flavarr);
}

template <typename T>
void NGluon2<T>::setProcess(const RealFlavourList& flavours)
{
  setProcess(flavours.size(), flavours.data());
}

template <typename T>
template <typename U>
void NGluon2<T>::setProcess(const int n_, const Flavour<U>* otherflavarr)
{
  RealFlavourList flavours(n_);
  for (int i=0; i<n_; i++) {
    flavours[i] = RealFlavour(otherflavarr[i]);
  }
  setProcess(flavours.size(), flavours.data());
}

template <typename T>
template <typename U>
void NGluon2<T>::setProcess(const std::vector<Flavour<U> >& otherflavours)
{
  setProcess(otherflavours.size(), otherflavours.data());
}

// initialize all internal variables
template <typename T>
void NGluon2<T>::initialize(const int n_, const RealFlavour* flavarr)
{
  if (n_ != n) {
    n = n_;

    propFlavour.resize(n);
    propFlavourD.resize(n);
    PropMomsSq.resize(n);
    isonshell.resize(n*n);  // technically n*(n-1)/2
    // coefficients
    box.resize(n, 4, 2);
    tri.resize(n, 3, 7);
    bub.resize(n, 2, 1);
    pent.resize(n, 5, 1);
    boxR.resize(n, 4, 6);
    triR.resize(n, 3, 10);
    bubR.resize(n, 2, 1);
    // kinematics for each topology
    boxK.resize(n, 4, 12);
    triK.resize(n, 3, 9);
    bubK.resize(n, 2, 4);
    // d-dim mass parametrizations
    pentMU.resize(n, 5, 1);
    boxMU.resize(n, 4, 2);
    triMU.resize(n, 3, 2);
    bubMU.resize(n, 2, 4);
    // loop momenta basis
    boxM.resize(n, 4, 3);
    triM.resize(n, 3, 5);
    bubM.resize(n, 2, 9);
    pentM.resize(n, 5, 1);
    boxRM.resize(n, 4, 2);
    triRM.resize(n, 3, 3);
    bubRM.resize(n, 2, 4);
  }
  clearCoefficients();

  RealFlavourList scaledflavours(n);
  for (int i=0; i<n; i++) {
    scaledflavours[i] = scalefactor*RealFlavour(flavarr[i]);
  }
  trees.setProcess(scaledflavours.size(), scaledflavours.data());
  int scalepow = trees.Nexternal() - 4;
  if (scaledflavours.back().isHiggs()) {
    scalepow -= 1;
  }
  amprescale = pow(scalefactor, scalepow);

  validity = 1;
}

template <typename T>
void NGluon2<T>::clearCoefficients()
{
  box.clear();
  tri.clear();
  bub.clear();
  pent.clear();
  boxR.clear();
  triR.clear();
  bubR.clear();
}

template <typename T>
void NGluon2<T>::setMomenta(const RealMom* moms)
{
#ifndef NDEBUG
  if (validity < 1) {
    NJET_ERROR("call setProcess first")
  }
#endif
  const int legs = trees.Nexternal();
  std::vector<RealMom> scaledmomenta(legs);
  for (int i=0; i<legs; i++) {
    scaledmomenta[i] = scalefactor*moms[i];
  }
  trees.setMomenta(scaledmomenta);
  validity = 2;
}

template <typename T>
void NGluon2<T>::setMomenta(const std::vector<RealMom>& moms)
{
  setMomenta(moms.data());
}

template <typename T>
template <typename U>
void NGluon2<T>::setMomenta(const MOM<U>* othermoms)
{
  const int legs = trees.Nexternal();
  std::vector<RealMom> moms(legs);
  for (int i=0; i<legs; i++) {
    moms[i] = RealMom(othermoms[i]);
  }
  setMomenta(moms.data());
}

template <typename T>
template <typename U>
void NGluon2<T>::setMomenta(const std::vector<MOM<U> >& othermoms)
{
  setMomenta(othermoms.data());
}

template <typename T>
void NGluon2<T>::setHelicity(const int* helicity)
{
#ifndef NDEBUG
  if (validity < 2) {
    NJET_ERROR("call setProcess and setMomenta first")
  }
#endif
  trees.setHelicity(helicity);
  validity = 3;
}

template <typename T>
void NGluon2<T>::setHelicity(const std::vector<int>& helicity)
{
  setHelicity(helicity.data());
}

template <typename T>
void NGluon2<T>::setOrder(const int* ord)
{
  trees.setOrder(ord);
}

template <typename T>
void NGluon2<T>::setOrder(const std::vector<int>& ord)
{
  setOrder(ord.data());
}

template <typename T>
void NGluon2<T>::setHeavyQuarkMass(const T m)
{
  trees.setHeavyLoopFlavour(scalefactor*m);
}

template <typename T>
LoopResult<T> NGluon2<T>::eval(const PRIMTYPE primtype_, const int* ord/*=0*/, bool cconly/*=false*/)
{
#ifndef NDEBUG
  if (validity < 3) {
    NJET_ERROR("call setProcess, setMomenta and setHelicity first")
  }
#endif

  clearCoefficients();
  if (ord) {
    setOrder(ord);
  }

  if (primtype_ != primtype) {
    primtype = primtype_;
  }

  trees.fillCurrents();
  value = AmpValue<T>();
  value.tree() = trees.eval();
  valueC = AmpValue<T>();
  valueC.tree() = conj(value.tree().get0());

  RealFlavour loopflavour;
  switch (primtype) {
    case MIXED:
      loopflavour = RealFlavour(ModelBase::Gluon);
      break;
    case LIGHTQ:
      loopflavour = trees.getLightLoopFlavour();
      break;
    case HEAVYQ:
      loopflavour = trees.getHeavyLoopFlavour();
      break;
    default:
      NJET_ERROR("unknown primitive type " << primtype)
      break;
  }
  fillPropFlavours(loopflavour);

  fillOnShell();
  fillPropMomsSq();  // move that to initonshell ?
  initonshell();

  if (n > 3) {
    calculate_boxes();
  }
  calculate_triangles();
  calculate_bubbles();

  // mass renormalization or smth

  if (not cconly) {
    if (n > 4) {
      calculate_pentagons();
    }
    if (n > 3) {
      calculate_boxesR();
    }
    calculate_trianglesR();
    calculate_bubblesR();
  }

  if (primtype == HEAVYQ) {
    MassRenormalizeH(loopflavour.Mass());
  }

  value *= amprescale;
  valueC *= amprescale;

  value.update();
  valueC.update();

  LoopResult<T> result = {value.loop(), valueC.loop()};
  return result;
}

template <typename T>
void NGluon2<T>::MassRenormalizeH(const T mH)
{
  EpsTriplet<T> ans, ansC;
  const T mHsq = mH*mH;

  // FIXME still some things unclear about this procedure
  // adding the amount of tree will fix the pole to the
  // correct "charge-unrenormalized" result but mess up
  // both the logs AND furry's theorem relations...
  complex<T> c2m = T(0.);// 2./T(3.)*trees.eval();
  complex<T> r22m = T(0.);
  for (int i0 = 0; i0 < n-1; i0++) {
    if (propFlavour[i0].isBlank()) continue;
    for (int i1=i0+1; i1<n; i1++) {
      if (propFlavour[i1].isBlank()) continue;
      if (isOnShell(i0, i1)) continue;
      Coefficients c2 = bub(i0, i1);
      Coefficients r22 = bubR(i0, i1);
      c2m -= c2[0];
      r22m -= r22[0];
    }
  }

  const EpsTriplet<T> integral = I2(T(0.), mHsq, mHsq, MuR2);
  ans = c2m*integral + r22m*mHsq;
  ansC = conj(c2m)*integral + conj(r22m*mHsq);
  value.MassRenorm() += ans;
  valueC.MassRenorm() += ansC;
}

template <typename T>
complex<T> NGluon2<T>::evalTree(const int* ord)
{
  if (ord) {
    setOrder(ord);
  }
  return amprescale*trees.eval();
}

template <typename T>
void NGluon2<T>::fillOnShell()
{
  const RealParticleList& process = trees.Process();
  for (int i0=0; i0<n-1; i0++) {
    const RealParticle& flav0 = process[i0];
    for (int i1=i0+1; i1<n; i1++) {
      isOnShell(i0, i1) = ((i1-i0) == 1 || (i0-i1+n)%n == 1)
                       && (flav0.isParton() or flav0.isLightVector());
    }
  }
}

template <typename T>
void NGluon2<T>::fillPropMomsSq()
{
  const RealParticleList& process = trees.Process();

  for (int k=0; k<n; k++) {
    if (process[k].isMassiveVector()) {
      PropMomsSq(k, k) = S(PropMoms(k, k));
    } else {
      const T mm = process[k].Mass();
      PropMomsSq(k, k) = mm*mm;
    }
  }
  for (int s=1; s<n-1; s++) {
    for (int k=0; k<n; k++) {
      PropMomsSq(k, k+s) = S(PropMoms(k, k+s));
    }
  }
}

template <typename T>
void NGluon2<T>::fillPropFlavours(const RealFlavour& l0)
{
  const RealParticleList& process = trees.Process();

  propFlavour.assign(propFlavour.size(), RealFlavour());
  propFlavour[0] = l0;

  for (int k=1; k<n; k++) {
    // if a blank propagator, need to look harder...
    // this looks ugly, should have a go at a better version sometime.
    if (propFlavour[k-1].isBlank()) {
      int k1 = k - 1;
      int flines = 0;

      while (propFlavour[k1].isBlank()) {
        if (process[k1].isFermion()) {
          flines += process[k1].idx();
        }
        k1--;
      }

      if (process[k1].isFermion()) {
        flines += process[k1].idx();
      }

      if (flines == 0) {
        propFlavour[k] = propFlavour[k1];
      } else {
        propFlavour[k] = RealFlavour();
      }
    }
    else if (propFlavour[k-1].isGluon()
             && process[k-1].isGluon()) {
      propFlavour[k] = RealFlavour(ModelBase::Gluon);
    }
    else if (propFlavour[k-1].isGluon()
             && process[k-1].isFermion()) {
      propFlavour[k] = process[k-1].C();
    }
    else if (propFlavour[k-1].isFermion()
             && process[k-1].isGluon()) {
      propFlavour[k] = propFlavour[k-1];
    }
    else if (propFlavour[k-1].isFermion()
             && process[k-1].isFermion()
             && propFlavour[k-1].idx() == process[k-1].idx()) {
      propFlavour[k] = RealFlavour(ModelBase::Gluon);
    }
    else if ((process[k-1].isMassiveVector() or process[k-1].isLightVector())
        && propFlavour[k-1].C() == process[k-1].Vq()) {
      propFlavour[k] = RealFlavour(process[k-1].VQ());
    }
    else if ((process[k-1].isMassiveVector() or process[k-1].isLightVector())
        && propFlavour[k-1].C() == process[k-1].VQ()) {
      propFlavour[k] = RealFlavour(process[k-1].Vq());
    }
    else {
      propFlavour[k] = RealFlavour();
    }
  }

  for (int k=0; k<n; k++) {
    propFlavourD[k] = propFlavour[k].DimShift();
  }

  // TODO get rid of the stuff below
  PropSpin.resize(n);
  PropSpinD.resize(n);
  for (int k=0; k<n; k++) {
    if (ModelBase::isScalar(propFlavour[k].idx())) {
      PropSpin[k] = 0;
    } else {
      PropSpin[k] = -1;
    }

    if (ModelBase::isScalar(propFlavourD[k].idx())) {
      PropSpinD[k] = 0;
    } else {
      PropSpinD[k] = -1;
    }
  }
}

template <typename T>
void NGluon2<T>::setLoopMass(const complex<T>& mDsq)
{
  trees.setLoopMass(mDsq);
  for (int k=0; k<n; k++) {
    propFlavourD[k].setMass(trees.getComplexFlavourMass(propFlavourD[k].idx()));
  }
}

template <typename T>
void NGluon2<T>::calculate_boxes()
{
  EpsTriplet<T> boxes, boxesC;

  for (int i0=0; i0<n-3; i0++) {
    if (propFlavour[i0].isBlank()) continue;
    for (int i1=i0+1; i1<n-2; i1++) {
      if (propFlavour[i1].isBlank()) continue;
      for (int i2=i1+1; i2<n-1; i2++) {
        if (propFlavour[i2].isBlank()) continue;
        for (int i3=i2+1; i3<n; i3++) {
          if (propFlavour[i3].isBlank()) continue;

          DEBUG_MSG(1, "box: " << i0 << i1 << i2 << i3)

          const Momenta LM = boxM(i0, i1, i2, i3);
          const RealMom& K1 = PropMoms(i0, i1-1);
          const RealMom& K2 = PropMoms(i1, i2-1);
          const RealMom& K3 = PropMoms(i2, i3-1);

          const ComplexMom l1[2] = {LM[0], conj(LM[0])};
          // ComplexMom l1[2] = {LM[0], LM[2]}; // gives the same result in case you're interested
          const ComplexMom l2[2] = {l1[0]-K2, l1[1]-K2};
          const ComplexMom l3[2] = {l2[0]-K3, l2[1]-K3};
          const ComplexMom l0[2] = {l1[0]+K1, l1[1]+K1};

          const Kinematics SS = boxK(i0, i1, i2, i3);

          complex<T> d[2];
          for (int sol=0; sol<2; sol++) {
            ComplexParticle L1(propFlavour[i1], l1[sol]);
            ComplexParticle L2(propFlavour[i2], l2[sol]);
            ComplexParticle L3(propFlavour[i3], l3[sol]);
            ComplexParticle L0(propFlavour[i0], l0[sol]);

#ifndef DEBUG_CUT
            complex<T> quickcut = T(0.);
            for (int h0=-1; h0<=1; h0+=2) {
              L0.setHelicity(h0);
              quickcut += trees.Ceval4(L0, L1, L2, L3, i0, i1, i2, i3, n);
            }
            d[sol] = -quickcut;
#else
            complex<T> cut = T(0.);
    #ifdef PEVAL
            cut = trees.Peval4(L0, L1, L2, L3, i0, i1, i2, i3, n);
    #else
            for (int h1=-1; h1<=1; h1+=2) {
              for (int h2=-1; h2<=1; h2+=2) {
                for (int h3=-1; h3<=1; h3+=2) {
                  for (int h0=-1; h0<=1; h0+=2) {
                    L0.setHelicity(h0);
                    L1.setHelicity(h1);
                    L2.setHelicity(h2);
                    L3.setHelicity(h3);
                    const complex<T> tmp =
                       trees.Leval(-L0, i0, i1-1, L1)
                      *trees.Leval(-L1, i1, i2-1, L2)
                      *trees.Leval(-L2, i2, i3-1, L3)
                      *trees.Leval(-L3, i3, (i0-1+n)%n, L0);
                    cut += tmp;
                  }
                }
              }
            }
    #endif // PEVAL
            d[sol] = i_*cut;
#endif // DEBUG_CUT
          }

          DEBUG_MSG(2, "d0 = " << d[0])
          DEBUG_MSG(2, "d1 = " << d[1])

          Coefficients c = box(i0, i1, i2, i3);
          c[0] = (d[0] + d[1])*0.5;
          c[1] = (d[0] - d[1])*0.25*SS[0];

          DEBUG_MSG(2, "check integrand coeffs:")
          DEBUG_MSG(2, "c0 = " << c[0])
          DEBUG_MSG(2, "c1 = " << c[1])

          // notation : s, t, s1, s2, s3, s4, m1^2, m2^2, m3^2, m4^2, MuR^2
          const EpsTriplet<T> integral = I4(SS[5], SS[6], SS[1], SS[2], SS[3], SS[4], SS[7], SS[8], SS[9], SS[10], MuR2);
          boxes += c[0]*integral;
          boxesC += conj(c[0])*integral;
        }
      }
    }
  }
  DEBUG_MSG(1, "sum of boxes : " << boxes)
  value.box() = boxes;
  valueC.box() = boxesC;
}

template <typename T>
void NGluon2<T>::calculate_triangles()
{
  EpsTriplet<T> triangles, trianglesC;
  const int NT1 = 4;

  for (int i0=0; i0<n-2; i0++) {
    if (propFlavour[i0].isBlank()) continue;
    for (int i1=i0+1; i1<n-1; i1++) {
      if (propFlavour[i1].isBlank()) continue;
      for (int i2=i1+1; i2<n; i2++) {
        if (propFlavour[i2].isBlank()) continue;
        complex<T> d[7] = {};

        DEBUG_MSG(1, "tri: " << i0 << i1 << i2)
        const Momenta LM = triM(i0, i1, i2);
        const Kinematics SS = triK(i0, i1, i2);

        // FIXME better check here??
        int massless = 0;
        if (abs(SS[0]) < TriThreshold) {
          DEBUG_MSG(1, "looks like D3=0")
          massless = 1;
        }

        ComplexMom l0[2], l1[2], l2[2];

        const RealMom& K1 = PropMoms(i0, i1-1);
        const RealMom& K2 = PropMoms(i1, i2-1);

        T extR = 1.;
        int TMAX = 2*NT1-1;
        int nsols = 1;
        if (massless) {
          extR = 2.;
          nsols = 2;
          TMAX = NT1;
        }

        complex<T> res[2];
        for (int aa1=0; aa1<TMAX; aa1++) {
          const complex<T> tt1 = extR*exp(i_*2.*PI*T(aa1)/T(TMAX));
          if (massless) {
            l1[0] = LM[0] + tt1*LM[1];
            l1[1] = LM[0] + tt1*LM[2];
          } else {
            complex<T> rtG3 = sqrt(abs(SS[0]));
            if (SS[0] < 0.) {
              rtG3 *= i_;
            }
            l1[0] = LM[0] + tt1*rtG3*LM[1] + (rtG3/tt1)*LM[2];
            l1[1] = LM[0] + tt1*rtG3*LM[2] + (rtG3/tt1)*LM[1];
          }

          for (int sol=0; sol<nsols; sol++) {
            l2[sol] = l1[sol] - K2;
            l0[sol] = l1[sol] + K1;

            // tree amplitudes
            ComplexParticle L1(propFlavour[i1], l1[sol]);
            ComplexParticle L2(propFlavour[i2], l2[sol]);
            ComplexParticle L0(propFlavour[i0], l0[sol]);

#ifndef DEBUG_CUT
            complex<T> quickcut = T(0.);
            for (int h0=-1; h0<=1; h0+=2) {
              L0.setHelicity(h0);
              quickcut += trees.Ceval3(L0, L1, L2, i0, i1, i2, n);
            }
            quickcut *= -1.;
            const complex<T> sub = subtract3(l1[sol], i0, i1, i2);
            res[sol] = quickcut - sub;
#else
            complex<T> cut = T(0.);
    #ifdef PEVAL
            cut = trees.Peval3(L0, L1, L2, i0, i1, i2, n);
    #else
            for (int h1=-1; h1<=1; h1+=2) {
              for (int h2=-1; h2<=1; h2+=2) {
                for (int h0=-1; h0<=1; h0+=2) {
                  L0.setHelicity(h0);
                  L1.setHelicity(h1);
                  L2.setHelicity(h2);
                  const complex<T> tmp =
                     trees.Leval(-L0, i0, i1-1, L1)
                    *trees.Leval(-L1, i1, i2-1, L2)
                    *trees.Leval(-L2, i2, (i0-1+n)%n, L0);
                  cut += tmp;
                }
              }
            }
    #endif // PEVAL
            const complex<T> sub = subtract3(l1[sol], i0, i1, i2);
            res[sol] = cut - sub;
            DEBUG_MSG(2, "cut = " << cut)
            DEBUG_MSG(2, "sub = " << sub)
#endif // DEBUG_CUT
            res[sol] /= T(TMAX);
          }

          if (massless) {
            d[0] += res[1]/(tt1*tt1*tt1);
            d[1] += res[1]/(tt1*tt1);
            d[2] += res[1]/tt1;
            d[3] += 0.5*(res[1] + res[0]);
            d[4] += res[0]/tt1;
            d[5] += res[0]/(tt1*tt1);
            d[6] += res[0]/(tt1*tt1*tt1);
          } else {
            d[0] += res[0]*tt1*tt1*tt1;
            d[1] += res[0]*tt1*tt1;
            d[2] += res[0]*tt1;
            d[3] += res[0];
            d[4] += res[0]/tt1;
            d[5] += res[0]/(tt1*tt1);
            d[6] += res[0]/(tt1*tt1*tt1);
          }
        }

        DEBUG_MSG(1, "massless check: " << SS[0])
        for (int k=0; k<7; k++) {
          DEBUG_MSG(2, "test d" << k << " = " << d[k])
        }

        Coefficients c = tri(i0, i1, i2);

        if (massless) {
          complex<T> rtg12 = sqrt(abs(SS[8]));
          if (SS[8] < 0.) {
            rtg12 *= i_;
          }
          c[0] = d[3];
          c[1] =    0.5*(d[4] - d[2])/rtg12;
          c[2] = i_*0.5*(d[4] + d[2])/rtg12;
          c[3] =   0.25*(d[5] + d[1])/SS[8];
          c[4] = i_*0.5*(d[5] - d[1])/SS[8];
          c[5] = i_*0.5*(d[6] + d[0])/(SS[8]*rtg12);
          c[6] =   -0.5*(d[6] - d[0])/(SS[8]*rtg12);
        }
        else {
          complex<T> rtG3h = sqrt(abs(SS[7]));
          if (SS[7] < 0.) {
            rtG3h *= i_;
          }
          c[0] = d[3];
          c[1] =    0.5*(d[0] - d[2] + d[4] - d[6])/rtG3h;
          c[2] = i_*0.5*(d[0] + d[2] + d[4] + d[6])/rtG3h;
          c[3] =    0.25*(d[1] + d[5])/SS[7];
          c[4] = -i_*0.5*(d[1] - d[5])/SS[7];
          c[5] = i_*0.5*(d[0] + d[6])/(SS[7]*rtG3h);
          c[6] =    0.5*(d[0] - d[6])/(SS[7]*rtG3h);
        }

        DEBUG_MSG(2, "check integrand coeffs:")
        for (int k=0; k<7; k++) {
          DEBUG_MSG(2, "c" << k << " = " << c[k])
        }

        const EpsTriplet<T> integral = I3(SS[1], SS[2], SS[3], SS[4], SS[5], SS[6], MuR2);
        triangles += c[0]*integral;
        trianglesC += conj(c[0])*integral;
      }
    }
  }
  DEBUG_MSG(1, "sum of triangles : " << triangles)
  value.tri() = triangles;
  valueC.tri() = trianglesC;
}

template <typename T>
void NGluon2<T>::calculate_bubbles()
{
  EpsTriplet<T> bubbles, bubblesC;
  const int NT2 = 3;

  for (int i0=0; i0<n-1; i0++) {
    if (propFlavour[i0].isBlank()) continue;
    for (int i1=i0+1; i1<n; i1++) {
      if (propFlavour[i1].isBlank()) continue;
      complex<T> d[2] = {};

      DEBUG_MSG(1, "bub: " << i0 << i1)
      if (isOnShell(i0, i1)) {
        DEBUG_MSG(1, "skipping on-shell bubble")
        continue;
      }

      const Momenta LM = bubM(i0, i1);
      const RealMom& K1 = PropMoms(i0, i1-1);

      const Kinematics SS = bubK(i0, i1);
      const T x2 = SS[3]/SS[0];
      const T tauscale = sqrt((x2*x2 - SS[2]/SS[0])/3.);  // assuming positive argument of the sqrt

      complex<T> res;
      for (int aa1=0; aa1<2; aa1++) {
        const complex<T> tt1 = tauscale*T(2*aa1-1);  // -tauscale, +tauscale
        for (int aa2=0; aa2<NT2; aa2++) {
          const complex<T> tt2 = exp(i_*2.*PI*T(aa2)/T(NT2));

          const ComplexMom l1 =
            LM[0]
            + tt1*LM[1]
            + tt2*LM[2]
            + (tt1*tt1/tt2)*LM[3]
            + (tt1/tt2)*LM[4]
            + (1./tt2)*LM[5];

          const ComplexMom l0 = l1 + K1;

          ComplexParticle L1(propFlavour[i1], l1);
          ComplexParticle L0(propFlavour[i0], l0);

#ifndef DEBUG_CUT
          complex<T> quickcut = T(0.);
          for (int h0=-1; h0<=1; h0+=2) {
            L0.setHelicity(h0);
            quickcut += trees.Ceval2(L0, L1, i0, i1, n);
          }
          quickcut *= -1.;
          const complex<T> sub = subtract2(l1, i0, i1);
          res = quickcut - sub;
#else
          complex<T> cut = T(0.);
    #ifdef PEVAL
            cut = trees.Peval2(L0, L1, i0, i1, n);
    #else
          for (int h1=-1; h1<=1; h1+=2) {
            for (int h0=-1; h0<=1; h0+=2) {
              L0.setHelicity(h0);
              L1.setHelicity(h1);
              const complex<T> tmp =
                  trees.Leval(-L0, i0, i1-1, L1)
                 *trees.Leval(-L1, i1, (i0-1+n)%n, L0);
              cut += tmp;
            }
          }
    #endif // PEVAL
          const complex<T> sub = subtract2(l1, i0, i1);
          cut *= -i_;
          res = cut - sub;
          DEBUG_MSG(2, "cut = " << cut)
          DEBUG_MSG(2, "sub = " << sub)
#endif // DEBUG_CUT
          d[aa1] += res/(NT2*2.);
        }
      }

      DEBUG_MSG(2, "d0 = " << d[0])
      DEBUG_MSG(2, "d1 = " << d[1])

      Coefficients c = bub(i0, i1);
      c[0] = (d[1] + d[0]) + (d[1] - d[0])*0.5*x2/tauscale;

      DEBUG_MSG(2, "c0 = " << c[0])

      const EpsTriplet<T> integral = I2(SS[0], SS[1], SS[2], MuR2);
      bubbles += c[0]*integral;
      bubblesC += conj(c[0])*integral;
    }
  }
  DEBUG_MSG(1, "sum of bubbles : " << bubbles)
  value.bub() = bubbles;
  valueC.bub() = bubblesC;
}

template <typename T>
void NGluon2<T>::calculate_pentagons()
{
  for (int i0=0; i0<n-4; i0++) {
    if (propFlavourD[i0].isBlank()) continue;
    for (int i1=i0+1; i1<n-3; i1++) {
      if (propFlavourD[i1].isBlank()) continue;
      for (int i2=i1+1; i2<n-2; i2++) {
        if (propFlavourD[i2].isBlank()) continue;
        for (int i3=i2+1; i3<n-1; i3++) {
          if (propFlavourD[i3].isBlank()) continue;
          for (int i4=i3+1; i4<n; i4++) {
            if (propFlavourD[i4].isBlank()) continue;

            DEBUG_MSG(1, "pentagon: " << i0 << i1 << i2 << i3 << i4)

            const Momenta LM = pentM(i0, i1, i2, i3, i4);
            const Coefficients MUsq = pentMU(i0, i1, i2, i3, i4);
            const RealMom& K1 = PropMoms(i0, i1-1);
            const RealMom& K2 = PropMoms(i1, i2-1);
            const RealMom& K3 = PropMoms(i2, i3-1);
            const RealMom& K4 = PropMoms(i3, i4-1);

            const ComplexMom l1 = LM[0];
            const ComplexMom l2 = l1 - K2;
            const ComplexMom l3 = l2 - K3;
            const ComplexMom l4 = l3 - K4;
            const ComplexMom l0 = l1 + K1;

            DEBUG_MSG(2, "pentagon Mu : " << sqrt(MUsq[0]))

            Coefficients c = pent(i0, i1, i2, i3, i4);

            setLoopMass(MUsq[0]);
            ComplexParticle L1(propFlavourD[i1], l1);
            ComplexParticle L2(propFlavourD[i2], l2);
            ComplexParticle L3(propFlavourD[i3], l3);
            ComplexParticle L4(propFlavourD[i4], l4);
            ComplexParticle L0(propFlavourD[i0], l0);

#ifndef DEBUG_CUT
            complex<T> quickcut = T(0.);
            for (int h0=PropSpinD[i0]; h0<=1; h0+=2) {
              L0.setHelicity(h0);
              quickcut += trees.Ceval5(L0, L1, L2, L3, L4, i0, i1, i2, i3, i4, n);
            }
            if (primtype == MIXED) {
              quickcut *= 2.;
            }
            c[0] = quickcut/MUsq[0];
#else
            complex<T> cut = T(0.);
    #ifdef PEVAL
            cut = trees.Peval5(L0, L1, L2, L3, L4, i0, i1, i2, i3, i4, n);
    #else
            for (int h1=PropSpinD[i1]; h1<=1; h1+=2) {
              for (int h2=PropSpinD[i2]; h2<=1; h2+=2) {
                for (int h3=PropSpinD[i3]; h3<=1; h3+=2) {
                  for (int h4=PropSpinD[i4]; h4<=1; h4+=2) {
                    for (int h0=PropSpinD[i0]; h0<=1; h0+=2) {
                      L1.setHelicity(h1);
                      L2.setHelicity(h2);
                      L3.setHelicity(h3);
                      L4.setHelicity(h4);
                      L0.setHelicity(h0);
                      const complex<T> tmp =
                         trees.Leval(-L0, i0, i1-1, L1)
                        *trees.Leval(-L1, i1, i2-1, L2)
                        *trees.Leval(-L2, i2, i3-1, L3)
                        *trees.Leval(-L3, i3, i4-1, L4)
                        *trees.Leval(-L4, i4, (i0-1+n)%n, L0);
                      cut += -tmp;
                    }
                  }
                }
              }
            }
    #endif // PEVAL
            if (primtype == MIXED) {
              cut *= 2.;
            }
            c[0] = cut/MUsq[0];
#endif // DEBUG_CUT

            DEBUG_MSG(2, "c0 = " << c[0])
          }
        }
      }
    }
  }
}

template <typename T>
void NGluon2<T>::calculate_boxesR()
{
  complex<T> boxesR;
  const int NT1 = 5;

  for (int i0=0; i0<n-3; i0++) {
    if (propFlavourD[i0].isBlank()) continue;
    for (int i1=i0+1; i1<n-2; i1++) {
      if (propFlavourD[i1].isBlank()) continue;
      for (int i2=i1+1; i2<n-1; i2++) {
        if (propFlavourD[i2].isBlank()) continue;
        for (int i3=i2+1; i3<n; i3++) {
          if (propFlavourD[i3].isBlank()) continue;
          complex<T> d[NT1];

          DEBUG_MSG(1, "boxR: " << i0 << i1 << i2 << i3)

          const Momenta LM = boxRM(i0, i1, i2, i3);
          const Coefficients MU = boxMU(i0, i1, i2, i3);
          const RealMom& K1 = PropMoms(i0, i1-1);
          const RealMom& K2 = PropMoms(i1, i2-1);
          const RealMom& K3 = PropMoms(i2, i3-1);

          const Kinematics SS = boxK(i0, i1, i2, i3);

          complex<T> res = T(0.);
          for (int aa1=0; aa1<NT1; aa1++) {
            const complex<T> tt1 = exp(i_*2.*PI*T(aa1)/T(NT1));

            const ComplexMom l1 = LM[0] + tt1*LM[1];
            const ComplexMom l2 = l1 - K2;
            const ComplexMom l3 = l2 - K3;
            const ComplexMom l0 = l1 + K1;
            const complex<T> MUsq = MU[0] + tt1*tt1*MU[1];

            setLoopMass(MUsq);
            ComplexParticle L1(propFlavourD[i1], l1);
            ComplexParticle L2(propFlavourD[i2], l2);
            ComplexParticle L3(propFlavourD[i3], l3);
            ComplexParticle L0(propFlavourD[i0], l0);

#ifndef DEBUG_CUT
            complex<T> quickcut = T(0.);
            for (int h0=PropSpinD[i0]; h0<=1; h0+=2) {
              L0.setHelicity(h0);
              quickcut += trees.Ceval4(L0, L1, L2, L3, i0, i1, i2, i3, n);
            }
            if (primtype == MIXED) {
              quickcut *= 2.;
            }
            const complex<T> sub = Dsubtract4(l1, MUsq, i0, i1, i2, i3);
            res = quickcut - sub;
#else
            complex<T> cut = T(0.);
    #ifdef PEVAL
            cut = i_*trees.Peval4(L0, L1, L2, L3, i0, i1, i2, i3, n);
    #else
            for (int h1=PropSpinD[i1]; h1<=1; h1+=2) {
              for (int h2=PropSpinD[i2]; h2<=1; h2+=2) {
                for (int h3=PropSpinD[i3]; h3<=1; h3+=2) {
                  for (int h0=PropSpinD[i0]; h0<=1; h0+=2) {
                    L1.setHelicity(h1);
                    L2.setHelicity(h2);
                    L3.setHelicity(h3);
                    L0.setHelicity(h0);
                    const complex<T> tmp =
                       trees.Leval(-L0, i0, i1-1, L1)
                      *trees.Leval(-L1, i1, i2-1, L2)
                      *trees.Leval(-L2, i2, i3-1, L3)
                      *trees.Leval(-L3, i3, (i0-1+n)%n, L0);
                    cut += i_*tmp;
                  }
                }
              }
            }
    #endif // PEVAL
            if (primtype == MIXED) {
              cut *= 2.;
            }

            const complex<T> sub = Dsubtract4(l1, MUsq, i0, i1, i2, i3);
            res = cut - sub;
#endif // DEBUG_CUT
            res /= T(NT1);

            d[0] += res;
            d[1] += res/tt1;
            d[2] += res/(tt1*tt1);
            d[3] += res/(tt1*tt1*tt1);
            d[4] += res/(tt1*tt1*tt1*tt1);

          }

          Coefficients c = boxR(i0, i1, i2, i3);
          c[0] = d[0] + d[2] + d[4];
          c[1] = 0.5*(d[1] + d[3])*SS[0];
          c[2] = (d[2] + 2.*d[4])*SS[11];
          c[3] = 0.5*d[3]*SS[11]*SS[0];
          c[4] = d[4]*SS[11]*SS[11];

          DEBUG_MSG(2, "check integrand coeffs:")
          for (int k=0; k<5; k++) {
            DEBUG_MSG(2, "c" << k << " = " << c[k])
          }

          boxesR -= c[4]/6.;
        }
      }
    }
  }
  DEBUG_MSG(1, "rational box = " << boxesR)
  value.boxR() = boxesR;
  valueC.boxR() = conj(boxesR);
}

template <typename T>
void NGluon2<T>::calculate_trianglesR()
{
  complex<T> trianglesR;
  const int NT1 = 4;
  const int NT2 = 4;

  for (int i0=0; i0<n-2; i0++) {
    if (propFlavourD[i0].isBlank()) continue;
    for (int i1=i0+1; i1<n-1; i1++) {
      if (propFlavourD[i1].isBlank()) continue;
      for (int i2=i1+1; i2<n; i2++) {
        if (propFlavourD[i2].isBlank()) continue;
        complex<T> d[10];

        DEBUG_MSG(1, "triR: " << i0 << i1 << i2)

        const Momenta LM = triRM(i0, i1, i2);
        const Coefficients MU = triMU(i0, i1, i2);
        const RealMom& K1 = PropMoms(i0, i1-1);
        const RealMom& K2 = PropMoms(i1, i2-1);

        const Kinematics SS = triK(i0, i1, i2);
        // FIXME better check here??
        int massless = 0;
        if (abs(SS[0]) < TriThreshold) {
          DEBUG_MSG(1, "looks like D3=0")
          massless = 1;
        }

        complex<T> res;
        for (int aa1=0; aa1<NT1; aa1++) {
          const complex<T> tt1 = exp(2.*i_*PI*(0.5+T(aa1))/T(NT1));
          for (int aa2=0; aa2<NT2; aa2++) {
            const complex<T> tt2 = 2.*exp(2.*i_*PI*T(aa2)/T(NT2));

            ComplexMom l1;
            complex<T> MUsq;
            if (massless) {
              l1 = LM[0] + tt1*LM[1] + tt2*LM[2];
              MUsq = -SS[8]*tt1*tt2;
            } else {
              complex<T> rtG3 = sqrt(abs(SS[0]));
              if (SS[0] < 0.) {
                rtG3 *= i_;
              }
              l1 = LM[0] + rtG3*tt1*LM[1] + rtG3*tt2*LM[2];
              MUsq = MU[0] + SS[0]*tt1*tt2*MU[1];
            }
            const ComplexMom l2 = l1 - K2;
            const ComplexMom l0 = l1 + K1;

            setLoopMass(MUsq);
            ComplexParticle L1(propFlavourD[i1], l1);
            ComplexParticle L2(propFlavourD[i2], l2);
            ComplexParticle L0(propFlavourD[i0], l0);

#ifndef DEBUG_CUT
            complex<T> quickcut = T(0.);
            for (int h0=PropSpinD[i0]; h0<=1; h0+=2) {
              L0.setHelicity(h0);
              quickcut += trees.Ceval3(L0, L1, L2, i0, i1, i2, n);
            }
            if (primtype == MIXED) {
              quickcut *= 2.;
            }
            const complex<T> sub = Dsubtract3(l1, MUsq, i0, i1, i2);
            res = quickcut - sub;
#else
            complex<T> cut = T(0.);
    #ifdef PEVAL
            cut = -trees.Peval3(L0, L1, L2, i0, i1, i2, n);
    #else
            for (int h1=PropSpinD[i1]; h1<=1; h1+=2) {
              for (int h2=PropSpinD[i2]; h2<=1; h2+=2) {
                for (int h0=PropSpinD[i0]; h0<=1; h0+=2) {
                  L1.setHelicity(h1);
                  L2.setHelicity(h2);
                  L0.setHelicity(h0);
                  const complex<T> tmp =
                     trees.Leval(-L0, i0, i1-1, L1)
                    *trees.Leval(-L1, i1, i2-1, L2)
                    *trees.Leval(-L2, i2, (i0-1+n)%n, L0);
                  cut += tmp;
                }
              }
            }
    #endif // PEVAL
            if (primtype == MIXED) {
              cut *= 2.;
            }

            const complex<T> sub = Dsubtract3(l1, MUsq, i0, i1, i2);
            res = cut - sub;
#endif // DEBUG_CUT
            res /= T(NT1*NT2);

            d[0] += res;
            d[1] += res/tt1;
            d[2] += res/(tt1*tt1);
            d[3] += res/(tt1*tt1*tt1);
            d[4] += res/tt2;
            d[5] += res/(tt2*tt2);
            d[6] += res/(tt2*tt2*tt2);
            d[7] += res/tt1/tt2;
            d[8] += res/(tt1*tt1)/tt2;
            d[9] += res/tt1/(tt2*tt2);
          }
        }

        Coefficients c = triR(i0, i1, i2);
        if (massless) {
          complex<T> rtg12 = sqrt(abs(SS[8]));
          if (SS[8] < 0.) {
            rtg12 *= i_;
          }
          c[0] = d[0];
          c[1] =    0.5*(d[1] - d[4])/rtg12;
          c[2] = i_*0.5*(d[1] + d[4])/rtg12;
          c[3] =   0.25*(d[2] + d[5])/SS[8];
          c[4] = i_*0.5*(d[2] - d[5])/SS[8];
          c[5] = i_*0.5*(d[3] + d[6])/(SS[8]*rtg12);
          c[6] =   -0.5*(d[3] - d[6])/(SS[8]*rtg12);
          c[7] =     0.5*(d[3] - d[6] - d[8] + d[9])/(SS[8]*rtg12);
          c[8] = -i_*0.5*(d[3] + d[6] + d[8] + d[9])/(SS[8]*rtg12);
          c[9] = -d[7]/SS[8];
        }
        else {
          complex<T> rtG3h = sqrt(abs(SS[7]));
          if (SS[7] < 0.) {
            rtG3h *= i_;
          }
          c[0] = d[0] + d[7];
          c[1] =    0.5*(-d[3] + d[6] + d[8] - d[9] + d[1] - d[4])/rtG3h;
          c[2] = i_*0.5*( d[3] + d[6] + d[8] + d[9] + d[1] + d[4])/rtG3h;
          c[3] =   0.25*(d[2] + d[5])/SS[7];
          c[4] = i_*0.5*(d[2] - d[5])/SS[7];
          c[5] = i_*0.5*(d[3] + d[6])/(SS[7]*rtG3h);
          c[6] =   -0.5*(d[3] - d[6])/(SS[7]*rtG3h);
          c[7] =     0.5*(d[3] - d[6] - d[8] + d[9])/(SS[7]*rtG3h);
          c[8] = -i_*0.5*(d[3] + d[6] + d[8] + d[9])/(SS[7]*rtG3h);
          c[9] = -d[7]/SS[7];
        }

        DEBUG_MSG(2, "check integrand coeffs:")
        for (int k=0; k<10; k++) {
          DEBUG_MSG(2, "c" << k << " = " << c[k])
        }

        trianglesR -= -c[9]*0.5;
      }
    }
  }
  DEBUG_MSG(1, "rational tri = " << trianglesR)
  value.triR() = trianglesR;
  valueC.triR() = conj(trianglesR);
}

template <typename T>
void NGluon2<T>::calculate_bubblesR()
{
  complex<T> bubblesR;
  const int NT1 = 3;

  for (int i0=0; i0<n-1; i0++) {
    if (propFlavourD[i0].isBlank()) continue;
    for (int i1=i0+1; i1<n; i1++) {
      if (propFlavourD[i1].isBlank()) continue;

      DEBUG_MSG(1, "bubR: " << i0 << i1)
      if (isOnShell(i0, i1)) {
        DEBUG_MSG(1, "skipping on-shell bubble")
        continue;
      }
      complex<T> d[1] = {};

      const Momenta LM = bubRM(i0, i1);
      const Coefficients MU = bubMU(i0, i1);
      const RealMom& K1 = PropMoms(i0, i1-1);

      complex<T> res;
      // tau2 = 2*tau1^2/tau3; tau3 = 1, -1; tau1 = 1, exp(2*Pi*I/3), exp(2*Pi*I*2/3)
      for (int aa1=0; aa1<NT1; aa1++) {
        const complex<T> tt1 = exp(i_*2.*PI*T(aa1)/T(NT1));
        for (int aa3=-1; aa3<=1; aa3+=2) {  // tt3 = -1, 1
          const complex<T> tt3 = T(aa3);
          const complex<T> tt2 = 2.*tt1*tt1/tt3;

          const ComplexMom l1 = LM[0] + tt1*LM[1] + tt2*LM[2] + tt3*LM[3];
          const ComplexMom l0 = l1 + K1;
          const complex<T> MUsq = MU[0] + tt1*MU[1] + tt1*tt1*MU[2] + tt2*tt3*MU[3];

          setLoopMass(MUsq);
          ComplexParticle L1(propFlavourD[i1], l1);
          ComplexParticle L0(propFlavourD[i0], l0);

#ifndef DEBUG_CUT
          complex<T> quickcut = T(0.);
          for (int h0=PropSpinD[i0]; h0<=1; h0+=2) {
            L0.setHelicity(h0);
            quickcut += trees.Ceval2(L0, L1, i0, i1, n);
          }
          if (primtype == MIXED) {
            quickcut *= 2.;
          }
          const complex<T> sub = Dsubtract2(l1, MUsq, i0, i1);
          res = quickcut - sub;
#else
          complex<T> cut = T(0.);
    #ifdef PEVAL
          cut = -i_*trees.Peval2(L0, L1, i0, i1, n);
    #else
          for (int h1=PropSpinD[i1]; h1<=1; h1+=2) {
            for (int h0=PropSpinD[i0]; h0<=1; h0+=2) {
              L1.setHelicity(h1);
              L0.setHelicity(h0);
              const complex<T> tmp =
                  trees.Leval(-L0, i0, i1-1, L1)
                 *trees.Leval(-L1, i1, (i0-1+n)%n, L0);
              cut += -i_*tmp;
            }
          }
    #endif // PEVAL
          if (primtype == MIXED) {
            cut *= 2.;
          }

          const complex<T> sub = Dsubtract2(l1, MUsq, i0, i1);
          DEBUG_MSG(2, "cut = " << cut)
          DEBUG_MSG(2, "sub = " << sub)
          res = cut - sub;
#endif // DEBUG_CUT
          res /= T(2.*NT1);
          d[0] += res/(tt1*tt1);
        }
      }

      DEBUG_MSG(2, "d0 = " << d[0])

      Coefficients c = bubR(i0, i1);
      const Kinematics SS = bubK(i0, i1);
      c[0] = -d[0]/(3.*SS[0]);

      const complex<T> curbubR = c[0]*(SS[0] - 3.*(SS[1]+SS[2]))/6.;

      DEBUG_MSG(2, "check integrand coeffs:")
      DEBUG_MSG(2, "BUBR c9*I2[6-2e] = " << curbubR)

      bubblesR -= curbubR;
    }
  }
  DEBUG_MSG(1, "rational bub = " << bubblesR)
  value.bubR() = bubblesR;
  valueC.bubR() = conj(bubblesR);
}

template <typename T>
void NGluon2<T>::print()
{
  for (int i0=0; i0<n-3; i0++) {
    for (int i1=i0+1; i1<n-2; i1++) {
      for (int i2=i1+1; i2<n-1; i2++) {
        for (int i3=i2+1; i3<n; i3++) {
          std::cout << "COEFFS c4_" << i0 << i1 << i2 << i3 << "  = "
                    << box(i0, i1, i2, i3)[0] << ";" << std::endl;
        }
      }
    }
  }
  std::cout << std::endl;
  for (int i0=0; i0<n-2; i0++) {
    for (int i1=i0+1; i1<n-1; i1++) {
      for (int i2=i1+1; i2<n; i2++) {
        std::cout << "COEFFS c3_" << i0 << i1 << i2 << "  = "
                  << tri(i0, i1, i2)[0] << ";" << std::endl;
      }
    }
  }
  std::cout << std::endl;
  for (int i0=0; i0<n-1; i0++) {
    for (int i1=i0+1; i1<n; i1++) {
      if (isOnShell(i0, i1)) {
        continue;
      }
      std::cout << "COEFFS c2_"<< i0 << i1 << "  = "
                << bub(i0, i1)[0] << ";" << std::endl;
    }
  }
  std::cout << std::endl;
  for (int i0=0; i0<n-4; i0++) {
    for (int i1=i0+1; i1<n-3; i1++) {
      for (int i2=i1+1; i2<n-2; i2++) {
        for (int i3=i2+1; i3<n-1; i3++) {
          for (int i4=i3+1; i4<n; i4++) {
            std::cout << "COEFFS c5_" << i0 << i1 << i2 << i3 << i4 << "  = "
                      << pent(i0, i1, i2, i3, i4)[0] << ";" << std::endl;
          }
        }
      }
    }
  }
  std::cout << std::endl;
  for (int i0=0; i0<n-3; i0++) {
    for (int i1=i0+1; i1<n-2; i1++) {
      for (int i2=i1+1; i2<n-1; i2++) {
        for (int i3=i2+1; i3<n; i3++) {
          std::cout << "COEFFS c44_" << i0 << i1 << i2 << i3 << "  = "
                    << boxR(i0, i1, i2, i3)[4] << ";" << std::endl;
        }
      }
    }
  }
  std::cout << std::endl;
  for (int i0=0; i0<n-2; i0++) {
    for (int i1=i0+1; i1<n-1; i1++) {
      for (int i2=i1+1; i2<n; i2++) {
        std::cout << "COEFFS c32_" << i0 << i1 << i2 << "  = "
                  << triR(i0, i1, i2)[9] << ";" << std::endl;
      }
    }
  }
  std::cout << std::endl;
  for (int i0=0; i0<n-1; i0++) {
    for (int i1=i0+1; i1<n; i1++) {
      if (isOnShell(i0, i1)) {
        continue;
      }
      std::cout << "COEFFS c22_" << i0 << i1 << "  = "
                << bubR(i0, i1)[0] << ";" << std::endl;
    }
  }
  std::cout << std::endl;
}

// parts of the class NGluon2 are defined in separate files
#include "NGluon2-onshell.cpp"
#include "NGluon2-subtract.cpp"

#ifdef USE_SD
  template class NGluon2<double>;
#endif
#ifdef USE_DD
  template class NGluon2<dd_real>;
  template void NGluon2<dd_real>::setProcess(std::vector<Flavour<double> > const&);
  template void NGluon2<dd_real>::setProcess(const int, const Flavour<double>*);
#endif
#ifdef USE_QD
  template class NGluon2<qd_real>;
  template void NGluon2<qd_real>::setProcess(std::vector<Flavour<double> > const&);
  template void NGluon2<qd_real>::setProcess(const int, const Flavour<double>*);
#endif
#ifdef USE_VC
  template class NGluon2<Vc::double_v>;
  template void NGluon2<Vc::double_v>::setProcess(std::vector<Flavour<double> > const&);
  template void NGluon2<Vc::double_v>::setProcess(const int, const Flavour<double>*);
  template void NGluon2<Vc::double_v>::setMomenta(const MOM<double>*);
  template void NGluon2<Vc::double_v>::setMomenta(const std::vector<MOM<double> >&);
#endif
