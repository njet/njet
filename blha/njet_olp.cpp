/*
* blha/njet_olp.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012, 2013 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>

#include <cstring>
#include <iostream>
#include <fstream>
#include <sstream>
#include <limits>

#include "version.h"
#include "njet_olp.h"

#include "../ngluon2/refine.h"
#include "../ngluon2/Initialize.h"
#include "../ngluon2/Model.h"
#include "../ngluon2/Particle.h"
#include "../chsums/NJetAccuracy.h"

// lh format version (should be the same as in njet.py)
#define FORMAT_VERSION 1025

// initialize loop integrals and fpu
static Initialize global_init;
// initialize loop integrals and fpu

using namespace NJet;

using std::cout;
using std::endl;
using std::ifstream;
using std::istream;
using std::vector;
using std::string;
using std::stringstream;

void OLP_Start(const char* filename, int* status)
{
  LH_OLP::OLP_Start(filename, status);
}

void OLP_EvalSubProcess(int mcn, double* pp, double scale, double* cpl, double* rval)
{
  LH_OLP::OLP_EvalSubProcess(mcn, pp, scale, cpl, rval);
}

void OLP_Info(char* olpname, char* olpversion, char* olpmessage)
{
  LH_OLP::OLP_Info(olpname, olpversion, olpmessage);
}

void OLP_SetParameter(const char* name, const double* reval,
                      const double* imval, int* rstatus)
{
  LH_OLP::OLP_SetParameter(name, reval, imval, rstatus);
}

void OLP_EvalSubProcess2(const int* pmcn, const double* pp,
                         const double* pscale, double* rval, double* racc)
{
  LH_OLP::OLP_EvalSubProcess2(pmcn, pp, pscale, rval, racc);
}

void OLP_Polvec(const double* p, const double* q, double* eps)
{
  LH_OLP::OLP_Polvec(p, q, eps);
}

void olpstart_(const char* filename, int* status, int slen)
{
  char* cfilename = new char[slen+1];
  for (int i=0; i<slen; i++) {
    cfilename[i] = filename[i];
  }
  cfilename[slen] = '\x0';
  LH_OLP::OLP_Start(cfilename, status);
  delete[] cfilename;
}

void olpevalsubprocess_(int* mcn, double* pp, double* scale, double* cpl, double* rval)
{
  LH_OLP::OLP_EvalSubProcess(*mcn, pp, *scale, cpl, rval);
}

void olpinfo_(char* olpname, char* olpversion, char* olpmessage)
{
  LH_OLP::OLP_Info(olpname, olpversion, olpmessage);
}

void olpsetparameter_(const char* name, const double* reval,
                      const double* imval, int* rstatus)
{
  LH_OLP::OLP_SetParameter(name, reval, imval, rstatus);
}

void olpevalsubprocess2_(const int* pmcn, const double* pp,
                         const double* pscale, double* rval, double* racc)
{
  LH_OLP::OLP_EvalSubProcess2(pmcn, pp, pscale, rval, racc);
}

namespace NJet
{
static LH_OLP Cleanup;
}

NJet::Contract* LH_OLP::njet_global = 0;

LH_OLP::~LH_OLP()
{
  delete njet_global;
};

void LH_OLP::OLP_Start(const char* filename, int* status)
{
  *status = 0;
  if (filename == 0) return;
  if (njet_global) {
    delete njet_global;
    njet_global = 0;
  }
  njet_global = new Contract(filename);
  if (njet_global->is_valid()) {
    *status = 1;
  }
  else {
    delete njet_global;
    njet_global = 0;
  }
}

void LH_OLP::OLP_EvalSubProcess(int mcn, const double* pp, double scale,
                                const double* cpl, double* rval)
{
  if (njet_global == 0) {
    cout << "Error: NJet_OLP is not initialized, call OLP_Start first" << endl;
    return;
  }
  if (cpl) {
    int rstatus;
    double impart = 0.;
    LH_OLP::OLP_SetParameter("alphas", &cpl[0], &impart, &rstatus);
    if (njet_global->modeEnabled(Contract::MODE_TWO_COUPLINGS)) {
      LH_OLP::OLP_SetParameter("alpha", &cpl[1], &impart, &rstatus);
    }
  }
  LH_OLP::OLP_EvalSubProcess2(&mcn, pp, &scale, rval, 0);
}

void LH_OLP::OLP_Info(char* olpname, char* olpversion, char* olpmessage)
{
  static const char* name = "NJet";
  static const char* version = PACKAGE_VERSION;
  static const char* message = "\\cite{Badger:2012pg}";
  strcpy(olpname, name);
  strcpy(olpversion, version);
  strcpy(olpmessage, message);
#ifdef HAVE_VERSION_H
  char* sub = strstr(olpversion, "git");
  if (sub) {
    const int maxlen = BLHA_VERSION_LEN-1;
    const int len = maxlen - (sub - olpversion);
    strncpy(sub, GIT_VERSION, len);
    olpversion[maxlen] = '\x00';
  }
#endif
}

void LH_OLP::OLP_SetParameter(const char* name, const double* reval,
                              const double* imval, int* rstatus)
{
  if (njet_global == 0) {
    cout << "Error: NJet_OLP is not initialized, call OLP_Start first" << endl;
    return;
  }
  njet_global->SetParameter(name, reval, imval, rstatus);
}

void LH_OLP::OLP_EvalSubProcess2(const int* pmcn, const double* pp,
                                 const double* pscale, double* rval, double* racc)
{
  if (njet_global == 0) {
    cout << "Error: NJet_OLP is not initialized, call OLP_Start first" << endl;
    return;
  }
  const int mcn = pmcn[0];
  if (njet_global->procs[mcn] == 0) {
    cout << "Error: process number not found (" << mcn << ") check your contract file" << endl;
    return;
  }
  njet_global->procs[mcn]->eval(pp, pscale[0], rval, racc);
}

void LH_OLP::OLP_Polvec(const double* p, const double* q, double* eps)
{
  if (njet_global == 0) {
    cout << "Error: NJet_OLP is not initialized, call OLP_Start first" << endl;
    return;
  }
  const MOM<double> mom = MOM<double>(p[0], p[1], p[2], p[3]);
  MOM<double> ref = Particle<double>::refvec;
  if (q != 0) {
    ref = MOM<double>(q[0], q[1], q[2], q[3]);
  }
  const SubCurrent<double> polvec = EPS(njet_global->param_h, mom, ref);
  for (int i = 0; i < 4; i++) {
    eps[2*i+0] = polvec.data(i).real();
    eps[2*i+1] = polvec.data(i).imag();
  }
}

// --------------------------------------------------------------------------
// --------------------------------------------------------------------------

std::vector<double> Process::value_buffer;
std::vector<double> Process::error_buffer;

Process::Process(Contract* njetc_, int ppn_, stringstream& ss, bool &success)
  : njetc(njetc_), ppn(ppn_), legs(0), stat()
{
  // decode process parameters
  ss >> rfactor >> flags >> type;
  char bra = 0;
  ss >> bra;
  success &= (bra == '(');

  while (ss.peek() != ')' && (success &= ss.good())) {
    perm.push_back(0);
    ss >> perm[legs++];
  }
  ss >> bra;
  success &= (bra == ')');
  // process parameters end

  success &= refresh_amps();

  if (not success) {
    return;
  }

  mom_sd.resize(legs);
#ifdef USE_DD
  mom_dd.resize(legs);
#ifdef USE_QD
  mom_qd.resize(legs);
#endif // USE_QD
#endif // USE_DD
}

bool Process::refresh_amps()
{
  bool success = true;

  amp_sd = njetc->njet_sd.getAmp(ppn);
  success &= (amp_sd != 0);
#ifdef USE_VC
  amp_vc = njetc->njet_vc.getAmp(ppn);
  success &= (amp_vc != 0);
#endif // USE_VC
#ifdef USE_DD
  if (njetc->mlevel >= 1) {
    amp_dd = njetc->njet_dd.getAmp(ppn);
    success &= (amp_dd != 0);
  }
#ifdef USE_QD
  if (njetc->mlevel >= 2) {
    amp_qd = njetc->njet_qd.getAmp(ppn);
    success &= (amp_qd != 0);
  }
#endif // USE_QD
#endif // USE_DD

  if (success) {
    const int buffer_size = amp_sd->get_buffer_size();
    if (int(value_buffer.size()) < buffer_size) {
      value_buffer.resize(buffer_size);
      error_buffer.resize(buffer_size);
    }
  }

  return success;
}

void Process::setMom(const double* pp, vector<SDMomentum>& mom)
{
  int ix = 1;
  int iy = 2;
  int iz = 3;
  for (int n=0; n<legs; n++) {
    const int nn = perm[n];
    const double* const pm = &pp[5*(abs(nn)-1)];
    if (abs(pm[0]) == abs(pm[SPIN_AXIS])) {
      std::swap(ix, iy);
      std::swap(iy, iz);
      break;
    }
  }
  for (int n=0; n<legs; n++) {
    const int nn = perm[n];
    if (nn > 0) {
      const double* const pm = &pp[5*(nn-1)];
      mom[n] = SDMomentum(pm[0], pm[ix], pm[iy], pm[iz]);
    } else {
      const double* const pm = &pp[5*(-nn-1)];
      mom[n] = SDMomentum(-pm[0], -pm[ix], -pm[iy], -pm[iz]);
    }
  }
}

template <typename T>
bool Process::aboveThreshold(const EpsTriplet<T>& val, const EpsTriplet<T>& err, const double th)
{
  // check only finite part
  return abs(err.get0().real()/val.get0().real()) > th;
}

void Process::eval(const double* pp, double scale, double* rval, double* racc)
{
  njetc->set_runtime();

  if (racc) {
    racc[0] = 0.;
  }

  switch (type) {
    case TYPE_LOOP:
      eval_loop(pp, scale, rval, racc);
      break;

    case TYPE_TREE:
      eval_tree(pp, scale, rval, racc);
      break;

    case TYPE_CCTREE:
      eval_cctree(pp, scale, rval, racc);
      break;

    case TYPE_CCTREEIJ:
      eval_cctreeij(pp, scale, rval, racc);
      break;

    case TYPE_SCTREE:
      eval_sctree(pp, scale, rval, racc);
      break;

    case TYPE_SCTREEIJ:
      eval_sctreeij(pp, scale, rval, racc);
      break;

    case TYPE_CSTREE:
      eval_cstree(pp, scale, rval, racc);
      break;

    case TYPE_CSTREEI:
      eval_cstreei(pp, scale, rval, racc);
      break;

    default:
      eval_zero(pp, scale, rval, racc);
      break;
  }
}

void Process::setCouplings()
{
  double ee2;
  if (njetc->modeEnabled(Contract::MODE_ADD_COUPLINGS)) {
    gs2 = 4.*M_PI*njetc->alphas;
    ee2 = 4.*M_PI*njetc->alpha;
  } else {
    gs2 = 4.*M_PI;
    ee2 = 4.*M_PI;
  }
  const int alphas_pow = amp_sd->getRenorm().getAlphasPow();
  const int alpha_pow = legs - (alphas_pow + 2);
  tree_cpl = pow(gs2, alphas_pow);
  tree_cpl *= pow(ee2, alpha_pow);
  if (flags & PROC_INFO_W) {
    const double sw2 = StandardModel::getSinThetaWSq();
    tree_cpl /= sw2*sw2;
  }
  tree_cpl /= double(rfactor);
}

void Process::eval_zero(const double* pp, double scale, double* rval, double* racc)
{
  rval[3] = 1.;

  if (njetc->modeEnabled(Contract::MODE_ZERO_HAS_TREE)) {
    eval_tree(pp, scale, rval, racc);
    rval[3] = rval[0];
  }

  if (njetc->modeEnabled(Contract::MODE_ZERO_IS_POSITIVE)) {
    const double minval = sqrt(std::numeric_limits<double>::min());
    rval[0] = minval;
    rval[1] = minval;
    rval[2] = minval;
  } else {
    rval[0] = 0.;
    rval[1] = 0.;
    rval[2] = 0.;
  }

  if (njetc->scheme == NJetRenorm<double>::SCHEME_ZERO) {
    setCouplings();
    EpsTriplet<double> val = EpsTriplet<double>(rval[2], rval[1], rval[0]);
    val = amp_sd->getRenorm().Renormalize(rval[3], val);
    val *= gs2;
    if (not njetc->modeEnabled(Contract::MODE_OMIT_16PISQ)) {
      const double norm = 16.*M_PI*M_PI;
      val /= norm;
    }

    rval[0] = val.get2().real();
    rval[1] = val.get1().real();
    rval[2] = val.get0().real();
  }

  njetc->last_proc = this;
}

void Process::eval_tree(const double* pp, double /*scale*/, double* rval, double* /*racc*/)
{
  bool have_accuracy;
//   bool failed;

  setCouplings();
  setMom(pp, mom_sd);

  double born_val_sd;
  double born_err_sd;

  have_accuracy = false;  // trees do not run scaling test FIXME

  if (njetc->modeEnabled(Contract::MODE_REFINE_DBL_MOM)) {
    refineM(mom_sd, mom_sd, njetc->scales2);
  }

  if (have_accuracy) {
#ifdef USE_VC
    if (njetc->modeEnabled(Contract::MODE_ENABLE_VC)) {
      amp_vc->setMomenta(mom_sd);
      born_val_sd = amp_vc->born();
      born_err_sd = amp_vc->born_error();
    }
    else
#endif
    {
      amp_sd->setMomenta(mom_sd);
      born_val_sd = amp_sd->born();
      born_err_sd = amp_sd->born_error();
    }
  } else {
    amp_sd->setMomenta(mom_sd);
    born_val_sd = amp_sd->born_single();
    born_err_sd = 0.;
  }
  rval[0] = born_val_sd;

  // FIXME Do we want DD here?

  rval[0] *= tree_cpl;

  njetc->last_proc = this;
}

void Process::eval_cctree(const double* pp, double /*scale*/, double* rval, double* /*racc*/)
{
  bool have_accuracy;
//   bool failed;

  setCouplings();
  setMom(pp, mom_sd);

  have_accuracy = false;  // trees do not run scaling test FIXME

  if (njetc->modeEnabled(Contract::MODE_REFINE_DBL_MOM)) {
    refineM(mom_sd, mom_sd, njetc->scales2);
  }

  if (have_accuracy) {
#ifdef USE_VC
    if (njetc->modeEnabled(Contract::MODE_ENABLE_VC)) {
      amp_vc->setMomenta(mom_sd);
      amp_vc->born_cc(value_buffer.data(), error_buffer.data());
    }
    else
#endif
    {
      amp_sd->setMomenta(mom_sd);
      amp_sd->born_cc(value_buffer.data(), error_buffer.data());
    }
  } else {
    amp_sd->setMomenta(mom_sd);
    amp_sd->born_cc_single(value_buffer.data());
  }

  // FIXME Do we want DD here?

  // (assuming that non-QCD legs are at the end)
  int legsQCD = amp_sd->getRenorm().legsQCD();
  for (int i=1; i<legsQCD; i++) {
    const int pi = abs(perm[i])-1;
    for (int j=0; j<i; j++) {
      const int pj = abs(perm[j])-1;
      rval[nis(pi,pj)] = -value_buffer[nis(i,j)]*tree_cpl;
    }
  }

  njetc->last_proc = this;
}

void Process::eval_sctree(const double* pp, double /*scale*/, double* rval, double* /*racc*/)
{
  bool have_accuracy;
//   bool failed;

  setCouplings();
  setMom(pp, mom_sd);

  have_accuracy = false;  // trees do not run scaling test FIXME

  if (njetc->modeEnabled(Contract::MODE_REFINE_DBL_MOM)) {
    refineM(mom_sd, mom_sd, njetc->scales2);
  }

  std::complex<double>* cval_buf = reinterpret_cast<std::complex<double>*>(value_buffer.data());
  std::complex<double>* cerr_buf = reinterpret_cast<std::complex<double>*>(error_buffer.data());
  if (have_accuracy) {
#ifdef USE_VC
    if (njetc->modeEnabled(Contract::MODE_ENABLE_VC)) {
      amp_vc->setMomenta(mom_sd);
      amp_vc->born_sc(cval_buf, cerr_buf);
    }
    else
#endif
    {
      amp_sd->setMomenta(mom_sd);
      amp_sd->born_sc(cval_buf, cerr_buf);
    }
  } else {
    amp_sd->setMomenta(mom_sd);
    amp_sd->born_sc_single(cval_buf);
  }

  // FIXME Do we want DD here?

  // (assuming that non-QCD legs are at the end)
  int legsQCD = amp_sd->getRenorm().legsQCD();
  for (int i=0; i<legsQCD; i++) {
    const int pi = abs(perm[i])-1;
    for (int j=0; j<legsQCD; j++) {
      const int pj = abs(perm[j])-1;
      rval[2*(pi+legs*pj)+0] = value_buffer[2*(i+legsQCD*j)+0]*tree_cpl;
      rval[2*(pi+legs*pj)+1] = value_buffer[2*(i+legsQCD*j)+1]*tree_cpl;
    }
  }

  njetc->last_proc = this;
}

void Process::eval_cstree(const double* pp, double /*scale*/, double* rval, double* /*racc*/)
{
  bool have_accuracy;
//   bool failed;

  setCouplings();
  setMom(pp, mom_sd);

  have_accuracy = false;  // trees do not run scaling test FIXME

  if (njetc->modeEnabled(Contract::MODE_REFINE_DBL_MOM)) {
    refineM(mom_sd, mom_sd, njetc->scales2);
  }

  std::complex<double>* cval_buf = reinterpret_cast<std::complex<double>*>(value_buffer.data());
  std::complex<double>* cerr_buf = reinterpret_cast<std::complex<double>*>(error_buffer.data());
  if (have_accuracy) {
#ifdef USE_VC
    if (njetc->modeEnabled(Contract::MODE_ENABLE_VC)) {
      amp_vc->setMomenta(mom_sd);
      amp_vc->born_cs(cval_buf, cerr_buf);
    }
    else
#endif
    {
      amp_sd->setMomenta(mom_sd);
      amp_sd->born_cs(cval_buf, cerr_buf);
    }
  } else {
    amp_sd->setMomenta(mom_sd);
    amp_sd->born_cs_single(cval_buf);
  }

  // FIXME Do we want DD here?

  // (assuming that non-QCD legs are at the end)
  int legsQCD = amp_sd->getRenorm().legsQCD();
  for (int i=0; i<legsQCD; i++) {
    const int pi = abs(perm[i])-1;
    for (int j=0; j<32; j++) {
      rval[pi*32+j] = value_buffer[i*32+j]*tree_cpl;
    }
  }

  njetc->last_proc = this;
}

void Process::eval_cctreeij(const double* pp, double /*scale*/, double* rval, double* /*racc*/)
{
  // first check that given i,j makes sense
  // (assuming that non-QCD legs are at the end)
  int legsQCD = amp_sd->getRenorm().legsQCD();
  int i = 0;
  int j = 0;
  while (i < legsQCD and abs(perm[i])-1 != njetc->param_i) {
    i++;
  }
  while (j < legsQCD and abs(perm[j])-1 != njetc->param_j) {
    j++;
  }
  if (i >= legsQCD or j >= legsQCD) {
    rval[0] = 0.;
    return;
  }

  bool have_accuracy;
//   bool failed;

  setCouplings();
  setMom(pp, mom_sd);

  have_accuracy = false;  // trees do not run scaling test FIXME

  if (njetc->modeEnabled(Contract::MODE_REFINE_DBL_MOM)) {
    refineM(mom_sd, mom_sd, njetc->scales2);
  }

  if (have_accuracy) {
#ifdef USE_VC
    if (njetc->modeEnabled(Contract::MODE_ENABLE_VC)) {
      amp_vc->setMomenta(mom_sd);
      value_buffer[0] = amp_vc->born_ccij(i, j);
      error_buffer[0] = amp_vc->born_ccij_error();
    }
    else
#endif
    {
      amp_sd->setMomenta(mom_sd);
      value_buffer[0] = amp_sd->born_ccij(i, j);
      error_buffer[0] = amp_sd->born_ccij_error();
    }
  } else {
    amp_sd->setMomenta(mom_sd);
    value_buffer[0] = amp_sd->born_ccij_single(i, j);
  }

  // FIXME Do we want DD here?

  rval[0] = -value_buffer[0]*tree_cpl;

  njetc->last_proc = this;
}

void Process::eval_sctreeij(const double* pp, double /*scale*/, double* rval, double* /*racc*/)
{
  // first check that given i,j makes sense
  // (assuming that non-QCD legs are at the end)
  int legsQCD = amp_sd->getRenorm().legsQCD();
  int i = 0;
  int j = 0;
  while (i < legsQCD and abs(perm[i])-1 != njetc->param_i) {
    i++;
  }
  while (j < legsQCD and abs(perm[j])-1 != njetc->param_j) {
    j++;
  }
  if (i >= legsQCD or j >= legsQCD) {
    rval[0] = 0.;
    rval[1] = 0.;
    return;
  }

  bool have_accuracy;
//   bool failed;

  setCouplings();
  setMom(pp, mom_sd);

  have_accuracy = false;  // trees do not run scaling test FIXME

  if (njetc->modeEnabled(Contract::MODE_REFINE_DBL_MOM)) {
    refineM(mom_sd, mom_sd, njetc->scales2);
  }

  std::complex<double>* cval_buf = reinterpret_cast<std::complex<double>*>(value_buffer.data());
  std::complex<double>* cerr_buf = reinterpret_cast<std::complex<double>*>(error_buffer.data());
  if (have_accuracy) {
#ifdef USE_VC
    if (njetc->modeEnabled(Contract::MODE_ENABLE_VC)) {
      amp_vc->setMomenta(mom_sd);
      cval_buf[0] = amp_vc->born_scij(i, j);
      cerr_buf[0] = amp_vc->born_scij_error();
    }
    else
#endif
    {
      amp_sd->setMomenta(mom_sd);
      cval_buf[0] = amp_sd->born_scij(i, j);
      cerr_buf[0] = amp_sd->born_scij_error();
    }
  } else {
    amp_sd->setMomenta(mom_sd);
    cval_buf[0] = amp_sd->born_scij_single(i, j);
  }

  // FIXME Do we want DD here?

  rval[0] = value_buffer[0]*tree_cpl;
  rval[1] = value_buffer[1]*tree_cpl;

  njetc->last_proc = this;
}

void Process::eval_cstreei(const double* pp, double /*scale*/, double* rval, double* /*racc*/)
{
  // first check that given i makes sense
  // (assuming that non-QCD legs are at the end)
  int legsQCD = amp_sd->getRenorm().legsQCD();
  int i = 0;
  while (i < legsQCD and abs(perm[i])-1 != njetc->param_i) {
    i++;
  }
  if (i >= legsQCD) {
    for (int k = 0; k < 32; k++) {
      rval[k] = 0.;
    }
    return;
  }

  bool have_accuracy;
//   bool failed;

  setCouplings();
  setMom(pp, mom_sd);

  have_accuracy = false;  // trees do not run scaling test FIXME

  if (njetc->modeEnabled(Contract::MODE_REFINE_DBL_MOM)) {
    refineM(mom_sd, mom_sd, njetc->scales2);
  }

  std::complex<double>* cval_buf = reinterpret_cast<std::complex<double>*>(value_buffer.data());
  std::complex<double>* cerr_buf = reinterpret_cast<std::complex<double>*>(error_buffer.data());
  if (have_accuracy) {
#ifdef USE_VC
    if (njetc->modeEnabled(Contract::MODE_ENABLE_VC)) {
      amp_vc->setMomenta(mom_sd);
      amp_vc->born_csi(i, cval_buf, cerr_buf);
    }
    else
#endif
    {
      amp_sd->setMomenta(mom_sd);
      amp_sd->born_csi(i, cval_buf, cerr_buf);
    }
  } else {
    amp_sd->setMomenta(mom_sd);
    amp_sd->born_csi_single(i, cval_buf);
  }

  // FIXME Do we want DD here?

  for (int k=0; k<32; k++) {
    rval[i] = value_buffer[k]*tree_cpl;
  }

  njetc->last_proc = this;
}

void Process::eval_loop(const double* pp, double scale, double* rval, double* racc)
{
  bool have_accuracy;
  bool failed;
  double localacc[3];

  const double scale2 = scale*scale;

  setCouplings();
  setMom(pp, mom_sd);

  EpsTriplet<double> virt_val_sd;
  EpsTriplet<double> virt_err_sd;
  double born_val_sd;

  have_accuracy = true;  // sd always run scaling test

  if (njetc->modeEnabled(Contract::MODE_REFINE_DBL_MOM)) {
    refineM(mom_sd, mom_sd, njetc->scales2);
  }

  if (have_accuracy) {
#ifdef USE_VC
    if (njetc->modeEnabled(Contract::MODE_ENABLE_VC)) {
      amp_vc->setMuR2(scale2);
      amp_vc->setMomenta(mom_sd);

      virt_val_sd = amp_vc->virt();
      virt_err_sd = amp_vc->virt_error();
      born_val_sd = amp_vc->born_value();  // computed by virt above
      stat.count_sdvc++;
    }
    else
#endif
    {
      amp_sd->setMuR2(scale2);
      amp_sd->setMomenta(mom_sd);

      virt_val_sd = amp_sd->virt();
      virt_err_sd = amp_sd->virt_error();
      born_val_sd = amp_sd->born_value();  // computed by virt above
      stat.count_sd2++;
    }
  } else {
    amp_sd->setMuR2(scale2);
    amp_sd->setMomenta(mom_sd);

    virt_val_sd = amp_sd->virt_single();
    virt_err_sd = EpsTriplet<double>();
    born_val_sd = amp_sd->born_value();  // computed by virt_single above
    stat.count_sd1++;
  }

  rval[0] = virt_val_sd.get2().real();
  rval[1] = virt_val_sd.get1().real();
  rval[2] = virt_val_sd.get0().real();
  rval[3] = born_val_sd;

  // check whether SD accuracy is worse than th1
  if (have_accuracy and aboveThreshold(virt_val_sd, virt_err_sd, njetc->th1)) {
    failed = true;
  } else {
    failed = false;
  }

  if (have_accuracy) {
    localacc[0] = abs(virt_err_sd.get2().real()/virt_val_sd.get2().real());
    localacc[1] = abs(virt_err_sd.get1().real()/virt_val_sd.get1().real());
    localacc[2] = abs(virt_err_sd.get0().real()/virt_val_sd.get0().real());
  } else {
    localacc[0] = 0.;
    localacc[1] = 0.;
    localacc[2] = 0.;
  }

#ifdef USE_DD
  EpsTriplet<dd_real> virt_val_dd;
  EpsTriplet<dd_real> virt_err_dd;
  dd_real born_val_dd;

  // If SD failed and DD is enabled
  if (failed and njetc->mlevel >= 1) {
    // if accuracy is forced
    // or QD is enabled
    // or SD accuracy is worse than th2, run scaling test
    have_accuracy = (njetc->accmode == RACC_ALWAYS) ||
                    (njetc->mlevel > 1) ||
                    aboveThreshold(virt_val_sd, virt_err_sd, njetc->th2);

    refineM(mom_sd, mom_dd, njetc->scales2);
    amp_dd->setMuR2(scale2);
    amp_dd->setMomenta(mom_dd);

    // clear amp_sd cache here to leave more mem for amp_dd
    if (have_accuracy) {
      virt_val_dd = amp_dd->virt();
      virt_err_dd = amp_dd->virt_error();
      born_val_dd = amp_dd->born_value();  // computed by virt above
      stat.count_dd2++;
    } else {
      virt_val_dd = amp_dd->virt_single();
      virt_err_dd = EpsTriplet<dd_real>();
      born_val_dd = amp_dd->born_value();  // computed by virt_single above
      stat.count_dd1++;
    }
    // clear amp_dd cache here to leave more mem

    rval[0] = to_double(virt_val_dd.get2().real());
    rval[1] = to_double(virt_val_dd.get1().real());
    rval[2] = to_double(virt_val_dd.get0().real());
    rval[3] = to_double(born_val_dd);

    // check if DD accuracy is worse than th1
    if (have_accuracy and aboveThreshold(virt_val_dd, virt_err_dd, njetc->th1)) {
      failed = true;
    } else {
      failed = false;
    }

    if (have_accuracy) {
      localacc[0] = to_double(abs(virt_err_dd.get2().real()/virt_val_dd.get2().real()));
      localacc[1] = to_double(abs(virt_err_dd.get1().real()/virt_val_dd.get1().real()));
      localacc[2] = to_double(abs(virt_err_dd.get0().real()/virt_val_dd.get0().real()));
    } else {
      localacc[0] = 0.;
      localacc[1] = 0.;
      localacc[2] = 0.;
    }
  }

#ifdef USE_QD
  EpsTriplet<qd_real> virt_val_qd;
  EpsTriplet<qd_real> virt_err_qd;
  qd_real born_val_qd;

  // If DD failed and QD is enabled
  if (failed and njetc->mlevel >= 2) {
    // if accuracy is forced
    // or GMP is enabled
    // or DD accuracy is worse than th2, run scaling test
    have_accuracy = (njetc->accmode == RACC_ALWAYS) ||
                    (njetc->mlevel > 2) ||
                    aboveThreshold(virt_val_dd, virt_err_dd, njetc->th2);

    refineM(mom_dd, mom_qd, njetc->scales2);
    amp_qd->setMuR2(scale2);
    amp_qd->setMomenta(mom_qd);

    if (have_accuracy) {
      virt_val_qd = amp_qd->virt();
      virt_err_qd = amp_qd->virt_error();
      born_val_qd = amp_qd->born_value();  // computed by virt above
      stat.count_qd2++;
    } else {
      virt_val_qd = amp_qd->virt_single();
      virt_err_qd = EpsTriplet<qd_real>();
      born_val_qd = amp_qd->born_value();  // computed by virt_single above
      stat.count_qd1++;
    }
    // clear amp_qd cache here to leave more mem

    rval[0] = to_double(virt_val_qd.get2().real());
    rval[1] = to_double(virt_val_qd.get1().real());
    rval[2] = to_double(virt_val_qd.get0().real());
    rval[3] = to_double(born_val_qd);

    // check if DD accuracy is worse than th1
    if (have_accuracy and aboveThreshold(virt_val_qd, virt_err_qd, njetc->th1)) {
      failed = true;
    } else {
      failed = false;
    }

    if (have_accuracy) {
      localacc[0] = to_double(abs(virt_err_qd.get2().real()/virt_val_qd.get2().real()));
      localacc[1] = to_double(abs(virt_err_qd.get1().real()/virt_val_qd.get1().real()));
      localacc[2] = to_double(abs(virt_err_qd.get0().real()/virt_val_qd.get0().real()));
    } else {
      localacc[0] = 0.;
      localacc[1] = 0.;
      localacc[2] = 0.;
    }
  }
#endif // USE_QD
#endif // USE_DD

  rval[0] *= gs2*tree_cpl;
  rval[1] *= gs2*tree_cpl;
  rval[2] *= gs2*tree_cpl;
  rval[3] *= tree_cpl;

  if (not njetc->modeEnabled(Contract::MODE_OMIT_16PISQ)) {
    const double norm = 16.*M_PI*M_PI;
    rval[0] /= norm;
    rval[1] /= norm;
    rval[2] /= norm;
  }

  if (racc) {  // return accuracy of the ep0 for BLHA2
    racc[0] = localacc[2];
  }
  if (njetc->accmode != RACC_NO) {  // return accuracy in rval if requested
    rval[4] = localacc[0];
    rval[5] = localacc[1];
    rval[6] = localacc[2];
  }
  if (failed) {
    // zero for BLHA2 and NaN for BLHA1
    double failval = racc ? 0. : std::numeric_limits<double>::quiet_NaN();
    if (not racc or racc[0] > 1.) {  // return failval if BLHA1 or accuracy > 1
      rval[0] = failval;
      rval[1] = failval;
      rval[2] = failval;
    }
    stat.count_failed++;
  }

  njetc->last_proc = this;
}

void Process::print_stat(std::ostream& stream, bool head)
{
  if (head) {
    stream << "  ppn" << " "
          << "    SD2" << " "
          << "    SD1" << " "
          << "   DD2" << " "
          << "   DD1" << " "
          << "  QD2" << " "
          << "  QD1" << " "
          << " FAIL";
  } else {
    stream.width(5);
    stream << ppn << " ";
    stream.width(7);
    stream << stat.count_sd2 + stat.count_sdvc << " ";
    stream.width(7);
    stream << stat.count_sd1 << " ";
    stream.width(6);
    stream << stat.count_dd2 << " ";
    stream.width(6);
    stream << stat.count_dd1 << " ";
    stream.width(5);
    stream << stat.count_qd2 << " ";
    stream.width(5);
    stream << stat.count_qd1 << " ";
    stream.width(5);
    stream << stat.count_failed;
  }
}

// --------------------------------------------------------------------------
// --------------------------------------------------------------------------

const string Contract::SIGNPREF=string("# Signed by NJet ");

Contract::Contract(const char* filename)
  : valid(false), runtime(false), dirty(false),
    alphas(1.), alpha(1.), param_i(0), param_j(0), param_h(1),
    procs(), last_proc(0)
{
  set_mass_scales();

  stringstream iss;
  ifstream ifs;

  istream* input;

  if (string(filename).find('\n') != string::npos) {
    iss.str(string(filename));
    input = &iss;
  } else {
    ifs.open(filename, ifstream::in | ifstream::binary);
    input = &ifs;
    if (not ifs.good()) {
      cout << "Error: cannot open file " << filename << endl;
      return;
    }
  }

  bool is_good = input->good();
  bool is_njet = false;
  bool is_init = true;

  vector<string> body;
  int first_proc = -1;

  string line;
  int linenum = 0;

  while (is_good && (is_good = static_cast<bool>(getline(*input, line)))) {
    linenum++;
    if (line.find(SIGNPREF) == 0) {
      is_njet = true;
      if (stringstream(line.substr(SIGNPREF.length())) >> signval) {
        is_good = static_cast<bool>(getline(*input, line));
        linenum++;
        body.push_back(line);
        continue;
      }
      else {
        cout << "Error: invalid NJet contract file, line " << linenum << endl;
        break;
      }
    }
    if (is_njet) {
      size_t nonspace_pos, comment_pos, space_pos;

      nonspace_pos = line.find_first_not_of(" \t");
      comment_pos = line.find_first_of("#", nonspace_pos);
      if (nonspace_pos == comment_pos) continue;

      const string param = line.substr(0, comment_pos);
      if (param.find("| Error:") != string::npos) {
        cout << "Error: contract file contains unrecognized options, line " << linenum << endl;
        break;
      }

      static const string setparam_opt = string("SetParameter");
      size_t setparam_pos = param.find(setparam_opt);
      if (setparam_pos != string::npos) {
        setparam_pos += setparam_opt.size();
        nonspace_pos = param.substr(setparam_pos).find_first_not_of(" \t");
        space_pos = param.substr(setparam_pos+nonspace_pos).find_first_of(" \t");

        string setparam_name = param.substr(setparam_pos+nonspace_pos, space_pos);
        stringstream setparam_vals(param.substr(setparam_pos+nonspace_pos+space_pos));
        double reval;
        setparam_vals >> reval;

        int rstatus = STATUS_BAD;
        if (setparam_vals.good()) {
          SetParameter(setparam_name.c_str(), &reval, 0, &rstatus);
        }
        if (rstatus == STATUS_BAD) {
          cout << "Error: bad SetParameter value, line " << linenum << endl;
          break;
        }
      }

      if (is_init && param.find("| OK") == string::npos) {
        is_init = false;
        first_proc = body.size();
      }
      body.push_back(line);
    }
  }
  if (ifs.is_open()) {
    ifs.close();
  }

  // if file is not fully read (due to error) - return
  if (is_good) {
    return;
  }

  // if file is fully read but not NJet signature found
  if (not is_njet && not is_good) {
    cout << "Error: not NJet contract file, line EOF" << endl;
    return;
  }

  unsigned int calc = sign(body);
  if (calc == signval && parse(body, first_proc)) {
    valid = true;
    cout << "Info: NJet_OLP initialized" << endl;
  } else if (calc == signval) {
    cout << "Error: failed to load contract file (see messages above)" << endl;
    return;
  } else {
    cout << "Error: modified NJet contract file (" << signval << " != " << calc << ")" << endl;
    return;
  }
}

Contract::~Contract()
{
  if (modeEnabled(MODE_PRINT_STATS)) {
    bool head = true;
    for (unsigned i=0; i<procs.size(); i++) {
      if (not procs[i]) {
        continue;
      }
      if (head) {
        cout << "---------------------- NJet run stats --------------------" << endl;
        cout << " mcn" << " ";
        procs[i]->print_stat(cout, true);
        cout << endl;
        head = false;
      }
      cout.width(4);
      cout << i << " ";
      procs[i]->print_stat(cout);
      cout << endl;
    }
    if (not head) {
      cout << "----------------------------------------------------------" << endl;
    }
  }

  for (unsigned i=0; i<procs.size(); i++) {
    delete procs[i];
  }
  procs.clear();
}

unsigned int Contract::sign(vector<string> &body)
{
  const unsigned int m = 65521;
  unsigned int a = 1;
  unsigned int b = 0;

  vector<string>::iterator it;
  for (it=body.begin(); it!=body.end(); ++it) {
    const char* c = it->data();
    for (unsigned int i=0; i<it->length(); ++i) {
      a = (a + c[i]) % m;
      b = (b + a) % m;
    }
    a = (a + '\n') % m;
    b = (b + a) % m;
  }
  return (b << 16) | a;
}

bool Contract::parse(vector<string> &body, int first_proc)
{
  bool success = true;
  if (body[0].at(0) != '#') {
    return false;
  }

  // parse infoline here
  stringstream info(body[0].substr(1));
  {
    int format = 0;
    info >> format;
    if (format != FORMAT_VERSION) {
      cout << "Error: contract signed by different NJet version (run njet.py again)" << endl;
      cout << "Running version " << FORMAT_VERSION << ". Contract version " << format << "." << endl;
      return false;
    }
  }

  info >> mlevel >> th1 >> th2 >> accmode
       >> opmode >> scheme >> renorm >> Nc;

#ifndef USE_VC
  opmode &= ~MODE_ENABLE_VC;  // disable Vc if not available
#else
  if (not checkVcStatus()) {
    cout << "Warning: problem with Vc, disabling" << endl;
    opmode &= ~MODE_ENABLE_VC;  // disable Vc if not functional
  }
#endif

  assert(0 <= mlevel && mlevel <= 2);
#ifndef USE_QD
  if (mlevel >= 2) {
    cout << "Warning: no QD support, downgrading mlevel to 1" << endl;
    mlevel = 1;
  }
#endif
#ifndef USE_DD
  if (mlevel >= 1) {
    cout << "Warning: no DD support, downgrading mlevel to 0" << endl;
    mlevel = 0;
  }
#endif

  if (th1 > 0.01) {
    cout << "Warning: NJetSwitchAcc = " << th1 <<" > 0.01 is unreliable." << endl;
  }
  if (th2 > 0.01) {
    cout << "Warning: NJetExtraCheckAcc = " << th2 << " > 0.01 is unreliable." << endl;
  }

  // parse additional LH options here

  njet_sd.setNc(Nc);
  njet_sd.setScheme(scheme);
  njet_sd.setRenorm(renorm);
#ifdef USE_VC
  njet_vc.setNc(Nc);
  njet_vc.setScheme(scheme);
  njet_vc.setRenorm(renorm);
#endif
#ifdef USE_DD
  njet_dd.setNc(Nc);
  njet_dd.setScheme(scheme);
  njet_dd.setRenorm(renorm);
#ifdef USE_QD
  njet_qd.setNc(Nc);
  njet_qd.setScheme(scheme);
  njet_qd.setRenorm(renorm);
#endif
#endif

  vector<string>::iterator it;

  // find last proc str to get total number of channels
  {
    it = body.end()-1;
    while (it->find("| OK") != string::npos) {
      --it;
    }
    stringstream ss(it->substr(it->find("|")+1));
    int count(0), mcn(0);
    ss >> count >> mcn;
    // resize procs array (NB MCn starts from 1)
    procs.assign(count + mcn, static_cast<Process*>(0));
  }

  int i = 0;
  for (it=body.begin()+first_proc; it!=body.end(); ++it) {
    if (it->find("| OK") != string::npos) {  // skip strings with local parameters
      continue;
    }
    i++;
    stringstream ss(it->substr(it->find("|")+1));
    int one(0), mcn(0), ppn(0);
    ss >> one >> mcn;
    assert(one == 1);
    assert(mcn == i && unsigned(mcn) < procs.size());
    while (ss && ss.get() != '#') {};
    ss >> ppn;
    if (not procs[mcn]) {
      procs[mcn] = new Process(this, ppn, ss, success);
    }
    if (not success) {
      cout << "Error: process not supported " << *it << endl;
      break;
    }
  }

  return success;
}

void Contract::set_mass_scales()
{
  scales2.clear();
  scales2.push_back(0.);
  const double mH = StandardModel::getHmass();
  scales2.push_back(mH*mH);
}

void Contract::set_runtime()
{
  if (not runtime and dirty) {
    njet_sd.clear();
#ifdef USE_VC
    njet_vc.clear();
#endif
#ifdef USE_DD
    njet_dd.clear();
#ifdef USE_QD
    njet_qd.clear();
#endif
#endif
    set_mass_scales();
    for (unsigned i=0; i<procs.size(); i++) {
      if (procs[i]) {
        procs[i]->refresh_amps();
      }
    }
    dirty = false;
  }
  runtime = true;
}

void Contract::setNf(double nf)
{
  njet_sd.setNf(nf);
#ifdef USE_VC
  njet_vc.setNf(nf);
#endif
#ifdef USE_DD
  njet_dd.setNf(nf);
#ifdef USE_QD
  njet_qd.setNf(nf);
#endif
#endif
}

void Contract::SetParameter(const char* name, const double* reval,
                            const double* imval, int* rstatus)
{
  rstatus[0] = STATUS_IGNORE;
  if (not runtime) {
    StaticParam(name, reval, imval, rstatus);
  }
  DynamicParam(name, reval, imval, rstatus);
}

void Contract::StaticParam(const char* name, const double* reval,
                           const double* imval, int* rstatus)
{
  if (0 == strcmpi(name, "mass(6)")) {
    if ((imval && imval[0] != 0.) || reval[0] < 0.) {
      cout << "Warning: set mass(6) to real non-negative value" << endl;
      rstatus[0] = STATUS_BAD;
      return;
    }
    if (reval[0] < 100000.) {
      cout << "Warning: set mass(6) larger than 100 TeV for correct running of AlphaS" << endl;
      rstatus[0] = STATUS_BAD;
      return;
    }
// NOTE: uncomment for massive T-quarks
//     StandardModel::setTmass(reval[0]);
//     dirty = true;
    return;
  }
  if (0 == strcmpi(name, "mass(23)")) {
    if ((imval && imval[0] != 0.) || reval[0] < 0.) {
      cout << "Warning: set " << name << " to real non-negative value" << endl;
      rstatus[0] = STATUS_BAD;
      return;
    }
    StandardModel::setZmass(reval[0]);
    rstatus[0] = STATUS_GOOD;
    dirty = true;
    return;
  }
  if (0 == strcmpi(name, "mass(24)")) {
    if ((imval && imval[0] != 0.) || reval[0] < 0.) {
      cout << "Warning: set " << name << " to real non-negative value" << endl;
      rstatus[0] = STATUS_BAD;
      return;
    }
    StandardModel::setWmass(reval[0]);
    rstatus[0] = STATUS_GOOD;
    dirty = true;
    return;
  }
  if (0 == strcmpi(name, "mass(25)")) {
    if ((imval && imval[0] != 0.) || reval[0] < 0.) {
      cout << "Warning: set " << name << " to real non-negative value" << endl;
      rstatus[0] = STATUS_BAD;
      return;
    }
    StandardModel::setHmass(reval[0]);
    rstatus[0] = STATUS_GOOD;
    dirty = true;
    return;
  }
  if (0 == strcmpi(name, "width(23)")) {
    if ((imval && imval[0] != 0.) || reval[0] < 0.) {
      cout << "Warning: set " << name << " to real non-negative value" << endl;
      rstatus[0] = STATUS_BAD;
      return;
    }
    StandardModel::setZwidth(reval[0]);
    rstatus[0] = STATUS_GOOD;
    dirty = true;
    return;
  }
  if (0 == strcmpi(name, "width(24)")) {
    if ((imval && imval[0] != 0.) || reval[0] < 0.) {
      cout << "Warning: set " << name << " to real non-negative value" << endl;
      rstatus[0] = STATUS_BAD;
      return;
    }
    StandardModel::setWwidth(reval[0]);
    rstatus[0] = STATUS_GOOD;
    dirty = true;
    return;
  }
  if (0 == strcmpi(name, "width(25)")) {
    if ((imval && imval[0] != 0.) || reval[0] < 0.) {
      cout << "Warning: set " << name << " to real non-negative value" << endl;
      rstatus[0] = STATUS_BAD;
      return;
    }
    StandardModel::setHwidth(reval[0]);
    rstatus[0] = STATUS_GOOD;
    dirty = true;
    return;
  }
  if (0 == strcmpi(name, "sw2")) {
    if ((imval && imval[0] != 0.) || reval[0] < 0.) {
      cout << "Warning: set " << name << " to real non-negative value" << endl;
      rstatus[0] = STATUS_BAD;
      return;
    }
    StandardModel::setSinThetaWSq(reval[0]);
    rstatus[0] = STATUS_GOOD;
    dirty = true;
    return;
  }
  // options below could be made dynamic/local if needed
  if (0 == strcmpi(name, "qcd(nf)")) {
    if ((imval && imval[0] != 0.) || reval[0] < 0. || int(reval[0]) != reval[0]) {
      cout << "Warning: set " << name << " to non-negative integer value" << endl;
      rstatus[0] = STATUS_BAD;
      return;
    }
    StandardModel::setNf(reval[0]);
    setNf(reval[0]);
    rstatus[0] = STATUS_GOOD;
    return;
  }
}

void Contract::DynamicParam(const char* name, const double* reval,
                            const double* imval, int* rstatus)
{
  switch (name[0]) {
    case 'a':
      if (0 == strcmpi(name, "alphas")) {
        assert(imval == 0 or imval[0] == 0.);
        alphas = reval[0];
        rstatus[0] = STATUS_GOOD;
        return;
      }
      if (0 == strcmpi(name, "alpha")) {
        assert(imval == 0 or imval[0] == 0.);
        alpha = reval[0];
        rstatus[0] = STATUS_GOOD;
        return;
      }
    break;

    case '#':
      if (0 == strcmpi(name, "#ij")) {
        param_i = int(reval[0]);
        param_j = int(imval[0]);
        if (param_i>=0 && param_j>=0) {
          rstatus[0] = STATUS_GOOD;
        } else {
          rstatus[0] = STATUS_BAD;
        }
        return;
      }
      if (0 == strcmpi(name, "#h")) {
        param_h = int(reval[0]);
        if (param_h == 1 or param_h == -1) {
          rstatus[0] = STATUS_GOOD;
        } else {
          rstatus[0] = STATUS_BAD;
        }
        return;
      }
    break;
  }
}

int Contract::strcmpi(const char* a, const char* b)
{
  int i;
  for (i = 0; a[i] != '\0' and b[i] != '\0'; i++) {
    if (tolower(a[i]) != tolower(b[i])) {
      return 1;
    }
  }
  return a[i] != '\0' or b[i] != '\0';
}
