
---------------------------------
Les Houches order file parameters
---------------------------------

Schematic layout of the order file
======= order.lh ========
# comment
<option> <value>
<option> <value>
<option> <value>
# comment
<process>
<process>
<local option> <value>
<local option> <value>
<process>
<local option> <value>
<process>
=========================

Global standard parameters
--------------------------

Global parameters can only appear in the beginning of the order file, before
the first process declaration.

InterfaceVersion
 - 'BLHA2' (default) interface version 2 as of June 2013
 - 'BLHA1' backward compatibility mode

Model
 - if present must be 'SM', 'SMdiag'
   this value is not used anywhere in program

CorrectionType
 - if present must be 'QCD'
   this value is not used anywhere in program

IRregularisation
 - 'CDR' (default) conventional dimensional regularization
 - 'FDH' or 'DRED'
 - 'ZERO' special scheme for AmplitudeType SLC and ZERO
   (makes their poles sensitive to renormalization)

MatrixElementSquareType (obsolete in BLHA2)
 - 'CHsummed' (default) colour/helicity-summed interference of virtual and born amplitudes,
   averaged over colour/helicity of the incoming particles

OperationMode (obsolete in BLHA2)
 - 'CouplingsStrippedOff' set alphas_s = 1 and alpha = 1


Local standard parameters
--------------------------

Local parameters can appear anywhere in the order file as they affect only process
declarations which appear after them.

In addition, extended syntax can be used to change local parameter of a specific process
(it still has to appear before that process declaration)
Extra Process <N> <Option> <Value>

AlphasPower
 - Must be set to AlphaS power of the born process

AlphaPower
 - 0 (default) set to Alpha power of the born process

AmplitudeType (supersedes NJetType)
  - 'loop' (default) standard loop amplitude

  - 'leadingpart' loop amplitude leading part (loopds or looplc depending on channel)
  - 'subleadingpart' loop amplitude subleadingpart (zero or loopslc depending on channel)
    (subleadingpart has to be be used with IRregularisation NONE or ZERO)

  - 'loopds' loop amplitude desymmetrized in final gluon states (when available)
  - 'looplc' loop amplitude in leading-colour approximation
  - 'loopslc' loop amplitude subleading-colour part
  - 'zero' return 1 for tree and 0 for loop parts (see more options below)
  - 'loopax' vector loops contribution (only for A+2j, A+3j, AA+2j)

  - 'tree' compute only tree amplitude and return as 1st element of result array
  - 'cctree' colour-correlated tree amplitudes returned as n*(n-1)/2 result array
  - 'sctree' spin-correlated trees returned as 2*n*n result array
  - 'cstree' spin-correlated CS trees returned as n*32 result array

  - 'cctreeij' single colour-correlated tree (1 result array) (to set i,j see SetParameter below)
  - 'sctreeij' single spin-correlated tree (2 result array) (to set i,j see SetParameter below)
  - 'cstreei' single spin-correlated CS tree (32 result array) (to set i see SetParameter below)


OLP_SetParameter parameters
---------------------------

These can be set in the order file using 'SetParameter <parameter> <value>'.
Ensure 'parameter' and 'value' are separated by a space, not a tab.
They can also be set in the C++ code using the BLHA2 function OLP_SetParameter,
which has interface 
'void OLP_SetParameter(char* parameter, double* re, double* im, int* return_status)'
where 're' and 'im' are the real and imaginary parts of the complex number 'value'.
The latter takes precedence if both are used.

Unknown parameters are ignored.

Static parameters:
(can be set only before first call to OLP_EvalSubProcess)

 - 'Mass(23)' mass of the Z-boson
 - 'Width(23)' width of the Z-boson

 - 'Mass(24)' mass of the W-boson
 - 'Width(24)' width of the W-boson

 - 'sw2' value of \sin^2 \theta_W

 - 'qcd(Nf)' (default 5) number of light fermion flavours contributing to fermion loops

Dynamic parameters:
(can be set for each phase-space point separately)

 - 'AlphaS' value of the strong coupling
 - 'Alpha' value of the weak coupling
 - '#ij' two integers for indexing next call to correlated trees
   (i - real argument, j - imaginary argument)


Global Extra parameters
-----------------------

Extra parameters are implementation specific and should be prefixed with
the keyword 'Extra' in the order file

Precision (aka NJetSwitchAcc)
 - '1e-5' (default) target relative accuracy of the result
   (should be at least 2 digits more than desired MC accuracy)

   If accuracy is not reached in double precision, the rescue system will make
   second attempt in higher precision (if available).

   If rescue system have failed to produce target accuracy, the program returns
   zeroes in the first three entries and the tree amplitude in the fourth:
   [0*eps^-2, 0*eps^-1, 0*eps^0, tree]

   If OLP_EvalSubProcess2 is used the last argument will contain return code.

NJetReturnAccuracy
 - '0' - no (default), '1' - when available, '2' - always
   return accuracy estimate for the eps^2,1,0 parts in the 5th,6th,7th elements of array

NJetMultiPrec
 - '1' (default,recommended) enable rescue using quadruple precision with QD library
   NB if library is compiled without extended precision it will print a warning during initialization

NJetExtraCheckAcc
 - '0.01' (default) accuracy threshold at which rescue system will do additional checks
   it is not recommended to use values greater than default setting

NJetRenormalize
 - 'yes' (default) return renormalized amplitude

NJetOmit16PiSq
 - 'no' (default) if enabled allows to omit 1/(16*pi^2) normalization factor

NJetVectorClass
 - 'yes' (default) use Vc library if available

NJetPrintStats
 - 'no' (default) print runtime statistics at the end of the run

NJetZeroHasTree
 - 'no' (default) zero-type amplitude will return full tree instead of 1

NJetZeroIsPositive
 - 'no' (default) zero-type amplitude will use tiny positive number instead of zero

NJetNf (obsoleted by SetParameter)

NJetNc
  - '3' (default) group parameter of SU(Nc). Also changes colour averaging factor of initial states.

NJetRefineMomenta - internal


Local Extra parameters
----------------------

MCSymmetrizeFinal
 - 'yes' (default) include final state identical particle symmetry factor

HelAvgInitial
 - 'yes' (default) average over helicity dof of incoming particles

ColAvgInitial
 - 'yes' (default) average over colour dof of incoming particles

NJetType (obsoleted by AmplitudeType)

