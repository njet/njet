/*
* blha/njet_olp.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012, 2013 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef BLHA_NJET_OLP_H
#define BLHA_NJET_OLP_H

#include <string>
#include <vector>

#include "njet.h"

#include "../ngluon2/Mom.h"
#include "../ngluon2/EpsTriplet.h"
#include "../chsums/NJetInterface.h"

template <typename T>
class NJetAccuracy;

namespace NJet
{

struct ProcStat
{
  int count_sd2, count_sd1, count_sdvc;
  int count_dd2, count_dd1;
  int count_qd2, count_qd1;
  int count_failed;
};

class Process
{
  public:
    Process(Contract* njetc_, int ppn_, std::stringstream& ss, bool &success);

    void eval(const double* pp, double scale, double* rval, double* racc);

    void eval_loop(const double* pp, double scale, double* rval, double* racc);
    void eval_tree(const double* pp, double scale, double* rval, double* racc);
    void eval_zero(const double* pp, double scale, double* rval, double* racc);

    void eval_cctree(const double* pp, double scale, double* rval, double* racc);
    void eval_sctree(const double* pp, double scale, double* rval, double* racc);
    void eval_cstree(const double* pp, double scale, double* rval, double* racc);

    void eval_cctreeij(const double* pp, double scale, double* rval, double* racc);
    void eval_sctreeij(const double* pp, double scale, double* rval, double* racc);
    void eval_cstreei(const double* pp, double scale, double* rval, double* racc);

    bool refresh_amps();

    void print_stat(std::ostream& stream, bool head=false);

  protected:
    typedef MOM<double> SDMomentum;
#ifdef USE_DD
    typedef MOM<dd_real> DDMomentum;
#ifdef USE_QD
    typedef MOM<qd_real> QDMomentum;
#endif // USE_QD
#endif // USE_DD

    enum RETURN_ACCURACY {
      RACC_NO      = 0,
      RACC_IFAVAIL = 1,
      RACC_ALWAYS  = 2
    };

    enum AMP_TYPE {
      TYPE_ZERO     = 0,
      TYPE_LOOP     = 1,
      TYPE_TREE     = 2,
      TYPE_CCTREE   = 3,
      TYPE_CCTREEIJ = 4,
      TYPE_SCTREE   = 5,
      TYPE_SCTREEIJ = 6,
      TYPE_CSTREE   = 7,
      TYPE_CSTREEI  = 8,
    };

    enum PROC_INFO {
      PROC_INFO_W  = 1<<0,
    };

    void setMom(const double* pp, std::vector<SDMomentum>& mom);

    template <typename T>
    bool aboveThreshold(const EpsTriplet<T>& val, const EpsTriplet<T>& err, const double th);

    double rfactor;
    int flags, type;

    Contract* const njetc;
    const int ppn;

    int legs;
    std::vector<int> perm;

    static std::vector<double> value_buffer;
    static std::vector<double> error_buffer;

    void setCouplings();
    double gs2, tree_cpl;

    ProcStat stat;

    std::vector<SDMomentum> mom_sd;
#ifdef USE_DD
    std::vector<DDMomentum> mom_dd;
#ifdef USE_QD
    std::vector<QDMomentum> mom_qd;
#endif // USE_QD
#endif // USE_DD

    NJetAccuracy<double>* amp_sd;
#ifdef USE_VC
    NJetAccuracy<Vc::double_v>* amp_vc;
#endif // USE_VC
#ifdef USE_DD
    NJetAccuracy<dd_real>* amp_dd;
#ifdef USE_QD
    NJetAccuracy<qd_real>* amp_qd;
#endif // USE_QD
#endif // USE_DD
};

class Contract
{
    friend class LH_OLP;
    friend class Process;

  public:
    Contract(const char* filename);
    ~Contract();

    void SetParameter(const char* name, const double* reval,
                      const double* imval, int* rstatus);

    bool is_valid() const {
      return valid;
    }

    bool is_runtime() const {
      return runtime;
    }

  private:
    // runtime parameters
    enum {
      STATUS_INVALID = -1,
      STATUS_BAD     = 0,
      STATUS_GOOD    = 1,
      STATUS_IGNORE  = 2,
    };

    void StaticParam(const char* name, const double* reval,
                     const double* imval, int* rstatus);
    void DynamicParam(const char* name, const double* reval,
                      const double* imval, int* rstatus);
    int strcmpi(const char* a, const char* b);

    void set_runtime();

    // contract content variables
    enum OPMODE {
      MODE_ADD_COUPLINGS     = 1<<0,
      MODE_ENABLE_VC         = 1<<1,
      MODE_REFINE_DBL_MOM    = 1<<2,
      MODE_OMIT_16PISQ       = 1<<3,
      MODE_PRINT_STATS       = 1<<4,
      MODE_ZERO_HAS_TREE     = 1<<5,
      MODE_ZERO_IS_POSITIVE  = 1<<6,
      MODE_TWO_COUPLINGS     = 1<<7,
    };

    bool modeEnabled(OPMODE m) { return bool(opmode & m); }

    bool valid, runtime, dirty;  // init status

    void setNf(double nf);

    // known external mass scales (squared)
    std::vector<double> scales2;
    void set_mass_scales();

    // dynamic parameters
    double alphas, alpha;
    int param_i, param_j, param_h;

    // contract parameters
    double Nc, Nf;
    double th1, th2;
    int mlevel, accmode, opmode, scheme, renorm;

    // contract parsing and signing
    static const std::string SIGNPREF;
    unsigned int signval;
    unsigned int sign(std::vector<std::string> &body);

    bool parse(std::vector<std::string> &body, int first_proc);

    // processes and interface objects
    std::vector<Process*> procs;  // processes, indexed with MCnum

    Process* last_proc;  // last used process

    NJetInterface<double> njet_sd;
#ifdef USE_VC
    NJetInterface<Vc::double_v> njet_vc;
#endif // USE_VC
#ifdef USE_DD
    NJetInterface<dd_real> njet_dd;
#ifdef USE_QD
    NJetInterface<qd_real> njet_qd;
#endif // USE_QD
#endif // USE_DD
};

} // namespace NJet

#endif /* BLHA_NJET_OLP_H */
