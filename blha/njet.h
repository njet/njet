/*
* blha/njet.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef BLHA_NJET_H
#define BLHA_NJET_H

#define BLHA_NAME_LEN 15
#define BLHA_VERSION_LEN 15
#define BLHA_MESSAGE_LEN 255

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
  // BLHA1 functions
  void OLP_Start(const char* filename, int* status);
  void OLP_EvalSubProcess(int mcn, double* pp, double scale, double* cpl, double* rval);
  // fortran versions
  void olpstart_(const char* filename, int* status, int slen);
  void olpevalsubprocess_(int* mcn, double* pp, double* scale, double* cpl, double* rval);

  // BLHA2 functions
  void OLP_Info(char* olpname, char* olpversion, char* olpmessage);
  void OLP_SetParameter(const char* name, const double* reval,
                        const double* imval, int* rstatus);
  void OLP_EvalSubProcess2(const int* pmcn, const double* pp,
                           const double* pscale, double* rval, double* racc);
  void OLP_Polvec(const double* p, const double* q, double* eps);
  // fortran versions
  void olpinfo_(char* olpname, char* olpversion, char* olpmessage);
  void olpsetparameter_(const char* name, const double* reval,
                        const double* imval, int* rstatus);
  void olpevalsubprocess2_(const int* pmcn, const double* pp,
                           const double* pscale, double* rval, double* racc);
#ifdef __cplusplus
}
#endif /* __cplusplus */

#ifdef __cplusplus
namespace NJet
{

class Contract;

class LH_OLP
{
  public:
    static void OLP_Start(const char* filename, int* status);
    static void OLP_EvalSubProcess(int mcn, const double* pp, double scale,
                                   const double* cpl, double* rval);

    static void OLP_Info(char* olpname, char* olpversion, char* olpmessage);
    static void OLP_SetParameter(const char* name, const double* reval,
                                 const double* imval, int* rstatus);
    static void OLP_EvalSubProcess2(const int* pmcn, const double* pp,
                                    const double* pscale, double* rval, double* racc);

    static void OLP_Polvec(const double* p, const double* q, double* eps);

    LH_OLP() {};
    ~LH_OLP();
  private:
    static Contract* njet_global;
};

// non-diagonal symmetric index: i != j, (i,j) in [0,1,...], "nis" in [0,1,...]
// also natural symmetric index: (i,j) in [1,2,...], "nis" in [1,2,...]
int nis(int i, int j)
{
  return ( i<=j ? i+j*(j-1)/2 : j+i*(i-1)/2 );
}

} /* namespace NJet */
#endif /* __cplusplus */

#endif /* BLHA_NJET_H */
