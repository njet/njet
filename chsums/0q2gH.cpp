/*
* chsums/0q2gH.cpp
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>

#include "0q2gH.h"
#include "NJetAmp-T.h"

// class Amp0q2gH

template <typename T>
Amp0q2gH<T>::Amp0q2gH(const T scalefactor,
                      const int mFC, const NJetAmpTables& tables)
  : BaseClass(mFC, tables)
{
  for (int fv=0; fv<mFC; fv++) {
    BaseClass::initNG(scalefactor, fv);
  }

  const Flavour<double> ff = StandardModel::RealFlavour(ModelBase::Higgs);
  initProcess(ff);
  initNc();
}


template <typename T>
void Amp0q2gH<T>::initProcess(const Flavour<double>& ff)
{
  for (int fv=0; fv<BaseClass::mFC; fv++) {
    Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
    flavours.push_back(ff);
    BaseClass::setProcess(flavours, fv);
  }
}

template <typename T>
void Amp0q2gH<T>::setNc(const ST Nc_)
{
  BaseClass::setNc(Nc_);
  initNc();
}

template <typename T>
void Amp0q2gH<T>::initNc()
{
  Nmat[0] = 1.;
  assert(0 < NJetAmp<T>::NmatLen);

  Nmatcc[0] = 1.;
  assert(0 < NJetAmp<T>::NmatccLen);

  bornFactor = V;
  loopFactor = 2.*bornFactor;
  bornccFactor = 0.5*V*Nc;
}

template <typename T>
typename Amp0q2gH<T>::TreeValue
Amp0q2gH<T>::A0(int p0, int p1)
{
  const int* O = getFperm(mfv);
  int order[] = {NN, O[p0], O[p1]};
  return -(2.)*BaseClass::A0nH(order);
}

template <typename T>
void Amp0q2gH<T>::getfvpart0(const int fv, TreeValue* fvpart, TreeValue* fvpartC)
{
  fvSet(fv);
  fvpart[0] = A0(0,1);

  if (fvpartC) {
    for (int i=0; i<C0; i++) {
      fvpartC[i] = conj(fvpart[i]);
    }
  }
}

#ifdef USE_SD
  template class Amp0q2gH<double>;
#endif
#ifdef USE_DD
  template class Amp0q2gH<dd_real>;
#endif
#ifdef USE_QD
  template class Amp0q2gH<qd_real>;
#endif
#ifdef USE_VC
  template class Amp0q2gH<Vc::double_v>;
#endif
