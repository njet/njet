/*
* chsums/NJetAmp-static.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>

#include "NJetAmp.h"

const unsigned int
Amp0Static::colmatds[1][1] = {};
