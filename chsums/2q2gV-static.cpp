/*
* chsums/2q2gV-static.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include "2q2gV.h"

// class Amp2q2gVStatic

const int
Amp2q2gVStatic::HSarr[HS][HSNN] = {
  { 1,-1, 1, 1, 1},
  { 1,-1, 1,-1, 1},
  { 1,-1,-1, 1, 1},
  { 1,-1,-1,-1, 1}
};

// class Amp2q2gZStatic

const int
Amp2q2gZStatic::HSarr[HS][HSNN] = {
  { 1,-1, 1, 1, 1},
  { 1,-1, 1,-1, 1},
  { 1,-1,-1, 1, 1},
  { 1,-1,-1,-1, 1},
  {-1, 1, 1, 1, 1},
  {-1, 1, 1,-1, 1},
  {-1, 1,-1, 1, 1},
  {-1, 1,-1,-1, 1},
  { 1,-1, 1, 1,-1},
  { 1,-1, 1,-1,-1},
  { 1,-1,-1, 1,-1},
  { 1,-1,-1,-1,-1},
  {-1, 1, 1, 1,-1},
  {-1, 1, 1,-1,-1},
  {-1, 1,-1, 1,-1},
  {-1, 1,-1,-1,-1},
};
