/*
* chsums/NJetAmp-T.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef CHSUM_NJETAMPEXTRA_H
#define CHSUM_NJETAMPEXTRA_H

#include <cassert>

#include "../ngluon2/NGluon2.h"

// QCD primitives
// ================================================

template <typename T>
typename NJetAmp<T>::TreeValue
NJetAmp<T>::A0n(int* order)
{
// order[] = {O[p0], O[p1], O[p2], O[p3], ...};
  return ngluons[mfv]->evalTree(order);
}

template <typename T>
LoopResult<T> NJetAmp<T>::AFn(int* order)
{
// order[] = {O[p0], O[p1], O[p2], O[p3], ...};
  return ngluons[mfv]->eval(NGluon2<T>::LIGHTQ, order);
}

template <typename T>
LoopResult<T> NJetAmp<T>::ALn(int* order)
{
// order[] = {O[p0], O[p1], O[p2], O[p3], ...};
  return ngluons[mfv]->eval(NGluon2<T>::MIXED, order);
}

// QCD+V primitives for the case of single quark line
// ==================================================

template <typename T>
typename NJetAmp<T>::TreeValue
NJetAmp<T>::A0nVqq(int* order)
{
// order[] = {O[p0], NN, O[p1], O[p2], O[p3], ...};
  const int* O = getFperm(mfv);
  const int Vflav = getFlav(mfv, O[0]); // assuming flav[O[0]] == V-quark and p0 == 0

  int iw = 1;

  TreeValue amp = TreeValue();
  do {
    amp += ngluons[mfv]->evalTree(order);
    std::swap(order[iw], order[iw+1]);
    iw += 1;
  } while (Vflav != -getFlav(mfv, order[iw-1]));
  return amp;
}

template <typename T>
LoopResult<T> NJetAmp<T>::AFnVqq(int* order)
{
// order[] = {O[p0], NN, O[p1], O[p2], O[p3], ...};
  const int* O = getFperm(mfv);
  const int Vflav = getFlav(mfv, O[0]); // assuming flav[O[0]] == V-quark and p0 == 0

  int iw = 1;

  LoopResult<T> amp = LoopResult<T>();
  do {
    amp += ngluons[mfv]->eval(NGluon2<T>::LIGHTQ, order);
    std::swap(order[iw], order[iw+1]);
    iw += 1;
  } while (Vflav != -getFlav(mfv, order[iw-1]));
  return amp;
}

template <typename T>
LoopResult<T> NJetAmp<T>::ALnVqq(int* order)
{
// order[] = {O[p0], NN, O[p1], O[p2], O[p3], ...};
  const int* O = getFperm(mfv);
  const int Vflav = getFlav(mfv, O[0]); // assuming flav[O[0]] == V-quark and p0 == 0

  int iw = 1;

  LoopResult<T> amp = LoopResult<T>();
  do {
    amp += ngluons[mfv]->eval(NGluon2<T>::MIXED, order);
    std::swap(order[iw], order[iw+1]);
    iw += 1;
  } while (Vflav != -getFlav(mfv, order[iw-1]));
  return amp;
}

// QCD+V primitives for the case of multiple quark lines
// =====================================================

template <typename T>
typename NJetAmp<T>::TreeValue
NJetAmp<T>::A0nV(int* order)
{
// order[] = {O[p0], NN, O[p1], O[p2], O[p3], ...};
  const int* O = getFperm(mfv);
  int Vflav = abs(getFlav(mfv, O[0])); // assuming flav[O[0]] == V-quark

  int iw = 1;

  // move NN into position
  while (abs(getFlav(mfv, order[iw-1])) != Vflav) {
    std::swap(order[iw], order[iw+1]);
    iw += 1;
  }
  const int i0 = iw;
  Vflav = getFlav(mfv, order[i0-1]); // get flavour sign

  TreeValue amp = TreeValue();
  int qcross = 0;
  do {
    if (qcross == 0) {
      amp += ngluons[mfv]->evalTree(order);
    }
    std::swap(order[iw], order[iw+1]);
    iw += 1;
    qcross += getFlav(mfv, order[iw-1]);
  } while (Vflav != -getFlav(mfv, order[iw-1]));
  return amp;
}

template <typename T>
LoopResult<T> NJetAmp<T>::AFnV(int* order)
{
// order[] = {O[p0], NN, O[p1], O[p2], O[p3], ...};
  const int* O = getFperm(mfv);
  int Vflav = abs(getFlav(mfv, O[0])); // assuming flav[O[0]] == V-quark

  int iw = 1;

  // move NN into position
  while (abs(getFlav(mfv, order[iw-1])) != Vflav) {
    std::swap(order[iw], order[iw+1]);
    iw += 1;
  }
  const int i0 = iw;
  Vflav = getFlav(mfv, order[i0-1]); // get flavour sign

  LoopResult<T> amp = LoopResult<T>();
  int qcross = 0;
  do {
    if (qcross == 0) {
      amp += ngluons[mfv]->eval(NGluon2<T>::LIGHTQ, order);
    }
    std::swap(order[iw], order[iw+1]);
    iw += 1;
    qcross += getFlav(mfv, order[iw-1]);
  } while (Vflav != -getFlav(mfv, order[iw-1]));
  return amp;
}

template <typename T>
LoopResult<T> NJetAmp<T>::ALnV(int* order)
{
// order[] = {O[p0], NN, O[p1], O[p2], O[p3], ...};
  const int* O = getFperm(mfv);
  int Vflav = abs(getFlav(mfv, O[0])); // assuming flav[O[0]] == V-quark

  int iw = 1;

  // move NN into position
  while (abs(getFlav(mfv, order[iw-1])) != Vflav) {
    std::swap(order[iw], order[iw+1]);
    iw += 1;
  }
  const int i0 = iw;
  Vflav = getFlav(mfv, order[i0-1]); // get flavour sign

  LoopResult<T> amp = LoopResult<T>();
  int qcross = 0;
  do {
    if (qcross == 0) {
      amp += ngluons[mfv]->eval(NGluon2<T>::MIXED, order);
    }
    std::swap(order[iw], order[iw+1]);
    iw += 1;
    qcross += getFlav(mfv, order[iw-1]);
  } while (Vflav != -getFlav(mfv, order[iw-1]));
  return amp;
}

// QCD+V primitives for vector loops
// =====================================================
template <typename T>
LoopResult<T> NJetAmp<T>::AFnVax(int* order)
{
// order[] = {O[p0], O[p1], O[p2], O[p3], NN};
  int iw = NN;
  LoopResult<T> amp = LoopResult<T>();
  int qcross = 0;
  do {
    if (qcross == 0) {
      amp += ngluons[mfv]->eval(NGluon2<T>::LIGHTQ, order);
    }
    std::swap(order[iw], order[iw-1]);
    iw -= 1;
    qcross += getFlav(mfv, order[iw+1]);
  } while (iw > 0);
  return amp;
}

// QCD + AA primitives
// =====================================================

// AA on the same q-line
// =====================================================

template <typename T>
typename NJetAmp<T>::TreeValue
NJetAmp<T>::A0nAA(int* order)
{
// order[] = {O[p0], NN, NN+1, O[p1], O[p2], O[p3], ...};
#ifdef NDEBUG
  TreeValue amp = TreeValue();
  amp += ngluons[mfv]->evalTree(order);
  std::swap(order[1], order[2]);
  amp += ngluons[mfv]->evalTree(order);
  return amp;
#else
  const int* O = getFperm(mfv);
  int AAflav = abs(getFlav(mfv, O[0])); // assuming flav[O[0]] == AA-quark

  int iw1 = 1; // current position of A1 (NN)
  int iw2 = 2; // current position of A2 (NN+1)

  // move NN and NN+1 into position
  while (abs(getFlav(mfv, order[iw1-1])) != AAflav) {
    order[iw1] = order[iw2+1];
    order[iw1+1] = NN;
    order[iw2+1] = NN+1;
    iw1 += 1;
    iw2 += 1;
  }
  int i0 = iw1;
  AAflav = getFlav(mfv, order[i0-1]); // get flavour sign

  TreeValue amp = TreeValue();
  int qcross1 = 0;
  do {
    if (qcross1 == 0) {
      while (iw2 > iw1+1) { // iw2 = iw1+1;
        std::swap(order[iw2], order[iw2-1]);
        iw2 -= 1;
      }
      int qcross2 = qcross1;
      do {
        if (qcross2 == 0) {
          amp += ngluons[mfv]->evalTree(order);
          std::swap(order[iw1], order[iw2]);
          amp += ngluons[mfv]->evalTree(order);
          std::swap(order[iw1], order[iw2]);
        }
        std::swap(order[iw2], order[iw2+1]);
        iw2 += 1;
        qcross2 += getFlav(mfv, order[iw2-1]);
      } while (AAflav != -getFlav(mfv, order[iw2-1]));
    }
    std::swap(order[iw1], order[iw1+1]);
    iw1 += 1;
    qcross1 += getFlav(mfv, order[iw1-1]);
  } while (iw1 < iw2);
  return amp;
#endif
}

template <typename T>
LoopResult<T> NJetAmp<T>::AFnAA(int* order)
{
// order[] = {O[p0], NN, NN+1, O[p1], O[p2], O[p3], ...};
  const int* O = getFperm(mfv);
  int AAflav = abs(getFlav(mfv, O[0])); // assuming flav[O[0]] == AA-quark

  int iw1 = 1; // current position of A1 (NN)
  int iw2 = 2; // current position of A2 (NN+1)

  // move NN and NN+1 into position
  while (abs(getFlav(mfv, order[iw1-1])) != AAflav) {
    order[iw1] = order[iw2+1];
    order[iw1+1] = NN;
    order[iw2+1] = NN+1;
    iw1 += 1;
    iw2 += 1;
  }
  int i0 = iw1;
  AAflav = getFlav(mfv, order[i0-1]); // get flavour sign

  LoopResult<T> amp = LoopResult<T>();
  int qcross1 = 0;
  do {
    if (qcross1 == 0) {
      while (iw2 > iw1+1) { // iw2 = iw1+1;
        std::swap(order[iw2], order[iw2-1]);
        iw2 -= 1;
      }
      int qcross2 = qcross1;
      do {
        if (qcross2 == 0) {
          amp += ngluons[mfv]->eval(NGluon2<T>::LIGHTQ, order);
#ifdef SYMMETRIZE_AA
          std::swap(order[iw1], order[iw2]);
          amp += ngluons[mfv]->eval(NGluon2<T>::LIGHTQ, order);
          std::swap(order[iw1], order[iw2]);
#endif
        }
        std::swap(order[iw2], order[iw2+1]);
        iw2 += 1;
        qcross2 += getFlav(mfv, order[iw2-1]);
      } while (AAflav != -getFlav(mfv, order[iw2-1]));
    }
    std::swap(order[iw1], order[iw1+1]);
    iw1 += 1;
    qcross1 += getFlav(mfv, order[iw1-1]);
  } while (iw1 < iw2);
#ifdef SYMMETRIZE_AA
  return amp;
#else
  return 2.*amp; // desymmetrized needs symmetry factor
#endif
}

template <typename T>
LoopResult<T> NJetAmp<T>::ALnAA(int* order)
{
// order[] = {O[p0], NN, NN+1, O[p1], O[p2], O[p3], ...};
  const int* O = getFperm(mfv);
  int AAflav = abs(getFlav(mfv, O[0])); // assuming flav[O[0]] == AA-quark

  int iw1 = 1; // current position of A1 (NN)
  int iw2 = 2; // current position of A2 (NN+1)

  // move NN and NN+1 into position
  while (abs(getFlav(mfv, order[iw1-1])) != AAflav) {
    order[iw1] = order[iw2+1];
    order[iw1+1] = NN;
    order[iw2+1] = NN+1;
    iw1 += 1;
    iw2 += 1;
  }
  int i0 = iw1;
  AAflav = getFlav(mfv, order[i0-1]); // get flavour sign

  LoopResult<T> amp = LoopResult<T>();
  int qcross1 = 0;
  do {
    if (qcross1 == 0) {
      while (iw2 > iw1+1) { // iw2 = iw1+1;
        std::swap(order[iw2], order[iw2-1]);
        iw2 -= 1;
      }
      int qcross2 = qcross1;
      do {
        if (qcross2 == 0) {
          amp += ngluons[mfv]->eval(NGluon2<T>::MIXED, order);
#ifdef SYMMETRIZE_AA
          std::swap(order[iw1], order[iw2]);
          amp += ngluons[mfv]->eval(NGluon2<T>::MIXED, order);
          std::swap(order[iw1], order[iw2]);
#endif
        }
        std::swap(order[iw2], order[iw2+1]);
        iw2 += 1;
        qcross2 += getFlav(mfv, order[iw2-1]);
      } while (AAflav != -getFlav(mfv, order[iw2-1]));
    }
    std::swap(order[iw1], order[iw1+1]);
    iw1 += 1;
    qcross1 += getFlav(mfv, order[iw1-1]);
  } while (iw1 < iw2);
#ifdef SYMMETRIZE_AA
  return amp;
#else
  return 2.*amp; // desymmetrized needs symmetry factor
#endif
}

// AA on two different q-lines
// =====================================================

template <typename T>
typename NJetAmp<T>::TreeValue
NJetAmp<T>::A0nAA2(int* order)
{
// order[] = {O[p0], NN, NN+1, O[p1], O[p2], O[p3], ...};
  const int* O = getFperm(mfv);
  int A1flav = abs(getFlav(mfv, O[0])); // assuming flav[O[0]] == A1-quark
  int A2flav = abs(getFlav(mfv, O[2])); // assuming flav[O[2]] == A2-quark

  int iw1 = 1; // current position of A1 (NN)
  int iw2 = 2; // current position of A2 (NN+1)

  // move NN into position
  while (iw1-1 == iw2 or abs(getFlav(mfv, order[iw1-1])) != A1flav) {
    std::swap(order[iw1], order[iw1+1]);
    iw1 += 1;
    if (iw1 == iw2) { // crossed A2
      iw2 -= 1;
    }
  }
  int i01 = iw1; // starting position of A1
  A1flav = getFlav(mfv, order[i01-1]); // get flavour sign

  // move NN+1 into position
  while (iw2-1 == iw1 or abs(getFlav(mfv, order[iw2-1])) != A2flav) {
    std::swap(order[iw2], order[iw2+1]);
    iw2 += 1;
    if (iw2 == iw1) { // crossed A1
      iw1 -= 1;
    }
  }
  int i02 = iw2; // starting position of A2
  A2flav = getFlav(mfv, order[i02-1]); // get flavour sign

  if (i02 < i01) {  // make A2 rightmost flavour (only A1 can cross A2 later)
    std::swap(i02, i01);
    std::swap(iw2, iw1);
    std::swap(A2flav, A1flav);
  }

  TreeValue amp = TreeValue();
  int qcross1 = 0;
  do {
    if (qcross1 == 0) {
      int qcross2 = 0;
      do {
        if (qcross2 == 0) {
          amp += ngluons[mfv]->evalTree(order);
        }
        std::swap(order[iw2], order[iw2+1]);
        iw2 += 1;
        qcross2 += getFlav(mfv, order[iw2-1]);  // crossed parton
      } while (A2flav != -getFlav(mfv, order[iw2-1]));  // until A2 crossed its flavour
      while (iw2 > i02) { // return A2 to starting position
        std::swap(order[iw2], order[iw2-1]);
        iw2 -= 1;
      }
    }
    std::swap(order[iw1], order[iw1+1]);
    iw1 += 1;
    if (iw1 == i02) {  // crossed A2
      i02 -= 1;
      iw2 -= 1;
    } else {  // crossed parton
      qcross1 += getFlav(mfv, order[iw1-1]);
    }
  } while (iw1-1 == iw2 or A1flav != -getFlav(mfv, order[iw1-1]));  // until A1 crossed its flavour
  return amp;
}

template <typename T>
LoopResult<T> NJetAmp<T>::ALnAA2(int* order)
{
// order[] = {O[p0], NN, NN+1, O[p1], O[p2], O[p3], ...};
  const int* O = getFperm(mfv);
  int A1flav = abs(getFlav(mfv, O[0])); // assuming flav[O[0]] == A1-quark
  int A2flav = abs(getFlav(mfv, O[2])); // assuming flav[O[2]] == A2-quark

  int iw1 = 1; // current position of A1 (NN)
  int iw2 = 2; // current position of A2 (NN+1)

  // move NN into position
  while (iw1-1 == iw2 or abs(getFlav(mfv, order[iw1-1])) != A1flav) {
    std::swap(order[iw1], order[iw1+1]);
    iw1 += 1;
    if (iw1 == iw2) { // crossed A2
      iw2 -= 1;
    }
  }
  int i01 = iw1; // starting position of A1
  A1flav = getFlav(mfv, order[i01-1]); // get flavour sign

  // move NN+1 into position
  while (iw2-1 == iw1 or abs(getFlav(mfv, order[iw2-1])) != A2flav) {
    std::swap(order[iw2], order[iw2+1]);
    iw2 += 1;
    if (iw2 == iw1) { // crossed A1
      iw1 -= 1;
    }
  }
  int i02 = iw2; // starting position of A2
  A2flav = getFlav(mfv, order[i02-1]); // get flavour sign

  if (i02 < i01) {  // make A2 rightmost flavour (only A1 can cross A2 later)
    std::swap(i02, i01);
    std::swap(iw2, iw1);
    std::swap(A2flav, A1flav);
  }

  LoopResult<T> amp = LoopResult<T>();
  int qcross1 = 0;
  do {
    if (qcross1 == 0) {
      int qcross2 = 0;
      do {
        if (qcross2 == 0) {
          amp += ngluons[mfv]->eval(NGluon2<T>::MIXED, order);
        }
        std::swap(order[iw2], order[iw2+1]);
        iw2 += 1;
        qcross2 += getFlav(mfv, order[iw2-1]);  // crossed parton
      } while (A2flav != -getFlav(mfv, order[iw2-1]));  // until A2 crossed its flavour
      while (iw2 > i02) { // return A2 to starting position
        std::swap(order[iw2], order[iw2-1]);
        iw2 -= 1;
      }
    }
    std::swap(order[iw1], order[iw1+1]);
    iw1 += 1;
    if (iw1 == i02) {  // crossed A2
      i02 -= 1;
      iw2 -= 1;
    } else {  // crossed parton
      qcross1 += getFlav(mfv, order[iw1-1]);
    }
  } while (iw1-1 == iw2 or A1flav != -getFlav(mfv, order[iw1-1]));  // until A1 crossed its flavour
  return amp;
}

template <typename T>
LoopResult<T> NJetAmp<T>::AFnAA2(int* order)
{
// order[] = {O[p0], NN, NN+1, O[p1], O[p2], O[p3], ...};
  const int* O = getFperm(mfv);
  int A1flav = abs(getFlav(mfv, O[0])); // assuming flav[O[0]] == A1-quark
  int A2flav = abs(getFlav(mfv, O[2])); // assuming flav[O[2]] == A2-quark

  int iw1 = 1; // current position of A1 (NN)
  int iw2 = 2; // current position of A2 (NN+1)

  // move NN into position
  while (iw1-1 == iw2 or abs(getFlav(mfv, order[iw1-1])) != A1flav) {
    std::swap(order[iw1], order[iw1+1]);
    iw1 += 1;
    if (iw1 == iw2) { // crossed A2
      iw2 -= 1;
    }
  }
  int i01 = iw1; // starting position of A1
  A1flav = getFlav(mfv, order[i01-1]); // get flavour sign

  // move NN+1 into position
  while (iw2-1 == iw1 or abs(getFlav(mfv, order[iw2-1])) != A2flav) {
    std::swap(order[iw2], order[iw2+1]);
    iw2 += 1;
    if (iw2 == iw1) { // crossed A1
      iw1 -= 1;
    }
  }
  int i02 = iw2; // starting position of A2
  A2flav = getFlav(mfv, order[i02-1]); // get flavour sign

  if (i02 < i01) {  // make A2 rightmost flavour (only A1 can cross A2 later)
    std::swap(i02, i01);
    std::swap(iw2, iw1);
    std::swap(A2flav, A1flav);
  }

  LoopResult<T> amp = LoopResult<T>();
  int qcross1 = 0;
  do {
    if (qcross1 == 0) {
      int qcross2 = 0;
      do {
        if (qcross2 == 0) {
          amp += ngluons[mfv]->eval(NGluon2<T>::LIGHTQ, order);
        }
        std::swap(order[iw2], order[iw2+1]);
        iw2 += 1;
        qcross2 += getFlav(mfv, order[iw2-1]);  // crossed parton
      } while (A2flav != -getFlav(mfv, order[iw2-1]));  // until A2 crossed its flavour
      while (iw2 > i02) { // return A2 to starting position
        std::swap(order[iw2], order[iw2-1]);
        iw2 -= 1;
      }
    }
    std::swap(order[iw1], order[iw1+1]);
    iw1 += 1;
    if (iw1 == i02) {  // crossed A2
      i02 -= 1;
      iw2 -= 1;
    } else {  // crossed parton
      qcross1 += getFlav(mfv, order[iw1-1]);
    }
  } while (iw1-1 == iw2 or A1flav != -getFlav(mfv, order[iw1-1]));  // until A1 crossed its flavour
  return amp;
}

// AAx - A-q + A-l single vector loops
// NOTE this contribution cannot be symmetrized on the prim-amp level
// =====================================================
template <typename T>
LoopResult<T> NJetAmp<T>::AFnAAx(int* order)
{
// order[] = {O[p0], NN, O[p1], O[p2], O[p3], NN+1};
  const int* O = getFperm(mfv);
  int Vflav = abs(getFlav(mfv, O[0])); // assuming flav[O[0]] == V-quark

  int iw = 1;      // initial position of A-q
  int ix = NN+1;   // initial position of A-L
  const int i0x = ix;

  // move NN into position
  while (abs(getFlav(mfv, order[iw-1])) != Vflav) {
    std::swap(order[iw], order[iw+1]);
    iw += 1;
  }
  const int i0 = iw;
  Vflav = getFlav(mfv, order[i0-1]); // get flavour sign

  LoopResult<T> amp = LoopResult<T>();
  int qcross = 0;
  do {
    if (qcross == 0) {
      int qcrossx = 0;
      do {
        if (qcrossx == 0) {
          amp += ngluons[mfv]->eval(NGluon2<T>::LIGHTQ, order);
        }
        std::swap(order[ix], order[ix-1]);
        ix -= 1;
        if (ix != iw) {
          qcrossx += getFlav(mfv, order[ix+1]);
        }
      } while (ix > 0);
      while (ix < i0x) {
        std::swap(order[ix], order[ix+1]);
        ix += 1;
      }
    }
    std::swap(order[iw], order[iw+1]);
    iw += 1;
    qcross += getFlav(mfv, order[iw-1]);
  } while (Vflav != -getFlav(mfv, order[iw-1]));
  // always desymmetrized
  return 2.*amp;
}

// AAxx - A-l + A-l double vector loops
// =====================================================
template <typename T>
LoopResult<T> NJetAmp<T>::AFnAAxx(int* order)
{
// order[] = {O[p0], O[p1], O[p2], O[p3], NN, NN+1};

  int iw1 = NN;   // current position of A1 (NN)
  int iw2 = NN+1; // current position of A2 (NN+1)
  int i02 = iw2;

  LoopResult<T> amp = LoopResult<T>();
  int qcross1 = 0;
  do {
    if (qcross1 == 0) {
      int qcross2 = 0;
      do {
        if (qcross2 == 0) {
          amp += ngluons[mfv]->eval(NGluon2<T>::LIGHTQ, order);
#ifdef SYMMETRIZE_AA
          std::swap(order[iw1], order[iw2]);
          amp += ngluons[mfv]->eval(NGluon2<T>::LIGHTQ, order);
          std::swap(order[iw1], order[iw2]);
#endif
        }
        std::swap(order[iw2], order[iw2-1]);
        iw2 -= 1;
        qcross2 += getFlav(mfv, order[iw2+1]);
      } while (iw2 > iw1);
      while (iw2 < i02) {
        std::swap(order[iw2], order[iw2+1]);
        iw2 += 1;
      }
    }
    std::swap(order[iw1], order[iw1-1]);
    iw1 -= 1;
    qcross1 += getFlav(mfv, order[iw1+1]);
  } while (iw1 > 0);
#ifdef SYMMETRIZE_AA
  return amp;
#else
  return 2.*amp;
#endif
}

// A0nH - QCD+H tree primitives
// =====================================================
template <typename T>
typename NJetAmp<T>::TreeValue
NJetAmp<T>::A0nH(int* order)
{
// order[] = {NN, O[p0], O[p1], O[p2], O[p3], ...};

  int ih = 0;
  TreeValue amp = TreeValue();
  do {
    amp += ngluons[mfv]->evalTree(order);
    std::swap(order[ih], order[ih+1]);
    ih += 1;
  } while (ih < NN);

  return amp;
}

#endif // CHSUM_NJETAMPEXTRA_H
