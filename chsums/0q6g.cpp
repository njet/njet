/*
* chsums/0q6g.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>

#include "0q6g.h"
#include "../ngluon2/NGluon2.h"

// class Amp0q6g

template <typename T>
Amp0q6g<T>::Amp0q6g(const T scalefactor,
                    const int mFC, const NJetAmpTables& tables)
  : BaseClass(mFC, tables),
    Rcache(RcacheLen)
{
  for (int fv=0; fv<mFC; fv++) {
    BaseClass::initNG(scalefactor, fv);
    BaseClass::setProcess(StandardModel::NGluon1compat(NN, getFlav(fv)), fv);
  }

  initNc();
}

template <typename T>
void Amp0q6g<T>::setNc(const ST Nc_)
{
  BaseClass::setNc(Nc_);
  initNc();
}

template <typename T>
void Amp0q6g<T>::initNc()
{
  Nmat[0] = 0.;
  Nmat[1] = 2.;
  Nmat[2] = 4.;
  Nmat[3] = 8.;
  Nmat[4] = 16.;
  Nmat[5] = 24./Nc2;
  Nmat[6] = 2. + 24./Nc2;
  assert(6 < BaseClass::NmatLen);

  Nmatcc[ 0] = 0.;
  Nmatcc[ 1] = Nc2;
  Nmatcc[ 2] = 2.*Nc2;
  Nmatcc[ 3] = 4.*Nc2;
  Nmatcc[ 4] = 8.*Nc2;
  Nmatcc[ 5] = Nc2 + 16.;
  Nmatcc[ 6] = Nc2 + 12.;
  Nmatcc[ 7] = 2.*Nc2 + 24.;
  Nmatcc[ 8] = -4.;
  Nmatcc[ 9] = 4.;
  Nmatcc[10] = -8.;
  Nmatcc[11] = 8.;
  Nmatcc[12] = -12.;
  Nmatcc[13] = 12.;
  Nmatcc[14] = -16.;
  Nmatcc[15] = 16.;
  Nmatcc[16] = -24.;
  Nmatcc[17] = 24.;
  assert(17 < BaseClass::NmatccLen);

  NmatDS[ 0] = 0.;
  NmatDS[ 1] = Nc2;
  NmatDS[ 2] = -Nc2;
  NmatDS[ 3] = 4.;
  NmatDS[ 4] = -4.;
  NmatDS[ 5] = 8.;
  NmatDS[ 6] = -8.;
  NmatDS[ 7] = 12.;
  NmatDS[ 8] = -12.;
  NmatDS[ 9] = 16.;
  NmatDS[10] = -16.;
  NmatDS[11] = 1.;
  NmatDS[12] = -1.;
  NmatDS[13] = 2.;
  NmatDS[14] = -2.;
  NmatDS[15] = 3.;
  NmatDS[16] = -3.;
  assert(16 < BaseClass::NmatDSLen);

  bornFactor = Nc4*V;
  loopFactor = 4.*Nc2*V;
  bornccFactor = Nc3*V;
}

template <typename T>
LoopResult<T> Amp0q6g<T>::AF(int p0, int p1, int p2, int p3, int p4, int p5, int pos)
{
  const int* O = getFperm(mfv);
  const int order[] = {O[p0], O[p1], O[p2], O[p3], O[p4], O[p5]};
  LoopResult<T> ans = ngluons[mfv]->eval(NGluon2<T>::LIGHTQ, order, true);
  const LoopResult<T> rat = {Rcache[pos], conj(Rcache[pos])};
  ans += rat;
  return ans;
}

template <typename T>
LoopResult<T> Amp0q6g<T>::AL(int p0, int p1, int p2, int p3, int p4, int p5, int pos)
{
  const int* O = getFperm(mfv);
  const int order[] = {O[p0], O[p1], O[p2], O[p3], O[p4], O[p5]};
  const LoopResult<T> ans = ngluons[mfv]->eval(NGluon2<T>::MIXED, order);
  Rcache[pos] = ngluons[mfv]->lastValue().looprat();
  return ans;
}

template <typename T>
void Amp0q6g<T>::getfvpart0(const int fv, TreeValue* fvpart, TreeValue* fvpartC)
{
  fvSet(fv);
  fvpart[ 0] = A0(0,1,2,3,4,5);
  fvpart[ 1] = A0(0,1,2,3,5,4);
  fvpart[ 2] = A0(0,1,2,4,3,5);
  fvpart[ 3] = A0(0,1,2,4,5,3);
  fvpart[ 4] = A0(0,1,2,5,3,4);
  fvpart[ 5] = A0(0,1,2,5,4,3);
  fvpart[ 6] = A0(0,1,3,2,4,5);
  fvpart[ 7] = A0(0,1,3,2,5,4);
  fvpart[ 8] = A0(0,1,3,4,2,5);
  fvpart[ 9] = A0(0,1,3,4,5,2);
  fvpart[10] = A0(0,1,3,5,2,4);
  fvpart[11] = A0(0,1,3,5,4,2);
  fvpart[12] = A0(0,1,4,2,3,5);
  fvpart[13] = A0(0,1,4,2,5,3);
  fvpart[14] = A0(0,1,4,3,2,5);
  fvpart[15] = A0(0,1,4,3,5,2);
  fvpart[16] = A0(0,1,4,5,2,3);
  fvpart[17] = A0(0,1,4,5,3,2);
  fvpart[18] = A0(0,1,5,2,3,4);
  fvpart[19] = A0(0,1,5,2,4,3);
  fvpart[20] = A0(0,1,5,3,2,4);
  fvpart[21] = A0(0,1,5,3,4,2);
  fvpart[22] = A0(0,1,5,4,2,3);
  fvpart[23] = A0(0,1,5,4,3,2);

  if (fvpartC) {
    for (int i=0; i<C0; i++) {
      fvpartC[i] = conj(fvpart[i]);
    }
  }
}

template <typename T>
void Amp0q6g<T>::getfvpart1ds(const int fv, LoopValue* fvpart)
{
  return getfvpart1ds_(fv, fvpart);
}

template <typename T>
void Amp0q6g<T>::getfvpart1ds(const int fv, LoopResult<T>* fvpart)
{
  return getfvpart1ds_(fv, fvpart);
}

template <typename T>
template <typename LT>
inline
void Amp0q6g<T>::getfvpart1ds_(const int fv, LT* fvpart)
{
  fvSet(fv);
  fvpart[ 0] = Nc*AL(0,1,2,3,4,5,  0);
  fvpart[ 1] = Nc*AL(0,1,2,4,5,3,  1);
  fvpart[ 2] = Nc*AL(0,1,2,5,3,4,  2);
  fvpart[ 3] = Nc*AL(0,1,3,2,4,5,  3);
  fvpart[ 4] = Nc*AL(0,1,3,4,2,5,  4);
  fvpart[ 5] = Nc*AL(0,1,3,4,5,2,  5);
  fvpart[ 6] = Nc*AL(0,1,4,2,5,3,  6);
  fvpart[ 7] = Nc*AL(0,1,4,5,2,3,  7);
  fvpart[ 8] = Nc*AL(0,1,4,5,3,2,  8);
  fvpart[ 9] = Nc*AL(0,1,5,2,3,4,  9);
  fvpart[10] = Nc*AL(0,1,5,3,2,4, 10);
  fvpart[11] = Nc*AL(0,1,5,3,4,2, 11);
  fvpart[12] = Nc*AL(0,2,1,3,4,5, 12);
  fvpart[13] = Nc*AL(0,2,1,4,5,3, 13);
  fvpart[14] = Nc*AL(0,2,1,5,3,4, 14);
  fvpart[15] = Nc*AL(0,2,3,1,4,5, 15);
  fvpart[16] = Nc*AL(0,2,3,4,1,5, 16);
  fvpart[17] = Nc*AL(0,2,3,4,5,1, 17);
  fvpart[18] = Nc*AL(0,2,4,1,5,3, 18);
  fvpart[19] = Nc*AL(0,2,4,5,1,3, 19);
  fvpart[20] = Nc*AL(0,2,4,5,3,1, 20);
  fvpart[21] = Nc*AL(0,2,5,1,3,4, 21);
  fvpart[22] = Nc*AL(0,2,5,3,1,4, 22);
  fvpart[23] = Nc*AL(0,2,5,3,4,1, 23);
  fvpart[24] = Nc*AL(0,3,1,2,4,5, 24);
  fvpart[25] = Nc*AL(0,3,1,4,2,5, 25);
  fvpart[26] = Nc*AL(0,3,1,4,5,2, 26);
  fvpart[27] = Nc*AL(0,3,2,1,4,5, 27);
  fvpart[28] = Nc*AL(0,3,2,4,1,5, 28);
  fvpart[29] = Nc*AL(0,3,2,4,5,1, 29);
  fvpart[30] = Nc*AL(0,3,4,1,2,5, 30);
  fvpart[31] = Nc*AL(0,3,4,1,5,2, 31);
  fvpart[32] = Nc*AL(0,3,4,2,1,5, 32);
  fvpart[33] = Nc*AL(0,3,4,2,5,1, 33);
  fvpart[34] = Nc*AL(0,3,4,5,1,2, 34);
  fvpart[35] = Nc*AL(0,3,4,5,2,1, 35);
  fvpart[36] = Nc*AL(0,4,1,2,5,3, 36);
  fvpart[37] = Nc*AL(0,4,1,5,2,3, 37);
  fvpart[38] = Nc*AL(0,4,1,5,3,2, 38);
  fvpart[39] = Nc*AL(0,4,2,1,5,3, 39);
  fvpart[40] = Nc*AL(0,4,2,5,1,3, 40);
  fvpart[41] = Nc*AL(0,4,2,5,3,1, 41);
  fvpart[42] = Nc*AL(0,4,5,1,2,3, 42);
  fvpart[43] = Nc*AL(0,4,5,1,3,2, 43);
  fvpart[44] = Nc*AL(0,4,5,2,1,3, 44);
  fvpart[45] = Nc*AL(0,4,5,2,3,1, 45);
  fvpart[46] = Nc*AL(0,4,5,3,1,2, 46);
  fvpart[47] = Nc*AL(0,4,5,3,2,1, 47);
  fvpart[48] = Nc*AL(0,5,1,2,3,4, 48);
  fvpart[49] = Nc*AL(0,5,1,3,2,4, 49);
  fvpart[50] = Nc*AL(0,5,1,3,4,2, 50);
  fvpart[51] = Nc*AL(0,5,2,1,3,4, 51);
  fvpart[52] = Nc*AL(0,5,2,3,1,4, 52);
  fvpart[53] = Nc*AL(0,5,2,3,4,1, 53);
  fvpart[54] = Nc*AL(0,5,3,1,2,4, 54);
  fvpart[55] = Nc*AL(0,5,3,1,4,2, 55);
  fvpart[56] = Nc*AL(0,5,3,2,1,4, 56);
  fvpart[57] = Nc*AL(0,5,3,2,4,1, 57);
  fvpart[58] = Nc*AL(0,5,3,4,1,2, 58);
  fvpart[59] = Nc*AL(0,5,3,4,2,1, 59);

  if (Nf != 0.) {
    fvpart[ 60] = Nf*AF(0,1,2,3,4,5,  0);
    fvpart[ 61] = Nf*AF(0,1,2,4,5,3,  1);
    fvpart[ 62] = Nf*AF(0,1,2,5,3,4,  2);
    fvpart[ 63] = Nf*AF(0,1,3,2,4,5,  3);
    fvpart[ 64] = Nf*AF(0,1,3,4,2,5,  4);
    fvpart[ 65] = Nf*AF(0,1,3,4,5,2,  5);
    fvpart[ 66] = Nf*AF(0,1,4,2,5,3,  6);
    fvpart[ 67] = Nf*AF(0,1,4,5,2,3,  7);
    fvpart[ 68] = Nf*AF(0,1,4,5,3,2,  8);
    fvpart[ 69] = Nf*AF(0,1,5,2,3,4,  9);
    fvpart[ 70] = Nf*AF(0,1,5,3,2,4, 10);
    fvpart[ 71] = Nf*AF(0,1,5,3,4,2, 11);
    fvpart[ 72] = Nf*AF(0,2,1,3,4,5, 12);
    fvpart[ 73] = Nf*AF(0,2,1,4,5,3, 13);
    fvpart[ 74] = Nf*AF(0,2,1,5,3,4, 14);
    fvpart[ 75] = Nf*AF(0,2,3,1,4,5, 15);
    fvpart[ 76] = Nf*AF(0,2,3,4,1,5, 16);
    fvpart[ 77] = Nf*AF(0,2,3,4,5,1, 17);
    fvpart[ 78] = Nf*AF(0,2,4,1,5,3, 18);
    fvpart[ 79] = Nf*AF(0,2,4,5,1,3, 19);
    fvpart[ 80] = Nf*AF(0,2,4,5,3,1, 20);
    fvpart[ 81] = Nf*AF(0,2,5,1,3,4, 21);
    fvpart[ 82] = Nf*AF(0,2,5,3,1,4, 22);
    fvpart[ 83] = Nf*AF(0,2,5,3,4,1, 23);
    fvpart[ 84] = Nf*AF(0,3,1,2,4,5, 24);
    fvpart[ 85] = Nf*AF(0,3,1,4,2,5, 25);
    fvpart[ 86] = Nf*AF(0,3,1,4,5,2, 26);
    fvpart[ 87] = Nf*AF(0,3,2,1,4,5, 27);
    fvpart[ 88] = Nf*AF(0,3,2,4,1,5, 28);
    fvpart[ 89] = Nf*AF(0,3,2,4,5,1, 29);
    fvpart[ 90] = Nf*AF(0,3,4,1,2,5, 30);
    fvpart[ 91] = Nf*AF(0,3,4,1,5,2, 31);
    fvpart[ 92] = Nf*AF(0,3,4,2,1,5, 32);
    fvpart[ 93] = Nf*AF(0,3,4,2,5,1, 33);
    fvpart[ 94] = Nf*AF(0,3,4,5,1,2, 34);
    fvpart[ 95] = Nf*AF(0,3,4,5,2,1, 35);
    fvpart[ 96] = Nf*AF(0,4,1,2,5,3, 36);
    fvpart[ 97] = Nf*AF(0,4,1,5,2,3, 37);
    fvpart[ 98] = Nf*AF(0,4,1,5,3,2, 38);
    fvpart[ 99] = Nf*AF(0,4,2,1,5,3, 39);
    fvpart[100] = Nf*AF(0,4,2,5,1,3, 40);
    fvpart[101] = Nf*AF(0,4,2,5,3,1, 41);
    fvpart[102] = Nf*AF(0,4,5,1,2,3, 42);
    fvpart[103] = Nf*AF(0,4,5,1,3,2, 43);
    fvpart[104] = Nf*AF(0,4,5,2,1,3, 44);
    fvpart[105] = Nf*AF(0,4,5,2,3,1, 45);
    fvpart[106] = Nf*AF(0,4,5,3,1,2, 46);
    fvpart[107] = Nf*AF(0,4,5,3,2,1, 47);
    fvpart[108] = Nf*AF(0,5,1,2,3,4, 48);
    fvpart[109] = Nf*AF(0,5,1,3,2,4, 49);
    fvpart[110] = Nf*AF(0,5,1,3,4,2, 50);
    fvpart[111] = Nf*AF(0,5,2,1,3,4, 51);
    fvpart[112] = Nf*AF(0,5,2,3,1,4, 52);
    fvpart[113] = Nf*AF(0,5,2,3,4,1, 53);
    fvpart[114] = Nf*AF(0,5,3,1,2,4, 54);
    fvpart[115] = Nf*AF(0,5,3,1,4,2, 55);
    fvpart[116] = Nf*AF(0,5,3,2,1,4, 56);
    fvpart[117] = Nf*AF(0,5,3,2,4,1, 57);
    fvpart[118] = Nf*AF(0,5,3,4,1,2, 58);
    fvpart[119] = Nf*AF(0,5,3,4,2,1, 59);
  } else {
    for (int i=60; i<120; i++) {
      fvpart[i] = LT();
    }
  }
}

#ifdef USE_SD
  template class Amp0q6g<double>;
#endif
#ifdef USE_DD
  template class Amp0q6g<dd_real>;
#endif
#ifdef USE_QD
  template class Amp0q6g<qd_real>;
#endif
#ifdef USE_VC
  template class Amp0q6g<Vc::double_v>;
#endif

// class Amp0q6g_ds4

template <typename T>
Amp0q6g_ds4<T>::Amp0q6g_ds4(const T scalefactor,
                            const int mFC, const NJetAmpTables& tables)
  : BaseClass(scalefactor, mFC, tables)
{
  initNc();
}

template <typename T>
void Amp0q6g_ds4<T>::setNc(const ST Nc_)
{
  BaseClass::setNc(Nc_);
  initNc();
}

template <typename T>
void Amp0q6g_ds4<T>::initNc()
{
  loopFactor = 12.*BaseClass::loopFactor;  // add n!/2 factor
}

template <typename T>
void Amp0q6g_ds4<T>::getfvpart1ds(const int fv, LoopValue* fvpart)
{
  return getfvpart1ds_(fv, fvpart);
}

template <typename T>
void Amp0q6g_ds4<T>::getfvpart1ds(const int fv, LoopResult<T>* fvpart)
{
  return getfvpart1ds_(fv, fvpart);
}

template <typename T>
template <typename LT>
inline
void Amp0q6g_ds4<T>::getfvpart1ds_(const int fv, LT* fvpart)
{
  fvSet(fv);
  fvpart[0] = Nc*AL(0,1,2,3,4,5, 0);
  fvpart[1] = Nc*AL(0,2,1,3,4,5, 1);
  fvpart[2] = Nc*AL(0,2,3,1,4,5, 2);
  fvpart[3] = Nc*AL(0,2,3,4,1,5, 3);
  fvpart[4] = Nc*AL(0,2,3,4,5,1, 4);

  if (Nf != 0.) {
    fvpart[5] = Nf*AF(0,1,2,3,4,5, 0);
    fvpart[6] = Nf*AF(0,2,1,3,4,5, 1);
    fvpart[7] = Nf*AF(0,2,3,1,4,5, 2);
    fvpart[8] = Nf*AF(0,2,3,4,1,5, 3);
    fvpart[9] = Nf*AF(0,2,3,4,5,1, 4);
  } else {
    fvpart[5] = LT();
    fvpart[6] = LT();
    fvpart[7] = LT();
    fvpart[8] = LT();
    fvpart[9] = LT();
  }
}

#ifdef USE_SD
  template class Amp0q6g_ds4<double>;
#endif
#ifdef USE_DD
  template class Amp0q6g_ds4<dd_real>;
#endif
#ifdef USE_QD
  template class Amp0q6g_ds4<qd_real>;
#endif
#ifdef USE_VC
  template class Amp0q6g_ds4<Vc::double_v>;
#endif
