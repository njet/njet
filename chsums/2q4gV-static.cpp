/*
* chsums/2q4gV-static.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include "2q4gV.h"

// class Amp2q4gVStatic

const int
Amp2q4gVStatic::HSarr[HS][HSNN] = {
  { 1,-1, 1, 1, 1, 1, 1},
  { 1,-1, 1, 1, 1,-1, 1},
  { 1,-1, 1, 1,-1, 1, 1},
  { 1,-1, 1, 1,-1,-1, 1},
  { 1,-1, 1,-1, 1, 1, 1},
  { 1,-1, 1,-1, 1,-1, 1},
  { 1,-1, 1,-1,-1, 1, 1},
  { 1,-1, 1,-1,-1,-1, 1},
  { 1,-1,-1, 1, 1, 1, 1},
  { 1,-1,-1, 1, 1,-1, 1},
  { 1,-1,-1, 1,-1, 1, 1},
  { 1,-1,-1, 1,-1,-1, 1},
  { 1,-1,-1,-1, 1, 1, 1},
  { 1,-1,-1,-1, 1,-1, 1},
  { 1,-1,-1,-1,-1, 1, 1},
  { 1,-1,-1,-1,-1,-1, 1},
};

const int
Amp2q4gZStatic::HSarr[HS][HSNN] = {
  { 1,-1, 1, 1, 1, 1, 1},
  { 1,-1, 1, 1, 1,-1, 1},
  { 1,-1, 1, 1,-1, 1, 1},
  { 1,-1, 1, 1,-1,-1, 1},
  { 1,-1, 1,-1, 1, 1, 1},
  { 1,-1, 1,-1, 1,-1, 1},
  { 1,-1, 1,-1,-1, 1, 1},
  { 1,-1, 1,-1,-1,-1, 1},
  { 1,-1,-1, 1, 1, 1, 1},
  { 1,-1,-1, 1, 1,-1, 1},
  { 1,-1,-1, 1,-1, 1, 1},
  { 1,-1,-1, 1,-1,-1, 1},
  { 1,-1,-1,-1, 1, 1, 1},
  { 1,-1,-1,-1, 1,-1, 1},
  { 1,-1,-1,-1,-1, 1, 1},
  { 1,-1,-1,-1,-1,-1, 1},
  { 1,-1, 1, 1, 1, 1,-1},
  { 1,-1, 1, 1, 1,-1,-1},
  { 1,-1, 1, 1,-1, 1,-1},
  { 1,-1, 1, 1,-1,-1,-1},
  { 1,-1, 1,-1, 1, 1,-1},
  { 1,-1, 1,-1, 1,-1,-1},
  { 1,-1, 1,-1,-1, 1,-1},
  { 1,-1, 1,-1,-1,-1,-1},
  { 1,-1,-1, 1, 1, 1,-1},
  { 1,-1,-1, 1, 1,-1,-1},
  { 1,-1,-1, 1,-1, 1,-1},
  { 1,-1,-1, 1,-1,-1,-1},
  { 1,-1,-1,-1, 1, 1,-1},
  { 1,-1,-1,-1, 1,-1,-1},
  { 1,-1,-1,-1,-1, 1,-1},
  { 1,-1,-1,-1,-1,-1,-1},
  {-1, 1, 1, 1, 1, 1, 1},
  {-1, 1, 1, 1, 1,-1, 1},
  {-1, 1, 1, 1,-1, 1, 1},
  {-1, 1, 1, 1,-1,-1, 1},
  {-1, 1, 1,-1, 1, 1, 1},
  {-1, 1, 1,-1, 1,-1, 1},
  {-1, 1, 1,-1,-1, 1, 1},
  {-1, 1, 1,-1,-1,-1, 1},
  {-1, 1,-1, 1, 1, 1, 1},
  {-1, 1,-1, 1, 1,-1, 1},
  {-1, 1,-1, 1,-1, 1, 1},
  {-1, 1,-1, 1,-1,-1, 1},
  {-1, 1,-1,-1, 1, 1, 1},
  {-1, 1,-1,-1, 1,-1, 1},
  {-1, 1,-1,-1,-1, 1, 1},
  {-1, 1,-1,-1,-1,-1, 1},
  {-1, 1, 1, 1, 1, 1,-1},
  {-1, 1, 1, 1, 1,-1,-1},
  {-1, 1, 1, 1,-1, 1,-1},
  {-1, 1, 1, 1,-1,-1,-1},
  {-1, 1, 1,-1, 1, 1,-1},
  {-1, 1, 1,-1, 1,-1,-1},
  {-1, 1, 1,-1,-1, 1,-1},
  {-1, 1, 1,-1,-1,-1,-1},
  {-1, 1,-1, 1, 1, 1,-1},
  {-1, 1,-1, 1, 1,-1,-1},
  {-1, 1,-1, 1,-1, 1,-1},
  {-1, 1,-1, 1,-1,-1,-1},
  {-1, 1,-1,-1, 1, 1,-1},
  {-1, 1,-1,-1, 1,-1,-1},
  {-1, 1,-1,-1,-1, 1,-1},
  {-1, 1,-1,-1,-1,-1,-1},
};
