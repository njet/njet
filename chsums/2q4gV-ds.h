/*
* chsums/2q4gV-ds.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012, 2013 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef CHSUM_2Q4GV_DS_H
#define CHSUM_2Q4GV_DS_H

#include "2q4g-ds.h"
#include "2q4gV.h"
#include "../ngluon2/Model.h"

// ============================================================================
// classes Amp2q4gV_ds4 and Amp2q4gZ_ds4
// ============================================================================

class Amp2q4gV_ds4Static : public Amp2q4gVStatic, public Amp2q4g_ds4Static
{
};

template <typename T>
class Amp2q4gV_ds4 : public Amp2q4g_ds4<T>
{
    typedef Amp2q4g_ds4<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp2q4gV_ds4(const T scalefactor,
                 const int mFC=1, const NJetAmpTables& tables=amptables());
    Amp2q4gV_ds4(const Flavour<double>& Vflav, const T scalefactor,
                 const int mFC=1, const NJetAmpTables& tables=amptables());

    void born_part_fill() { BaseClass::born_part_fullfill(); }
    LoopValue virt() { return BaseClass::virt_dsfullsum(); }

  protected:
    using BaseClass::NN;
    using BaseClass::vhel;
    using BaseClass::getFlav;
    using BaseClass::getFperm;
    using BaseClass::mfv;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp2q4gV_ds4Static>();
    }

    void initProcess(const Flavour<double>& ff);

    TreeValue A0(int p0, int p1, int p2, int p3, int p4, int p5);
    LoopResult<T> AL(int p0, int p1, int p2, int p3, int p4, int p5);
    LoopResult<T> AF(int p0, int p1, int p2, int p3, int p4, int p5);
};

class Amp2q4gZ_ds4Static : public Amp2q4gZStatic, public Amp2q4g_ds4Static
{
};

template <typename T>
class Amp2q4gZ_ds4 : public Amp2q4gV_ds4<T>
{
    typedef Amp2q4gV_ds4<T> BaseClass;
  public:

    Amp2q4gZ_ds4(const Flavour<double>& ff, const T scalefactor,
                 const int mFC=1, const NJetAmpTables& tables=amptables())
    : BaseClass(ff, scalefactor, mFC, tables)
    { }

  protected:
    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp2q4gZ_ds4Static>();
    }
};

// ============================================================================
// classes Amp2q4gV_ds3 and Amp2q4gZ_ds3
// ============================================================================

class Amp2q4gV_ds3Static : public Amp2q4gVStatic, public Amp2q4g_ds3Static
{
};

template <typename T>
class Amp2q4gV_ds3 : public Amp2q4g_ds3<T>
{
    typedef Amp2q4g_ds3<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp2q4gV_ds3(const T scalefactor,
                 const int mFC=1, const NJetAmpTables& tables=amptables());
    Amp2q4gV_ds3(const Flavour<double>& Vflav, const T scalefactor,
                 const int mFC=1, const NJetAmpTables& tables=amptables());

    void born_part_fill() { BaseClass::born_part_fullfill(); }
    LoopValue virt() { return BaseClass::virt_dsfullsum(); }

  protected:
    using BaseClass::NN;
    using BaseClass::vhel;
    using BaseClass::getFlav;
    using BaseClass::getFperm;
    using BaseClass::mfv;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp2q4gV_ds3Static>();
    }

    void initProcess(const Flavour<double>& ff);

    TreeValue A0(int p0, int p1, int p2, int p3, int p4, int p5);
    LoopResult<T> AL(int p0, int p1, int p2, int p3, int p4, int p5);
    LoopResult<T> AF(int p0, int p1, int p2, int p3, int p4, int p5);
};

class Amp2q4gZ_ds3Static : public Amp2q4gZStatic, public Amp2q4g_ds3Static
{
};

template <typename T>
class Amp2q4gZ_ds3 : public Amp2q4gV_ds3<T>
{
    typedef Amp2q4gV_ds3<T> BaseClass;
  public:

    Amp2q4gZ_ds3(const Flavour<double>& ff, const T scalefactor,
                 const int mFC=1, const NJetAmpTables& tables=amptables())
    : BaseClass(ff, scalefactor, mFC, tables)
    { }

  protected:
    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp2q4gZ_ds3Static>();
    }
};

#endif // CHSUM_2Q4GV_DS_H
