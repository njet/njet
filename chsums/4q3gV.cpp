/*
* chsums/4q3gV.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include "4q3gV.h"
#include "NJetAmp-T.h"

// class Amp4q3gV

template <typename T>
Amp4q3gV<T>::Amp4q3gV(const T scalefactor, const int mFC, const NJetAmpTables& tables)
  : BaseClass(scalefactor, mFC, tables)
{
  Flavour<double> ff = StandardModel::Wp(StandardModel::u(), StandardModel::ubar(), 1.);  // ckm = 1
  initProcess(ff);
}

template <typename T>
Amp4q3gV<T>::Amp4q3gV(const Flavour<double>& ff,
                      const T scalefactor, const int mFC, const NJetAmpTables& tables)
  : BaseClass(scalefactor, mFC, tables)
{
  initProcess(ff);
}

template <typename T>
void Amp4q3gV<T>::initProcess(const Flavour<double>& ff)
{
  for (int fv=0; fv<BaseClass::mFC; fv++) {
    Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
    flavours.push_back(ff);
    BaseClass::setProcess(flavours, fv);
  }
}

template <typename T>
typename Amp4q3gV<T>::TreeValue
Amp4q3gV<T>::A0(int p0, int p1, int p2, int p3, int p4, int p5, int p6)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, O[p1], O[p2], O[p3], O[p4], O[p5], O[p6]};
  return BaseClass::A0nVqq(order);
}

template <typename T>
Amp4q3gZd<T>::Amp4q3gZd(const Flavour<double>& ff,
                        const T scalefactor, const int mFC, const NJetAmpTables& tables)
  : BaseClass(ff, scalefactor, mFC, tables)
{
  const Flavour<double> Zu = ff;
  const Flavour<double> Zd = StandardModel::BosonFlip(ff);

  Flavour<double> ffs[2] = {Zu, Zd};

  for (int fv=0; fv<BaseClass::mFC; fv++) {
    Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
    flavours.push_back(ffs[fv]);
    BaseClass::setProcess(flavours, fv);
  }
}

template <typename T>
LoopResult<T> Amp4q3gV<T>::AF(int p0, int p1, int p2, int p3, int p4, int p5, int p6)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, O[p1], O[p2], O[p3], O[p4], O[p5], O[p6]};
  return BaseClass::AFnV(order);
}

template <typename T>
LoopResult<T> Amp4q3gV<T>::AL(int p0, int p1, int p2, int p3, int p4, int p5, int p6)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, O[p1], O[p2], O[p3], O[p4], O[p5], O[p6]};
  return BaseClass::ALnV(order);
}

#ifdef USE_SD
  template class Amp4q3gV<double>;
  template class Amp4q3gZd<double>;
#endif
#ifdef USE_DD
  template class Amp4q3gV<dd_real>;
  template class Amp4q3gZd<dd_real>;
#endif
#ifdef USE_QD
  template class Amp4q3gV<qd_real>;
  template class Amp4q3gZd<qd_real>;
#endif
#ifdef USE_VC
  template class Amp4q3gV<Vc::double_v>;
  template class Amp4q3gZd<Vc::double_v>;
#endif
