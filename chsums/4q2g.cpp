/*
* chsums/4q2g.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>

#include "4q2g.h"
#include "../ngluon2/Model.h"

// class Amp4q2g

template <typename T>
Amp4q2g<T>::Amp4q2g(const T scalefactor, const int mFC, const NJetAmpTables& tables)
  : BaseClass(mFC, tables)
{
  for (int fv=0; fv<mFC; fv++) {
    BaseClass::initNG(scalefactor, fv);
    BaseClass::setProcess(StandardModel::NGluon1compat(NN, getFlav(fv)), fv);
  }

  initNc();
}


template <typename T>
void Amp4q2g<T>::setNc(const ST Nc_)
{
  BaseClass::setNc(Nc_);
  initNc();
}

template <typename T>
void Amp4q2g<T>::initNc()
{
  Nmat[0] = Nc;
  Nmat[1] = V;
  Nmat[2] = 0.;
  Nmat[3] = -1;
  Nmat[4] = Nc*V;
  Nmat[5] = Nc2;
  Nmat[6] = -Nc;
  Nmat[7] = Nc3;
  assert(7 < BaseClass::NmatLen);

  Nmatcc[0] = 0.;
  Nmatcc[1] = Nc2;
  Nmatcc[2] = -Nc2*V;
  Nmatcc[3] = V;
  Nmatcc[4] = Nc3;
  Nmatcc[5] = -1.;
  Nmatcc[6] = -Nc3;
  Nmatcc[7] = -Nc*V;
  Nmatcc[8] = -Nc2;
  Nmatcc[9] = -V*V;
  Nmatcc[10] = Nc;
  Nmatcc[11] = -Nc3*V;
  Nmatcc[12] = -1.-Nc2;
  Nmatcc[13] = Nc2*V;
  Nmatcc[14] = (2.-Nc2)*Nc;
  Nmatcc[15] = 2.*Nc;
  Nmatcc[16] = -Nc4;
  Nmatcc[17] = Nc*V;
  Nmatcc[18] = -Nc*V*V;
  Nmatcc[19] = -Nc*Nc4;
  Nmatcc[20] = -2.*Nc3;
  Nmatcc[21] = -Nc;
  Nmatcc[22] = -Nc*(1.+Nc2);
  Nmatcc[23] = Nc4;
  Nmatcc[24] = 2.*Nc3;
  assert(24 < BaseClass::NmatccLen);

  bornFactor = V/Nc;
  loopFactor = 2.*bornFactor;
  bornccFactor = -0.5*V/Nc2;
}

template <typename T>
void Amp4q2g<T>::getfvpart1_full(const int fv, LoopValue* fvpart)
{
  return getfvpart1_full_(fv, fvpart);
}

template <typename T>
void Amp4q2g<T>::getfvpart1_full(const int fv, LoopResult<T>* fvpart)
{
  return getfvpart1_full_(fv, fvpart);
}

// -------- BEGIN -- automatically generated code -- BEGIN -------- //

template <typename T>
void Amp4q2g<T>::getfvpart0(const int fv, TreeValue* fvpart, TreeValue* fvpartC)
{
  fvSet(fv);
  // 12 primitives
  const TreeValue P123456 = A0(0,1,2,3,4,5);
  const TreeValue P123465 = A0(0,1,2,3,5,4);
  const TreeValue P123546 = A0(0,1,2,4,3,5);
  const TreeValue P123564 = A0(0,1,2,4,5,3);
  const TreeValue P123645 = A0(0,1,2,5,3,4);
  const TreeValue P123654 = A0(0,1,2,5,4,3);
  const TreeValue P125346 = A0(0,1,4,2,3,5);
  const TreeValue P125364 = A0(0,1,4,2,5,3);
  const TreeValue P125634 = A0(0,1,4,5,2,3);
  const TreeValue P126345 = A0(0,1,5,2,3,4);
  const TreeValue P126354 = A0(0,1,5,2,4,3);
  const TreeValue P126534 = A0(0,1,5,4,2,3);
  fvpart[0] = P123456;
  fvpart[1] = P125346;
  fvpart[2] = P125634;
  fvpart[3] = P123465;
  fvpart[4] = P126345;
  fvpart[5] = P126534;
  fvpart[6] = -(P123465+P123645+P123654+P126345+P126354+P126534)/Nc;
  fvpart[7] = (P123564+P123645+P123654+P125364)/Nc;
  fvpart[8] = -P123564/Nc;
  fvpart[9] = -(P123456+P123546+P123564+P125346+P125364+P125634)/Nc;
  fvpart[10] = (P123546+P123564+P123654+P126354)/Nc;
  fvpart[11] = -P123654/Nc;

  if (fvpartC) {
    for (int i=0; i<C0; i++) {
      fvpartC[i] = conj(fvpart[i]);
    }
  }
}

template <typename T>
template <typename LT>
inline
void Amp4q2g<T>::getfvpart1_full_(const int fv, LT* fvpart)
{
  fvSet(fv);
  {
    // 80 primitives
    const LT P123456 = AL(0,1,2,3,4,5);
    const LT P123465 = AL(0,1,2,3,5,4);
    const LT P123546 = AL(0,1,2,4,3,5);
    const LT P123564 = AL(0,1,2,4,5,3);
    const LT P123645 = AL(0,1,2,5,3,4);
    const LT P123654 = AL(0,1,2,5,4,3);
    const LT P124356 = AL(0,1,3,2,4,5);
    const LT P124365 = AL(0,1,3,2,5,4);
    const LT P124536 = AL(0,1,3,4,2,5);
    const LT P124563 = AL(0,1,3,4,5,2);
    const LT P124635 = AL(0,1,3,5,2,4);
    const LT P124653 = AL(0,1,3,5,4,2);
    const LT P125346 = AL(0,1,4,2,3,5);
    const LT P125364 = AL(0,1,4,2,5,3);
    const LT P125436 = AL(0,1,4,3,2,5);
    const LT P125463 = AL(0,1,4,3,5,2);
    const LT P125634 = AL(0,1,4,5,2,3);
    const LT P125643 = AL(0,1,4,5,3,2);
    const LT P126345 = AL(0,1,5,2,3,4);
    const LT P126354 = AL(0,1,5,2,4,3);
    const LT P126435 = AL(0,1,5,3,2,4);
    const LT P126453 = AL(0,1,5,3,4,2);
    const LT P126534 = AL(0,1,5,4,2,3);
    const LT P126543 = AL(0,1,5,4,3,2);
    const LT P143256 = AL(0,3,2,1,4,5);
    const LT P143265 = AL(0,3,2,1,5,4);
    const LT P143526 = AL(0,3,2,4,1,5);
    const LT P143562 = AL(0,3,2,4,5,1);
    const LT P143625 = AL(0,3,2,5,1,4);
    const LT P143652 = AL(0,3,2,5,4,1);
    const LT P145326 = AL(0,3,4,2,1,5);
    const LT P145362 = AL(0,3,4,2,5,1);
    const LT P145632 = AL(0,3,4,5,2,1);
    const LT P146325 = AL(0,3,5,2,1,4);
    const LT P146352 = AL(0,3,5,2,4,1);
    const LT P146532 = AL(0,3,5,4,2,1);
    const LT P152346 = AL(0,4,1,2,3,5);
    const LT P152364 = AL(0,4,1,2,5,3);
    const LT P152436 = AL(0,4,1,3,2,5);
    const LT P152463 = AL(0,4,1,3,5,2);
    const LT P152634 = AL(0,4,1,5,2,3);
    const LT P152643 = AL(0,4,1,5,3,2);
    const LT P154326 = AL(0,4,3,2,1,5);
    const LT P154362 = AL(0,4,3,2,5,1);
    const LT P154632 = AL(0,4,3,5,2,1);
    const LT P156234 = AL(0,4,5,1,2,3);
    const LT P156243 = AL(0,4,5,1,3,2);
    const LT P156432 = AL(0,4,5,3,2,1);
    const LT P162345 = AL(0,5,1,2,3,4);
    const LT P162354 = AL(0,5,1,2,4,3);
    const LT P162435 = AL(0,5,1,3,2,4);
    const LT P162453 = AL(0,5,1,3,4,2);
    const LT P162534 = AL(0,5,1,4,2,3);
    const LT P162543 = AL(0,5,1,4,3,2);
    const LT P164325 = AL(0,5,3,2,1,4);
    const LT P164352 = AL(0,5,3,2,4,1);
    const LT P164532 = AL(0,5,3,4,2,1);
    const LT P165234 = AL(0,5,4,1,2,3);
    const LT P165243 = AL(0,5,4,1,3,2);
    const LT P165432 = AL(0,5,4,3,2,1);
    const LT P321456 = AL(2,1,0,3,4,5);
    const LT P321465 = AL(2,1,0,3,5,4);
    const LT P321546 = AL(2,1,0,4,3,5);
    const LT P321564 = AL(2,1,0,4,5,3);
    const LT P321645 = AL(2,1,0,5,3,4);
    const LT P321654 = AL(2,1,0,5,4,3);
    const LT P325146 = AL(2,1,4,0,3,5);
    const LT P325164 = AL(2,1,4,0,5,3);
    const LT P325614 = AL(2,1,4,5,0,3);
    const LT P326145 = AL(2,1,5,0,3,4);
    const LT P326154 = AL(2,1,5,0,4,3);
    const LT P326514 = AL(2,1,5,4,0,3);
    const LT P352146 = AL(2,4,1,0,3,5);
    const LT P352164 = AL(2,4,1,0,5,3);
    const LT P352614 = AL(2,4,1,5,0,3);
    const LT P356214 = AL(2,4,5,1,0,3);
    const LT P362145 = AL(2,5,1,0,3,4);
    const LT P362154 = AL(2,5,1,0,4,3);
    const LT P362514 = AL(2,5,1,4,0,3);
    const LT P365214 = AL(2,5,4,1,0,3);

    fvpart[0] = Nc*P123456-(P123456+P123654+P124356+P124536+P124563+P126354+P126534+P126543+P162354
    +P162534+P162543+P165234+P165243+P165432+P321654)/Nc;
    fvpart[1] = Nc*P125346+(P123456+P123465+P123546+P124356+P124365+P124635+P125364+P125634+P125643
    +P126453+P126534+P126543+P152346+P152436+P152463+P162453+P162534+P162543-P164352-P352164)/Nc;
    fvpart[2] = Nc*P125634-(P123465+P123645+P123654+P124365+P124563+P125463+P125634+P125643+P143652
    +P152346+P152364+P152436+P165234+P165243+P365214)/Nc;
    fvpart[3] = Nc*P123465-(P123465+P123564+P124365+P124635+P124653+P125364+P125634+P125643+P152364
    +P152634+P152643+P156234+P156243+P156432+P321564)/Nc;
    fvpart[4] = Nc*P126345+(P123456+P123465+P123645+P124356+P124365+P124536+P125463+P125634+P125643
    +P126354+P126534+P126543+P152463+P152634+P152643-P154362+P162345+P162435+P162453-P362154)/Nc;
    fvpart[5] = Nc*P126534-(P123456+P123546+P123564+P124356+P124653+P126453+P126534+P126543+P143562
    +P156234+P156243+P162345+P162354+P162435+P356214)/Nc;
    fvpart[6] = P124365+P124635+P124653+P126435+P126453+P126543+P321456+P321546+P321564-P326514+P356214
    +P362145+P362154+(P143562+P145362+P145632+P154362+P154632+P156234+P156243+P156432
    +P326514)/Nc2;
    fvpart[7] = -(P124356+P124365+P124536+P125436+P125643+P126435+P126453+P126543+P143256+P143265
    +P143625+P146325+P162435+P162453+P162543+P164325+P321456+P321465+P321546+P325146+P352146)
    +(-P145632-P146352-P146532+P152364-P152463-P154632-P325164-P325614-P326514-P362514)/Nc2;
    fvpart[8] = P124356+P125643+P126435+P143256+P143526+P143562-P146532+P152643+P156243+P156432+P162435
    +P164325+P164352+(P123564+P124653+P146532+P321564+P325164+P325614+P352164+P352614
    +P356214)/Nc2;
    fvpart[9] = P124356+P124536+P124563+P125436+P125463+P125643+P321465+P321645+P321654-P325614+P352146
    +P352164+P365214+(P143652+P146352+P146532+P164352+P164532+P165234+P165243+P165432
    +P325614)/Nc2;
    fvpart[10] = -(P124356+P124365+P124635+P125436+P125463+P125643+P126435+P126543+P143256+P143265
    +P143526+P145326+P152436+P152463+P152643+P154326+P321456+P321465+P321645+P326145+P362145)
    +(-P145362-P145632-P146532+P162354-P162453-P164532-P325614-P326154-P326514-P352614)/Nc2;
    fvpart[11] = P124365+P125436+P126543+P143265+P143625+P143652-P145632+P152436+P154326+P154362+P162543
    +P165243+P165432+(P123654+P124563+P145632+P321654+P326154+P326514+P362154+P362514
    +P365214)/Nc2;
    fvpart[12] = P123456+P123465+P123546+P123564+P123645+P123654+P125346+P125364+P125634+P126345+P126354
    +P126534+P152346+P152364+P152634+P156234+P162345+P162354+P162534+P165234;
    fvpart[13] = -(P143256+P143265+P143526+P143562+P143625+P143652+P145326+P145362+P145632+P146325
    +P146352+P146532+P154326+P154362+P154632+P156432+P164325+P164352+P164532+P165432+P321456
    +P321465+P321546+P321564+P321645+P321654+P325146+P325164+P325614+P326145+P326154+P326514
    +P352146+P352164+P352614+P356214+P362145+P362154+P362514+P365214)/Nc;

  }
  if (Nf != 0.) {
    // 13 primitives
    const LT Q123456 = AF(0,1,2,3,4,5);
    const LT Q123465 = AF(0,1,2,3,5,4);
    const LT Q123546 = AF(0,1,2,4,3,5);
    const LT Q125346 = AF(0,1,4,2,3,5);
    const LT Q125634 = AF(0,1,4,5,2,3);
    const LT Q125643 = AF(0,1,4,5,3,2);
    const LT Q126345 = AF(0,1,5,2,3,4);
    const LT Q126435 = AF(0,1,5,3,2,4);
    const LT Q126453 = AF(0,1,5,3,4,2);
    const LT Q126534 = AF(0,1,5,4,2,3);
    const LT Q126543 = AF(0,1,5,4,3,2);
    const LT Q152634 = AF(0,4,1,5,2,3);
    const LT Q152643 = AF(0,4,1,5,3,2);

    fvpart[0] += Nf*(-Q123456);
    fvpart[1] += Nf*(-Q125346);
    fvpart[2] += Nf*(-Q125634);
    fvpart[3] += Nf*(-Q123465);
    fvpart[4] += Nf*(-Q126345);
    fvpart[5] += Nf*(-Q126534);
    fvpart[6] += Nf*((Q123465-Q126435-Q126453-Q126543)/Nc);
    fvpart[7] += Nf*(-(Q152634+Q152643)/Nc);
    fvpart[8] += Nf*(-(Q125643+Q126435-Q126534+Q152643)/Nc);
    fvpart[9] += Nf*((Q123456+Q123546+Q125346-Q125643)/Nc);
    fvpart[10] += Nf*(-(Q123546+Q126453)/Nc);
    fvpart[11] += Nf*((Q125634+Q126345-Q126543+Q152634)/Nc);
    fvpart[13] += Nf*(2.*(Q125643+Q126435+Q126453+Q126543+Q152643)/Nc2);
  }
}

// --------- END --- automatically generated code --- END --------- //

#ifdef USE_SD
  template class Amp4q2g<double>;
#endif
#ifdef USE_DD
  template class Amp4q2g<dd_real>;
#endif
#ifdef USE_QD
  template class Amp4q2g<qd_real>;
#endif
#ifdef USE_VC
  template class Amp4q2g<Vc::double_v>;
#endif
