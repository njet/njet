/*
* chsums/NJetRenorm.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012, 2013 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef CHSUM_NJETRENORM_H
#define CHSUM_NJETRENORM_H

#include "../ngluon2/utility.h"
#include "../ngluon2/EpsTriplet.h"

template <typename T>
class NJetAmp;

template <typename T>
class NJetRenorm
{
  public:
    typedef typename vector_traits<T>::scalar ST;

    enum SCHEME {SCHEME_NONE=-1, SCHEME_FDH=0, SCHEME_CDR=1, SCHEME_ZERO=2};
    enum RENORM {UNRENORM=0, QCDRENORM=1};

    NJetRenorm(const NJetAmp<T>* amp=0);

    void initialize(const NJetAmp<T>* amp);

    EpsTriplet<T> Renormalize(const T born, const EpsTriplet<T>& virt) const;

    void setNc(const ST Nc_);
    void setNf(const ST Nf_);

    void setScheme(int x);
    void setRenorm(int x);

    int getScheme() const { return scheme; }
    int getRenorm() const { return renorm; }

    int legsQCD() const { return legs; }
    int getAlphasPow() const { return legs-2; }

  protected:
    ST Nc, Nf, CF, BETA0;
    int legs, nG, nQ;

    int scheme, renorm;
};

#endif /* CHSUM_NJETRENORM_H */
