/*
* chsums/4q0g.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>

#include "4q0g.h"
#include "../ngluon2/Model.h"

// class Amp4q0g

template <typename T>
Amp4q0g<T>::Amp4q0g(const T scalefactor, const int mFC, const NJetAmpTables& tables)
  : BaseClass(mFC, tables)
{
  for (int fv=0; fv<mFC; fv++) {
    BaseClass::initNG(scalefactor, fv);
    BaseClass::setProcess(StandardModel::NGluon1compat(NN, getFlav(fv)), fv);
  }

  initNc();
}

template <typename T>
void Amp4q0g<T>::setNc(const ST Nc_)
{
  BaseClass::setNc(Nc_);
  initNc();
}

template <typename T>
void Amp4q0g<T>::initNc()
{
  Nmat[0] = Nc;
  Nmat[1] = 1.;
  assert(1 < BaseClass::NmatLen);

  Nmatcc[0] = 0.;
  Nmatcc[1] = -Nc;
  Nmatcc[2] = -Nc2;
  Nmatcc[3] = Nc;
  assert(3 < BaseClass::NmatccLen);

  bornFactor = Nc;
  loopFactor = 2.*bornFactor;
  bornccFactor = -0.5*V/Nc;
}

template <typename T>
void Amp4q0g<T>::getfvpart1_full(const int fv, LoopValue* fvpart)
{
  return getfvpart1_full_(fv, fvpart);
}

template <typename T>
void Amp4q0g<T>::getfvpart1_full(const int fv, LoopResult<T>* fvpart)
{
  return getfvpart1_full_(fv, fvpart);
}

// -------- BEGIN -- automatically generated code -- BEGIN -------- //

template <typename T>
void Amp4q0g<T>::getfvpart0(const int fv, TreeValue* fvpart, TreeValue* fvpartC)
{
  fvSet(fv);
  // 1 primitive
  const TreeValue P1234 = A0(0,1,2,3);
  fvpart[0] = P1234;
  fvpart[1] = -P1234/Nc;

  if (fvpartC) {
    for (int i=0; i<C0; i++) {
      fvpartC[i] = conj(fvpart[i]);
    }
  }
}

template <typename T>
template <typename LT>
inline
void Amp4q0g<T>::getfvpart1_full_(const int fv, LT* fvpart)
{
  fvSet(fv);
  {
    // 4 primitives
    const LT P1234 = AL(0,1,2,3);
    const LT P1243 = AL(0,1,3,2);
    const LT P1432 = AL(0,3,2,1);
    const LT P3214 = AL(2,1,0,3);

    fvpart[0] = P1234*Nc - (2.*P1234+2.*P1243+P1432+P3214)/Nc;
    fvpart[1] = P1243 + (P1234+P1243+P1432+P3214)/Nc2;

  }
  if (Nf != 0.) {
    // 1 primitive
    const LT Q1234 = AF(0,1,2,3);

    fvpart[0] += Nf*(-Q1234);
    fvpart[1] += Nf*(Q1234/Nc);

  }
}

// --------- END --- automatically generated code --- END --------- //

#ifdef USE_SD
  template class Amp4q0g<double>;
#endif
#ifdef USE_DD
  template class Amp4q0g<dd_real>;
#endif
#ifdef USE_QD
  template class Amp4q0g<qd_real>;
#endif
#ifdef USE_VC
  template class Amp4q0g<Vc::double_v>;
#endif
