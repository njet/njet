/*
* chsums/2q2gA.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include "2q2gA.h"
#include "NJetAmp-T.h"

// class Amp2q2gAx

template <typename T>
Amp2q2gAx<T>::Amp2q2gAx(const Flavour<double>& ff, const T scalefactor,
                        const int mFC, const NJetAmpTables& tables)
 : BaseClass(ff, scalefactor, mFC, tables)
{
  const Flavour<double> Ax = StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C());
  for (int fv=0; fv<mFC; fv++) {
    if (fv % 2 == 0) {
      Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
      flavours.push_back(Ax);
      BaseClass::setProcess(flavours, fv);
    }
  }
}

template <typename T>
void Amp2q2gAx<T>::setNf(const ST Nf_)
{
  ST newNf = Nf_;
  const int intNf = int(to_double(newNf));
  if (double(intNf) == newNf) {
    const int nu = intNf/2;
    const int nd = (intNf+1)/2;
    newNf = -(double(nu)*Particle<ST>::getQu() + double(nd)*Particle<ST>::getQd());
  }
  BaseClass::setNf(newNf);
}

template <typename T>
void Amp2q2gAx<T>::getfvpart1_full(const int fv, LoopValue* fvpart)
{
  return getfvpart1_full_(fv, fvpart);
}

template <typename T>
void Amp2q2gAx<T>::getfvpart1_full(const int fv, LoopResult<T>* fvpart)
{
  return getfvpart1_full_(fv, fvpart);
}

template <typename T>
template <typename LT>
inline
void Amp2q2gAx<T>::getfvpart1_full_(const int fv, LT* fvpart)
{
  fvSet(fv);

  if (fv % 2 == 0 and Nf != 0.) {
    // 1 primitives
    const LT Q1234 = AFx(0,1,2,3);

    fvpart[0] = Nf*(-Q1234);
    fvpart[1] = Nf*(-Q1234);
    fvpart[2] = Nf*(2.*Q1234/Nc);
  } else {
    fvpart[0] = LT();
    fvpart[1] = LT();
    fvpart[2] = LT();
  }
}

template <typename T>
LoopResult<T> Amp2q2gAx<T>::AFx(int p0, int p1, int p2, int p3)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], O[p1], O[p2], O[p3], NN};
  return BaseClass::AFnVax(order);
}

// class Amp2q2gAA

template <typename T>
Amp2q2gAA<T>::Amp2q2gAA(const Flavour<double>& ff, const T scalefactor,
                        const int mFC, const NJetAmpTables& tables)
  : BaseClass(scalefactor, mFC, tables)
{
  initProcess(ff);
}

template <typename T>
void Amp2q2gAA<T>::initProcess(const Flavour<double>& ff)
{
  for (int fv=0; fv<BaseClass::mFC; fv++) {
    Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
    flavours.push_back(ff);
    flavours.push_back(ff);
    BaseClass::setProcess(flavours, fv);
  }
}

template <typename T>
typename Amp2q2gAA<T>::TreeValue
Amp2q2gAA<T>::A0(int p0, int p1, int p2, int p3)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, NN+1, O[p1], O[p2], O[p3]};
  return BaseClass::A0nAA(order);
}

template <typename T>
LoopResult<T> Amp2q2gAA<T>::AF(int p0, int p1, int p2, int p3)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, NN+1, O[p1], O[p2], O[p3]};
  return BaseClass::AFnAA(order);
}

template <typename T>
LoopResult<T> Amp2q2gAA<T>::AL(int p0, int p1, int p2, int p3)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, NN+1, O[p1], O[p2], O[p3]};
  return BaseClass::ALnAA(order);
}

// class Amp2q2gAAx

template <typename T>
Amp2q2gAAx<T>::Amp2q2gAAx(const Flavour<double>& ff, const T scalefactor,
                          const int mFC, const NJetAmpTables& tables)
 : BaseClass(ff, scalefactor, mFC, tables)
{
  const Flavour<double> Ax = StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C());
  for (int fv=0; fv<mFC; fv++) {
    if (fv % modFC == 0) {
      Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
      flavours.push_back(ff);
      flavours.push_back(Ax);
      BaseClass::setProcess(flavours, fv);
    }
    else if (fv % modFC == 1) {
      Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
      flavours.push_back(Ax);
      flavours.push_back(Ax);
      BaseClass::setProcess(flavours, fv);
    }
  }
}

template <typename T>
void Amp2q2gAAx<T>::setNf(const ST Nf_)
{
  const int intNf = int(to_double(Nf_));
  if (double(intNf) == Nf_) {
    const int nu = intNf/2;
    const int nd = (intNf+1)/2;
    Nfx = -(double(nu)*Particle<ST>::getQu() + double(nd)*Particle<ST>::getQd());
    Nfxx = (double(nu)*Particle<ST>::getQu()*Particle<ST>::getQu()
          + double(nd)*Particle<ST>::getQd()*Particle<ST>::getQd());
  }
  BaseClass::setNf(Nf_);
}

template <typename T>
void Amp2q2gAAx<T>::getfvpart1_full(const int fv, LoopValue* fvpart)
{
  return getfvpart1_full_(fv, fvpart);
}

template <typename T>
void Amp2q2gAAx<T>::getfvpart1_full(const int fv, LoopResult<T>* fvpart)
{
  return getfvpart1_full_(fv, fvpart);
}

template <typename T>
template <typename LT>
inline
void Amp2q2gAAx<T>::getfvpart1_full_(const int fv, LT* fvpart)
{
  fvSet(fv);

  if (fv % modFC == 0 and Nf != 0.) {
    // 1 primitives
    const LT Q1234 = AFx(0,1,2,3);

    fvpart[0] = Nfx*(-Q1234);
    fvpart[1] = Nfx*(-Q1234);
    fvpart[2] = Nfx*(2.*Q1234/Nc);
  } else if (fv % modFC == 1 and Nf != 0.) {
    // 2 primitives
    const LT Q1234 = AFxx(0,1,2,3);
    const LT Q1243 = AFxx(0,1,3,2);

    fvpart[0] = Nfxx*(-Q1234);
    fvpart[1] = Nfxx*(-Q1243);
    fvpart[2] = LT(); /* Nf*(Q1234 + Q1243)/Nc; */
  } else {
    fvpart[0] = LT();
    fvpart[1] = LT();
    fvpart[2] = LT();
  }
}

template <typename T>
LoopResult<T> Amp2q2gAAx<T>::AFx(int p0, int p1, int p2, int p3)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, O[p1], O[p2], O[p3], NN+1};
  return BaseClass::AFnAAx(order);
}

template <typename T>
LoopResult<T> Amp2q2gAAx<T>::AFxx(int p0, int p1, int p2, int p3)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], O[p1], O[p2], O[p3], NN, NN+1};
  return BaseClass::AFnAAxx(order);
}

#ifdef USE_SD
  template class Amp2q2gAx<double>;
  template class Amp2q2gAA<double>;
  template class Amp2q2gAAx<double>;
#endif
#ifdef USE_DD
  template class Amp2q2gAx<dd_real>;
  template class Amp2q2gAA<dd_real>;
  template class Amp2q2gAAx<dd_real>;
#endif
#ifdef USE_QD
  template class Amp2q2gAx<qd_real>;
  template class Amp2q2gAA<qd_real>;
  template class Amp2q2gAAx<qd_real>;
#endif
#ifdef USE_VC
  template class Amp2q2gAx<Vc::double_v>;
  template class Amp2q2gAA<Vc::double_v>;
  template class Amp2q2gAAx<Vc::double_v>;
#endif
