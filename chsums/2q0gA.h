/*
* chsums/2q0gA.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012, 2013 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef CHSUM_2Q0GA_H
#define CHSUM_2Q0GA_H

#include "2q0gV.h"

// Two photons

class Amp2q0gAAStatic : public Amp2q0gVStatic
{
  public:
    static const int HS = 8;
    static const int HSNN = NN+2;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp2q0gAA : public Amp2q0gV<T>
{
    typedef Amp2q0gV<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp2q0gAA(const Flavour<double>& Vflav, const T scalefactor,
              const int mFC=1, const NJetAmpTables& tables=amptables());

  protected:
    using BaseClass::NN;
    using BaseClass::vhel;
    using BaseClass::getFlav;
    using BaseClass::getFperm;
    using BaseClass::mfv;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp2q0gAAStatic>();
    }

    void initProcess(const Flavour<double>& ff);

    TreeValue A0(int p0, int p1);
    LoopResult<T> AL(int p0, int p1);
    LoopResult<T> AF(int p0, int p1);
};

#endif // CHSUM_2Q0GA_H
