/*
* chsums/2q0gA-static.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>

#include "2q0gA.h"

// class Amp2q0gAAStatic

const int
Amp2q0gAAStatic::HSarr[HS][HSNN] = {
  { 1,-1,-1, 1},
  { 1,-1, 1, 1},
  {-1, 1,-1, 1},
  {-1, 1, 1, 1},
  { 1,-1,-1,-1},
  { 1,-1, 1,-1},
  {-1, 1,-1,-1},
  {-1, 1, 1,-1},
};
