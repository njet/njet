/*
* chsums/4q0gA.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef CHSUM_4Q0GA_H
#define CHSUM_4Q0GA_H

#include "4q0gV.h"
#include "../ngluon2/Model.h"

template <typename T>
class Amp4q0gA : public Amp4q0gZ<T>
{
    typedef Amp4q0gZ<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp4q0gA(const Flavour<double>& ff, const T scalefactor,
             const int mFC=2, const NJetAmpTables& tables=BaseClass::amptables())
    : BaseClass(ff, scalefactor, mFC, tables)
    { }

    void born_part_fill() { BaseClass::born_part_trickfill(); }
    LoopValue virt() { return BaseClass::virt_tricksum(); }
};

template <typename T>
class Amp4q0gAd : public Amp4q0gZd<T>
{
    typedef Amp4q0gZd<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp4q0gAd(const Flavour<double>& ff, const T scalefactor,
              const int mFC=2, const NJetAmpTables& tables=BaseClass::amptables())
    : BaseClass(ff, scalefactor, mFC, tables)
    { }

    void born_part_fill() { BaseClass::born_part_trickfill(); }
    LoopValue virt() { return BaseClass::virt_tricksum(); }
};

template <typename T>
class Amp4q0gA2 : public Amp4q0gZ2<T>
{
    typedef Amp4q0gZ2<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp4q0gA2(const Flavour<double>& ff, const T scalefactor,
              const int mFC=4, const NJetAmpTables& tables=BaseClass::amptables())
    : BaseClass(ff, scalefactor, mFC, tables)
    { }

    void born_part_fill() { BaseClass::born_part_trickfill(); }
    LoopValue virt() { return BaseClass::virt_tricksum(); }
};

// One photon vector loop

class Amp4q0gAxStatic : public Amp4q0gZStatic
{
  public:
    static const int FC = 6;

    static const int flav[FC][NN];  // flavour vectors for ngluon
    static const int fvsign[FC];    // flavour channels' signs
    static const int fperm[FC][NN];        // flavour pemutation matrix
    static const int fvcol[FC][CC];        // flavour-partial pemutation matrix
};

template <typename T>
class Amp4q0gAx : public Amp4q0gA<T>
{
    typedef Amp4q0gA<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;
    typedef typename BaseClass::ST ST;

    Amp4q0gAx(const Flavour<double>& ff, const T scalefactor,
              const int mFC=3, const NJetAmpTables& tables=amptables());

    void setNf(const ST Nf_);

  protected:
    using BaseClass::NN;
    using BaseClass::Nc;
    using BaseClass::Nf;
    using BaseClass::vhel;
    using BaseClass::getFlav;
    using BaseClass::getFperm;
    using BaseClass::fvSet;
    using BaseClass::mfv;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp4q0gAxStatic>();
    }

    LoopResult<T> AFx(int p0, int p1, int p2, int p3);

    void getfvpart1_full(const int fv, LoopValue* fvpart);
    void getfvpart1_full(const int fv, LoopResult<T>* fvpart);

    template <typename LT>
    void getfvpart1_full_(const int fv, LT* fvpart);
};

template <typename T>
class Amp4q0gAxd : public Amp4q0gAx<T>
{
    typedef Amp4q0gAx<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp4q0gAxd(const Flavour<double>& ff, const T scalefactor,
              const int mFC=3, const NJetAmpTables& tables=BaseClass::amptables());

  protected:
    using BaseClass::NN;
    using BaseClass::getFlav;
};

class Amp4q0gAx2Static : public Amp4q0gAxStatic
{
  public:
    static const int HS = 12;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp4q0gAx2 : public Amp4q0gAx<T>
{
    typedef Amp4q0gAx<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp4q0gAx2(const Flavour<double>& ff, const T scalefactor,
               const int mFC=6, const NJetAmpTables& tables=amptables())
    : BaseClass(ff, scalefactor, mFC, tables)
    { }

  protected:
    using BaseClass::fvZero;
    using BaseClass::vhel;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp4q0gAx2Static>();
    }

    void markZeroFv()
    {
      fvZero.reset();
      if (vhel[0] == vhel[1] || vhel[2] == vhel[3]) {
        fvZero[0] = true;
        fvZero[1] = true;
        fvZero[2] = true;
      }
      if (vhel[0] == vhel[3] || vhel[2] == vhel[1]) {
        fvZero[3] = true;
        fvZero[4] = true;
        fvZero[5] = true;
      }
    }
};

// Two photons

class Amp4q0gAAStatic : public Amp4q0gStatic
{
  public:
    static const int FC = 8;

    static const int flav[FC][NN];  // flavour vectors for ngluon
    static const int fvsign[FC];    // flavour channels' signs
    static const int fperm[FC][NN];        // flavour pemutation matrix
    static const int fvcol[FC][CC];        // flavour-partial pemutation matrix

    static const int HS = 16;
    static const int HSNN = NN+2;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp4q0gAA : public Amp4q0g<T>
{
    typedef Amp4q0g<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp4q0gAA(const Flavour<double>& Vflav, const T scalefactor,
              const int mFC=4, const NJetAmpTables& tables=amptables());

  protected:
    using BaseClass::NN;
    using BaseClass::vhel;
    using BaseClass::getFlav;
    using BaseClass::getFperm;
    using BaseClass::mfv;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp4q0gAAStatic>();
    }

    void initProcess(const Flavour<double>& ff);

    TreeValue A0(int p0, int p1, int p2, int p3);
    LoopResult<T> AL(int p0, int p1, int p2, int p3);
    LoopResult<T> AF(int p0, int p1, int p2, int p3);
};

template <typename T>
class Amp4q0gAAd : public Amp4q0gAA<T>
{
    typedef Amp4q0gAA<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp4q0gAAd(const Flavour<double>& Vflav, const T scalefactor,
               const int mFC=4, const NJetAmpTables& tables=BaseClass::amptables());

  protected:
    using BaseClass::NN;
    using BaseClass::getFlav;
    using BaseClass::mfv;

    void initProcess(const Flavour<double>& ff);
};

class Amp4q0gAA2Static : public Amp4q0gAAStatic
{
  public:
    static const int HS = 24;
    static const int HSNN = NN+2;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp4q0gAA2 : public Amp4q0gAA<T>
{
    typedef Amp4q0gAA<T> BaseClass;
  public:

    Amp4q0gAA2(const Flavour<double>& Vflav,
               const T scalefactor, const NJetAmpTables& tables=amptables())
      : BaseClass(Vflav, scalefactor, 8, tables)
    { }

  protected:
    using BaseClass::fvZero;
    using BaseClass::vhel;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp4q0gAA2Static>();
    }

    void markZeroFv()
    {
      fvZero.reset();
      if (vhel[0] == vhel[1] || vhel[2] == vhel[3]) {
        fvZero[0] = true;
        fvZero[1] = true;
        fvZero[2] = true;
        fvZero[3] = true;
      }
      if (vhel[0] == vhel[3] || vhel[2] == vhel[1]) {
        fvZero[4] = true;
        fvZero[5] = true;
        fvZero[6] = true;
        fvZero[7] = true;
      }
    }
};

// Two photons vector loop

class Amp4q0gAAxStatic : public Amp4q0gAAStatic
{
  public:
    static const int FC = 12;

    static const int flav[FC][NN];  // flavour vectors for ngluon
    static const int fvsign[FC];    // flavour channels' signs
    static const int fperm[FC][NN];        // flavour pemutation matrix
    static const int fvcol[FC][CC];        // flavour-partial pemutation matrix
};

template <typename T>
class Amp4q0gAAx : public Amp4q0gAA<T>
{
    typedef Amp4q0gAA<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;
    typedef typename BaseClass::ST ST;

    Amp4q0gAAx(const Flavour<double>& ff, const T scalefactor,
               const int mFC=6, const NJetAmpTables& tables=amptables());

    void setNf(const ST Nf_);

  protected:
    using BaseClass::NN;
    using BaseClass::Nc;
    using BaseClass::Nf;
    using BaseClass::vhel;
    using BaseClass::getFlav;
    using BaseClass::getFperm;
    using BaseClass::fvSet;
    using BaseClass::mfv;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp4q0gAAxStatic>();
    }

    static const int modFC = 6;
    ST Nfx, Nfxx;

    LoopResult<T> AFx(int p0, int p1, int p2, int p3);
    LoopResult<T> AFxx(int p0, int p1, int p2, int p3);

    void getfvpart1_full(const int fv, LoopValue* fvpart);
    void getfvpart1_full(const int fv, LoopResult<T>* fvpart);

    template <typename LT>
    void getfvpart1_full_(const int fv, LT* fvpart);
};

template <typename T>
class Amp4q0gAAxd : public Amp4q0gAAx<T>
{
    typedef Amp4q0gAAx<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp4q0gAAxd(const Flavour<double>& Vflav, const T scalefactor,
                const int mFC=6, const NJetAmpTables& tables=BaseClass::amptables());

  protected:
    using BaseClass::NN;
    using BaseClass::getFlav;
    using BaseClass::mfv;
};

class Amp4q0gAAx2Static : public Amp4q0gAAxStatic
{
  public:
    static const int HS = 24;
    static const int HSNN = NN+2;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp4q0gAAx2 : public Amp4q0gAAx<T>
{
    typedef Amp4q0gAAx<T> BaseClass;
  public:

    Amp4q0gAAx2(const Flavour<double>& Vflav,
               const T scalefactor, const NJetAmpTables& tables=amptables())
      : BaseClass(Vflav, scalefactor, 12, tables)
    { }

  protected:
    using BaseClass::fvZero;
    using BaseClass::vhel;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp4q0gAAx2Static>();
    }

    void markZeroFv()
    {
      fvZero.reset();
      if (vhel[0] == vhel[1] || vhel[2] == vhel[3]) {
        fvZero[0] = true;
        fvZero[1] = true;
        fvZero[2] = true;
        fvZero[3] = true;
        fvZero[4] = true;
        fvZero[5] = true;
      }
      if (vhel[0] == vhel[3] || vhel[2] == vhel[1]) {
        fvZero[6] = true;
        fvZero[7] = true;
        fvZero[8] = true;
        fvZero[9] = true;
        fvZero[10] = true;
        fvZero[11] = true;
      }
    }
};

#endif // CHSUM_4Q0GA_H
