/*
* chsums/2q0gV-static.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>

#include "2q0gV.h"

// class Amp2q0gVStatic

const int
Amp2q0gVStatic::flav[FC][NN] = {
  {-1,1}
};

const int
Amp2q0gVStatic::fvsign[FC] = {1};

const int
Amp2q0gVStatic::ccsign[NN*(NN+1)/2] = {1,1,1};

const int
Amp2q0gVStatic::fperm[FC][NN] = {
    {0,1}
};

const int
Amp2q0gVStatic::fvcol[FC][CC] = {
    {0}
};

const unsigned char
Amp2q0gVStatic::colmat[CC*(CC+1)/2] = {
  0
};

const unsigned char
Amp2q0gVStatic::colmatcc[NN*(NN+1)/2][C0*(C0+1)/2] = {
    {0},
    {1},
    {0}
};

const int
Amp2q0gVStatic::HSarr[HS][HSNN] = {
  { 1,-1, 1}
};

// class Amp2q0gZStatic

const int
Amp2q0gZStatic::HSarr[HS][HSNN] = {
  { 1,-1,-1},
  { 1,-1, 1},
  {-1, 1,-1},
  {-1, 1, 1}
};
