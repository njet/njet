/*
* chsums/4q1gH-static.cpp
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>

#include "4q1gH.h"

// class Amp4q1gHStatic

const int
Amp4q1gHStatic::HSarr[HS][HSNN] = {
  {-1, 1, 1,-1, 1, 1},
  {-1, 1, 1,-1,-1, 1},
  {-1, 1,-1, 1, 1, 1},
  {-1, 1,-1, 1,-1, 1},
  { 1,-1,-1, 1,-1, 1},
  { 1,-1,-1, 1, 1, 1},
  { 1,-1, 1,-1,-1, 1},
  { 1,-1, 1,-1, 1, 1},
};

// class Amp4q1gH2Static

const int
Amp4q1gH2Static::HSarr[HS][HSNN] = {
  {-1, 1, 1,-1, 1, 1},
  {-1, 1, 1,-1,-1, 1},
  {-1, 1,-1, 1, 1, 1},
  {-1, 1,-1, 1,-1, 1},
  {-1,-1, 1, 1,-1, 1},
  {-1,-1, 1, 1, 1, 1},
  { 1,-1,-1, 1,-1, 1},
  { 1,-1,-1, 1, 1, 1},
  { 1,-1, 1,-1,-1, 1},
  { 1,-1, 1,-1, 1, 1},
  { 1, 1,-1,-1, 1, 1},
  { 1, 1,-1,-1,-1, 1},
};
