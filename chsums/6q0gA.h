/*
* chsums/6q0gA.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef CHSUM_6Q0GA_H
#define CHSUM_6Q0GA_H

#include "6q0gV.h"

template <typename T>
class Amp6q0gA : public Amp6q0gZ<T>
{
    typedef Amp6q0gZ<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp6q0gA(const Flavour<double>& ff, const T scalefactor,
             const int mFC=3, const NJetAmpTables& tables=BaseClass::amptables())
    : BaseClass(ff, scalefactor, mFC, tables)
    { }

    void born_part_fill() { BaseClass::born_part_trickfill(); }
    LoopValue virt() { return BaseClass::virt_tricksum(); }
};

template <typename T>
class Amp6q0gAd : public Amp6q0gZd<T>
{
    typedef Amp6q0gZd<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp6q0gAd(const Flavour<double>& ff, const T scalefactor,
              const int mFC=3, const NJetAmpTables& tables=BaseClass::amptables())
    : BaseClass(ff, scalefactor, mFC, tables)
    { }

    void born_part_fill() { BaseClass::born_part_trickfill(); }
    LoopValue virt() { return BaseClass::virt_tricksum(); }
};

template <typename T>
class Amp6q0gA2 : public Amp6q0gZ2<T>
{
    typedef Amp6q0gZ2<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp6q0gA2(const Flavour<double>& ff, const T scalefactor,
              const int mFC=6, const NJetAmpTables& tables=BaseClass::amptables())
    : BaseClass(ff, scalefactor, mFC, tables)
    { }

    void born_part_fill() { BaseClass::born_part_trickfill(); }
    LoopValue virt() { return BaseClass::virt_tricksum(); }
};

template <typename T>
class Amp6q0gA2d : public Amp6q0gZ2d<T>
{
    typedef Amp6q0gZ2d<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp6q0gA2d(const Flavour<double>& ff, const T scalefactor,
               const int mFC=6, const NJetAmpTables& tables=BaseClass::amptables())
    : BaseClass(ff, scalefactor, mFC, tables)
    { }

    void born_part_fill() { BaseClass::born_part_trickfill(); }
    LoopValue virt() { return BaseClass::virt_tricksum(); }
};

template <typename T>
class Amp6q0gA6 : public Amp6q0gZ6<T>
{
    typedef Amp6q0gZ6<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp6q0gA6(const Flavour<double>& ff, const T scalefactor,
              const int mFC=18, const NJetAmpTables& tables=BaseClass::amptables())
    : BaseClass(ff, scalefactor, mFC, tables)
    { }

    void born_part_fill() { BaseClass::born_part_trickfill(); }
    LoopValue virt() { return BaseClass::virt_tricksum(); }
};

// Two photons

class Amp6q0gAAStatic : public Amp6q0gStatic
{
  public:
    static const int FC = 54;

    static const int flav[FC][NN];  // flavour vectors for ngluon
    static const int fvsign[FC];    // flavour channels' signs
    static const int fperm[FC][NN];        // flavour pemutation matrix
    static const int fvcol[FC][CC];        // flavour-partial pemutation matrix

    static const int HS = 32;
    static const int HSNN = NN+2;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp6q0gAA : public Amp6q0g<T>
{
    typedef Amp6q0g<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp6q0gAA(const Flavour<double>& Vflav, const T scalefactor,
              const int mFC=9, const NJetAmpTables& tables=amptables());

  protected:
    using BaseClass::NN;
    using BaseClass::vhel;
    using BaseClass::getFlav;
    using BaseClass::getFperm;
    using BaseClass::mfv;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp6q0gAAStatic>();
    }

    void initProcess(const Flavour<double>& ff);

    TreeValue A0(int p0, int p1, int p2, int p3, int p4, int p5);
    LoopResult<T> AL(int p0, int p1, int p2, int p3, int p4, int p5);
    LoopResult<T> AF(int p0, int p1, int p2, int p3, int p4, int p5);
};

template <typename T>
class Amp6q0gAAd : public Amp6q0gAA<T>
{
    typedef Amp6q0gAA<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp6q0gAAd(const Flavour<double>& Vflav, const T scalefactor,
               const int mFC=9, const NJetAmpTables& tables=BaseClass::amptables());

  protected:
    using BaseClass::NN;
    using BaseClass::getFlav;
    using BaseClass::mfv;

    void initProcess(const Flavour<double>& ff);
};

class Amp6q0gAA2Static : public Amp6q0gAAStatic
{
  public:
    static const int HS = 48;
    static const int HSNN = NN+2;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp6q0gAA2 : public Amp6q0gAA<T>
{
    typedef Amp6q0gAA<T> BaseClass;
  public:

    Amp6q0gAA2(const Flavour<double>& Vflav,
               const T scalefactor, const NJetAmpTables& tables=amptables())
      : BaseClass(Vflav, scalefactor, 18, tables)
    { }

  protected:
    using BaseClass::fvZero;
    using BaseClass::vhel;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp6q0gAA2Static>();
    }

    void markZeroFv()
    {
      fvZero.reset();
      if (vhel[0] == vhel[1] || vhel[2] == vhel[3]) {
        for (int i=9*0; i<9*1; i++) {
          fvZero[i] = true;
        }
      }
      if (vhel[0] == vhel[3] || vhel[2] == vhel[1]) {
        for (int i=9*1; i<9*2; i++) {
          fvZero[i] = true;
        }
      }
    }
};

template <typename T>
class Amp6q0gAA2d : public Amp6q0gAA2<T>
{
    typedef Amp6q0gAA2<T> BaseClass;
  public:

    Amp6q0gAA2d(const Flavour<double>& ff, const T scalefactor,
                const NJetAmpTables& tables=BaseClass::amptables());

  protected:
    using BaseClass::NN;
    using BaseClass::getFlav;
    using BaseClass::mfv;

    void initProcess(const Flavour<double>& ff);
};

class Amp6q0gAA6Static : public Amp6q0gAAStatic
{
  public:
    static const int HS = 80;
    static const int HSNN = NN+2;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp6q0gAA6 : public Amp6q0gAA<T>
{
    typedef Amp6q0gAA<T> BaseClass;
  public:

    Amp6q0gAA6(const Flavour<double>& Vflav,
               const T scalefactor, const NJetAmpTables& tables=amptables())
      : BaseClass(Vflav, scalefactor, 54, tables)
    { }

  protected:
    using BaseClass::fvZero;
    using BaseClass::vhel;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp6q0gAA6Static>();
    }

    void markZeroFv()
    {
      fvZero.reset();
      if (vhel[0] == vhel[1]) {
        for (int i=9*0; i<9*1; i++) {
          fvZero[i] = true;
        }
        for (int i=9*5; i<9*6; i++) {
          fvZero[i] = true;
        }
      }
      if (vhel[0] == vhel[3]) {
        for (int i=9*1; i<9*2; i++) {
          fvZero[i] = true;
        }
        for (int i=9*4; i<9*5; i++) {
          fvZero[i] = true;
        }
      }
      if (vhel[0] == vhel[5]) {
        for (int i=9*2; i<9*3; i++) {
          fvZero[i] = true;
        }
        for (int i=9*3; i<9*4; i++) {
          fvZero[i] = true;
        }
      }
      if (vhel[2] == vhel[3]) {
        for (int i=9*0; i<9*1; i++) {
          fvZero[i] = true;
        }
        for (int i=9*3; i<9*4; i++) {
          fvZero[i] = true;
        }
      }
      if (vhel[2] == vhel[5]) {
        for (int i=9*4; i<9*5; i++) {
          fvZero[i] = true;
        }
        for (int i=9*5; i<9*6; i++) {
          fvZero[i] = true;
        }
      }
      if (vhel[2] == vhel[1]) {
        for (int i=9*1; i<9*2; i++) {
          fvZero[i] = true;
        }
        for (int i=9*2; i<9*3; i++) {
          fvZero[i] = true;
        }
      }
      if (vhel[4] == vhel[5]) {
        for (int i=9*0; i<9*1; i++) {
          fvZero[i] = true;
        }
        for (int i=9*1; i<9*2; i++) {
          fvZero[i] = true;
        }
      }
      if (vhel[4] == vhel[1]) {
        for (int i=9*3; i<9*4; i++) {
          fvZero[i] = true;
        }
        for (int i=9*4; i<9*5; i++) {
          fvZero[i] = true;
        }
      }
      if (vhel[4] == vhel[3]) {
        for (int i=9*2; i<9*3; i++) {
          fvZero[i] = true;
        }
        for (int i=9*5; i<9*6; i++) {
          fvZero[i] = true;
        }
      }
    }
};

#endif // CHSUM_6Q0G_H
