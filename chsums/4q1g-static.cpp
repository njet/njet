/*
* chsums/4q1g-static.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include "4q1g.h"

// class Amp4q1gStatic

const int
Amp4q1gStatic::flav[FC][NN] = {
    {-1,1,-2,2,0},
    {-1,2,-2,1,0}
};

const int
Amp4q1gStatic::fvsign[FC] = {1,-1};

const int
Amp4q1gStatic::ccsign[NN*(NN+1)/2] = {1,1,1,-1,1,1,1,-1,1,-1,-1,-1,-1,-1,1};

const int
Amp4q1gStatic::fperm[FC][NN] = {
    {0,1,2,3,4},
    {0,3,2,1,4}
};

const int
Amp4q1gStatic::fvcol[FC][CC] = {
    {0,1,2,3},
    {2,3,0,1}
};

const unsigned char
Amp4q1gStatic::colmat[CC*(CC+1)/2] = {2,0,2,1,1,2,1,1,0,2};

const unsigned char
Amp4q1gStatic::colmatcc[NN*(NN+1)/2][C0*(C0+1)/2] = {
    {0},
    {6,0,8,2,3,0,2,3,5,0},
    {0},
    {0,5,0,3,2,0,2,3,5,0},
    {0,5,0,3,3,8,2,2,0,6},
    {0},
    {0,5,0,2,2,6,3,3,0,8},
    {0,5,0,2,3,0,3,2,5,0},
    {8,0,6,3,2,0,3,2,5,0},
    {0},
    {0,0,4,7,1,0,1,1,0,4},
    {0,0,4,1,1,4,7,1,0,0},
    {4,0,0,1,1,4,1,7,0,0},
    {4,0,0,1,7,0,1,1,0,4},
    {0}
};

const int
Amp4q1gStatic::HSarr[HS][HSNN] = {
  {-1, 1, 1,-1, 1},
  {-1, 1, 1,-1,-1},
  {-1, 1,-1, 1, 1},
  {-1, 1,-1, 1,-1},
  { 1,-1,-1, 1,-1},
  { 1,-1,-1, 1, 1},
  { 1,-1, 1,-1,-1},
  { 1,-1, 1,-1, 1},
};

// class Amp4q1g2Static

const int
Amp4q1g2Static::HSarr[HS][HSNN] = {
  {-1, 1, 1,-1, 1},
  {-1, 1, 1,-1,-1},
  {-1, 1,-1, 1, 1},
  {-1, 1,-1, 1,-1},
  {-1,-1, 1, 1,-1},
  {-1,-1, 1, 1, 1},
  { 1,-1,-1, 1,-1},
  { 1,-1,-1, 1, 1},
  { 1,-1, 1,-1,-1},
  { 1,-1, 1,-1, 1},
  { 1, 1,-1,-1, 1},
  { 1, 1,-1,-1,-1},
};
