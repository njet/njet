/*
* chsums/NJetAccuracy.cpp
*
* This file is part of NJet library
* Copyright (C) 2013 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>

#include "NJetAccuracy.h"
#include "NJetAmp.h"

template <typename T>
std::vector<T> NJetAccuracy<T>::tree_buf1;
template <typename T>
std::vector<T> NJetAccuracy<T>::tree_buf2;
template <typename T>
T* NJetAccuracy<T>::cctree1;
template <typename T>
T* NJetAccuracy<T>::cctree2;
template <typename T>
std::complex<T>* NJetAccuracy<T>::sctree1;
template <typename T>
std::complex<T>* NJetAccuracy<T>::sctree2;

template <typename T>
void NJetAccuracy<T>::resize_buffers(int legs)
{
  const int cclen = get_cctree_size(legs);
  const int sclen = get_sctree_size(legs);
  const int cslen = get_cstree_size(legs);
  const int cachelen = std::max(cclen, 2*std::max(sclen, cslen));  // spin-corr are complex
  if (cachelen > int(tree_buf1.size())) {
    tree_buf1.resize(cachelen);
    tree_buf2.resize(cachelen);
  }

  cctree1 = tree_buf1.data();
  cctree2 = tree_buf2.data();

  sctree1 = reinterpret_cast<std::complex<T>*>(tree_buf1.data());
  sctree2 = reinterpret_cast<std::complex<T>*>(tree_buf2.data());
}

template <typename T>
NJetAccuracy<T>::~NJetAccuracy()
{
  for (int i=0; i<AMPS_LEN; i++) {
    delete amps[i];
    amps[i] = 0;
  }
}

template <typename T>
void NJetAccuracy<T>::initialize()
{
  renorm.initialize(amps[0]);
  prevmoms.resize(amps[0]->legsMOM());

  const int legs = renorm.legsQCD();
  resize_buffers(legs);

  cctree_size = get_cctree_size(legs);
  sctree_size = get_sctree_size(legs);
  cstree_size = get_cstree_size(legs);
}

template <typename T>
void NJetAccuracy<T>::setMomenta(const RealMom* moms)
{
  bool changed = false;
  for (unsigned i=0; i<prevmoms.size(); i++) {
    if (changed or prevmoms[i] != moms[i]) {
      prevmoms[i] = moms[i];
      changed = true;
    }
  }
  if (changed) {
    for (int i=0; i<AMPS_LEN; i++) {
      amps[i]->setMomenta(prevmoms);
    }
  }
}

template <typename T>
void NJetAccuracy<T>::setMomenta(const std::vector<RealMom>& moms)
{
  assert(moms.size() == prevmoms.size());
  setMomenta(moms.data());
}

template <typename T>
template <typename U>
void NJetAccuracy<T>::setMomenta(const MOM<U>* othermoms)
{
  bool changed = false;
  for (unsigned i=0; i<prevmoms.size(); i++) {
    const RealMom mom = RealMom(othermoms[i]);
    if (changed or prevmoms[i] != mom) {
      prevmoms[i] = mom;
      changed = true;
    }
  }
  if (changed) {
    for (int i=0; i<AMPS_LEN; i++) {
      amps[i]->setMomenta(prevmoms);
    }
  }
}

template <typename T>
template <typename U>
void NJetAccuracy<T>::setMomenta(const std::vector<MOM<U> >& othermoms)
{
  assert(othermoms.size() == prevmoms.size());
  setMomenta(othermoms.data());
}

template <typename T>
void NJetAccuracy<T>::setMuR2(const T mur2)
{
  for (int i=0; i<AMPS_LEN; i++) {
    amps[i]->setMuR2(mur2);
  }
}

template <typename T>
void NJetAccuracy<T>::setNc(const ST Nc_)
{
  for (int i=0; i<AMPS_LEN; i++) {
    amps[i]->setNc(Nc_);
  }
  renorm.setNc(Nc_);
}

template <typename T>
void NJetAccuracy<T>::setNf(const ST Nf_)
{
  for (int i=0; i<AMPS_LEN; i++) {
    amps[i]->setNf(Nf_);
  }
  renorm.setNf(Nf_);
}

template <typename T>
bool NJetAccuracy<T>::setLoopType(int type, int norderL, int norderF)
{
  bool val = true;
  for (int i=0; i<AMPS_LEN; i++) {
    val &= amps[i]->setLoopType(type, norderL, norderF);
  }
  // SLC has no renorm. or scheme contribution (unless switched to ZERO scheme later)
  if (type == NJetAmpTables::COLOR_SLC) {
    renorm.setScheme(NJetRenorm<T>::SCHEME_NONE);
  }
  return val;
}

template <typename T>
typename NJetAccuracy<T>::ST NJetAccuracy<T>::born_single()
{
  T valB = amps[0]->born();
  tree_val = get_average(valB);
  tree_err = get_error(valB);

  return tree_val;
}

template <typename T>
typename NJetAccuracy<T>::ST NJetAccuracy<T>::born()
{
  if (vector_traits<T>::isScalar) {
    T valB1 = amps[0]->born();
    T valB2 = amps[1]->born();
    tree_val = get_average(valB1, valB2);
    tree_err = get_error(valB1, valB2);
  } else {
    T valB = amps[0]->born();
    tree_val = get_average(valB);
    tree_err = get_error(valB);
  }
  return tree_val;
}

template <typename T>
typename NJetAccuracy<T>::LoopValue NJetAccuracy<T>::virt_single()
{
  EpsTriplet<T> valV = amps[0]->virt();
  T valB = amps[0]->born();
  valV = renorm.Renormalize(valB, valV);

  loop_val = get_average(valV);
  loop_err = get_error(valV);

  tree_val = get_average(valB);
  tree_err = get_error(valB);

  return loop_val;
}

template <typename T>
typename NJetAccuracy<T>::LoopValue NJetAccuracy<T>::virt()
{
  if (vector_traits<T>::isScalar) {
    EpsTriplet<T> valV1 = amps[0]->virt();
    T valB1 = amps[0]->born();
    valV1 = renorm.Renormalize(valB1, valV1);

    EpsTriplet<T> valV2 = amps[1]->virt();
    T valB2 = amps[1]->born();
    valV2 = renorm.Renormalize(valB2, valV2);

    loop_val = get_average(valV1, valV2);
    loop_err = get_error(valV1, valV2);

    tree_val = get_average(valB1, valB2);
    tree_err = get_error(valB1, valB2);
  } else {
    EpsTriplet<T> valV = amps[0]->virt();
    T valB = amps[0]->born();

    valV = renorm.Renormalize(valB, valV);
    loop_val = get_average(valV);
    loop_err = get_error(valV);

    tree_val = get_average(valB);
    tree_err = get_error(valB);
  }
  return loop_val;
}

// colour-correlated trees

template <typename T>
typename NJetAccuracy<T>::ST NJetAccuracy<T>::born_ccij_single(int i, int j)
{
  T valB = amps[0]->born_ccij(i, j);
  cctree_val = get_average(valB);
  cctree_err = get_error(valB);

  return cctree_val;
}

template <typename T>
typename NJetAccuracy<T>::ST NJetAccuracy<T>::born_ccij(int i, int j)
{
  if (vector_traits<T>::isScalar) {
    T valB1 = amps[0]->born_ccij(i, j);
    T valB2 = amps[1]->born_ccij(i, j);
    cctree_val = get_average(valB1, valB2);
    cctree_err = get_error(valB1, valB2);
  } else {
    T valB = amps[0]->born_ccij(i, j);
    cctree_val = get_average(valB);
    cctree_err = get_error(valB);
  }
  return cctree_val;
}

template <typename T>
void NJetAccuracy<T>::born_cc_single(ST* cc_arr)
{
  amps[0]->born_cc(cctree1);
  for (int i=0; i<cctree_size; i++) {
    cc_arr[i] = get_average(cctree1[i]);
  }
}

template <typename T>
void NJetAccuracy<T>::born_cc(ST* cc_arr, ST* cc_err)
{
  if (vector_traits<T>::isScalar) {
    amps[0]->born_cc(cctree1);
    amps[1]->born_cc(cctree2);
    for (int i=0; i<cctree_size; i++) {
      cc_arr[i] = get_average(cctree1[i], cctree2[i]);
      cc_err[i] = get_error(cctree1[i], cctree2[i]);
    }
  } else {
    amps[0]->born_cc(cctree1);
    for (int i=0; i<cctree_size; i++) {
      cc_arr[i] = get_average(cctree1[i]);
      cc_err[i] = get_error(cctree1[i]);
    }
  }
}

// spin-correlated FKS trees

template <typename T>
typename NJetAccuracy<T>::TreeValue NJetAccuracy<T>::born_scij_single(int i, int j)
{
  std::complex<T> valB = amps[0]->born_scij(i, j);
  sctree_val = get_average(valB);
  sctree_err = get_error(valB);

  return sctree_val;
}

template <typename T>
typename NJetAccuracy<T>::TreeValue NJetAccuracy<T>::born_scij(int i, int j)
{
  if (vector_traits<T>::isScalar) {
    std::complex<T> valB1 = amps[0]->born_scij(i, j);
    std::complex<T> valB2 = amps[1]->born_scij(i, j);
    sctree_val = get_average(valB1, valB2);
    sctree_err = get_error(valB1, valB2);
  } else {
    std::complex<T> valB = amps[0]->born_scij(i, j);
    sctree_val = get_average(valB);
    sctree_err = get_error(valB);
  }
  return sctree_val;
}

template <typename T>
void NJetAccuracy<T>::born_sc_single(TreeValue* sc_arr)
{
  amps[0]->born_sc(sctree1);
  for (int i=0; i<sctree_size; i++) {
    sc_arr[i] = get_average(sctree1[i]);
  }
}

template <typename T>
void NJetAccuracy<T>::born_sc(TreeValue* sc_arr, TreeValue* sc_err)
{
  if (vector_traits<T>::isScalar) {
    amps[0]->born_sc(sctree1);
    amps[1]->born_sc(sctree2);
    for (int i=0; i<sctree_size; i++) {
      sc_arr[i] = get_average(sctree1[i], sctree2[i]);
      sc_err[i] = get_error(sctree1[i], sctree2[i]);
    }
  } else {
    amps[0]->born_sc(sctree1);
    for (int i=0; i<sctree_size; i++) {
      sc_arr[i] = get_average(sctree1[i]);
      sc_err[i] = get_error(sctree1[i]);
    }
  }
}

// spin-correlated CS trees

template <typename T>
void NJetAccuracy<T>::born_csi_single(int i, TreeValue* cs_arr)
{
  amps[0]->born_csi(i, sctree1);
  for (int i=0; i<16; i++) {
    cs_arr[i] = get_average(sctree1[i]);
  }
}

template <typename T>
void NJetAccuracy<T>::born_csi(int i, TreeValue* cs_arr, TreeValue* cs_err)
{
  if (vector_traits<T>::isScalar) {
    amps[0]->born_csi(i, sctree1);
    amps[1]->born_csi(i, sctree2);
    for (int i=0; i<16; i++) {
      cs_arr[i] = get_average(sctree1[i], sctree2[i]);
      cs_err[i] = get_error(sctree1[i], sctree2[i]);
    }
  } else {
    amps[0]->born_csi(i, sctree1);
    for (int i=0; i<16; i++) {
      cs_arr[i] = get_average(sctree1[i]);
      cs_err[i] = get_error(sctree1[i]);
    }
  }
}

template <typename T>
void NJetAccuracy<T>::born_cs_single(TreeValue* cs_arr)
{
  amps[0]->born_cs(sctree1);
  for (int i=0; i<cstree_size; i++) {
    cs_arr[i] = get_average(sctree1[i]);
  }
}

template <typename T>
void NJetAccuracy<T>::born_cs(TreeValue* cs_arr, TreeValue* cs_err)
{
  if (vector_traits<T>::isScalar) {
    amps[0]->born_cs(sctree1);
    amps[1]->born_cs(sctree2);
    for (int i=0; i<cstree_size; i++) {
      cs_arr[i] = get_average(sctree1[i], sctree2[i]);
      cs_err[i] = get_error(sctree1[i], sctree2[i]);
    }
  } else {
    amps[0]->born_cs(sctree1);
    for (int i=0; i<cstree_size; i++) {
      cs_arr[i] = get_average(sctree1[i]);
      cs_err[i] = get_error(sctree1[i]);
    }
  }
}

#ifdef USE_SD
  template class NJetAccuracy<double>;
#endif
#ifdef USE_DD
  template class NJetAccuracy<dd_real>;
#endif
#ifdef USE_QD
  template class NJetAccuracy<qd_real>;
#endif
#ifdef USE_VC
  template class NJetAccuracy<Vc::double_v>;
  template void NJetAccuracy<Vc::double_v>::setMomenta(const MOM<double>*);
  template void NJetAccuracy<Vc::double_v>::setMomenta(const std::vector<MOM<double> >&);
#endif
