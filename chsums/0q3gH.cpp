/*
* chsums/0q3gH.cpp
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>

#include "0q3gH.h"
#include "NJetAmp-T.h"

// class Amp0q3gH

template <typename T>
Amp0q3gH<T>::Amp0q3gH(const T scalefactor,
                      const int mFC, const NJetAmpTables& tables)
  : BaseClass(mFC, tables)
{
  for (int fv=0; fv<mFC; fv++) {
    BaseClass::initNG(scalefactor, fv);
  }

  const Flavour<double> ff = StandardModel::RealFlavour(ModelBase::Higgs);
  initProcess(ff);
  initNc();
}


template <typename T>
void Amp0q3gH<T>::initProcess(const Flavour<double>& ff)
{
  for (int fv=0; fv<BaseClass::mFC; fv++) {
    Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
    flavours.push_back(ff);
    BaseClass::setProcess(flavours, fv);
  }
}

template <typename T>
void Amp0q3gH<T>::setNc(const ST Nc_)
{
  BaseClass::setNc(Nc_);
  initNc();
}

template <typename T>
void Amp0q3gH<T>::initNc()
{
  Nmat[0] = -2.;
  Nmat[1] = V - 1.;
  assert(1 < NJetAmp<T>::NmatLen);

  Nmatcc[0] = -2.;
  Nmatcc[1] = V - 1.;
  assert(1 < NJetAmp<T>::NmatccLen);

  bornFactor = V/Nc;
  loopFactor = 2.*bornFactor;
  bornccFactor = 0.5*V;
}

template <typename T>
typename Amp0q3gH<T>::TreeValue
Amp0q3gH<T>::A0(int p0, int p1, int p2)
{
  const int* O = getFperm(mfv);
  int order[] = {NN, O[p0], O[p1], O[p2]};
  return BaseClass::A0nH(order);
}

template <typename T>
void Amp0q3gH<T>::getfvpart0(const int fv, TreeValue* fvpart, TreeValue* fvpartC)
{
  fvSet(fv);
  fvpart[0] = -A0(0,1,2);
  fvpart[1] = -fvpart[0]; // A0(0,1,2);

  if (fvpartC) {
    for (int i=0; i<C0; i++) {
      fvpartC[i] = conj(fvpart[i]);
    }
  }
}

#ifdef USE_SD
  template class Amp0q3gH<double>;
#endif
#ifdef USE_DD
  template class Amp0q3gH<dd_real>;
#endif
#ifdef USE_QD
  template class Amp0q3gH<qd_real>;
#endif
#ifdef USE_VC
  template class Amp0q3gH<Vc::double_v>;
#endif
