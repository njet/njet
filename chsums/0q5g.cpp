/*
* chsums/0q5g.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>

#include "0q5g.h"
#include "../ngluon2/NGluon2.h"

// class Amp0q5g

template <typename T>
Amp0q5g<T>::Amp0q5g(const T scalefactor,
                    const int mFC, const NJetAmpTables& tables)
  : BaseClass(mFC, tables),
    Rcache(RcacheLen)
{
  for (int fv=0; fv<mFC; fv++) {
    BaseClass::initNG(scalefactor, fv);
    BaseClass::setProcess(StandardModel::NGluon1compat(NN, getFlav(fv)), fv);
  }

  initNc();
}

template <typename T>
void Amp0q5g<T>::setNc(const ST Nc_)
{
  BaseClass::setNc(Nc_);
  initNc();
}

template <typename T>
void Amp0q5g<T>::initNc()
{
  Nmat[0] = 0.;
  Nmat[1] = 2.;
  Nmat[2] = 4.;
  Nmat[3] = 8.;
  assert(3 < BaseClass::NmatLen);

  Nmatcc[0] = 0.;
  Nmatcc[1] = Nc2;
  Nmatcc[2] = 2.*Nc2;
  Nmatcc[3] = 4.*Nc2;
  Nmatcc[4] = Nc2 + 12.;
  Nmatcc[5] = -12.;
  Nmatcc[6] = 12.;
  assert(6 < BaseClass::NmatccLen);

  NmatDS[0]  = 0.;
  NmatDS[1]  = Nc2;
  NmatDS[2]  = -Nc2;
  NmatDS[3]  = 12.;
  NmatDS[4]  = -12.;
  NmatDS[5]  = 2.;
  NmatDS[6]  = -2.;
  assert(6 < BaseClass::NmatDSLen);

  bornFactor = Nc3*V;
  loopFactor = 4.*Nc*V;
  bornccFactor = Nc2*V;
}

template <typename T>
LoopResult<T> Amp0q5g<T>::AF(int p0, int p1, int p2, int p3, int p4, int pos)
{
  const int* O = getFperm(mfv);
  const int order[] = {O[p0], O[p1], O[p2], O[p3], O[p4]};
  LoopResult<T> ans = ngluons[mfv]->eval(NGluon2<T>::LIGHTQ, order, true);
  const LoopResult<T> rat = {Rcache[pos], conj(Rcache[pos])};
  ans += rat;
  return ans;
}

template <typename T>
LoopResult<T> Amp0q5g<T>::AL(int p0, int p1, int p2, int p3, int p4, int pos)
{
  const int* O = getFperm(mfv);
  const int order[] = {O[p0], O[p1], O[p2], O[p3], O[p4]};
  const LoopResult<T> ans = ngluons[mfv]->eval(NGluon2<T>::MIXED, order);
  Rcache[pos] = ngluons[mfv]->lastValue().looprat();
  return ans;
}

template <typename T>
void Amp0q5g<T>::getfvpart0(const int fv, TreeValue* fvpart, TreeValue* fvpartC)
{
  fvSet(fv);
  fvpart[0] = A0(0,1,2,3,4);
  fvpart[1] = A0(0,1,2,4,3);
  fvpart[2] = A0(0,1,3,2,4);
  fvpart[3] = A0(0,1,3,4,2);
  fvpart[4] = A0(0,1,4,2,3);
  fvpart[5] = A0(0,1,4,3,2);

  if (fvpartC) {
    for (int i=0; i<C0; i++) {
      fvpartC[i] = conj(fvpart[i]);
    }
  }
}

template <typename T>
void Amp0q5g<T>::getfvpart1ds(const int fv, LoopValue* fvpart)
{
  return getfvpart1ds_(fv, fvpart);
}

template <typename T>
void Amp0q5g<T>::getfvpart1ds(const int fv, LoopResult<T>* fvpart)
{
  return getfvpart1ds_(fv, fvpart);
}

template <typename T>
template <typename LT>
inline
void Amp0q5g<T>::getfvpart1ds_(const int fv, LT* fvpart)
{
  fvSet(fv);
  fvpart[ 0] = Nc*AL(0,1,2,3,4,  0);
  fvpart[ 1] = Nc*AL(0,1,3,4,2,  1);
  fvpart[ 2] = Nc*AL(0,1,4,2,3,  2);
  fvpart[ 3] = Nc*AL(0,2,1,3,4,  3);
  fvpart[ 4] = Nc*AL(0,2,3,1,4,  4);
  fvpart[ 5] = Nc*AL(0,2,3,4,1,  5);
  fvpart[ 6] = Nc*AL(0,3,1,4,2,  6);
  fvpart[ 7] = Nc*AL(0,3,4,1,2,  7);
  fvpart[ 8] = Nc*AL(0,3,4,2,1,  8);
  fvpart[ 9] = Nc*AL(0,4,1,2,3,  9);
  fvpart[10] = Nc*AL(0,4,2,1,3, 10);
  fvpart[11] = Nc*AL(0,4,2,3,1, 11);

  if (Nf != 0.) {
    fvpart[12] = Nf*AF(0,1,2,3,4,  0);
    fvpart[13] = Nf*AF(0,1,3,4,2,  1);
    fvpart[14] = Nf*AF(0,1,4,2,3,  2);
    fvpart[15] = Nf*AF(0,2,1,3,4,  3);
    fvpart[16] = Nf*AF(0,2,3,1,4,  4);
    fvpart[17] = Nf*AF(0,2,3,4,1,  5);
    fvpart[18] = Nf*AF(0,3,1,4,2,  6);
    fvpart[19] = Nf*AF(0,3,4,1,2,  7);
    fvpart[20] = Nf*AF(0,3,4,2,1,  8);
    fvpart[21] = Nf*AF(0,4,1,2,3,  9);
    fvpart[22] = Nf*AF(0,4,2,1,3, 10);
    fvpart[23] = Nf*AF(0,4,2,3,1, 11);
  } else {
    for (int i=12; i<24; i++) {
      fvpart[i] = LT();
    }
  }
}

// desymmetrized stuff

template <typename T>
void Amp0q5g<T>::initNc3()
{
  loopFactor = 3.*BaseClass::loopFactor;  // add n!/2 factor
}

template <typename T>
template <typename LT>
void Amp0q5g<T>::getfvpart1ds3_(const int fv, LT* fvpart)
{
  fvSet(fv);
  fvpart[0] = Nc*AL(0,1,2,3,4, 0);
  fvpart[1] = Nc*AL(0,2,1,3,4, 1);
  fvpart[2] = Nc*AL(0,2,3,1,4, 2);
  fvpart[3] = Nc*AL(0,2,3,4,1, 3);

  if (Nf != 0.) {
    fvpart[4] = Nf*AF(0,1,2,3,4, 0);
    fvpart[5] = Nf*AF(0,2,1,3,4, 1);
    fvpart[6] = Nf*AF(0,2,3,1,4, 2);
    fvpart[7] = Nf*AF(0,2,3,4,1, 3);
  } else {
    fvpart[4] = LT();
    fvpart[5] = LT();
    fvpart[6] = LT();
    fvpart[7] = LT();
  }
}

#ifdef USE_SD
  template class Amp0q5g<double>;
#endif
#ifdef USE_DD
  template class Amp0q5g<dd_real>;
#endif
#ifdef USE_QD
  template class Amp0q5g<qd_real>;
#endif
#ifdef USE_VC
  template class Amp0q5g<Vc::double_v>;
#endif

// class Amp0q5g_ds3

template <typename T>
Amp0q5g_ds3<T>::Amp0q5g_ds3(const T scalefactor,
                            const int mFC, const NJetAmpTables& tables)
  : BaseClass(scalefactor, mFC, tables)
{
  BaseClass::initNc3();
}

template <typename T>
void Amp0q5g_ds3<T>::setNc(const ST Nc_)
{
  BaseClass::setNc(Nc_);
  BaseClass::initNc3();
}

template <typename T>
void Amp0q5g_ds3<T>::getfvpart1ds(const int fv, LoopValue* fvpart)
{
  return BaseClass::getfvpart1ds3_(fv, fvpart);
}

template <typename T>
void Amp0q5g_ds3<T>::getfvpart1ds(const int fv, LoopResult<T>* fvpart)
{
  return BaseClass::getfvpart1ds3_(fv, fvpart);
}

#ifdef USE_SD
  template class Amp0q5g_ds3<double>;
#endif
#ifdef USE_DD
  template class Amp0q5g_ds3<dd_real>;
#endif
#ifdef USE_QD
  template class Amp0q5g_ds3<qd_real>;
#endif
#ifdef USE_VC
  template class Amp0q5g_ds3<Vc::double_v>;
#endif
