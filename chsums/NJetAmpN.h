/*
* chsums/NJetAmpN.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef CHSUM_NJETAMPN_H
#define CHSUM_NJETAMPN_H

#include "NJetAmp.h"

template <typename T>
class NJetAmp4 : public NJetAmp<T>
{
    typedef NJetAmp<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    NJetAmp4(const int mFC, const NJetAmpTables& tables)
      : BaseClass(mFC, tables)
    { }

  protected:
    using BaseClass::getFperm;
    using BaseClass::mfv;

    virtual TreeValue A0(int p0, int p1, int p2, int p3);
    virtual LoopResult<T> AL(int p0, int p1, int p2, int p3);
    virtual LoopResult<T> AF(int p0, int p1, int p2, int p3);
};

template <typename T>
class NJetAmp5 : public NJetAmp<T>
{
    typedef NJetAmp<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    NJetAmp5(const int mFC, const NJetAmpTables& tables)
      : BaseClass(mFC, tables)
    { }

  protected:
    using BaseClass::getFperm;
    using BaseClass::mfv;

    virtual TreeValue A0(int p0, int p1, int p2, int p3, int p4);
    virtual LoopResult<T> AL(int p0, int p1, int p2, int p3, int p4);
    virtual LoopResult<T> AF(int p0, int p1, int p2, int p3, int p4);
};

template <typename T>
class NJetAmp6 : public NJetAmp<T>
{
    typedef NJetAmp<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    NJetAmp6(const int mFC, const NJetAmpTables& tables)
      : BaseClass(mFC, tables)
    { }

  protected:
    using BaseClass::getFperm;
    using BaseClass::mfv;

    virtual TreeValue A0(int p0, int p1, int p2, int p3, int p4, int p5);
    virtual LoopResult<T> AL(int p0, int p1, int p2, int p3, int p4, int p5);
    virtual LoopResult<T> AF(int p0, int p1, int p2, int p3, int p4, int p5);
};

template <typename T>
class NJetAmp7 : public NJetAmp<T>
{
    typedef NJetAmp<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    NJetAmp7(const int mFC, const NJetAmpTables& tables)
      : BaseClass(mFC, tables)
    { }

  protected:
    using BaseClass::getFperm;
    using BaseClass::mfv;

    virtual TreeValue A0(int p0, int p1, int p2, int p3, int p4, int p5, int p6);
    virtual LoopResult<T> AL(int p0, int p1, int p2, int p3, int p4, int p5, int p6);
    virtual LoopResult<T> AF(int p0, int p1, int p2, int p3, int p4, int p5, int p6);
};


#endif // CHSUM_NJETAMPN_H