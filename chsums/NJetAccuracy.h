/*
* chsums/NJetAccuracy.h
*
* This file is part of NJet library
* Copyright (C) 2013 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef CHSUM_NJETACCURACY_H
#define CHSUM_NJETACCURACY_H

#include <vector>

#include "NJetRenorm.h"
#include "../ngluon2/Mom.h"

template <typename T>
class NJetAmp;

template <typename T>
class NJetAccuracy
{
  public:
    typedef MOM<T> RealMom;
    typedef typename vector_traits<T>::scalar ST;
    typedef EpsTriplet<ST> LoopValue;
    typedef std::complex<ST> TreeValue;

    template <typename A>
    static NJetAccuracy* create();

    template <typename A, typename FT>
    static NJetAccuracy* create(const FT& flav);

    void setMomenta(const RealMom* moms);
    void setMomenta(const std::vector<RealMom>& moms);

    template <typename U>
    void setMomenta(const MOM<U>* othermoms);
    template <typename U>
    void setMomenta(const std::vector<MOM<U> >& othermoms);

    void setMuR2(const T mur2);
    void setNc(const ST Nc_);
    void setNf(const ST Nf_);

    void setScheme(int x) { renorm.setScheme(x); }
    void setRenorm(bool x) { renorm.setRenorm(x); }

    const NJetRenorm<T>& getRenorm() const { return renorm; }

    int get_buffer_size() const { return tree_buf1.size(); }

    bool setLoopType(int type, int norderL, int norderF);

    ST born_single();
    ST born();
    ST born_value() { return tree_val; }
    ST born_error() { return tree_err; }

    LoopValue virt_single();
    LoopValue virt();
    LoopValue virt_value() { return loop_val; }
    LoopValue virt_error() { return loop_err; }

    ST born_ccij_single(int i, int j);
    ST born_ccij(int i, int j);
    ST born_ccij_value() { return cctree_val; }
    ST born_ccij_error() { return cctree_err; }

    void born_cc_single(ST* cc_arr);
    void born_cc(ST* cc_arr, ST* cc_err);

    TreeValue born_scij_single(int i, int j);
    TreeValue born_scij(int i, int j);
    TreeValue born_scij_value() { return sctree_val; }
    TreeValue born_scij_error() { return sctree_err; }

    void born_sc_single(TreeValue* sc_arr);
    void born_sc(TreeValue* sc_arr, TreeValue* sc_err);

    void born_csi_single(int i, TreeValue* cs_arr);
    void born_csi(int i, TreeValue* cs_arr, TreeValue* cs_err);
    void born_cs_single(TreeValue* cs_arr);
    void born_cs(TreeValue* cs_arr, TreeValue* cs_err);

    ~NJetAccuracy();

  protected:
    static void resize_buffers(int legs);
    static std::vector<T> tree_buf1;
    static std::vector<T> tree_buf2;
    static T* cctree1;
    static T* cctree2;
    static std::complex<T>* sctree1;
    static std::complex<T>* sctree2;

    static int get_cctree_size(int legs) { return legs*(legs-1)/2; }
    static int get_sctree_size(int legs) { return legs*legs; }
    static int get_cstree_size(int legs) { return 16*legs; }

    std::vector<RealMom> prevmoms;

    NJetAccuracy() : renorm(), amps() {}

    void initialize();

    NJetRenorm<T> renorm;

    ST tree_val;
    ST tree_err;

    LoopValue loop_val;
    LoopValue loop_err;

    ST cctree_val;
    ST cctree_err;
    int cctree_size;

    TreeValue sctree_val;
    TreeValue sctree_err;
    int sctree_size;
    int cstree_size;

    static const int AMPS_LEN = vector_traits<T>::Size > 1 ? 1 : 2;
    NJetAmp<T>* amps[AMPS_LEN];
};

template <typename T>
template <typename A>
NJetAccuracy<T>* NJetAccuracy<T>::create()
{
  typename vector_traits<T>::scalar scale_arr[4] = {
    SCALEFACTOR_A, SCALEFACTOR_B, SCALEFACTOR_C, SCALEFACTOR_D
  };

  NJetAccuracy* val = new NJetAccuracy();
  if (vector_traits<T>::isScalar) {
    for (int i=0; i<AMPS_LEN; i++) {
      val->amps[i] = new A(scale_arr[i]);
    }
  } else {
    T scale_v;
    for (int i = 0; i < vector_traits<T>::Size; i++) {
      vector_traits<T>::set(scale_v, i, scale_arr[i]);
    }
    val->amps[0] = new A(scale_v);
  }

  val->initialize();
  return val;
}

template <typename T>
template <typename A, typename FT>
NJetAccuracy<T>* NJetAccuracy<T>::create(const FT& flav)
{
  typename vector_traits<T>::scalar scale_arr[4] = {
    SCALEFACTOR_A, SCALEFACTOR_B, SCALEFACTOR_C, SCALEFACTOR_D
  };

  NJetAccuracy* val = new NJetAccuracy();
  if (vector_traits<T>::isScalar) {
    for (int i=0; i<AMPS_LEN; i++) {
      val->amps[i] = new A(flav, scale_arr[i]);
    }
  } else {
    T scale_v;
    for (int i = 0; i < vector_traits<T>::Size; i++) {
      vector_traits<T>::set(scale_v, i, scale_arr[i]);
    }
    val->amps[0] = new A(flav, scale_v);
  }

  val->initialize();
  return val;
}

#endif /* CHSUM_NJETACCURACY_H */
