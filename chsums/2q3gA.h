/*
* chsums/2q3gA.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef CHSUM_2Q3GA_H
#define CHSUM_2Q3GA_H

#include "2q3gV.h"

template <typename T>
class Amp2q3gA : public Amp2q3gZ<T>
{
    typedef Amp2q3gZ<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp2q3gA(const Flavour<double>& ff, const T scalefactor,
             const int mFC=1, const NJetAmpTables& tables=BaseClass::amptables())
    : BaseClass(ff, scalefactor, mFC, tables)
    { }

    void born_part_fill() { BaseClass::born_part_trickfill(); }
    LoopValue virt() { return BaseClass::virt_tricksum(); }
};

// One photon vector loop

class Amp2q3gAxStatic : public Amp2q3gZStatic
{
  public:
    static const int FC = 2;

    static const int flav[FC][NN];  // flavour vectors for ngluon
    static const int fvsign[FC];    // flavour channels' signs
    static const int fperm[FC][NN];        // flavour pemutation matrix
    static const int fvcol[FC][CC];        // flavour-partial pemutation matrix
};

template <typename T>
class Amp2q3gAx : public Amp2q3gA<T>
{
    typedef Amp2q3gA<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;
    typedef typename BaseClass::ST ST;

    Amp2q3gAx(const Flavour<double>& ff, const T scalefactor,
              const int mFC=2, const NJetAmpTables& tables=amptables());

    void setNf(const ST Nf_);

  protected:
    using BaseClass::NN;
    using BaseClass::Nc;
    using BaseClass::Nf;
    using BaseClass::vhel;
    using BaseClass::getFlav;
    using BaseClass::getFperm;
    using BaseClass::fvSet;
    using BaseClass::mfv;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp2q3gAxStatic>();
    }

    LoopResult<T> AFx(int p0, int p1, int p2, int p3, int p4);

    void getfvpart1_full(const int fv, LoopValue* fvpart);
    void getfvpart1_full(const int fv, LoopResult<T>* fvpart);

    template <typename LT>
    void getfvpart1_full_(const int fv, LT* fvpart);
};

// Two photons

class Amp2q3gAAStatic : public Amp2q3gStatic
{
  public:
    static const int HS = 64;
    static const int HSNN = NN+2;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp2q3gAA : public Amp2q3g<T>
{
    typedef Amp2q3g<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp2q3gAA(const Flavour<double>& Vflav, const T scalefactor,
              const int mFC=1, const NJetAmpTables& tables=amptables());

  protected:
    using BaseClass::NN;
    using BaseClass::vhel;
    using BaseClass::getFlav;
    using BaseClass::getFperm;
    using BaseClass::mfv;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp2q3gAAStatic>();
    }

    void initProcess(const Flavour<double>& ff);

    TreeValue A0(int p0, int p1, int p2, int p3, int p4);
    LoopResult<T> AL(int p0, int p1, int p2, int p3, int p4);
    LoopResult<T> AF(int p0, int p1, int p2, int p3, int p4);

    LoopResult<T> AL(int p0, int p1, int p2, int p3, int p4, int pos);
    LoopResult<T> AF(int p0, int p1, int p2, int p3, int p4, int pos, int posR);
};

#endif // CHSUM_2Q3GA_H
