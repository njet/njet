/*
* chsums/2q5gV.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef CHSUM_2Q5GV_H
#define CHSUM_2Q5GV_H

#include "2q5g.h"
#include "../ngluon2/Model.h"

class Amp2q5gVStatic : public virtual Amp2q5gStatic
{
  public:
    static const int HS = 32;
    static const int HSNN = NN+1;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp2q5gV : public Amp2q5g<T>
{
    typedef Amp2q5g<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp2q5gV(const T scalefactor,
             const int mFC=1, const NJetAmpTables& tables=amptables());
    Amp2q5gV(const Flavour<double>& Vflav, const T scalefactor,
             const int mFC=1, const NJetAmpTables& tables=amptables());

    void born_part_fill() { BaseClass::born_part_fullfill(); }
    LoopValue virt() { return BaseClass::virt_fullsum(); }

  protected:
    using BaseClass::NN;
    using BaseClass::vhel;
    using BaseClass::getFlav;
    using BaseClass::getFperm;
    using BaseClass::mfv;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp2q5gVStatic>();
    }

    void initProcess(const Flavour<double>& ff);

    TreeValue A0(int p0, int p1, int p2, int p3, int p4, int p5, int p6);
    LoopResult<T> AL(int p0, int p1, int p2, int p3, int p4, int p5, int p6);
    LoopResult<T> AF(int p0, int p1, int p2, int p3, int p4, int p5, int p6);
};

class Amp2q5gZStatic : public Amp2q5gVStatic
{
  public:
    static const int HS = 128;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp2q5gZ : public Amp2q5gV<T>
{
    typedef Amp2q5gV<T> BaseClass;
  public:

    Amp2q5gZ(const Flavour<double>& ff, const T scalefactor,
             const int mFC=1, const NJetAmpTables& tables=amptables())
    : BaseClass(ff, scalefactor, mFC, tables)
    { }

  protected:
    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp2q5gZStatic>();
    }
};

#endif // CHSUM_2Q5GV_H
