/*
* chsums/2q3gA-ds3.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012, 2013 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef CHSUM_2Q3GA_DS3_H
#define CHSUM_2Q3GA_DS3_H

#include "2q3gV-ds.h"

template <typename T>
class Amp2q3gA_ds3 : public Amp2q3gZ_ds3<T>
{
    typedef Amp2q3gZ_ds3<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp2q3gA_ds3(const Flavour<double>& ff, const T scalefactor,
                 const int mFC=1, const NJetAmpTables& tables=BaseClass::amptables())
    : BaseClass(ff, scalefactor, mFC, tables)
    { }

    void born_part_fill() { BaseClass::born_part_trickfill(); }
    LoopValue virt() { return BaseClass::virt_dstricksum(); }
};

#endif // CHSUM_2Q3GA_DS3_H
