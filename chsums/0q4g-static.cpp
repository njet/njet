/*
* chsums/0q4g-static.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>

#include "0q4g.h"

// class Amp0q4gStatic

const int
Amp0q4gStatic::flav[FC][NN] = {
  {0,0,0,0}
};

const int
Amp0q4gStatic::fvsign[FC] = {1};

const int
Amp0q4gStatic::ccsign[NN*(NN+1)/2] = {1,1,1,1,1,1,1,1,1,1};

const int
Amp0q4gStatic::fperm[FC][NN] = {
  {0,1,2,3}
};

const int
Amp0q4gStatic::fvcol[FC][C0] = {
  {0,1}
};

const unsigned char
Amp0q4gStatic::colmat[C0*(C0+1)/2] = {
  1,0,1
};

const unsigned char
Amp0q4gStatic::colmatcc[NN*(NN+1)/2][C0*(C0+1)/2] = {
  {},
  {1,0,1},
  {},
  {1,1,2},
  {2,1,1},
  {},
  {2,1,1},
  {1,1,2},
  {1,0,1},
  {},
};

const unsigned int Amp0q4gStatic::colmatds[CDS][C0] = {
  // L
  {1, 0},
  {2, 2},
  {0, 1},
  // F
  {2, 0},
  {1, 1},
  {0, 2},
};

const int
Amp0q4gStatic::HSarr[HS][HSNN] = {
//   { 1, 1, 1, 1},
//   { 1, 1, 1,-1},
//   { 1, 1,-1, 1},
  { 1, 1,-1,-1},
//   {-1, 1, 1, 1},
  {-1, 1, 1,-1},
  {-1, 1,-1, 1},
//   {-1, 1,-1,-1},
//   {-1,-1,-1,-1},
//   {-1,-1,-1, 1},
//   {-1,-1, 1,-1},
  {-1,-1, 1, 1},
//   { 1,-1,-1,-1},
  { 1,-1,-1, 1},
  { 1,-1, 1,-1},
//   { 1,-1, 1, 1},
};
