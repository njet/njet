/*
* chsums/0q7g.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>

#include "0q7g.h"
#include "../ngluon2/NGluon2.h"

// class Amp0q7g

template <typename T>
Amp0q7g<T>::Amp0q7g(const T scalefactor,
                    const int mFC, const NJetAmpTables& tables)
  : BaseClass(mFC, tables),
    Rcache(RcacheLen)
{
  for (int fv=0; fv<mFC; fv++) {
    BaseClass::initNG(scalefactor, fv);
    BaseClass::setProcess(StandardModel::NGluon1compat(NN, getFlav(fv)), fv);
  }

  initNc();
}

template <typename T>
void Amp0q7g<T>::setNc(const ST Nc_)
{
  BaseClass::setNc(Nc_);
  initNc();
}

template <typename T>
void Amp0q7g<T>::initNc()
{
  Nmat[ 0] = 0.;
  Nmat[ 1] = 2.;
  Nmat[ 2] = 4.;
  Nmat[ 3] = 8.;
  Nmat[ 4] = 16.;
  Nmat[ 5] = 32.;
  Nmat[ 6] = -8./Nc2;
  Nmat[ 7] = 8./Nc2;
  Nmat[ 8] = 16./Nc2;
  Nmat[ 9] = 24./Nc2;
  Nmat[10] = -32./Nc2;
  Nmat[11] = 32./Nc2;
  Nmat[12] = 48./Nc2;
  Nmat[13] = 2. + 24./Nc2;
  Nmat[14] = 2. + 32./Nc2;
  Nmat[15] = 4. + 48./Nc2;
  assert(15 < BaseClass::NmatLen);

  Nmatcc[ 0] = 0.;
  Nmatcc[ 1] = Nc2;
  Nmatcc[ 2] = 2.*Nc2;
  Nmatcc[ 3] = 4.*Nc2;
  Nmatcc[ 4] = 8.*Nc2;
  Nmatcc[ 5] = 16.*Nc2;
  Nmatcc[ 6] = -4.;
  Nmatcc[ 7] = 4;
  Nmatcc[ 8] = -8.;
  Nmatcc[ 9] = 8;
  Nmatcc[10] = -12.;
  Nmatcc[11] = 12;
  Nmatcc[12] = -16.;
  Nmatcc[13] = 16.;
  Nmatcc[14] = -24.;
  Nmatcc[15] = 24.;
  Nmatcc[16] = -32.;
  Nmatcc[17] = 32.;
  Nmatcc[18] = -48.;
  Nmatcc[19] = 48.;
  Nmatcc[20] = Nc2 + 12.;
  Nmatcc[21] = Nc2 + 16.;
  Nmatcc[22] = 2.*Nc2 + 24.;
  Nmatcc[23] = 2.*Nc2 + 32.;
  Nmatcc[24] = 4.*Nc2 + 48.;
  Nmatcc[25] = -(2. + 24./Nc2);
  Nmatcc[26] =  (2. + 24./Nc2);
  Nmatcc[27] = -(2. - 24./Nc2);
  Nmatcc[28] =  (2. - 24./Nc2);
  Nmatcc[29] = -(6. + 24./Nc2);
  Nmatcc[30] =  (6. + 24./Nc2);
  Nmatcc[31] = -(10. + 24./Nc2);
  Nmatcc[32] =  (10. + 24./Nc2);
  Nmatcc[33] = -(12. + 48./Nc2);
  Nmatcc[34] =  (12. + 48./Nc2);
  Nmatcc[35] = -(14. + 24./Nc2);
  Nmatcc[36] =  (14. + 24./Nc2);
  Nmatcc[37] = -(16. + 48./Nc2);
  Nmatcc[38] =  (16. + 48./Nc2);
  Nmatcc[39] =  (20. + 48./Nc2);
  Nmatcc[40] = -(32. + 48./Nc2);
  Nmatcc[41] =  (32. + 48./Nc2);
  Nmatcc[42] = Nc2 + 14. - 24./Nc2;
  Nmatcc[43] = Nc2 + 32. + 48./Nc2;
  assert(43 < BaseClass::NmatccLen);

  NmatDS[ 0] = -2.;
  NmatDS[ 1] = 0.;
  NmatDS[ 2] = 2.;
  NmatDS[ 3] = -48. - 32.*Nc2;
  NmatDS[ 4] = -48. - 16.*Nc2;
  NmatDS[ 5] = -24. - 14.*Nc2;
  NmatDS[ 6] = -48. - 12.*Nc2;
  NmatDS[ 7] = -24. - 6.*Nc2;
  NmatDS[ 8] = 2. - 3.*Nc2;
  NmatDS[ 9] = -24. - 2.*Nc2;
  NmatDS[10] = -2. - 2.*Nc2;
  NmatDS[11] = 2. - 2.*Nc2;
  NmatDS[12] = 24. - 2.*Nc2;
  NmatDS[13] = -2. - Nc2;
  NmatDS[14] = 2. - Nc2;
  NmatDS[15] = -2. + Nc2;
  NmatDS[16] = -16.*Nc2;
  NmatDS[17] = -12.*Nc2;
  NmatDS[18] = -8.*Nc2;
  NmatDS[19] = -4.*Nc2;
  NmatDS[20] = -3.*Nc2;
  NmatDS[21] = -2.*Nc2;
  NmatDS[22] = -Nc2;
  NmatDS[23] = Nc2;
  NmatDS[24] = 2.*Nc2;
  NmatDS[25] = 3.*Nc2;
  NmatDS[26] = 4.*Nc2;
  NmatDS[27] = 8.*Nc2;
  NmatDS[28] = 12.*Nc2;
  NmatDS[29] = 16.*Nc2;
  NmatDS[30] = 2. + Nc2;
  NmatDS[31] = -24. + 2.*Nc2;
  NmatDS[32] = -2. + 2.*Nc2;
  NmatDS[33] = 2. + 2.*Nc2;
  NmatDS[34] = 24. + 2.*Nc2;
  NmatDS[35] = -2. + 3.*Nc2;
  NmatDS[36] = 24. + 6.*Nc2;
  NmatDS[37] = 48. + 12.*Nc2;
  NmatDS[38] = 24. + 14.*Nc2;
  NmatDS[39] = 48. + 16.*Nc2;
  NmatDS[40] = 48. + 32.*Nc2;
  NmatDS[41] = -Nc4;
  NmatDS[42] = Nc4;
  assert(42 < BaseClass::NmatDSLen);

  bornFactor = Nc*Nc4*V;
  loopFactor = 4.*Nc*V;
  bornccFactor = Nc4*V;
}

template <typename T>
LoopResult<T> Amp0q7g<T>::AF(int p0, int p1, int p2, int p3, int p4, int p5, int p6, int pos)
{
  const int* O = getFperm(mfv);
  const int order[] = {O[p0], O[p1], O[p2], O[p3], O[p4], O[p5], O[p6]};
  LoopResult<T> ans = ngluons[mfv]->eval(NGluon2<T>::LIGHTQ, order, true);
  const LoopResult<T> rat = {Rcache[pos], conj(Rcache[pos])};
  ans += rat;
  return ans;
}

template <typename T>
LoopResult<T> Amp0q7g<T>::AL(int p0, int p1, int p2, int p3, int p4, int p5, int p6, int pos)
{
  const int* O = getFperm(mfv);
  const int order[] = {O[p0], O[p1], O[p2], O[p3], O[p4], O[p5], O[p6]};
  const LoopResult<T> ans = ngluons[mfv]->eval(NGluon2<T>::MIXED, order);
  Rcache[pos] = ngluons[mfv]->lastValue().looprat();
  return ans;
}

template <typename T>
void Amp0q7g<T>::getfvpart0(const int fv, TreeValue* fvpart, TreeValue* fvpartC)
{
  fvSet(fv);
  fvpart[  0] = A0(0,1,2,3,4,5,6);
  fvpart[  1] = A0(0,1,2,3,4,6,5);
  fvpart[  2] = A0(0,1,2,3,5,4,6);
  fvpart[  3] = A0(0,1,2,3,5,6,4);
  fvpart[  4] = A0(0,1,2,3,6,4,5);
  fvpart[  5] = A0(0,1,2,3,6,5,4);
  fvpart[  6] = A0(0,1,2,4,3,5,6);
  fvpart[  7] = A0(0,1,2,4,3,6,5);
  fvpart[  8] = A0(0,1,2,4,5,3,6);
  fvpart[  9] = A0(0,1,2,4,5,6,3);
  fvpart[ 10] = A0(0,1,2,4,6,3,5);
  fvpart[ 11] = A0(0,1,2,4,6,5,3);
  fvpart[ 12] = A0(0,1,2,5,3,4,6);
  fvpart[ 13] = A0(0,1,2,5,3,6,4);
  fvpart[ 14] = A0(0,1,2,5,4,3,6);
  fvpart[ 15] = A0(0,1,2,5,4,6,3);
  fvpart[ 16] = A0(0,1,2,5,6,3,4);
  fvpart[ 17] = A0(0,1,2,5,6,4,3);
  fvpart[ 18] = A0(0,1,2,6,3,4,5);
  fvpart[ 19] = A0(0,1,2,6,3,5,4);
  fvpart[ 20] = A0(0,1,2,6,4,3,5);
  fvpart[ 21] = A0(0,1,2,6,4,5,3);
  fvpart[ 22] = A0(0,1,2,6,5,3,4);
  fvpart[ 23] = A0(0,1,2,6,5,4,3);
  fvpart[ 24] = A0(0,1,3,2,4,5,6);
  fvpart[ 25] = A0(0,1,3,2,4,6,5);
  fvpart[ 26] = A0(0,1,3,2,5,4,6);
  fvpart[ 27] = A0(0,1,3,2,5,6,4);
  fvpart[ 28] = A0(0,1,3,2,6,4,5);
  fvpart[ 29] = A0(0,1,3,2,6,5,4);
  fvpart[ 30] = A0(0,1,3,4,2,5,6);
  fvpart[ 31] = A0(0,1,3,4,2,6,5);
  fvpart[ 32] = A0(0,1,3,4,5,2,6);
  fvpart[ 33] = A0(0,1,3,4,5,6,2);
  fvpart[ 34] = A0(0,1,3,4,6,2,5);
  fvpart[ 35] = A0(0,1,3,4,6,5,2);
  fvpart[ 36] = A0(0,1,3,5,2,4,6);
  fvpart[ 37] = A0(0,1,3,5,2,6,4);
  fvpart[ 38] = A0(0,1,3,5,4,2,6);
  fvpart[ 39] = A0(0,1,3,5,4,6,2);
  fvpart[ 40] = A0(0,1,3,5,6,2,4);
  fvpart[ 41] = A0(0,1,3,5,6,4,2);
  fvpart[ 42] = A0(0,1,3,6,2,4,5);
  fvpart[ 43] = A0(0,1,3,6,2,5,4);
  fvpart[ 44] = A0(0,1,3,6,4,2,5);
  fvpart[ 45] = A0(0,1,3,6,4,5,2);
  fvpart[ 46] = A0(0,1,3,6,5,2,4);
  fvpart[ 47] = A0(0,1,3,6,5,4,2);
  fvpart[ 48] = A0(0,1,4,2,3,5,6);
  fvpart[ 49] = A0(0,1,4,2,3,6,5);
  fvpart[ 50] = A0(0,1,4,2,5,3,6);
  fvpart[ 51] = A0(0,1,4,2,5,6,3);
  fvpart[ 52] = A0(0,1,4,2,6,3,5);
  fvpart[ 53] = A0(0,1,4,2,6,5,3);
  fvpart[ 54] = A0(0,1,4,3,2,5,6);
  fvpart[ 55] = A0(0,1,4,3,2,6,5);
  fvpart[ 56] = A0(0,1,4,3,5,2,6);
  fvpart[ 57] = A0(0,1,4,3,5,6,2);
  fvpart[ 58] = A0(0,1,4,3,6,2,5);
  fvpart[ 59] = A0(0,1,4,3,6,5,2);
  fvpart[ 60] = A0(0,1,4,5,2,3,6);
  fvpart[ 61] = A0(0,1,4,5,2,6,3);
  fvpart[ 62] = A0(0,1,4,5,3,2,6);
  fvpart[ 63] = A0(0,1,4,5,3,6,2);
  fvpart[ 64] = A0(0,1,4,5,6,2,3);
  fvpart[ 65] = A0(0,1,4,5,6,3,2);
  fvpart[ 66] = A0(0,1,4,6,2,3,5);
  fvpart[ 67] = A0(0,1,4,6,2,5,3);
  fvpart[ 68] = A0(0,1,4,6,3,2,5);
  fvpart[ 69] = A0(0,1,4,6,3,5,2);
  fvpart[ 70] = A0(0,1,4,6,5,2,3);
  fvpart[ 71] = A0(0,1,4,6,5,3,2);
  fvpart[ 72] = A0(0,1,5,2,3,4,6);
  fvpart[ 73] = A0(0,1,5,2,3,6,4);
  fvpart[ 74] = A0(0,1,5,2,4,3,6);
  fvpart[ 75] = A0(0,1,5,2,4,6,3);
  fvpart[ 76] = A0(0,1,5,2,6,3,4);
  fvpart[ 77] = A0(0,1,5,2,6,4,3);
  fvpart[ 78] = A0(0,1,5,3,2,4,6);
  fvpart[ 79] = A0(0,1,5,3,2,6,4);
  fvpart[ 80] = A0(0,1,5,3,4,2,6);
  fvpart[ 81] = A0(0,1,5,3,4,6,2);
  fvpart[ 82] = A0(0,1,5,3,6,2,4);
  fvpart[ 83] = A0(0,1,5,3,6,4,2);
  fvpart[ 84] = A0(0,1,5,4,2,3,6);
  fvpart[ 85] = A0(0,1,5,4,2,6,3);
  fvpart[ 86] = A0(0,1,5,4,3,2,6);
  fvpart[ 87] = A0(0,1,5,4,3,6,2);
  fvpart[ 88] = A0(0,1,5,4,6,2,3);
  fvpart[ 89] = A0(0,1,5,4,6,3,2);
  fvpart[ 90] = A0(0,1,5,6,2,3,4);
  fvpart[ 91] = A0(0,1,5,6,2,4,3);
  fvpart[ 92] = A0(0,1,5,6,3,2,4);
  fvpart[ 93] = A0(0,1,5,6,3,4,2);
  fvpart[ 94] = A0(0,1,5,6,4,2,3);
  fvpart[ 95] = A0(0,1,5,6,4,3,2);
  fvpart[ 96] = A0(0,1,6,2,3,4,5);
  fvpart[ 97] = A0(0,1,6,2,3,5,4);
  fvpart[ 98] = A0(0,1,6,2,4,3,5);
  fvpart[ 99] = A0(0,1,6,2,4,5,3);
  fvpart[100] = A0(0,1,6,2,5,3,4);
  fvpart[101] = A0(0,1,6,2,5,4,3);
  fvpart[102] = A0(0,1,6,3,2,4,5);
  fvpart[103] = A0(0,1,6,3,2,5,4);
  fvpart[104] = A0(0,1,6,3,4,2,5);
  fvpart[105] = A0(0,1,6,3,4,5,2);
  fvpart[106] = A0(0,1,6,3,5,2,4);
  fvpart[107] = A0(0,1,6,3,5,4,2);
  fvpart[108] = A0(0,1,6,4,2,3,5);
  fvpart[109] = A0(0,1,6,4,2,5,3);
  fvpart[110] = A0(0,1,6,4,3,2,5);
  fvpart[111] = A0(0,1,6,4,3,5,2);
  fvpart[112] = A0(0,1,6,4,5,2,3);
  fvpart[113] = A0(0,1,6,4,5,3,2);
  fvpart[114] = A0(0,1,6,5,2,3,4);
  fvpart[115] = A0(0,1,6,5,2,4,3);
  fvpart[116] = A0(0,1,6,5,3,2,4);
  fvpart[117] = A0(0,1,6,5,3,4,2);
  fvpart[118] = A0(0,1,6,5,4,2,3);
  fvpart[119] = A0(0,1,6,5,4,3,2);

  if (fvpartC) {
    for (int i=0; i<C0; i++) {
      fvpartC[i] = conj(fvpart[i]);
    }
  }
}

template <typename T>
void Amp0q7g<T>::getfvpart1ds(const int fv, LoopValue* fvpart)
{
  return getfvpart1ds_(fv, fvpart);
}

template <typename T>
void Amp0q7g<T>::getfvpart1ds(const int fv, LoopResult<T>* fvpart)
{
  return getfvpart1ds_(fv, fvpart);
}

template <typename T>
template <typename LT>
inline
void Amp0q7g<T>::getfvpart1ds_(const int fv, LT* fvpart)
{
  fvSet(fv);
  fvpart[  0] = AL(0,1,2,3,4,5,6);
  fvpart[  1] = AL(0,1,2,3,5,6,4);
  fvpart[  2] = AL(0,1,2,3,6,4,5);
  fvpart[  3] = AL(0,1,2,4,3,5,6);
  fvpart[  4] = AL(0,1,2,4,5,3,6);
  fvpart[  5] = AL(0,1,2,4,5,6,3);
  fvpart[  6] = AL(0,1,2,5,3,6,4);
  fvpart[  7] = AL(0,1,2,5,6,3,4);
  fvpart[  8] = AL(0,1,2,5,6,4,3);
  fvpart[  9] = AL(0,1,2,6,3,4,5);
  fvpart[ 10] = AL(0,1,2,6,4,3,5);
  fvpart[ 11] = AL(0,1,2,6,4,5,3);
  fvpart[ 12] = AL(0,1,3,2,4,5,6);
  fvpart[ 13] = AL(0,1,3,2,5,6,4);
  fvpart[ 14] = AL(0,1,3,2,6,4,5);
  fvpart[ 15] = AL(0,1,3,4,2,5,6);
  fvpart[ 16] = AL(0,1,3,4,5,2,6);
  fvpart[ 17] = AL(0,1,3,4,5,6,2);
  fvpart[ 18] = AL(0,1,3,5,2,6,4);
  fvpart[ 19] = AL(0,1,3,5,6,2,4);
  fvpart[ 20] = AL(0,1,3,5,6,4,2);
  fvpart[ 21] = AL(0,1,3,6,2,4,5);
  fvpart[ 22] = AL(0,1,3,6,4,2,5);
  fvpart[ 23] = AL(0,1,3,6,4,5,2);
  fvpart[ 24] = AL(0,1,4,2,3,5,6);
  fvpart[ 25] = AL(0,1,4,2,5,3,6);
  fvpart[ 26] = AL(0,1,4,2,5,6,3);
  fvpart[ 27] = AL(0,1,4,3,2,5,6);
  fvpart[ 28] = AL(0,1,4,3,5,2,6);
  fvpart[ 29] = AL(0,1,4,3,5,6,2);
  fvpart[ 30] = AL(0,1,4,5,2,3,6);
  fvpart[ 31] = AL(0,1,4,5,2,6,3);
  fvpart[ 32] = AL(0,1,4,5,3,2,6);
  fvpart[ 33] = AL(0,1,4,5,3,6,2);
  fvpart[ 34] = AL(0,1,4,5,6,2,3);
  fvpart[ 35] = AL(0,1,4,5,6,3,2);
  fvpart[ 36] = AL(0,1,5,2,3,6,4);
  fvpart[ 37] = AL(0,1,5,2,6,3,4);
  fvpart[ 38] = AL(0,1,5,2,6,4,3);
  fvpart[ 39] = AL(0,1,5,3,2,6,4);
  fvpart[ 40] = AL(0,1,5,3,6,2,4);
  fvpart[ 41] = AL(0,1,5,3,6,4,2);
  fvpart[ 42] = AL(0,1,5,6,2,3,4);
  fvpart[ 43] = AL(0,1,5,6,2,4,3);
  fvpart[ 44] = AL(0,1,5,6,3,2,4);
  fvpart[ 45] = AL(0,1,5,6,3,4,2);
  fvpart[ 46] = AL(0,1,5,6,4,2,3);
  fvpart[ 47] = AL(0,1,5,6,4,3,2);
  fvpart[ 48] = AL(0,1,6,2,3,4,5);
  fvpart[ 49] = AL(0,1,6,2,4,3,5);
  fvpart[ 50] = AL(0,1,6,2,4,5,3);
  fvpart[ 51] = AL(0,1,6,3,2,4,5);
  fvpart[ 52] = AL(0,1,6,3,4,2,5);
  fvpart[ 53] = AL(0,1,6,3,4,5,2);
  fvpart[ 54] = AL(0,1,6,4,2,3,5);
  fvpart[ 55] = AL(0,1,6,4,2,5,3);
  fvpart[ 56] = AL(0,1,6,4,3,2,5);
  fvpart[ 57] = AL(0,1,6,4,3,5,2);
  fvpart[ 58] = AL(0,1,6,4,5,2,3);
  fvpart[ 59] = AL(0,1,6,4,5,3,2);
  fvpart[ 60] = AL(0,2,1,3,4,5,6);
  fvpart[ 61] = AL(0,2,1,3,5,6,4);
  fvpart[ 62] = AL(0,2,1,3,6,4,5);
  fvpart[ 63] = AL(0,2,1,4,3,5,6);
  fvpart[ 64] = AL(0,2,1,4,5,3,6);
  fvpart[ 65] = AL(0,2,1,4,5,6,3);
  fvpart[ 66] = AL(0,2,1,5,3,6,4);
  fvpart[ 67] = AL(0,2,1,5,6,3,4);
  fvpart[ 68] = AL(0,2,1,5,6,4,3);
  fvpart[ 69] = AL(0,2,1,6,3,4,5);
  fvpart[ 70] = AL(0,2,1,6,4,3,5);
  fvpart[ 71] = AL(0,2,1,6,4,5,3);
  fvpart[ 72] = AL(0,2,3,1,4,5,6);
  fvpart[ 73] = AL(0,2,3,1,5,6,4);
  fvpart[ 74] = AL(0,2,3,1,6,4,5);
  fvpart[ 75] = AL(0,2,3,4,1,5,6);
  fvpart[ 76] = AL(0,2,3,4,5,1,6);
  fvpart[ 77] = AL(0,2,3,4,5,6,1);
  fvpart[ 78] = AL(0,2,3,5,1,6,4);
  fvpart[ 79] = AL(0,2,3,5,6,1,4);
  fvpart[ 80] = AL(0,2,3,5,6,4,1);
  fvpart[ 81] = AL(0,2,3,6,1,4,5);
  fvpart[ 82] = AL(0,2,3,6,4,1,5);
  fvpart[ 83] = AL(0,2,3,6,4,5,1);
  fvpart[ 84] = AL(0,2,4,1,3,5,6);
  fvpart[ 85] = AL(0,2,4,1,5,3,6);
  fvpart[ 86] = AL(0,2,4,1,5,6,3);
  fvpart[ 87] = AL(0,2,4,3,1,5,6);
  fvpart[ 88] = AL(0,2,4,3,5,1,6);
  fvpart[ 89] = AL(0,2,4,3,5,6,1);
  fvpart[ 90] = AL(0,2,4,5,1,3,6);
  fvpart[ 91] = AL(0,2,4,5,1,6,3);
  fvpart[ 92] = AL(0,2,4,5,3,1,6);
  fvpart[ 93] = AL(0,2,4,5,3,6,1);
  fvpart[ 94] = AL(0,2,4,5,6,1,3);
  fvpart[ 95] = AL(0,2,4,5,6,3,1);
  fvpart[ 96] = AL(0,2,5,1,3,6,4);
  fvpart[ 97] = AL(0,2,5,1,6,3,4);
  fvpart[ 98] = AL(0,2,5,1,6,4,3);
  fvpart[ 99] = AL(0,2,5,3,1,6,4);
  fvpart[100] = AL(0,2,5,3,6,1,4);
  fvpart[101] = AL(0,2,5,3,6,4,1);
  fvpart[102] = AL(0,2,5,6,1,3,4);
  fvpart[103] = AL(0,2,5,6,1,4,3);
  fvpart[104] = AL(0,2,5,6,3,1,4);
  fvpart[105] = AL(0,2,5,6,3,4,1);
  fvpart[106] = AL(0,2,5,6,4,1,3);
  fvpart[107] = AL(0,2,5,6,4,3,1);
  fvpart[108] = AL(0,2,6,1,3,4,5);
  fvpart[109] = AL(0,2,6,1,4,3,5);
  fvpart[110] = AL(0,2,6,1,4,5,3);
  fvpart[111] = AL(0,2,6,3,1,4,5);
  fvpart[112] = AL(0,2,6,3,4,1,5);
  fvpart[113] = AL(0,2,6,3,4,5,1);
  fvpart[114] = AL(0,2,6,4,1,3,5);
  fvpart[115] = AL(0,2,6,4,1,5,3);
  fvpart[116] = AL(0,2,6,4,3,1,5);
  fvpart[117] = AL(0,2,6,4,3,5,1);
  fvpart[118] = AL(0,2,6,4,5,1,3);
  fvpart[119] = AL(0,2,6,4,5,3,1);
  fvpart[120] = AL(0,3,1,2,4,5,6);
  fvpart[121] = AL(0,3,1,2,5,6,4);
  fvpart[122] = AL(0,3,1,2,6,4,5);
  fvpart[123] = AL(0,3,1,4,2,5,6);
  fvpart[124] = AL(0,3,1,4,5,2,6);
  fvpart[125] = AL(0,3,1,4,5,6,2);
  fvpart[126] = AL(0,3,1,5,2,6,4);
  fvpart[127] = AL(0,3,1,5,6,2,4);
  fvpart[128] = AL(0,3,1,5,6,4,2);
  fvpart[129] = AL(0,3,1,6,2,4,5);
  fvpart[130] = AL(0,3,1,6,4,2,5);
  fvpart[131] = AL(0,3,1,6,4,5,2);
  fvpart[132] = AL(0,3,2,1,4,5,6);
  fvpart[133] = AL(0,3,2,1,5,6,4);
  fvpart[134] = AL(0,3,2,1,6,4,5);
  fvpart[135] = AL(0,3,2,4,1,5,6);
  fvpart[136] = AL(0,3,2,4,5,1,6);
  fvpart[137] = AL(0,3,2,4,5,6,1);
  fvpart[138] = AL(0,3,2,5,1,6,4);
  fvpart[139] = AL(0,3,2,5,6,1,4);
  fvpart[140] = AL(0,3,2,5,6,4,1);
  fvpart[141] = AL(0,3,2,6,1,4,5);
  fvpart[142] = AL(0,3,2,6,4,1,5);
  fvpart[143] = AL(0,3,2,6,4,5,1);
  fvpart[144] = AL(0,3,4,1,2,5,6);
  fvpart[145] = AL(0,3,4,1,5,2,6);
  fvpart[146] = AL(0,3,4,1,5,6,2);
  fvpart[147] = AL(0,3,4,2,1,5,6);
  fvpart[148] = AL(0,3,4,2,5,1,6);
  fvpart[149] = AL(0,3,4,2,5,6,1);
  fvpart[150] = AL(0,3,4,5,1,2,6);
  fvpart[151] = AL(0,3,4,5,1,6,2);
  fvpart[152] = AL(0,3,4,5,2,1,6);
  fvpart[153] = AL(0,3,4,5,2,6,1);
  fvpart[154] = AL(0,3,4,5,6,1,2);
  fvpart[155] = AL(0,3,4,5,6,2,1);
  fvpart[156] = AL(0,3,5,1,2,6,4);
  fvpart[157] = AL(0,3,5,1,6,2,4);
  fvpart[158] = AL(0,3,5,1,6,4,2);
  fvpart[159] = AL(0,3,5,2,1,6,4);
  fvpart[160] = AL(0,3,5,2,6,1,4);
  fvpart[161] = AL(0,3,5,2,6,4,1);
  fvpart[162] = AL(0,3,5,6,1,2,4);
  fvpart[163] = AL(0,3,5,6,1,4,2);
  fvpart[164] = AL(0,3,5,6,2,1,4);
  fvpart[165] = AL(0,3,5,6,2,4,1);
  fvpart[166] = AL(0,3,5,6,4,1,2);
  fvpart[167] = AL(0,3,5,6,4,2,1);
  fvpart[168] = AL(0,3,6,1,2,4,5);
  fvpart[169] = AL(0,3,6,1,4,2,5);
  fvpart[170] = AL(0,3,6,1,4,5,2);
  fvpart[171] = AL(0,3,6,2,1,4,5);
  fvpart[172] = AL(0,3,6,2,4,1,5);
  fvpart[173] = AL(0,3,6,2,4,5,1);
  fvpart[174] = AL(0,3,6,4,1,2,5);
  fvpart[175] = AL(0,3,6,4,1,5,2);
  fvpart[176] = AL(0,3,6,4,2,1,5);
  fvpart[177] = AL(0,3,6,4,2,5,1);
  fvpart[178] = AL(0,3,6,4,5,1,2);
  fvpart[179] = AL(0,3,6,4,5,2,1);
  fvpart[180] = AL(0,4,1,2,3,5,6);
  fvpart[181] = AL(0,4,1,2,5,3,6);
  fvpart[182] = AL(0,4,1,2,5,6,3);
  fvpart[183] = AL(0,4,1,3,2,5,6);
  fvpart[184] = AL(0,4,1,3,5,2,6);
  fvpart[185] = AL(0,4,1,3,5,6,2);
  fvpart[186] = AL(0,4,1,5,2,3,6);
  fvpart[187] = AL(0,4,1,5,2,6,3);
  fvpart[188] = AL(0,4,1,5,3,2,6);
  fvpart[189] = AL(0,4,1,5,3,6,2);
  fvpart[190] = AL(0,4,1,5,6,2,3);
  fvpart[191] = AL(0,4,1,5,6,3,2);
  fvpart[192] = AL(0,4,2,1,3,5,6);
  fvpart[193] = AL(0,4,2,1,5,3,6);
  fvpart[194] = AL(0,4,2,1,5,6,3);
  fvpart[195] = AL(0,4,2,3,1,5,6);
  fvpart[196] = AL(0,4,2,3,5,1,6);
  fvpart[197] = AL(0,4,2,3,5,6,1);
  fvpart[198] = AL(0,4,2,5,1,3,6);
  fvpart[199] = AL(0,4,2,5,1,6,3);
  fvpart[200] = AL(0,4,2,5,3,1,6);
  fvpart[201] = AL(0,4,2,5,3,6,1);
  fvpart[202] = AL(0,4,2,5,6,1,3);
  fvpart[203] = AL(0,4,2,5,6,3,1);
  fvpart[204] = AL(0,4,3,1,2,5,6);
  fvpart[205] = AL(0,4,3,1,5,2,6);
  fvpart[206] = AL(0,4,3,1,5,6,2);
  fvpart[207] = AL(0,4,3,2,1,5,6);
  fvpart[208] = AL(0,4,3,2,5,1,6);
  fvpart[209] = AL(0,4,3,2,5,6,1);
  fvpart[210] = AL(0,4,3,5,1,2,6);
  fvpart[211] = AL(0,4,3,5,1,6,2);
  fvpart[212] = AL(0,4,3,5,2,1,6);
  fvpart[213] = AL(0,4,3,5,2,6,1);
  fvpart[214] = AL(0,4,3,5,6,1,2);
  fvpart[215] = AL(0,4,3,5,6,2,1);
  fvpart[216] = AL(0,4,5,1,2,3,6);
  fvpart[217] = AL(0,4,5,1,2,6,3);
  fvpart[218] = AL(0,4,5,1,3,2,6);
  fvpart[219] = AL(0,4,5,1,3,6,2);
  fvpart[220] = AL(0,4,5,1,6,2,3);
  fvpart[221] = AL(0,4,5,1,6,3,2);
  fvpart[222] = AL(0,4,5,2,1,3,6);
  fvpart[223] = AL(0,4,5,2,1,6,3);
  fvpart[224] = AL(0,4,5,2,3,1,6);
  fvpart[225] = AL(0,4,5,2,3,6,1);
  fvpart[226] = AL(0,4,5,2,6,1,3);
  fvpart[227] = AL(0,4,5,2,6,3,1);
  fvpart[228] = AL(0,4,5,3,1,2,6);
  fvpart[229] = AL(0,4,5,3,1,6,2);
  fvpart[230] = AL(0,4,5,3,2,1,6);
  fvpart[231] = AL(0,4,5,3,2,6,1);
  fvpart[232] = AL(0,4,5,3,6,1,2);
  fvpart[233] = AL(0,4,5,3,6,2,1);
  fvpart[234] = AL(0,4,5,6,1,2,3);
  fvpart[235] = AL(0,4,5,6,1,3,2);
  fvpart[236] = AL(0,4,5,6,2,1,3);
  fvpart[237] = AL(0,4,5,6,2,3,1);
  fvpart[238] = AL(0,4,5,6,3,1,2);
  fvpart[239] = AL(0,4,5,6,3,2,1);
  fvpart[240] = AL(0,5,1,2,3,6,4);
  fvpart[241] = AL(0,5,1,2,6,3,4);
  fvpart[242] = AL(0,5,1,2,6,4,3);
  fvpart[243] = AL(0,5,1,3,2,6,4);
  fvpart[244] = AL(0,5,1,3,6,2,4);
  fvpart[245] = AL(0,5,1,3,6,4,2);
  fvpart[246] = AL(0,5,1,6,2,3,4);
  fvpart[247] = AL(0,5,1,6,2,4,3);
  fvpart[248] = AL(0,5,1,6,3,2,4);
  fvpart[249] = AL(0,5,1,6,3,4,2);
  fvpart[250] = AL(0,5,1,6,4,2,3);
  fvpart[251] = AL(0,5,1,6,4,3,2);
  fvpart[252] = AL(0,5,2,1,3,6,4);
  fvpart[253] = AL(0,5,2,1,6,3,4);
  fvpart[254] = AL(0,5,2,1,6,4,3);
  fvpart[255] = AL(0,5,2,3,1,6,4);
  fvpart[256] = AL(0,5,2,3,6,1,4);
  fvpart[257] = AL(0,5,2,3,6,4,1);
  fvpart[258] = AL(0,5,2,6,1,3,4);
  fvpart[259] = AL(0,5,2,6,1,4,3);
  fvpart[260] = AL(0,5,2,6,3,1,4);
  fvpart[261] = AL(0,5,2,6,3,4,1);
  fvpart[262] = AL(0,5,2,6,4,1,3);
  fvpart[263] = AL(0,5,2,6,4,3,1);
  fvpart[264] = AL(0,5,3,1,2,6,4);
  fvpart[265] = AL(0,5,3,1,6,2,4);
  fvpart[266] = AL(0,5,3,1,6,4,2);
  fvpart[267] = AL(0,5,3,2,1,6,4);
  fvpart[268] = AL(0,5,3,2,6,1,4);
  fvpart[269] = AL(0,5,3,2,6,4,1);
  fvpart[270] = AL(0,5,3,6,1,2,4);
  fvpart[271] = AL(0,5,3,6,1,4,2);
  fvpart[272] = AL(0,5,3,6,2,1,4);
  fvpart[273] = AL(0,5,3,6,2,4,1);
  fvpart[274] = AL(0,5,3,6,4,1,2);
  fvpart[275] = AL(0,5,3,6,4,2,1);
  fvpart[276] = AL(0,5,6,1,2,3,4);
  fvpart[277] = AL(0,5,6,1,2,4,3);
  fvpart[278] = AL(0,5,6,1,3,2,4);
  fvpart[279] = AL(0,5,6,1,3,4,2);
  fvpart[280] = AL(0,5,6,1,4,2,3);
  fvpart[281] = AL(0,5,6,1,4,3,2);
  fvpart[282] = AL(0,5,6,2,1,3,4);
  fvpart[283] = AL(0,5,6,2,1,4,3);
  fvpart[284] = AL(0,5,6,2,3,1,4);
  fvpart[285] = AL(0,5,6,2,3,4,1);
  fvpart[286] = AL(0,5,6,2,4,1,3);
  fvpart[287] = AL(0,5,6,2,4,3,1);
  fvpart[288] = AL(0,5,6,3,1,2,4);
  fvpart[289] = AL(0,5,6,3,1,4,2);
  fvpart[290] = AL(0,5,6,3,2,1,4);
  fvpart[291] = AL(0,5,6,3,2,4,1);
  fvpart[292] = AL(0,5,6,3,4,1,2);
  fvpart[293] = AL(0,5,6,3,4,2,1);
  fvpart[294] = AL(0,5,6,4,1,2,3);
  fvpart[295] = AL(0,5,6,4,1,3,2);
  fvpart[296] = AL(0,5,6,4,2,1,3);
  fvpart[297] = AL(0,5,6,4,2,3,1);
  fvpart[298] = AL(0,5,6,4,3,1,2);
  fvpart[299] = AL(0,5,6,4,3,2,1);
  fvpart[300] = AL(0,6,1,2,3,4,5);
  fvpart[301] = AL(0,6,1,2,4,3,5);
  fvpart[302] = AL(0,6,1,2,4,5,3);
  fvpart[303] = AL(0,6,1,3,2,4,5);
  fvpart[304] = AL(0,6,1,3,4,2,5);
  fvpart[305] = AL(0,6,1,3,4,5,2);
  fvpart[306] = AL(0,6,1,4,2,3,5);
  fvpart[307] = AL(0,6,1,4,2,5,3);
  fvpart[308] = AL(0,6,1,4,3,2,5);
  fvpart[309] = AL(0,6,1,4,3,5,2);
  fvpart[310] = AL(0,6,1,4,5,2,3);
  fvpart[311] = AL(0,6,1,4,5,3,2);
  fvpart[312] = AL(0,6,2,1,3,4,5);
  fvpart[313] = AL(0,6,2,1,4,3,5);
  fvpart[314] = AL(0,6,2,1,4,5,3);
  fvpart[315] = AL(0,6,2,3,1,4,5);
  fvpart[316] = AL(0,6,2,3,4,1,5);
  fvpart[317] = AL(0,6,2,3,4,5,1);
  fvpart[318] = AL(0,6,2,4,1,3,5);
  fvpart[319] = AL(0,6,2,4,1,5,3);
  fvpart[320] = AL(0,6,2,4,3,1,5);
  fvpart[321] = AL(0,6,2,4,3,5,1);
  fvpart[322] = AL(0,6,2,4,5,1,3);
  fvpart[323] = AL(0,6,2,4,5,3,1);
  fvpart[324] = AL(0,6,3,1,2,4,5);
  fvpart[325] = AL(0,6,3,1,4,2,5);
  fvpart[326] = AL(0,6,3,1,4,5,2);
  fvpart[327] = AL(0,6,3,2,1,4,5);
  fvpart[328] = AL(0,6,3,2,4,1,5);
  fvpart[329] = AL(0,6,3,2,4,5,1);
  fvpart[330] = AL(0,6,3,4,1,2,5);
  fvpart[331] = AL(0,6,3,4,1,5,2);
  fvpart[332] = AL(0,6,3,4,2,1,5);
  fvpart[333] = AL(0,6,3,4,2,5,1);
  fvpart[334] = AL(0,6,3,4,5,1,2);
  fvpart[335] = AL(0,6,3,4,5,2,1);
  fvpart[336] = AL(0,6,4,1,2,3,5);
  fvpart[337] = AL(0,6,4,1,2,5,3);
  fvpart[338] = AL(0,6,4,1,3,2,5);
  fvpart[339] = AL(0,6,4,1,3,5,2);
  fvpart[340] = AL(0,6,4,1,5,2,3);
  fvpart[341] = AL(0,6,4,1,5,3,2);
  fvpart[342] = AL(0,6,4,2,1,3,5);
  fvpart[343] = AL(0,6,4,2,1,5,3);
  fvpart[344] = AL(0,6,4,2,3,1,5);
  fvpart[345] = AL(0,6,4,2,3,5,1);
  fvpart[346] = AL(0,6,4,2,5,1,3);
  fvpart[347] = AL(0,6,4,2,5,3,1);
  fvpart[348] = AL(0,6,4,3,1,2,5);
  fvpart[349] = AL(0,6,4,3,1,5,2);
  fvpart[350] = AL(0,6,4,3,2,1,5);
  fvpart[351] = AL(0,6,4,3,2,5,1);
  fvpart[352] = AL(0,6,4,3,5,1,2);
  fvpart[353] = AL(0,6,4,3,5,2,1);
  fvpart[354] = AL(0,6,4,5,1,2,3);
  fvpart[355] = AL(0,6,4,5,1,3,2);
  fvpart[356] = AL(0,6,4,5,2,1,3);
  fvpart[357] = AL(0,6,4,5,2,3,1);
  fvpart[358] = AL(0,6,4,5,3,1,2);
  fvpart[359] = AL(0,6,4,5,3,2,1);

  for (int i=0; i<360; i++) {
    fvpart[i] *= Nc;
  }

  if (Nf != 0.) {
    fvpart[360] = AF(0,1,2,3,4,5,6);
    fvpart[361] = AF(0,1,2,3,5,6,4);
    fvpart[362] = AF(0,1,2,3,6,4,5);
    fvpart[363] = AF(0,1,2,4,3,5,6);
    fvpart[364] = AF(0,1,2,4,5,3,6);
    fvpart[365] = AF(0,1,2,4,5,6,3);
    fvpart[366] = AF(0,1,2,5,3,6,4);
    fvpart[367] = AF(0,1,2,5,6,3,4);
    fvpart[368] = AF(0,1,2,5,6,4,3);
    fvpart[369] = AF(0,1,2,6,3,4,5);
    fvpart[370] = AF(0,1,2,6,4,3,5);
    fvpart[371] = AF(0,1,2,6,4,5,3);
    fvpart[372] = AF(0,1,3,2,4,5,6);
    fvpart[373] = AF(0,1,3,2,5,6,4);
    fvpart[374] = AF(0,1,3,2,6,4,5);
    fvpart[375] = AF(0,1,3,4,2,5,6);
    fvpart[376] = AF(0,1,3,4,5,2,6);
    fvpart[377] = AF(0,1,3,4,5,6,2);
    fvpart[378] = AF(0,1,3,5,2,6,4);
    fvpart[379] = AF(0,1,3,5,6,2,4);
    fvpart[380] = AF(0,1,3,5,6,4,2);
    fvpart[381] = AF(0,1,3,6,2,4,5);
    fvpart[382] = AF(0,1,3,6,4,2,5);
    fvpart[383] = AF(0,1,3,6,4,5,2);
    fvpart[384] = AF(0,1,4,2,3,5,6);
    fvpart[385] = AF(0,1,4,2,5,3,6);
    fvpart[386] = AF(0,1,4,2,5,6,3);
    fvpart[387] = AF(0,1,4,3,2,5,6);
    fvpart[388] = AF(0,1,4,3,5,2,6);
    fvpart[389] = AF(0,1,4,3,5,6,2);
    fvpart[390] = AF(0,1,4,5,2,3,6);
    fvpart[391] = AF(0,1,4,5,2,6,3);
    fvpart[392] = AF(0,1,4,5,3,2,6);
    fvpart[393] = AF(0,1,4,5,3,6,2);
    fvpart[394] = AF(0,1,4,5,6,2,3);
    fvpart[395] = AF(0,1,4,5,6,3,2);
    fvpart[396] = AF(0,1,5,2,3,6,4);
    fvpart[397] = AF(0,1,5,2,6,3,4);
    fvpart[398] = AF(0,1,5,2,6,4,3);
    fvpart[399] = AF(0,1,5,3,2,6,4);
    fvpart[400] = AF(0,1,5,3,6,2,4);
    fvpart[401] = AF(0,1,5,3,6,4,2);
    fvpart[402] = AF(0,1,5,6,2,3,4);
    fvpart[403] = AF(0,1,5,6,2,4,3);
    fvpart[404] = AF(0,1,5,6,3,2,4);
    fvpart[405] = AF(0,1,5,6,3,4,2);
    fvpart[406] = AF(0,1,5,6,4,2,3);
    fvpart[407] = AF(0,1,5,6,4,3,2);
    fvpart[408] = AF(0,1,6,2,3,4,5);
    fvpart[409] = AF(0,1,6,2,4,3,5);
    fvpart[410] = AF(0,1,6,2,4,5,3);
    fvpart[411] = AF(0,1,6,3,2,4,5);
    fvpart[412] = AF(0,1,6,3,4,2,5);
    fvpart[413] = AF(0,1,6,3,4,5,2);
    fvpart[414] = AF(0,1,6,4,2,3,5);
    fvpart[415] = AF(0,1,6,4,2,5,3);
    fvpart[416] = AF(0,1,6,4,3,2,5);
    fvpart[417] = AF(0,1,6,4,3,5,2);
    fvpart[418] = AF(0,1,6,4,5,2,3);
    fvpart[419] = AF(0,1,6,4,5,3,2);
    fvpart[420] = AF(0,2,1,3,4,5,6);
    fvpart[421] = AF(0,2,1,3,5,6,4);
    fvpart[422] = AF(0,2,1,3,6,4,5);
    fvpart[423] = AF(0,2,1,4,3,5,6);
    fvpart[424] = AF(0,2,1,4,5,3,6);
    fvpart[425] = AF(0,2,1,4,5,6,3);
    fvpart[426] = AF(0,2,1,5,3,6,4);
    fvpart[427] = AF(0,2,1,5,6,3,4);
    fvpart[428] = AF(0,2,1,5,6,4,3);
    fvpart[429] = AF(0,2,1,6,3,4,5);
    fvpart[430] = AF(0,2,1,6,4,3,5);
    fvpart[431] = AF(0,2,1,6,4,5,3);
    fvpart[432] = AF(0,2,3,1,4,5,6);
    fvpart[433] = AF(0,2,3,1,5,6,4);
    fvpart[434] = AF(0,2,3,1,6,4,5);
    fvpart[435] = AF(0,2,3,4,1,5,6);
    fvpart[436] = AF(0,2,3,4,5,1,6);
    fvpart[437] = AF(0,2,3,4,5,6,1);
    fvpart[438] = AF(0,2,3,5,1,6,4);
    fvpart[439] = AF(0,2,3,5,6,1,4);
    fvpart[440] = AF(0,2,3,5,6,4,1);
    fvpart[441] = AF(0,2,3,6,1,4,5);
    fvpart[442] = AF(0,2,3,6,4,1,5);
    fvpart[443] = AF(0,2,3,6,4,5,1);
    fvpart[444] = AF(0,2,4,1,3,5,6);
    fvpart[445] = AF(0,2,4,1,5,3,6);
    fvpart[446] = AF(0,2,4,1,5,6,3);
    fvpart[447] = AF(0,2,4,3,1,5,6);
    fvpart[448] = AF(0,2,4,3,5,1,6);
    fvpart[449] = AF(0,2,4,3,5,6,1);
    fvpart[450] = AF(0,2,4,5,1,3,6);
    fvpart[451] = AF(0,2,4,5,1,6,3);
    fvpart[452] = AF(0,2,4,5,3,1,6);
    fvpart[453] = AF(0,2,4,5,3,6,1);
    fvpart[454] = AF(0,2,4,5,6,1,3);
    fvpart[455] = AF(0,2,4,5,6,3,1);
    fvpart[456] = AF(0,2,5,1,3,6,4);
    fvpart[457] = AF(0,2,5,1,6,3,4);
    fvpart[458] = AF(0,2,5,1,6,4,3);
    fvpart[459] = AF(0,2,5,3,1,6,4);
    fvpart[460] = AF(0,2,5,3,6,1,4);
    fvpart[461] = AF(0,2,5,3,6,4,1);
    fvpart[462] = AF(0,2,5,6,1,3,4);
    fvpart[463] = AF(0,2,5,6,1,4,3);
    fvpart[464] = AF(0,2,5,6,3,1,4);
    fvpart[465] = AF(0,2,5,6,3,4,1);
    fvpart[466] = AF(0,2,5,6,4,1,3);
    fvpart[467] = AF(0,2,5,6,4,3,1);
    fvpart[468] = AF(0,2,6,1,3,4,5);
    fvpart[469] = AF(0,2,6,1,4,3,5);
    fvpart[470] = AF(0,2,6,1,4,5,3);
    fvpart[471] = AF(0,2,6,3,1,4,5);
    fvpart[472] = AF(0,2,6,3,4,1,5);
    fvpart[473] = AF(0,2,6,3,4,5,1);
    fvpart[474] = AF(0,2,6,4,1,3,5);
    fvpart[475] = AF(0,2,6,4,1,5,3);
    fvpart[476] = AF(0,2,6,4,3,1,5);
    fvpart[477] = AF(0,2,6,4,3,5,1);
    fvpart[478] = AF(0,2,6,4,5,1,3);
    fvpart[479] = AF(0,2,6,4,5,3,1);
    fvpart[480] = AF(0,3,1,2,4,5,6);
    fvpart[481] = AF(0,3,1,2,5,6,4);
    fvpart[482] = AF(0,3,1,2,6,4,5);
    fvpart[483] = AF(0,3,1,4,2,5,6);
    fvpart[484] = AF(0,3,1,4,5,2,6);
    fvpart[485] = AF(0,3,1,4,5,6,2);
    fvpart[486] = AF(0,3,1,5,2,6,4);
    fvpart[487] = AF(0,3,1,5,6,2,4);
    fvpart[488] = AF(0,3,1,5,6,4,2);
    fvpart[489] = AF(0,3,1,6,2,4,5);
    fvpart[490] = AF(0,3,1,6,4,2,5);
    fvpart[491] = AF(0,3,1,6,4,5,2);
    fvpart[492] = AF(0,3,2,1,4,5,6);
    fvpart[493] = AF(0,3,2,1,5,6,4);
    fvpart[494] = AF(0,3,2,1,6,4,5);
    fvpart[495] = AF(0,3,2,4,1,5,6);
    fvpart[496] = AF(0,3,2,4,5,1,6);
    fvpart[497] = AF(0,3,2,4,5,6,1);
    fvpart[498] = AF(0,3,2,5,1,6,4);
    fvpart[499] = AF(0,3,2,5,6,1,4);
    fvpart[500] = AF(0,3,2,5,6,4,1);
    fvpart[501] = AF(0,3,2,6,1,4,5);
    fvpart[502] = AF(0,3,2,6,4,1,5);
    fvpart[503] = AF(0,3,2,6,4,5,1);
    fvpart[504] = AF(0,3,4,1,2,5,6);
    fvpart[505] = AF(0,3,4,1,5,2,6);
    fvpart[506] = AF(0,3,4,1,5,6,2);
    fvpart[507] = AF(0,3,4,2,1,5,6);
    fvpart[508] = AF(0,3,4,2,5,1,6);
    fvpart[509] = AF(0,3,4,2,5,6,1);
    fvpart[510] = AF(0,3,4,5,1,2,6);
    fvpart[511] = AF(0,3,4,5,1,6,2);
    fvpart[512] = AF(0,3,4,5,2,1,6);
    fvpart[513] = AF(0,3,4,5,2,6,1);
    fvpart[514] = AF(0,3,4,5,6,1,2);
    fvpart[515] = AF(0,3,4,5,6,2,1);
    fvpart[516] = AF(0,3,5,1,2,6,4);
    fvpart[517] = AF(0,3,5,1,6,2,4);
    fvpart[518] = AF(0,3,5,1,6,4,2);
    fvpart[519] = AF(0,3,5,2,1,6,4);
    fvpart[520] = AF(0,3,5,2,6,1,4);
    fvpart[521] = AF(0,3,5,2,6,4,1);
    fvpart[522] = AF(0,3,5,6,1,2,4);
    fvpart[523] = AF(0,3,5,6,1,4,2);
    fvpart[524] = AF(0,3,5,6,2,1,4);
    fvpart[525] = AF(0,3,5,6,2,4,1);
    fvpart[526] = AF(0,3,5,6,4,1,2);
    fvpart[527] = AF(0,3,5,6,4,2,1);
    fvpart[528] = AF(0,3,6,1,2,4,5);
    fvpart[529] = AF(0,3,6,1,4,2,5);
    fvpart[530] = AF(0,3,6,1,4,5,2);
    fvpart[531] = AF(0,3,6,2,1,4,5);
    fvpart[532] = AF(0,3,6,2,4,1,5);
    fvpart[533] = AF(0,3,6,2,4,5,1);
    fvpart[534] = AF(0,3,6,4,1,2,5);
    fvpart[535] = AF(0,3,6,4,1,5,2);
    fvpart[536] = AF(0,3,6,4,2,1,5);
    fvpart[537] = AF(0,3,6,4,2,5,1);
    fvpart[538] = AF(0,3,6,4,5,1,2);
    fvpart[539] = AF(0,3,6,4,5,2,1);
    fvpart[540] = AF(0,4,1,2,3,5,6);
    fvpart[541] = AF(0,4,1,2,5,3,6);
    fvpart[542] = AF(0,4,1,2,5,6,3);
    fvpart[543] = AF(0,4,1,3,2,5,6);
    fvpart[544] = AF(0,4,1,3,5,2,6);
    fvpart[545] = AF(0,4,1,3,5,6,2);
    fvpart[546] = AF(0,4,1,5,2,3,6);
    fvpart[547] = AF(0,4,1,5,2,6,3);
    fvpart[548] = AF(0,4,1,5,3,2,6);
    fvpart[549] = AF(0,4,1,5,3,6,2);
    fvpart[550] = AF(0,4,1,5,6,2,3);
    fvpart[551] = AF(0,4,1,5,6,3,2);
    fvpart[552] = AF(0,4,2,1,3,5,6);
    fvpart[553] = AF(0,4,2,1,5,3,6);
    fvpart[554] = AF(0,4,2,1,5,6,3);
    fvpart[555] = AF(0,4,2,3,1,5,6);
    fvpart[556] = AF(0,4,2,3,5,1,6);
    fvpart[557] = AF(0,4,2,3,5,6,1);
    fvpart[558] = AF(0,4,2,5,1,3,6);
    fvpart[559] = AF(0,4,2,5,1,6,3);
    fvpart[560] = AF(0,4,2,5,3,1,6);
    fvpart[561] = AF(0,4,2,5,3,6,1);
    fvpart[562] = AF(0,4,2,5,6,1,3);
    fvpart[563] = AF(0,4,2,5,6,3,1);
    fvpart[564] = AF(0,4,3,1,2,5,6);
    fvpart[565] = AF(0,4,3,1,5,2,6);
    fvpart[566] = AF(0,4,3,1,5,6,2);
    fvpart[567] = AF(0,4,3,2,1,5,6);
    fvpart[568] = AF(0,4,3,2,5,1,6);
    fvpart[569] = AF(0,4,3,2,5,6,1);
    fvpart[570] = AF(0,4,3,5,1,2,6);
    fvpart[571] = AF(0,4,3,5,1,6,2);
    fvpart[572] = AF(0,4,3,5,2,1,6);
    fvpart[573] = AF(0,4,3,5,2,6,1);
    fvpart[574] = AF(0,4,3,5,6,1,2);
    fvpart[575] = AF(0,4,3,5,6,2,1);
    fvpart[576] = AF(0,4,5,1,2,3,6);
    fvpart[577] = AF(0,4,5,1,2,6,3);
    fvpart[578] = AF(0,4,5,1,3,2,6);
    fvpart[579] = AF(0,4,5,1,3,6,2);
    fvpart[580] = AF(0,4,5,1,6,2,3);
    fvpart[581] = AF(0,4,5,1,6,3,2);
    fvpart[582] = AF(0,4,5,2,1,3,6);
    fvpart[583] = AF(0,4,5,2,1,6,3);
    fvpart[584] = AF(0,4,5,2,3,1,6);
    fvpart[585] = AF(0,4,5,2,3,6,1);
    fvpart[586] = AF(0,4,5,2,6,1,3);
    fvpart[587] = AF(0,4,5,2,6,3,1);
    fvpart[588] = AF(0,4,5,3,1,2,6);
    fvpart[589] = AF(0,4,5,3,1,6,2);
    fvpart[590] = AF(0,4,5,3,2,1,6);
    fvpart[591] = AF(0,4,5,3,2,6,1);
    fvpart[592] = AF(0,4,5,3,6,1,2);
    fvpart[593] = AF(0,4,5,3,6,2,1);
    fvpart[594] = AF(0,4,5,6,1,2,3);
    fvpart[595] = AF(0,4,5,6,1,3,2);
    fvpart[596] = AF(0,4,5,6,2,1,3);
    fvpart[597] = AF(0,4,5,6,2,3,1);
    fvpart[598] = AF(0,4,5,6,3,1,2);
    fvpart[599] = AF(0,4,5,6,3,2,1);
    fvpart[600] = AF(0,5,1,2,3,6,4);
    fvpart[601] = AF(0,5,1,2,6,3,4);
    fvpart[602] = AF(0,5,1,2,6,4,3);
    fvpart[603] = AF(0,5,1,3,2,6,4);
    fvpart[604] = AF(0,5,1,3,6,2,4);
    fvpart[605] = AF(0,5,1,3,6,4,2);
    fvpart[606] = AF(0,5,1,6,2,3,4);
    fvpart[607] = AF(0,5,1,6,2,4,3);
    fvpart[608] = AF(0,5,1,6,3,2,4);
    fvpart[609] = AF(0,5,1,6,3,4,2);
    fvpart[610] = AF(0,5,1,6,4,2,3);
    fvpart[611] = AF(0,5,1,6,4,3,2);
    fvpart[612] = AF(0,5,2,1,3,6,4);
    fvpart[613] = AF(0,5,2,1,6,3,4);
    fvpart[614] = AF(0,5,2,1,6,4,3);
    fvpart[615] = AF(0,5,2,3,1,6,4);
    fvpart[616] = AF(0,5,2,3,6,1,4);
    fvpart[617] = AF(0,5,2,3,6,4,1);
    fvpart[618] = AF(0,5,2,6,1,3,4);
    fvpart[619] = AF(0,5,2,6,1,4,3);
    fvpart[620] = AF(0,5,2,6,3,1,4);
    fvpart[621] = AF(0,5,2,6,3,4,1);
    fvpart[622] = AF(0,5,2,6,4,1,3);
    fvpart[623] = AF(0,5,2,6,4,3,1);
    fvpart[624] = AF(0,5,3,1,2,6,4);
    fvpart[625] = AF(0,5,3,1,6,2,4);
    fvpart[626] = AF(0,5,3,1,6,4,2);
    fvpart[627] = AF(0,5,3,2,1,6,4);
    fvpart[628] = AF(0,5,3,2,6,1,4);
    fvpart[629] = AF(0,5,3,2,6,4,1);
    fvpart[630] = AF(0,5,3,6,1,2,4);
    fvpart[631] = AF(0,5,3,6,1,4,2);
    fvpart[632] = AF(0,5,3,6,2,1,4);
    fvpart[633] = AF(0,5,3,6,2,4,1);
    fvpart[634] = AF(0,5,3,6,4,1,2);
    fvpart[635] = AF(0,5,3,6,4,2,1);
    fvpart[636] = AF(0,5,6,1,2,3,4);
    fvpart[637] = AF(0,5,6,1,2,4,3);
    fvpart[638] = AF(0,5,6,1,3,2,4);
    fvpart[639] = AF(0,5,6,1,3,4,2);
    fvpart[640] = AF(0,5,6,1,4,2,3);
    fvpart[641] = AF(0,5,6,1,4,3,2);
    fvpart[642] = AF(0,5,6,2,1,3,4);
    fvpart[643] = AF(0,5,6,2,1,4,3);
    fvpart[644] = AF(0,5,6,2,3,1,4);
    fvpart[645] = AF(0,5,6,2,3,4,1);
    fvpart[646] = AF(0,5,6,2,4,1,3);
    fvpart[647] = AF(0,5,6,2,4,3,1);
    fvpart[648] = AF(0,5,6,3,1,2,4);
    fvpart[649] = AF(0,5,6,3,1,4,2);
    fvpart[650] = AF(0,5,6,3,2,1,4);
    fvpart[651] = AF(0,5,6,3,2,4,1);
    fvpart[652] = AF(0,5,6,3,4,1,2);
    fvpart[653] = AF(0,5,6,3,4,2,1);
    fvpart[654] = AF(0,5,6,4,1,2,3);
    fvpart[655] = AF(0,5,6,4,1,3,2);
    fvpart[656] = AF(0,5,6,4,2,1,3);
    fvpart[657] = AF(0,5,6,4,2,3,1);
    fvpart[658] = AF(0,5,6,4,3,1,2);
    fvpart[659] = AF(0,5,6,4,3,2,1);
    fvpart[660] = AF(0,6,1,2,3,4,5);
    fvpart[661] = AF(0,6,1,2,4,3,5);
    fvpart[662] = AF(0,6,1,2,4,5,3);
    fvpart[663] = AF(0,6,1,3,2,4,5);
    fvpart[664] = AF(0,6,1,3,4,2,5);
    fvpart[665] = AF(0,6,1,3,4,5,2);
    fvpart[666] = AF(0,6,1,4,2,3,5);
    fvpart[667] = AF(0,6,1,4,2,5,3);
    fvpart[668] = AF(0,6,1,4,3,2,5);
    fvpart[669] = AF(0,6,1,4,3,5,2);
    fvpart[670] = AF(0,6,1,4,5,2,3);
    fvpart[671] = AF(0,6,1,4,5,3,2);
    fvpart[672] = AF(0,6,2,1,3,4,5);
    fvpart[673] = AF(0,6,2,1,4,3,5);
    fvpart[674] = AF(0,6,2,1,4,5,3);
    fvpart[675] = AF(0,6,2,3,1,4,5);
    fvpart[676] = AF(0,6,2,3,4,1,5);
    fvpart[677] = AF(0,6,2,3,4,5,1);
    fvpart[678] = AF(0,6,2,4,1,3,5);
    fvpart[679] = AF(0,6,2,4,1,5,3);
    fvpart[680] = AF(0,6,2,4,3,1,5);
    fvpart[681] = AF(0,6,2,4,3,5,1);
    fvpart[682] = AF(0,6,2,4,5,1,3);
    fvpart[683] = AF(0,6,2,4,5,3,1);
    fvpart[684] = AF(0,6,3,1,2,4,5);
    fvpart[685] = AF(0,6,3,1,4,2,5);
    fvpart[686] = AF(0,6,3,1,4,5,2);
    fvpart[687] = AF(0,6,3,2,1,4,5);
    fvpart[688] = AF(0,6,3,2,4,1,5);
    fvpart[689] = AF(0,6,3,2,4,5,1);
    fvpart[690] = AF(0,6,3,4,1,2,5);
    fvpart[691] = AF(0,6,3,4,1,5,2);
    fvpart[692] = AF(0,6,3,4,2,1,5);
    fvpart[693] = AF(0,6,3,4,2,5,1);
    fvpart[694] = AF(0,6,3,4,5,1,2);
    fvpart[695] = AF(0,6,3,4,5,2,1);
    fvpart[696] = AF(0,6,4,1,2,3,5);
    fvpart[697] = AF(0,6,4,1,2,5,3);
    fvpart[698] = AF(0,6,4,1,3,2,5);
    fvpart[699] = AF(0,6,4,1,3,5,2);
    fvpart[700] = AF(0,6,4,1,5,2,3);
    fvpart[701] = AF(0,6,4,1,5,3,2);
    fvpart[702] = AF(0,6,4,2,1,3,5);
    fvpart[703] = AF(0,6,4,2,1,5,3);
    fvpart[704] = AF(0,6,4,2,3,1,5);
    fvpart[705] = AF(0,6,4,2,3,5,1);
    fvpart[706] = AF(0,6,4,2,5,1,3);
    fvpart[707] = AF(0,6,4,2,5,3,1);
    fvpart[708] = AF(0,6,4,3,1,2,5);
    fvpart[709] = AF(0,6,4,3,1,5,2);
    fvpart[710] = AF(0,6,4,3,2,1,5);
    fvpart[711] = AF(0,6,4,3,2,5,1);
    fvpart[712] = AF(0,6,4,3,5,1,2);
    fvpart[713] = AF(0,6,4,3,5,2,1);
    fvpart[714] = AF(0,6,4,5,1,2,3);
    fvpart[715] = AF(0,6,4,5,1,3,2);
    fvpart[716] = AF(0,6,4,5,2,1,3);
    fvpart[717] = AF(0,6,4,5,2,3,1);
    fvpart[718] = AF(0,6,4,5,3,1,2);
    fvpart[719] = AF(0,6,4,5,3,2,1);

    for (int i=360; i<720; i++) {
      fvpart[i] *= Nf;
    }
  } else {
    for (int i=360; i<720; i++) {
      fvpart[i] = LT();
    }
  }
}

#ifdef USE_SD
  template class Amp0q7g<double>;
#endif
#ifdef USE_DD
  template class Amp0q7g<dd_real>;
#endif
#ifdef USE_QD
  template class Amp0q7g<qd_real>;
#endif
#ifdef USE_VC
  template class Amp0q7g<Vc::double_v>;
#endif

// class Amp0q7g_ds5

template <typename T>
Amp0q7g_ds5<T>::Amp0q7g_ds5(const T scalefactor,
                            const int mFC, const NJetAmpTables& tables)
  : BaseClass(scalefactor, mFC, tables)
{
  initNc();
}

template <typename T>
void Amp0q7g_ds5<T>::setNc(const ST Nc_)
{
  BaseClass::setNc(Nc_);
  initNc();
}

template <typename T>
void Amp0q7g_ds5<T>::initNc()
{
  loopFactor = 60.*BaseClass::loopFactor;  // add n!/2 factor
}

template <typename T>
void Amp0q7g_ds5<T>::getfvpart1ds(const int fv, LoopValue* fvpart)
{
  return getfvpart1ds_(fv, fvpart);
}

template <typename T>
void Amp0q7g_ds5<T>::getfvpart1ds(const int fv, LoopResult<T>* fvpart)
{
  return getfvpart1ds_(fv, fvpart);
}

template <typename T>
template <typename LT>
inline
void Amp0q7g_ds5<T>::getfvpart1ds_(const int fv, LT* fvpart)
{
  fvSet(fv);
  fvpart[0] = Nc*AL(0,1,2,3,4,5,6);
  fvpart[1] = Nc*AL(0,2,1,3,4,5,6);
  fvpart[2] = Nc*AL(0,2,3,1,4,5,6);
  fvpart[3] = Nc*AL(0,2,3,4,1,5,6);
  fvpart[4] = Nc*AL(0,2,3,4,5,1,6);
  fvpart[5] = Nc*AL(0,2,3,4,5,6,1);

  if (Nf != 0.) {
    fvpart[ 6] = Nf*AF(0,1,2,3,4,5,6);
    fvpart[ 7] = Nf*AF(0,2,1,3,4,5,6);
    fvpart[ 8] = Nf*AF(0,2,3,1,4,5,6);
    fvpart[ 9] = Nf*AF(0,2,3,4,1,5,6);
    fvpart[10] = Nf*AF(0,2,3,4,5,1,6);
    fvpart[11] = Nf*AF(0,2,3,4,5,6,1);
  } else {
    fvpart[ 6] = LT();
    fvpart[ 7] = LT();
    fvpart[ 8] = LT();
    fvpart[ 9] = LT();
    fvpart[10] = LT();
    fvpart[11] = LT();
  }
}

#ifdef USE_SD
  template class Amp0q7g_ds5<double>;
#endif
#ifdef USE_DD
  template class Amp0q7g_ds5<dd_real>;
#endif
#ifdef USE_QD
  template class Amp0q7g_ds5<qd_real>;
#endif
#ifdef USE_VC
  template class Amp0q7g_ds5<Vc::double_v>;
#endif
