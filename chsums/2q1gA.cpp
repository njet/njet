/*
* chsums/2q1gA.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include "2q1gA.h"
#include "NJetAmp-T.h"

// class Amp2q1gAA

template <typename T>
Amp2q1gAA<T>::Amp2q1gAA(const Flavour<double>& ff, const T scalefactor,
                        const int mFC, const NJetAmpTables& tables)
  : BaseClass(scalefactor, mFC, tables)
{
  initProcess(ff);
}

template <typename T>
void Amp2q1gAA<T>::initProcess(const Flavour<double>& ff)
{
  for (int fv=0; fv<BaseClass::mFC; fv++) {
    Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
    flavours.push_back(ff);
    flavours.push_back(ff);
    BaseClass::setProcess(flavours, fv);
  }
}

template <typename T>
typename Amp2q1gAA<T>::TreeValue
Amp2q1gAA<T>::A0(int p0, int p1, int p2)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, NN+1, O[p1], O[p2]};
  return BaseClass::A0nAA(order);
}

template <typename T>
LoopResult<T> Amp2q1gAA<T>::AF(int p0, int p1, int p2)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, NN+1, O[p1], O[p2]};
  return BaseClass::AFnAA(order);
}

template <typename T>
LoopResult<T> Amp2q1gAA<T>::AL(int p0, int p1, int p2)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, NN+1, O[p1], O[p2]};
  return BaseClass::ALnAA(order);
}

// class Amp2q1gAAx

template <typename T>
Amp2q1gAAx<T>::Amp2q1gAAx(const Flavour<double>& ff, const T scalefactor,
                          const int mFC, const NJetAmpTables& tables)
 : BaseClass(ff, scalefactor, mFC, tables)
{
  const Flavour<double> Ax = StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C());
  for (int fv=0; fv<mFC; fv++) {
    if (fv % modFC == 0) {
      Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
      flavours.push_back(ff);
      flavours.push_back(Ax);
      BaseClass::setProcess(flavours, fv);
    }
    else if (fv % modFC == 1) {
      Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
      flavours.push_back(Ax);
      flavours.push_back(Ax);
      BaseClass::setProcess(flavours, fv);
    }
  }
}

template <typename T>
void Amp2q1gAAx<T>::setNf(const ST Nf_)
{
  const int intNf = int(to_double(Nf_));
  if (double(intNf) == Nf_) {
    const int nu = intNf/2;
    const int nd = (intNf+1)/2;
    Nfx = -(double(nu)*Particle<ST>::getQu() + double(nd)*Particle<ST>::getQd());
    Nfxx = (double(nu)*Particle<ST>::getQu()*Particle<ST>::getQu()
          + double(nd)*Particle<ST>::getQd()*Particle<ST>::getQd());
  }
  BaseClass::setNf(Nf_);
}

template <typename T>
void Amp2q1gAAx<T>::getfvpart1_full(const int fv, LoopValue* fvpart)
{
  return getfvpart1_full_(fv, fvpart);
}

template <typename T>
void Amp2q1gAAx<T>::getfvpart1_full(const int fv, LoopResult<T>* fvpart)
{
  return getfvpart1_full_(fv, fvpart);
}

template <typename T>
template <typename LT>
inline
void Amp2q1gAAx<T>::getfvpart1_full_(const int fv, LT* fvpart)
{
  fvSet(fv);

  if (fv % modFC == 0 and Nf != 0.) {
    // 1 primitive
    // const LT Q1234 = AFx(0,1,2); // always zero

    fvpart[0] = LT(); // Nfx*(-Q1234);
  } else if (fv % modFC == 1 and Nf != 0.) {
    // 1 primitive
    const LT Q1234 = AFxx(0,1,2);

    fvpart[0] = Nfxx*(-Q1234);
  } else {
    fvpart[0] = LT();
  }
}

template <typename T>
LoopResult<T> Amp2q1gAAx<T>::AFx(int p0, int p1, int p2)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, O[p1], O[p2], NN+1};
  return BaseClass::AFnAAx(order);
}

template <typename T>
LoopResult<T> Amp2q1gAAx<T>::AFxx(int p0, int p1, int p2)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], O[p1], O[p2], NN, NN+1};
  return BaseClass::AFnAAxx(order);
}

#ifdef USE_SD
  template class Amp2q1gAA<double>;
  template class Amp2q1gAAx<double>;
#endif
#ifdef USE_DD
  template class Amp2q1gAA<dd_real>;
  template class Amp2q1gAAx<dd_real>;
#endif
#ifdef USE_QD
  template class Amp2q1gAA<qd_real>;
  template class Amp2q1gAAx<qd_real>;
#endif
#ifdef USE_VC
  template class Amp2q1gAA<Vc::double_v>;
  template class Amp2q1gAAx<Vc::double_v>;
#endif
