/*
* chsums/6q0gV.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef CHSUM_6Q0GV_H
#define CHSUM_6Q0GV_H

#include "6q0g.h"
#include "../ngluon2/Model.h"

class Amp6q0gVStatic : public Amp6q0gStatic
{
  public:
    static const int HS = 4;
    static const int HSNN = NN+1;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp6q0gV : public Amp6q0g<T>
{
    typedef Amp6q0g<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp6q0gV(const T scalefactor, const int mFC=1, const NJetAmpTables& tables=amptables());
    Amp6q0gV(const Flavour<double>& Vflav,
             const T scalefactor, const int mFC=1, const NJetAmpTables& tables=amptables());

    void born_part_fill() { BaseClass::born_part_fullfill(); }
    LoopValue virt() { return BaseClass::virt_fullsum(); }

  protected:
    using BaseClass::NN;
    using BaseClass::vhel;
    using BaseClass::getFlav;
    using BaseClass::getFperm;
    using BaseClass::mfv;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp6q0gVStatic>();
    }

    void initProcess(const Flavour<double>& ff);

    TreeValue A0(int p0, int p1, int p2, int p3, int p4, int p5);
    LoopResult<T> AL(int p0, int p1, int p2, int p3, int p4, int p5);
    LoopResult<T> AF(int p0, int p1, int p2, int p3, int p4, int p5);
};

class Amp6q0gV2Static : public Amp6q0gVStatic
{
  public:
    static const int HS = 6;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp6q0gV2 : public Amp6q0gV<T>
{
    typedef Amp6q0gV<T> BaseClass;
  public:

    Amp6q0gV2(const T scalefactor, const NJetAmpTables& tables=amptables())
      : BaseClass(scalefactor, 2, tables)
    { }

    Amp6q0gV2(const Flavour<double>& Vflav,
              const T scalefactor, const NJetAmpTables& tables=amptables())
      : BaseClass(Vflav, scalefactor, 2, tables)
    { }

  protected:
    using BaseClass::fvZero;
    using BaseClass::vhel;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp6q0gV2Static>();
    }

    void markZeroFv()
    {
      fvZero.reset();
      if (vhel[2] == vhel[3] || vhel[4] == vhel[5]) {
        fvZero[0] = true;
      }
      if (vhel[2] == vhel[5] || vhel[4] == vhel[3]) {
        fvZero[1] = true;
      }
    }
};

// symmetric quarks

class Amp6q0gV2nStatic : public Amp6q0gVStatic
{
  public:
    static const int HS = 6;
    static const int HSarr[HS][HSNN];

    static const int FC = 6;
    static const int flav[FC][NN];
    static const int fperm[FC][NN];
    static const int fvcol[FC][CC];
};

template <typename T>
class Amp6q0gV2n : public Amp6q0gV<T>
{
    typedef Amp6q0gV<T> BaseClass;
  public:

    Amp6q0gV2n(const T scalefactor, const int mFC=2, const NJetAmpTables& tables=amptables())
      : BaseClass(scalefactor, mFC, tables)
    { }

    Amp6q0gV2n(const Flavour<double>& Vflav,
               const T scalefactor, const int mFC=2, const NJetAmpTables& tables=amptables())
      : BaseClass(Vflav, scalefactor, mFC, tables)
    { }

  protected:
    using BaseClass::fvZero;
    using BaseClass::vhel;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp6q0gV2nStatic>();
    }

    void markZeroFv()
    {
      fvZero.reset();
      if (vhel[0] == vhel[1] || vhel[2] == vhel[3]) {
        fvZero[0] = true;
      }
      if (vhel[0] == vhel[3] || vhel[2] == vhel[1]) {
        fvZero[1] = true;
      }
    }
};

class Amp6q0gV6nStatic : public Amp6q0gV2nStatic
{
  public:
    static const int HS = 10;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp6q0gV6n : public Amp6q0gV2n<T>
{
    typedef Amp6q0gV2n<T> BaseClass;
  public:

    Amp6q0gV6n(const T scalefactor, const NJetAmpTables& tables=amptables())
      : BaseClass(scalefactor, 6, tables)
    { }

    Amp6q0gV6n(const Flavour<double>& Vflav,
              const T scalefactor, const NJetAmpTables& tables=amptables())
      : BaseClass(Vflav, scalefactor, 6, tables)
    { }

  protected:
    using BaseClass::fvZero;
    using BaseClass::vhel;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp6q0gV6nStatic>();
    }

    void markZeroFv()
    {
      fvZero.reset();
      if (vhel[0] == vhel[1]) {
        fvZero[0] = true;
        fvZero[5] = true;
      }
      if (vhel[0] == vhel[3]) {
        fvZero[4] = true;
        fvZero[1] = true;
      }
      if (vhel[0] == vhel[5]) {
        fvZero[2] = true;
        fvZero[3] = true;
      }

      if (vhel[2] == vhel[3]) {
        fvZero[0] = true;
        fvZero[3] = true;
      }
      if (vhel[2] == vhel[5]) {
        fvZero[4] = true;
        fvZero[5] = true;
      }
      if (vhel[2] == vhel[1]) {
        fvZero[2] = true;
        fvZero[1] = true;
      }

      if (vhel[4] == vhel[5]) {
        fvZero[0] = true;
        fvZero[1] = true;
      }
      if (vhel[4] == vhel[1]) {
        fvZero[4] = true;
        fvZero[3] = true;
      }
      if (vhel[4] == vhel[3]) {
        fvZero[2] = true;
        fvZero[5] = true;
      }
    }
};

// symmetric anti-quarks

class Amp6q0gV2bStatic : public Amp6q0gVStatic
{
  public:
    static const int HS = 6;
    static const int HSarr[HS][HSNN];

    static const int FC = 6;
    static const int flav[FC][NN];
    static const int fperm[FC][NN];
    static const int fvcol[FC][CC];
};

template <typename T>
class Amp6q0gV2b : public Amp6q0gV<T>
{
    typedef Amp6q0gV<T> BaseClass;
  public:

    Amp6q0gV2b(const T scalefactor, const int mFC=2, const NJetAmpTables& tables=amptables())
      : BaseClass(scalefactor, mFC, tables)
    { }

    Amp6q0gV2b(const Flavour<double>& Vflav,
               const T scalefactor, const int mFC=2, const NJetAmpTables& tables=amptables())
      : BaseClass(Vflav, scalefactor, mFC, tables)
    { }

  protected:
    using BaseClass::fvZero;
    using BaseClass::vhel;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp6q0gV2bStatic>();
    }

    void markZeroFv()
    {
      fvZero.reset();
      if (vhel[0] == vhel[1] || vhel[2] == vhel[3]) {
        fvZero[0] = true;
      }
      if (vhel[0] == vhel[3] || vhel[2] == vhel[1]) {
        fvZero[1] = true;
      }
    }
};

class Amp6q0gV6bStatic : public Amp6q0gV2bStatic
{
  public:
    static const int HS = 10;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp6q0gV6b : public Amp6q0gV2b<T>
{
    typedef Amp6q0gV2b<T> BaseClass;
  public:

    Amp6q0gV6b(const T scalefactor, const NJetAmpTables& tables=amptables())
      : BaseClass(scalefactor, 6, tables)
    { }

    Amp6q0gV6b(const Flavour<double>& Vflav,
              const T scalefactor, const NJetAmpTables& tables=amptables())
      : BaseClass(Vflav, scalefactor, 6, tables)
    { }

  protected:
    using BaseClass::fvZero;
    using BaseClass::vhel;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp6q0gV6bStatic>();
    }

    void markZeroFv()
    {
      fvZero.reset();
      if (vhel[0] == vhel[1]) {
        fvZero[0] = true;
        fvZero[5] = true;
      }
      if (vhel[0] == vhel[3]) {
        fvZero[1] = true;
        fvZero[2] = true;
      }
      if (vhel[0] == vhel[5]) {
        fvZero[4] = true;
        fvZero[3] = true;
      }

      if (vhel[2] == vhel[3]) {
        fvZero[0] = true;
        fvZero[3] = true;
      }
      if (vhel[2] == vhel[5]) {
        fvZero[2] = true;
        fvZero[5] = true;
      }
      if (vhel[2] == vhel[1]) {
        fvZero[4] = true;
        fvZero[1] = true;
      }

      if (vhel[4] == vhel[5]) {
        fvZero[0] = true;
        fvZero[1] = true;
      }
      if (vhel[4] == vhel[1]) {
        fvZero[2] = true;
        fvZero[3] = true;
      }
      if (vhel[4] == vhel[3]) {
        fvZero[4] = true;
        fvZero[5] = true;
      }
    }
};

// both

class Amp6q0gV4nbStatic : public Amp6q0gVStatic
{
  public:
    static const int HS = 9;
    static const int HSarr[HS][HSNN];

    static const int FC = 4;
    static const int flav[FC][NN];
    static const int fperm[FC][NN];
    static const int fvcol[FC][CC];
    static const int fvsign[FC];
};

template <typename T>
class Amp6q0gV4nb : public Amp6q0gV<T>
{
    typedef Amp6q0gV<T> BaseClass;
  public:

    Amp6q0gV4nb(const T scalefactor, const NJetAmpTables& tables=amptables())
      : BaseClass(scalefactor, 4, tables)
    { }

    Amp6q0gV4nb(const Flavour<double>& Vflav,
              const T scalefactor, const NJetAmpTables& tables=amptables())
      : BaseClass(Vflav, scalefactor, 4, tables)
    { }

  protected:
    using BaseClass::fvZero;
    using BaseClass::vhel;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp6q0gV4nbStatic>();
    }

    void markZeroFv()
    {
      fvZero.reset();
      if (vhel[0] == vhel[1] || vhel[2] == vhel[3] || vhel[4] == vhel[5]) {
        fvZero[0] = true;
      }
      if (vhel[0] == vhel[3] || vhel[2] == vhel[1] || vhel[4] == vhel[5]) {
        fvZero[1] = true;
      }
      if (vhel[4] == vhel[1] || vhel[2] == vhel[3] || vhel[0] == vhel[5]) {
        fvZero[2] = true;
      }
      if (vhel[4] == vhel[3] || vhel[2] == vhel[1] || vhel[0] == vhel[5]) {
        fvZero[3] = true;
      }
    }
};

// Z amplitudes

class Amp6q0gZStatic : public Amp6q0gVStatic
{
  public:
    static const int FC = 18;

    static const int flav[FC][NN];  // flavour vectors for ngluon
    static const int fvsign[FC];    // flavour channels' signs
    static const int fperm[FC][NN];        // flavour pemutation matrix
    static const int fvcol[FC][CC];        // flavour-partial pemutation matrix

    static const int HS = 16;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp6q0gZ : public Amp6q0gV<T>
{
    typedef Amp6q0gV<T> BaseClass;
  public:

    Amp6q0gZ(const Flavour<double>& ff, const T scalefactor,
             const int mFC=3, const NJetAmpTables& tables=amptables())
    : BaseClass(ff, scalefactor, mFC, tables)
    { }

  protected:
    using BaseClass::fvZero;
    using BaseClass::vhel;
    using BaseClass::NN;
    using BaseClass::getFlav;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp6q0gZStatic>();
    }
};

template <typename T>
class Amp6q0gZd : public Amp6q0gZ<T>
{
    typedef Amp6q0gZ<T> BaseClass;
  public:

    Amp6q0gZd(const Flavour<double>& ff, const T scalefactor,
             const int mFC=3, const NJetAmpTables& tables=BaseClass::amptables());

  protected:
    using BaseClass::NN;
    using BaseClass::getFlav;
};

class Amp6q0gZ2Static : public Amp6q0gZStatic
{
  public:
    static const int HS = 24;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp6q0gZ2 : public Amp6q0gZ<T>
{
    typedef Amp6q0gZ<T> BaseClass;
  public:

    Amp6q0gZ2(const Flavour<double>& ff, const T scalefactor,
             const int mFC=6, const NJetAmpTables& tables=amptables())
    : BaseClass(ff, scalefactor, mFC, tables)
    { }

  protected:
    using BaseClass::fvZero;
    using BaseClass::vhel;
    using BaseClass::NN;
    using BaseClass::getFlav;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp6q0gZ2Static>();
    }

    void markZeroFv()
    {
      fvZero.reset();
      if (vhel[0] == vhel[1] || vhel[2] == vhel[3]) {
        fvZero[0] = true;
        fvZero[1] = true;
        fvZero[2] = true;
      }
      if (vhel[0] == vhel[3] || vhel[2] == vhel[1]) {
        fvZero[3] = true;
        fvZero[4] = true;
        fvZero[5] = true;
      }
    }
};

template <typename T>
class Amp6q0gZ2d : public Amp6q0gZ2<T>
{
    typedef Amp6q0gZ2<T> BaseClass;
  public:

    Amp6q0gZ2d(const Flavour<double>& ff, const T scalefactor,
               const int mFC=6, const NJetAmpTables& tables=BaseClass::amptables());

  protected:
    using BaseClass::NN;
    using BaseClass::getFlav;
};

class Amp6q0gZ6Static : public Amp6q0gZStatic
{
  public:
    static const int HS = 40;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp6q0gZ6 : public Amp6q0gZ<T>
{
    typedef Amp6q0gZ<T> BaseClass;
  public:

    Amp6q0gZ6(const Flavour<double>& ff, const T scalefactor,
             const int mFC=18, const NJetAmpTables& tables=amptables())
    : BaseClass(ff, scalefactor, mFC, tables)
    { }

  protected:
    using BaseClass::fvZero;
    using BaseClass::vhel;
    using BaseClass::NN;
    using BaseClass::getFlav;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp6q0gZ6Static>();
    }

    void markZeroFv()
    {
      fvZero.reset();
      if (vhel[0] == vhel[1]) {
        fvZero[0] = true;
        fvZero[1] = true;
        fvZero[2] = true;
        fvZero[15] = true;
        fvZero[16] = true;
        fvZero[17] = true;
      }
      if (vhel[0] == vhel[3]) {
        fvZero[3] = true;
        fvZero[4] = true;
        fvZero[5] = true;
        fvZero[12] = true;
        fvZero[13] = true;
        fvZero[14] = true;
      }
      if (vhel[0] == vhel[5]) {
        fvZero[6] = true;
        fvZero[7] = true;
        fvZero[8] = true;
        fvZero[9] = true;
        fvZero[10] = true;
        fvZero[11] = true;
      }
      if (vhel[2] == vhel[3]) {
        fvZero[0] = true;
        fvZero[1] = true;
        fvZero[2] = true;
        fvZero[9] = true;
        fvZero[10] = true;
        fvZero[11] = true;
      }
      if (vhel[2] == vhel[5]) {
        fvZero[12] = true;
        fvZero[13] = true;
        fvZero[14] = true;
        fvZero[15] = true;
        fvZero[16] = true;
        fvZero[17] = true;
      }
      if (vhel[2] == vhel[1]) {
        fvZero[3] = true;
        fvZero[4] = true;
        fvZero[5] = true;
        fvZero[6] = true;
        fvZero[7] = true;
        fvZero[8] = true;
      }
      if (vhel[4] == vhel[5]) {
        fvZero[0] = true;
        fvZero[1] = true;
        fvZero[2] = true;
        fvZero[3] = true;
        fvZero[4] = true;
        fvZero[5] = true;
      }
      if (vhel[4] == vhel[1]) {
        fvZero[9] = true;
        fvZero[10] = true;
        fvZero[11] = true;
        fvZero[12] = true;
        fvZero[13] = true;
        fvZero[14] = true;
      }
      if (vhel[4] == vhel[3]) {
        fvZero[6] = true;
        fvZero[7] = true;
        fvZero[8] = true;
        fvZero[15] = true;
        fvZero[16] = true;
        fvZero[17] = true;
      }
    }
};

#endif // CHSUM_6Q0G_H
