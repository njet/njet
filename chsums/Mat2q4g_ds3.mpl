with(CodeGeneration);

ex:=[
  matds0   = simplify(0),
  matds1   = simplify(6*Nc^5),
  matds2   = simplify(12*Nc^5),
  matds3   = simplify(12*Nc^3),
  matds4   = simplify(6*Nc^3),
  matds5   = simplify(6*(Nc+Nc^3)),
  matds6   = simplify(6*Nc),
  matds7   = simplify(-6*Nc^5),
  matds8   = simplify(-6*Nc*(-1-2*Nc^2+Nc^4)),
  matds9   = simplify(-6*(-2+Nc^2)*Nc^3),
  matds10  = simplify(6/Nc+24*Nc-6*Nc^3),
  matds11  = simplify(6/Nc+12*Nc-6*Nc^3),
  matds12  = simplify(-6*Nc*V),
  matds13  = simplify(6*(Nc^3+Nc^5)),
  matds14  = simplify(6/Nc+30*Nc-12*Nc^3),
  matds15  = simplify(6/Nc+6*Nc),
  matds16  = simplify(6*(2+Nc^2)*Nc^3),
  matds17  = simplify(6*Nc*V*V),
  matds18  = simplify((6*(1+3*Nc^2-3*Nc^4+Nc^6))/Nc),
  matds19  = simplify(-6*Nc^3*V),
  matds20  = simplify(-12*Nc^3*V),
  matds21  = simplify(6*(Nc+3*Nc^3)),
  matds22  = simplify(6/Nc+18*Nc-6*Nc^3),
  matds23  = simplify(6/Nc+18*Nc),
  matds24  = simplify(18*Nc^5),
  matds25  = simplify(6-3*Nc^2),
  matds26  = simplify((6*(1+(-2+Nc^2)*(-2+Nc^2)*Nc^2))/Nc),
  matds27  = simplify(-12*(-2+Nc^2)*Nc^3),
  matds28  = simplify((6*(1+6*Nc^2+Nc^4))/Nc),
  matds29  = simplify((6*(1+Nc^2)*(1+Nc^2))/Nc),
  matds30  = simplify(6-Nc^2),
  matds31  = simplify(6*Nc^2*Nc^5),
  matds32  = simplify(-12*Nc^5),
  matds33  = simplify(24*Nc^3),
  matds34  = simplify(6/Nc),
  matds35  = simplify(3*(2-3*Nc^2+Nc^4)),
  matds36  = simplify(12+Nc^2+Nc^4),
  matds37  = simplify(6-3*Nc^2+Nc^4),
  matds38  = simplify(12-5*Nc^2+Nc^4),
  matds39  = simplify(6-7*Nc^2+Nc^4),
  matds40  = simplify(-((-2+Nc^2)*(3+Nc^2))),
  matds41  = simplify(6+Nc^2),
  matds42  = simplify(-6+Nc^2),
  matds43  = simplify(-((-3+Nc^2)*(12-Nc^2+Nc^4))),
  matds44  = simplify(6-11*Nc^2+7*Nc^4-2*Nc^6),
  matds45  = simplify(36-45*Nc^2+13*Nc^4),
  matds46  = simplify(36-39*Nc^2+9*Nc^4),
  matds47  = simplify(6-5*Nc^2+2*Nc^4),
  matds48  = simplify(6+Nc^2+Nc^4),
  matds49  = simplify(6-5*Nc^2+Nc^4),
  matds50  = simplify(12-9*Nc^2+Nc^4),
  matds51  = simplify(36-21*Nc^2+Nc^4),
  matds52  = simplify(6-Nc^2*(5+Nc^2)),
  matds53  = simplify(36+3*Nc^2-2*Nc^4),
  matds54  = simplify(6+3*Nc^2-2*Nc^4),
  matds55  = simplify(6+Nc^2-2*Nc^4),
  matds56  = simplify(6-Nc^2-2*Nc^4),
  matds57  = simplify(-3*(-12+5*Nc^2+Nc^4)),
  matds58  = simplify(3*(-2+Nc^2)),
  matds59  = simplify(-6+11*Nc^2-7*Nc^4+2*Nc^6),
  matds60  = simplify(12-19*Nc^2+6*Nc^4+Nc^6),
  matds61  = simplify(12-15*Nc^2+2*Nc^4+Nc^6),
  matds62  = simplify(12+5*Nc^2-6*Nc^4+Nc^6),
  matds63  = simplify(12+31*Nc^2-8*Nc^4+Nc^6),
  matds64  = simplify(12+9*Nc^2-10*Nc^4+Nc^6),
  matds65  = simplify(12-Nc^2+Nc^6),
  matds66  = simplify(12-13*Nc^2+Nc^6),
  matds67  = simplify(36-47*Nc^2+12*Nc^4-Nc^6),
  matds68  = simplify(36-27*Nc^2+10*Nc^4-Nc^6),
  matds69  = simplify(-((-2+Nc)*(2+Nc)*(3-6*Nc^2+Nc^4))),
  matds70  = simplify(-((-4+Nc^2)*(-3+Nc^2)*(-3+Nc^2))),
  matds71  = simplify(-((-1+Nc)*(1+Nc)*(36-9*Nc^2+Nc^4))),
  matds72  = simplify(12-5*Nc^2+6*Nc^4-Nc^6),
  matds73  = simplify(12-17*Nc^2+6*Nc^4-Nc^6),
  matds74  = simplify(-((-2+Nc)*(2+Nc)*(9+Nc^4))),
  matds75  = simplify(36-21*Nc^2+4*Nc^4-Nc^6),
  matds76  = simplify(36-13*Nc^2+2*Nc^4-Nc^6),
  matds77  = simplify(36-25*Nc^2+2*Nc^4-Nc^6),
  matds78  = simplify(36-Nc^2*(3+2*Nc^2+Nc^4)),
  matds79  = simplify(12-Nc^2*(3+2*Nc^2+Nc^4)),
  matds80  = simplify(36-Nc^2*(23+Nc^4)),
  matds81  = simplify(18-33*Nc^2+11*Nc^4-2*Nc^6),
  matds82  = simplify(18-25*Nc^2+9*Nc^4-2*Nc^6),
  matds83  = simplify(18-Nc^2+3*Nc^4-2*Nc^6),
  matds84  = simplify(12-29*Nc^2+16*Nc^4-5*Nc^6),
  matds85  = simplify(36-59*Nc^2+27*Nc^4-6*Nc^6),
  matds86  = simplify(12-51*Nc^2+26*Nc^4-6*Nc^6),
  matds87  = simplify(36-61*Nc^2+25*Nc^4-6*Nc^6),
  matds88  = simplify(12-29*Nc^2+10*Nc^4),
  matds89  = simplify(12-25*Nc^2+8*Nc^4),
  matds90  = simplify(18-31*Nc^2+8*Nc^4),
  matds91  = simplify(36-37*Nc^2+8*Nc^4),
  matds92  = simplify(12-19*Nc^2+7*Nc^4),
  matds93  = simplify(36-27*Nc^2+7*Nc^4),
  matds94  = simplify(36-39*Nc^2+7*Nc^4),
  matds95  = simplify(18-29*Nc^2+6*Nc^4),
  matds96  = simplify(12-9*Nc^2+5*Nc^4),
  matds97  = simplify(6-17*Nc^2+5*Nc^4),
  matds98  = simplify((-3+Nc^2)*(-4+5*Nc^2)),
  matds99  = simplify(18-23*Nc^2+5*Nc^4),
  matds100 = simplify(36+5*(-5+Nc^2)*Nc^2),
  matds101 = simplify(36-9*Nc^2+4*Nc^4),
  matds102 = simplify(-6-11*Nc^2+4*Nc^4),
  matds103 = simplify(36-15*Nc^2+4*Nc^4),
  matds104 = simplify(18-15*Nc^2+4*Nc^4),
  matds105 = simplify(12-17*Nc^2+4*Nc^4),
  matds106 = simplify(18-25*Nc^2+4*Nc^4),
  matds107 = simplify(36-27*Nc^2+4*Nc^4),
  matds108 = simplify(36+Nc^2+3*Nc^4),
  matds109 = simplify(18-5*Nc^2+3*Nc^4),
  matds110 = simplify(6-11*Nc^2+3*Nc^4),
  matds111 = simplify(3*(6-5*Nc^2+Nc^4)),
  matds112 = simplify(3*(12-7*Nc^2+Nc^4)),
  matds113 = simplify(3*(12-11*Nc^2+Nc^4)),
  matds114 = simplify(12+9*Nc^2+2*Nc^4),
  matds115 = simplify(-6+Nc^2+2*Nc^4),
  matds116 = simplify((-2+Nc^2)*(3+2*Nc^2)),
  matds117 = simplify(12-3*Nc^2+2*Nc^4),
  matds118 = simplify(-6-5*Nc^2+2*Nc^4),
  matds119 = simplify(18-7*Nc^2+2*Nc^4),
  matds120 = simplify(12-7*Nc^2+2*Nc^4),
  matds121 = simplify(6-7*Nc^2+2*Nc^4),
  matds122 = simplify(6-9*Nc^2+2*Nc^4),
  matds123 = simplify(12-15*Nc^2+2*Nc^4),
  matds124 = simplify(12-19*Nc^2+2*Nc^4),
  matds125 = simplify(18-23*Nc^2+2*Nc^4),
  matds126 = simplify((3+Nc^2)*(4+Nc^2)),
  matds127 = simplify(-6+5*Nc^2+Nc^4),
  matds128 = simplify(18+3*Nc^2+Nc^4),
  matds129 = simplify(12+3*Nc^2+Nc^4),
  matds130 = simplify(-6+Nc^2+Nc^4),
  matds131 = simplify(36-Nc^2+Nc^4),
  matds132 = simplify(36-3*Nc^2+Nc^4),
  matds133 = simplify((-6+Nc^2)*(1+Nc^2)),
  matds134 = simplify(12-11*Nc^2+Nc^4),
  matds135 = simplify(36-15*Nc^2+Nc^4),
  matds136 = simplify(18-15*Nc^2+Nc^4),
  matds137 = simplify(-6+9*Nc^2-Nc^4),
  matds138 = simplify(-6+7*Nc^2-Nc^4),
  matds139 = simplify(12+5*Nc^2-Nc^4),
  matds140 = simplify(-6+5*Nc^2-Nc^4),
  matds141 = simplify(-6+3*Nc^2-Nc^4),
  matds142 = simplify(18-Nc^2*V),
  matds143 = simplify(6-Nc^2*V),
  matds144 = simplify(-6-Nc^2*V),
  matds145 = simplify(-6-Nc^2*(1+Nc^2)),
  matds146 = simplify(-((-2+Nc^2)*(9+Nc^2))),
  matds147 = simplify(12-Nc^2*(7+Nc^2)),
  matds148 = simplify(36-Nc^2*(11+Nc^2)),
  matds149 = simplify(36+9*Nc^2-2*Nc^4),
  matds150 = simplify(6+7*Nc^2-2*Nc^4),
  matds151 = simplify(-6+7*Nc^2-2*Nc^4),
  matds152 = simplify(6+5*Nc^2-2*Nc^4),
  matds153 = simplify(18+3*Nc^2-2*Nc^4),
  matds154 = simplify(18-Nc^2-2*Nc^4),
  matds155 = simplify(36-3*Nc^2-2*Nc^4),
  matds156 = simplify(12-5*Nc^2-2*Nc^4),
  matds157 = simplify(18+19*Nc^2-3*Nc^4),
  matds158 = simplify(-3*(2-5*Nc^2+Nc^4)),
  matds159 = simplify(-3*(2-3*Nc^2+Nc^4)),
  matds160 = simplify(6+7*Nc^2-3*Nc^4),
  matds161 = simplify(12+5*Nc^2-3*Nc^4),
  matds162 = simplify(36+3*Nc^2-3*Nc^4),
  matds163 = simplify(3*(4-Nc^2*V)),
  matds164 = simplify(12-7*Nc^2-3*Nc^4),
  matds165 = simplify(-3*(-12+3*Nc^2+Nc^4)),
  matds166 = simplify(-3*(-4+3*Nc^2+Nc^4)),
  matds167 = simplify(-6+13*Nc^2-4*Nc^4),
  matds168 = simplify(18+Nc^2-4*Nc^4),
  matds169 = simplify(12-Nc^2-4*Nc^4),
  matds170 = simplify(36-13*Nc^2-4*Nc^4),
  matds171 = simplify(12+5*Nc^2-5*Nc^4),
  matds172 = simplify(36+13*Nc^2-6*Nc^4),
  matds173 = simplify(18+9*Nc^2-6*Nc^4),
  matds174 = simplify(36+Nc^2-6*Nc^4),
  matds175 = simplify(18+17*Nc^2-7*Nc^4),
  matds176 = simplify(12+15*Nc^2-7*Nc^4),
  matds177 = simplify(12+5*Nc^2-7*Nc^4),
  matds178 = simplify(36-Nc^2-7*Nc^4),
  matds179 = simplify(36+15*Nc^2-8*Nc^4),
  matds180 = simplify(12+7*Nc^2-8*Nc^4),
  matds181 = simplify(12+15*Nc^2-11*Nc^4),
  matds182 = simplify(36+13*Nc^2-13*Nc^4),
  matds183 = simplify(-6-Nc^2),
  matds184 = simplify(18-5*Nc^2),
  matds185 = simplify(-9*(-2+Nc^2))
];

exopt:=[codegen[optimize](ex,tryhard)];

codegen[C](exopt,optimized,filename="Mat2q4g_ds3.c");


codegen[cost](ex);
codegen[cost](exopt);
%%-%;

