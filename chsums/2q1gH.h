/*
* chsums/2q1gA.h
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef CHSUM_2Q1GH_H
#define CHSUM_2Q1GH_H

#include "2q1gV.h"

class Amp2q1gHStatic : public Amp2q1gVStatic
{
  public:
    static const int HS = 4;
    static const int HSNN = NN+1;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp2q1gH : public Amp2q1gV<T>
{
    typedef Amp2q1gV<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp2q1gH(const T scalefactor, const int mFC=1, const NJetAmpTables& tables=amptables());

    void born_part_fill() { BaseClass::born_part_trickfill(); }
    LoopValue virt() { return BaseClass::virt_tricksum(); }

  protected:
    using BaseClass::ngluons;
    using BaseClass::NN;
    using BaseClass::getFlav;
    using BaseClass::getFperm;
    using BaseClass::mfv;
    using BaseClass::fvSet;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp2q1gHStatic>();
    }

    virtual TreeValue A0(int p0, int p1, int p2);
    virtual LoopResult<T> AL(int /*p0*/, int /*p1*/, int /*p2*/) { return LoopResult<T>(); }
    virtual LoopResult<T> AF(int /*p0*/, int /*p1*/, int /*p2*/) { return LoopResult<T>(); }
};

#endif // CHSUM_2Q1GH_H
