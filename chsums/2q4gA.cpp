/*
* chsums/2q4gA.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012, 2013 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include "2q4gA.h"
#include "NJetAmp-T.h"

// class Amp2q4gAA

template <typename T>
Amp2q4gAA<T>::Amp2q4gAA(const Flavour<double>& ff, const T scalefactor,
                        const int mFC, const NJetAmpTables& tables)
  : BaseClass(scalefactor, mFC, tables)
{
  initProcess(ff);
}

template <typename T>
void Amp2q4gAA<T>::initProcess(const Flavour<double>& ff)
{
  for (int fv=0; fv<BaseClass::mFC; fv++) {
    Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
    flavours.push_back(ff);
    flavours.push_back(ff);
    BaseClass::setProcess(flavours, fv);
  }
}

template <typename T>
typename Amp2q4gAA<T>::TreeValue
Amp2q4gAA<T>::A0(int p0, int p1, int p2, int p3, int p4, int p5)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, NN+1, O[p1], O[p2], O[p3], O[p4], O[p5]};
  return BaseClass::A0nAA(order);
}

template <typename T>
LoopResult<T> Amp2q4gAA<T>::AF(int p0, int p1, int p2, int p3, int p4, int p5)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, NN+1, O[p1], O[p2], O[p3], O[p4], O[p5]};
  return BaseClass::AFnAA(order);
}

template <typename T>
LoopResult<T> Amp2q4gAA<T>::AL(int p0, int p1, int p2, int p3, int p4, int p5)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, NN+1, O[p1], O[p2], O[p3], O[p4], O[p5]};
  return BaseClass::ALnAA(order);
}

#ifdef USE_SD
  template class Amp2q4gAA<double>;
#endif
#ifdef USE_DD
  template class Amp2q4gAA<dd_real>;
#endif
#ifdef USE_QD
  template class Amp2q4gAA<qd_real>;
#endif
#ifdef USE_VC
  template class Amp2q4gAA<Vc::double_v>;
#endif
