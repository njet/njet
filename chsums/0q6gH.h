/*
* chsums/0q6gH.h
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef CHSUM_0Q6GH_H
#define CHSUM_0Q6GH_H

#include "0q6g.h"

class Amp0q6gHStatic : public Amp0q6gStatic
{
  public:
    static const int HS = 64;
    static const int HSNN = NN+1;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp0q6gH : public Amp0q6g<T>
{
    typedef Amp0q6g<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp0q6gH(const T scalefactor, const int mFC=1, const NJetAmpTables& tables=amptables());

    void born_part_fill() { BaseClass::born_part_trickfill(); }

  protected:
    using BaseClass::ngluons;
    using BaseClass::NN;
    using BaseClass::getFlav;
    using BaseClass::getFperm;
    using BaseClass::mfv;
    using BaseClass::fvSet;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp0q6gHStatic>();
    }

    void initProcess(const Flavour<double>& ff);

    virtual TreeValue A0(int p0, int p1, int p2, int p3, int p4, int p5);
    virtual LoopResult<T> AL(int /*p0*/, int /*p1*/, int /*p2*/, int /*p3*/,
                             int /*p4*/, int /*p5*/, int /*pos*/) { return LoopResult<T>(); }
    virtual LoopResult<T> AF(int /*p0*/, int /*p1*/, int /*p2*/, int /*p3*/,
                             int /*p4*/, int /*p5*/, int /*pos*/) { return LoopResult<T>(); }
};

#endif // CHSUM_0Q6GH_H
