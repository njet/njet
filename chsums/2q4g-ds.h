/*
* chsums/2q4g_ds.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef CHSUM_2Q4G_DS_H
#define CHSUM_2Q4G_DS_H

#include "2q4g.h"

// Amp2q4g_ds4

class Amp2q4g_ds4Static : public virtual Amp2q4gStatic
{
  public:
    static const int CDS = 5 + 2;
    static const unsigned int colmatds[CDS][C0];
    static const int NmatDSLen = 74;
};

template <typename T>
class Amp2q4g_ds4 : public Amp2q4g<T>
{
    typedef Amp2q4g<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;
    typedef typename BaseClass::ST ST;

    Amp2q4g_ds4(const T scalefactor,
                const int mFC=1, const NJetAmpTables& tables=amptables());

    void setNc(const ST Nc_);

    LoopValue virt() { return BaseClass::virt_dstricksum(); }
    LoopValue virt(const int* h) { return BaseClass::virtds(h); }

  protected:
    using BaseClass::V;
    using BaseClass::Nc;
    using BaseClass::Nc2;
    using BaseClass::Nc3;
    using BaseClass::Nc4;
    using BaseClass::Nc5;
    using BaseClass::Nf;
    using BaseClass::AL;
    using BaseClass::AF;
    using BaseClass::NmatDS;
    using BaseClass::NmatDSLen;
    using BaseClass::fvSet;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp2q4g_ds4Static>();
    }

    void initNc();

    bool setLoopType(int type, int norderL_, int norderF_)
    {
      return BaseClass::setLoopTypeNONE(type, norderL_, norderF_);
    }

    void getfvpart1ds(const int fv, LoopValue* fvpart);
    void getfvpart1ds(const int fv, LoopResult<T>* fvpart);

    template <typename LT>
    void getfvpart1ds_(const int fv, LT* fvpart);
};

// Amp2q4g_ds3

class Amp2q4g_ds3Static : public virtual Amp2q4gStatic
{
  public:
    static const int CDS = 20 + 8;
    static const unsigned int colmatds[CDS][C0];
    static const int NmatDSLen = 186;
};

template <typename T>
class Amp2q4g_ds3 : public Amp2q4g<T>
{
    typedef Amp2q4g<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;
    typedef typename BaseClass::ST ST;

    Amp2q4g_ds3(const T scalefactor,
                const int mFC=1, const NJetAmpTables& tables=amptables());

    void setNc(const ST Nc_);

    LoopValue virt() { return BaseClass::virt_dstricksum(); }
    LoopValue virt(const int* h) { return BaseClass::virtds(h); }

  protected:
    using BaseClass::V;
    using BaseClass::Nc;
    using BaseClass::Nc2;
    using BaseClass::Nc3;
    using BaseClass::Nc4;
    using BaseClass::Nc5;
    using BaseClass::Nf;
    using BaseClass::AL;
    using BaseClass::AF;
    using BaseClass::NmatDS;
    using BaseClass::NmatDSLen;
    using BaseClass::fvSet;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp2q4g_ds3Static>();
    }

    void initNc();

    bool setLoopType(int type, int norderL_, int norderF_)
    {
      return BaseClass::setLoopTypeNONE(type, norderL_, norderF_);
    }

    void getfvpart1ds(const int fv, LoopValue* fvpart);
    void getfvpart1ds(const int fv, LoopResult<T>* fvpart);

    template <typename LT>
    void getfvpart1ds_(const int fv, LT* fvpart);
};

#endif // CHSUM_2Q4G_DS_H
