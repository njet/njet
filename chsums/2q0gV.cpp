/*
* chsums/2q0gV.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>

#include "2q0gV.h"
#include "../ngluon2/NGluon2.h"

// class Amp2q0gV

template <typename T>
Amp2q0gV<T>::Amp2q0gV(const T scalefactor,
                      const int mFC, const NJetAmpTables& tables)
  : BaseClass(mFC, tables)
{
  for (int fv=0; fv<mFC; fv++) {
    BaseClass::initNG(scalefactor, fv);
  }

  const Flavour<double> ff = StandardModel::Wp(StandardModel::u(), StandardModel::ubar(), 1.);  // ckm = 1
  initProcess(ff);
  initNc();
}

template <typename T>
Amp2q0gV<T>::Amp2q0gV(const Flavour<double>& ff, const T scalefactor,
                      const int mFC, const NJetAmpTables& tables)
  : BaseClass(mFC, tables)
{
  for (int fv=0; fv<mFC; fv++) {
    BaseClass::initNG(scalefactor, fv);
  }

  initProcess(ff);
  initNc();
}

template <typename T>
void Amp2q0gV<T>::initProcess(const Flavour<double>& ff)
{
  for (int fv=0; fv<BaseClass::mFC; fv++) {
    Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
    flavours.push_back(ff);
    BaseClass::setProcess(flavours, fv);
  }
}

template <typename T>
void Amp2q0gV<T>::setNc(const ST Nc_)
{
  BaseClass::setNc(Nc_);
  initNc();
}

template <typename T>
void Amp2q0gV<T>::initNc()
{
  Nmat[0] = 1.;
  assert(0 < NJetAmp<T>::NmatLen);

  Nmatcc[0] = 0.;
  Nmatcc[1] = 1.;
  assert(1 < NJetAmp<T>::NmatccLen);

  bornFactor = Nc;
  loopFactor = 2.*bornFactor;
  bornccFactor = 0.5*V;
}

template <typename T>
typename Amp2q0gV<T>::TreeValue
Amp2q0gV<T>::A0(int p0, int p1)
{
  const int* O = getFperm(mfv);
  const int order[] = {O[p0], NN, O[p1]};
  return ngluons[mfv]->evalTree(order);
}

template <typename T>
LoopResult<T> Amp2q0gV<T>::AL(int p0, int p1)
{
  const int* O = getFperm(mfv);
  const int order[] = {O[p0], NN, O[p1]};
  return ngluons[mfv]->eval(NGluon2<T>::MIXED, order);
}

template <typename T>
void Amp2q0gV<T>::getfvpart1_full(const int fv, LoopValue* fvpart)
{
  return getfvpart1_full_(fv, fvpart);
}

template <typename T>
void Amp2q0gV<T>::getfvpart1_full(const int fv, LoopResult<T>* fvpart)
{
  return getfvpart1_full_(fv, fvpart);
}

// -------- BEGIN -- automatically generated code -- BEGIN -------- //

template <typename T>
void Amp2q0gV<T>::getfvpart0(const int fv, TreeValue* fvpart, TreeValue* fvpartC)
{
  fvSet(fv);
  fvpart[0] = A0(0,1);

  if (fvpartC) {
    for (int i=0; i<C0; i++) {
      fvpartC[i] = conj(fvpart[i]);
    }
  }
}

template <typename T>
template <typename LT>
inline
void Amp2q0gV<T>::getfvpart1_full_(const int fv, LT* fvpart)
{
  fvSet(fv);
  {
    // 1 primitives
    const LT P12 = AL(0,1);

    fvpart[0] = V/Nc*P12;
  }
  if (Nf != 0.) {
    // 0 primitives
  }
}

// --------- END --- automatically generated code --- END --------- //

#ifdef USE_SD
  template class Amp2q0gV<double>;
#endif
#ifdef USE_DD
  template class Amp2q0gV<dd_real>;
#endif
#ifdef USE_QD
  template class Amp2q0gV<qd_real>;
#endif
#ifdef USE_VC
  template class Amp2q0gV<Vc::double_v>;
#endif
