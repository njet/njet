/*
* chsums/6q0gH-static.cpp
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include "6q0gH.h"

// class Amp6q0gHStatic

const int
Amp6q0gHStatic::HSarr[HS][HSNN] = {
  { 1,-1, 1,-1, 1,-1, 1},
  { 1,-1, 1,-1,-1, 1, 1},
  { 1,-1,-1, 1, 1,-1, 1},
  { 1,-1,-1, 1,-1, 1, 1},
  {-1, 1,-1, 1,-1, 1, 1},
  {-1, 1,-1, 1, 1,-1, 1},
  {-1, 1, 1,-1,-1, 1, 1},
  {-1, 1, 1,-1, 1,-1, 1}
};

// class Amp6q0gH2Static

const int
Amp6q0gH2Static::HSarr[HS][HSNN] = {
  { 1,-1, 1, 1,-1,-1, 1},
  { 1,-1, 1,-1, 1,-1, 1},
  { 1,-1, 1,-1,-1, 1, 1},
  { 1,-1,-1, 1, 1,-1, 1},
  { 1,-1,-1, 1,-1, 1, 1},
  { 1,-1,-1,-1, 1, 1, 1},
  {-1, 1,-1,-1, 1, 1, 1},
  {-1, 1,-1, 1,-1, 1, 1},
  {-1, 1,-1, 1, 1,-1, 1},
  {-1, 1, 1,-1,-1, 1, 1},
  {-1, 1, 1,-1, 1,-1, 1},
  {-1, 1, 1, 1,-1,-1, 1},
};

// class Amp6q0gH2Static

const int
Amp6q0gH6Static::HSarr[HS][HSNN] = {
  { 1, 1, 1,-1,-1,-1, 1},
  { 1, 1,-1, 1,-1,-1, 1},
  { 1, 1,-1,-1, 1,-1, 1},
  { 1, 1,-1,-1,-1, 1, 1},
  { 1,-1, 1, 1,-1,-1, 1},
  { 1,-1, 1,-1, 1,-1, 1},
  { 1,-1, 1,-1,-1, 1, 1},
  { 1,-1,-1, 1, 1,-1, 1},
  { 1,-1,-1, 1,-1, 1, 1},
  { 1,-1,-1,-1, 1, 1, 1},
  {-1,-1,-1, 1, 1, 1, 1},
  {-1,-1, 1,-1, 1, 1, 1},
  {-1,-1, 1, 1,-1, 1, 1},
  {-1,-1, 1, 1, 1,-1, 1},
  {-1, 1,-1,-1, 1, 1, 1},
  {-1, 1,-1, 1,-1, 1, 1},
  {-1, 1,-1, 1, 1,-1, 1},
  {-1, 1, 1,-1,-1, 1, 1},
  {-1, 1, 1,-1, 1,-1, 1},
  {-1, 1, 1, 1,-1,-1, 1},
};
