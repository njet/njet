/*
* chsums/2q1gV-static.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>

#include "2q1gV.h"

// class Amp2q1gVStatic

const int
Amp2q1gVStatic::flav[FC][NN] = {
  {-1,1,0}
};

const int
Amp2q1gVStatic::fvsign[FC] = {1};

const int
Amp2q1gVStatic::ccsign[NN*(NN+1)/2] = {1,1,1,1,1,1};

const int
Amp2q1gVStatic::fperm[FC][NN] = {
    {0,1,2}
};

const int
Amp2q1gVStatic::fvcol[FC][CC] = {
    {0}
};

const unsigned char
Amp2q1gVStatic::colmat[CC*(CC+1)/2] = {
  0
};

const unsigned char
Amp2q1gVStatic::colmatcc[NN*(NN+1)/2][C0*(C0+1)/2] = {
    {0},
    {1},
    {0},
    {2},
    {2},
    {0}
};

const int
Amp2q1gVStatic::HSarr[HS][HSNN] = {
  { 1,-1,-1, 1},
  { 1,-1, 1, 1}
};

// class Amp2q1gZStatic

const int
Amp2q1gZStatic::HSarr[HS][HSNN] = {
  { 1,-1,-1,-1},
  { 1,-1, 1,-1},
  { 1,-1,-1, 1},
  { 1,-1, 1, 1},
  {-1, 1,-1,-1},
  {-1, 1, 1,-1},
  {-1, 1,-1, 1},
  {-1, 1, 1, 1}
};
