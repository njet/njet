/*
* chsums/2q1gA.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012, 2013 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef CHSUM_2Q1GA_H
#define CHSUM_2Q1GA_H

#include "2q1gV.h"

template <typename T>
class Amp2q1gA : public Amp2q1gZ<T>
{
    typedef Amp2q1gZ<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp2q1gA(const Flavour<double>& ff, const T scalefactor,
             const int mFC=1, const NJetAmpTables& tables=BaseClass::amptables())
    : BaseClass(ff, scalefactor, mFC, tables)
    { }

    void born_part_fill() { BaseClass::born_part_trickfill(); }
    LoopValue virt() { return BaseClass::virt_tricksum(); }
};

// Two photons

class Amp2q1gAAStatic : public Amp2q1gVStatic
{
  public:
    static const int HS = 16;
    static const int HSNN = NN+2;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp2q1gAA : public Amp2q1gV<T>
{
    typedef Amp2q1gV<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp2q1gAA(const Flavour<double>& Vflav, const T scalefactor,
              const int mFC=1, const NJetAmpTables& tables=amptables());

  protected:
    using BaseClass::NN;
    using BaseClass::vhel;
    using BaseClass::getFlav;
    using BaseClass::getFperm;
    using BaseClass::mfv;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp2q1gAAStatic>();
    }

    void initProcess(const Flavour<double>& ff);

    TreeValue A0(int p0, int p1, int p2);
    LoopResult<T> AL(int p0, int p1, int p2);
    LoopResult<T> AF(int p0, int p1, int p2);
};

// Two photons vector loop

class Amp2q1gAAxStatic : public Amp2q1gAAStatic
{
  public:
    static const int FC = 3;

    static const int flav[FC][NN];  // flavour vectors for ngluon
    static const int fvsign[FC];    // flavour channels' signs
    static const int fperm[FC][NN];        // flavour pemutation matrix
    static const int fvcol[FC][CC];        // flavour-partial pemutation matrix
};

template <typename T>
class Amp2q1gAAx : public Amp2q1gAA<T>
{
    typedef Amp2q1gAA<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;
    typedef typename BaseClass::ST ST;

    Amp2q1gAAx(const Flavour<double>& ff, const T scalefactor,
               const int mFC=3, const NJetAmpTables& tables=amptables());

    void setNf(const ST Nf_);

  protected:
    using BaseClass::NN;
    using BaseClass::Nc;
    using BaseClass::Nf;
    using BaseClass::vhel;
    using BaseClass::getFlav;
    using BaseClass::getFperm;
    using BaseClass::fvSet;
    using BaseClass::mfv;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp2q1gAAxStatic>();
    }

    static const int modFC = 3;
    ST Nfx, Nfxx;

    LoopResult<T> AFx(int p0, int p1, int p2);
    LoopResult<T> AFxx(int p0, int p1, int p2);

    void getfvpart1_full(const int fv, LoopValue* fvpart);
    void getfvpart1_full(const int fv, LoopResult<T>* fvpart);

    template <typename LT>
    void getfvpart1_full_(const int fv, LT* fvpart);
};

#endif // CHSUM_2Q1GA_H
