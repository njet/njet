/*
* chsums/0q4gH-static.cpp
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>

#include "0q4gH.h"

// class Amp0q4gHStatic

const int
Amp0q4gHStatic::HSarr[HS][HSNN] = {
  { 1, 1, 1, 1, 1},
  { 1, 1, 1,-1, 1},
  { 1, 1,-1, 1, 1},
  { 1, 1,-1,-1, 1},
  {-1, 1, 1, 1, 1},
  {-1, 1, 1,-1, 1},
  {-1, 1,-1, 1, 1},
  {-1, 1,-1,-1, 1},
  {-1,-1,-1,-1, 1},
  {-1,-1,-1, 1, 1},
  {-1,-1, 1,-1, 1},
  {-1,-1, 1, 1, 1},
  { 1,-1,-1,-1, 1},
  { 1,-1,-1, 1, 1},
  { 1,-1, 1,-1, 1},
  { 1,-1, 1, 1, 1},
};
