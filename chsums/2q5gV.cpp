/*
* chsums/2q5gV.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include "2q5gV.h"
#include "NJetAmp-T.h"

// class Amp2q5gV

template <typename T>
Amp2q5gV<T>::Amp2q5gV(const T scalefactor,
                      const int mFC, const NJetAmpTables& tables)
  : BaseClass(scalefactor, mFC, tables)
{
  Flavour<double> ff = StandardModel::Wp(StandardModel::u(), StandardModel::ubar(), 1.);  // ckm = 1
  initProcess(ff);
}

template <typename T>
Amp2q5gV<T>::Amp2q5gV(const Flavour<double>& ff, const T scalefactor,
                      const int mFC, const NJetAmpTables& tables)
  : BaseClass(scalefactor, mFC, tables)
{
  initProcess(ff);
}

template <typename T>
void Amp2q5gV<T>::initProcess(const Flavour<double>& ff)
{
  for (int fv=0; fv<BaseClass::mFC; fv++) {
    Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
    flavours.push_back(ff);
    BaseClass::setProcess(flavours, fv);
  }
}

template <typename T>
typename Amp2q5gV<T>::TreeValue
Amp2q5gV<T>::A0(int p0, int p1, int p2, int p3, int p4, int p5, int p6)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, O[p1], O[p2], O[p3], O[p4], O[p5], O[p6]};
  return BaseClass::A0nVqq(order);
}

template <typename T>
LoopResult<T> Amp2q5gV<T>::AF(int p0, int p1, int p2, int p3, int p4, int p5, int p6)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, O[p1], O[p2], O[p3], O[p4], O[p5], O[p6]};
  return BaseClass::AFnVqq(order);
}

template <typename T>
LoopResult<T> Amp2q5gV<T>::AL(int p0, int p1, int p2, int p3, int p4, int p5, int p6)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, O[p1], O[p2], O[p3], O[p4], O[p5], O[p6]};
  return BaseClass::ALnVqq(order);
}

#ifdef USE_SD
  template class Amp2q5gV<double>;
#endif
#ifdef USE_DD
  template class Amp2q5gV<dd_real>;
#endif
#ifdef USE_QD
  template class Amp2q5gV<qd_real>;
#endif
#ifdef USE_VC
  template class Amp2q5gV<Vc::double_v>;
#endif
