/*
* chsums/4q0gH-static.cpp
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>

#include "4q0gH.h"

// class Amp4q0gHStatic

const int
Amp4q0gHStatic::HSarr[HS][HSNN] = {
  {-1, 1, 1,-1, 1},
  {-1, 1,-1, 1, 1},
  { 1,-1,-1, 1, 1},
  { 1,-1, 1,-1, 1},
};

// class Amp4q0gH2Static

const int
Amp4q0gH2Static::HSarr[HS][HSNN] = {
  { 1,-1, 1,-1, 1},
  { 1,-1,-1, 1, 1},
  { 1, 1,-1,-1, 1},
  {-1, 1,-1, 1, 1},
  {-1, 1, 1,-1, 1},
  {-1,-1, 1, 1, 1}
};
