/*
* chsums/0q4g.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>

#include "0q4g.h"
#include "../ngluon2/NGluon2.h"

// class Amp0q4g

template <typename T>
Amp0q4g<T>::Amp0q4g(const T scalefactor,
                    const int mFC, const NJetAmpTables& tables)
  : BaseClass(mFC, tables),
    Rcache(RcacheLen)
{
  for (int fv=0; fv<mFC; fv++) {
    BaseClass::initNG(scalefactor, fv);
    BaseClass::setProcess(StandardModel::NGluon1compat(NN, getFlav(fv)), fv);
  }

  initNc();
}

template <typename T>
void Amp0q4g<T>::setNc(const ST Nc_)
{
  BaseClass::setNc(Nc_);
  initNc();
}

template <typename T>
void Amp0q4g<T>::initNc()
{
  Nmat[0] = 2.;
  Nmat[1] = 4.;
  assert(1 < BaseClass::NmatLen);

  Nmatcc[0] = 0.;
  Nmatcc[1] = 1.;
  Nmatcc[2] = 2.;
  assert(2 < BaseClass::NmatccLen);

  NmatDS[0]  = 0.;
  NmatDS[1]  = 2.;
  NmatDS[2]  = -2.;
  assert(2 < BaseClass::NmatDSLen);

  bornFactor = Nc2*V;
  loopFactor = 2.*Nc2*V;
  bornccFactor = Nc3*V;
}

template <typename T>
LoopResult<T> Amp0q4g<T>::AF(int p0, int p1, int p2, int p3, int pos)
{
  const int* O = getFperm(mfv);
  const int order[] = {O[p0], O[p1], O[p2], O[p3]};
  LoopResult<T> ans = ngluons[mfv]->eval(NGluon2<T>::LIGHTQ, order, true);
  const LoopResult<T> rat = {Rcache[pos], conj(Rcache[pos])};
  ans += rat;
  return ans;
}

template <typename T>
LoopResult<T> Amp0q4g<T>::AL(int p0, int p1, int p2, int p3, int pos)
{
  const int* O = getFperm(mfv);
  const int order[] = {O[p0], O[p1], O[p2], O[p3]};
  const LoopResult<T> ans = ngluons[mfv]->eval(NGluon2<T>::MIXED, order);
  Rcache[pos] = ngluons[mfv]->lastValue().looprat();
  return ans;
}

template <typename T>
void Amp0q4g<T>::getfvpart0(const int fv, TreeValue* fvpart, TreeValue* fvpartC)
{
  fvSet(fv);
  fvpart[0] = A0(0,1,2,3);
  fvpart[1] = A0(0,1,3,2);

  if (fvpartC) {
    for (int i=0; i<C0; i++) {
      fvpartC[i] = conj(fvpart[i]);
    }
  }
}

template <typename T>
void Amp0q4g<T>::getfvpart1ds(const int fv, LoopValue* fvpart)
{
  return getfvpart1ds_(fv, fvpart);
}

template <typename T>
void Amp0q4g<T>::getfvpart1ds(const int fv, LoopResult<T>* fvpart)
{
  return getfvpart1ds_(fv, fvpart);
}

template <typename T>
template <typename LT>
inline
void Amp0q4g<T>::getfvpart1ds_(const int fv, LT* fvpart)
{
  fvSet(fv);
  fvpart[ 0] = Nc*AL(0,1,2,3, 0);
  fvpart[ 1] = Nc*AL(0,2,1,3, 1);
  fvpart[ 2] = Nc*AL(0,2,3,1, 2);

  if (Nf != 0.) {
    fvpart[3] = Nf*AF(0,1,2,3, 0);
    fvpart[4] = Nf*AF(0,2,1,3, 1);
    fvpart[5] = Nf*AF(0,2,3,1, 2);
  } else {
    for (int i=3; i<6; i++) {
      fvpart[i] = LT();
    }
  }
}

#ifdef USE_SD
  template class Amp0q4g<double>;
#endif
#ifdef USE_DD
  template class Amp0q4g<dd_real>;
#endif
#ifdef USE_QD
  template class Amp0q4g<qd_real>;
#endif
#ifdef USE_VC
  template class Amp0q4g<Vc::double_v>;
#endif
