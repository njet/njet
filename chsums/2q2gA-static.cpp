/*
* chsums/2q2gA-static.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include "2q2gA.h"

// class Amp2q2gAxStatic

const int
Amp2q2gAxStatic::flav[FC][NN] = {
  {-1,1,0,0},
  {-1,1,0,0},
};

const int
Amp2q2gAxStatic::fvsign[FC] = {1, 1};

const int
Amp2q2gAxStatic::fperm[FC][NN] = {
  {0,1,2,3},
  {0,1,2,3},
};

const int
Amp2q2gAxStatic::fvcol[FC][CC] = {
  {0,1,2},
  {0,1,2},
};

// class Amp2q2gAAStatic

const int
Amp2q2gAAStatic::HSarr[HS][HSNN] = {
  { 1,-1, 1, 1, 1, 1},
  { 1,-1, 1,-1, 1, 1},
  { 1,-1,-1, 1, 1, 1},
  { 1,-1,-1,-1, 1, 1},
  {-1, 1, 1, 1, 1, 1},
  {-1, 1, 1,-1, 1, 1},
  {-1, 1,-1, 1, 1, 1},
  {-1, 1,-1,-1, 1, 1},
  { 1,-1, 1, 1,-1, 1},
  { 1,-1, 1,-1,-1, 1},
  { 1,-1,-1, 1,-1, 1},
  { 1,-1,-1,-1,-1, 1},
  {-1, 1, 1, 1,-1, 1},
  {-1, 1, 1,-1,-1, 1},
  {-1, 1,-1, 1,-1, 1},
  {-1, 1,-1,-1,-1, 1},
  { 1,-1, 1, 1, 1,-1},
  { 1,-1, 1,-1, 1,-1},
  { 1,-1,-1, 1, 1,-1},
  { 1,-1,-1,-1, 1,-1},
  {-1, 1, 1, 1, 1,-1},
  {-1, 1, 1,-1, 1,-1},
  {-1, 1,-1, 1, 1,-1},
  {-1, 1,-1,-1, 1,-1},
  { 1,-1, 1, 1,-1,-1},
  { 1,-1, 1,-1,-1,-1},
  { 1,-1,-1, 1,-1,-1},
  { 1,-1,-1,-1,-1,-1},
  {-1, 1, 1, 1,-1,-1},
  {-1, 1, 1,-1,-1,-1},
  {-1, 1,-1, 1,-1,-1},
  {-1, 1,-1,-1,-1,-1},
};

// class Amp2q2gAAxStatic

const int
Amp2q2gAAxStatic::flav[FC][NN] = {
  {-1,1,0,0}, // ax
  {-1,1,0,0}, // axx
  {-1,1,0,0},
};

const int
Amp2q2gAAxStatic::fvsign[FC] = {1, 1, 1};

const int
Amp2q2gAAxStatic::fperm[FC][NN] = {
  {0,1,2,3}, // ax
  {0,1,2,3}, // axx
  {0,1,2,3},
};

const int
Amp2q2gAAxStatic::fvcol[FC][CC] = {
  {0,1,2}, // ax
  {0,1,2}, // axx
  {0,1,2},
};
