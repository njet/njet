/*
* chsums/2q1gA-static.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>

#include "2q1gA.h"

// class Amp2q1gAAStatic

const int
Amp2q1gAAStatic::HSarr[HS][HSNN] = {
  { 1,-1,-1,-1, 1},
  { 1,-1, 1,-1, 1},
  { 1,-1,-1, 1, 1},
  { 1,-1, 1, 1, 1},
  {-1, 1,-1,-1, 1},
  {-1, 1, 1,-1, 1},
  {-1, 1,-1, 1, 1},
  {-1, 1, 1, 1, 1},
  { 1,-1,-1,-1,-1},
  { 1,-1, 1,-1,-1},
  { 1,-1,-1, 1,-1},
  { 1,-1, 1, 1,-1},
  {-1, 1,-1,-1,-1},
  {-1, 1, 1,-1,-1},
  {-1, 1,-1, 1,-1},
  {-1, 1, 1, 1,-1},
};

// class Amp2q1gAAxStatic

const int
Amp2q1gAAxStatic::flav[FC][NN] = {
  {-1,1,0}, // ax
  {-1,1,0}, // axx
  {-1,1,0},
};

const int
Amp2q1gAAxStatic::fvsign[FC] = {1, 1, 1};

const int
Amp2q1gAAxStatic::fperm[FC][NN] = {
  {0,1,2}, // ax
  {0,1,2}, // axx
  {0,1,2},
};

const int
Amp2q1gAAxStatic::fvcol[FC][CC] = {
  {0}, // ax
  {0}, // axx
  {0},
};

