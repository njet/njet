/*
* chsums/6q0g.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>

#include "6q0g.h"
#include "../ngluon2/Model.h"

// class Amp6q0g

template <typename T>
Amp6q0g<T>::Amp6q0g(const T scalefactor, const int mFC, const NJetAmpTables& tables)
  : BaseClass(mFC, tables)
{
  for (int fv=0; fv<mFC; fv++) {
    BaseClass::initNG(scalefactor, fv);
    BaseClass::setProcess(StandardModel::NGluon1compat(NN, getFlav(fv)), fv);
  }

  initNc();
}

template <typename T>
void Amp6q0g<T>::setNc(const ST Nc_)
{
  BaseClass::setNc(Nc_);
  initNc();
}

template <typename T>
void Amp6q0g<T>::initNc()
{
  Nmat[0] = 0.;
  Nmat[1] = 1.;
  Nmat[2] = Nc;
  Nmat[3] = Nc2;
  assert(3 < BaseClass::NmatLen);

  Nmatcc[0] = 0.;
  Nmatcc[1] = 1.;
  Nmatcc[2] = Nc;
  Nmatcc[3] = Nc2;
  assert(3 < BaseClass::NmatccLen);

  bornFactor = Nc;
  loopFactor = 2.*bornFactor;
  bornccFactor = -0.5*V;
}

template <typename T>
void Amp6q0g<T>::getfvpart1_full(const int fv, LoopValue* fvpart)
{
  return getfvpart1_full_(fv, fvpart);
}

template <typename T>
void Amp6q0g<T>::getfvpart1_full(const int fv, LoopResult<T>* fvpart)
{
  return getfvpart1_full_(fv, fvpart);
}

// -------- BEGIN -- automatically generated code -- BEGIN -------- //

template <typename T>
void Amp6q0g<T>::getfvpart0(const int fv, TreeValue* fvpart, TreeValue* fvpartC)
{
  fvSet(fv);
  // 4 primitives
  const TreeValue P123456 = A0(0,1,2,3,4,5);
  const TreeValue P123465 = A0(0,1,2,3,5,4);
  const TreeValue P124356 = A0(0,1,3,2,4,5);
  const TreeValue P125634 = A0(0,1,4,5,2,3);
  fvpart[0] = P123456;
  fvpart[1] = P125634;
  fvpart[2] = -(2.*P123456+P123465+P124356)/Nc;
  fvpart[3] = (P123465-P125634)/Nc;
  fvpart[4] = (P124356-P125634)/Nc;
  fvpart[5] = (P123456+P125634)/Nc2;

  if (fvpartC) {
    for (int i=0; i<C0; i++) {
      fvpartC[i] = conj(fvpart[i]);
    }
  }
}

template <typename T>
template <typename LT>
inline
void Amp6q0g<T>::getfvpart1_full_(const int fv, LT* fvpart)
{
  fvSet(fv);
  {
    // 32 primitives
    const LT P123456 = AL(0,1,2,3,4,5);
    const LT P123465 = AL(0,1,2,3,5,4);
    const LT P123564 = AL(0,1,2,4,5,3);
    const LT P124356 = AL(0,1,3,2,4,5);
    const LT P124365 = AL(0,1,3,2,5,4);
    const LT P124563 = AL(0,1,3,4,5,2);
    const LT P125346 = AL(0,1,4,2,3,5);
    const LT P125634 = AL(0,1,4,5,2,3);
    const LT P125643 = AL(0,1,4,5,3,2);
    const LT P126345 = AL(0,1,5,2,3,4);
    const LT P126534 = AL(0,1,5,4,2,3);
    const LT P126543 = AL(0,1,5,4,3,2);
    const LT P134256 = AL(0,2,3,1,4,5);
    const LT P134265 = AL(0,2,3,1,5,4);
    const LT P134562 = AL(0,2,3,4,5,1);
    const LT P134652 = AL(0,2,3,5,4,1);
    const LT P135642 = AL(0,2,4,5,3,1);
    const LT P156234 = AL(0,4,5,1,2,3);
    const LT P156243 = AL(0,4,5,1,3,2);
    const LT P156342 = AL(0,4,5,2,3,1);
    const LT P312456 = AL(2,0,1,3,4,5);
    const LT P312465 = AL(2,0,1,3,5,4);
    const LT P312564 = AL(2,0,1,4,5,3);
    const LT P312654 = AL(2,0,1,5,4,3);
    const LT P315624 = AL(2,0,4,5,1,3);
    const LT P345126 = AL(2,3,4,0,1,5);
    const LT P346125 = AL(2,3,5,0,1,4);
    const LT P356124 = AL(2,4,5,0,1,3);
    const LT P512346 = AL(4,0,1,2,3,5);
    const LT P512436 = AL(4,0,1,3,2,5);
    const LT P513426 = AL(4,0,2,3,1,5);
    const LT P534126 = AL(4,2,3,0,1,5);

    fvpart[0] = Nc*P123456+(-3.*P123456-2.*P123465+P123564-2.*P124356-P124365-P124563+P125346-P125643
    -P126345-P126534-2.*P126543+P134256+P134265+P134562+P134652-P135642+P156234+P156243
    -P156342+P312456+P312465+P312654-P315624+P345126-P346125+P512346+P512436-P513426-P534126)/Nc;
    fvpart[1] = Nc*P125634+(-P123465+P123564-P124356-2.*P124365-P124563+P125346-3.*P125634-2.*P125643
    -P126345-2.*P126534-P126543+P134256+P134265+P134652-P135642+P156234+P156243+P312456
    +P312465+P312564+P312654-P315624+P345126-P346125-P356124+P512436-P513426)/Nc;
    fvpart[2] = P124365+P126543-P134265-P134652+P135642-P156243+P156342+(P123456+P123465-P123564
    +P124356+P124365+P124563-P125346+P125634+P125643+P126345+P126534+P126543-P134562-P134652
    +P135642-2.*P312456-2.*P312465-P312564-2.*P312654+P315624-2.*P345126+2.*P346125+P356124
    -P512346-2.*P512436+P513426+P534126)/Nc2;
    fvpart[3] = P123465+P126345+P126534+P346125-P512436+P513426+P534126+(P123456+P123465-2.*P123564
    +P124356+P124365+2.*P124563+P125634+P125643+P126534+P126543-P134256-P134265-P134652
    +2.*P135642-2.*P156234-2.*P156243+P156342-P312456-P312465-P312654+2.*P315624+P356124-P512346
    -P512436+P513426)/Nc2;
    fvpart[4] = P124356+P124563+P125643-P312465-P312654+P315624+P356124+(P123456+P123465+P124356
    +P124365-2.*P125346+P125634+P125643+2.*P126345+P126534+P126543-2.*P134256-2.*P134265-P134562
    -2.*P134652+P135642-P156234-P156243+P156342-P312564-P312654+P315624-P345126+P346125
    -P512436+2.*P513426+P534126)/Nc2;
    fvpart[5] = (-P124563-P126345+P134265+P134652-P135642+P156243-P156342+P312465+P312654-P315624
    -P346125-P356124+P512436-P513426-P534126)/Nc+(-P123456-P123465+P123564-P124356-P124365
    -P124563+P125346-P125634-P125643-P126345-P126534-P126543+P134256+P134265+P134562+2.*P134652
    -2.*P135642+P156234+P156243-P156342+P312456+P312465+P312564+2.*P312654-2.*P315624+P345126
    -P346125-P356124+P512346+2.*P512436-2.*P513426-P534126)/Nc3;

  }
  if (Nf != 0.) {
    // 4 primitives
    const LT Q123456 = AF(0,1,2,3,4,5);
    const LT Q123465 = AF(0,1,2,3,5,4);
    const LT Q124356 = AF(0,1,3,2,4,5);
    const LT Q125634 = AF(0,1,4,5,2,3);

    fvpart[0] += Nf*(-Q123456);
    fvpart[1] += Nf*(-Q125634);
    fvpart[2] += Nf*((2.*Q123456+Q123465+Q124356)/Nc);
    fvpart[3] += Nf*((-Q123465+Q125634)/Nc);
    fvpart[4] += Nf*((-Q124356+Q125634)/Nc);
    fvpart[5] += Nf*((-Q123456-Q125634)/Nc2);

  }
}

// --------- END --- automatically generated code --- END --------- //

#ifdef USE_SD
  template class Amp6q0g<double>;
#endif
#ifdef USE_DD
  template class Amp6q0g<dd_real>;
#endif
#ifdef USE_QD
  template class Amp6q0g<qd_real>;
#endif
#ifdef USE_VC
  template class Amp6q0g<Vc::double_v>;
#endif
