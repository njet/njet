/*
* chsums/2q4g-ds.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>

#include "2q4g-ds.h"

// ============================================================================
// class Amp2q4g_ds4
// ============================================================================

template <typename T>
Amp2q4g_ds4<T>::Amp2q4g_ds4(const T scalefactor,
                            const int mFC, const NJetAmpTables& tables)
  : BaseClass(scalefactor, mFC, tables)
{
  initNc();
}

template <typename T>
void Amp2q4g_ds4<T>::setNc(const ST Nc_)
{
  BaseClass::setNc(Nc_);
  initNc();
}

template <typename T>
void Amp2q4g_ds4<T>::initNc()
{
  const ST t18 = 24.*Nc;
  const ST t59 = -t18;
  const ST t58 = t18;
  const ST t35 = 1./Nc;
  const ST t19 = 24.*t35;
  const ST t57 = -t19;
  const ST t56 = t19;
  const ST t34 = Nc*Nc;
  const ST t36 = Nc*t34;
  const ST t55 = 24.*t36;
  const ST t54 = t36*V;
  const ST t39 = t36*t36;
  const ST t53 = 1.+t39;
  const ST t5 = -2.+t34;
  const ST t52 = t5*t36;
  const ST t51 = 12.-t39;
  const ST t21 = 2.*t39;
  const ST t50 = 24.-t21;
  const ST t37 = t34*t34;
  const ST t25 = 6.*t37;
  const ST t49 = 24.+t25;
  const ST t48 = 24.-t25;
  const ST t47 = 96.-t39;
  const ST t46 = 96.+t39;
  const ST t29 = 2.*t34;
  const ST t28 = -t29;
  const ST t45 = t28+t37;
  const ST t30 = 4.*t34;
  const ST t44 = -t30+t37;
  const ST t32 = 10.*t37;
  const ST t10 = -t32;
  const ST t43 = t10+t47;
  const ST t42 = -84.*t34+t47;
  const ST t23 = 2.*t37;
  const ST t41 = t23+t46;
  const ST t31 = Nc*t37;
  const ST t27 = 3.*t34;
  const ST t26 = 8.*t34;
  const ST t38 = 4.*t37;
  const ST t24 = -t38;
  const ST t22 = t38;
  const ST t20 = 8.*t37;
  const ST t17 = -38.*t34;
  const ST t60 = 22.*t34;
  const ST t16 = -t60;
  const ST t15 = -18.*t34;
  const ST t62 = 12.*t34;
  const ST t14 = -t62;
  const ST t13 = t60;
  const ST t12 = -22.*t37;
  const ST t64 = 14.*t37;
  const ST t11 = -t64;
  const ST t9 = 12.*t37;
  const ST t8 = t64;
  const ST t7 = 18.*t37;
  const ST t6 = 26.*t37;
  const ST t4 = 1.+t34;
  const ST t3 = 1.+t27;
  const ST t2 = 2.+t34;
  const ST t1 = -1.+t45;
  NmatDS[0] = 0.;
  NmatDS[1] = 24.*t31;
  NmatDS[2] = 48.*t31;
  NmatDS[3] = 48.*t36;
  NmatDS[4] = t55;
  NmatDS[5] = t4*t58;
  NmatDS[6] = t58;
  NmatDS[7] = -NmatDS[1];
  NmatDS[8] = t1*t59;
  NmatDS[9] = -24.*t52;
  NmatDS[10] = (-1.+t44)*t57;
  NmatDS[11] = t1*t57;
  NmatDS[12] = V*t59;
  NmatDS[13] = t4*t55;
  NmatDS[14] = -34.*t34+t49;
  NmatDS[15] = (-1.-5.*t34+t23)*t57;
  NmatDS[16] = t4*t56;
  NmatDS[17] = -16.*t34+t22+t47;
  NmatDS[18] = t2*t55;
  const ST t71 = V*V;
  NmatDS[19] = t71*t58;
  NmatDS[20] = (t27-3.*t37+t53)*t56;
  NmatDS[21] = -24.*t54;
  NmatDS[22] = -48.*t54;
  NmatDS[23] = 24.+t17+t20;
  NmatDS[24] = 24.+t16+t23;
  const ST t76 = 6.*t34;
  NmatDS[25] = 24.-t76-t23;
  NmatDS[26] = t3*t58;
  NmatDS[27] = (-1.-t27+t37)*t57;
  NmatDS[28] = 24.+t28;
  NmatDS[29] = t3*t56;
  NmatDS[30] = 72.*t31;
  NmatDS[31] = 24.+t13+t11+t21;
  NmatDS[32] = -128.*t34+36.*t37+t46;
  NmatDS[33] = -88.*t34+t6+t46;
  NmatDS[34] = t14+t41;
  NmatDS[35] = -60.*t34+t41;
  NmatDS[36] = t2*(48.+t44);
  NmatDS[37] = -32.*t34-t9+t46;
  NmatDS[38] = t26+t12+t46;
  NmatDS[39] = -104.*t34+34.*t37+t47;
  NmatDS[40] = -108.*t34+t6+t47;
  NmatDS[41] = t7+t42;
  NmatDS[42] = -36.*t34+t8+t47;
  NmatDS[43] = t8+t42;
  NmatDS[44] = -20.*t34+t9+t47;
  const ST t89 = 10.*t34;
  NmatDS[45] = -t5*(48.-t89+t37);
  NmatDS[46] = -(-6.+t34)*(16.+t45);
  NmatDS[47] = -76.*t34+t20+t47;
  NmatDS[48] = t26+t43;
  NmatDS[49] = -40.*t34+t43;
  NmatDS[50] = -t26+t11+t47;
  NmatDS[51] = t14+t12+t47;
  NmatDS[52] = t62-30.*t37+t47;
  NmatDS[53] = -50.*t34+t7+t50;
  NmatDS[54] = t15+t9+t50;
  NmatDS[55] = t16+t32+t50;
  NmatDS[56] = 2.*t34+2.*t23+2.*t51;
  NmatDS[57] = t89+t23+2.*t51;
  NmatDS[58] = 96.-23.*t39-200.*t34+94.*t37;
  NmatDS[59] = (t30+t24+t53)*t56;
  NmatDS[60] = -48.*t52;
  NmatDS[61] = t17+t49;
  NmatDS[62] = 24.+t15+t22;
  NmatDS[63] = 24.+t28-t23;
  NmatDS[64] = 24.+t29+t24;
  NmatDS[65] = t13+t48;
  NmatDS[66] = 14.*t34+t48;
  NmatDS[67] = 24.+26.*t34+t10;
  NmatDS[68] = (1.+t76+t37)*t56;
  const ST t109 = t4*t4;
  NmatDS[69] = t109*t56;
  NmatDS[70] = t39*t58;
  NmatDS[71] = -NmatDS[2];
  NmatDS[72] = 96.*t36;
  NmatDS[73] = t56;
  assert(73 < NmatDSLen);
}

template <typename T>
void Amp2q4g_ds4<T>::getfvpart1ds(const int fv, LoopValue* fvpart)
{
  return getfvpart1ds_(fv, fvpart);
}

template <typename T>
void Amp2q4g_ds4<T>::getfvpart1ds(const int fv, LoopResult<T>* fvpart)
{
  return getfvpart1ds_(fv, fvpart);
}

template <typename T>
template <typename LT>
inline
void Amp2q4g_ds4<T>::getfvpart1ds_(const int fv, LT* fvpart)
{
  fvSet(fv);

  fvpart[0] = AL(0,1,2,3,4,5);
  fvpart[1] = AL(0,2,1,3,4,5);
  fvpart[2] = AL(0,2,3,1,4,5);
  fvpart[3] = AL(0,2,3,4,1,5);
  fvpart[4] = AL(0,2,3,4,5,1);

  if (Nf != 0.) {
    fvpart[5] = Nf*AF(0,1,2,3,4,5);
    fvpart[6] = Nf*AF(0,2,1,3,4,5);
  } else {
    fvpart[5] = LT();
    fvpart[6] = LT();
  }
}

#ifdef USE_SD
  template class Amp2q4g_ds4<double>;
#endif
#ifdef USE_DD
  template class Amp2q4g_ds4<dd_real>;
#endif
#ifdef USE_QD
  template class Amp2q4g_ds4<qd_real>;
#endif
#ifdef USE_VC
  template class Amp2q4g_ds4<Vc::double_v>;
#endif

// ============================================================================
// class Amp2q4g_ds3
// ============================================================================

template <typename T>
Amp2q4g_ds3<T>::Amp2q4g_ds3(const T scalefactor,
                            const int mFC, const NJetAmpTables& tables)
  : BaseClass(scalefactor, mFC, tables)
{
  initNc();
}

template <typename T>
void Amp2q4g_ds3<T>::setNc(const ST Nc_)
{
  BaseClass::setNc(Nc_);
  initNc();
}

template <typename T>
void Amp2q4g_ds3<T>::initNc()
{
  const ST t60 = 6.*Nc;
  const ST t118 = -t60;
  const ST t59 = t60;
  const ST t65 = 1./Nc;
  const ST t62 = 6.*t65;
  const ST t117 = -t62;
  const ST t116 = t62;
  const ST t64 = Nc*Nc;
  const ST t66 = Nc*t64;
  const ST t115 = 6.*t66;
  const ST t114 = t64*V;
  const ST t113 = t66*V;
  const ST t67 = t64*t64;
  const ST t112 = -1.+t67;
  const ST t69 = t66*t66;
  const ST t111 = 1.+t69;
  const ST t23 = -11.*t64;
  const ST t110 = 6.+t23;
  const ST t52 = 7.*t64;
  const ST t109 = 6.+t52;
  const ST t68 = 9.*t64;
  const ST t58 = -t68;
  const ST t108 = 6.+t58;
  const ST t12 = 6.+t64;
  const ST t107 = 6.+t67;
  const ST t21 = 15.*t64;
  const ST t106 = 12.+t21;
  const ST t32 = -29.*t64;
  const ST t105 = 12.+t32;
  const ST t44 = 2.*t67;
  const ST t104 = 12.+t44;
  const ST t53 = 5.*t64;
  const ST t103 = 12.+t53;
  const ST t102 = 12.-t67;
  const ST t101 = 12.+t69;
  const ST t29 = -23.*t64;
  const ST t100 = 18.+t29;
  const ST t35 = -2.*t69;
  const ST t99 = 18.+t35;
  const ST t43 = 3.*t67;
  const ST t98 = 18.+t43;
  const ST t56 = -t53;
  const ST t97 = 18.+t56;
  const ST t22 = 13.*t64;
  const ST t96 = 36.+t22;
  const ST t36 = -6.*t69;
  const ST t95 = 36.+t36;
  const ST t39 = 7.*t67;
  const ST t94 = 36.+t39;
  const ST t42 = 4.*t67;
  const ST t93 = 36.+t42;
  const ST t92 = 36.+t43;
  const ST t91 = 36.+t64;
  const ST t90 = 36.-t69;
  const ST t17 = -2.+t64;
  const ST t89 = t17*t66;
  const ST t25 = -t21;
  const ST t88 = t25+t42;
  const ST t87 = t25+t67;
  const ST t86 = t44+t56;
  const ST t57 = -t52;
  const ST t85 = t44+t57;
  const ST t45 = -t44;
  const ST t54 = 3.*t64;
  const ST t84 = t45+t54;
  const ST t55 = -t54;
  const ST t83 = t45+t55;
  const ST t82 = t45-t64;
  const ST t46 = -t43;
  const ST t81 = t46+t58;
  const ST t80 = t54+t67;
  const ST t79 = t55+t67;
  const ST t78 = t56+t67;
  const ST t77 = t58+t67;
  const ST t76 = t64+t67;
  const ST t75 = t67-t64;
  const ST t74 = t25+t104;
  const ST t40 = 6.*t67;
  const ST t73 = 12.+t40-t69;
  const ST t72 = t44+t90;
  const ST t71 = -t69+t83;
  const ST t61 = Nc*t67;
  const ST t51 = t68;
  const ST t122 = 8.*t67;
  const ST t50 = -t122;
  const ST t49 = -t39;
  const ST t48 = -t40;
  const ST t47 = -t42;
  const ST t41 = 5.*t67;
  const ST t38 = t122;
  const ST t37 = 9.*t67;
  const ST t34 = -39.*t64;
  const ST t33 = -33.*t64;
  const ST t31 = -27.*t64;
  const ST t30 = -25.*t64;
  const ST t28 = -21.*t64;
  const ST t128 = 19.*t64;
  const ST t27 = -t128;
  const ST t129 = 17.*t64;
  const ST t26 = -t129;
  const ST t24 = -t22;
  const ST t20 = 10.*t67;
  const ST t19 = -4.+t64;
  const ST t18 = -3.+t64;
  const ST t16 = 1.+t64;
  const ST t15 = 3.+t64;
  const ST t14 = 6.+t55;
  const ST t13 = 6.-t64;
  const ST t130 = 2.*t64;
  const ST t11 = -t130+t112;
  const ST t10 = t43+t108;
  const ST t9 = t57+t107;
  const ST t8 = 6.+t85;
  const ST t7 = 6.+t56-t67;
  const ST t6 = 6.+t78;
  const ST t5 = 6.+t79;
  const ST t4 = 6.+t82;
  const ST t3 = 6.+t76;
  const ST t2 = 6.+t53+t45;
  const ST t1 = t39+t35+t110;
  NmatDS[0] = 0.;
  NmatDS[1] = 6.*t61;
  NmatDS[2] = 12.*t61;
  NmatDS[3] = 12.*t66;
  NmatDS[4] = t115;
  NmatDS[5] = 6.*Nc+6.*t66;
  NmatDS[6] = t59;
  NmatDS[7] = -NmatDS[1];
  NmatDS[8] = t11*t118;
  NmatDS[9] = -6.*t89;
  const ST t133 = 4.*t64;
  NmatDS[10] = (-t133+t112)*t117;
  NmatDS[11] = t11*t117;
  NmatDS[12] = V*t118;
  NmatDS[13] = 6.*t66+6.*t61;
  NmatDS[14] = (-1.+t86)*t117;
  NmatDS[15] = t16*t116;
  NmatDS[16] = (2.+t64)*t115;
  const ST t138 = V*V;
  NmatDS[17] = t138*t59;
  NmatDS[18] = (t54+t46+t111)*t116;
  NmatDS[19] = -6.*t113;
  NmatDS[20] = -12.*t113;
  NmatDS[21] = t59+18.*t66;
  NmatDS[22] = (-1.+t79)*t117;
  NmatDS[23] = (1.+t54)*t116;
  NmatDS[24] = 18.*t61;
  NmatDS[25] = t14;
  NmatDS[26] = (t133+t47+t111)*t116;
  NmatDS[27] = -12.*t89;
  const ST t147 = 6.*t64;
  NmatDS[28] = (1.+t147+t67)*t116;
  const ST t149 = t16*t16;
  NmatDS[29] = t149*t116;
  NmatDS[30] = t13;
  NmatDS[31] = t69*t59;
  NmatDS[32] = -NmatDS[2];
  NmatDS[33] = 24.*t66;
  NmatDS[34] = t116;
  NmatDS[35] = t10;
  NmatDS[36] = 12.+t76;
  NmatDS[37] = t5;
  NmatDS[38] = 12.+t78;
  NmatDS[39] = t9;
  NmatDS[40] = -t17*t15;
  NmatDS[41] = t12;
  NmatDS[42] = -t13;
  NmatDS[43] = -t18*(12.+t75);
  NmatDS[44] = t1;
  const ST t154 = 13.*t67;
  NmatDS[45] = 36.-45.*t64+t154;
  NmatDS[46] = 36.+t34+t37;
  NmatDS[47] = 6.+t86;
  NmatDS[48] = t3;
  NmatDS[49] = t6;
  NmatDS[50] = 12.+t77;
  NmatDS[51] = 36.+t28+t67;
  NmatDS[52] = t7;
  NmatDS[53] = 36.+t84;
  NmatDS[54] = 6.+t84;
  NmatDS[55] = t45+t12;
  NmatDS[56] = t4;
  NmatDS[57] = 36.+t25+t46;
  NmatDS[58] = -t14;
  NmatDS[59] = -t1;
  NmatDS[60] = t27+t40+t101;
  NmatDS[61] = t69+t74;
  NmatDS[62] = t53+t48+t101;
  const ST t155 = 31.*t64;
  NmatDS[63] = t155+t50+t101;
  NmatDS[64] = t51-t20+t101;
  NmatDS[65] = -t64+t101;
  NmatDS[66] = t24+t101;
  NmatDS[67] = -47.*t64+12.*t67+t90;
  NmatDS[68] = t31+t20+t90;
  NmatDS[69] = -(3.-t147+t67)*t19;
  const ST t160 = t18*t18;
  NmatDS[70] = -t19*t160;
  NmatDS[71] = -(36.+t77)*(-1.+t64);
  NmatDS[72] = t56+t73;
  NmatDS[73] = t26+t73;
  NmatDS[74] = -(9.+t67)*t19;
  NmatDS[75] = t28+t42+t90;
  NmatDS[76] = t24+t72;
  NmatDS[77] = t30+t72;
  NmatDS[78] = 36.+t71;
  NmatDS[79] = 12.+t71;
  NmatDS[80] = t29+t90;
  const ST t167 = 11.*t67;
  NmatDS[81] = t33+t167+t99;
  NmatDS[82] = t30+t37+t99;
  NmatDS[83] = -t64+t35+t98;
  NmatDS[84] = 16.*t67-5.*t69+t105;
  NmatDS[85] = -59.*t64+27.*t67+t95;
  NmatDS[86] = 12.-51.*t64+26.*t67+t36;
  NmatDS[87] = -61.*t64+25.*t67+t95;
  NmatDS[88] = t20+t105;
  NmatDS[89] = 12.+t30+t38;
  NmatDS[90] = 18.-t155+t38;
  NmatDS[91] = 36.-37.*t64+t38;
  NmatDS[92] = 12.+t27+t39;
  NmatDS[93] = t31+t94;
  NmatDS[94] = t34+t94;
  NmatDS[95] = 18.+t32+t40;
  NmatDS[96] = 12.+t58+t41;
  NmatDS[97] = 6.+t26+t41;
  NmatDS[98] = t18*(-4.+t53);
  NmatDS[99] = t41+t100;
  NmatDS[100] = 36.+t30+t41;
  NmatDS[101] = t58+t93;
  NmatDS[102] = -6.+t23+t42;
  NmatDS[103] = 36.+t88;
  NmatDS[104] = 18.+t88;
  NmatDS[105] = 12.+t26+t42;
  NmatDS[106] = 18.+t30+t42;
  NmatDS[107] = t31+t93;
  NmatDS[108] = t43+t91;
  NmatDS[109] = t43+t97;
  NmatDS[110] = t43+t110;
  NmatDS[111] = t25+t98;
  NmatDS[112] = t28+t92;
  NmatDS[113] = t33+t92;
  NmatDS[114] = t51+t104;
  NmatDS[115] = -t4;
  NmatDS[116] = t17*(3.+t130);
  NmatDS[117] = t55+t104;
  NmatDS[118] = -t2;
  NmatDS[119] = 18.+t85;
  NmatDS[120] = 12.+t85;
  NmatDS[121] = t8;
  NmatDS[122] = t44+t108;
  NmatDS[123] = t74;
  NmatDS[124] = t27+t104;
  NmatDS[125] = t44+t100;
  NmatDS[126] = t15*(4.+t64);
  NmatDS[127] = -t7;
  NmatDS[128] = 18.+t80;
  NmatDS[129] = 12.+t80;
  NmatDS[130] = -6.+t76;
  NmatDS[131] = 36.+t75;
  NmatDS[132] = 36.+t79;
  NmatDS[133] = -t13*t16;
  NmatDS[134] = 12.+t23+t67;
  NmatDS[135] = 36.+t87;
  NmatDS[136] = 18.+t87;
  NmatDS[137] = t51-t107;
  NmatDS[138] = -t9;
  NmatDS[139] = t53+t102;
  NmatDS[140] = -t6;
  NmatDS[141] = -t5;
  NmatDS[142] = 18.-t114;
  NmatDS[143] = 6.-t114;
  NmatDS[144] = -6.-t114;
  NmatDS[145] = -t3;
  NmatDS[146] = -t17*(9.+t64);
  NmatDS[147] = t57+t102;
  NmatDS[148] = 36.+t23-t67;
  NmatDS[149] = 36.+t51+t45;
  NmatDS[150] = t45+t109;
  NmatDS[151] = -t8;
  NmatDS[152] = t2;
  NmatDS[153] = 18.+t84;
  NmatDS[154] = 18.+t82;
  NmatDS[155] = 36.+t83;
  NmatDS[156] = 12.+t56+t45;
  NmatDS[157] = 18.+t128+t46;
  NmatDS[158] = -6.+t21+t46;
  NmatDS[159] = -t10;
  NmatDS[160] = t46+t109;
  NmatDS[161] = t46+t103;
  NmatDS[162] = 36.-3.*t75;
  NmatDS[163] = 12.-3.*t114;
  NmatDS[164] = 12.+t57+t46;
  NmatDS[165] = 36.+t81;
  NmatDS[166] = 12.+t81;
  NmatDS[167] = -6.+t22+t47;
  NmatDS[168] = 18.+t64+t47;
  NmatDS[169] = 12.-t64+t47;
  NmatDS[170] = 36.+t24+t47;
  NmatDS[171] = -t41+t103;
  NmatDS[172] = t48+t96;
  NmatDS[173] = 18.+t51+t48;
  NmatDS[174] = t48+t91;
  NmatDS[175] = 18.+t129+t49;
  NmatDS[176] = t49+t106;
  NmatDS[177] = t49+t103;
  NmatDS[178] = 36.-t64+t49;
  NmatDS[179] = 36.+t21+t50;
  NmatDS[180] = 12.+t52+t50;
  NmatDS[181] = -t167+t106;
  NmatDS[182] = -t154+t96;
  NmatDS[183] = -t12;
  NmatDS[184] = t97;
  NmatDS[185] = 18.+t58;
  assert(185 < NmatDSLen);
}

template <typename T>
void Amp2q4g_ds3<T>::getfvpart1ds(const int fv, LoopValue* fvpart)
{
  return getfvpart1ds_(fv, fvpart);
}

template <typename T>
void Amp2q4g_ds3<T>::getfvpart1ds(const int fv, LoopResult<T>* fvpart)
{
  return getfvpart1ds_(fv, fvpart);
}

template <typename T>
template <typename LT>
inline
void Amp2q4g_ds3<T>::getfvpart1ds_(const int fv, LT* fvpart)
{
  fvSet(fv);

  fvpart[ 0] = AL(0,1,2,3,4,5);
  fvpart[ 1] = AL(0,1,3,2,4,5);
  fvpart[ 2] = AL(0,1,3,4,2,5);
  fvpart[ 3] = AL(0,1,3,4,5,2);
  fvpart[ 4] = AL(0,2,1,3,4,5);
  fvpart[ 5] = AL(0,3,1,2,4,5);
  fvpart[ 6] = AL(0,3,1,4,2,5);
  fvpart[ 7] = AL(0,3,1,4,5,2);
  fvpart[ 8] = AL(0,2,3,1,4,5);
  fvpart[ 9] = AL(0,2,3,4,1,5);
  fvpart[10] = AL(0,2,3,4,5,1);
  fvpart[11] = AL(0,3,2,1,4,5);
  fvpart[12] = AL(0,3,2,4,1,5);
  fvpart[13] = AL(0,3,2,4,5,1);
  fvpart[14] = AL(0,3,4,1,2,5);
  fvpart[15] = AL(0,3,4,1,5,2);
  fvpart[16] = AL(0,3,4,2,1,5);
  fvpart[17] = AL(0,3,4,2,5,1);
  fvpart[18] = AL(0,3,4,5,1,2);
  fvpart[19] = AL(0,3,4,5,2,1);

  if (Nf != 0.) {
    fvpart[20] = Nf*AF(0,1,2,3,4,5);
    fvpart[21] = Nf*AF(0,1,3,2,4,5);
    fvpart[22] = Nf*AF(0,1,3,4,2,5);
    fvpart[23] = Nf*AF(0,1,3,4,5,2);
    fvpart[24] = Nf*AF(0,2,1,3,4,5);
    fvpart[25] = Nf*AF(0,3,1,2,4,5);
    fvpart[26] = Nf*AF(0,3,1,4,2,5);
    fvpart[27] = Nf*AF(0,3,1,4,5,2);
    fvpart[28] = Nf*AF(0,2,3,1,4,5);
  } else {
    fvpart[20] = LT();
    fvpart[21] = LT();
    fvpart[22] = LT();
    fvpart[23] = LT();
    fvpart[24] = LT();
    fvpart[25] = LT();
    fvpart[26] = LT();
    fvpart[27] = LT();
    fvpart[28] = LT();
  }
}

#ifdef USE_SD
  template class Amp2q4g_ds3<double>;
#endif
#ifdef USE_DD
  template class Amp2q4g_ds3<dd_real>;
#endif
#ifdef USE_QD
  template class Amp2q4g_ds3<qd_real>;
#endif
#ifdef USE_VC
  template class Amp2q4g_ds3<Vc::double_v>;
#endif
