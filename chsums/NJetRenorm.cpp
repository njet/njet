/*
* chsums/NJetRenorm.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012, 2013 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include "NJetRenorm.h"
#include "NJetAmp.h"

template <typename T>
void NJetRenorm<T>::setNc(const ST Nc_) {
  Nc = Nc_;
  CF = 0.5*(Nc - 1./Nc);
  BETA0 = (11.*Nc - 2.*Nf)/3.;
}

template <typename T>
void NJetRenorm<T>::setNf(const ST Nf_) {
  Nf = Nf_;
  BETA0 = (11.*Nc - 2.*Nf)/3.;
}

template <typename T>
void NJetRenorm<T>::setScheme(int x) {
  // NONE scheme can be switched only to ZERO scheme
  if (scheme != SCHEME_NONE or x == SCHEME_ZERO) {
    scheme = x;
  }
}

template <typename T>
void NJetRenorm<T>::setRenorm(int x) {
  renorm = x;
}

template <typename T>
NJetRenorm<T>::NJetRenorm(const NJetAmp<T>* amp)
  : legs(0), nG(0), nQ(0),
    scheme(SCHEME_FDH), renorm(UNRENORM)
{
  if (amp) {
    initialize(amp);
  }
}

template <typename T>
void NJetRenorm<T>::initialize(const NJetAmp<T>* amp)
{
  setNc(amp->getNc());
  setNf(amp->getNf());

  nG = 0;
  nQ = 0;
  legs = amp->legsQCD();
  for (int i=0; i<legs; i++) {
    if (amp->getFlav(0, i) == 0) {
      nG += 1;
    } else {
      nQ += 1;
    }
  }
}

template <typename T>
EpsTriplet<T> NJetRenorm<T>::Renormalize(const T born, const EpsTriplet<T>& virt) const
{
  if (not legs or scheme == SCHEME_NONE) {
    return virt;
  }

  T virt0 = 0.;
  T virt1 = 0.;
  if (renorm == QCDRENORM) {
    virt1 -= double(legs-2)*BETA0;
    if (scheme == SCHEME_FDH) {
      virt0 += double(legs-2)*(Nc/3.);
    }
  }
  if (scheme == SCHEME_CDR) {
    virt0 += (double(legs-2)*(Nc/3.) - (double(nG)*(Nc/3.) + double(nQ)*CF));
  }
  return virt + born*EpsTriplet<T>(virt0, virt1);
}

#ifdef USE_SD
  template class NJetRenorm<double>;
#endif
#ifdef USE_DD
  template class NJetRenorm<dd_real>;
#endif
#ifdef USE_QD
  template class NJetRenorm<qd_real>;
#endif
#ifdef USE_VC
  template class NJetRenorm<Vc::double_v>;
#endif
