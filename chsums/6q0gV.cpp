/*
* chsums/6q0gV.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include "6q0gV.h"
#include "NJetAmp-T.h"

// class Amp6q0gV

template <typename T>
Amp6q0gV<T>::Amp6q0gV(const T scalefactor, const int mFC, const NJetAmpTables& tables)
  : BaseClass(scalefactor, mFC, tables)
{
  Flavour<double> ff = StandardModel::Wp(StandardModel::u(), StandardModel::ubar(), 1.);  // ckm = 1
  initProcess(ff);
}

template <typename T>
Amp6q0gV<T>::Amp6q0gV(const Flavour<double>& ff,
                      const T scalefactor, const int mFC, const NJetAmpTables& tables)
  : BaseClass(scalefactor, mFC, tables)
{
  initProcess(ff);
}

template <typename T>
void Amp6q0gV<T>::initProcess(const Flavour<double>& ff)
{
  for (int fv=0; fv<BaseClass::mFC; fv++) {
    Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
    flavours.push_back(ff);
    BaseClass::setProcess(flavours, fv);
  }
}

template <typename T>
Amp6q0gZd<T>::Amp6q0gZd(const Flavour<double>& ff,
                        const T scalefactor, const int mFC, const NJetAmpTables& tables)
  : BaseClass(ff, scalefactor, mFC, tables)
{
  const Flavour<double> Zu = ff;
  const Flavour<double> Zd = StandardModel::BosonFlip(ff);

  Flavour<double> ffs[3] = {Zu, Zu, Zd};

  for (int fv=0; fv<BaseClass::mFC; fv++) {
    Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
    flavours.push_back(ffs[fv]);
    BaseClass::setProcess(flavours, fv);
  }
}

template <typename T>
Amp6q0gZ2d<T>::Amp6q0gZ2d(const Flavour<double>& ff,
                          const T scalefactor, const int mFC, const NJetAmpTables& tables)
  : BaseClass(ff, scalefactor, mFC, tables)
{
  const Flavour<double> Zu = ff;
  const Flavour<double> Zd = StandardModel::BosonFlip(ff);

  Flavour<double> ffs[6] = {Zu, Zu, Zd, Zu, Zu, Zd};

  for (int fv=0; fv<BaseClass::mFC; fv++) {
    Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
    flavours.push_back(ffs[fv]);
    BaseClass::setProcess(flavours, fv);
  }
}

template <typename T>
typename Amp6q0gV<T>::TreeValue
Amp6q0gV<T>::A0(int p0, int p1, int p2, int p3, int p4, int p5)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, O[p1], O[p2], O[p3], O[p4], O[p5]};
  return BaseClass::A0nVqq(order);
}

template <typename T>
LoopResult<T> Amp6q0gV<T>::AF(int p0, int p1, int p2, int p3, int p4, int p5)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, O[p1], O[p2], O[p3], O[p4], O[p5]};
  return BaseClass::AFnV(order);
}

template <typename T>
LoopResult<T> Amp6q0gV<T>::AL(int p0, int p1, int p2, int p3, int p4, int p5)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, O[p1], O[p2], O[p3], O[p4], O[p5]};
  return BaseClass::ALnV(order);
}

#ifdef USE_SD
  template class Amp6q0gV<double>;
  template class Amp6q0gZd<double>;
  template class Amp6q0gZ2d<double>;
#endif
#ifdef USE_DD
  template class Amp6q0gV<dd_real>;
  template class Amp6q0gZd<dd_real>;
  template class Amp6q0gZ2d<dd_real>;
#endif
#ifdef USE_QD
  template class Amp6q0gV<qd_real>;
  template class Amp6q0gZd<qd_real>;
  template class Amp6q0gZ2d<qd_real>;
#endif
#ifdef USE_VC
  template class Amp6q0gV<Vc::double_v>;
  template class Amp6q0gZd<Vc::double_v>;
  template class Amp6q0gZ2d<Vc::double_v>;
#endif
