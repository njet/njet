/*
* chsums/4q0g-static.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include "4q0g.h"

// class Amp4q0gStatic

const int
Amp4q0gStatic::flav[FC][NN] = {
    {-1,1,-2,2},
    {-1,2,-2,1}
};

const int
Amp4q0gStatic::fvsign[FC] = {1,-1};

const int
Amp4q0gStatic::ccsign[NN*(NN+1)/2] = {1,1,1,1,1,1,1,1,1,1};

const int
Amp4q0gStatic::fperm[FC][NN] = {
    {0,1,2,3},
    {0,3,2,1}
};

const int
Amp4q0gStatic::fvcol[FC][CC] = {
    {0,1},
    {1,0}
};

const unsigned char
Amp4q0gStatic::colmat[CC*(CC+1)/2] = {0,1,0};

const unsigned char
Amp4q0gStatic::colmatcc[NN*(NN+1)/2][C0*(C0+1)/2] = {
    {0}, // 00
    {0,1,2}, // 01
    {0}, // 11
    {0,3,0}, // 02
    {2,1,0}, // 12
    {0}, // 22
    {2,1,0}, // 03
    {0,3,0}, // 13
    {0,1,2}, // 23
    {0}  // 33
};

const int
Amp4q0gStatic::HSarr[HS][HSNN] = {
  {-1, 1, 1,-1},
  {-1, 1,-1, 1},
  { 1,-1,-1, 1},
  { 1,-1, 1,-1}
};

// class Amp4q0g2Static

const int
Amp4q0g2Static::HSarr[HS][HSNN] = {
  { 1,-1, 1,-1},
  { 1,-1,-1, 1},
  { 1, 1,-1,-1},
  {-1, 1,-1, 1},
  {-1, 1, 1,-1},
  {-1,-1, 1, 1}
};
