/*
* chsums/0q3gH.h
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef CHSUM_0Q3GH_H
#define CHSUM_0Q3GH_H

#include "NJetAmpN.h"

class Amp0q3gHStatic : public AmpNStatic<3>
{
  public:
    static const int FC = 1;   // flavour permutations
    static const int C0 = 2;   // partial amplitudes
    static const int CC = 0;   // untested, probably 2

    static const int flav[FC][NN];  // flavour vectors for ngluon
    static const int fvsign[FC];    // flavour channels' signs
    static const int ccsign[NN*(NN+1)/2];  // ccborn signs
    static const int fperm[FC][NN];        // flavour pemutation matrix
    static const int fvcol[FC][C0];        // flavour-partial pemutation matrix

    static const unsigned char colmat[C0*(C0+1)/2];  // partial-partial colour factors
    static const unsigned char colmatcc[NN*(NN+1)/2][C0*(C0+1)/2];  // ccborn partial-partial factors

    static const int NmatLen = 2;
    static const int NmatccLen = 2;

    static const int HS = 8;
    static const int HSNN = NN+1;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp0q3gH : public NJetAmp<T>
{
    typedef NJetAmp<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;
    typedef typename BaseClass::ST ST;

    Amp0q3gH(const T scalefactor, const int mFC=1, const NJetAmpTables& tables=amptables());

    void setNc(const ST Nc_);

    void born_part_fill() { BaseClass::born_part_trickfill(); }

  protected:
    using BaseClass::ngluons;
    using BaseClass::NN;
    using BaseClass::V;
    using BaseClass::Nc;
    using BaseClass::C0;
    using BaseClass::Nmat;
    using BaseClass::Nmatcc;
    using BaseClass::bornFactor;
    using BaseClass::bornccFactor;
    using BaseClass::loopFactor;
    using BaseClass::getFlav;
    using BaseClass::getFperm;
    using BaseClass::mfv;
    using BaseClass::fvSet;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp0q3gHStatic>();
    }

    void initProcess(const Flavour<double>& ff);
    void initNc();

    virtual TreeValue A0(int p0, int p1, int p2);
//     virtual LoopResult<T> AL(int /*p0*/, int /*p1*/, int /*p2*/) { return LoopResult<T>(); }
//     virtual LoopResult<T> AF(int /*p0*/, int /*p1*/, int /*p2*/) { return LoopResult<T>(); }

    void getfvpart0(const int fv, TreeValue* fvpart, TreeValue* fvpartC);
};

#endif // CHSUM_0Q3GH_H
