/*
* chsums/0q2gH-static.cpp
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>

#include "0q2gH.h"

// class Amp0q2gHStatic

const int
Amp0q2gHStatic::flav[FC][NN] = {
  {0,0}
};

const int
Amp0q2gHStatic::fvsign[FC] = {1};

const int
Amp0q2gHStatic::ccsign[NN*(NN+1)/2] = {1,1,1};

const int
Amp0q2gHStatic::fperm[FC][NN] = {
  {0,1}
};

const int
Amp0q2gHStatic::fvcol[FC][C0] = {
  {0}
};

const unsigned char
Amp0q2gHStatic::colmat[C0*(C0+1)/2] = {
  0
};

const unsigned char
Amp0q2gHStatic::colmatcc[NN*(NN+1)/2][C0*(C0+1)/2] = {
  {},
  {0},
  {}
};

const int
Amp0q2gHStatic::HSarr[HS][HSNN] = {
  { 1, 1, 1},
  { 1,-1, 1},
  {-1, 1, 1},
  {-1,-1, 1},
};
