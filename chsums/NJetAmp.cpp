/*
* chsums/NJetAmp.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>

#include "NJetAmp.h"
#include "NJetAnalytic.h"
#include "../ngluon2/NGluon2.h"
#include "../ngluon2/SpnMatrix.h"

template <typename T>
NJetAmp<T>::NJetAmp(const int mFC_, const NJetAmpTables& tables)
  : NJetAmpTables(tables), mFC(mFC_), norderL(100), norderF(100),
    mfv(0), mhel(-1), mhelint(0),
    Nc(double(DEFAULT_Nc)), Nf(double(DEFAULT_Nf)),
    fvZero(0), vhel(HSNN),
    Nmat(NmatLen), Nmatcc(NmatccLen), NmatDS(NmatDSLen),
    allpart0(HS*C0), allpart1(CC),
    allfvpart0(2*FC*C0), allfvpart1(2*FC*CC),
    allfvpartarr0(2*FC), allfvpartarr1(FC),
    njetan(0)
{
  initNc();
  initHS();
  setLoopType(COLOR_FULL);
  vhel[0] = 99;
  for (int fv=0; fv<2*FC; fv++) {
    allfvpartarr0[fv] = &allfvpart0[fv*C0];
  }
  for (int fv=0; fv<FC; fv++) {
    allfvpartarr1[fv] = &allfvpart1[2*fv*CC];
  }
}

template <typename T>
NJetAmp<T>::~NJetAmp()
{
  delete njetan;
  njetan = 0;
}

template <typename T>
void NJetAmp<T>::resetCache()
{
  cached0 = CACHED0_NONE;
}

template <typename T>
void NJetAmp<T>::setMuR2(const T rscale)
{
  if (njetan) {
    njetan->setMuR2(rscale);
  }
  NAmp<T>::setMuR2(rscale);
}

template <typename T>
void NJetAmp<T>::setMomenta(const RealMom* moms)
{
  if (njetan) {
    njetan->setMomenta(moms);
  }
  NAmp<T>::setMomenta(moms);
  resetCache();
}

template <typename T>
void NJetAmp<T>::setMomenta(const std::vector<RealMom>& moms)
{
  setMomenta(moms.data());
}

template <typename T>
template <typename U>
void NJetAmp<T>::setMomenta(const MOM<U>* othermoms)
{
  if (njetan) {
    njetan->setMomenta(othermoms);
  }
  NAmp<T>::setMomenta(othermoms);
  resetCache();
}

template <typename T>
template <typename U>
void NJetAmp<T>::setMomenta(const std::vector<MOM<U> >& othermoms)
{
  setMomenta(othermoms.data());
}

template <typename T>
void NJetAmp<T>::setHelicity(const int* helicity)
{
  bool changed = false;
  mhelint = 0;
  for (unsigned i=0; i<vhel.size(); i++) {
    changed |= (vhel[i] != helicity[i]);
    vhel[i] = helicity[i];
    mhelint |= int(helicity[i] == 1) << i;
  }
  if (changed) {
    markZeroFv();
    mhel = mapinths[mhelint];
    NAmp<T>::setHelicity(helicity);
    cached0 = CACHED0_NONE;
  }
}

template <typename T>
void NJetAmp<T>::setHelicity(const std::vector<int>& helicity)
{
  setHelicity(helicity.data());
}

template <typename T>
int NJetAmp<T>::legsMOM() const
{
  if (njetan) {
    return njetan->legsMOM();
  } else {
    return ngluons[0]->Nexternal();
  }
}

template <typename T>
void NJetAmp<T>::setNc(const ST Nc_)
{
  Nc = Nc_;
  initNc();
  resetCache();
}

template <typename T>
void NJetAmp<T>::initNc()
{
  Nc2 = Nc*Nc;
  Nc3 = Nc*Nc2;
  Nc4 = Nc*Nc3;
  Nc5 = Nc*Nc4;
  V = Nc2 - 1.;
}

template <typename T>
void NJetAmp<T>::setNf(const ST Nf_)
{
  Nf = Nf_;
}

template <typename T>
void NJetAmp<T>::initHS()
{
  allpart0.resize(HS*C0);

  maphsint.clear();
  maphsint.resize(HS);

  mapinths.clear();
  mapinths.assign(1<<HSNN, -1);

  for (int h=0; h<HS; h++) {
    unsigned idx = 0;
    for (int i=0; i<HSNN; i++) {
      idx |= int(getHelicity(h)[i] == 1) << i;
    }
    maphsint[h] = idx;
    mapinths[idx] = h;
  }
}

template <typename T>
void NJetAmp<T>::setHS(const int hs, const int* hsarr)
{
  HS = hs;
  HSarr = hsarr;
  initHS();
  cached0 = CACHED0_NONE;
}

template <typename T>
bool NJetAmp<T>::setLoopType(int type, int norderL_, int norderF_)
{
  return setLoopTypeNONE(type, norderL_, norderF_);
}

// setLoopType - with no LC and no SLC
template <typename T>
bool NJetAmp<T>::setLoopTypeNONE(int /*type*/, int /*norderL_*/, int /*norderF_*/)
{
  getfvpart1_normal = &NJetAmp<T>::getfvpart1_full;
  getfvpart1_trick = &NJetAmp<T>::getfvpart1_full;
  return false;
}

// setLoopType - with both LC and SLC
template <typename T>
bool NJetAmp<T>::setLoopTypeLCSLC(int type, int norderL_, int norderF_)
{
  norderL = norderL_;
  norderF = norderF_;
  switch (type) {
    case COLOR_FULL:
      getfvpart1_normal = &NJetAmp<T>::getfvpart1_full;
      getfvpart1_trick = &NJetAmp<T>::getfvpart1_full;
      break;

    case COLOR_LC:
      getfvpart1_normal = &NJetAmp<T>::getfvpart1_lc;
      getfvpart1_trick = &NJetAmp<T>::getfvpart1_lc;
      break;

    case COLOR_SLC:
      getfvpart1_normal = &NJetAmp<T>::getfvpart1_slc;
      getfvpart1_trick = &NJetAmp<T>::getfvpart1_slc;
      break;

    default:
      getfvpart1_normal = &NJetAmp<T>::getfvpart1_zero;
      getfvpart1_trick = &NJetAmp<T>::getfvpart1_zero;
      break;
  }
  for (unsigned i=0; i<allfvpart1.size(); i++) {
    allfvpart1[i] = LoopValue();
  }
  return true;
}

template <typename T>
void NJetAmp<T>::getfvpart1_zero(const int /*fv*/, LoopValue* fvpart)
{
  for (int i=0; i<CC; i++) {
    fvpart[i] = LoopValue();
  }
}

template <typename T>
void NJetAmp<T>::getfvpart1_zero(const int /*fv*/, LoopResult<T>* fvpart)
{
  for (int i=0; i<CC; i++) {
    fvpart[i] = LoopResult<T>();
  }
}

// born

template <typename T>
void NJetAmp<T>::born_part_trickfill()
{
  if (cached0 == CACHED0_NONE) {
    for (int h = 0; h < HS/2; h++) {
      const int hC = h + HS/2;
      setHelicity(getHelicity(h));

      for (int fv=0; fv<mFC; fv++) {
        if (fvZero[fv]) continue;
        getfvpart0(fv, allfvpartarr0[fv], allfvpartarr0[FC+fv]);
      }

      born_part0(&allpart0[h*C0], allfvpartarr0);
      born_part0(&allpart0[hC*C0], allfvpartarr0, FC);
    }
    cached0 = CACHED0_TRICK;
  }
}

template <typename T>
void NJetAmp<T>::born_part_fullfill()
{
  const int hlow = (cached0 == CACHED0_TRICK ? HS/2 : 0);
  if (cached0 != CACHED0_FULL) {
    for (int h = hlow; h < HS; h++) {
      setHelicity(getHelicity(h));

      for (int fv=0; fv<mFC; fv++) {
        if (fvZero[fv]) continue;
        getfvpart0(fv, allfvpartarr0[fv]);
      }

      born_part0(&allpart0[h*C0], allfvpartarr0);
    }
    cached0 = CACHED0_FULL;
  }
}

template <typename T>
T NJetAmp<T>::born()
{
  born_part_fill();

  T ans = T();
  for (int h = 0; h < HS; h++) {
    ans += born_colsum(&allpart0[h*C0]);
  }
  return ans;
}

template <typename T>
T NJetAmp<T>::born(const int* h_)
{
  setHelicity(h_);

  for (int fv=0; fv<mFC; fv++) {
    if (fvZero[fv]) continue;
    getfvpart0(fv, allfvpartarr0[fv]);
  }

  born_part0(&allpart0[0], allfvpartarr0);
  return born_colsum(&allpart0[0]);
}

template <typename T>
void NJetAmp<T>::born_part0(TreeValue* part0, const std::vector<TreeValue*>& fvpartarr0, const int inputoffs)
{
  for (int col=0; col<C0; col++) {
    part0[col] = TreeValue();
  }

  for (int fv=0; fv<mFC; fv++) {
    if (fvZero[fv]) continue;
    const TreeValue* fvpart0 = fvpartarr0[fv+inputoffs];
    const T sign = T(fvsign[fv]);
    for (int col=0; col<C0; col++) {
      part0[getFvcol(fv, col)] += sign*fvpart0[col];
    }
  }
}

template <typename T>
T NJetAmp<T>::born_colsum(const TreeValue* part0)
{
  TreeValue res = TreeValue();
  for (int c=0; c<C0; c++) {
    TreeValue row = TreeValue();
    for (int r=0; r<C0; r++) {
      row += part0[r]*Colmat(r, c);
    }
    res += row*conj(part0[c]);
  }
  res *= bornFactor;
  return res.real();
}

// colour-correlated

template <typename T>
T NJetAmp<T>::born_ccij(int i, int j)
{
  born_part_fill();

  T ans = T();
  for (int h = 0; h < HS; h++) {
    ans += born_ccij_colsum(i, j, &allpart0[h*C0]);
  }
  return ans;
}

template <typename T>
T NJetAmp<T>::born_ccij(const int* h_, int i, int j)
{
  setHelicity(h_);

  for (int fv=0; fv<mFC; fv++) {
    if (fvZero[fv]) continue;
    getfvpart0(fv, allfvpartarr0[fv]);
  }

  born_part0(&allpart0[0], allfvpartarr0);
  return born_ccij_colsum(i, j, &allpart0[0]);
}

template <typename T>
T NJetAmp<T>::born_ccij_colsum(int i, int j, const TreeValue* part0)
{
  setColmatcc(i, j);
  TreeValue res = TreeValue();
  for (int c=0; c<C0; c++) {
    TreeValue row = TreeValue();
    for (int r=0; r<C0; r++) {
      row += part0[r]*Colmatcc(r, c);
    }
    res += row*conj(part0[c]);
  }
  res *= bornccFactor;
  return Signcc(i, j)*res.real();
}

template <typename T>
void NJetAmp<T>::born_cc(T* cc_arr)
{
  born_part_fill();

  for (int h = 0; h < HS; h++) {
    born_cc_colsum(&allpart0[h*C0], cc_arr, h == 0);
  }
}

template <typename T>
void NJetAmp<T>::born_cc(const int* h_, T* cc_arr)
{
  setHelicity(h_);

  for (int fv=0; fv<mFC; fv++) {
    if (fvZero[fv]) continue;
    getfvpart0(fv, allfvpartarr0[fv]);
  }

  born_part0(&allpart0[0], allfvpartarr0);
  born_cc_colsum(&allpart0[0], cc_arr);
}

template <typename T>
void NJetAmp<T>::born_cc_colsum(const TreeValue* part0, T* cc_arr, bool clear)
{
  if (clear) {
    for (int i=0; i<NN*(NN-1)/2; i++) {
      cc_arr[i] = T();
    }
  }
  for (int i=0; i<NN; i++) {
    for (int j=i+1; j<NN; j++) {
      cc_arr[nis(i,j)] += born_ccij_colsum(i, j, part0);
    }
  }
}

// spin-correlated

template <typename T>
typename NJetAmp<T>::TreeValue NJetAmp<T>::born_scij(int i, int j)
{
  if (getFlav(0, i) != 0) return TreeValue();

  born_part_fullfill(); // born_part_fill();

  TreeValue amp_pm = TreeValue();

  const int ihel = 1 << i;
  for (int h = 0; h < HS; h++) {
    const unsigned idx = maphsint[h];
    const int hC = mapinths[idx ^ ihel];
    if (hC < 0) continue;
    if (idx & ihel) {
      amp_pm += born_ccij_colsum2(i, j, &allpart0[h*C0], &allpart0[hC*C0]);
    }
  }

  return amp_pm;
}

template <typename T>
void NJetAmp<T>::born_sc(TreeValue* sc_arr)
{
  for (int i=0; i<NN; i++) {
    for (int j=0; j<NN; j++) {
      sc_arr[i+NN*j] = born_scij(i, j);
    }
  }
}

template <typename T>
typename NJetAmp<T>::TreeValue
NJetAmp<T>::born_ccij_colsum2(int i, int j, const TreeValue* part0, const TreeValue* part0c)
{
  setColmatcc(i, j);
  TreeValue res = TreeValue();
  for (int c=0; c<C0; c++) {
    TreeValue row = TreeValue();
    for (int r=0; r<C0; r++) {
      row += part0[r]*Colmatcc(r, c);
    }
    res += row*conj(part0c[c]);
  }
  res *= bornccFactor;
  return Signcc(i, j)*res;
}

// cs-spin-correlated

template <typename T>
void NJetAmp<T>::born_csi(int i, TreeValue* cs_arr)
{
  if (getFlav(0, i) != 0) return;

  born_part_fullfill(); // born_part_fill();

  TreeValue amp_pp = TreeValue();
  TreeValue amp_mm = TreeValue();
  TreeValue amp_pm = TreeValue();
  TreeValue amp_mp = TreeValue();

  const int ihel = 1 << i;
  for (int h = 0; h < HS; h++) {
    const unsigned idx = maphsint[h];
    const int hC = mapinths[idx ^ ihel];
    if (hC < 0) continue;
    if (idx & ihel) {
      amp_pp += born_colsum2(&allpart0[h*C0], &allpart0[h*C0]);
      amp_pm += born_colsum2(&allpart0[h*C0], &allpart0[hC*C0]);
    } else {
      amp_mm += born_colsum2(&allpart0[h*C0], &allpart0[h*C0]);
      amp_mp += born_colsum2(&allpart0[h*C0], &allpart0[hC*C0]);
    }
  }

  const typename NGluon2<T>::RealParticle& gluon = ngluons[0]->getExtParticle(i);
  const SpnMatrix<T> ans =
        amp_pp*ContractMatrix(gluon.getCachedPol(1).conj(), gluon.getCachedPol(1))
      + amp_pm*ContractMatrix(gluon.getCachedPol(1).conj(), gluon.getCachedPol(-1))
      + amp_mm*ContractMatrix(gluon.getCachedPol(-1).conj(), gluon.getCachedPol(-1))
      + amp_mp*ContractMatrix(gluon.getCachedPol(-1).conj(), gluon.getCachedPol(1));

  for (int i = 0; i < 16; i++) {
    cs_arr[i] = ans.data(i);
  }
}

template <typename T>
void NJetAmp<T>::born_cs(TreeValue* cs_arr)
{
  for (int i = 0; i < NN; i++) {
    born_csi(i, &cs_arr[16*i]);
  }
}

template <typename T>
typename NJetAmp<T>::TreeValue
NJetAmp<T>::born_colsum2(const TreeValue* part0, const TreeValue* part0c)
{
  TreeValue res = TreeValue();
  for (int c=0; c<C0; c++) {
    TreeValue row = TreeValue();
    for (int r=0; r<C0; r++) {
      row += part0[r]*Colmat(r, c);
    }
    res += row*conj(part0c[c]);
  }
  res *= bornFactor;
  return res;
}

// virtuals

template <typename T>
typename NJetAmp<T>::LoopValue
NJetAmp<T>::virt_tricksum()
{
  LoopValue ans = LoopValue();
  for (int h = 0; h < HS/2; h++) {
    const int hC = h + HS/2;
    setHelicity(getHelicity(h));

    for (int fv=0; fv<mFC; fv++) {
      if (fvZero[fv]) continue;
      getfvpart1(fv, reinterpret_cast<LoopResult<T>*>(allfvpartarr1[fv]));
      getfvpart0(fv, allfvpartarr0[fv], allfvpartarr0[FC+fv]);
    }

    born_part0(&allpart0[h*C0], allfvpartarr0);
    ans += virt_colsum(&allpart0[h*C0], allfvpartarr1, 2, 0);

    born_part0(&allpart0[hC*C0], allfvpartarr0, FC);
    ans += virt_colsum(&allpart0[hC*C0], allfvpartarr1, 2, 1);
  }
  cached0 = CACHED0_TRICK;
  return ans;
}

template <typename T>
typename NJetAmp<T>::LoopValue
NJetAmp<T>::virt_fullsum()
{
  LoopValue ans = LoopValue();
  for (int h = 0; h < HS; h++) {
    setHelicity(getHelicity(h));

    for (int fv=0; fv<mFC; fv++) {
      if (fvZero[fv]) continue;
      getfvpart1(fv, allfvpartarr1[fv]);
      getfvpart0(fv, allfvpartarr0[fv]);
    }

    born_part0(&allpart0[h*C0], allfvpartarr0);
    ans += virt_colsum(&allpart0[h*C0], allfvpartarr1);
  }
  cached0 = CACHED0_FULL;
  return ans;
}

template <typename T>
typename NJetAmp<T>::LoopValue
NJetAmp<T>::virt(const int* h_)
{
  setHelicity(h_);

  for (int fv=0; fv<mFC; fv++) {
    if (fvZero[fv]) continue;
    getfvpart1(fv, allfvpartarr1[fv]);
    getfvpart0(fv, allfvpartarr0[fv]);
  }

  born_part0(&allpart0[0], allfvpartarr0);
  return virt_colsum(&allpart0[0], allfvpartarr1);
}

template <typename T>
typename NJetAmp<T>::LoopValue
NJetAmp<T>::virt_colsum(const TreeValue* part0, const std::vector<LoopValue*>& fvpartarr1,
                        const int inputstep, const int inputoffs)
{
  LoopValue* part1 = &allpart1[0];
  for (int col=0; col<CC; col++) {
    part1[col] = LoopValue();
  }
  for (int fv=0; fv<mFC; fv++) {
    if (fvZero[fv]) continue;
    const T sign = T(fvsign[fv]);
    const LoopValue* fvpart1 = fvpartarr1[fv];
    for (int col=0; col<CC; col++) {
      part1[getFvcol(fv, col)] += sign*fvpart1[inputstep*col+inputoffs];
    }
  }
  LoopValue res = LoopValue();
  for (int c=0; c<C0; c++) {
    LoopValue row = LoopValue();
    for (int r=0; r<CC; r++) {
      row += part1[r]*Colmat(r, c);
    }
    res += row*conj(part0[c]);
  }
  res *= loopFactor;
  return res.real();
}

// desymmetrized sums

template <typename T>
typename NJetAmp<T>::LoopValue
NJetAmp<T>::virt_dstricksum()
{
  assert(mFC == 1);
  const int fv = 0;

  LoopValue ans = LoopValue();
  for (int h = 0; h < HS/2; h++) {
    const int hC = h + HS/2;
    setHelicity(getHelicity(h));

    getfvpart1ds(fv, reinterpret_cast<LoopResult<T>*>(allfvpartarr1[fv]));
    getfvpart0(fv, allfvpartarr0[fv], allfvpartarr0[FC+fv]);

    ans += virt_dscolsum(allfvpartarr0[fv], allfvpartarr1[fv], 2, 0);
    ans += virt_dscolsum(allfvpartarr0[FC+fv], allfvpartarr1[fv], 2, 1);

    born_part0(&allpart0[h*C0], allfvpartarr0);       // fill tree cache
    born_part0(&allpart0[hC*C0], allfvpartarr0, FC);  // fill treeC cache
  }
  cached0 = CACHED0_TRICK;
  return ans;
}

template <typename T>
typename NJetAmp<T>::LoopValue
NJetAmp<T>::virt_dsfullsum()
{
  assert(mFC == 1);
  const int fv = 0;

  LoopValue ans = LoopValue();
  for (int h = 0; h < HS; h++) {
    setHelicity(getHelicity(h));

    getfvpart1ds(fv, allfvpartarr1[fv]);
    getfvpart0(fv, allfvpartarr0[fv]);
    ans += virt_dscolsum(allfvpartarr0[fv], allfvpartarr1[fv]);

    born_part0(&allpart0[h*C0], allfvpartarr0);  // fill tree cache
  }
  cached0 = CACHED0_FULL;
  return ans;
}

template <typename T>
typename NJetAmp<T>::LoopValue
NJetAmp<T>::virtds(const int* h_)
{
  assert(mFC == 1);
  const int fv = 0;

  LoopValue ans = LoopValue();
  setHelicity(h_);

  getfvpart1ds(fv, allfvpartarr1[fv]);
  getfvpart0(fv, allfvpartarr0[fv]);
  ans += virt_dscolsum(allfvpartarr0[fv], allfvpartarr1[fv]);

  return ans;
}

template <typename T>
typename NJetAmp<T>::LoopValue
NJetAmp<T>::virt_dscolsum(const TreeValue* part0, const LoopValue* part1,
                          const int inputstep, const int inputoffs)
{
  LoopValue res = LoopValue();
  for (int c=0; c<C0; c++) {
    LoopValue row = LoopValue();
    for (int r=0; r<CDS; r++) {
      row += part1[inputstep*r+inputoffs]*ColmatDS(r, c);
    }
    res += row*conj(part0[c]);
  }
  res *= loopFactor;
  return res.real();
}

#ifdef USE_SD
  template class NJetAmp<double>;
#endif
#ifdef USE_DD
  template class NJetAmp<dd_real>;
#endif
#ifdef USE_QD
  template class NJetAmp<qd_real>;
#endif
#ifdef USE_VC
  template class NJetAmp<Vc::double_v>;
  template void NJetAmp<Vc::double_v>::setMomenta(const MOM<double>*);
  template void NJetAmp<Vc::double_v>::setMomenta(const std::vector<MOM<double> >&);
#endif
