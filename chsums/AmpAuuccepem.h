
class AmpsAuuccepem {
  public:
    typedef complex<double> (AmpsAuuccepem::*HelAmp)(const MOM<double>* p);

    AmpsAuuccepem() : se2(0.), mZ(0.), gZ(0.)
  {
    // for gamma*
    double Qu = 2./3;
    xVeL = -1.;
    xVeR = -1.;
    xVuL = Qu;
    xVuR = Qu;

    initfptrs();
  }
    AmpsAuuccepem(double se2_, double mZ_, double gZ_) : se2(se2_), mZ(mZ_), gZ(gZ_)
  {
    // for Z
    double se = sqrt(se2);
    double ce2 = 1.-se2;
    double ce = sqrt(ce2);

    double Qu = 2./3;
    double Qe = -1.;
    xVeL = (-0.5 - Qe*se2)/(se*ce);
    xVeR = -Qe*se/ce;

    xVuL = (0.5 - Qu*se2)/(se*ce);
    xVuR = -Qu*se/ce;
    initfptrs();
  }

    void initfptrs() {
      Auuccepem[0] = &AmpsAuuccepem::Auuccepem0;
      Auuccepem[1] = &AmpsAuuccepem::Auuccepem1;
      Auuccepem[2] = &AmpsAuuccepem::Auuccepem2;
      Auuccepem[3] = &AmpsAuuccepem::Auuccepem3;
      Auuccepem[4] = &AmpsAuuccepem::Auuccepem4;
      Auuccepem[5] = &AmpsAuuccepem::Auuccepem5;
      Auuccepem[6] = &AmpsAuuccepem::Auuccepem6;
      Auuccepem[7] = &AmpsAuuccepem::Auuccepem7;
      Auuccepem[8] = &AmpsAuuccepem::Auuccepem8;
      Auuccepem[9] = &AmpsAuuccepem::Auuccepem9;
      Auuccepem[10] = &AmpsAuuccepem::Auuccepem10;
      Auuccepem[11] = &AmpsAuuccepem::Auuccepem11;
      Auuccepem[12] = &AmpsAuuccepem::Auuccepem12;
      Auuccepem[13] = &AmpsAuuccepem::Auuccepem13;
      Auuccepem[14] = &AmpsAuuccepem::Auuccepem14;
      Auuccepem[15] = &AmpsAuuccepem::Auuccepem15;
      Auuccepem[16] = &AmpsAuuccepem::Auuccepem16;
      Auuccepem[17] = &AmpsAuuccepem::Auuccepem17;
      Auuccepem[18] = &AmpsAuuccepem::Auuccepem18;
      Auuccepem[19] = &AmpsAuuccepem::Auuccepem19;
      Auuccepem[20] = &AmpsAuuccepem::Auuccepem20;
      Auuccepem[21] = &AmpsAuuccepem::Auuccepem21;
      Auuccepem[22] = &AmpsAuuccepem::Auuccepem22;
      Auuccepem[23] = &AmpsAuuccepem::Auuccepem23;
      Auuccepem[24] = &AmpsAuuccepem::Auuccepem24;
      Auuccepem[25] = &AmpsAuuccepem::Auuccepem25;
      Auuccepem[26] = &AmpsAuuccepem::Auuccepem26;
      Auuccepem[27] = &AmpsAuuccepem::Auuccepem27;
      Auuccepem[28] = &AmpsAuuccepem::Auuccepem28;
      Auuccepem[29] = &AmpsAuuccepem::Auuccepem29;
      Auuccepem[30] = &AmpsAuuccepem::Auuccepem30;
      Auuccepem[31] = &AmpsAuuccepem::Auuccepem31;
      Auuccepem[32] = &AmpsAuuccepem::Auuccepem32;
      Auuccepem[33] = &AmpsAuuccepem::Auuccepem33;
      Auuccepem[34] = &AmpsAuuccepem::Auuccepem34;
      Auuccepem[35] = &AmpsAuuccepem::Auuccepem35;
      Auuccepem[36] = &AmpsAuuccepem::Auuccepem36;
      Auuccepem[37] = &AmpsAuuccepem::Auuccepem37;
      Auuccepem[38] = &AmpsAuuccepem::Auuccepem38;
      Auuccepem[39] = &AmpsAuuccepem::Auuccepem39;
      Auuccepem[40] = &AmpsAuuccepem::Auuccepem40;
      Auuccepem[41] = &AmpsAuuccepem::Auuccepem41;
      Auuccepem[42] = &AmpsAuuccepem::Auuccepem42;
      Auuccepem[43] = &AmpsAuuccepem::Auuccepem43;
      Auuccepem[44] = &AmpsAuuccepem::Auuccepem44;
      Auuccepem[45] = &AmpsAuuccepem::Auuccepem45;
      Auuccepem[46] = &AmpsAuuccepem::Auuccepem46;
      Auuccepem[47] = &AmpsAuuccepem::Auuccepem47;
      Auuccepem[48] = &AmpsAuuccepem::Auuccepem48;
      Auuccepem[49] = &AmpsAuuccepem::Auuccepem49;
      Auuccepem[50] = &AmpsAuuccepem::Auuccepem50;
      Auuccepem[51] = &AmpsAuuccepem::Auuccepem51;
      Auuccepem[52] = &AmpsAuuccepem::Auuccepem52;
      Auuccepem[53] = &AmpsAuuccepem::Auuccepem53;
      Auuccepem[54] = &AmpsAuuccepem::Auuccepem54;
      Auuccepem[55] = &AmpsAuuccepem::Auuccepem55;
      Auuccepem[56] = &AmpsAuuccepem::Auuccepem56;
      Auuccepem[57] = &AmpsAuuccepem::Auuccepem57;
      Auuccepem[58] = &AmpsAuuccepem::Auuccepem58;
      Auuccepem[59] = &AmpsAuuccepem::Auuccepem59;
      Auuccepem[60] = &AmpsAuuccepem::Auuccepem60;
      Auuccepem[61] = &AmpsAuuccepem::Auuccepem61;
      Auuccepem[62] = &AmpsAuuccepem::Auuccepem62;
      Auuccepem[63] = &AmpsAuuccepem::Auuccepem63;
    }

    complex<double> getAmp(int* h, const MOM<double>* p)
    {
      int tp=1;
      int hidx = 0;
      for (int j=0; j<6; j++) {
        hidx += (h[j]+1)/2*tp;
        tp *= 2;
      }
      return (this->*Auuccepem[hidx])(p);
    }

  private:
    HelAmp Auuccepem[64];

    complex<double> Auuccepem0(const MOM<double>* p);
    complex<double> Auuccepem1(const MOM<double>* p);
    complex<double> Auuccepem2(const MOM<double>* p);
    complex<double> Auuccepem3(const MOM<double>* p);
    complex<double> Auuccepem4(const MOM<double>* p);
    complex<double> Auuccepem5(const MOM<double>* p);
    complex<double> Auuccepem6(const MOM<double>* p);
    complex<double> Auuccepem7(const MOM<double>* p);
    complex<double> Auuccepem8(const MOM<double>* p);
    complex<double> Auuccepem9(const MOM<double>* p);
    complex<double> Auuccepem10(const MOM<double>* p);
    complex<double> Auuccepem11(const MOM<double>* p);
    complex<double> Auuccepem12(const MOM<double>* p);
    complex<double> Auuccepem13(const MOM<double>* p);
    complex<double> Auuccepem14(const MOM<double>* p);
    complex<double> Auuccepem15(const MOM<double>* p);
    complex<double> Auuccepem16(const MOM<double>* p);
    complex<double> Auuccepem17(const MOM<double>* p);
    complex<double> Auuccepem18(const MOM<double>* p);
    complex<double> Auuccepem19(const MOM<double>* p);
    complex<double> Auuccepem20(const MOM<double>* p);
    complex<double> Auuccepem21(const MOM<double>* p);
    complex<double> Auuccepem22(const MOM<double>* p);
    complex<double> Auuccepem23(const MOM<double>* p);
    complex<double> Auuccepem24(const MOM<double>* p);
    complex<double> Auuccepem25(const MOM<double>* p);
    complex<double> Auuccepem26(const MOM<double>* p);
    complex<double> Auuccepem27(const MOM<double>* p);
    complex<double> Auuccepem28(const MOM<double>* p);
    complex<double> Auuccepem29(const MOM<double>* p);
    complex<double> Auuccepem30(const MOM<double>* p);
    complex<double> Auuccepem31(const MOM<double>* p);
    complex<double> Auuccepem32(const MOM<double>* p);
    complex<double> Auuccepem33(const MOM<double>* p);
    complex<double> Auuccepem34(const MOM<double>* p);
    complex<double> Auuccepem35(const MOM<double>* p);
    complex<double> Auuccepem36(const MOM<double>* p);
    complex<double> Auuccepem37(const MOM<double>* p);
    complex<double> Auuccepem38(const MOM<double>* p);
    complex<double> Auuccepem39(const MOM<double>* p);
    complex<double> Auuccepem40(const MOM<double>* p);
    complex<double> Auuccepem41(const MOM<double>* p);
    complex<double> Auuccepem42(const MOM<double>* p);
    complex<double> Auuccepem43(const MOM<double>* p);
    complex<double> Auuccepem44(const MOM<double>* p);
    complex<double> Auuccepem45(const MOM<double>* p);
    complex<double> Auuccepem46(const MOM<double>* p);
    complex<double> Auuccepem47(const MOM<double>* p);
    complex<double> Auuccepem48(const MOM<double>* p);
    complex<double> Auuccepem49(const MOM<double>* p);
    complex<double> Auuccepem50(const MOM<double>* p);
    complex<double> Auuccepem51(const MOM<double>* p);
    complex<double> Auuccepem52(const MOM<double>* p);
    complex<double> Auuccepem53(const MOM<double>* p);
    complex<double> Auuccepem54(const MOM<double>* p);
    complex<double> Auuccepem55(const MOM<double>* p);
    complex<double> Auuccepem56(const MOM<double>* p);
    complex<double> Auuccepem57(const MOM<double>* p);
    complex<double> Auuccepem58(const MOM<double>* p);
    complex<double> Auuccepem59(const MOM<double>* p);
    complex<double> Auuccepem60(const MOM<double>* p);
    complex<double> Auuccepem61(const MOM<double>* p);
    complex<double> Auuccepem62(const MOM<double>* p);
    complex<double> Auuccepem63(const MOM<double>* p);

    double se2, mZ, gZ, xVeL, xVeR, xVuR, xVuL;
};

complex<double> AmpsAuuccepem::Auuccepem0(const MOM<double>* p)
{
  // -1 -1 -1 -1 -1 -1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem1(const MOM<double>* p)
{
  // 1 -1 -1 -1 -1 -1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem2(const MOM<double>* p)
{
  // -1 1 -1 -1 -1 -1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem3(const MOM<double>* p)
{
  // 1 1 -1 -1 -1 -1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem4(const MOM<double>* p)
{
  // -1 -1 1 -1 -1 -1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem5(const MOM<double>* p)
{
  // 1 -1 1 -1 -1 -1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem6(const MOM<double>* p)
{
  // -1 1 1 -1 -1 -1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem7(const MOM<double>* p)
{
  // 1 1 1 -1 -1 -1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem8(const MOM<double>* p)
{
  // -1 -1 -1 1 -1 -1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem9(const MOM<double>* p)
{
  // 1 -1 -1 1 -1 -1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem10(const MOM<double>* p)
{
  // -1 1 -1 1 -1 -1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem11(const MOM<double>* p)
{
  // 1 1 -1 1 -1 -1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem12(const MOM<double>* p)
{
  // -1 -1 1 1 -1 -1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem13(const MOM<double>* p)
{
  // 1 -1 1 1 -1 -1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem14(const MOM<double>* p)
{
  // -1 1 1 1 -1 -1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem15(const MOM<double>* p)
{
  // 1 1 1 1 -1 -1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem16(const MOM<double>* p)
{
  // -1 -1 -1 -1 1 -1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem17(const MOM<double>* p)
{
  // 1 -1 -1 -1 1 -1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem18(const MOM<double>* p)
{
  // -1 1 -1 -1 1 -1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem19(const MOM<double>* p)
{
  // 1 1 -1 -1 1 -1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem20(const MOM<double>* p)
{
  // -1 -1 1 -1 1 -1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem21(const MOM<double>* p)
{
  // 1 -1 1 -1 1 -1
  return 1./(S(p[3] + p[2]))/(S(p[5] + p[4] + p[0]))*xspA(p[1],p[3])*xspB(p[0],p[4])*xspAB(p[5],p[4] + p[0],p[2])/(mZ*gZ*i_ - mZ*mZ + S(p[5] + p[4]))*i_*xVeL*xVuL
       + 1./(S(p[3] + p[2]))/(S(p[3] + p[2] + p[0]))*xspA(p[1],p[5])*xspB(p[0],p[2])*xspAB(p[3],p[2] + p[0],p[4])/(mZ*gZ*i_ - mZ*mZ + S(p[5] + p[4]))*i_*xVeL*xVuL;
}
complex<double> AmpsAuuccepem::Auuccepem22(const MOM<double>* p)
{
  // -1 1 1 -1 1 -1
  return 1./(S(p[3] + p[2]))/(S(p[5] + p[4] + p[0]))*xspA(p[0],p[5])*xspB(p[1],p[2])*xspAB(p[3],p[5] + p[4] + p[0],p[4])/(mZ*gZ*i_ - mZ*mZ + S(p[5] + p[4]))*i_*xVeL*xVuR
       + 1./(S(p[3] + p[2]))/(S(p[3] + p[2] + p[0]))*xspA(p[0],p[3])*xspB(p[1],p[4])*xspAB(p[5],p[3] + p[2] + p[0],p[2])/(mZ*gZ*i_ - mZ*mZ + S(p[5] + p[4]))*i_*xVeL*xVuR;
}
complex<double> AmpsAuuccepem::Auuccepem23(const MOM<double>* p)
{
  // 1 1 1 -1 1 -1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem24(const MOM<double>* p)
{
  // -1 -1 -1 1 1 -1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem25(const MOM<double>* p)
{
  // 1 -1 -1 1 1 -1
  return 1./(S(p[3] + p[2]))/(S(p[5] + p[4] + p[0]))*xspA(p[1],p[2])*xspB(p[0],p[4])*xspAB(p[5],p[4] + p[0],p[3])/(mZ*gZ*i_ - mZ*mZ + S(p[5] + p[4]))*i_*xVeL*xVuL
       + 1./(S(p[3] + p[2]))/(S(p[3] + p[2] + p[0]))*xspA(p[1],p[5])*xspB(p[0],p[3])*xspAB(p[2],p[3] + p[0],p[4])/(mZ*gZ*i_ - mZ*mZ + S(p[5] + p[4]))*i_*xVeL*xVuL;

}
complex<double> AmpsAuuccepem::Auuccepem26(const MOM<double>* p)
{
  // -1 1 -1 1 1 -1
  return 1./(S(p[3] + p[2]))/(S(p[5] + p[4] + p[0]))*xspA(p[0],p[5])*xspB(p[1],p[3])*xspAB(p[2],p[5] + p[4] + p[0],p[4])/(mZ*gZ*i_ - mZ*mZ + S(p[5] + p[4]))*i_*xVeL*xVuR
       + 1./(S(p[3] + p[2]))/(S(p[3] + p[2] + p[0]))*xspA(p[0],p[2])*xspB(p[1],p[4])*xspAB(p[5],p[2] + p[0],p[3])/(mZ*gZ*i_ - mZ*mZ + S(p[5] + p[4]))*i_*xVeL*xVuR;
}
complex<double> AmpsAuuccepem::Auuccepem27(const MOM<double>* p)
{
  // 1 1 -1 1 1 -1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem28(const MOM<double>* p)
{
  // -1 -1 1 1 1 -1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem29(const MOM<double>* p)
{
  // 1 -1 1 1 1 -1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem30(const MOM<double>* p)
{
  // -1 1 1 1 1 -1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem31(const MOM<double>* p)
{
  // 1 1 1 1 1 -1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem32(const MOM<double>* p)
{
  // -1 -1 -1 -1 -1 1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem33(const MOM<double>* p)
{
  // 1 -1 -1 -1 -1 1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem34(const MOM<double>* p)
{
  // -1 1 -1 -1 -1 1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem35(const MOM<double>* p)
{
  // 1 1 -1 -1 -1 1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem36(const MOM<double>* p)
{
  // -1 -1 1 -1 -1 1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem37(const MOM<double>* p)
{
  // 1 -1 1 -1 -1 1
  return 1./(S(p[3] + p[2]))/(S(p[5] + p[4] + p[0]))*xspA(p[1],p[3])*xspB(p[0],p[5])*xspAB(p[4],p[5] + p[0],p[2])/(mZ*gZ*i_ - mZ*mZ + S(p[5] + p[4]))*i_*xVeR*xVuL
       + 1./(S(p[3] + p[2]))/(S(p[3] + p[2] + p[0]))*xspA(p[1],p[4])*xspB(p[0],p[2])*xspAB(p[3],p[2] + p[0],p[5])/(mZ*gZ*i_ - mZ*mZ + S(p[5] + p[4]))*i_*xVeR*xVuL;
}
complex<double> AmpsAuuccepem::Auuccepem38(const MOM<double>* p)
{
  // -1 1 1 -1 -1 1
  return 1./(S(p[3] + p[2]))/(S(p[5] + p[4] + p[0]))*xspA(p[0],p[4])*xspB(p[1],p[2])*xspAB(p[3],p[4] + p[0],p[5])/(mZ*gZ*i_ - mZ*mZ + S(p[5] + p[4]))*i_*xVeR*xVuR
       + 1./(S(p[3] + p[2]))/(S(p[3] + p[2] + p[0]))*xspA(p[0],p[3])*xspB(p[1],p[5])*xspAB(p[4],p[3] + p[2] + p[0],p[2])/(mZ*gZ*i_ - mZ*mZ + S(p[5] + p[4]))*i_*xVeR*xVuR;
}
complex<double> AmpsAuuccepem::Auuccepem39(const MOM<double>* p)
{
  // 1 1 1 -1 -1 1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem40(const MOM<double>* p)
{
  // -1 -1 -1 1 -1 1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem41(const MOM<double>* p)
{
  // 1 -1 -1 1 -1 1
  return 1./(S(p[3] + p[2]))/(S(p[5] + p[4] + p[0]))*xspA(p[1],p[2])*xspB(p[0],p[5])*xspAB(p[4],p[5] + p[0],p[3])/(mZ*gZ*i_ - mZ*mZ + S(p[5] + p[4]))*i_*xVeR*xVuL
       + 1./(S(p[3] + p[2]))/(S(p[3] + p[2] + p[0]))*xspA(p[1],p[4])*xspB(p[0],p[3])*xspAB(p[2],p[3] + p[0],p[5])/(mZ*gZ*i_ - mZ*mZ + S(p[5] + p[4]))*i_*xVeR*xVuL;
}
complex<double> AmpsAuuccepem::Auuccepem42(const MOM<double>* p)
{
  // -1 1 -1 1 -1 1
  return 1./(S(p[3] + p[2]))/(S(p[5] + p[4] + p[0]))*xspA(p[0],p[4])*xspB(p[1],p[3])*xspAB(p[2],p[4] + p[0],p[5])/(mZ*gZ*i_ - mZ*mZ + S(p[5] + p[4]))*i_*xVeR*xVuR
       + 1./(S(p[3] + p[2]))/(S(p[3] + p[2] + p[0]))*xspA(p[0],p[2])*xspB(p[1],p[5])*xspAB(p[4],p[2] + p[0],p[3])/(mZ*gZ*i_ - mZ*mZ + S(p[5] + p[4]))*i_*xVeR*xVuR;
}
complex<double> AmpsAuuccepem::Auuccepem43(const MOM<double>* p)
{
  // 1 1 -1 1 -1 1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem44(const MOM<double>* p)
{
  // -1 -1 1 1 -1 1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem45(const MOM<double>* p)
{
  // 1 -1 1 1 -1 1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem46(const MOM<double>* p)
{
  // -1 1 1 1 -1 1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem47(const MOM<double>* p)
{
  // 1 1 1 1 -1 1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem48(const MOM<double>* p)
{
  // -1 -1 -1 -1 1 1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem49(const MOM<double>* p)
{
  // 1 -1 -1 -1 1 1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem50(const MOM<double>* p)
{
  // -1 1 -1 -1 1 1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem51(const MOM<double>* p)
{
  // 1 1 -1 -1 1 1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem52(const MOM<double>* p)
{
  // -1 -1 1 -1 1 1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem53(const MOM<double>* p)
{
  // 1 -1 1 -1 1 1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem54(const MOM<double>* p)
{
  // -1 1 1 -1 1 1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem55(const MOM<double>* p)
{
  // 1 1 1 -1 1 1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem56(const MOM<double>* p)
{
  // -1 -1 -1 1 1 1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem57(const MOM<double>* p)
{
  // 1 -1 -1 1 1 1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem58(const MOM<double>* p)
{
  // -1 1 -1 1 1 1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem59(const MOM<double>* p)
{
  // 1 1 -1 1 1 1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem60(const MOM<double>* p)
{
  // -1 -1 1 1 1 1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem61(const MOM<double>* p)
{
  // 1 -1 1 1 1 1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem62(const MOM<double>* p)
{
  // -1 1 1 1 1 1
  return 0.;
}
complex<double> AmpsAuuccepem::Auuccepem63(const MOM<double>* p)
{
  // 1 1 1 1 1 1
  return 0.;
}
