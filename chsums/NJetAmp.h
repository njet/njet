/*
* chsums/NJetAmp.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef CHSUM_NJETAMP_H
#define CHSUM_NJETAMP_H

#include <bitset>

#include "../ngluon2/NAmp.h"

#include "NJetAnalytic.h"
// template <typename T> class NJetAnalytic;

class NJetAmpTables
{
  public:
    template <class T>
    static
    NJetAmpTables create()
    {
      return NJetAmpTables(T::NN, T::FC, T::C0, T::CC, T::CDS,
                           &T::flav[0][0], T::fvsign,
                           &T::fperm[0][0], &T::fvcol[0][0], T::ccsign,
                           T::colmat, T::NmatLen,
                           &T::colmatcc[0][0], T::NmatccLen,
                           &T::colmatds[0][0], T::NmatDSLen,
                           T::HS, T::HSNN, &T::HSarr[0][0]);
    }

    NJetAmpTables(const int NN_, const int FC_, const int C0_, const int CC_,
                  const int CDS_,
                  const int* flav_, const int* fvsign_,
                  const int* fperm_, const int* fvcol_,
                  const int* ccsign_,
                  const unsigned char* colmat_, const int NmatLen_,
                  const unsigned char* colmatcc_, const int NmatccLen_,
                  const unsigned int* colmatds_, const int NmatDSLen_,
                  const int HS_, const int HSNN_, const int* HSarr_
                 )
    : NN(NN_), FC(FC_), C0(C0_), CC(CC_), CDS(CDS_),
      flav(flav_), fvsign(fvsign_),
      fperm(fperm_), fvcol(fvcol_),
      ccsign(ccsign_),
      colmat(colmat_), NmatLen(NmatLen_),
      colmatcc(colmatcc_), NmatccLen(NmatccLen_),
      colmatds(colmatds_), NmatDSLen(NmatDSLen_),
      HS(HS_), HSNN(HSNN_), HSarr(HSarr_)
    {
      curcolmatcc = 0;
    }

    // symmetric index: (i,j) in [0,1,...], "is" in [0,1,...]
    inline int is(int i, int j) const {
      return ( i<=j ? i+j*(j+1)/2 : j+i*(i+1)/2 );
    }

    // natural symmetric index: (i,j) in [1,2,...], "nis" in [1,2,...]
    // non-diagonal symmetric index: i != j, (i,j) in [0,1,...], "nis" in [0,1,...]
    inline int nis(int i, int j) const {
      return ( i<=j ? i+j*(j-1)/2 : j+i*(i-1)/2 );
    }

    inline const int* getFlav(int fv) const { return &flav[NN*fv]; }
    inline const int* getFperm(int fv) const { return &fperm[NN*fv]; }
    inline const int* getFvcol(int fv) const { return &fvcol[CC*fv]; }

    inline int getFlav(int fv, int i) const { return flav[NN*fv + i]; }
    inline int getFperm(int fv, int i) const { return fperm[NN*fv + i]; }
    inline int getFvcol(int fv, int col) const { return fvcol[CC*fv + col]; }

    inline int getColmat(int r, int c) const { return int(colmat[is(r, c)]); }

    inline void setColmatcc(int n1, int n2) {
      curcolmatcc = &colmatcc[(C0*(C0+1)/2)*is(n1, n2)];
    }
    inline int getColmatcc(int r, int c) const { return int(curcolmatcc[is(r, c)]); }

    inline int getColmatDS(int r, int c) const { return int(colmatds[r*C0 + c]); }

    inline const int* getHelicity(int h) const { return &HSarr[h*HSNN]; }

    inline int legsQCD() const { return NN; }
    inline int legsHS() const { return HSNN; }

    static const int DEFAULT_Nc = 3;
    static const int DEFAULT_Nf = 5;

    enum COLORTYPE {
      COLOR_FULL,
      COLOR_LC,
      COLOR_SLC
    };

  protected:
    const int NN;   // coloured legs
    const int FC;   // flavour permutations
    const int C0;   // tree partial amplitudes
    const int CC;   // loop partial amplitudes
    const int CDS;  // mix+qloop desymmetrized primitive amplitudes

    const int* const flav;   // [FC][NN]        // flavour vectors for ngluon
    const int* const fvsign; // [FC]            // flavour channels' signs
    const int* const fperm;  // [FC][NN]        // flavour pemutation matrix
    const int* const fvcol;  // [FC][CC]        // flavour-partial pemutation matrix
    const int* const ccsign; // [NN*(NN+1)/2]   // ccborn signs

    const unsigned char* colmat;   // [CC*(CC+1)/2]               // partial-partial colour factors
    const int NmatLen;  // length of above

    const unsigned char* colmatcc; // [NN*(NN+1)/2][C0*(C0+1)/2]  // ccborn partial-partial factors
    const int NmatccLen;  // length of above

    const unsigned char* curcolmatcc; // [C0*(C0+1)/2]            // pointer to selected colmatcc

    const unsigned int* colmatds; // partial-primitive desymmetrized colour factors
    const int NmatDSLen;   // length of above

    int HS;               // number of non-zero helicities
    const int HSNN;       // all legs
    const int* HSarr;     // [HS*HSNN]    // all helicities
};

class Amp0Static
{
  public:
    static const int CDS = 0;
    static const unsigned int colmatds[1][1];
    static const int NmatDSLen = 0;
};

template <int N>
class AmpNStatic : public Amp0Static
{
  public:
    static const int NN = N;
    static const int HSNN = NN;
};

template <typename T>
class NJetAmp : public NAmp<T>, public NJetAmpTables
{
  public:
    typedef typename NAmp<T>::RealMom RealMom;
    typedef typename vector_traits<T>::scalar ST;

    typedef EpsTriplet<T> LoopValue;
    typedef std::complex<T> TreeValue;

    NJetAmp(const int mFC_, const NJetAmpTables& tables);
    virtual ~NJetAmp();

    void setMuR2(const T rscale);

    void setMomenta(const RealMom* moms);
    void setMomenta(const std::vector<RealMom>& moms);

    template <typename U>
    void setMomenta(const MOM<U>* othermoms);
    template <typename U>
    void setMomenta(const std::vector<MOM<U> >& othermoms);

    void setHelicity(const int* helicity);
    void setHelicity(const std::vector<int>& helicity);

    int legsMOM() const;

    virtual void setNc(const ST Nc_);
    virtual void setNf(const ST Nf_);

    ST getNc() const { return Nc; }
    ST getNf() const { return Nf; }

    T born();
    T born_ccij(int i, int j);
    void born_cc(T* cc_arr);
    TreeValue born_scij(int i, int j);
    void born_sc(TreeValue* sc_arr);
    void born_csi(int i, TreeValue* cs_arr);
    void born_cs(TreeValue* cs_arr);
    virtual LoopValue virt() { return virt_fullsum(); }

    virtual T born(const int* h);
    virtual T born_ccij(const int* h, int i, int j);
    virtual void born_cc(const int* h, T* cc_arr);
    virtual LoopValue virt(const int* h);

    virtual bool setLoopType(int type, int norderL_=0, int norderF_=0);

  protected:
    using NAmp<T>::ngluons;
    using NAmp<T>::setProcess;  // hide from public interface

    virtual void born_part_fill() { born_part_fullfill(); }
    void born_part_fullfill();
    void born_part_trickfill();

    LoopValue virt_fullsum();
    LoopValue virt_tricksum();

    LoopValue virtds(const int* h);
    LoopValue virt_dsfullsum();
    LoopValue virt_dstricksum();

    void born_part0(TreeValue* part0, const std::vector<TreeValue*>& fvpartarr0, const int inputoffs=0);

    T born_colsum(const TreeValue* part0);
    TreeValue born_colsum2(const TreeValue* part0, const TreeValue* part0c);
    T born_ccij_colsum(int i, int j, const TreeValue* part0);
    TreeValue born_ccij_colsum2(int i, int j, const TreeValue* part0, const TreeValue* part0c);
    void born_cc_colsum(const TreeValue* part0, T* cc_arr, bool clear=true);
    LoopValue virt_colsum(const TreeValue* part0, const std::vector<LoopValue*>& fvpartarr1,
                          const int inputstep=1, const int inputoffs=0);

    LoopValue virt_dscolsum(const TreeValue* part0, const LoopValue* part1,
                            const int inputstep=1, const int inputoffs=0);

    void initNc();
    void initHS();
    void setHS(const int hs, const int* hsarr);
    virtual void markZeroFv() {}

    void fvSet(const int fv) { mfv = fv; }

    T Colmat(int r, int c) { return Nmat[getColmat(r, c)]; }
    T Colmatcc(int r, int c) { return Nmatcc[getColmatcc(r, c)]; }
    T Signcc(int n1, int n2) { return T(ccsign[is(n1, n2)]); }
    T ColmatDS(int r, int c) { return NmatDS[getColmatDS(r, c)]; }

    virtual void getfvpart0(const int /*fv*/, TreeValue* /*fvpart*/, TreeValue* fvpartC=0) { (void)fvpartC; }

    // getfvpart1 full-lc-slc
    typedef void (NJetAmp::*t_getfvpart1_normal)(const int, LoopValue*);
    typedef void (NJetAmp::*t_getfvpart1_trick)(const int, LoopResult<T>*);

    t_getfvpart1_normal getfvpart1_normal;
    t_getfvpart1_trick getfvpart1_trick;

    void getfvpart1(const int fv, LoopValue* fvpart) { (this->*getfvpart1_normal)(fv, fvpart); }
    void getfvpart1(const int fv, LoopResult<T>* fvpart) { (this->*getfvpart1_trick)(fv, fvpart); }

    virtual void getfvpart1_full(const int /*fv*/, LoopValue* /*fvpart*/) { }
    virtual void getfvpart1_full(const int /*fv*/, LoopResult<T>* /*fvpart*/) { }

    virtual void getfvpart1_lc(const int /*fv*/, LoopValue* /*fvpart*/) { }
    virtual void getfvpart1_lc(const int /*fv*/, LoopResult<T>* /*fvpart*/) { }

    virtual void getfvpart1_slc(const int /*fv*/, LoopValue* /*fvpart*/) { }
    virtual void getfvpart1_slc(const int /*fv*/, LoopResult<T>* /*fvpart*/) { }

    void getfvpart1_zero(const int fv, LoopValue* fvpart);
    void getfvpart1_zero(const int fv, LoopResult<T>* fvpart);

    bool setLoopTypeNONE(int type, int norderL_, int norderF_);
    bool setLoopTypeLCSLC(int type, int norderL_, int norderF_);

    // getfvpart1 ds
    virtual void getfvpart1ds(const int /*fv*/, LoopValue* /*fvpart*/) { }
    virtual void getfvpart1ds(const int /*fv*/, LoopResult<T>* /*fvpart*/) { }

    int mFC;  // number of fv components in multi-quark process
    int norderL, norderF;  // Nc-order for Mixed and Qloop parts (used in -lc -slc sub-classes)

    int mfv;  // current fv
    int mhel; // current helicity number in HSarr
    int mhelint; // current helicity encoded in binary

    enum {CACHED0_NONE=0, CACHED0_TRICK, CACHED0_FULL};
    int cached0;  // true if tree level partials for all helicities are already available
    void resetCache();

    ST Nc, Nc2, Nc3, Nc4, Nc5, V;
    ST Nf;

    ST bornFactor, loopFactor, bornccFactor;  // common colour factors for colour sums (set in setNc in sub-class)

    std::bitset<16> fvZero;  // which fv-parts are zero
    std::vector<int> vhel;  // copy of helicity array
    std::vector<int> maphsint;  // map helicity index to integer
    std::vector<int> mapinths;  // map integer to helicity index

    std::vector<ST> Nmat;    // index-colour-factor mapping for tree*loop colour matrix (set in setNc in sub-class)
    std::vector<ST> Nmatcc;  // index-colour-factor mapping for treeI*treeJ cc-colour matrix (set in setNc in sub-class)
    std::vector<ST> NmatDS;  // index-colour-factor mapping for desymmetrized sums (uninitialized by default)

    std::vector<TreeValue> allpart0;  // tree partials for all helicities
    std::vector<LoopValue> allpart1;  // loop partials for current helicity

    std::vector<TreeValue> allfvpart0;  // tree fv-partials for current helicity and helicityC
    std::vector<LoopValue> allfvpart1;  // loop fv-partials for current helicity or (helicity, helicityC) tuples

    std::vector<TreeValue*> allfvpartarr0;  // pointers to tree fv-partials in above
    std::vector<LoopValue*> allfvpartarr1;  // pointers to loop fv-partials in above

    // Various primitives to be used by derived classes

    inline TreeValue A0n(int* order);
    inline LoopResult<T> AFn(int* order);
    inline LoopResult<T> ALn(int* order);

    inline TreeValue A0nVqq(int* order);
    inline LoopResult<T> AFnVqq(int* order);
    inline LoopResult<T> ALnVqq(int* order);

    inline TreeValue A0nV(int* order);
    inline LoopResult<T> AFnV(int* order);
    inline LoopResult<T> ALnV(int* order);

    inline TreeValue A0nAA(int* order);
    inline LoopResult<T> AFnAA(int* order);
    inline LoopResult<T> ALnAA(int* order);

    inline TreeValue A0nAA2(int* order);
    inline LoopResult<T> AFnAA2(int* order);
    inline LoopResult<T> ALnAA2(int* order);

    inline LoopResult<T> AFnVax(int* order);
    inline LoopResult<T> AFnAAx(int* order);
    inline LoopResult<T> AFnAAxx(int* order);

    inline TreeValue A0nH(int* order);

    // Analytic stuff
    NJetAnalytic<T>* njetan;
    inline std::complex<T> CyclicSpinorsA(const int* ord) const
    { return const_cast<const NJetAnalytic<T>*>(njetan)->CyclicSpinorsA(ord); }
    inline std::complex<T> CyclicSpinorsB(const int* ord) const
    { return const_cast<const NJetAnalytic<T>*>(njetan)->CyclicSpinorsB(ord); }
    inline std::complex<T> sA(int i, int j) const
    { return const_cast<const NJetAnalytic<T>*>(njetan)->sA(i, j); }
    inline std::complex<T> sB(int i, int j) const
    { return const_cast<const NJetAnalytic<T>*>(njetan)->sB(i, j); }
    inline std::complex<T> sAB(int i, int j) const
    { return const_cast<const NJetAnalytic<T>*>(njetan)->sAB(i, j); }
    inline T lS(int i, int j) const
    { return const_cast<const NJetAnalytic<T>*>(njetan)->lS(i, j); }
    inline T lS(int i, int j, int k) const
    { return const_cast<const NJetAnalytic<T>*>(njetan)->lS(i, j, k); }
    inline int HelicityOrder(const int h, const int* ord) const
    { return const_cast<const NJetAnalytic<T>*>(njetan)->HelicityOrder(h, ord); }

    // rescaling helper functions
    template <typename AMP, typename FUN, typename ARG>
    TreeValue callTree(AMP* /*aptr*/, FUN fun, ARG arg)
    { return njetan->rescaleAmp((static_cast<AMP*>(this)->*fun)(arg)); }

    template <typename AMP, typename FUN, typename ARG>
    LoopResult<T> callLoop(AMP* /*aptr*/, FUN fun, ARG arg)
    { return njetan->rescaleAmp((static_cast<AMP*>(this)->*fun)(arg)); }
};

#endif // CHSUM_NJETAMP_H
