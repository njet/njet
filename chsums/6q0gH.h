/*
* chsums/6q0g.h
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef CHSUM_6Q0GH_H
#define CHSUM_6Q0GH_H

#include "6q0g.h"

class Amp6q0gHStatic : public Amp6q0gStatic
{
  public:
    static const int HS = 8;
    static const int HSNN = NN+1;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp6q0gH : public Amp6q0g<T>
{
    typedef Amp6q0g<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp6q0gH(const T scalefactor, const int mFC=1, const NJetAmpTables& tables=amptables());

    void born_part_fill() { BaseClass::born_part_trickfill(); }

  protected:
    using BaseClass::ngluons;
    using BaseClass::NN;
    using BaseClass::getFlav;
    using BaseClass::getFperm;
    using BaseClass::mfv;
    using BaseClass::fvSet;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp6q0gHStatic>();
    }

    void initProcess(const Flavour<double>& ff);

    virtual TreeValue A0(int p0, int p1, int p2, int p3, int p4, int p5);
    virtual LoopResult<T> AL(int /*p0*/, int /*p1*/, int /*p2*/, int /*p3*/,
                             int /*p4*/, int /*p5*/) { return LoopResult<T>(); }
    virtual LoopResult<T> AF(int /*p0*/, int /*p1*/, int /*p2*/, int /*p3*/,
                             int /*p4*/, int /*p5*/) { return LoopResult<T>(); }
};

class Amp6q0gH2Static : public Amp6q0gHStatic
{
  public:
    static const int HS = 12;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp6q0gH2 : public Amp6q0gH<T>
{
    typedef Amp6q0gH<T> BaseClass;
  public:
    using BaseClass::fvZero;
    using BaseClass::vhel;

    Amp6q0gH2(const T scalefactor)
      : BaseClass(scalefactor, 2, amptables())
    { }

  protected:
    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp6q0gH2Static>();
    }

    void markZeroFv()
    {
      fvZero.reset();
      if (vhel[2] == vhel[3] || vhel[4] == vhel[5]) {
        fvZero[0] = true;
      }
      if (vhel[2] == vhel[5] || vhel[4] == vhel[3]) {
        fvZero[1] = true;
      }
    }
};

class Amp6q0gH6Static : public Amp6q0gHStatic
{
  public:
    static const int HS = 20;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp6q0gH6 : public Amp6q0gH<T>
{
    typedef Amp6q0gH<T> BaseClass;
  public:
    using BaseClass::fvZero;
    using BaseClass::vhel;

    Amp6q0gH6(const T scalefactor)
      : BaseClass(scalefactor, 6, amptables())
    { }

  protected:
    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp6q0gH6Static>();
    }

    void markZeroFv()
    {
      fvZero.reset();
      if (vhel[0] == vhel[1]) {
        fvZero[0] = true;
        fvZero[1] = true;
      }
      if (vhel[0] == vhel[3]) {
        fvZero[2] = true;
        fvZero[5] = true;
      }
      if (vhel[0] == vhel[5]) {
        fvZero[4] = true;
        fvZero[3] = true;
      }

      if (vhel[2] == vhel[3]) {
        fvZero[0] = true;
        fvZero[3] = true;
      }
      if (vhel[2] == vhel[5]) {
        fvZero[2] = true;
        fvZero[1] = true;
      }
      if (vhel[2] == vhel[1]) {
        fvZero[4] = true;
        fvZero[5] = true;
      }

      if (vhel[4] == vhel[5]) {
        fvZero[0] = true;
        fvZero[5] = true;
      }
      if (vhel[4] == vhel[1]) {
        fvZero[2] = true;
        fvZero[3] = true;
      }
      if (vhel[4] == vhel[3]) {
        fvZero[4] = true;
        fvZero[1] = true;
      }
    }
};

#endif // CHSUM_6Q0GH_H
