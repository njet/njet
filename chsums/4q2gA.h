/*
* chsums/4q2gA.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012, 2013 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef CHSUM_4Q2GA_H
#define CHSUM_4Q2GA_H

#include "4q2gV.h"

template <typename T>
class Amp4q2gA : public Amp4q2gZ<T>
{
    typedef Amp4q2gZ<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp4q2gA(const Flavour<double>& ff, const T scalefactor,
             const int mFC=2, const NJetAmpTables& tables=BaseClass::amptables())
    : BaseClass(ff, scalefactor, mFC, tables)
    { }

    void born_part_fill() { BaseClass::born_part_trickfill(); }
    LoopValue virt() { return BaseClass::virt_tricksum(); }
};

template <typename T>
class Amp4q2gAd : public Amp4q2gZd<T>
{
    typedef Amp4q2gZd<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp4q2gAd(const Flavour<double>& ff, const T scalefactor,
              const int mFC=2, const NJetAmpTables& tables=BaseClass::amptables())
    : BaseClass(ff, scalefactor, mFC, tables)
    { }

    void born_part_fill() { BaseClass::born_part_trickfill(); }
    LoopValue virt() { return BaseClass::virt_tricksum(); }
};

template <typename T>
class Amp4q2gA2 : public Amp4q2gZ2<T>
{
    typedef Amp4q2gZ2<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp4q2gA2(const Flavour<double>& ff, const T scalefactor,
              const int mFC=4, const NJetAmpTables& tables=BaseClass::amptables())
    : BaseClass(ff, scalefactor, mFC, tables)
    { }

    void born_part_fill() { BaseClass::born_part_trickfill(); }
    LoopValue virt() { return BaseClass::virt_tricksum(); }
};

// Two photons

class Amp4q2gAAStatic : public Amp4q2gStatic
{
  public:
    static const int FC = 8;

    static const int flav[FC][NN];  // flavour vectors for ngluon
    static const int fvsign[FC];    // flavour channels' signs
    static const int fperm[FC][NN];        // flavour pemutation matrix
    static const int fvcol[FC][CC];        // flavour-partial pemutation matrix

    static const int HS = 64;
    static const int HSNN = NN+2;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp4q2gAA : public Amp4q2g<T>
{
    typedef Amp4q2g<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp4q2gAA(const Flavour<double>& Vflav, const T scalefactor,
              const int mFC=4, const NJetAmpTables& tables=amptables());

  protected:
    using BaseClass::NN;
    using BaseClass::vhel;
    using BaseClass::getFlav;
    using BaseClass::getFperm;
    using BaseClass::mfv;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp4q2gAAStatic>();
    }

    void initProcess(const Flavour<double>& ff);

    TreeValue A0(int p0, int p1, int p2, int p3, int p4, int p5);
    LoopResult<T> AL(int p0, int p1, int p2, int p3, int p4, int p5);
    LoopResult<T> AF(int p0, int p1, int p2, int p3, int p4, int p5);
};

template <typename T>
class Amp4q2gAAd : public Amp4q2gAA<T>
{
    typedef Amp4q2gAA<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp4q2gAAd(const Flavour<double>& Vflav, const T scalefactor,
               const int mFC=4, const NJetAmpTables& tables=BaseClass::amptables());

  protected:
    using BaseClass::NN;
    using BaseClass::getFlav;
    using BaseClass::mfv;

    void initProcess(const Flavour<double>& ff);
};

class Amp4q2gAA2Static : public Amp4q2gAAStatic
{
  public:
    static const int HS = 96;
    static const int HSNN = NN+2;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp4q2gAA2 : public Amp4q2gAA<T>
{
    typedef Amp4q2gAA<T> BaseClass;
  public:

    Amp4q2gAA2(const Flavour<double>& Vflav,
               const T scalefactor, const NJetAmpTables& tables=amptables())
      : BaseClass(Vflav, scalefactor, 8, tables)
    { }

  protected:
    using BaseClass::fvZero;
    using BaseClass::vhel;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp4q2gAA2Static>();
    }

    void markZeroFv()
    {
      fvZero.reset();
      if (vhel[0] == vhel[1] || vhel[2] == vhel[3]) {
        fvZero[0] = true;
        fvZero[1] = true;
        fvZero[2] = true;
        fvZero[3] = true;
      }
      if (vhel[0] == vhel[3] || vhel[2] == vhel[1]) {
        fvZero[4] = true;
        fvZero[5] = true;
        fvZero[6] = true;
        fvZero[7] = true;
      }
    }
};

#endif // CHSUM_4Q2GA_H
