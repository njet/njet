/*
* chsums/4q2gA.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012, 2013 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include "4q2gA.h"
#include "NJetAmp-T.h"

// class Amp4q2gAA
template <typename T>
Amp4q2gAA<T>::Amp4q2gAA(const Flavour<double>& ff, const T scalefactor,
                        const int mFC, const NJetAmpTables& tables)
  : BaseClass(scalefactor, mFC, tables)
{
  initProcess(ff);
}

template <typename T>
void Amp4q2gAA<T>::initProcess(const Flavour<double>& ff)
{
  const Flavour<double> A1 = ff;
  const Flavour<double> A2 = StandardModel::BosonNext(ff);

  Flavour<double> ffs1[] = {A1, A1, A1, A1,   A1, A1, A1, A1};
  Flavour<double> ffs2[] = {A1, A2, A1, A2,   A1, A2, A1, A2};

  for (int fv=0; fv<BaseClass::mFC; fv++) {
    Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
    flavours.push_back(ffs1[fv]);
    flavours.push_back(ffs2[fv]);
    BaseClass::setProcess(flavours, fv);
  }
}

template <typename T>
Amp4q2gAAd<T>::Amp4q2gAAd(const Flavour<double>& ff, const T scalefactor,
                          const int mFC, const NJetAmpTables& tables)
  : BaseClass(ff, scalefactor, mFC, tables)
{
  initProcess(ff);
}

template <typename T>
void Amp4q2gAAd<T>::initProcess(const Flavour<double>& ff)
{
  const Flavour<double> A1u = ff;
  const Flavour<double> A2u = StandardModel::BosonNext(ff);

  const Flavour<double> A1d = StandardModel::BosonFlip(A1u);
  const Flavour<double> A2d = StandardModel::BosonFlip(A2u);

  Flavour<double> ffs1[] = {A1u, A1u, A1d, A1d};
  Flavour<double> ffs2[] = {A1u, A2d, A1d, A2u};

  for (int fv=0; fv<BaseClass::mFC; fv++) {
    Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
    flavours.push_back(ffs1[fv]);
    flavours.push_back(ffs2[fv]);
    BaseClass::setProcess(flavours, fv);
  }
}

template <typename T>
typename Amp4q2gAA<T>::TreeValue
Amp4q2gAA<T>::A0(int p0, int p1, int p2, int p3, int p4, int p5)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, NN+1, O[p1], O[p2], O[p3], O[p4], O[p5]};
  TreeValue amp = TreeValue();
  if (mfv % 2 == 0) {
    amp = BaseClass::A0nAA(order);
  } else {
    amp = BaseClass::A0nAA2(order);
  }
  return amp;
}

template <typename T>
LoopResult<T> Amp4q2gAA<T>::AF(int p0, int p1, int p2, int p3, int p4, int p5)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, NN+1, O[p1], O[p2], O[p3], O[p4], O[p5]};
  LoopResult<T> amp = LoopResult<T>();
  if (mfv % 2 == 0) {
    amp = BaseClass::AFnAA(order);
  } else {
#ifdef SYMMETRIZE_AA
    amp = BaseClass::AFnAA2(order);
#else
    if (mfv % 4 == 1) {
      amp = 2.*BaseClass::AFnAA2(order);
    }
#endif
  }
  return amp;
}

template <typename T>
LoopResult<T> Amp4q2gAA<T>::AL(int p0, int p1, int p2, int p3, int p4, int p5)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, NN+1, O[p1], O[p2], O[p3], O[p4], O[p5]};
  LoopResult<T> amp = LoopResult<T>();
  if (mfv % 2 == 0) {
    amp = BaseClass::ALnAA(order);
  } else {
#ifdef SYMMETRIZE_AA
    amp = BaseClass::ALnAA2(order);
#else
    if (mfv % 4 == 1) {
      amp = 2.*BaseClass::ALnAA2(order);
    }
#endif
  }
  return amp;
}

#ifdef USE_SD
  template class Amp4q2gAA<double>;
  template class Amp4q2gAAd<double>;
#endif
#ifdef USE_DD
  template class Amp4q2gAA<dd_real>;
  template class Amp4q2gAAd<dd_real>;
#endif
#ifdef USE_QD
  template class Amp4q2gAA<qd_real>;
  template class Amp4q2gAAd<qd_real>;
#endif
#ifdef USE_VC
  template class Amp4q2gAA<Vc::double_v>;
  template class Amp4q2gAAd<Vc::double_v>;
#endif
