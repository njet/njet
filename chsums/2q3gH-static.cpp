/*
* chsums/2q3gH-static.cpp
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>

#include "2q3gH.h"

// class Amp2q3gHStatic

const int
Amp2q3gHStatic::HSarr[HS][HSNN] = {
  {-1, 1, 1, 1, 1, 1},
  {-1, 1, 1, 1,-1, 1},
  {-1, 1, 1,-1, 1, 1},
  {-1, 1, 1,-1,-1, 1},
  {-1, 1,-1, 1, 1, 1},
  {-1, 1,-1, 1,-1, 1},
  {-1, 1,-1,-1, 1, 1},
  {-1, 1,-1,-1,-1, 1},
  { 1,-1,-1,-1,-1, 1},
  { 1,-1,-1,-1, 1, 1},
  { 1,-1,-1, 1,-1, 1},
  { 1,-1,-1, 1, 1, 1},
  { 1,-1, 1,-1,-1, 1},
  { 1,-1, 1,-1, 1, 1},
  { 1,-1, 1, 1,-1, 1},
  { 1,-1, 1, 1, 1, 1}
};
