/*
* chsums/2q3gV.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include "2q3gV.h"
#include "NJetAmp-T.h"

// class Amp2q3gV

template <typename T>
Amp2q3gV<T>::Amp2q3gV(const T scalefactor,
                      const int mFC, const NJetAmpTables& tables)
  : BaseClass(scalefactor, mFC, tables)
{
  Flavour<double> ff = StandardModel::Wp(StandardModel::u(), StandardModel::ubar(), 1.);  // ckm = 1
  initProcess(ff);
}

template <typename T>
Amp2q3gV<T>::Amp2q3gV(const Flavour<double>& ff, const T scalefactor,
                      const int mFC, const NJetAmpTables& tables)
  : BaseClass(scalefactor, mFC, tables)
{
  initProcess(ff);
}

template <typename T>
void Amp2q3gV<T>::initProcess(const Flavour<double>& ff)
{
  for (int fv=0; fv<BaseClass::mFC; fv++) {
    Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
    flavours.push_back(ff);
    BaseClass::setProcess(flavours, fv);
  }
}

template <typename T>
typename Amp2q3gV<T>::TreeValue
Amp2q3gV<T>::A0(int p0, int p1, int p2, int p3, int p4)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, O[p1], O[p2], O[p3], O[p4]};
  return BaseClass::A0nVqq(order);
}

template <typename T>
LoopResult<T> Amp2q3gV<T>::AF(int p0, int p1, int p2, int p3, int p4)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, O[p1], O[p2], O[p3], O[p4]};
  return BaseClass::AFnVqq(order);
}

template <typename T>
LoopResult<T> Amp2q3gV<T>::AL(int p0, int p1, int p2, int p3, int p4)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, O[p1], O[p2], O[p3], O[p4]};
  return BaseClass::ALnVqq(order);
}

template <typename T>
LoopResult<T> Amp2q3gV<T>::AF(int p0, int p1, int p2, int p3, int p4, int /*pos*/, int /*posR*/)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, O[p1], O[p2], O[p3], O[p4]};
  return BaseClass::AFnVqq(order);
}

template <typename T>
LoopResult<T> Amp2q3gV<T>::AL(int p0, int p1, int p2, int p3, int p4, int /*pos*/)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, O[p1], O[p2], O[p3], O[p4]};
  return BaseClass::ALnVqq(order);
}

#ifdef USE_SD
  template class Amp2q3gV<double>;
#endif
#ifdef USE_DD
  template class Amp2q3gV<dd_real>;
#endif
#ifdef USE_QD
  template class Amp2q3gV<qd_real>;
#endif
#ifdef USE_VC
  template class Amp2q3gV<Vc::double_v>;
#endif
