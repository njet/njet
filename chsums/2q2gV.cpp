/*
* chsums/2q2gV.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include "2q2gV.h"
#include "NJetAmp-T.h"

// class Amp2q2gV

template <typename T>
Amp2q2gV<T>::Amp2q2gV(const T scalefactor,
                      const int mFC, const NJetAmpTables& tables)
  : BaseClass(scalefactor, mFC, tables)
{
  Flavour<double> ff = StandardModel::Wp(StandardModel::u(), StandardModel::ubar(), 1.);  // ckm = 1
  initProcess(ff);
}

template <typename T>
Amp2q2gV<T>::Amp2q2gV(const Flavour<double>& ff, const T scalefactor,
                      const int mFC, const NJetAmpTables& tables)
  : BaseClass(scalefactor, mFC, tables)
{
  initProcess(ff);
}

template <typename T>
void Amp2q2gV<T>::initProcess(const Flavour<double>& ff)
{
  for (int fv=0; fv<BaseClass::mFC; fv++) {
    Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
    flavours.push_back(ff);
    BaseClass::setProcess(flavours, fv);
  }
}

template <typename T>
typename Amp2q2gV<T>::TreeValue
Amp2q2gV<T>::A0(int p0, int p1, int p2, int p3)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, O[p1], O[p2], O[p3]};
  return BaseClass::A0nVqq(order);
}

template <typename T>
LoopResult<T> Amp2q2gV<T>::AF(int p0, int p1, int p2, int p3)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, O[p1], O[p2], O[p3]};
  return BaseClass::AFnVqq(order);
}

template <typename T>
LoopResult<T> Amp2q2gV<T>::AL(int p0, int p1, int p2, int p3)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, O[p1], O[p2], O[p3]};
  return BaseClass::ALnVqq(order);
}

#ifdef USE_SD
  template class Amp2q2gV<double>;
#endif
#ifdef USE_DD
  template class Amp2q2gV<dd_real>;
#endif
#ifdef USE_QD
  template class Amp2q2gV<qd_real>;
#endif
#ifdef USE_VC
  template class Amp2q2gV<Vc::double_v>;
#endif
