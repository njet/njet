/*
* chsums/2q0gA.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include "2q0gA.h"
#include "NJetAmp-T.h"

// class Amp2q0gAA

template <typename T>
Amp2q0gAA<T>::Amp2q0gAA(const Flavour<double>& ff, const T scalefactor,
                        const int mFC, const NJetAmpTables& tables)
  : BaseClass(scalefactor, mFC, tables)
{
  initProcess(ff);
}

template <typename T>
void Amp2q0gAA<T>::initProcess(const Flavour<double>& ff)
{
  for (int fv=0; fv<BaseClass::mFC; fv++) {
    Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
    flavours.push_back(ff);
    flavours.push_back(ff);
    BaseClass::setProcess(flavours, fv);
  }
}

template <typename T>
typename Amp2q0gAA<T>::TreeValue
Amp2q0gAA<T>::A0(int p0, int p1)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, NN+1, O[p1]};
  return BaseClass::A0nAA(order);
}

template <typename T>
LoopResult<T> Amp2q0gAA<T>::AF(int p0, int p1)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, NN+1, O[p1]};
  return BaseClass::AFnAA(order);
}

template <typename T>
LoopResult<T> Amp2q0gAA<T>::AL(int p0, int p1)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, NN+1, O[p1]};
  return BaseClass::ALnAA(order);
}

#ifdef USE_SD
  template class Amp2q0gAA<double>;
#endif
#ifdef USE_DD
  template class Amp2q0gAA<dd_real>;
#endif
#ifdef USE_QD
  template class Amp2q0gAA<qd_real>;
#endif
#ifdef USE_VC
  template class Amp2q0gAA<Vc::double_v>;
#endif
