/*
* chsums/4q3gV.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef CHSUM_4Q3GV_H
#define CHSUM_4Q3GV_H

#include "4q3g.h"
#include "../ngluon2/Model.h"

class Amp4q3gVStatic : public Amp4q3gStatic
{
  public:
    static const int HS = 16;
    static const int HSNN = NN+1;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp4q3gV : public Amp4q3g<T>
{
    typedef Amp4q3g<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp4q3gV(const T scalefactor, const int mFC=1, const NJetAmpTables& tables=amptables());
    Amp4q3gV(const Flavour<double>& Vflav,
             const T scalefactor, const int mFC=1, const NJetAmpTables& tables=amptables());

    void born_part_fill() { BaseClass::born_part_fullfill(); }
    LoopValue virt() { return BaseClass::virt_fullsum(); }

  protected:
    using BaseClass::NN;
    using BaseClass::vhel;
    using BaseClass::getFlav;
    using BaseClass::getFperm;
    using BaseClass::mfv;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp4q3gVStatic>();
    }

    void initProcess(const Flavour<double>& ff);

    TreeValue A0(int p0, int p1, int p2, int p3, int p4, int p5, int p6);
    LoopResult<T> AL(int p0, int p1, int p2, int p3, int p4, int p5, int p6);
    LoopResult<T> AF(int p0, int p1, int p2, int p3, int p4, int p5, int p6);
};

class Amp4q3gV2Static : public Amp4q3gVStatic
{
  public:
    static const int HS = 24;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp4q3gV2 : public Amp4q3gV<T>
{
    typedef Amp4q3gV<T> BaseClass;
  public:

    Amp4q3gV2(const T scalefactor, const NJetAmpTables& tables=amptables())
      : BaseClass(scalefactor, 2, tables)
    { }

    Amp4q3gV2(const Flavour<double>& Vflav,
              const T scalefactor, const NJetAmpTables& tables=amptables())
      : BaseClass(Vflav, scalefactor, 2, tables)
    { }

  protected:
    using BaseClass::fvZero;
    using BaseClass::vhel;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp4q3gV2Static>();
    }

    void markZeroFv()
    {
      fvZero.reset();
      if (vhel[0] == vhel[1] || vhel[2] == vhel[3]) {
        fvZero[0] = true;
      }
      if (vhel[0] == vhel[3] || vhel[2] == vhel[1]) {
        fvZero[1] = true;
      }
    }
};

class Amp4q3gV2bStatic : public Amp4q3gVStatic
{
  public:
    static const int HS = 24;
    static const int HSarr[HS][HSNN];

    static const int flav[FC][NN];
    static const int fperm[FC][NN];
    static const int fvcol[FC][CC];
};

template <typename T>
class Amp4q3gV2b : public Amp4q3gV2<T>
{
    typedef Amp4q3gV2<T> BaseClass;
  public:

    Amp4q3gV2b(const T scalefactor, const NJetAmpTables& tables=amptables())
      : BaseClass(scalefactor, tables)
    { }

    Amp4q3gV2b(const Flavour<double>& Vflav,
               const T scalefactor, const NJetAmpTables& tables=amptables())
      : BaseClass(Vflav, scalefactor, tables)
    { }

  protected:
    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp4q3gV2bStatic>();
    }
};

// Z amplitudes

class Amp4q3gZStatic : public Amp4q3gVStatic
{
  public:
    static const int FC = 4;

    static const int flav[FC][NN];  // flavour vectors for ngluon
    static const int fvsign[FC];    // flavour channels' signs
    static const int fperm[FC][NN];        // flavour pemutation matrix
    static const int fvcol[FC][CC];        // flavour-partial pemutation matrix

    static const int HS = 64;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp4q3gZ : public Amp4q3gV<T>
{
    typedef Amp4q3gV<T> BaseClass;
  public:

    Amp4q3gZ(const Flavour<double>& ff, const T scalefactor,
             const int mFC=2, const NJetAmpTables& tables=amptables())
    : BaseClass(ff, scalefactor, mFC, tables)
    { }

  protected:
    using BaseClass::fvZero;
    using BaseClass::vhel;
    using BaseClass::NN;
    using BaseClass::getFlav;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp4q3gZStatic>();
    }
};

template <typename T>
class Amp4q3gZd : public Amp4q3gZ<T>
{
    typedef Amp4q3gZ<T> BaseClass;
  public:

    Amp4q3gZd(const Flavour<double>& ff, const T scalefactor,
             const int mFC=2, const NJetAmpTables& tables=amptables());

  protected:
    using BaseClass::fvZero;
    using BaseClass::vhel;
    using BaseClass::NN;
    using BaseClass::getFlav;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp4q3gZStatic>();
    }
};

class Amp4q3gZ2Static : public Amp4q3gZStatic
{
  public:
    static const int HS = 96;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp4q3gZ2 : public Amp4q3gZ<T>
{
    typedef Amp4q3gZ<T> BaseClass;
  public:

    Amp4q3gZ2(const Flavour<double>& ff, const T scalefactor,
              const int mFC=4, const NJetAmpTables& tables=amptables())
    : BaseClass(ff, scalefactor, mFC, tables)
    { }

  protected:
    using BaseClass::fvZero;
    using BaseClass::vhel;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp4q3gZ2Static>();
    }

    void markZeroFv()
    {
      fvZero.reset();
      if (vhel[0] == vhel[1] || vhel[2] == vhel[3]) {
        fvZero[0] = true;
        fvZero[1] = true;
      }
      if (vhel[0] == vhel[3] || vhel[2] == vhel[1]) {
        fvZero[2] = true;
        fvZero[3] = true;
      }
    }
};

#endif // CHSUM_4Q3G_H
