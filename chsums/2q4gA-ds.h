/*
* chsums/2q4gA-ds.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012, 2013 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef CHSUM_2Q4GA_DS_H
#define CHSUM_2Q4GA_DS_H

#include "2q4gV-ds.h"
#include "2q4gA.h"

// ============================================================================
// classes Amp2q4gA_ds4 and Amp2q4gAA_ds4
// ============================================================================

template <typename T>
class Amp2q4gA_ds4 : public Amp2q4gZ_ds4<T>
{
    typedef Amp2q4gZ_ds4<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;

    Amp2q4gA_ds4(const Flavour<double>& ff, const T scalefactor,
                 const int mFC=1, const NJetAmpTables& tables=BaseClass::amptables())
    : BaseClass(ff, scalefactor, mFC, tables)
    { }

    void born_part_fill() { BaseClass::born_part_trickfill(); }
    LoopValue virt() { return BaseClass::virt_dstricksum(); }
};

class Amp2q4gAA_ds4Static : public Amp2q4gAAStatic, public Amp2q4g_ds4Static
{
};

template <typename T>
class Amp2q4gAA_ds4 : public Amp2q4gA_ds4<T>
{
    typedef Amp2q4gA_ds4<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp2q4gAA_ds4(const Flavour<double>& ff, const T scalefactor,
                  const int mFC=1, const NJetAmpTables& tables=amptables());

  protected:
    using BaseClass::NN;
    using BaseClass::vhel;
    using BaseClass::getFlav;
    using BaseClass::getFperm;
    using BaseClass::mfv;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp2q4gAA_ds4Static>();
    }

    void initProcess(const Flavour<double>& ff);

    TreeValue A0(int p0, int p1, int p2, int p3, int p4, int p5);
    LoopResult<T> AL(int p0, int p1, int p2, int p3, int p4, int p5);
    LoopResult<T> AF(int p0, int p1, int p2, int p3, int p4, int p5);
};

// ============================================================================
// classes Amp2q4gA_ds3 and Amp2q4gAA_ds3
// ============================================================================

template <typename T>
class Amp2q4gA_ds3 : public Amp2q4gZ_ds3<T>
{
    typedef Amp2q4gZ_ds3<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;

    Amp2q4gA_ds3(const Flavour<double>& ff, const T scalefactor,
                 const int mFC=1, const NJetAmpTables& tables=BaseClass::amptables())
    : BaseClass(ff, scalefactor, mFC, tables)
    { }

    void born_part_fill() { BaseClass::born_part_trickfill(); }
    LoopValue virt() { return BaseClass::virt_dstricksum(); }
};

class Amp2q4gAA_ds3Static : public Amp2q4gAAStatic, public Amp2q4g_ds3Static
{
};

template <typename T>
class Amp2q4gAA_ds3 : public Amp2q4gA_ds3<T>
{
    typedef Amp2q4gA_ds3<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp2q4gAA_ds3(const Flavour<double>& ff, const T scalefactor,
                 const int mFC=1, const NJetAmpTables& tables=amptables());

  protected:
    using BaseClass::NN;
    using BaseClass::vhel;
    using BaseClass::getFlav;
    using BaseClass::getFperm;
    using BaseClass::mfv;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp2q4gAA_ds3Static>();
    }

    void initProcess(const Flavour<double>& ff);

    TreeValue A0(int p0, int p1, int p2, int p3, int p4, int p5);
    LoopResult<T> AL(int p0, int p1, int p2, int p3, int p4, int p5);
    LoopResult<T> AF(int p0, int p1, int p2, int p3, int p4, int p5);
};

#endif // CHSUM_2Q4GA_DS_H
