/*
* analytic/NJetAnalytic.cpp
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <bitset>
#include <algorithm>

#include "../ngluon2/LoopIntegrals.h"
#include "NJetAnalytic.h"

template <typename T>
NJetAnalytic<T>::NJetAnalytic(const T scalefactor_, const int NMOM_, int extscalepow)
  : NMOM(NMOM_), NAB(NMOM), scalefactor(scalefactor_),
    spvA(NMOM*NMOM), spvB(NMOM*NMOM), spvAB(NMOM*NMOM),
    lpvS(NMOM*NMOM), moms(NMOM)
{
  const int scalepow = NMOM - 4 + extscalepow;
  amprescale = pow(scalefactor, scalepow);

  cacheI2args = new std::vector<I2arg<T> >();
  cacheI3args = new std::vector<I3arg<T> >();
  cacheI4args = new std::vector<I4arg<T> >();
}

template <typename T>
NJetAnalytic<T>::~NJetAnalytic()
{
  delete cacheI2args;
  delete cacheI3args;
  delete cacheI4args;
}

template <typename T>
void NJetAnalytic<T>::setMomenta(const RealMom* moms_)
{
  for (int i = 0; i < NMOM; i++) {
    moms[i] = scalefactor*moms_[i];
  }
  initKinematics();
}

template <typename T>
template <typename U>
void NJetAnalytic<T>::setMomenta(const MOM<U>* othermoms)
{
  for (int i = 0; i < NMOM; i++) {
    moms[i] = scalefactor*RealMom(othermoms[i]);
  }
  initKinematics();
}

template <typename T>
void NJetAnalytic<T>::setMuR2(const T rscale)
{
  MuR2 = scalefactor*scalefactor*rscale;
  resetCache();
}

template <typename T>
void NJetAnalytic<T>::initKinematics()
{
  for (int i = 0; i < NMOM; i++) {
    lS(i, i) = S(moms[i]);
    if (i == NAB) continue;
    for (int j = i + 1; j < NMOM; j++) {
      sA(i, j) = xspA(moms[i], moms[j]);
      sA(j, i) = -sA(i, j);
      sB(i, j) = xspB(moms[i], moms[j]);
      sB(j, i) = -sB(i, j);
      lS(i, j) = S(moms[i] + moms[j]);
      lS(j, i) = lS(i, j);
    }
  }
  if (NAB < NMOM) {
    for (int i = 0; i < NMOM; i++) {
      if (i == NAB) continue;
      for (int j = 0; j < NMOM; j++) {
        if (j == NAB) continue;
        sAB(i, j) = xspAB(moms[i], moms[NAB], moms[j]);
      }
    }
  }
  resetCache();
}

template <typename T>
std::complex<T> NJetAnalytic<T>::CyclicSpinorsA(const int* ord) const
{
  std::complex<T> den = sA(ord[NMOM - 1], ord[0]);

  for (int i = 0; i < NMOM - 1; i++) {
    den *= sA(ord[i], ord[i + 1]);
  }

  return den;
}

template <typename T>
std::complex<T> NJetAnalytic<T>::CyclicSpinorsB(const int* ord) const
{
  std::complex<T> den = sB(ord[NMOM - 1], ord[0]);

  for (int i = 0; i < NMOM - 1; i++) {
    den *= sB(ord[i], ord[i + 1]);
  }

  return den;
}

template <typename T>
int NJetAnalytic<T>::HelicityOrder(const int h, const int* op) const
{
  std::bitset<32> hin(h);
  std::bitset<32> hout(h);

  for (int i = 0; i < NMOM; i++) {
    hout[i] = hin[op[i]];
  }

  return static_cast<int>(hout.to_ulong());
}

template <typename T, int N>
struct INTarg
{
  static const int LEN = N;

  bool operator== (const INTarg& other) {
    for (int i=0; i<LEN; i++) {
      if (vars[i] != other.vars[i]) {
        return false;
      }
    }
    return true;
  }

  T vars[LEN];
};

template <typename T>
struct I2arg : public INTarg<T, 4>
{
  using INTarg<T, 4>::vars;

  I2arg(const T s, const T m1, const T m2, const T mur)
  {
    vars[0] = s;
    vars[1] = m1;
    vars[2] = m2;
    vars[3] = mur;
  }
};

template <typename T>
struct I3arg : public INTarg<T, 7>
{
  using INTarg<T, 7>::vars;

  I3arg(const T M1, const T M2, const T M3,
        const T m1, const T m2, const T m3, const T mur)
  {
    vars[0] = M1;
    vars[1] = M2;
    vars[2] = M3;
    vars[3] = m1;
    vars[4] = m2;
    vars[5] = m3;
    vars[6] = mur;
  }
};

template <typename T>
struct I4arg : public INTarg<T, 11>
{
  using INTarg<T, 11>::vars;

  I4arg(const T s, const T t,
        const T M1, const T M2, const T M3, const T M4,
        const T m1, const T m2, const T m3, const T m4, const T mur)
  {
    vars[0] = s;
    vars[1] = t;
    vars[2] = M1;
    vars[3] = M2;
    vars[4] = M3;
    vars[5] = M4;
    vars[6] = m1;
    vars[7] = m2;
    vars[8] = m3;
    vars[9] = m4;
    vars[10] = mur;
  }
};

template <typename T>
void NJetAnalytic<T>::resetCache()
{
  cacheI2args->clear();
  cacheI3args->clear();
  cacheI4args->clear();

  cacheI2vals.clear();
  cacheI3vals.clear();
  cacheI4vals.clear();
}

template <typename T>
EpsTriplet<T> NJetAnalytic<T>::I2(const T s, const T m1, const T m2, const T mur)
{
  const I2arg<T> args = I2arg<T>(s, m1, m2, mur);
  const std::ptrdiff_t pos = std::find(cacheI2args->begin(), cacheI2args->end(), args) - cacheI2args->begin();
  if (cacheI2args->begin() + pos == cacheI2args->end()) {
    cacheI2args->push_back(args);
    cacheI2vals.push_back(::I2(s, m1, m2, mur));
  }
  return cacheI2vals[pos];
}

template <typename T>
EpsTriplet<T> NJetAnalytic<T>::I3(const T M1, const T M2, const T M3,
                      const T m1, const T m2, const T m3, const T mur)
{
  const I3arg<T> args = I3arg<T>(M1, M2, M3, m1, m2, m3, mur);
  const std::ptrdiff_t pos = std::find(cacheI3args->begin(), cacheI3args->end(), args) - cacheI3args->begin();
  if (cacheI3args->begin() + pos == cacheI3args->end()) {
    cacheI3args->push_back(args);
    cacheI3vals.push_back(::I3(M1, M2, M3, m1, m2, m3, mur));
  }
  return cacheI3vals[pos];
}

template <typename T>
EpsTriplet<T> NJetAnalytic<T>::I4(const T s, const T t,
                      const T M1, const T M2, const T M3, const T M4,
                      const T m1, const T m2, const T m3, const T m4, const T mur)
{
  const I4arg<T> args = I4arg<T>(s, t, M1, M2, M3, M4, m1, m2, m3, m4, mur);
  const std::ptrdiff_t pos = std::find(cacheI4args->begin(), cacheI4args->end(), args) - cacheI4args->begin();
  if (cacheI4args->begin() + pos == cacheI4args->end()) {
    cacheI4args->push_back(args);
    cacheI4vals.push_back(::I4(s, t, M1, M2, M3, M4, m1, m2, m3, m4, mur));
  }
  return cacheI4vals[pos];
}

// watch out for the minus sign in the definition here //
// this comes from the definition used in the code generation //
// L0(s,t) = I2(s) - I2(t)
template <typename T>
std::complex<T> NJetAnalytic<T>::L0(const T a, const T b) {
  const T r = a/b;
  const T re = -log(abs(r));
  const T ima  = (a<0.) ? -T(M_PI) : T(0.);
  const T imb  = (b<0.) ? T(M_PI) : T(0.);
  return std::complex<T>(re, ima+imb);
}

template <typename T>
std::complex<T> NJetAnalytic<T>::L1(const T a, const T b) {
  const T r = a/b;
  return L0(a,b)/(1.-r);
}

template <typename T>
std::complex<T> NJetAnalytic<T>::L2hat(const T a, const T b) {
  const T r = a/b;
  const T omr = 1.-r;
  return L0(a,b)/(omr*omr)-1./omr;
}

template <typename T>
std::complex<T> NJetAnalytic<T>::L3hat(const T a, const T b) {
  const T r = a/b;
  const T omr = 1.-r;
  return L0(a,b)/(omr*omr*omr)-(1.+r)/(T(2.)*r*omr*omr);
}

#ifdef USE_SD
  template class NJetAnalytic<double>;
#endif
#ifdef USE_DD
  template class NJetAnalytic<dd_real>;
#endif
#ifdef USE_QD
  template class NJetAnalytic<qd_real>;
#endif
#ifdef USE_VC
  template class NJetAnalytic<Vc::double_v>;
  template void NJetAnalytic<Vc::double_v>::setMomenta(const MOM<double>*);
#endif
