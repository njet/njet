/*
* chsums/4q0gV-static.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include "4q0gV.h"

// class Amp4q0gVStatic

const int
Amp4q0gVStatic::HSarr[HS][HSNN] = {
  { 1,-1,-1, 1, 1},
  { 1,-1, 1,-1, 1},
};

// class Amp4q0gV2Static

const int
Amp4q0gV2Static::HSarr[HS][HSNN] = {
  { 1,-1,-1, 1, 1},
  { 1,-1, 1,-1, 1},
  { 1, 1,-1,-1, 1}
};

// class Amp4q0gV2bStatic

const int
Amp4q0gV2bStatic::HSarr[HS][HSNN] = {
  { 1,-1,-1, 1, 1},
  { 1,-1, 1,-1, 1},
  {-1,-1, 1, 1, 1},
};

const int
Amp4q0gV2bStatic::flav[FC][NN] = {
    {-1,1,-2,2},
    {-2,1,-1,2},
};

const int
Amp4q0gV2bStatic::fperm[FC][NN] = {
    {0,1,2,3},
    {2,1,0,3},
};

const int
Amp4q0gV2bStatic::fvcol[FC][CC] = {
    {0,1},
    {1,0}
};

// class Amp4q0gZStatic

const int
Amp4q0gZStatic::flav[FC][NN] = {
    {-1,1,-2,2},
    {-2,2,-1,1},
    {-1,2,-2,1},
    {-2,1,-1,2}
};

const int
Amp4q0gZStatic::fvsign[FC] = {1, 1,-1,-1};

const int
Amp4q0gZStatic::fperm[FC][NN] = {
    {0,1,2,3},
    {2,3,0,1},
    {0,3,2,1},
    {2,1,0,3}
};

const int
Amp4q0gZStatic::fvcol[FC][CC] = {
    {0,1},
    {0,1},
    {1,0},
    {1,0}
};

const int
Amp4q0gZStatic::HSarr[HS][HSNN] = {
  { 1,-1,-1, 1, 1},
  { 1,-1, 1,-1, 1},
  {-1, 1,-1, 1, 1},
  {-1, 1, 1,-1, 1},
  { 1,-1,-1, 1,-1},
  { 1,-1, 1,-1,-1},
  {-1, 1,-1, 1,-1},
  {-1, 1, 1,-1,-1},
};

// class Amp4q0gZ2Static

const int
Amp4q0gZ2Static::HSarr[HS][HSNN] = {
  { 1,-1, 1,-1, 1},
  { 1,-1,-1, 1, 1},
  { 1, 1,-1,-1, 1},
  {-1, 1,-1, 1, 1},
  {-1, 1, 1,-1, 1},
  {-1,-1, 1, 1, 1},
  { 1,-1, 1,-1,-1},
  { 1,-1,-1, 1,-1},
  { 1, 1,-1,-1,-1},
  {-1, 1,-1, 1,-1},
  {-1, 1, 1,-1,-1},
  {-1,-1, 1, 1,-1},
};
