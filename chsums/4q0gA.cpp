/*
* chsums/4q0gA.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include "4q0gA.h"
#include "NJetAmp-T.h"

// class Amp4q0gAx

template <typename T>
Amp4q0gAx<T>::Amp4q0gAx(const Flavour<double>& ff, const T scalefactor,
                        const int mFC, const NJetAmpTables& tables)
 : BaseClass(ff, scalefactor, mFC, tables)
{
  const Flavour<double> Ax = StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C());
  for (int fv=0; fv<mFC; fv++) {
    if (fv % 3 == 0) {
      Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
      flavours.push_back(Ax);
      BaseClass::setProcess(flavours, fv);
    }
  }
}

template <typename T>
void Amp4q0gAx<T>::setNf(const ST Nf_)
{
  ST newNf = Nf_;
  const int intNf = int(to_double(newNf));
  if (double(intNf) == newNf) {
    const int nu = intNf/2;
    const int nd = (intNf+1)/2;
    newNf = -(double(nu)*Particle<ST>::getQu() + double(nd)*Particle<ST>::getQd());
  }
  BaseClass::setNf(newNf);
}

template <typename T>
void Amp4q0gAx<T>::getfvpart1_full(const int fv, LoopValue* fvpart)
{
  return getfvpart1_full_(fv, fvpart);
}

template <typename T>
void Amp4q0gAx<T>::getfvpart1_full(const int fv, LoopResult<T>* fvpart)
{
  return getfvpart1_full_(fv, fvpart);
}

template <typename T>
template <typename LT>
inline
void Amp4q0gAx<T>::getfvpart1_full_(const int fv, LT* fvpart)
{
  fvSet(fv);

  if (fv % 3 == 0 and Nf != 0.) {
    // 1 primitive
    const LT Q1234 = AFx(0,1,2,3);

    fvpart[0] = Nf*(-Q1234);
    fvpart[1] = Nf*(Q1234/Nc);
  } else {
    fvpart[0] = LT();
    fvpart[1] = LT();
  }
}

template <typename T>
LoopResult<T> Amp4q0gAx<T>::AFx(int p0, int p1, int p2, int p3)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], O[p1], O[p2], O[p3], NN};
  return BaseClass::AFnVax(order);
}

// class Amp4q0gAxd

template <typename T>
Amp4q0gAxd<T>::Amp4q0gAxd(const Flavour<double>& ff,
                          const T scalefactor, const int mFC, const NJetAmpTables& tables)
  : BaseClass(ff, scalefactor, mFC, tables)
{
  const Flavour<double> Ax = StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C());
  const Flavour<double> Au = ff;
  const Flavour<double> Ad = StandardModel::BosonFlip(ff);

  Flavour<double> ffs[3] = {Ax, Au, Ad};

  for (int fv=0; fv<BaseClass::mFC; fv++) {
    Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
    flavours.push_back(ffs[fv]);
    BaseClass::setProcess(flavours, fv);
  }
}

// class Amp4q0gAA

template <typename T>
Amp4q0gAA<T>::Amp4q0gAA(const Flavour<double>& ff, const T scalefactor,
                        const int mFC, const NJetAmpTables& tables)
  : BaseClass(scalefactor, mFC, tables)
{
  initProcess(ff);
}

template <typename T>
void Amp4q0gAA<T>::initProcess(const Flavour<double>& ff)
{
  const Flavour<double> A1 = ff;
  const Flavour<double> A2 = StandardModel::BosonNext(ff);

  Flavour<double> ffs1[] = {A1, A1, A1, A1,   A1, A1, A1, A1};
  Flavour<double> ffs2[] = {A1, A2, A1, A2,   A1, A2, A1, A2};

  for (int fv=0; fv<BaseClass::mFC && fv < 8; fv++) {
    Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
    flavours.push_back(ffs1[fv]);
    flavours.push_back(ffs2[fv]);
    BaseClass::setProcess(flavours, fv);
  }
}

template <typename T>
Amp4q0gAAd<T>::Amp4q0gAAd(const Flavour<double>& ff, const T scalefactor,
                          const int mFC, const NJetAmpTables& tables)
  : BaseClass(ff, scalefactor, mFC, tables)
{
  initProcess(ff);
}

template <typename T>
void Amp4q0gAAd<T>::initProcess(const Flavour<double>& ff)
{
  const Flavour<double> A1u = ff;
  const Flavour<double> A2u = StandardModel::BosonNext(ff);

  const Flavour<double> A1d = StandardModel::BosonFlip(A1u);
  const Flavour<double> A2d = StandardModel::BosonFlip(A2u);

  Flavour<double> ffs1[] = {A1u, A1u, A1d, A1d};
  Flavour<double> ffs2[] = {A1u, A2d, A1d, A2u};

  for (int fv=0; fv<BaseClass::mFC && fv < 4; fv++) {
    Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
    flavours.push_back(ffs1[fv]);
    flavours.push_back(ffs2[fv]);
    BaseClass::setProcess(flavours, fv);
  }
}

template <typename T>
typename Amp4q0gAA<T>::TreeValue
Amp4q0gAA<T>::A0(int p0, int p1, int p2, int p3)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, NN+1, O[p1], O[p2], O[p3]};
  TreeValue amp = TreeValue();
  if (mfv % 2 == 0) {
    amp = BaseClass::A0nAA(order);
  } else {
    amp = BaseClass::A0nAA2(order);
  }
  return amp;
}

template <typename T>
LoopResult<T> Amp4q0gAA<T>::AF(int p0, int p1, int p2, int p3)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, NN+1, O[p1], O[p2], O[p3]};
  LoopResult<T> amp = LoopResult<T>();
  if (mfv % 2 == 0) {
    amp = BaseClass::AFnAA(order);
  } else {
#ifdef SYMMETRIZE_AA
    amp = BaseClass::AFnAA2(order);
#else
    if (mfv % 4 == 1) {
      amp = 2.*BaseClass::AFnAA2(order);
    }
#endif
  }
  return amp;
}

template <typename T>
LoopResult<T> Amp4q0gAA<T>::AL(int p0, int p1, int p2, int p3)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, NN+1, O[p1], O[p2], O[p3]};
  LoopResult<T> amp = LoopResult<T>();
  if (mfv % 2 == 0) {
    amp = BaseClass::ALnAA(order);
  } else {
#ifdef SYMMETRIZE_AA
    amp = BaseClass::ALnAA2(order);
#else
    if (mfv % 4 == 1) {
      amp = 2.*BaseClass::ALnAA2(order);
    }
#endif
  }
  return amp;
}

// class Amp4q0gAAx

template <typename T>
Amp4q0gAAx<T>::Amp4q0gAAx(const Flavour<double>& ff, const T scalefactor,
                          const int mFC, const NJetAmpTables& tables)
 : BaseClass(ff, scalefactor, mFC, tables)
{
  const Flavour<double> Ax = StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C());
  const Flavour<double> A1 = ff;
  const Flavour<double> A2 = StandardModel::BosonNext(ff);

  Flavour<double> ffs1[] = {A1, Ax, A1, A1, A1, A1,   A1, Ax, A1, A1, A1, A1};
  Flavour<double> ffs2[] = {Ax, Ax, A1, A2, A1, A2,   Ax, Ax, A1, A2, A1, A2};

  for (int fv=0; fv<BaseClass::mFC; fv++) {
    Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
    flavours.push_back(ffs1[fv]);
    flavours.push_back(ffs2[fv]);
    BaseClass::setProcess(flavours, fv);
  }
}

template <typename T>
Amp4q0gAAxd<T>::Amp4q0gAAxd(const Flavour<double>& ff, const T scalefactor,
                            const int mFC, const NJetAmpTables& tables)
 : BaseClass(ff, scalefactor, mFC, tables)
{
  const Flavour<double> Ax = StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C());

  const Flavour<double> A1u = ff;
  const Flavour<double> A2u = StandardModel::BosonNext(ff);

  const Flavour<double> A1d = StandardModel::BosonFlip(A1u);
  const Flavour<double> A2d = StandardModel::BosonFlip(A2u);

  Flavour<double> ffs1[] = {A1u, Ax, A1u, A1u, A1d, A1d};
  Flavour<double> ffs2[] = {Ax,  Ax, A1u, A2d, A1d, A2u};

  for (int fv=0; fv<BaseClass::mFC; fv++) {
    Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
    flavours.push_back(ffs1[fv]);
    flavours.push_back(ffs2[fv]);
    BaseClass::setProcess(flavours, fv);
  }
}

template <typename T>
void Amp4q0gAAx<T>::setNf(const ST Nf_)
{
  const int intNf = int(to_double(Nf_));
  if (double(intNf) == Nf_) {
    const int nu = intNf/2;
    const int nd = (intNf+1)/2;
    Nfx = -(double(nu)*Particle<ST>::getQu() + double(nd)*Particle<ST>::getQd());
    Nfxx = (double(nu)*Particle<ST>::getQu()*Particle<ST>::getQu()
          + double(nd)*Particle<ST>::getQd()*Particle<ST>::getQd());
  }
  BaseClass::setNf(Nf_);
}

template <typename T>
void Amp4q0gAAx<T>::getfvpart1_full(const int fv, LoopValue* fvpart)
{
  return getfvpart1_full_(fv, fvpart);
}

template <typename T>
void Amp4q0gAAx<T>::getfvpart1_full(const int fv, LoopResult<T>* fvpart)
{
  return getfvpart1_full_(fv, fvpart);
}

template <typename T>
template <typename LT>
inline
void Amp4q0gAAx<T>::getfvpart1_full_(const int fv, LT* fvpart)
{
  fvSet(fv);

  if (false and fv % modFC == 0 and Nf != 0.) { // this part is zero
    // 1 primitive
    const LT Q1234 = AFx(0,1,2,3);

    fvpart[0] = Nfx*(-Q1234);
    fvpart[1] = Nfx*(Q1234/Nc);
  } else if (fv % modFC == 1 and Nf != 0.) {
    // 1 primitive
    const LT Q1234 = AFxx(0,1,2,3);

    fvpart[0] = Nfxx*(-Q1234);
    fvpart[1] = Nfxx*(Q1234/Nc);
  } else {
    fvpart[0] = LT();
    fvpart[1] = LT();
  }
}

template <typename T>
LoopResult<T> Amp4q0gAAx<T>::AFx(int p0, int p1, int p2, int p3)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, O[p1], O[p2], O[p3], NN+1};
  return BaseClass::AFnAAx(order);
}

template <typename T>
LoopResult<T> Amp4q0gAAx<T>::AFxx(int p0, int p1, int p2, int p3)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], O[p1], O[p2], O[p3], NN, NN+1};
  return BaseClass::AFnAAxx(order);
}

#ifdef USE_SD
  template class Amp4q0gAx<double>;
  template class Amp4q0gAxd<double>;
  template class Amp4q0gAA<double>;
  template class Amp4q0gAAd<double>;
  template class Amp4q0gAAx<double>;
  template class Amp4q0gAAxd<double>;
#endif
#ifdef USE_DD
  template class Amp4q0gAx<dd_real>;
  template class Amp4q0gAxd<dd_real>;
  template class Amp4q0gAA<dd_real>;
  template class Amp4q0gAAd<dd_real>;
  template class Amp4q0gAAx<dd_real>;
  template class Amp4q0gAAxd<dd_real>;
#endif
#ifdef USE_QD
  template class Amp4q0gAx<qd_real>;
  template class Amp4q0gAxd<qd_real>;
  template class Amp4q0gAA<qd_real>;
  template class Amp4q0gAAd<qd_real>;
  template class Amp4q0gAAx<qd_real>;
  template class Amp4q0gAAxd<qd_real>;
#endif
#ifdef USE_VC
  template class Amp4q0gAx<Vc::double_v>;
  template class Amp4q0gAxd<Vc::double_v>;
  template class Amp4q0gAA<Vc::double_v>;
  template class Amp4q0gAAd<Vc::double_v>;
  template class Amp4q0gAAx<Vc::double_v>;
  template class Amp4q0gAAxd<Vc::double_v>;
#endif
