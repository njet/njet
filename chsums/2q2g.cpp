/*
* chsums/2q2g.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>

#include "2q2g.h"
#include "../ngluon2/Model.h"

// class Amp2q2g

template <typename T>
Amp2q2g<T>::Amp2q2g(const T scalefactor, const int mFC, const NJetAmpTables& tables)
  : BaseClass(mFC, tables)
{
  for (int fv=0; fv<mFC; fv++) {
    BaseClass::initNG(scalefactor, fv);
    BaseClass::setProcess(StandardModel::NGluon1compat(NN, getFlav(fv)), fv);
  }

  initNc();
}

template <typename T>
void Amp2q2g<T>::setNc(const ST Nc_)
{
  BaseClass::setNc(Nc_);
  initNc();
}

template <typename T>
void Amp2q2g<T>::initNc()
{
  Nmat[0] = V;
  Nmat[1] = Nc;
  Nmat[2] = Nc2;
  Nmat[3] = -1.;
  assert(3 < BaseClass::NmatLen);

  Nmatcc[0] = 0.;
  Nmatcc[1] = Nc2;
  Nmatcc[2] = -Nc2*V;
  Nmatcc[3] = -Nc4;
  Nmatcc[4] = -1.;
  Nmatcc[5] = -1.-Nc2;
  assert(5 < BaseClass::NmatccLen);

  bornFactor = V/Nc;
  loopFactor = 2.*bornFactor;
  bornccFactor = -0.5*V/Nc2;
}

template <typename T>
void Amp2q2g<T>::getfvpart1_full(const int fv, LoopValue* fvpart)
{
  return getfvpart1_full_(fv, fvpart);
}

template <typename T>
void Amp2q2g<T>::getfvpart1_full(const int fv, LoopResult<T>* fvpart)
{
  return getfvpart1_full_(fv, fvpart);
}

// -------- BEGIN -- automatically generated code -- BEGIN -------- //

template <typename T>
void Amp2q2g<T>::getfvpart0(const int fv, TreeValue* fvpart, TreeValue* fvpartC)
{
  fvSet(fv);
  fvpart[0] = A0(0,1,2,3);
  fvpart[1] = A0(0,1,3,2);

  if (fvpartC) {
    for (int i=0; i<C0; i++) {
      fvpartC[i] = conj(fvpart[i]);
    }
  }
}

template <typename T>
template <typename LT>
inline
void Amp2q2g<T>::getfvpart1_full_(const int fv, LT* fvpart)
{
  fvSet(fv);
  {
    // 6 primitives
    const LT P1234 = AL(0,1,2,3);
    const LT P1243 = AL(0,1,3,2);
    const LT P1432 = AL(0,3,2,1);
    const LT P1342 = AL(0,2,3,1);
    const LT P1324 = AL(0,2,1,3);
    const LT P1423 = AL(0,3,1,2);

    fvpart[0] = Nc*P1234-P1432/Nc;
    fvpart[1] = Nc*P1243-P1342/Nc;
    fvpart[2] = P1234+P1243+P1324+P1423+P1342+P1432;
  }
  if (Nf != 0.) {
    // 1 primitives
    const LT Q1234 = AF(0,1,2,3);

    fvpart[0] += Nf*(-Q1234);
    fvpart[1] += Nf*(Q1234);
  }
}

// --------- END --- automatically generated code --- END --------- //

#ifdef USE_SD
  template class Amp2q2g<double>;
#endif
#ifdef USE_DD
  template class Amp2q2g<dd_real>;
#endif
#ifdef USE_QD
  template class Amp2q2g<qd_real>;
#endif
#ifdef USE_VC
  template class Amp2q2g<Vc::double_v>;
#endif
