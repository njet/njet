/*
* chsums/2q1gV.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>

#include "2q1gV.h"
#include "NJetAmp-T.h"

// class Amp2q1gV

template <typename T>
Amp2q1gV<T>::Amp2q1gV(const T scalefactor,
                      const int mFC, const NJetAmpTables& tables)
  : BaseClass(mFC, tables)
{
  for (int fv=0; fv<mFC; fv++) {
    BaseClass::initNG(scalefactor, fv);
  }

  const Flavour<double> ff = StandardModel::Wp(StandardModel::u(), StandardModel::ubar(), 1.);  // ckm = 1
  initProcess(ff);
  initNc();
}

template <typename T>
Amp2q1gV<T>::Amp2q1gV(const Flavour<double>& ff, const T scalefactor,
                      const int mFC, const NJetAmpTables& tables)
  : BaseClass(mFC, tables)
{
  for (int fv=0; fv<mFC; fv++) {
    BaseClass::initNG(scalefactor, fv);
  }

  initProcess(ff);
  initNc();
}

template <typename T>
void Amp2q1gV<T>::initProcess(const Flavour<double>& ff)
{
  for (int fv=0; fv<BaseClass::mFC; fv++) {
    Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
    flavours.push_back(ff);
    BaseClass::setProcess(flavours, fv);
  }
}

template <typename T>
void Amp2q1gV<T>::setNc(const ST Nc_)
{
  BaseClass::setNc(Nc_);
  initNc();
}

template <typename T>
void Amp2q1gV<T>::initNc()
{
  Nmat[0] = 1.;
  assert(0 < NJetAmp<T>::NmatLen);

  Nmatcc[0] = 0.;
  Nmatcc[1] = -1.;
  Nmatcc[2] = Nc2;
  assert(2 < NJetAmp<T>::NmatccLen);

  bornFactor = V;
  loopFactor = 2.*bornFactor;
  bornccFactor = 0.5*V/Nc;
}

template <typename T>
typename Amp2q1gV<T>::TreeValue
Amp2q1gV<T>::A0(int p0, int p1, int p2)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, O[p1], O[p2]};
  return BaseClass::A0nVqq(order);
}

template <typename T>
LoopResult<T> Amp2q1gV<T>::AF(int p0, int p1, int p2)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, O[p1], O[p2]};
  return BaseClass::AFnVqq(order);
}

template <typename T>
LoopResult<T> Amp2q1gV<T>::AL(int p0, int p1, int p2)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, O[p1], O[p2]};
  return BaseClass::ALnVqq(order);
}

template <typename T>
void Amp2q1gV<T>::getfvpart1_full(const int fv, LoopValue* fvpart)
{
  return getfvpart1_full_(fv, fvpart);
}

template <typename T>
void Amp2q1gV<T>::getfvpart1_full(const int fv, LoopResult<T>* fvpart)
{
  return getfvpart1_full_(fv, fvpart);
}

// -------- BEGIN -- automatically generated code -- BEGIN -------- //

template <typename T>
void Amp2q1gV<T>::getfvpart0(const int fv, TreeValue* fvpart, TreeValue* fvpartC)
{
  fvSet(fv);
  fvpart[0] = A0(0,1,2);

  if (fvpartC) {
    for (int i=0; i<C0; i++) {
      fvpartC[i] = conj(fvpart[i]);
    }
  }
}

template <typename T>
template <typename LT>
inline
void Amp2q1gV<T>::getfvpart1_full_(const int fv, LT* fvpart)
{
  fvSet(fv);
  {
    // 2 primitives
    const LT P123 = AL(0,1,2);
    const LT P132 = AL(0,2,1);

    fvpart[0] = Nc*P123 + P132/Nc;
  }
  if (Nf != 0.) {
    // 0 primitives
  }
}

// --------- END --- automatically generated code --- END --------- //

#ifdef USE_SD
  template class Amp2q1gV<double>;
#endif
#ifdef USE_DD
  template class Amp2q1gV<dd_real>;
#endif
#ifdef USE_QD
  template class Amp2q1gV<qd_real>;
#endif
#ifdef USE_VC
  template class Amp2q1gV<Vc::double_v>;
#endif
