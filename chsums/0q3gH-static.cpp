/*
* chsums/0q3gH-static.cpp
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>

#include "0q3gH.h"

// class Amp0q3gHStatic

const int
Amp0q3gHStatic::flav[FC][NN] = {
  {0,0,0}
};

const int
Amp0q3gHStatic::fvsign[FC] = {1};

const int
Amp0q3gHStatic::ccsign[NN*(NN+1)/2] = {1,1,1,1,1,1};

const int
Amp0q3gHStatic::fperm[FC][NN] = {
  {0,1,2}
};

const int
Amp0q3gHStatic::fvcol[FC][C0] = {
  {0,1}
};

const unsigned char
Amp0q3gHStatic::colmat[C0*(C0+1)/2] = {
  1,0,1
};

const unsigned char
Amp0q3gHStatic::colmatcc[NN*(NN+1)/2][C0*(C0+1)/2] = {
  {},
  {1,0,1},
  {},
  {1,0,1},
  {1,0,1},
  {},
};

const int
Amp0q3gHStatic::HSarr[HS][HSNN] = {
  { 1, 1, 1, 1},
  { 1, 1,-1, 1},
  { 1,-1, 1, 1},
  { 1,-1,-1, 1},
  {-1,-1,-1, 1},
  {-1,-1, 1, 1},
  {-1, 1,-1, 1},
  {-1, 1, 1, 1},
};
