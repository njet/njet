/*
* chsums/2q2gA.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef CHSUM_2Q2GA_H
#define CHSUM_2Q2GA_H

#include "2q2gV.h"

template <typename T>
class Amp2q2gA : public Amp2q2gZ<T>
{
    typedef Amp2q2gZ<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp2q2gA(const Flavour<double>& ff, const T scalefactor,
             const int mFC=1, const NJetAmpTables& tables=BaseClass::amptables())
    : BaseClass(ff, scalefactor, mFC, tables)
    { }

    void born_part_fill() { BaseClass::born_part_trickfill(); }
    LoopValue virt() { return BaseClass::virt_tricksum(); }
};

// One photon vector loop

class Amp2q2gAxStatic : public Amp2q2gZStatic
{
  public:
    static const int FC = 2;

    static const int flav[FC][NN];  // flavour vectors for ngluon
    static const int fvsign[FC];    // flavour channels' signs
    static const int fperm[FC][NN];        // flavour pemutation matrix
    static const int fvcol[FC][CC];        // flavour-partial pemutation matrix
};

template <typename T>
class Amp2q2gAx : public Amp2q2gA<T>
{
    typedef Amp2q2gA<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;
    typedef typename BaseClass::ST ST;

    Amp2q2gAx(const Flavour<double>& ff, const T scalefactor,
              const int mFC=2, const NJetAmpTables& tables=amptables());

    void setNf(const ST Nf_);

  protected:
    using BaseClass::NN;
    using BaseClass::Nc;
    using BaseClass::Nf;
    using BaseClass::vhel;
    using BaseClass::getFlav;
    using BaseClass::getFperm;
    using BaseClass::fvSet;
    using BaseClass::mfv;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp2q2gAxStatic>();
    }

    LoopResult<T> AFx(int p0, int p1, int p2, int p3);

    void getfvpart1_full(const int fv, LoopValue* fvpart);
    void getfvpart1_full(const int fv, LoopResult<T>* fvpart);

    template <typename LT>
    void getfvpart1_full_(const int fv, LT* fvpart);
};

// Two photons

class Amp2q2gAAStatic : public Amp2q2gStatic
{
  public:
    static const int HS = 32;
    static const int HSNN = NN+2;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp2q2gAA : public Amp2q2g<T>
{
    typedef Amp2q2g<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp2q2gAA(const Flavour<double>& Vflav, const T scalefactor,
              const int mFC=1, const NJetAmpTables& tables=amptables());

  protected:
    using BaseClass::NN;
    using BaseClass::vhel;
    using BaseClass::getFlav;
    using BaseClass::getFperm;
    using BaseClass::mfv;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp2q2gAAStatic>();
    }

    void initProcess(const Flavour<double>& ff);

    TreeValue A0(int p0, int p1, int p2, int p3);
    LoopResult<T> AL(int p0, int p1, int p2, int p3);
    LoopResult<T> AF(int p0, int p1, int p2, int p3);
};

// Two photons vector loop

class Amp2q2gAAxStatic : public Amp2q2gAAStatic
{
  public:
    static const int FC = 3;

    static const int flav[FC][NN];  // flavour vectors for ngluon
    static const int fvsign[FC];    // flavour channels' signs
    static const int fperm[FC][NN];        // flavour pemutation matrix
    static const int fvcol[FC][CC];        // flavour-partial pemutation matrix
};

template <typename T>
class Amp2q2gAAx : public Amp2q2gAA<T>
{
    typedef Amp2q2gAA<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;
    typedef typename BaseClass::ST ST;

    Amp2q2gAAx(const Flavour<double>& ff, const T scalefactor,
               const int mFC=3, const NJetAmpTables& tables=amptables());

    void setNf(const ST Nf_);

  protected:
    using BaseClass::NN;
    using BaseClass::Nc;
    using BaseClass::Nf;
    using BaseClass::vhel;
    using BaseClass::getFlav;
    using BaseClass::getFperm;
    using BaseClass::fvSet;
    using BaseClass::mfv;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp2q2gAAxStatic>();
    }

    static const int modFC = 3;
    ST Nfx, Nfxx;

    LoopResult<T> AFx(int p0, int p1, int p2, int p3);
    LoopResult<T> AFxx(int p0, int p1, int p2, int p3);

    void getfvpart1_full(const int fv, LoopValue* fvpart);
    void getfvpart1_full(const int fv, LoopResult<T>* fvpart);

    template <typename LT>
    void getfvpart1_full_(const int fv, LT* fvpart);
};

#endif // CHSUM_2Q2GA_H
