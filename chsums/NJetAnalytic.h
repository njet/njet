/*
* chsums/NJetAnalytic.h
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef CHSUMS_NJETANALYTIC_H
#define CHSUMS_NJETANALYTIC_H

#include "../ngluon2/NAmp.h"

template <typename T> struct I2arg;
template <typename T> struct I3arg;
template <typename T> struct I4arg;

template <typename T>
class NJetAnalytic
{
  public:
    typedef typename NAmp<T>::RealMom RealMom;
    typedef typename vector_traits<T>::scalar ST;

    typedef EpsTriplet<T> LoopValue;
    typedef std::complex<T> TreeValue;

    NJetAnalytic(const T scalefactor_, const int NMOM_, int extscalepow=0);
    ~NJetAnalytic();

    void setMomenta(const RealMom* moms);

    template <typename U>
    void setMomenta(const MOM<U>* othermoms);

    void setMuR2(const T rscale);
    T getMuR2() const { return MuR2; }

    std::complex<T> CyclicSpinorsA(const int* ord) const;
    std::complex<T> CyclicSpinorsB(const int* ord) const;
    int HelicityOrder(const int h, const int* ord) const;

    std::complex<T> sA(int i, int j) const { return spvA[i + NMOM*j]; }
    std::complex<T> sB(int i, int j) const { return spvB[i + NMOM*j]; }
    std::complex<T> sAB(int i, int j) const { return spvAB[i + NMOM*j]; }
    T lS(int i, int j) const { return lpvS[i + NMOM*j]; }
    T lS(int i, int j, int k) const { return lS(i, j) + lS(i, k) + lS(j, k); }

    int legsMOM() const { return NMOM; }
    void setNAB(int NAB_) { NAB = NAB_; }

    template <typename U>
    U rescaleAmp(const U value) const { return amprescale*value; }

    // short calls imply MuR2
    EpsTriplet<T> I2(const T s, const T m1, const T m2) {
      return I2(s, m1, m2, MuR2);
    }
    EpsTriplet<T> I3(const T M1, const T M2, const T M3,
                     const T m1, const T m2, const T m3)  {
      return I3(M1, M2, M3, m1, m2, m3, MuR2);
    }
    EpsTriplet<T> I4(const T s, const T t,
                     const T M1, const T M2, const T M3, const T M4,
                     const T m1, const T m2, const T m3, const T m4) {
      return I4(s, t, M1, M2, M3, M4, m1, m2, m3, m4, MuR2);
    }
    // long explicit calls (make sure mur is correctly scaled)
    EpsTriplet<T> I2(const T s, const T m1, const T m2, const T mur);
    EpsTriplet<T> I3(const T M1, const T M2, const T M3,
                     const T m1, const T m2, const T m3, const T mur);
    EpsTriplet<T> I4(const T s, const T t,
                     const T M1, const T M2, const T M3, const T M4,
                     const T m1, const T m2, const T m3, const T m4, const T mur);

    // Finite log functions used in BDK scale removal of spurious poles
    std::complex<T> L0(const T a, const T b);
    std::complex<T> L1(const T a, const T b);
    std::complex<T> L2hat(const T a, const T b);
    std::complex<T> L3hat(const T a, const T b);

  protected:
    std::complex<T>& sA(int i, int j) { return spvA[i + NMOM*j]; }
    std::complex<T>& sB(int i, int j) { return spvB[i + NMOM*j]; }
    std::complex<T>& sAB(int i, int j) { return spvAB[i + NMOM*j]; }
    T& lS(int i, int j) { return lpvS[i + NMOM*j]; }

    void initKinematics();

    int NMOM, NAB;

    T scalefactor, amprescale, MuR2;

    std::vector<std::complex<T> > spvA, spvB, spvAB;
    std::vector<T> lpvS;
    std::vector<RealMom> moms;

    void resetCache();

  private:
    std::vector<I2arg<T> >* cacheI2args;
    std::vector<I3arg<T> >* cacheI3args;
    std::vector<I4arg<T> >* cacheI4args;

    std::vector<EpsTriplet<T> > cacheI2vals;
    std::vector<EpsTriplet<T> > cacheI3vals;
    std::vector<EpsTriplet<T> > cacheI4vals;
};

#endif /* CHSUMS_NJETANALYTIC_H */
