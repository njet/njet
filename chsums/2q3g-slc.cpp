/*
* chsums/2q3g-slc.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include "2q3g.h"

template <typename T>
void Amp2q3g<T>::getfvpart1_slc(const int fv, LoopValue* fvpart)
{
  return getfvpart1_slc_(fv, fvpart);
}

template <typename T>
void Amp2q3g<T>::getfvpart1_slc(const int fv, LoopResult<T>* fvpart)
{
  return getfvpart1_slc_(fv, fvpart);
}

// -------- BEGIN -- automatically generated code -- BEGIN -------- //

template <typename T>
template <typename LT>
inline
void Amp2q3g<T>::getfvpart1_slc_(const int fv, LT* fvpart)
{
  fvSet(fv);
  {
    const int norder = norderL;

    if (norder < 1) {
    // [0, -1] order 24 primitives
    const LT P12345 = AL(0,1,2,3,4, 0);
    const LT P12354 = AL(0,1,2,4,3, 1);
    const LT P12435 = AL(0,1,3,2,4, 2);
    const LT P12453 = AL(0,1,3,4,2, 3);
    const LT P12534 = AL(0,1,4,2,3, 4);
    const LT P12543 = AL(0,1,4,3,2, 5);
    const LT P13245 = AL(0,2,1,3,4, 6);
    const LT P13254 = AL(0,2,1,4,3, 7);
    const LT P13425 = AL(0,2,3,1,4, 8);
    const LT P13452 = AL(0,2,3,4,1, 9);
    const LT P13524 = AL(0,2,4,1,3, 10);
    const LT P13542 = AL(0,2,4,3,1, 11);
    const LT P14235 = AL(0,3,1,2,4, 12);
    const LT P14253 = AL(0,3,1,4,2, 13);
    const LT P14325 = AL(0,3,2,1,4, 14);
    const LT P14352 = AL(0,3,2,4,1, 15);
    const LT P14523 = AL(0,3,4,1,2, 16);
    const LT P14532 = AL(0,3,4,2,1, 17);
    const LT P15234 = AL(0,4,1,2,3, 18);
    const LT P15243 = AL(0,4,1,3,2, 19);
    const LT P15324 = AL(0,4,2,1,3, 20);
    const LT P15342 = AL(0,4,2,3,1, 21);
    const LT P15423 = AL(0,4,3,1,2, 22);
    const LT P15432 = AL(0,4,3,2,1, 23);
    fvpart[0] = P15432/Nc;
    fvpart[1] = P14532/Nc;
    fvpart[2] = P15342/Nc;
    fvpart[3] = P13542/Nc;
    fvpart[4] = P14352/Nc;
    fvpart[5] = P13452/Nc;
    fvpart[6] = P12345+P12354+P12435+P12453+P12534+P12543+P13245+P13254+P13425+P14235+P14253+P14325;
    fvpart[7] = P12345+P12354+P12435+P12453+P12534+P12543+P13245+P13254+P13524+P15234+P15243+P15324;
    fvpart[8] = P12345+P12354+P12435+P12453+P12534+P12543+P14235+P14253+P14523+P15234+P15243+P15423;
    fvpart[9] = -P12354-P12435-P12543-P13254-P13524-P13542-P14235-P14325-P14352-P15243-P15423-P15432;
    fvpart[10] = -P12345-P12453-P12534-P13245-P13425-P13452-P14253-P14523-P14532-P15234-P15324-P15342;

    if (norder < 0) {
    // [1] order 0 primitives
    fvpart[0] += Nc*P12345;
    fvpart[1] += Nc*P12354;
    fvpart[2] += Nc*P12435;
    fvpart[3] += Nc*P12453;
    fvpart[4] += Nc*P12534;
    fvpart[5] += Nc*P12543;
    }}
  }
  if (Nf != 0.) {
    const int norder = norderF;

    if (norder < 0) {
    // [0, -1] order 6 primitives
    const LT Q12345 = AF(0,1,2,3,4, 0,23);
    const LT Q12354 = AF(0,1,2,4,3, 1,17);
    const LT Q12435 = AF(0,1,3,2,4, 2,21);
    const LT Q12453 = AF(0,1,3,4,2, 3,11);
    const LT Q12534 = AF(0,1,4,2,3, 4,15);
    const LT Q12543 = AF(0,1,4,3,2, 5, 9);
    fvpart[0] += Nf*(-Q12345);
    fvpart[1] += Nf*(-Q12354);
    fvpart[2] += Nf*(-Q12435);
    fvpart[3] += Nf*(-Q12453);
    fvpart[4] += Nf*(-Q12534);
    fvpart[5] += Nf*(-Q12543);
    fvpart[9] += Nf*((Q12345+Q12354+Q12435+Q12453+Q12534+Q12543)/(2.*Nc));
    fvpart[10] += Nf*((Q12345+Q12354+Q12435+Q12453+Q12534+Q12543)/(2.*Nc));
    }
  }
}

// --------- END --- automatically generated code --- END --------- //

#define INSTANTIATE(T) \
  template void Amp2q3g<T>::getfvpart1_slc(const int fv, LoopValue* fvpart); \
  template void Amp2q3g<T>::getfvpart1_slc(const int fv, LoopResult<T>* fvpart);

#ifdef USE_SD
  INSTANTIATE(double)
#endif
#ifdef USE_DD
  INSTANTIATE(dd_real)
#endif
#ifdef USE_QD
  INSTANTIATE(qd_real)
#endif
#ifdef USE_VC
  INSTANTIATE(Vc::double_v)
#endif

#undef INSTANTIATE
