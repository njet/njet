/*
* chsums/NJetInterface.cpp
*
* This file is part of NJet library
* Copyright (C) 2013 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>

#include "NJetInterface.h"
#include "NJetAccuracy.h"

#include "../analytic/0q3gH-analytic.h"
#include "../analytic/2q1gH-analytic.h"
#include "../analytic/0q4gH-analytic.h"
#include "../analytic/0q5gH-analytic.h"

#include "../analytic/0q4g-analytic.h"
#include "../analytic/2q2g-analytic.h"
#include "../analytic/4q0g-analytic.h"

#include "../analytic/0q5g-analytic.h"
#include "../analytic/0q5g-ds-analytic.h"
#include "../analytic/2q3g-analytic.h"
#include "../analytic/2q3g-ds-analytic.h"
#include "../analytic/4q1g-analytic.h"

#include "../analytic/0q6g-analytic.h"

#include "0q4g.h"
#include "2q2g.h"
#include "4q0g.h"

#include "0q5g.h"
#include "2q3g.h"
#include "4q1g.h"

#include "0q6g.h"
#include "2q4g.h"
#include "2q4g-ds.h"
#include "4q2g.h"
#include "6q0g.h"


#ifdef NJET_ENABLE_5
#include "0q7g.h"
#include "2q5g.h"
#include "2q5g-ds.h"
#include "4q3g.h"
#include "6q1g.h"
#endif

#include "2q0gV.h"
#include "2q1gV.h"

#include "2q0gA.h"
#include "2q1gA.h"

#include "0q3gH.h"
#include "2q1gH.h"

#include "2q2gV.h"
#include "4q0gV.h"

#include "2q2gA.h"
#include "4q0gA.h"

#include "0q4gH.h"
#include "2q2gH.h"
#include "4q0gH.h"

#include "2q3gV.h"
#include "2q3gV-ds.h"
#include "4q1gV.h"

#include "2q3gA.h"
#include "2q3gA-ds.h"
#include "4q1gA.h"

#include "0q5gH.h"
#include "2q3gH.h"
#include "4q1gH.h"

#include "2q4gV.h"
#include "2q4gV-ds.h"
#include "4q2gV.h"
#include "6q0gV.h"

#include "2q4gA.h"
#include "2q4gA-ds.h"
#include "4q2gA.h"
#include "6q0gA.h"

#include "0q6gH.h"
#include "2q4gH.h"
#include "4q2gH.h"
#include "6q0gH.h"

#ifdef NJET_ENABLE_5
#include "2q5gV.h"
#include "2q5gV-ds.h"
#include "4q3gV.h"
#include "6q1gV.h"
#endif

template <typename T>
typename NJetInterface<T>::ST NJetInterface<T>::DEFAULT_Nc = double(NJetAmpTables::DEFAULT_Nc);

template <typename T>
typename NJetInterface<T>::ST NJetInterface<T>::DEFAULT_Nf = double(NJetAmpTables::DEFAULT_Nf);

template <typename T>
int NJetInterface<T>::DEFAULT_scheme = NJetRenorm<T>::SCHEME_FDH;

template <typename T>
int NJetInterface<T>::DEFAULT_renorm = NJetRenorm<T>::UNRENORM;

template <typename T>
NJetInterface<T>::NJetInterface(const ST Nc_, const ST Nf_, int scm_, int ren_)
  : Nc(Nc_), Nf(Nf_), scheme(scm_), renorm(ren_)
{
}

template <typename T>
NJetInterface<T>::~NJetInterface()
{
  clear();
}

template <typename T>
void NJetInterface<T>::clear()
{
  for (typename AmpMap::iterator it=amps.begin(); it!=amps.end(); /* noop */) {
    if (it->second) {
      delete it->second;
      amps.erase(it++);
    } else {
      ++it;
    }
  }
}

template <typename T>
void NJetInterface<T>::setNc(const ST Nc_)
{
  Nc = Nc_;
  for (typename AmpMap::iterator it=amps.begin(); it!=amps.end(); ++it) {
    if (it->second) {
      it->second->setNc(Nc);
    }
  }
}

template <typename T>
void NJetInterface<T>::setNf(const ST Nf_)
{
  Nf = Nf_;
  for (typename AmpMap::iterator it=amps.begin(); it!=amps.end(); ++it) {
    if (it->second) {
      it->second->setNf(Nf);
    }
  }
}

template <typename T>
void NJetInterface<T>::setScheme(int scheme_)
{
  scheme = scheme_;
  for (typename AmpMap::iterator it=amps.begin(); it!=amps.end(); ++it) {
    if (it->second) {
      it->second->setScheme(scheme);
    }
  }
}

template <typename T>
void NJetInterface<T>::setRenorm(int renorm_)
{
  renorm = renorm_;
  for (typename AmpMap::iterator it=amps.begin(); it!=amps.end(); ++it) {
    if (it->second) {
      it->second->setRenorm(renorm);
    }
  }
}

template <typename T>
typename NJetInterface<T>::AmpType NJetInterface<T>::getAmp(const int idx)
{
  AmpType val = amps[idx];
  if (not val) {
    amps[idx] = val = createAmp(idx);
  }
  return val;
}

template <typename T>
typename NJetInterface<T>::AmpType NJetInterface<T>::createAmp(const int idx)
{
  AmpType val = AmpType();

  const Flavour<double> Zu = StandardModel::Zu(StandardModel::u(), StandardModel::ubar());
  const Flavour<double> Zd = StandardModel::Zd(StandardModel::u(), StandardModel::ubar());

  const Flavour<double> Au = StandardModel::Au(StandardModel::u(), StandardModel::ubar());
  const Flavour<double> Ad = StandardModel::Ad(StandardModel::u(), StandardModel::ubar());

  bool valid = true;

  const int norderL = 0;
  const int norderF = -1;

  switch (idx) {
    case EGGGG:
      val = NJetAccuracy<T>::template create<Amp0q4g_a<T> >();
    break;

    case EddGG:
      val = NJetAccuracy<T>::template create<Amp2q2g_a<T> >();
    break;

    case Edduu:
      val = NJetAccuracy<T>::template create<Amp4q0g_a<T> >();
    break;

    case Edddd:
      val = NJetAccuracy<T>::template create<Amp4q0g2_a<T> >();
    break;

    case EGGGGG:
      val = NJetAccuracy<T>::template create<Amp0q5g_a<T> >();
    break;

    case EddGGG:
      val = NJetAccuracy<T>::template create<Amp2q3g_a<T> >();
    break;

    case EdduuG:
      val = NJetAccuracy<T>::template create<Amp4q1g_a<T> >();
    break;

    case EddddG:
      val = NJetAccuracy<T>::template create<Amp4q1g2_a<T> >();
    break;

    case EGGGGGG:
      val = NJetAccuracy<T>::template create<Amp0q6g<T> >();
    break;

    case EddGGGG:
      val = NJetAccuracy<T>::template create<Amp2q4g<T> >();
    break;

    case EdduuGG:
      val = NJetAccuracy<T>::template create<Amp4q2g<T> >();
    break;

    case EddddGG:
      val = NJetAccuracy<T>::template create<Amp4q2g2<T> >();
    break;

    case Edduuss:
      val = NJetAccuracy<T>::template create<Amp6q0g<T> >();
    break;

    case Euudddd:
      val = NJetAccuracy<T>::template create<Amp6q0g2<T> >();
    break;

    case Edddddd:
      val = NJetAccuracy<T>::template create<Amp6q0g6<T> >();
    break;

#ifdef NJET_ENABLE_5
    case EGGGGGGG:
      val = NJetAccuracy<T>::template create<Amp0q7g<T> >();
    break;

    case EddGGGGG:
      val = NJetAccuracy<T>::template create<Amp2q5g<T> >();
    break;

    case EdduuGGG:
      val = NJetAccuracy<T>::template create<Amp4q3g<T> >();
    break;

    case EddddGGG:
      val = NJetAccuracy<T>::template create<Amp4q3g2<T> >();
    break;

    case EdduussG:
      val = NJetAccuracy<T>::template create<Amp6q1g<T> >();
    break;

    case EuuddddG:
      val = NJetAccuracy<T>::template create<Amp6q1g2<T> >();
    break;

    case EddddddG:
      val = NJetAccuracy<T>::template create<Amp6q1g6<T> >();
    break;
#endif

    case EudW:
      val = NJetAccuracy<T>::template create<Amp2q0gV<T> >();
    break;

    case EudGW:
      val = NJetAccuracy<T>::template create<Amp2q1gV<T> >();
    break;

    case EudGGW:
      val = NJetAccuracy<T>::template create<Amp2q2gV<T> >();
    break;

    case EudssW:
    case EudccW:
      val = NJetAccuracy<T>::template create<Amp4q0gV<T> >();
    break;

    case EudddW:
      val = NJetAccuracy<T>::template create<Amp4q0gV2<T> >();
    break;

    case EuduuW:
      val = NJetAccuracy<T>::template create<Amp4q0gV2b<T> >();
    break;

    case EudGGGW:
      val = NJetAccuracy<T>::template create<Amp2q3gV<T> >();
    break;

    case EudssGW:
    case EudccGW:
      val = NJetAccuracy<T>::template create<Amp4q1gV<T> >();
    break;

    case EudddGW:
      val = NJetAccuracy<T>::template create<Amp4q1gV2<T> >();
    break;

    case EuduuGW:
      val = NJetAccuracy<T>::template create<Amp4q1gV2b<T> >();
    break;

    case EudGGGGW:
      val = NJetAccuracy<T>::template create<Amp2q4gV<T> >();
    break;

    case EudssGGW:
    case EudccGGW:
      val = NJetAccuracy<T>::template create<Amp4q2gV<T> >();
    break;

    case EudddGGW:
      val = NJetAccuracy<T>::template create<Amp4q2gV2<T> >();
    break;

    case EuduuGGW:
      val = NJetAccuracy<T>::template create<Amp4q2gV2b<T> >();
    break;

    case EudssccW:
    case EudssbbW:
    case EudccttW:
      val = NJetAccuracy<T>::template create<Amp6q0gV<T> >();
    break;

    case EudssssW:
    case EudccccW:
      val = NJetAccuracy<T>::template create<Amp6q0gV2<T> >();
    break;

    case EudddssW:
    case EudddccW:
      val = NJetAccuracy<T>::template create<Amp6q0gV2n<T> >();
    break;

    case EuduussW:
    case EuduuccW:
      val = NJetAccuracy<T>::template create<Amp6q0gV2b<T> >();
    break;

    case EudddddW:
      val = NJetAccuracy<T>::template create<Amp6q0gV6n<T> >();
    break;

    case EuduuuuW:
      val = NJetAccuracy<T>::template create<Amp6q0gV6b<T> >();
    break;

    case EuddduuW:
      val = NJetAccuracy<T>::template create<Amp6q0gV4nb<T> >();
    break;

#ifdef NJET_ENABLE_5
    case EudGGGGGW:
      val = NJetAccuracy<T>::template create<Amp2q5gV<T> >();
    break;

    case EudssGGGW:
    case EudccGGGW:
      val = NJetAccuracy<T>::template create<Amp4q3gV<T> >();
    break;

    case EudddGGGW:
      val = NJetAccuracy<T>::template create<Amp4q3gV2<T> >();
    break;

    case EuduuGGGW:
      val = NJetAccuracy<T>::template create<Amp4q3gV2b<T> >();
    break;

    case EudssccGW:
    case EudssbbGW:
    case EudccttGW:
      val = NJetAccuracy<T>::template create<Amp6q1gV<T> >();
    break;

    case EudssssGW:
    case EudccccGW:
      val = NJetAccuracy<T>::template create<Amp6q1gV2<T> >();
    break;

    case EudddssGW:
    case EudddccGW:
      val = NJetAccuracy<T>::template create<Amp6q1gV2n<T> >();
    break;

    case EuduussGW:
    case EuduuccGW:
      val = NJetAccuracy<T>::template create<Amp6q1gV2b<T> >();
    break;

    case EudddddGW:
      val = NJetAccuracy<T>::template create<Amp6q1gV6n<T> >();
    break;

    case EuduuuuGW:
      val = NJetAccuracy<T>::template create<Amp6q1gV6b<T> >();
    break;

    case EuddduuGW:
      val = NJetAccuracy<T>::template create<Amp6q1gV4nb<T> >();
    break;
#endif

    case EddZ:
      val = NJetAccuracy<T>::template create<Amp2q0gZ<T> >(Zd);
    break;

    case EuuZ:
      val = NJetAccuracy<T>::template create<Amp2q0gZ<T> >(Zu);
    break;

    case EddGZ:
      val = NJetAccuracy<T>::template create<Amp2q1gZ<T> >(Zd);
    break;

    case EuuGZ:
      val = NJetAccuracy<T>::template create<Amp2q1gZ<T> >(Zu);
    break;

    case EddGGZ:
      val = NJetAccuracy<T>::template create<Amp2q2gZ<T> >(Zd);
    break;

    case EuuGGZ:
      val = NJetAccuracy<T>::template create<Amp2q2gZ<T> >(Zu);
    break;

    case EddssZ:
      val = NJetAccuracy<T>::template create<Amp4q0gZ<T> >(Zd);
    break;

    case EdduuZ:
      val = NJetAccuracy<T>::template create<Amp4q0gZd<T> >(Zd);
    break;

    case EuuccZ:
      val = NJetAccuracy<T>::template create<Amp4q0gZ<T> >(Zu);
    break;

    case EddddZ:
      val = NJetAccuracy<T>::template create<Amp4q0gZ2<T> >(Zd);
    break;

    case EuuuuZ:
      val = NJetAccuracy<T>::template create<Amp4q0gZ2<T> >(Zu);
    break;

    case EddGGGZ:
      val = NJetAccuracy<T>::template create<Amp2q3gZ<T> >(Zd);
    break;

    case EuuGGGZ:
      val = NJetAccuracy<T>::template create<Amp2q3gZ<T> >(Zu);
    break;

    case EddssGZ:
      val = NJetAccuracy<T>::template create<Amp4q1gZ<T> >(Zd);
    break;

    case EdduuGZ:
      val = NJetAccuracy<T>::template create<Amp4q1gZd<T> >(Zd);
    break;

    case EuuccGZ:
      val = NJetAccuracy<T>::template create<Amp4q1gZ<T> >(Zu);
    break;

    case EddddGZ:
      val = NJetAccuracy<T>::template create<Amp4q1gZ2<T> >(Zd);
    break;

    case EuuuuGZ:
      val = NJetAccuracy<T>::template create<Amp4q1gZ2<T> >(Zu);
    break;

    case EddGGGGZ:
      val = NJetAccuracy<T>::template create<Amp2q4gZ<T> >(Zd);
    break;

    case EuuGGGGZ:
      val = NJetAccuracy<T>::template create<Amp2q4gZ<T> >(Zu);
    break;

    case EddssGGZ:
      val = NJetAccuracy<T>::template create<Amp4q2gZ<T> >(Zd);
    break;

    case EdduuGGZ:
      val = NJetAccuracy<T>::template create<Amp4q2gZd<T> >(Zd);
    break;

    case EuuccGGZ:
      val = NJetAccuracy<T>::template create<Amp4q2gZ<T> >(Zu);
    break;

    case EddddGGZ:
      val = NJetAccuracy<T>::template create<Amp4q2gZ2<T> >(Zd);
    break;

    case EuuuuGGZ:
      val = NJetAccuracy<T>::template create<Amp4q2gZ2<T> >(Zu);
    break;

    case EddssbbZ:
      val = NJetAccuracy<T>::template create<Amp6q0gZ<T> >(Zd);
    break;

    case EuuccttZ:
      val = NJetAccuracy<T>::template create<Amp6q0gZ<T> >(Zu);
    break;

    case EddssuuZ:
      val = NJetAccuracy<T>::template create<Amp6q0gZd<T> >(Zd);
    break;

    case EuuccddZ:
      val = NJetAccuracy<T>::template create<Amp6q0gZd<T> >(Zu);
    break;

    case EddddssZ:
      val = NJetAccuracy<T>::template create<Amp6q0gZ2<T> >(Zd);
    break;

    case EuuuuccZ:
      val = NJetAccuracy<T>::template create<Amp6q0gZ2<T> >(Zu);
    break;

    case EdddduuZ:
      val = NJetAccuracy<T>::template create<Amp6q0gZ2d<T> >(Zd);
    break;

    case EuuuuddZ:
      val = NJetAccuracy<T>::template create<Amp6q0gZ2d<T> >(Zu);
    break;

    case EddddddZ:
      val = NJetAccuracy<T>::template create<Amp6q0gZ6<T> >(Zd);
    break;

    case EuuuuuuZ:
      val = NJetAccuracy<T>::template create<Amp6q0gZ6<T> >(Zu);
    break;

#ifdef NJET_ENABLE_5
    case EddGGGGGZ:
      val = NJetAccuracy<T>::template create<Amp2q5gZ<T> >(Zd);
    break;

    case EuuGGGGGZ:
      val = NJetAccuracy<T>::template create<Amp2q5gZ<T> >(Zu);
    break;

    case EddssGGGZ:
      val = NJetAccuracy<T>::template create<Amp4q3gZ<T> >(Zd);
    break;

    case EdduuGGGZ:
      val = NJetAccuracy<T>::template create<Amp4q3gZd<T> >(Zd);
    break;

    case EuuccGGGZ:
      val = NJetAccuracy<T>::template create<Amp4q3gZ<T> >(Zu);
    break;

    case EddddGGGZ:
      val = NJetAccuracy<T>::template create<Amp4q3gZ2<T> >(Zd);
    break;

    case EuuuuGGGZ:
      val = NJetAccuracy<T>::template create<Amp4q3gZ2<T> >(Zu);
    break;

    case EddssbbGZ:
      val = NJetAccuracy<T>::template create<Amp6q1gZ<T> >(Zd);
    break;

    case EuuccttGZ:
      val = NJetAccuracy<T>::template create<Amp6q1gZ<T> >(Zu);
    break;

    case EddssuuGZ:
      val = NJetAccuracy<T>::template create<Amp6q1gZd<T> >(Zd);
    break;

    case EuuccddGZ:
      val = NJetAccuracy<T>::template create<Amp6q1gZd<T> >(Zu);
    break;

    case EddddssGZ:
      val = NJetAccuracy<T>::template create<Amp6q1gZ2<T> >(Zd);
    break;

    case EuuuuccGZ:
      val = NJetAccuracy<T>::template create<Amp6q1gZ2<T> >(Zu);
    break;

    case EdddduuGZ:
      val = NJetAccuracy<T>::template create<Amp6q1gZ2d<T> >(Zd);
    break;

    case EuuuuddGZ:
      val = NJetAccuracy<T>::template create<Amp6q1gZ2d<T> >(Zu);
    break;

    case EddddddGZ:
      val = NJetAccuracy<T>::template create<Amp6q1gZ6<T> >(Zd);
    break;

    case EuuuuuuGZ:
      val = NJetAccuracy<T>::template create<Amp6q1gZ6<T> >(Zu);
    break;
#endif

    case EddGA:
      val = NJetAccuracy<T>::template create<Amp2q1gA<T> >(Ad);
    break;

    case EuuGA:
      val = NJetAccuracy<T>::template create<Amp2q1gA<T> >(Au);
    break;

    case EddGGA:
      val = NJetAccuracy<T>::template create<Amp2q2gA<T> >(Ad);
    break;

    case EuuGGA:
      val = NJetAccuracy<T>::template create<Amp2q2gA<T> >(Au);
    break;

    case EddssA:
      val = NJetAccuracy<T>::template create<Amp4q0gA<T> >(Ad);
    break;

    case EdduuA:
      val = NJetAccuracy<T>::template create<Amp4q0gAd<T> >(Ad);
    break;

    case EuuccA:
      val = NJetAccuracy<T>::template create<Amp4q0gA<T> >(Au);
    break;

    case EddddA:
      val = NJetAccuracy<T>::template create<Amp4q0gA2<T> >(Ad);
    break;

    case EuuuuA:
      val = NJetAccuracy<T>::template create<Amp4q0gA2<T> >(Au);
    break;

    case EddGGGA:
      val = NJetAccuracy<T>::template create<Amp2q3gA<T> >(Ad);
    break;

    case EuuGGGA:
      val = NJetAccuracy<T>::template create<Amp2q3gA<T> >(Au);
    break;

    case EddssGA:
      val = NJetAccuracy<T>::template create<Amp4q1gA<T> >(Ad);
    break;

    case EdduuGA:
      val = NJetAccuracy<T>::template create<Amp4q1gAd<T> >(Ad);
    break;

    case EuuccGA:
      val = NJetAccuracy<T>::template create<Amp4q1gA<T> >(Au);
    break;

    case EddddGA:
      val = NJetAccuracy<T>::template create<Amp4q1gA2<T> >(Ad);
    break;

    case EuuuuGA:
      val = NJetAccuracy<T>::template create<Amp4q1gA2<T> >(Au);
    break;

    case EddGGGGA:
      val = NJetAccuracy<T>::template create<Amp2q4gA<T> >(Ad);
    break;

    case EuuGGGGA:
      val = NJetAccuracy<T>::template create<Amp2q4gA<T> >(Au);
    break;

    case EddssGGA:
      val = NJetAccuracy<T>::template create<Amp4q2gA<T> >(Ad);
    break;

    case EdduuGGA:
      val = NJetAccuracy<T>::template create<Amp4q2gAd<T> >(Ad);
    break;

    case EuuccGGA:
      val = NJetAccuracy<T>::template create<Amp4q2gA<T> >(Au);
    break;

    case EddddGGA:
      val = NJetAccuracy<T>::template create<Amp4q2gA2<T> >(Ad);
    break;

    case EuuuuGGA:
      val = NJetAccuracy<T>::template create<Amp4q2gA2<T> >(Au);
    break;

    case EddssbbA:
      val = NJetAccuracy<T>::template create<Amp6q0gA<T> >(Ad);
    break;

    case EuuccttA:
      val = NJetAccuracy<T>::template create<Amp6q0gA<T> >(Au);
    break;

    case EddssuuA:
      val = NJetAccuracy<T>::template create<Amp6q0gAd<T> >(Ad);
    break;

    case EuuccddA:
      val = NJetAccuracy<T>::template create<Amp6q0gAd<T> >(Au);
    break;

    case EddddssA:
      val = NJetAccuracy<T>::template create<Amp6q0gA2<T> >(Ad);
    break;

    case EuuuuccA:
      val = NJetAccuracy<T>::template create<Amp6q0gA2<T> >(Au);
    break;

    case EdddduuA:
      val = NJetAccuracy<T>::template create<Amp6q0gA2d<T> >(Ad);
    break;

    case EuuuuddA:
      val = NJetAccuracy<T>::template create<Amp6q0gA2d<T> >(Au);
    break;

    case EddddddA:
      val = NJetAccuracy<T>::template create<Amp6q0gA6<T> >(Ad);
    break;

    case EuuuuuuA:
      val = NJetAccuracy<T>::template create<Amp6q0gA6<T> >(Au);
    break;

#ifdef NJET_ENABLE_5
    case EddGGGGGA:
      val = NJetAccuracy<T>::template create<Amp2q5gZ<T> >(Ad);
    break;

    case EuuGGGGGA:
      val = NJetAccuracy<T>::template create<Amp2q5gZ<T> >(Au);
    break;

    case EddssGGGA:
      val = NJetAccuracy<T>::template create<Amp4q3gZ<T> >(Ad);
    break;

    case EdduuGGGA:
      val = NJetAccuracy<T>::template create<Amp4q3gZd<T> >(Ad);
    break;

    case EuuccGGGA:
      val = NJetAccuracy<T>::template create<Amp4q3gZ<T> >(Au);
    break;

    case EddddGGGA:
      val = NJetAccuracy<T>::template create<Amp4q3gZ2<T> >(Ad);
    break;

    case EuuuuGGGA:
      val = NJetAccuracy<T>::template create<Amp4q3gZ2<T> >(Au);
    break;

    case EddssbbGA:
      val = NJetAccuracy<T>::template create<Amp6q1gZ<T> >(Ad);
    break;

    case EuuccttGA:
      val = NJetAccuracy<T>::template create<Amp6q1gZ<T> >(Au);
    break;

    case EddssuuGA:
      val = NJetAccuracy<T>::template create<Amp6q1gZd<T> >(Ad);
    break;

    case EuuccddGA:
      val = NJetAccuracy<T>::template create<Amp6q1gZd<T> >(Au);
    break;

    case EddddssGA:
      val = NJetAccuracy<T>::template create<Amp6q1gZ2<T> >(Ad);
    break;

    case EuuuuccGA:
      val = NJetAccuracy<T>::template create<Amp6q1gZ2<T> >(Au);
    break;

    case EdddduuGA:
      val = NJetAccuracy<T>::template create<Amp6q1gZ2d<T> >(Ad);
    break;

    case EuuuuddGA:
      val = NJetAccuracy<T>::template create<Amp6q1gZ2d<T> >(Au);
    break;

    case EddddddGA:
      val = NJetAccuracy<T>::template create<Amp6q1gZ6<T> >(Ad);
    break;

    case EuuuuuuGA:
      val = NJetAccuracy<T>::template create<Amp6q1gZ6<T> >(Au);
    break;
#endif

    case EddAA:
      val = NJetAccuracy<T>::template create<Amp2q0gAA<T> >(Ad);
    break;

    case EuuAA:
      val = NJetAccuracy<T>::template create<Amp2q0gAA<T> >(Au);
    break;

    case EddGAA:
      val = NJetAccuracy<T>::template create<Amp2q1gAA<T> >(Ad);
    break;

    case EuuGAA:
      val = NJetAccuracy<T>::template create<Amp2q1gAA<T> >(Au);
    break;

    case EddGGAA:
      val = NJetAccuracy<T>::template create<Amp2q2gAA<T> >(Ad);
    break;

    case EuuGGAA:
      val = NJetAccuracy<T>::template create<Amp2q2gAA<T> >(Au);
    break;

    case EddssAA:
      val = NJetAccuracy<T>::template create<Amp4q0gAA<T> >(Ad);
    break;

    case EdduuAA:
      val = NJetAccuracy<T>::template create<Amp4q0gAAd<T> >(Ad);
    break;

    case EuuccAA:
      val = NJetAccuracy<T>::template create<Amp4q0gAA<T> >(Au);
    break;

    case EddddAA:
      val = NJetAccuracy<T>::template create<Amp4q0gAA2<T> >(Ad);
    break;

    case EuuuuAA:
      val = NJetAccuracy<T>::template create<Amp4q0gAA2<T> >(Au);
    break;

    case EddGGGAA:
      val = NJetAccuracy<T>::template create<Amp2q3gAA<T> >(Ad);
    break;

    case EuuGGGAA:
      val = NJetAccuracy<T>::template create<Amp2q3gAA<T> >(Au);
    break;

    case EddssGAA:
      val = NJetAccuracy<T>::template create<Amp4q1gAA<T> >(Ad);
    break;

    case EdduuGAA:
      val = NJetAccuracy<T>::template create<Amp4q1gAAd<T> >(Ad);
    break;

    case EuuccGAA:
      val = NJetAccuracy<T>::template create<Amp4q1gAA<T> >(Au);
    break;

    case EddddGAA:
      val = NJetAccuracy<T>::template create<Amp4q1gAA2<T> >(Ad);
    break;

    case EuuuuGAA:
      val = NJetAccuracy<T>::template create<Amp4q1gAA2<T> >(Au);
    break;

    case EddGGGGAA:
      val = NJetAccuracy<T>::template create<Amp2q4gAA<T> >(Ad);
    break;

    case EuuGGGGAA:
      val = NJetAccuracy<T>::template create<Amp2q4gAA<T> >(Au);
    break;

    case EddssGGAA:
      val = NJetAccuracy<T>::template create<Amp4q2gAA<T> >(Ad);
    break;

    case EdduuGGAA:
      val = NJetAccuracy<T>::template create<Amp4q2gAAd<T> >(Ad);
    break;

    case EuuccGGAA:
      val = NJetAccuracy<T>::template create<Amp4q2gAA<T> >(Au);
    break;

    case EddddGGAA:
      val = NJetAccuracy<T>::template create<Amp4q2gAA2<T> >(Ad);
    break;

    case EuuuuGGAA:
      val = NJetAccuracy<T>::template create<Amp4q2gAA2<T> >(Au);
    break;

    case EddssbbAA:
      val = NJetAccuracy<T>::template create<Amp6q0gAA<T> >(Ad);
    break;

    case EuuccttAA:
      val = NJetAccuracy<T>::template create<Amp6q0gAA<T> >(Au);
    break;

    case EddssuuAA:
      val = NJetAccuracy<T>::template create<Amp6q0gAAd<T> >(Ad);
    break;

    case EuuccddAA:
      val = NJetAccuracy<T>::template create<Amp6q0gAAd<T> >(Au);
    break;

    case EddddssAA:
      val = NJetAccuracy<T>::template create<Amp6q0gAA2<T> >(Ad);
    break;

    case EuuuuccAA:
      val = NJetAccuracy<T>::template create<Amp6q0gAA2<T> >(Au);
    break;

    case EdddduuAA:
      val = NJetAccuracy<T>::template create<Amp6q0gAA2d<T> >(Ad);
    break;

    case EuuuuddAA:
      val = NJetAccuracy<T>::template create<Amp6q0gAA2d<T> >(Au);
    break;

    case EddddddAA:
      val = NJetAccuracy<T>::template create<Amp6q0gAA6<T> >(Ad);
    break;

    case EuuuuuuAA:
      val = NJetAccuracy<T>::template create<Amp6q0gAA6<T> >(Au);
    break;

    case EGGGH:
      val = NJetAccuracy<T>::template create<Amp0q3gH_a<T> >();
    break;

    case EddGH:
      val = NJetAccuracy<T>::template create<Amp2q1gH_a<T> >();
    break;

    case EGGGGH:
      val = NJetAccuracy<T>::template create<Amp0q4gH_a<T> >();
    break;

    case EddGGH:
      val = NJetAccuracy<T>::template create<Amp2q2gH<T> >();
    break;

    case EdduuH:
      val = NJetAccuracy<T>::template create<Amp4q0gH<T> >();
    break;

    case EddddH:
      val = NJetAccuracy<T>::template create<Amp4q0gH2<T> >();
    break;

    case EGGGGGH:
      val = NJetAccuracy<T>::template create<Amp0q5gH_a<T> >();
    break;

    case EddGGGH:
      val = NJetAccuracy<T>::template create<Amp2q3gH<T> >();
    break;

    case EdduuGH:
      val = NJetAccuracy<T>::template create<Amp4q1gH<T> >();
    break;

    case EddddGH:
      val = NJetAccuracy<T>::template create<Amp4q1gH2<T> >();
    break;

    case EGGGGGGH:
      val = NJetAccuracy<T>::template create<Amp0q6gH<T> >();
    break;

    case EddGGGGH:
      val = NJetAccuracy<T>::template create<Amp2q4gH<T> >();
    break;

    case EdduuGGH:
      val = NJetAccuracy<T>::template create<Amp4q2gH<T> >();
    break;

    case EddddGGH:
      val = NJetAccuracy<T>::template create<Amp4q2gH2<T> >();
    break;

    case EdduussH:
      val = NJetAccuracy<T>::template create<Amp6q0gH<T> >();
    break;

    case EuuddddH:
      val = NJetAccuracy<T>::template create<Amp6q0gH2<T> >();
    break;

    case EddddddH:
      val = NJetAccuracy<T>::template create<Amp6q0gH6<T> >();
    break;

    case EGGGGGds3:
      val = NJetAccuracy<T>::template create<Amp0q5g_ds3_a<T> >();
    break;

    case EGGGGGGds4:
      val = NJetAccuracy<T>::template create<Amp0q6g_ds4<T> >();
    break;

#ifdef NJET_ENABLE_5
    case EGGGGGGGds5:
      val = NJetAccuracy<T>::template create<Amp0q7g_ds5<T> >();
    break;
#endif

    case EddGGGds3:
      val = NJetAccuracy<T>::template create<Amp2q3g_ds3_a<T> >();
    break;

    case EddGGGGds3:
      val = NJetAccuracy<T>::template create<Amp2q4g_ds3<T> >();
    break;

    case EddGGGGds4:
      val = NJetAccuracy<T>::template create<Amp2q4g_ds4<T> >();
    break;

#ifdef NJET_ENABLE_5
    case EddGGGGGds3:
      val = NJetAccuracy<T>::template create<Amp2q5g_ds3<T> >();
    break;

    case EddGGGGGds4:
      val = NJetAccuracy<T>::template create<Amp2q5g_ds4<T> >();
    break;

    case EddGGGGGds5:
      val = NJetAccuracy<T>::template create<Amp2q5g_ds5<T> >();
    break;
#endif

    case EudGGGWds3:
      val = NJetAccuracy<T>::template create<Amp2q3gV_ds3<T> >();
    break;

    case EddGGGZds3:
      val = NJetAccuracy<T>::template create<Amp2q3gZ_ds3<T> >(Zd);
    break;

    case EuuGGGZds3:
      val = NJetAccuracy<T>::template create<Amp2q3gZ_ds3<T> >(Zu);
    break;

    case EddGGGAds3:
      val = NJetAccuracy<T>::template create<Amp2q3gA_ds3<T> >(Ad);
    break;

    case EuuGGGAds3:
      val = NJetAccuracy<T>::template create<Amp2q3gA_ds3<T> >(Au);
    break;

    case EudGGGGWds3:
      val = NJetAccuracy<T>::template create<Amp2q4gV_ds3<T> >();
    break;

    case EudGGGGWds4:
      val = NJetAccuracy<T>::template create<Amp2q4gV_ds4<T> >();
    break;

    case EddGGGGZds3:
      val = NJetAccuracy<T>::template create<Amp2q4gZ_ds3<T> >(Zd);
    break;

    case EddGGGGZds4:
      val = NJetAccuracy<T>::template create<Amp2q4gZ_ds4<T> >(Zd);
    break;

    case EuuGGGGZds3:
      val = NJetAccuracy<T>::template create<Amp2q4gZ_ds3<T> >(Zu);
    break;

    case EuuGGGGZds4:
      val = NJetAccuracy<T>::template create<Amp2q4gZ_ds4<T> >(Zu);
    break;

    case EddGGGGAds3:
      val = NJetAccuracy<T>::template create<Amp2q4gA_ds3<T> >(Ad);
    break;

    case EddGGGGAds4:
      val = NJetAccuracy<T>::template create<Amp2q4gA_ds4<T> >(Ad);
    break;

    case EuuGGGGAds3:
      val = NJetAccuracy<T>::template create<Amp2q4gA_ds3<T> >(Au);
    break;

    case EuuGGGGAds4:
      val = NJetAccuracy<T>::template create<Amp2q4gA_ds4<T> >(Au);
    break;

    case EddGGGGAAds3:
      val = NJetAccuracy<T>::template create<Amp2q4gAA_ds3<T> >(Ad);
    break;

    case EddGGGGAAds4:
      val = NJetAccuracy<T>::template create<Amp2q4gAA_ds4<T> >(Ad);
    break;

    case EuuGGGGAAds3:
      val = NJetAccuracy<T>::template create<Amp2q4gAA_ds3<T> >(Au);
    break;

    case EuuGGGGAAds4:
      val = NJetAccuracy<T>::template create<Amp2q4gAA_ds4<T> >(Au);
    break;

#ifdef NJET_ENABLE_5
    case EudGGGGGWds3:
      val = NJetAccuracy<T>::template create<Amp2q5gV_ds3<T> >();
    break;

    case EudGGGGGWds4:
      val = NJetAccuracy<T>::template create<Amp2q5gV_ds4<T> >();
    break;

    case EudGGGGGWds5:
      val = NJetAccuracy<T>::template create<Amp2q5gV_ds5<T> >();
    break;

    case EddGGGGGZds3:
      val = NJetAccuracy<T>::template create<Amp2q5gZ_ds3<T> >(Zd);
    break;

    case EddGGGGGZds4:
      val = NJetAccuracy<T>::template create<Amp2q5gZ_ds4<T> >(Zd);
    break;

    case EddGGGGGZds5:
      val = NJetAccuracy<T>::template create<Amp2q5gZ_ds5<T> >(Zd);
    break;

    case EuuGGGGGZds3:
      val = NJetAccuracy<T>::template create<Amp2q5gZ_ds3<T> >(Zu);
    break;

    case EuuGGGGGZds4:
      val = NJetAccuracy<T>::template create<Amp2q5gZ_ds4<T> >(Zu);
    break;

    case EuuGGGGGZds5:
      val = NJetAccuracy<T>::template create<Amp2q5gZ_ds5<T> >(Zu);
    break;

    case EddGGGGGAds3:
      val = NJetAccuracy<T>::template create<Amp2q5gZ_ds3<T> >(Ad);
    break;

    case EddGGGGGAds4:
      val = NJetAccuracy<T>::template create<Amp2q5gZ_ds4<T> >(Ad);
    break;

    case EddGGGGGAds5:
      val = NJetAccuracy<T>::template create<Amp2q5gZ_ds5<T> >(Ad);
    break;

    case EuuGGGGGAds3:
      val = NJetAccuracy<T>::template create<Amp2q5gZ_ds3<T> >(Au);
    break;

    case EuuGGGGGAds4:
      val = NJetAccuracy<T>::template create<Amp2q5gZ_ds4<T> >(Au);
    break;

    case EuuGGGGGAds5:
      val = NJetAccuracy<T>::template create<Amp2q5gZ_ds5<T> >(Au);
    break;
#endif

    // LC channels
    case EddGGG_lc:
      val = NJetAccuracy<T>::template create<Amp2q3g_a<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EdduuG_lc:
      val = NJetAccuracy<T>::template create<Amp4q1g_a<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddddG_lc:
      val = NJetAccuracy<T>::template create<Amp4q1g2_a<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddGGGG_lc:
      val = NJetAccuracy<T>::template create<Amp2q4g<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EdduuGG_lc:
      val = NJetAccuracy<T>::template create<Amp4q2g<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddddGG_lc:
      val = NJetAccuracy<T>::template create<Amp4q2g2<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case Edduuss_lc:
      val = NJetAccuracy<T>::template create<Amp6q0g<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case Euudddd_lc:
      val = NJetAccuracy<T>::template create<Amp6q0g2<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case Edddddd_lc:
      val = NJetAccuracy<T>::template create<Amp6q0g6<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

#ifdef NJET_ENABLE_5
    case EddGGGGG_lc:
      val = NJetAccuracy<T>::template create<Amp2q5g<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EdduuGGG_lc:
      val = NJetAccuracy<T>::template create<Amp4q3g<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddddGGG_lc:
      val = NJetAccuracy<T>::template create<Amp4q3g2<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EdduussG_lc:
      val = NJetAccuracy<T>::template create<Amp6q1g<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuddddG_lc:
      val = NJetAccuracy<T>::template create<Amp6q1g2<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddddddG_lc:
      val = NJetAccuracy<T>::template create<Amp6q1g6<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;
#endif

    case EudGGGW_lc:
      val = NJetAccuracy<T>::template create<Amp2q3gV<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EudssGW_lc:
    case EudccGW_lc:
      val = NJetAccuracy<T>::template create<Amp4q1gV<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EudddGW_lc:
      val = NJetAccuracy<T>::template create<Amp4q1gV2<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuduuGW_lc:
      val = NJetAccuracy<T>::template create<Amp4q1gV2b<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EudGGGGW_lc:
      val = NJetAccuracy<T>::template create<Amp2q4gV<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EudssGGW_lc:
    case EudccGGW_lc:
      val = NJetAccuracy<T>::template create<Amp4q2gV<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EudddGGW_lc:
      val = NJetAccuracy<T>::template create<Amp4q2gV2<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuduuGGW_lc:
      val = NJetAccuracy<T>::template create<Amp4q2gV2b<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EudssccW_lc:
    case EudssbbW_lc:
    case EudccttW_lc:
      val = NJetAccuracy<T>::template create<Amp6q0gV<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EudssssW_lc:
    case EudccccW_lc:
      val = NJetAccuracy<T>::template create<Amp6q0gV2<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EudddssW_lc:
    case EudddccW_lc:
      val = NJetAccuracy<T>::template create<Amp6q0gV2n<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuduussW_lc:
    case EuduuccW_lc:
      val = NJetAccuracy<T>::template create<Amp6q0gV2b<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EudddddW_lc:
      val = NJetAccuracy<T>::template create<Amp6q0gV6n<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuduuuuW_lc:
      val = NJetAccuracy<T>::template create<Amp6q0gV6b<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuddduuW_lc:
      val = NJetAccuracy<T>::template create<Amp6q0gV4nb<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

#ifdef NJET_ENABLE_5
    case EudGGGGGW_lc:
      val = NJetAccuracy<T>::template create<Amp2q5gV<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EudssGGGW_lc:
    case EudccGGGW_lc:
      val = NJetAccuracy<T>::template create<Amp4q3gV<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EudddGGGW_lc:
      val = NJetAccuracy<T>::template create<Amp4q3gV2<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuduuGGGW_lc:
      val = NJetAccuracy<T>::template create<Amp4q3gV2b<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EudssccGW_lc:
    case EudssbbGW_lc:
    case EudccttGW_lc:
      val = NJetAccuracy<T>::template create<Amp6q1gV<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EudssssGW_lc:
    case EudccccGW_lc:
      val = NJetAccuracy<T>::template create<Amp6q1gV2<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EudddssGW_lc:
    case EudddccGW_lc:
      val = NJetAccuracy<T>::template create<Amp6q1gV2n<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuduussGW_lc:
    case EuduuccGW_lc:
      val = NJetAccuracy<T>::template create<Amp6q1gV2b<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EudddddGW_lc:
      val = NJetAccuracy<T>::template create<Amp6q1gV6n<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuduuuuGW_lc:
      val = NJetAccuracy<T>::template create<Amp6q1gV6b<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuddduuGW_lc:
      val = NJetAccuracy<T>::template create<Amp6q1gV4nb<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;
#endif

    case EddGGGZ_lc:
      val = NJetAccuracy<T>::template create<Amp2q3gZ<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuGGGZ_lc:
      val = NJetAccuracy<T>::template create<Amp2q3gZ<T> >(Zu);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddssGZ_lc:
      val = NJetAccuracy<T>::template create<Amp4q1gZ<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EdduuGZ_lc:
      val = NJetAccuracy<T>::template create<Amp4q1gZd<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuccGZ_lc:
      val = NJetAccuracy<T>::template create<Amp4q1gZ<T> >(Zu);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddddGZ_lc:
      val = NJetAccuracy<T>::template create<Amp4q1gZ2<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuuuGZ_lc:
      val = NJetAccuracy<T>::template create<Amp4q1gZ2<T> >(Zu);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddGGGGZ_lc:
      val = NJetAccuracy<T>::template create<Amp2q4gZ<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuGGGGZ_lc:
      val = NJetAccuracy<T>::template create<Amp2q4gZ<T> >(Zu);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddssGGZ_lc:
      val = NJetAccuracy<T>::template create<Amp4q2gZ<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EdduuGGZ_lc:
      val = NJetAccuracy<T>::template create<Amp4q2gZd<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuccGGZ_lc:
      val = NJetAccuracy<T>::template create<Amp4q2gZ<T> >(Zu);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddddGGZ_lc:
      val = NJetAccuracy<T>::template create<Amp4q2gZ2<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuuuGGZ_lc:
      val = NJetAccuracy<T>::template create<Amp4q2gZ2<T> >(Zu);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddssbbZ_lc:
      val = NJetAccuracy<T>::template create<Amp6q0gZ<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuccttZ_lc:
      val = NJetAccuracy<T>::template create<Amp6q0gZ<T> >(Zu);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddssuuZ_lc:
      val = NJetAccuracy<T>::template create<Amp6q0gZd<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuccddZ_lc:
      val = NJetAccuracy<T>::template create<Amp6q0gZd<T> >(Zu);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddddssZ_lc:
      val = NJetAccuracy<T>::template create<Amp6q0gZ2<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuuuccZ_lc:
      val = NJetAccuracy<T>::template create<Amp6q0gZ2<T> >(Zu);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EdddduuZ_lc:
      val = NJetAccuracy<T>::template create<Amp6q0gZ2d<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuuuddZ_lc:
      val = NJetAccuracy<T>::template create<Amp6q0gZ2d<T> >(Zu);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddddddZ_lc:
      val = NJetAccuracy<T>::template create<Amp6q0gZ6<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuuuuuZ_lc:
      val = NJetAccuracy<T>::template create<Amp6q0gZ6<T> >(Zu);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

#ifdef NJET_ENABLE_5
    case EddGGGGGZ_lc:
      val = NJetAccuracy<T>::template create<Amp2q5gZ<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuGGGGGZ_lc:
      val = NJetAccuracy<T>::template create<Amp2q5gZ<T> >(Zu);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddssGGGZ_lc:
      val = NJetAccuracy<T>::template create<Amp4q3gZ<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EdduuGGGZ_lc:
      val = NJetAccuracy<T>::template create<Amp4q3gZd<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuccGGGZ_lc:
      val = NJetAccuracy<T>::template create<Amp4q3gZ<T> >(Zu);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddddGGGZ_lc:
      val = NJetAccuracy<T>::template create<Amp4q3gZ2<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuuuGGGZ_lc:
      val = NJetAccuracy<T>::template create<Amp4q3gZ2<T> >(Zu);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddssbbGZ_lc:
      val = NJetAccuracy<T>::template create<Amp6q1gZ<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuccttGZ_lc:
      val = NJetAccuracy<T>::template create<Amp6q1gZ<T> >(Zu);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddssuuGZ_lc:
      val = NJetAccuracy<T>::template create<Amp6q1gZd<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuccddGZ_lc:
      val = NJetAccuracy<T>::template create<Amp6q1gZd<T> >(Zu);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddddssGZ_lc:
      val = NJetAccuracy<T>::template create<Amp6q1gZ2<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuuuccGZ_lc:
      val = NJetAccuracy<T>::template create<Amp6q1gZ2<T> >(Zu);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EdddduuGZ_lc:
      val = NJetAccuracy<T>::template create<Amp6q1gZ2d<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuuuddGZ_lc:
      val = NJetAccuracy<T>::template create<Amp6q1gZ2d<T> >(Zu);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddddddGZ_lc:
      val = NJetAccuracy<T>::template create<Amp6q1gZ6<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuuuuuGZ_lc:
      val = NJetAccuracy<T>::template create<Amp6q1gZ6<T> >(Zu);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;
#endif

    case EddGGGA_lc:
      val = NJetAccuracy<T>::template create<Amp2q3gA<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuGGGA_lc:
      val = NJetAccuracy<T>::template create<Amp2q3gA<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddssGA_lc:
      val = NJetAccuracy<T>::template create<Amp4q1gA<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EdduuGA_lc:
      val = NJetAccuracy<T>::template create<Amp4q1gAd<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuccGA_lc:
      val = NJetAccuracy<T>::template create<Amp4q1gA<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddddGA_lc:
      val = NJetAccuracy<T>::template create<Amp4q1gA2<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuuuGA_lc:
      val = NJetAccuracy<T>::template create<Amp4q1gA2<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddGGGGA_lc:
      val = NJetAccuracy<T>::template create<Amp2q4gA<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuGGGGA_lc:
      val = NJetAccuracy<T>::template create<Amp2q4gA<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddssGGA_lc:
      val = NJetAccuracy<T>::template create<Amp4q2gA<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EdduuGGA_lc:
      val = NJetAccuracy<T>::template create<Amp4q2gAd<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuccGGA_lc:
      val = NJetAccuracy<T>::template create<Amp4q2gA<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddddGGA_lc:
      val = NJetAccuracy<T>::template create<Amp4q2gA2<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuuuGGA_lc:
      val = NJetAccuracy<T>::template create<Amp4q2gA2<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddssbbA_lc:
      val = NJetAccuracy<T>::template create<Amp6q0gA<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuccttA_lc:
      val = NJetAccuracy<T>::template create<Amp6q0gA<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddssuuA_lc:
      val = NJetAccuracy<T>::template create<Amp6q0gAd<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuccddA_lc:
      val = NJetAccuracy<T>::template create<Amp6q0gAd<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddddssA_lc:
      val = NJetAccuracy<T>::template create<Amp6q0gA2<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuuuccA_lc:
      val = NJetAccuracy<T>::template create<Amp6q0gA2<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EdddduuA_lc:
      val = NJetAccuracy<T>::template create<Amp6q0gA2d<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuuuddA_lc:
      val = NJetAccuracy<T>::template create<Amp6q0gA2d<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddddddA_lc:
      val = NJetAccuracy<T>::template create<Amp6q0gA6<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuuuuuA_lc:
      val = NJetAccuracy<T>::template create<Amp6q0gA6<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

#ifdef NJET_ENABLE_5
    case EddGGGGGA_lc:
      val = NJetAccuracy<T>::template create<Amp2q5gZ<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuGGGGGA_lc:
      val = NJetAccuracy<T>::template create<Amp2q5gZ<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddssGGGA_lc:
      val = NJetAccuracy<T>::template create<Amp4q3gZ<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EdduuGGGA_lc:
      val = NJetAccuracy<T>::template create<Amp4q3gZd<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuccGGGA_lc:
      val = NJetAccuracy<T>::template create<Amp4q3gZ<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddddGGGA_lc:
      val = NJetAccuracy<T>::template create<Amp4q3gZ2<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuuuGGGA_lc:
      val = NJetAccuracy<T>::template create<Amp4q3gZ2<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddssbbGA_lc:
      val = NJetAccuracy<T>::template create<Amp6q1gZ<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuccttGA_lc:
      val = NJetAccuracy<T>::template create<Amp6q1gZ<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddssuuGA_lc:
      val = NJetAccuracy<T>::template create<Amp6q1gZd<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuccddGA_lc:
      val = NJetAccuracy<T>::template create<Amp6q1gZd<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddddssGA_lc:
      val = NJetAccuracy<T>::template create<Amp6q1gZ2<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuuuccGA_lc:
      val = NJetAccuracy<T>::template create<Amp6q1gZ2<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EdddduuGA_lc:
      val = NJetAccuracy<T>::template create<Amp6q1gZ2d<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuuuddGA_lc:
      val = NJetAccuracy<T>::template create<Amp6q1gZ2d<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddddddGA_lc:
      val = NJetAccuracy<T>::template create<Amp6q1gZ6<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuuuuuGA_lc:
      val = NJetAccuracy<T>::template create<Amp6q1gZ6<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;
#endif

    case EddGGGAA_lc:
      val = NJetAccuracy<T>::template create<Amp2q3gAA<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuGGGAA_lc:
      val = NJetAccuracy<T>::template create<Amp2q3gAA<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddssGAA_lc:
      val = NJetAccuracy<T>::template create<Amp4q1gAA<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EdduuGAA_lc:
      val = NJetAccuracy<T>::template create<Amp4q1gAAd<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuccGAA_lc:
      val = NJetAccuracy<T>::template create<Amp4q1gAA<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddddGAA_lc:
      val = NJetAccuracy<T>::template create<Amp4q1gAA2<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuuuGAA_lc:
      val = NJetAccuracy<T>::template create<Amp4q1gAA2<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddGGGGAA_lc:
      val = NJetAccuracy<T>::template create<Amp2q4gAA<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuGGGGAA_lc:
      val = NJetAccuracy<T>::template create<Amp2q4gAA<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddssGGAA_lc:
      val = NJetAccuracy<T>::template create<Amp4q2gAA<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EdduuGGAA_lc:
      val = NJetAccuracy<T>::template create<Amp4q2gAAd<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuccGGAA_lc:
      val = NJetAccuracy<T>::template create<Amp4q2gAA<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddddGGAA_lc:
      val = NJetAccuracy<T>::template create<Amp4q2gAA2<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuuuGGAA_lc:
      val = NJetAccuracy<T>::template create<Amp4q2gAA2<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddssbbAA_lc:
      val = NJetAccuracy<T>::template create<Amp6q0gAA<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuccttAA_lc:
      val = NJetAccuracy<T>::template create<Amp6q0gAA<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddssuuAA_lc:
      val = NJetAccuracy<T>::template create<Amp6q0gAAd<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuccddAA_lc:
      val = NJetAccuracy<T>::template create<Amp6q0gAAd<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddddssAA_lc:
      val = NJetAccuracy<T>::template create<Amp6q0gAA2<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuuuccAA_lc:
      val = NJetAccuracy<T>::template create<Amp6q0gAA2<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EdddduuAA_lc:
      val = NJetAccuracy<T>::template create<Amp6q0gAA2d<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuuuddAA_lc:
      val = NJetAccuracy<T>::template create<Amp6q0gAA2d<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EddddddAA_lc:
      val = NJetAccuracy<T>::template create<Amp6q0gAA6<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    case EuuuuuuAA_lc:
      val = NJetAccuracy<T>::template create<Amp6q0gAA6<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_LC, norderL, norderF);
    break;

    // SLC channels
    case EddGGG_slc:
      val = NJetAccuracy<T>::template create<Amp2q3g_a<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EdduuG_slc:
      val = NJetAccuracy<T>::template create<Amp4q1g_a<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddddG_slc:
      val = NJetAccuracy<T>::template create<Amp4q1g2_a<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddGGGG_slc:
      val = NJetAccuracy<T>::template create<Amp2q4g<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EdduuGG_slc:
      val = NJetAccuracy<T>::template create<Amp4q2g<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddddGG_slc:
      val = NJetAccuracy<T>::template create<Amp4q2g2<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case Edduuss_slc:
      val = NJetAccuracy<T>::template create<Amp6q0g<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case Euudddd_slc:
      val = NJetAccuracy<T>::template create<Amp6q0g2<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case Edddddd_slc:
      val = NJetAccuracy<T>::template create<Amp6q0g6<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

#ifdef NJET_ENABLE_5
    case EddGGGGG_slc:
      val = NJetAccuracy<T>::template create<Amp2q5g<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EdduuGGG_slc:
      val = NJetAccuracy<T>::template create<Amp4q3g<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddddGGG_slc:
      val = NJetAccuracy<T>::template create<Amp4q3g2<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EdduussG_slc:
      val = NJetAccuracy<T>::template create<Amp6q1g<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuddddG_slc:
      val = NJetAccuracy<T>::template create<Amp6q1g2<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddddddG_slc:
      val = NJetAccuracy<T>::template create<Amp6q1g6<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;
#endif

    case EudGGGW_slc:
      val = NJetAccuracy<T>::template create<Amp2q3gV<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EudssGW_slc:
    case EudccGW_slc:
      val = NJetAccuracy<T>::template create<Amp4q1gV<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EudddGW_slc:
      val = NJetAccuracy<T>::template create<Amp4q1gV2<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuduuGW_slc:
      val = NJetAccuracy<T>::template create<Amp4q1gV2b<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EudGGGGW_slc:
      val = NJetAccuracy<T>::template create<Amp2q4gV<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EudssGGW_slc:
    case EudccGGW_slc:
      val = NJetAccuracy<T>::template create<Amp4q2gV<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EudddGGW_slc:
      val = NJetAccuracy<T>::template create<Amp4q2gV2<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuduuGGW_slc:
      val = NJetAccuracy<T>::template create<Amp4q2gV2b<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EudssccW_slc:
    case EudssbbW_slc:
    case EudccttW_slc:
      val = NJetAccuracy<T>::template create<Amp6q0gV<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EudssssW_slc:
    case EudccccW_slc:
      val = NJetAccuracy<T>::template create<Amp6q0gV2<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EudddssW_slc:
    case EudddccW_slc:
      val = NJetAccuracy<T>::template create<Amp6q0gV2n<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuduussW_slc:
    case EuduuccW_slc:
      val = NJetAccuracy<T>::template create<Amp6q0gV2b<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EudddddW_slc:
      val = NJetAccuracy<T>::template create<Amp6q0gV6n<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuduuuuW_slc:
      val = NJetAccuracy<T>::template create<Amp6q0gV6b<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuddduuW_slc:
      val = NJetAccuracy<T>::template create<Amp6q0gV4nb<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

#ifdef NJET_ENABLE_5
    case EudGGGGGW_slc:
      val = NJetAccuracy<T>::template create<Amp2q5gV<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EudssGGGW_slc:
    case EudccGGGW_slc:
      val = NJetAccuracy<T>::template create<Amp4q3gV<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EudddGGGW_slc:
      val = NJetAccuracy<T>::template create<Amp4q3gV2<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuduuGGGW_slc:
      val = NJetAccuracy<T>::template create<Amp4q3gV2b<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EudssccGW_slc:
    case EudssbbGW_slc:
    case EudccttGW_slc:
      val = NJetAccuracy<T>::template create<Amp6q1gV<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EudssssGW_slc:
    case EudccccGW_slc:
      val = NJetAccuracy<T>::template create<Amp6q1gV2<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EudddssGW_slc:
    case EudddccGW_slc:
      val = NJetAccuracy<T>::template create<Amp6q1gV2n<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuduussGW_slc:
    case EuduuccGW_slc:
      val = NJetAccuracy<T>::template create<Amp6q1gV2b<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EudddddGW_slc:
      val = NJetAccuracy<T>::template create<Amp6q1gV6n<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuduuuuGW_slc:
      val = NJetAccuracy<T>::template create<Amp6q1gV6b<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuddduuGW_slc:
      val = NJetAccuracy<T>::template create<Amp6q1gV4nb<T> >();
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;
#endif

    case EddGGGZ_slc:
      val = NJetAccuracy<T>::template create<Amp2q3gZ<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuGGGZ_slc:
      val = NJetAccuracy<T>::template create<Amp2q3gZ<T> >(Zu);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddssGZ_slc:
      val = NJetAccuracy<T>::template create<Amp4q1gZ<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EdduuGZ_slc:
      val = NJetAccuracy<T>::template create<Amp4q1gZd<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuccGZ_slc:
      val = NJetAccuracy<T>::template create<Amp4q1gZ<T> >(Zu);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddddGZ_slc:
      val = NJetAccuracy<T>::template create<Amp4q1gZ2<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuuuGZ_slc:
      val = NJetAccuracy<T>::template create<Amp4q1gZ2<T> >(Zu);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddGGGGZ_slc:
      val = NJetAccuracy<T>::template create<Amp2q4gZ<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuGGGGZ_slc:
      val = NJetAccuracy<T>::template create<Amp2q4gZ<T> >(Zu);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddssGGZ_slc:
      val = NJetAccuracy<T>::template create<Amp4q2gZ<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EdduuGGZ_slc:
      val = NJetAccuracy<T>::template create<Amp4q2gZd<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuccGGZ_slc:
      val = NJetAccuracy<T>::template create<Amp4q2gZ<T> >(Zu);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddddGGZ_slc:
      val = NJetAccuracy<T>::template create<Amp4q2gZ2<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuuuGGZ_slc:
      val = NJetAccuracy<T>::template create<Amp4q2gZ2<T> >(Zu);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddssbbZ_slc:
      val = NJetAccuracy<T>::template create<Amp6q0gZ<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuccttZ_slc:
      val = NJetAccuracy<T>::template create<Amp6q0gZ<T> >(Zu);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddssuuZ_slc:
      val = NJetAccuracy<T>::template create<Amp6q0gZd<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuccddZ_slc:
      val = NJetAccuracy<T>::template create<Amp6q0gZd<T> >(Zu);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddddssZ_slc:
      val = NJetAccuracy<T>::template create<Amp6q0gZ2<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuuuccZ_slc:
      val = NJetAccuracy<T>::template create<Amp6q0gZ2<T> >(Zu);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EdddduuZ_slc:
      val = NJetAccuracy<T>::template create<Amp6q0gZ2d<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuuuddZ_slc:
      val = NJetAccuracy<T>::template create<Amp6q0gZ2d<T> >(Zu);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddddddZ_slc:
      val = NJetAccuracy<T>::template create<Amp6q0gZ6<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuuuuuZ_slc:
      val = NJetAccuracy<T>::template create<Amp6q0gZ6<T> >(Zu);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

#ifdef NJET_ENABLE_5
    case EddGGGGGZ_slc:
      val = NJetAccuracy<T>::template create<Amp2q5gZ<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuGGGGGZ_slc:
      val = NJetAccuracy<T>::template create<Amp2q5gZ<T> >(Zu);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddssGGGZ_slc:
      val = NJetAccuracy<T>::template create<Amp4q3gZ<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EdduuGGGZ_slc:
      val = NJetAccuracy<T>::template create<Amp4q3gZd<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuccGGGZ_slc:
      val = NJetAccuracy<T>::template create<Amp4q3gZ<T> >(Zu);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddddGGGZ_slc:
      val = NJetAccuracy<T>::template create<Amp4q3gZ2<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuuuGGGZ_slc:
      val = NJetAccuracy<T>::template create<Amp4q3gZ2<T> >(Zu);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddssbbGZ_slc:
      val = NJetAccuracy<T>::template create<Amp6q1gZ<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuccttGZ_slc:
      val = NJetAccuracy<T>::template create<Amp6q1gZ<T> >(Zu);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddssuuGZ_slc:
      val = NJetAccuracy<T>::template create<Amp6q1gZd<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuccddGZ_slc:
      val = NJetAccuracy<T>::template create<Amp6q1gZd<T> >(Zu);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddddssGZ_slc:
      val = NJetAccuracy<T>::template create<Amp6q1gZ2<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuuuccGZ_slc:
      val = NJetAccuracy<T>::template create<Amp6q1gZ2<T> >(Zu);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EdddduuGZ_slc:
      val = NJetAccuracy<T>::template create<Amp6q1gZ2d<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuuuddGZ_slc:
      val = NJetAccuracy<T>::template create<Amp6q1gZ2d<T> >(Zu);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddddddGZ_slc:
      val = NJetAccuracy<T>::template create<Amp6q1gZ6<T> >(Zd);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuuuuuGZ_slc:
      val = NJetAccuracy<T>::template create<Amp6q1gZ6<T> >(Zu);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;
#endif

    case EddGGGA_slc:
      val = NJetAccuracy<T>::template create<Amp2q3gA<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuGGGA_slc:
      val = NJetAccuracy<T>::template create<Amp2q3gA<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddssGA_slc:
      val = NJetAccuracy<T>::template create<Amp4q1gA<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EdduuGA_slc:
      val = NJetAccuracy<T>::template create<Amp4q1gAd<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuccGA_slc:
      val = NJetAccuracy<T>::template create<Amp4q1gA<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddddGA_slc:
      val = NJetAccuracy<T>::template create<Amp4q1gA2<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuuuGA_slc:
      val = NJetAccuracy<T>::template create<Amp4q1gA2<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddGGGGA_slc:
      val = NJetAccuracy<T>::template create<Amp2q4gA<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuGGGGA_slc:
      val = NJetAccuracy<T>::template create<Amp2q4gA<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddssGGA_slc:
      val = NJetAccuracy<T>::template create<Amp4q2gA<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EdduuGGA_slc:
      val = NJetAccuracy<T>::template create<Amp4q2gAd<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuccGGA_slc:
      val = NJetAccuracy<T>::template create<Amp4q2gA<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddddGGA_slc:
      val = NJetAccuracy<T>::template create<Amp4q2gA2<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuuuGGA_slc:
      val = NJetAccuracy<T>::template create<Amp4q2gA2<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddssbbA_slc:
      val = NJetAccuracy<T>::template create<Amp6q0gA<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuccttA_slc:
      val = NJetAccuracy<T>::template create<Amp6q0gA<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddssuuA_slc:
      val = NJetAccuracy<T>::template create<Amp6q0gAd<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuccddA_slc:
      val = NJetAccuracy<T>::template create<Amp6q0gAd<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddddssA_slc:
      val = NJetAccuracy<T>::template create<Amp6q0gA2<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuuuccA_slc:
      val = NJetAccuracy<T>::template create<Amp6q0gA2<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EdddduuA_slc:
      val = NJetAccuracy<T>::template create<Amp6q0gA2d<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuuuddA_slc:
      val = NJetAccuracy<T>::template create<Amp6q0gA2d<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddddddA_slc:
      val = NJetAccuracy<T>::template create<Amp6q0gA6<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuuuuuA_slc:
      val = NJetAccuracy<T>::template create<Amp6q0gA6<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

#ifdef NJET_ENABLE_5
    case EddGGGGGA_slc:
      val = NJetAccuracy<T>::template create<Amp2q5gZ<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuGGGGGA_slc:
      val = NJetAccuracy<T>::template create<Amp2q5gZ<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddssGGGA_slc:
      val = NJetAccuracy<T>::template create<Amp4q3gZ<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EdduuGGGA_slc:
      val = NJetAccuracy<T>::template create<Amp4q3gZd<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuccGGGA_slc:
      val = NJetAccuracy<T>::template create<Amp4q3gZ<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddddGGGA_slc:
      val = NJetAccuracy<T>::template create<Amp4q3gZ2<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuuuGGGA_slc:
      val = NJetAccuracy<T>::template create<Amp4q3gZ2<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddssbbGA_slc:
      val = NJetAccuracy<T>::template create<Amp6q1gZ<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuccttGA_slc:
      val = NJetAccuracy<T>::template create<Amp6q1gZ<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddssuuGA_slc:
      val = NJetAccuracy<T>::template create<Amp6q1gZd<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuccddGA_slc:
      val = NJetAccuracy<T>::template create<Amp6q1gZd<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddddssGA_slc:
      val = NJetAccuracy<T>::template create<Amp6q1gZ2<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuuuccGA_slc:
      val = NJetAccuracy<T>::template create<Amp6q1gZ2<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EdddduuGA_slc:
      val = NJetAccuracy<T>::template create<Amp6q1gZ2d<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuuuddGA_slc:
      val = NJetAccuracy<T>::template create<Amp6q1gZ2d<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddddddGA_slc:
      val = NJetAccuracy<T>::template create<Amp6q1gZ6<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuuuuuGA_slc:
      val = NJetAccuracy<T>::template create<Amp6q1gZ6<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;
#endif

    case EddGGGAA_slc:
      val = NJetAccuracy<T>::template create<Amp2q3gAA<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuGGGAA_slc:
      val = NJetAccuracy<T>::template create<Amp2q3gAA<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddssGAA_slc:
      val = NJetAccuracy<T>::template create<Amp4q1gAA<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EdduuGAA_slc:
      val = NJetAccuracy<T>::template create<Amp4q1gAAd<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuccGAA_slc:
      val = NJetAccuracy<T>::template create<Amp4q1gAA<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddddGAA_slc:
      val = NJetAccuracy<T>::template create<Amp4q1gAA2<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuuuGAA_slc:
      val = NJetAccuracy<T>::template create<Amp4q1gAA2<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddGGGGAA_slc:
      val = NJetAccuracy<T>::template create<Amp2q4gAA<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuGGGGAA_slc:
      val = NJetAccuracy<T>::template create<Amp2q4gAA<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddssGGAA_slc:
      val = NJetAccuracy<T>::template create<Amp4q2gAA<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EdduuGGAA_slc:
      val = NJetAccuracy<T>::template create<Amp4q2gAAd<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuccGGAA_slc:
      val = NJetAccuracy<T>::template create<Amp4q2gAA<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddddGGAA_slc:
      val = NJetAccuracy<T>::template create<Amp4q2gAA2<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuuuGGAA_slc:
      val = NJetAccuracy<T>::template create<Amp4q2gAA2<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddssbbAA_slc:
      val = NJetAccuracy<T>::template create<Amp6q0gAA<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuccttAA_slc:
      val = NJetAccuracy<T>::template create<Amp6q0gAA<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddssuuAA_slc:
      val = NJetAccuracy<T>::template create<Amp6q0gAAd<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuccddAA_slc:
      val = NJetAccuracy<T>::template create<Amp6q0gAAd<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddddssAA_slc:
      val = NJetAccuracy<T>::template create<Amp6q0gAA2<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuuuccAA_slc:
      val = NJetAccuracy<T>::template create<Amp6q0gAA2<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EdddduuAA_slc:
      val = NJetAccuracy<T>::template create<Amp6q0gAA2d<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuuuddAA_slc:
      val = NJetAccuracy<T>::template create<Amp6q0gAA2d<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddddddAA_slc:
      val = NJetAccuracy<T>::template create<Amp6q0gAA6<T> >(Ad);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EuuuuuuAA_slc:
      val = NJetAccuracy<T>::template create<Amp6q0gAA6<T> >(Au);
      valid &= val->setLoopType(NJetAmpTables::COLOR_SLC, norderL, norderF);
    break;

    case EddGGA_ax:
      val = NJetAccuracy<T>::template create<Amp2q2gAx<T> >(Ad);
    break;

    case EuuGGA_ax:
      val = NJetAccuracy<T>::template create<Amp2q2gAx<T> >(Au);
    break;

    case EddssA_ax:
      val = NJetAccuracy<T>::template create<Amp4q0gAx<T> >(Ad);
    break;

    case EdduuA_ax:
      val = NJetAccuracy<T>::template create<Amp4q0gAxd<T> >(Ad);
    break;

    case EuuccA_ax:
      val = NJetAccuracy<T>::template create<Amp4q0gAx<T> >(Au);
    break;

    case EddddA_ax:
      val = NJetAccuracy<T>::template create<Amp4q0gAx2<T> >(Ad);
    break;

    case EuuuuA_ax:
      val = NJetAccuracy<T>::template create<Amp4q0gAx2<T> >(Au);
    break;

    case EddGGGA_ax:
      val = NJetAccuracy<T>::template create<Amp2q3gAx<T> >(Ad);
    break;

    case EuuGGGA_ax:
      val = NJetAccuracy<T>::template create<Amp2q3gAx<T> >(Au);
    break;

    case EddssGA_ax:
      val = NJetAccuracy<T>::template create<Amp4q1gAx<T> >(Ad);
    break;

    case EdduuGA_ax:
      val = NJetAccuracy<T>::template create<Amp4q1gAxd<T> >(Ad);
    break;

    case EuuccGA_ax:
      val = NJetAccuracy<T>::template create<Amp4q1gAx<T> >(Au);
    break;

    case EddddGA_ax:
      val = NJetAccuracy<T>::template create<Amp4q1gAx2<T> >(Ad);
    break;

    case EuuuuGA_ax:
      val = NJetAccuracy<T>::template create<Amp4q1gAx2<T> >(Au);
    break;

    case EddGAA_ax:
      val = NJetAccuracy<T>::template create<Amp2q1gAAx<T> >(Ad);
    break;

    case EuuGAA_ax:
      val = NJetAccuracy<T>::template create<Amp2q1gAAx<T> >(Au);
    break;

    case EddGGAA_ax:
      val = NJetAccuracy<T>::template create<Amp2q2gAAx<T> >(Ad);
    break;

    case EuuGGAA_ax:
      val = NJetAccuracy<T>::template create<Amp2q2gAAx<T> >(Au);
    break;

    case EddssAA_ax:
      val = NJetAccuracy<T>::template create<Amp4q0gAAx<T> >(Ad);
    break;

    case EdduuAA_ax:
      val = NJetAccuracy<T>::template create<Amp4q0gAAxd<T> >(Ad);
    break;

    case EuuccAA_ax:
      val = NJetAccuracy<T>::template create<Amp4q0gAAx<T> >(Au);
    break;

    case EddddAA_ax:
      val = NJetAccuracy<T>::template create<Amp4q0gAAx2<T> >(Ad);
    break;

    case EuuuuAA_ax:
      val = NJetAccuracy<T>::template create<Amp4q0gAAx2<T> >(Au);
    break;
  }

  if (val) {
    val->setNf(Nf);
    val->setNc(Nc);
    val->setScheme(scheme);
    val->setRenorm(renorm);
  }

  if (not valid or not val) {
    std::cout << "NJet Warning: broken process idx = " << idx << std::endl;
#ifndef NJET_ENABLE_5
    std::cout << "Did you forget to compile 5-jet amplitudes?" << idx << std::endl;
#endif
  }

  return val;
}

#ifdef USE_SD
  template class NJetInterface<double>;
#endif
#ifdef USE_DD
  template class NJetInterface<dd_real>;
#endif
#ifdef USE_QD
  template class NJetInterface<qd_real>;
#endif
#ifdef USE_VC
  template class NJetInterface<Vc::double_v>;
#endif
