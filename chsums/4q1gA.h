/*
* chsums/4q1gA.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012, 2013 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef CHSUM_4Q1GA_H
#define CHSUM_4Q1GA_H

#include "4q1gV.h"

template <typename T>
class Amp4q1gA : public Amp4q1gZ<T>
{
    typedef Amp4q1gZ<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp4q1gA(const Flavour<double>& ff, const T scalefactor,
             const int mFC=2, const NJetAmpTables& tables=BaseClass::amptables())
    : BaseClass(ff, scalefactor, mFC, tables)
    { }

    void born_part_fill() { BaseClass::born_part_trickfill(); }
    LoopValue virt() { return BaseClass::virt_tricksum(); }
};

template <typename T>
class Amp4q1gAd : public Amp4q1gZd<T>
{
    typedef Amp4q1gZd<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp4q1gAd(const Flavour<double>& ff, const T scalefactor,
              const int mFC=2, const NJetAmpTables& tables=BaseClass::amptables())
    : BaseClass(ff, scalefactor, mFC, tables)
    { }

    void born_part_fill() { BaseClass::born_part_trickfill(); }
    LoopValue virt() { return BaseClass::virt_tricksum(); }
};

template <typename T>
class Amp4q1gA2 : public Amp4q1gZ2<T>
{
    typedef Amp4q1gZ2<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp4q1gA2(const Flavour<double>& ff, const T scalefactor,
              const int mFC=4, const NJetAmpTables& tables=BaseClass::amptables())
    : BaseClass(ff, scalefactor, mFC, tables)
    { }

    void born_part_fill() { BaseClass::born_part_trickfill(); }
    LoopValue virt() { return BaseClass::virt_tricksum(); }
};

// One photon vector loop

class Amp4q1gAxStatic : public Amp4q1gZStatic
{
  public:
    static const int FC = 6;

    static const int flav[FC][NN];  // flavour vectors for ngluon
    static const int fvsign[FC];    // flavour channels' signs
    static const int fperm[FC][NN];        // flavour pemutation matrix
    static const int fvcol[FC][CC];        // flavour-partial pemutation matrix
};

template <typename T>
class Amp4q1gAx : public Amp4q1gA<T>
{
    typedef Amp4q1gA<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;
    typedef typename BaseClass::ST ST;

    Amp4q1gAx(const Flavour<double>& ff, const T scalefactor,
              const int mFC=3, const NJetAmpTables& tables=amptables());

    void setNf(const ST Nf_);

  protected:
    using BaseClass::NN;
    using BaseClass::Nc;
    using BaseClass::Nf;
    using BaseClass::vhel;
    using BaseClass::getFlav;
    using BaseClass::getFperm;
    using BaseClass::fvSet;
    using BaseClass::mfv;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp4q1gAxStatic>();
    }

    LoopResult<T> AFx(int p0, int p1, int p2, int p3, int p4);

    void getfvpart1_full(const int fv, LoopValue* fvpart);
    void getfvpart1_full(const int fv, LoopResult<T>* fvpart);

    template <typename LT>
    void getfvpart1_full_(const int fv, LT* fvpart);
};

template <typename T>
class Amp4q1gAxd : public Amp4q1gAx<T>
{
    typedef Amp4q1gAx<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp4q1gAxd(const Flavour<double>& ff, const T scalefactor,
               const int mFC=3, const NJetAmpTables& tables=BaseClass::amptables());

  protected:
    using BaseClass::NN;
    using BaseClass::getFlav;
};

class Amp4q1gAx2Static : public Amp4q1gAxStatic
{
  public:
    static const int HS = 24;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp4q1gAx2 : public Amp4q1gAx<T>
{
    typedef Amp4q1gAx<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp4q1gAx2(const Flavour<double>& ff, const T scalefactor,
               const int mFC=6, const NJetAmpTables& tables=amptables())
    : BaseClass(ff, scalefactor, mFC, tables)
    { }

  protected:
    using BaseClass::fvZero;
    using BaseClass::vhel;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp4q1gAx2Static>();
    }

    void markZeroFv()
    {
      fvZero.reset();
      if (vhel[0] == vhel[1] || vhel[2] == vhel[3]) {
        fvZero[0] = true;
        fvZero[1] = true;
        fvZero[2] = true;
      }
      if (vhel[0] == vhel[3] || vhel[2] == vhel[1]) {
        fvZero[3] = true;
        fvZero[4] = true;
        fvZero[5] = true;
      }
    }
};

// Two photons

class Amp4q1gAAStatic : public Amp4q1gStatic
{
  public:
    static const int FC = 8;

    static const int flav[FC][NN];  // flavour vectors for ngluon
    static const int fvsign[FC];    // flavour channels' signs
    static const int fperm[FC][NN];        // flavour pemutation matrix
    static const int fvcol[FC][CC];        // flavour-partial pemutation matrix

    static const int HS = 32;
    static const int HSNN = NN+2;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp4q1gAA : public Amp4q1g<T>
{
    typedef Amp4q1g<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp4q1gAA(const Flavour<double>& Vflav, const T scalefactor,
              const int mFC=4, const NJetAmpTables& tables=amptables());

  protected:
    using BaseClass::NN;
    using BaseClass::vhel;
    using BaseClass::getFlav;
    using BaseClass::getFperm;
    using BaseClass::mfv;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp4q1gAAStatic>();
    }

    void initProcess(const Flavour<double>& ff);

    TreeValue A0(int p0, int p1, int p2, int p3, int p4);
    LoopResult<T> AL(int p0, int p1, int p2, int p3, int p4);
    LoopResult<T> AF(int p0, int p1, int p2, int p3, int p4);
};

template <typename T>
class Amp4q1gAAd : public Amp4q1gAA<T>
{
    typedef Amp4q1gAA<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp4q1gAAd(const Flavour<double>& Vflav, const T scalefactor,
               const int mFC=4, const NJetAmpTables& tables=BaseClass::amptables());

  protected:
    using BaseClass::NN;
    using BaseClass::getFlav;
    using BaseClass::mfv;

    void initProcess(const Flavour<double>& ff);
};

class Amp4q1gAA2Static : public Amp4q1gAAStatic
{
  public:
    static const int HS = 48;
    static const int HSNN = NN+2;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp4q1gAA2 : public Amp4q1gAA<T>
{
    typedef Amp4q1gAA<T> BaseClass;
  public:

    Amp4q1gAA2(const Flavour<double>& Vflav,
               const T scalefactor, const NJetAmpTables& tables=amptables())
      : BaseClass(Vflav, scalefactor, 8, tables)
    { }

  protected:
    using BaseClass::fvZero;
    using BaseClass::vhel;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp4q1gAA2Static>();
    }

    void markZeroFv()
    {
      fvZero.reset();
      if (vhel[0] == vhel[1] || vhel[2] == vhel[3]) {
        fvZero[0] = true;
        fvZero[1] = true;
        fvZero[2] = true;
        fvZero[3] = true;
      }
      if (vhel[0] == vhel[3] || vhel[2] == vhel[1]) {
        fvZero[4] = true;
        fvZero[5] = true;
        fvZero[6] = true;
        fvZero[7] = true;
      }
    }
};

#endif // CHSUM_4Q1GA_H
