/*
* chsums/2q1gH.cpp
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include "2q1gH.h"
#include "NJetAmp-T.h"

// class Amp2q1gH

template <typename T>
Amp2q1gH<T>::Amp2q1gH(const T scalefactor, const int mFC, const NJetAmpTables& tables)
  : BaseClass(scalefactor, mFC, tables)
{
  const Flavour<double> H = StandardModel::RealFlavour(ModelBase::Higgs);
  for (int fv=0; fv<mFC; fv++) {
    Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
    flavours.push_back(H);
    BaseClass::setProcess(flavours, fv);
  }
}

template <typename T>
typename Amp2q1gH<T>::TreeValue
Amp2q1gH<T>::A0(int p0, int p1, int p2)
{
  const int* O = getFperm(mfv);
  int order[] = {NN, O[p0], O[p1], O[p2]};
  return BaseClass::A0nH(order);
}

#ifdef USE_SD
  template class Amp2q1gH<double>;
#endif
#ifdef USE_DD
  template class Amp2q1gH<dd_real>;
#endif
#ifdef USE_QD
  template class Amp2q1gH<qd_real>;
#endif
#ifdef USE_VC
  template class Amp2q1gH<Vc::double_v>;
#endif
