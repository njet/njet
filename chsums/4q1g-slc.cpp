/*
* chsums/4q1g-slc.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include "4q1g.h"

template <typename T>
void Amp4q1g<T>::getfvpart1_slc(const int fv, LoopValue* fvpart)
{
  return getfvpart1_slc_(fv, fvpart);
}

template <typename T>
void Amp4q1g<T>::getfvpart1_slc(const int fv, LoopResult<T>* fvpart)
{
  return getfvpart1_slc_(fv, fvpart);
}

// -------- BEGIN -- automatically generated code -- BEGIN -------- //

template <typename T>
template <typename LT>
inline
void Amp4q1g<T>::getfvpart1_slc_(const int fv, LT* fvpart)
{
  fvSet(fv);
  {
    const int norder = norderL;

    if (norder < 2) {
    // [-1, -2] order 14 primitives
    const LT P12345 = AL(0,1,2,3,4);
    const LT P12354 = AL(0,1,2,4,3);
    const LT P12435 = AL(0,1,3,2,4);
    const LT P12453 = AL(0,1,3,4,2);
    const LT P12534 = AL(0,1,4,2,3);
    const LT P12543 = AL(0,1,4,3,2);
    const LT P14352 = AL(0,3,2,4,1);
    const LT P14532 = AL(0,3,4,2,1);
    const LT P15234 = AL(0,4,1,2,3);
    const LT P15243 = AL(0,4,1,3,2);
    const LT P15432 = AL(0,4,3,2,1);
    const LT P32154 = AL(2,1,0,4,3);
    const LT P32514 = AL(2,1,4,0,3);
    const LT P35214 = AL(2,4,1,0,3);
    fvpart[0] = (-P12354+P12453+P14532-P32154-P32514-P35214)/Nc2;
    fvpart[1] = (-P14352-P14532-P15234-P15243-P15432+P32514)/Nc2;
    fvpart[2] = (P12345+P12354+P12435-P12453-P12534-P12543+P14352+P15234+P15243+P35214)/Nc;
    fvpart[3] = (-P12345+P12354-P12435-P12453+P12534+P12543+P15234+P15243+P15432+P32154)/Nc;

    if (norder < 1) {
    // [0] order 2 primitives
    const LT P14325 = AL(0,3,2,1,4);
    const LT P32145 = AL(2,1,0,3,4);
    fvpart[0] += -P12435-P12543-P14325-P14352-P14532-P15243-P15432;
    fvpart[1] += P12435+P12453+P12543-P32145-P32154-P32514-P35214;

    if (norder < 0) {
    // [1] order 0 primitives
    fvpart[2] += Nc*P12534;
    fvpart[3] += Nc*P12345;
    }}}
  }
  if (Nf != 0.) {
    const int norder = norderF;

    if (norder < 1) {
    // [-1] order 3 primitives
    const LT Q12345 = AF(0,1,2,3,4);
    const LT Q12354 = AF(0,1,2,4,3);
    const LT Q12534 = AF(0,1,4,2,3);
    fvpart[0] += Nf*(-Q12354/Nc);
    fvpart[1] += Nf*((Q12345+Q12354+Q12534)/Nc);

    if (norder < 0) {
    // [0] order 0 primitives
    fvpart[2] += Nf*(-Q12534);
    fvpart[3] += Nf*(-Q12345);
    }}
  }
}

// --------- END --- automatically generated code --- END --------- //

#define INSTANTIATE(T) \
  template void Amp4q1g<T>::getfvpart1_slc(const int fv, LoopValue* fvpart); \
  template void Amp4q1g<T>::getfvpart1_slc(const int fv, LoopResult<T>* fvpart);

#ifdef USE_SD
  INSTANTIATE(double)
#endif
#ifdef USE_DD
  INSTANTIATE(dd_real)
#endif
#ifdef USE_QD
  INSTANTIATE(qd_real)
#endif
#ifdef USE_VC
  INSTANTIATE(Vc::double_v)
#endif

#undef INSTANTIATE
