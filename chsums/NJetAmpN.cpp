/*
* chsums/NJetAmpN.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include "NJetAmpN.h"
#include "NJetAmp-T.h"

// NJetAmp4

template <typename T>
typename NJetAmp4<T>::TreeValue
NJetAmp4<T>::A0(int p0, int p1, int p2, int p3)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], O[p1], O[p2], O[p3]};
  return BaseClass::A0n(order);
}

template <typename T>
LoopResult<T> NJetAmp4<T>::AF(int p0, int p1, int p2, int p3)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], O[p1], O[p2], O[p3]};
  return BaseClass::AFn(order);
}

template <typename T>
LoopResult<T> NJetAmp4<T>::AL(int p0, int p1, int p2, int p3)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], O[p1], O[p2], O[p3]};
  return BaseClass::ALn(order);
}

// NJetAmp5

template <typename T>
typename NJetAmp5<T>::TreeValue
NJetAmp5<T>::A0(int p0, int p1, int p2, int p3, int p4)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], O[p1], O[p2], O[p3], O[p4]};
  return BaseClass::A0n(order);
}

template <typename T>
LoopResult<T> NJetAmp5<T>::AF(int p0, int p1, int p2, int p3, int p4)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], O[p1], O[p2], O[p3], O[p4]};
  return BaseClass::AFn(order);
}

template <typename T>
LoopResult<T> NJetAmp5<T>::AL(int p0, int p1, int p2, int p3, int p4)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], O[p1], O[p2], O[p3], O[p4]};
  return BaseClass::ALn(order);
}

// NJetAmp6

template <typename T>
typename NJetAmp6<T>::TreeValue
NJetAmp6<T>::A0(int p0, int p1, int p2, int p3, int p4, int p5)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], O[p1], O[p2], O[p3], O[p4], O[p5]};
  return BaseClass::A0n(order);
}

template <typename T>
LoopResult<T> NJetAmp6<T>::AF(int p0, int p1, int p2, int p3, int p4, int p5)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], O[p1], O[p2], O[p3], O[p4], O[p5]};
  return BaseClass::AFn(order);
}

template <typename T>
LoopResult<T> NJetAmp6<T>::AL(int p0, int p1, int p2, int p3, int p4, int p5)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], O[p1], O[p2], O[p3], O[p4], O[p5]};
  return BaseClass::ALn(order);
}

// NJetAmp6

template <typename T>
typename NJetAmp7<T>::TreeValue
NJetAmp7<T>::A0(int p0, int p1, int p2, int p3, int p4, int p5, int p6)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], O[p1], O[p2], O[p3], O[p4], O[p5], O[p6]};
  return BaseClass::A0n(order);
}

template <typename T>
LoopResult<T> NJetAmp7<T>::AF(int p0, int p1, int p2, int p3, int p4, int p5, int p6)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], O[p1], O[p2], O[p3], O[p4], O[p5], O[p6]};
  return BaseClass::AFn(order);
}

template <typename T>
LoopResult<T> NJetAmp7<T>::AL(int p0, int p1, int p2, int p3, int p4, int p5, int p6)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], O[p1], O[p2], O[p3], O[p4], O[p5], O[p6]};
  return BaseClass::ALn(order);
}

#ifdef USE_SD
  template class NJetAmp4<double>;
  template class NJetAmp5<double>;
  template class NJetAmp6<double>;
  template class NJetAmp7<double>;
#endif
#ifdef USE_DD
  template class NJetAmp4<dd_real>;
  template class NJetAmp5<dd_real>;
  template class NJetAmp6<dd_real>;
  template class NJetAmp7<dd_real>;
#endif
#ifdef USE_QD
  template class NJetAmp4<qd_real>;
  template class NJetAmp5<qd_real>;
  template class NJetAmp6<qd_real>;
  template class NJetAmp7<qd_real>;
#endif
#ifdef USE_VC
  template class NJetAmp4<Vc::double_v>;
  template class NJetAmp5<Vc::double_v>;
  template class NJetAmp6<Vc::double_v>;
  template class NJetAmp7<Vc::double_v>;
#endif
