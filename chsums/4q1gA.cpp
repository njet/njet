/*
* chsums/4q1gA.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012, 2013 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include "4q1gA.h"
#include "NJetAmp-T.h"

// class Amp2q3gAx

template <typename T>
Amp4q1gAx<T>::Amp4q1gAx(const Flavour<double>& ff, const T scalefactor,
                        const int mFC, const NJetAmpTables& tables)
 : BaseClass(ff, scalefactor, mFC, tables)
{
  const Flavour<double> Ax = StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C());
  for (int fv=0; fv<mFC; fv++) {
    if (fv % 3 == 0) {
      Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
      flavours.push_back(Ax);
      BaseClass::setProcess(flavours, fv);
    }
  }
}

template <typename T>
void Amp4q1gAx<T>::setNf(const ST Nf_)
{
  ST newNf = Nf_;
  const int intNf = int(to_double(newNf));
  if (double(intNf) == newNf) {
    const int nu = intNf/2;
    const int nd = (intNf+1)/2;
    newNf = -(double(nu)*Particle<ST>::getQu() + double(nd)*Particle<ST>::getQd());
  }
  BaseClass::setNf(newNf);
}

template <typename T>
void Amp4q1gAx<T>::getfvpart1_full(const int fv, LoopValue* fvpart)
{
  return getfvpart1_full_(fv, fvpart);
}

template <typename T>
void Amp4q1gAx<T>::getfvpart1_full(const int fv, LoopResult<T>* fvpart)
{
  return getfvpart1_full_(fv, fvpart);
}

template <typename T>
template <typename LT>
inline
void Amp4q1gAx<T>::getfvpart1_full_(const int fv, LT* fvpart)
{
  fvSet(fv);

  if (fv % 3 == 0 and Nf != 0.) {
    // 4 primitives
    const LT Q12345 = AFx(0,1,2,3,4);
    const LT Q12354 = AFx(0,1,2,4,3);
    const LT Q12534 = AFx(0,1,4,2,3);
    const LT Q15234 = AFx(0,4,1,2,3);

    fvpart[0] = Nf*((Q12345 + Q12534 + Q15234))/Nc;
    fvpart[1] = Nf*((Q12345 + Q12354 + Q12534))/Nc;
    fvpart[2] = Nf*(-Q12534);
    fvpart[3] = Nf*(-Q12345);
  } else {
    fvpart[0] = LT();
    fvpart[1] = LT();
    fvpart[2] = LT();
    fvpart[3] = LT();
  }
}

template <typename T>
LoopResult<T> Amp4q1gAx<T>::AFx(int p0, int p1, int p2, int p3, int p4)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], O[p1], O[p2], O[p3], O[p4], NN};
  return BaseClass::AFnVax(order);
}

// class Amp4q1gAxd

template <typename T>
Amp4q1gAxd<T>::Amp4q1gAxd(const Flavour<double>& ff,
                          const T scalefactor, const int mFC, const NJetAmpTables& tables)
  : BaseClass(ff, scalefactor, mFC, tables)
{
  const Flavour<double> Ax = StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C());
  const Flavour<double> Au = ff;
  const Flavour<double> Ad = StandardModel::BosonFlip(ff);

  Flavour<double> ffs[3] = {Ax, Au, Ad};

  for (int fv=0; fv<BaseClass::mFC; fv++) {
    Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
    flavours.push_back(ffs[fv]);
    BaseClass::setProcess(flavours, fv);
  }
}

// class Amp4q1gAA

template <typename T>
Amp4q1gAA<T>::Amp4q1gAA(const Flavour<double>& ff, const T scalefactor,
                        const int mFC, const NJetAmpTables& tables)
  : BaseClass(scalefactor, mFC, tables)
{
  initProcess(ff);
}

template <typename T>
void Amp4q1gAA<T>::initProcess(const Flavour<double>& ff)
{
  const Flavour<double> A1 = ff;
  const Flavour<double> A2 = StandardModel::BosonNext(ff);

  Flavour<double> ffs1[] = {A1, A1, A1, A1,   A1, A1, A1, A1};
  Flavour<double> ffs2[] = {A1, A2, A1, A2,   A1, A2, A1, A2};

  for (int fv=0; fv<BaseClass::mFC; fv++) {
    Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
    flavours.push_back(ffs1[fv]);
    flavours.push_back(ffs2[fv]);
    BaseClass::setProcess(flavours, fv);
  }
}

template <typename T>
Amp4q1gAAd<T>::Amp4q1gAAd(const Flavour<double>& ff, const T scalefactor,
                          const int mFC, const NJetAmpTables& tables)
  : BaseClass(ff, scalefactor, mFC, tables)
{
  initProcess(ff);
}

template <typename T>
void Amp4q1gAAd<T>::initProcess(const Flavour<double>& ff)
{
  const Flavour<double> A1u = ff;
  const Flavour<double> A2u = StandardModel::BosonNext(ff);

  const Flavour<double> A1d = StandardModel::BosonFlip(A1u);
  const Flavour<double> A2d = StandardModel::BosonFlip(A2u);

  Flavour<double> ffs1[] = {A1u, A1u, A1d, A1d};
  Flavour<double> ffs2[] = {A1u, A2d, A1d, A2u};

  for (int fv=0; fv<BaseClass::mFC; fv++) {
    Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
    flavours.push_back(ffs1[fv]);
    flavours.push_back(ffs2[fv]);
    BaseClass::setProcess(flavours, fv);
  }
}

template <typename T>
typename Amp4q1gAA<T>::TreeValue
Amp4q1gAA<T>::A0(int p0, int p1, int p2, int p3, int p4)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, NN+1, O[p1], O[p2], O[p3], O[p4]};
  TreeValue amp = TreeValue();
  if (mfv % 2 == 0) {
    amp = BaseClass::A0nAA(order);
  } else {
    amp = BaseClass::A0nAA2(order);
  }
  return amp;
}

template <typename T>
LoopResult<T> Amp4q1gAA<T>::AF(int p0, int p1, int p2, int p3, int p4)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, NN+1, O[p1], O[p2], O[p3], O[p4]};
  LoopResult<T> amp = LoopResult<T>();
  if (mfv % 2 == 0) {
    amp = BaseClass::AFnAA(order);
  } else {
#ifdef SYMMETRIZE_AA
    amp = BaseClass::AFnAA2(order);
#else
    if (mfv % 4 == 1) {
      amp = 2.*BaseClass::AFnAA2(order);
    }
#endif
  }
  return amp;
}

template <typename T>
LoopResult<T> Amp4q1gAA<T>::AL(int p0, int p1, int p2, int p3, int p4)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, NN+1, O[p1], O[p2], O[p3], O[p4]};
  LoopResult<T> amp = LoopResult<T>();
  if (mfv % 2 == 0) {
    amp = BaseClass::ALnAA(order);
  } else {
#ifdef SYMMETRIZE_AA
    amp = BaseClass::ALnAA2(order);
#else
    if (mfv % 4 == 1) {
      amp = 2.*BaseClass::ALnAA2(order);
    }
#endif
  }
  return amp;
}

#ifdef USE_SD
  template class Amp4q1gAx<double>;
  template class Amp4q1gAxd<double>;
  template class Amp4q1gAA<double>;
  template class Amp4q1gAAd<double>;
#endif
#ifdef USE_DD
  template class Amp4q1gAx<dd_real>;
  template class Amp4q1gAxd<dd_real>;
  template class Amp4q1gAA<dd_real>;
  template class Amp4q1gAAd<dd_real>;
#endif
#ifdef USE_QD
  template class Amp4q1gAx<qd_real>;
  template class Amp4q1gAxd<qd_real>;
  template class Amp4q1gAA<qd_real>;
  template class Amp4q1gAAd<qd_real>;
#endif
#ifdef USE_VC
  template class Amp4q1gAx<Vc::double_v>;
  template class Amp4q1gAxd<Vc::double_v>;
  template class Amp4q1gAA<Vc::double_v>;
  template class Amp4q1gAAd<Vc::double_v>;
#endif
