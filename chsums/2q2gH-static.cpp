/*
* chsums/2q2gH-static.cpp
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>

#include "2q2gH.h"

// class Amp2q2gHStatic

const int
Amp2q2gHStatic::HSarr[HS][HSNN] = {
  {-1, 1, 1, 1, 1},
  {-1, 1, 1,-1, 1},
  {-1, 1,-1, 1, 1},
  {-1, 1,-1,-1, 1},
  { 1,-1,-1,-1, 1},
  { 1,-1,-1, 1, 1},
  { 1,-1, 1,-1, 1},
  { 1,-1, 1, 1, 1},
};
