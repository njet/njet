/*
* chsums/2q4gA.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012, 2013 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef CHSUM_2Q4GA_H
#define CHSUM_2Q4GA_H

#include "2q4gV.h"

template <typename T>
class Amp2q4gA : public Amp2q4gZ<T>
{
    typedef Amp2q4gZ<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp2q4gA(const Flavour<double>& ff, const T scalefactor,
             const int mFC=1, const NJetAmpTables& tables=BaseClass::amptables())
    : BaseClass(ff, scalefactor, mFC, tables)
    { }

    void born_part_fill() { BaseClass::born_part_trickfill(); }
    LoopValue virt() { return BaseClass::virt_tricksum(); }
};

// Two photons

class Amp2q4gAAStatic : public virtual Amp2q4gStatic
{
  public:
    static const int HS = 128;
    static const int HSNN = NN+2;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp2q4gAA : public Amp2q4g<T>
{
    typedef Amp2q4g<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp2q4gAA(const Flavour<double>& Vflav, const T scalefactor,
              const int mFC=1, const NJetAmpTables& tables=amptables());

  protected:
    using BaseClass::NN;
    using BaseClass::vhel;
    using BaseClass::getFlav;
    using BaseClass::getFperm;
    using BaseClass::mfv;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp2q4gAAStatic>();
    }

    void initProcess(const Flavour<double>& ff);

    TreeValue A0(int p0, int p1, int p2, int p3, int p4, int p5);
    LoopResult<T> AL(int p0, int p1, int p2, int p3, int p4, int p5);
    LoopResult<T> AF(int p0, int p1, int p2, int p3, int p4, int p5);
};

#endif // CHSUM_2Q4GA_H
