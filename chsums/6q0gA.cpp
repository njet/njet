/*
* chsums/6q0gA.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012, 2013 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include "6q0gA.h"
#include "NJetAmp-T.h"

// class Amp6q0gAA
template <typename T>
Amp6q0gAA<T>::Amp6q0gAA(const Flavour<double>& ff, const T scalefactor,
                        const int mFC, const NJetAmpTables& tables)
  : BaseClass(scalefactor, mFC, tables)
{
  initProcess(ff);
}

template <typename T>
void Amp6q0gAA<T>::initProcess(const Flavour<double>& ff)
{
  const Flavour<double> A1 = ff;
  const Flavour<double> A2 = StandardModel::BosonNext(A1);

  Flavour<double> ffs1[] = {A1, A1, A1, A1, A1, A1, A1, A1, A1,
                            A1, A1, A1, A1, A1, A1, A1, A1, A1,
                            A1, A1, A1, A1, A1, A1, A1, A1, A1,
                            A1, A1, A1, A1, A1, A1, A1, A1, A1,
                            A1, A1, A1, A1, A1, A1, A1, A1, A1,
                            A1, A1, A1, A1, A1, A1, A1, A1, A1};
  Flavour<double> ffs2[] = {A1, A2, A2, A1, A2, A2, A1, A2, A2,
                            A1, A2, A2, A1, A2, A2, A1, A2, A2,
                            A1, A2, A2, A1, A2, A2, A1, A2, A2,
                            A1, A2, A2, A1, A2, A2, A1, A2, A2,
                            A1, A2, A2, A1, A2, A2, A1, A2, A2,
                            A1, A2, A2, A1, A2, A2, A1, A2, A2};

  for (int fv=0; fv<BaseClass::mFC; fv++) {
    Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
    flavours.push_back(ffs1[fv]);
    flavours.push_back(ffs2[fv]);
    BaseClass::setProcess(flavours, fv);
  }
}

template <typename T>
Amp6q0gAAd<T>::Amp6q0gAAd(const Flavour<double>& ff, const T scalefactor,
                          const int mFC, const NJetAmpTables& tables)
  : BaseClass(ff, scalefactor, mFC, tables)
{
  initProcess(ff);
}

template <typename T>
void Amp6q0gAAd<T>::initProcess(const Flavour<double>& ff)
{
  const Flavour<double> A1u = ff;
  const Flavour<double> A2u = StandardModel::BosonNext(A1u);

  const Flavour<double> A1d = StandardModel::BosonFlip(A1u);
  const Flavour<double> A2d = StandardModel::BosonFlip(A2u);

  Flavour<double> ffs1[] = {A1u, A1u, A1u, A1u, A1u, A1u, A1d, A1d, A1d};
  Flavour<double> ffs2[] = {A1u, A2u, A2d, A1u, A2u, A2d, A1d, A2u, A2u};

  for (int fv=0; fv<BaseClass::mFC; fv++) {
    Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
    flavours.push_back(ffs1[fv]);
    flavours.push_back(ffs2[fv]);
    BaseClass::setProcess(flavours, fv);
  }
}

template <typename T>
Amp6q0gAA2d<T>::Amp6q0gAA2d(const Flavour<double>& ff, const T scalefactor,
                            const NJetAmpTables& tables)
  : BaseClass(ff, scalefactor, tables)
{
  initProcess(ff);
}

template <typename T>
void Amp6q0gAA2d<T>::initProcess(const Flavour<double>& ff)
{
  const Flavour<double> A1u = ff;
  const Flavour<double> A2u = StandardModel::BosonNext(A1u);

  const Flavour<double> A1d = StandardModel::BosonFlip(A1u);
  const Flavour<double> A2d = StandardModel::BosonFlip(A2u);

  Flavour<double> ffs1[] = {A1u, A1u, A1u, A1u, A1u, A1u, A1d, A1d, A1d,
                            A1u, A1u, A1u, A1u, A1u, A1u, A1d, A1d, A1d};
  Flavour<double> ffs2[] = {A1u, A2u, A2d, A1u, A2u, A2d, A1d, A2u, A2u,
                            A1u, A2u, A2d, A1u, A2u, A2d, A1d, A2u, A2u};

  for (int fv=0; fv<BaseClass::mFC; fv++) {
    Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
    flavours.push_back(ffs1[fv]);
    flavours.push_back(ffs2[fv]);
    BaseClass::setProcess(flavours, fv);
  }
}

template <typename T>
typename Amp6q0gAA<T>::TreeValue
Amp6q0gAA<T>::A0(int p0, int p1, int p2, int p3, int p4, int p5)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, NN+1, O[p1], O[p2], O[p3], O[p4], O[p5]};
  TreeValue amp = TreeValue();
  if (mfv % 3 == 0) {
    amp = BaseClass::A0nAA(order);
  } else {
    amp = BaseClass::A0nAA2(order);
  }
  return amp;
}

template <typename T>
LoopResult<T> Amp6q0gAA<T>::AF(int p0, int p1, int p2, int p3, int p4, int p5)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, NN+1, O[p1], O[p2], O[p3], O[p4], O[p5]};
  LoopResult<T> amp = LoopResult<T>();
  if (mfv % 3 == 0) {
    amp = BaseClass::AFnAA(order);
  } else {
#ifdef SYMMETRIZE_AA
    amp = BaseClass::AFnAA2(order);
#else
    const int mfv9 = mfv % 9;
    if (mfv9 == 1 or mfv9 == 2 or mfv9 == 5) { // 1-4 2-7 5-8
      amp = 2.*BaseClass::AFnAA2(order);
    }
#endif
  }
  return amp;
}

template <typename T>
LoopResult<T> Amp6q0gAA<T>::AL(int p0, int p1, int p2, int p3, int p4, int p5)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, NN+1, O[p1], O[p2], O[p3], O[p4], O[p5]};
  LoopResult<T> amp = LoopResult<T>();
  if (mfv % 3 == 0) {
    amp = BaseClass::ALnAA(order);
  } else {
#ifdef SYMMETRIZE_AA
    amp = BaseClass::ALnAA2(order);
#else
    const int mfv9 = mfv % 9;
    if (mfv9 == 1 or mfv9 == 2 or mfv9 == 5) { // 1-4 2-7 5-8
      amp = 2.*BaseClass::ALnAA2(order);
    }
#endif
  }
  return amp;
}

#ifdef USE_SD
  template class Amp6q0gAA<double>;
  template class Amp6q0gAAd<double>;
  template class Amp6q0gAA2d<double>;
#endif
#ifdef USE_DD
  template class Amp6q0gAA<dd_real>;
  template class Amp6q0gAAd<dd_real>;
  template class Amp6q0gAA2d<dd_real>;
#endif
#ifdef USE_QD
  template class Amp6q0gAA<qd_real>;
  template class Amp6q0gAAd<qd_real>;
  template class Amp6q0gAA2d<qd_real>;
#endif
#ifdef USE_VC
  template class Amp6q0gAA<Vc::double_v>;
  template class Amp6q0gAAd<Vc::double_v>;
  template class Amp6q0gAA2d<Vc::double_v>;
#endif
