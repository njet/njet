/*
* chsums/4q3g.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef CHSUM_4Q3G_H
#define CHSUM_4Q3G_H

#include "NJetAmpN.h"

class Amp4q3gStatic : public AmpNStatic<7>
{
  public:
    static const int FC = 2;    // flavour permutations
    static const int C0 = 48;   // partial amplitudes
    static const int CC = 64;   // partial amplitudes

    static const int flav[FC][NN];  // flavour vectors for ngluon
    static const int fvsign[FC];    // flavour channels' signs
    static const int ccsign[NN*(NN+1)/2];  // ccborn signs
    static const int fperm[FC][NN];        // flavour pemutation matrix
    static const int fvcol[FC][CC];        // flavour-partial pemutation matrix

    static const unsigned char colmat[CC*(CC+1)/2];  // partial-partial colour factors
    static const unsigned char colmatcc[NN*(NN+1)/2][C0*(C0+1)/2];  // ccborn partial-partial factors
    static const int NmatLen = 22;
    static const int NmatccLen = 60;

    static const int HS = 32;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp4q3g : public NJetAmp7<T>
{
    typedef NJetAmp7<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;
    typedef typename BaseClass::ST ST;

    Amp4q3g(const T scalefactor, const int mFC=1, const NJetAmpTables& tables=amptables());

    void setNc(const ST Nc_);

    LoopValue virt() { return BaseClass::virt_tricksum(); }
    void born_part_fill() { BaseClass::born_part_trickfill(); }

  protected:
    using BaseClass::NN;
    using BaseClass::V;
    using BaseClass::Nc;
    using BaseClass::Nc2;
    using BaseClass::Nc3;
    using BaseClass::Nc4;
    using BaseClass::Nf;
    using BaseClass::vhel;
    using BaseClass::C0;
    using BaseClass::CC;
    using BaseClass::Nmat;
    using BaseClass::Nmatcc;
    using BaseClass::bornFactor;
    using BaseClass::bornccFactor;
    using BaseClass::loopFactor;
    using BaseClass::getFlav;
    using BaseClass::getFperm;
    using BaseClass::fvSet;
    using BaseClass::norderL;
    using BaseClass::norderF;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp4q3gStatic>();
    }

    void initNc();

    using BaseClass::A0;
    using BaseClass::AF;
    using BaseClass::AL;

    void getfvpart0(const int fv, TreeValue* fvpart, TreeValue* fvpartC);

    bool setLoopType(int type, int norderL_, int norderF_)
    {
      return BaseClass::setLoopTypeLCSLC(type, norderL_, norderF_);
    }

    void getfvpart1_full(const int fv, LoopValue* fvpart);
    void getfvpart1_full(const int fv, LoopResult<T>* fvpart);

    void getfvpart1_lc(const int fv, LoopValue* fvpart);
    void getfvpart1_lc(const int fv, LoopResult<T>* fvpart);

    void getfvpart1_slc(const int fv, LoopValue* fvpart);
    void getfvpart1_slc(const int fv, LoopResult<T>* fvpart);

    template <typename LT>
    void getfvpart1_full_(const int fv, LT* fvpart);

    template <typename LT>
    void getfvpart1_lc_(const int fv, LT* fvpart);

    template <typename LT>
    void getfvpart1_slc_(const int fv, LT* fvpart);
};

class Amp4q3g2Static : public Amp4q3gStatic
{
  public:
    static const int HS = 48;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp4q3g2 : public Amp4q3g<T>
{
    typedef Amp4q3g<T> BaseClass;
  public:

    Amp4q3g2(const T scalefactor)
      : BaseClass(scalefactor, 2, amptables())
    { }

  protected:
    using BaseClass::fvZero;
    using BaseClass::vhel;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp4q3g2Static>();
    }

    void markZeroFv()
    {
      fvZero.reset();
      if (vhel[0] == vhel[1] || vhel[2] == vhel[3]) {
        fvZero[0] = true;
      }
      if (vhel[0] == vhel[3] || vhel[2] == vhel[1]) {
        fvZero[1] = true;
      }
    }
};

#endif // CHSUM_4Q3G_H
