/*
* chsums/4q1gV.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef CHSUM_4Q1GV_H
#define CHSUM_4Q1GV_H

#include "4q1g.h"
#include "../ngluon2/Model.h"

class Amp4q1gVStatic : public Amp4q1gStatic
{
  public:
    static const int HS = 4;
    static const int HSNN = NN+1;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp4q1gV : public Amp4q1g<T>
{
    typedef Amp4q1g<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp4q1gV(const T scalefactor, const int mFC=1, const NJetAmpTables& tables=amptables());
    Amp4q1gV(const Flavour<double>& Vflav,
             const T scalefactor, const int mFC=1, const NJetAmpTables& tables=amptables());

    void born_part_fill() { BaseClass::born_part_fullfill(); }
    LoopValue virt() { return BaseClass::virt_fullsum(); }

  protected:
    using BaseClass::NN;
    using BaseClass::vhel;
    using BaseClass::getFlav;
    using BaseClass::getFperm;
    using BaseClass::mfv;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp4q1gVStatic>();
    }

    void initProcess(const Flavour<double>& ff);

    TreeValue A0(int p0, int p1, int p2, int p3, int p4);
    LoopResult<T> AL(int p0, int p1, int p2, int p3, int p4);
    LoopResult<T> AF(int p0, int p1, int p2, int p3, int p4);
};

class Amp4q1gV2Static : public Amp4q1gVStatic
{
  public:
    static const int HS = 6;
    static const int HSNN = NN+1;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp4q1gV2 : public Amp4q1gV<T>
{
    typedef Amp4q1gV<T> BaseClass;
  public:

    Amp4q1gV2(const T scalefactor, const NJetAmpTables& tables=amptables())
      : BaseClass(scalefactor, 2, tables)
    { }

    Amp4q1gV2(const Flavour<double>& Vflav,
              const T scalefactor, const NJetAmpTables& tables=amptables())
      : BaseClass(Vflav, scalefactor, 2, tables)
    { }

  protected:
    using BaseClass::fvZero;
    using BaseClass::vhel;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp4q1gV2Static>();
    }

    void markZeroFv()
    {
      fvZero.reset();
      if (vhel[0] == vhel[1] || vhel[2] == vhel[3]) {
        fvZero[0] = true;
      }
      if (vhel[0] == vhel[3] || vhel[2] == vhel[1]) {
        fvZero[1] = true;
      }
    }
};

class Amp4q1gV2bStatic : public Amp4q1gVStatic
{
  public:
    static const int HS = 6;
    static const int HSNN = NN+1;
    static const int HSarr[HS][HSNN];

    static const int flav[FC][NN];
    static const int fperm[FC][NN];
    static const int fvcol[FC][CC];
};

template <typename T>
class Amp4q1gV2b : public Amp4q1gV2<T>
{
    typedef Amp4q1gV2<T> BaseClass;
  public:

    Amp4q1gV2b(const T scalefactor, const NJetAmpTables& tables=amptables())
      : BaseClass(scalefactor, tables)
    { }

    Amp4q1gV2b(const Flavour<double>& Vflav,
               const T scalefactor, const NJetAmpTables& tables=amptables())
      : BaseClass(Vflav, scalefactor, tables)
    { }

  protected:
    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp4q1gV2bStatic>();
    }
};

// Z amplitudes

class Amp4q1gZStatic : public Amp4q1gVStatic
{
  public:
    static const int FC = 4;

    static const int flav[FC][NN];  // flavour vectors for ngluon
    static const int fvsign[FC];    // flavour channels' signs
    static const int fperm[FC][NN];        // flavour pemutation matrix
    static const int fvcol[FC][CC];        // flavour-partial pemutation matrix

    static const int HS = 16;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp4q1gZ : public Amp4q1gV<T>
{
    typedef Amp4q1gV<T> BaseClass;
  public:

    Amp4q1gZ(const Flavour<double>& ff, const T scalefactor,
             const int mFC=2, const NJetAmpTables& tables=amptables())
    : BaseClass(ff, scalefactor, mFC, tables)
    { }

  protected:
    using BaseClass::fvZero;
    using BaseClass::vhel;
    using BaseClass::NN;
    using BaseClass::getFlav;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp4q1gZStatic>();
    }
};

template <typename T>
class Amp4q1gZd : public Amp4q1gZ<T>
{
    typedef Amp4q1gZ<T> BaseClass;
  public:

    Amp4q1gZd(const Flavour<double>& ff, const T scalefactor,
              const int mFC=2, const NJetAmpTables& tables=BaseClass::amptables());

  protected:
    using BaseClass::NN;
    using BaseClass::getFlav;
};

class Amp4q1gZ2Static : public Amp4q1gZStatic
{
  public:
    static const int HS = 24;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp4q1gZ2 : public Amp4q1gZ<T>
{
    typedef Amp4q1gZ<T> BaseClass;
  public:

    Amp4q1gZ2(const Flavour<double>& ff, const T scalefactor,
              const int mFC=4, const NJetAmpTables& tables=amptables())
    : BaseClass(ff, scalefactor, mFC, tables)
    { }

  protected:
    using BaseClass::fvZero;
    using BaseClass::vhel;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp4q1gZ2Static>();
    }

    void markZeroFv()
    {
      fvZero.reset();
      if (vhel[0] == vhel[1] || vhel[2] == vhel[3]) {
        fvZero[0] = true;
        fvZero[1] = true;
      }
      if (vhel[0] == vhel[3] || vhel[2] == vhel[1]) {
        fvZero[2] = true;
        fvZero[3] = true;
      }
    }
};

#endif // CHSUM_4Q1GV_H
