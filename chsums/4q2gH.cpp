/*
* chsums/4q2gH.cpp
*
* This file is part of NJet library
* Copyright (C) 2013, 2014 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>

#include "4q2gH.h"
#include "NJetAmp-T.h"

// class Amp4q2gH

template <typename T>
Amp4q2gH<T>::Amp4q2gH(const T scalefactor,
                      const int mFC, const NJetAmpTables& tables)
  : BaseClass(scalefactor, mFC, tables)
{
  for (int fv=0; fv<mFC; fv++) {
    BaseClass::initNG(scalefactor, fv);
  }

  const Flavour<double> ff = StandardModel::RealFlavour(ModelBase::Higgs);
  initProcess(ff);
}


template <typename T>
void Amp4q2gH<T>::initProcess(const Flavour<double>& ff)
{
  for (int fv=0; fv<BaseClass::mFC; fv++) {
    Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
    flavours.push_back(ff);
    BaseClass::setProcess(flavours, fv);
  }
}

template <typename T>
typename Amp4q2gH<T>::TreeValue
Amp4q2gH<T>::A0(int p0, int p1, int p2, int p3, int p4, int p5)
{
  const int* O = getFperm(mfv);
  int order[] = {NN, O[p0], O[p1], O[p2], O[p3], O[p4], O[p5]};
  return BaseClass::A0nH(order);
}

#ifdef USE_SD
  template class Amp4q2gH<double>;
#endif
#ifdef USE_DD
  template class Amp4q2gH<dd_real>;
#endif
#ifdef USE_QD
  template class Amp4q2gH<qd_real>;
#endif
#ifdef USE_VC
  template class Amp4q2gH<Vc::double_v>;
#endif
