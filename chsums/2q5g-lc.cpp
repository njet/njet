/*
* chsums/2q5g-lc.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include "2q5g.h"

template <typename T>
void Amp2q5g<T>::getfvpart1_lc(const int fv, LoopValue* fvpart)
{
  return getfvpart1_lc_(fv, fvpart);
}

template <typename T>
void Amp2q5g<T>::getfvpart1_lc(const int fv, LoopResult<T>* fvpart)
{
  return getfvpart1_lc_(fv, fvpart);
}

// -------- BEGIN -- automatically generated code -- BEGIN -------- //

template <typename T>
template <typename LT>
inline
void Amp2q5g<T>::getfvpart1_lc_(const int fv, LT* fvpart)
{
  fvSet(fv);
  {
    const int norder = norderL;

    if (norder >= 0) {
    // [1] order 120 primitives
    const LT P1234567 = AL(0,1,2,3,4,5,6);
    const LT P1234576 = AL(0,1,2,3,4,6,5);
    const LT P1234657 = AL(0,1,2,3,5,4,6);
    const LT P1234675 = AL(0,1,2,3,5,6,4);
    const LT P1234756 = AL(0,1,2,3,6,4,5);
    const LT P1234765 = AL(0,1,2,3,6,5,4);
    const LT P1235467 = AL(0,1,2,4,3,5,6);
    const LT P1235476 = AL(0,1,2,4,3,6,5);
    const LT P1235647 = AL(0,1,2,4,5,3,6);
    const LT P1235674 = AL(0,1,2,4,5,6,3);
    const LT P1235746 = AL(0,1,2,4,6,3,5);
    const LT P1235764 = AL(0,1,2,4,6,5,3);
    const LT P1236457 = AL(0,1,2,5,3,4,6);
    const LT P1236475 = AL(0,1,2,5,3,6,4);
    const LT P1236547 = AL(0,1,2,5,4,3,6);
    const LT P1236574 = AL(0,1,2,5,4,6,3);
    const LT P1236745 = AL(0,1,2,5,6,3,4);
    const LT P1236754 = AL(0,1,2,5,6,4,3);
    const LT P1237456 = AL(0,1,2,6,3,4,5);
    const LT P1237465 = AL(0,1,2,6,3,5,4);
    const LT P1237546 = AL(0,1,2,6,4,3,5);
    const LT P1237564 = AL(0,1,2,6,4,5,3);
    const LT P1237645 = AL(0,1,2,6,5,3,4);
    const LT P1237654 = AL(0,1,2,6,5,4,3);
    const LT P1243567 = AL(0,1,3,2,4,5,6);
    const LT P1243576 = AL(0,1,3,2,4,6,5);
    const LT P1243657 = AL(0,1,3,2,5,4,6);
    const LT P1243675 = AL(0,1,3,2,5,6,4);
    const LT P1243756 = AL(0,1,3,2,6,4,5);
    const LT P1243765 = AL(0,1,3,2,6,5,4);
    const LT P1245367 = AL(0,1,3,4,2,5,6);
    const LT P1245376 = AL(0,1,3,4,2,6,5);
    const LT P1245637 = AL(0,1,3,4,5,2,6);
    const LT P1245673 = AL(0,1,3,4,5,6,2);
    const LT P1245736 = AL(0,1,3,4,6,2,5);
    const LT P1245763 = AL(0,1,3,4,6,5,2);
    const LT P1246357 = AL(0,1,3,5,2,4,6);
    const LT P1246375 = AL(0,1,3,5,2,6,4);
    const LT P1246537 = AL(0,1,3,5,4,2,6);
    const LT P1246573 = AL(0,1,3,5,4,6,2);
    const LT P1246735 = AL(0,1,3,5,6,2,4);
    const LT P1246753 = AL(0,1,3,5,6,4,2);
    const LT P1247356 = AL(0,1,3,6,2,4,5);
    const LT P1247365 = AL(0,1,3,6,2,5,4);
    const LT P1247536 = AL(0,1,3,6,4,2,5);
    const LT P1247563 = AL(0,1,3,6,4,5,2);
    const LT P1247635 = AL(0,1,3,6,5,2,4);
    const LT P1247653 = AL(0,1,3,6,5,4,2);
    const LT P1253467 = AL(0,1,4,2,3,5,6);
    const LT P1253476 = AL(0,1,4,2,3,6,5);
    const LT P1253647 = AL(0,1,4,2,5,3,6);
    const LT P1253674 = AL(0,1,4,2,5,6,3);
    const LT P1253746 = AL(0,1,4,2,6,3,5);
    const LT P1253764 = AL(0,1,4,2,6,5,3);
    const LT P1254367 = AL(0,1,4,3,2,5,6);
    const LT P1254376 = AL(0,1,4,3,2,6,5);
    const LT P1254637 = AL(0,1,4,3,5,2,6);
    const LT P1254673 = AL(0,1,4,3,5,6,2);
    const LT P1254736 = AL(0,1,4,3,6,2,5);
    const LT P1254763 = AL(0,1,4,3,6,5,2);
    const LT P1256347 = AL(0,1,4,5,2,3,6);
    const LT P1256374 = AL(0,1,4,5,2,6,3);
    const LT P1256437 = AL(0,1,4,5,3,2,6);
    const LT P1256473 = AL(0,1,4,5,3,6,2);
    const LT P1256734 = AL(0,1,4,5,6,2,3);
    const LT P1256743 = AL(0,1,4,5,6,3,2);
    const LT P1257346 = AL(0,1,4,6,2,3,5);
    const LT P1257364 = AL(0,1,4,6,2,5,3);
    const LT P1257436 = AL(0,1,4,6,3,2,5);
    const LT P1257463 = AL(0,1,4,6,3,5,2);
    const LT P1257634 = AL(0,1,4,6,5,2,3);
    const LT P1257643 = AL(0,1,4,6,5,3,2);
    const LT P1263457 = AL(0,1,5,2,3,4,6);
    const LT P1263475 = AL(0,1,5,2,3,6,4);
    const LT P1263547 = AL(0,1,5,2,4,3,6);
    const LT P1263574 = AL(0,1,5,2,4,6,3);
    const LT P1263745 = AL(0,1,5,2,6,3,4);
    const LT P1263754 = AL(0,1,5,2,6,4,3);
    const LT P1264357 = AL(0,1,5,3,2,4,6);
    const LT P1264375 = AL(0,1,5,3,2,6,4);
    const LT P1264537 = AL(0,1,5,3,4,2,6);
    const LT P1264573 = AL(0,1,5,3,4,6,2);
    const LT P1264735 = AL(0,1,5,3,6,2,4);
    const LT P1264753 = AL(0,1,5,3,6,4,2);
    const LT P1265347 = AL(0,1,5,4,2,3,6);
    const LT P1265374 = AL(0,1,5,4,2,6,3);
    const LT P1265437 = AL(0,1,5,4,3,2,6);
    const LT P1265473 = AL(0,1,5,4,3,6,2);
    const LT P1265734 = AL(0,1,5,4,6,2,3);
    const LT P1265743 = AL(0,1,5,4,6,3,2);
    const LT P1267345 = AL(0,1,5,6,2,3,4);
    const LT P1267354 = AL(0,1,5,6,2,4,3);
    const LT P1267435 = AL(0,1,5,6,3,2,4);
    const LT P1267453 = AL(0,1,5,6,3,4,2);
    const LT P1267534 = AL(0,1,5,6,4,2,3);
    const LT P1267543 = AL(0,1,5,6,4,3,2);
    const LT P1273456 = AL(0,1,6,2,3,4,5);
    const LT P1273465 = AL(0,1,6,2,3,5,4);
    const LT P1273546 = AL(0,1,6,2,4,3,5);
    const LT P1273564 = AL(0,1,6,2,4,5,3);
    const LT P1273645 = AL(0,1,6,2,5,3,4);
    const LT P1273654 = AL(0,1,6,2,5,4,3);
    const LT P1274356 = AL(0,1,6,3,2,4,5);
    const LT P1274365 = AL(0,1,6,3,2,5,4);
    const LT P1274536 = AL(0,1,6,3,4,2,5);
    const LT P1274563 = AL(0,1,6,3,4,5,2);
    const LT P1274635 = AL(0,1,6,3,5,2,4);
    const LT P1274653 = AL(0,1,6,3,5,4,2);
    const LT P1275346 = AL(0,1,6,4,2,3,5);
    const LT P1275364 = AL(0,1,6,4,2,5,3);
    const LT P1275436 = AL(0,1,6,4,3,2,5);
    const LT P1275463 = AL(0,1,6,4,3,5,2);
    const LT P1275634 = AL(0,1,6,4,5,2,3);
    const LT P1275643 = AL(0,1,6,4,5,3,2);
    const LT P1276345 = AL(0,1,6,5,2,3,4);
    const LT P1276354 = AL(0,1,6,5,2,4,3);
    const LT P1276435 = AL(0,1,6,5,3,2,4);
    const LT P1276453 = AL(0,1,6,5,3,4,2);
    const LT P1276534 = AL(0,1,6,5,4,2,3);
    const LT P1276543 = AL(0,1,6,5,4,3,2);
    fvpart[0] = Nc*P1234567;
    fvpart[1] = Nc*P1234576;
    fvpart[2] = Nc*P1234657;
    fvpart[3] = Nc*P1234675;
    fvpart[4] = Nc*P1234756;
    fvpart[5] = Nc*P1234765;
    fvpart[6] = Nc*P1235467;
    fvpart[7] = Nc*P1235476;
    fvpart[8] = Nc*P1235647;
    fvpart[9] = Nc*P1235674;
    fvpart[10] = Nc*P1235746;
    fvpart[11] = Nc*P1235764;
    fvpart[12] = Nc*P1236457;
    fvpart[13] = Nc*P1236475;
    fvpart[14] = Nc*P1236547;
    fvpart[15] = Nc*P1236574;
    fvpart[16] = Nc*P1236745;
    fvpart[17] = Nc*P1236754;
    fvpart[18] = Nc*P1237456;
    fvpart[19] = Nc*P1237465;
    fvpart[20] = Nc*P1237546;
    fvpart[21] = Nc*P1237564;
    fvpart[22] = Nc*P1237645;
    fvpart[23] = Nc*P1237654;
    fvpart[24] = Nc*P1243567;
    fvpart[25] = Nc*P1243576;
    fvpart[26] = Nc*P1243657;
    fvpart[27] = Nc*P1243675;
    fvpart[28] = Nc*P1243756;
    fvpart[29] = Nc*P1243765;
    fvpart[30] = Nc*P1245367;
    fvpart[31] = Nc*P1245376;
    fvpart[32] = Nc*P1245637;
    fvpart[33] = Nc*P1245673;
    fvpart[34] = Nc*P1245736;
    fvpart[35] = Nc*P1245763;
    fvpart[36] = Nc*P1246357;
    fvpart[37] = Nc*P1246375;
    fvpart[38] = Nc*P1246537;
    fvpart[39] = Nc*P1246573;
    fvpart[40] = Nc*P1246735;
    fvpart[41] = Nc*P1246753;
    fvpart[42] = Nc*P1247356;
    fvpart[43] = Nc*P1247365;
    fvpart[44] = Nc*P1247536;
    fvpart[45] = Nc*P1247563;
    fvpart[46] = Nc*P1247635;
    fvpart[47] = Nc*P1247653;
    fvpart[48] = Nc*P1253467;
    fvpart[49] = Nc*P1253476;
    fvpart[50] = Nc*P1253647;
    fvpart[51] = Nc*P1253674;
    fvpart[52] = Nc*P1253746;
    fvpart[53] = Nc*P1253764;
    fvpart[54] = Nc*P1254367;
    fvpart[55] = Nc*P1254376;
    fvpart[56] = Nc*P1254637;
    fvpart[57] = Nc*P1254673;
    fvpart[58] = Nc*P1254736;
    fvpart[59] = Nc*P1254763;
    fvpart[60] = Nc*P1256347;
    fvpart[61] = Nc*P1256374;
    fvpart[62] = Nc*P1256437;
    fvpart[63] = Nc*P1256473;
    fvpart[64] = Nc*P1256734;
    fvpart[65] = Nc*P1256743;
    fvpart[66] = Nc*P1257346;
    fvpart[67] = Nc*P1257364;
    fvpart[68] = Nc*P1257436;
    fvpart[69] = Nc*P1257463;
    fvpart[70] = Nc*P1257634;
    fvpart[71] = Nc*P1257643;
    fvpart[72] = Nc*P1263457;
    fvpart[73] = Nc*P1263475;
    fvpart[74] = Nc*P1263547;
    fvpart[75] = Nc*P1263574;
    fvpart[76] = Nc*P1263745;
    fvpart[77] = Nc*P1263754;
    fvpart[78] = Nc*P1264357;
    fvpart[79] = Nc*P1264375;
    fvpart[80] = Nc*P1264537;
    fvpart[81] = Nc*P1264573;
    fvpart[82] = Nc*P1264735;
    fvpart[83] = Nc*P1264753;
    fvpart[84] = Nc*P1265347;
    fvpart[85] = Nc*P1265374;
    fvpart[86] = Nc*P1265437;
    fvpart[87] = Nc*P1265473;
    fvpart[88] = Nc*P1265734;
    fvpart[89] = Nc*P1265743;
    fvpart[90] = Nc*P1267345;
    fvpart[91] = Nc*P1267354;
    fvpart[92] = Nc*P1267435;
    fvpart[93] = Nc*P1267453;
    fvpart[94] = Nc*P1267534;
    fvpart[95] = Nc*P1267543;
    fvpart[96] = Nc*P1273456;
    fvpart[97] = Nc*P1273465;
    fvpart[98] = Nc*P1273546;
    fvpart[99] = Nc*P1273564;
    fvpart[100] = Nc*P1273645;
    fvpart[101] = Nc*P1273654;
    fvpart[102] = Nc*P1274356;
    fvpart[103] = Nc*P1274365;
    fvpart[104] = Nc*P1274536;
    fvpart[105] = Nc*P1274563;
    fvpart[106] = Nc*P1274635;
    fvpart[107] = Nc*P1274653;
    fvpart[108] = Nc*P1275346;
    fvpart[109] = Nc*P1275364;
    fvpart[110] = Nc*P1275436;
    fvpart[111] = Nc*P1275463;
    fvpart[112] = Nc*P1275634;
    fvpart[113] = Nc*P1275643;
    fvpart[114] = Nc*P1276345;
    fvpart[115] = Nc*P1276354;
    fvpart[116] = Nc*P1276435;
    fvpart[117] = Nc*P1276453;
    fvpart[118] = Nc*P1276534;
    fvpart[119] = Nc*P1276543;

    if (norder >= 1) {
#ifndef NDEBUG
    // [0, -1] order 600 primitives
    const LT P1324567 = AL(0,2,1,3,4,5,6);
    const LT P1324576 = AL(0,2,1,3,4,6,5);
    const LT P1324657 = AL(0,2,1,3,5,4,6);
    const LT P1324675 = AL(0,2,1,3,5,6,4);
    const LT P1324756 = AL(0,2,1,3,6,4,5);
    const LT P1324765 = AL(0,2,1,3,6,5,4);
    const LT P1325467 = AL(0,2,1,4,3,5,6);
    const LT P1325476 = AL(0,2,1,4,3,6,5);
    const LT P1325647 = AL(0,2,1,4,5,3,6);
    const LT P1325674 = AL(0,2,1,4,5,6,3);
    const LT P1325746 = AL(0,2,1,4,6,3,5);
    const LT P1325764 = AL(0,2,1,4,6,5,3);
    const LT P1326457 = AL(0,2,1,5,3,4,6);
    const LT P1326475 = AL(0,2,1,5,3,6,4);
    const LT P1326547 = AL(0,2,1,5,4,3,6);
    const LT P1326574 = AL(0,2,1,5,4,6,3);
    const LT P1326745 = AL(0,2,1,5,6,3,4);
    const LT P1326754 = AL(0,2,1,5,6,4,3);
    const LT P1327456 = AL(0,2,1,6,3,4,5);
    const LT P1327465 = AL(0,2,1,6,3,5,4);
    const LT P1327546 = AL(0,2,1,6,4,3,5);
    const LT P1327564 = AL(0,2,1,6,4,5,3);
    const LT P1327645 = AL(0,2,1,6,5,3,4);
    const LT P1327654 = AL(0,2,1,6,5,4,3);
    const LT P1342567 = AL(0,2,3,1,4,5,6);
    const LT P1342576 = AL(0,2,3,1,4,6,5);
    const LT P1342657 = AL(0,2,3,1,5,4,6);
    const LT P1342675 = AL(0,2,3,1,5,6,4);
    const LT P1342756 = AL(0,2,3,1,6,4,5);
    const LT P1342765 = AL(0,2,3,1,6,5,4);
    const LT P1345267 = AL(0,2,3,4,1,5,6);
    const LT P1345276 = AL(0,2,3,4,1,6,5);
    const LT P1345627 = AL(0,2,3,4,5,1,6);
    const LT P1345672 = AL(0,2,3,4,5,6,1);
    const LT P1345726 = AL(0,2,3,4,6,1,5);
    const LT P1345762 = AL(0,2,3,4,6,5,1);
    const LT P1346257 = AL(0,2,3,5,1,4,6);
    const LT P1346275 = AL(0,2,3,5,1,6,4);
    const LT P1346527 = AL(0,2,3,5,4,1,6);
    const LT P1346572 = AL(0,2,3,5,4,6,1);
    const LT P1346725 = AL(0,2,3,5,6,1,4);
    const LT P1346752 = AL(0,2,3,5,6,4,1);
    const LT P1347256 = AL(0,2,3,6,1,4,5);
    const LT P1347265 = AL(0,2,3,6,1,5,4);
    const LT P1347526 = AL(0,2,3,6,4,1,5);
    const LT P1347562 = AL(0,2,3,6,4,5,1);
    const LT P1347625 = AL(0,2,3,6,5,1,4);
    const LT P1347652 = AL(0,2,3,6,5,4,1);
    const LT P1352467 = AL(0,2,4,1,3,5,6);
    const LT P1352476 = AL(0,2,4,1,3,6,5);
    const LT P1352647 = AL(0,2,4,1,5,3,6);
    const LT P1352674 = AL(0,2,4,1,5,6,3);
    const LT P1352746 = AL(0,2,4,1,6,3,5);
    const LT P1352764 = AL(0,2,4,1,6,5,3);
    const LT P1354267 = AL(0,2,4,3,1,5,6);
    const LT P1354276 = AL(0,2,4,3,1,6,5);
    const LT P1354627 = AL(0,2,4,3,5,1,6);
    const LT P1354672 = AL(0,2,4,3,5,6,1);
    const LT P1354726 = AL(0,2,4,3,6,1,5);
    const LT P1354762 = AL(0,2,4,3,6,5,1);
    const LT P1356247 = AL(0,2,4,5,1,3,6);
    const LT P1356274 = AL(0,2,4,5,1,6,3);
    const LT P1356427 = AL(0,2,4,5,3,1,6);
    const LT P1356472 = AL(0,2,4,5,3,6,1);
    const LT P1356724 = AL(0,2,4,5,6,1,3);
    const LT P1356742 = AL(0,2,4,5,6,3,1);
    const LT P1357246 = AL(0,2,4,6,1,3,5);
    const LT P1357264 = AL(0,2,4,6,1,5,3);
    const LT P1357426 = AL(0,2,4,6,3,1,5);
    const LT P1357462 = AL(0,2,4,6,3,5,1);
    const LT P1357624 = AL(0,2,4,6,5,1,3);
    const LT P1357642 = AL(0,2,4,6,5,3,1);
    const LT P1362457 = AL(0,2,5,1,3,4,6);
    const LT P1362475 = AL(0,2,5,1,3,6,4);
    const LT P1362547 = AL(0,2,5,1,4,3,6);
    const LT P1362574 = AL(0,2,5,1,4,6,3);
    const LT P1362745 = AL(0,2,5,1,6,3,4);
    const LT P1362754 = AL(0,2,5,1,6,4,3);
    const LT P1364257 = AL(0,2,5,3,1,4,6);
    const LT P1364275 = AL(0,2,5,3,1,6,4);
    const LT P1364527 = AL(0,2,5,3,4,1,6);
    const LT P1364572 = AL(0,2,5,3,4,6,1);
    const LT P1364725 = AL(0,2,5,3,6,1,4);
    const LT P1364752 = AL(0,2,5,3,6,4,1);
    const LT P1365247 = AL(0,2,5,4,1,3,6);
    const LT P1365274 = AL(0,2,5,4,1,6,3);
    const LT P1365427 = AL(0,2,5,4,3,1,6);
    const LT P1365472 = AL(0,2,5,4,3,6,1);
    const LT P1365724 = AL(0,2,5,4,6,1,3);
    const LT P1365742 = AL(0,2,5,4,6,3,1);
    const LT P1367245 = AL(0,2,5,6,1,3,4);
    const LT P1367254 = AL(0,2,5,6,1,4,3);
    const LT P1367425 = AL(0,2,5,6,3,1,4);
    const LT P1367452 = AL(0,2,5,6,3,4,1);
    const LT P1367524 = AL(0,2,5,6,4,1,3);
    const LT P1367542 = AL(0,2,5,6,4,3,1);
    const LT P1372456 = AL(0,2,6,1,3,4,5);
    const LT P1372465 = AL(0,2,6,1,3,5,4);
    const LT P1372546 = AL(0,2,6,1,4,3,5);
    const LT P1372564 = AL(0,2,6,1,4,5,3);
    const LT P1372645 = AL(0,2,6,1,5,3,4);
    const LT P1372654 = AL(0,2,6,1,5,4,3);
    const LT P1374256 = AL(0,2,6,3,1,4,5);
    const LT P1374265 = AL(0,2,6,3,1,5,4);
    const LT P1374526 = AL(0,2,6,3,4,1,5);
    const LT P1374562 = AL(0,2,6,3,4,5,1);
    const LT P1374625 = AL(0,2,6,3,5,1,4);
    const LT P1374652 = AL(0,2,6,3,5,4,1);
    const LT P1375246 = AL(0,2,6,4,1,3,5);
    const LT P1375264 = AL(0,2,6,4,1,5,3);
    const LT P1375426 = AL(0,2,6,4,3,1,5);
    const LT P1375462 = AL(0,2,6,4,3,5,1);
    const LT P1375624 = AL(0,2,6,4,5,1,3);
    const LT P1375642 = AL(0,2,6,4,5,3,1);
    const LT P1376245 = AL(0,2,6,5,1,3,4);
    const LT P1376254 = AL(0,2,6,5,1,4,3);
    const LT P1376425 = AL(0,2,6,5,3,1,4);
    const LT P1376452 = AL(0,2,6,5,3,4,1);
    const LT P1376524 = AL(0,2,6,5,4,1,3);
    const LT P1376542 = AL(0,2,6,5,4,3,1);
    const LT P1423567 = AL(0,3,1,2,4,5,6);
    const LT P1423576 = AL(0,3,1,2,4,6,5);
    const LT P1423657 = AL(0,3,1,2,5,4,6);
    const LT P1423675 = AL(0,3,1,2,5,6,4);
    const LT P1423756 = AL(0,3,1,2,6,4,5);
    const LT P1423765 = AL(0,3,1,2,6,5,4);
    const LT P1425367 = AL(0,3,1,4,2,5,6);
    const LT P1425376 = AL(0,3,1,4,2,6,5);
    const LT P1425637 = AL(0,3,1,4,5,2,6);
    const LT P1425673 = AL(0,3,1,4,5,6,2);
    const LT P1425736 = AL(0,3,1,4,6,2,5);
    const LT P1425763 = AL(0,3,1,4,6,5,2);
    const LT P1426357 = AL(0,3,1,5,2,4,6);
    const LT P1426375 = AL(0,3,1,5,2,6,4);
    const LT P1426537 = AL(0,3,1,5,4,2,6);
    const LT P1426573 = AL(0,3,1,5,4,6,2);
    const LT P1426735 = AL(0,3,1,5,6,2,4);
    const LT P1426753 = AL(0,3,1,5,6,4,2);
    const LT P1427356 = AL(0,3,1,6,2,4,5);
    const LT P1427365 = AL(0,3,1,6,2,5,4);
    const LT P1427536 = AL(0,3,1,6,4,2,5);
    const LT P1427563 = AL(0,3,1,6,4,5,2);
    const LT P1427635 = AL(0,3,1,6,5,2,4);
    const LT P1427653 = AL(0,3,1,6,5,4,2);
    const LT P1432567 = AL(0,3,2,1,4,5,6);
    const LT P1432576 = AL(0,3,2,1,4,6,5);
    const LT P1432657 = AL(0,3,2,1,5,4,6);
    const LT P1432675 = AL(0,3,2,1,5,6,4);
    const LT P1432756 = AL(0,3,2,1,6,4,5);
    const LT P1432765 = AL(0,3,2,1,6,5,4);
    const LT P1435267 = AL(0,3,2,4,1,5,6);
    const LT P1435276 = AL(0,3,2,4,1,6,5);
    const LT P1435627 = AL(0,3,2,4,5,1,6);
    const LT P1435672 = AL(0,3,2,4,5,6,1);
    const LT P1435726 = AL(0,3,2,4,6,1,5);
    const LT P1435762 = AL(0,3,2,4,6,5,1);
    const LT P1436257 = AL(0,3,2,5,1,4,6);
    const LT P1436275 = AL(0,3,2,5,1,6,4);
    const LT P1436527 = AL(0,3,2,5,4,1,6);
    const LT P1436572 = AL(0,3,2,5,4,6,1);
    const LT P1436725 = AL(0,3,2,5,6,1,4);
    const LT P1436752 = AL(0,3,2,5,6,4,1);
    const LT P1437256 = AL(0,3,2,6,1,4,5);
    const LT P1437265 = AL(0,3,2,6,1,5,4);
    const LT P1437526 = AL(0,3,2,6,4,1,5);
    const LT P1437562 = AL(0,3,2,6,4,5,1);
    const LT P1437625 = AL(0,3,2,6,5,1,4);
    const LT P1437652 = AL(0,3,2,6,5,4,1);
    const LT P1452367 = AL(0,3,4,1,2,5,6);
    const LT P1452376 = AL(0,3,4,1,2,6,5);
    const LT P1452637 = AL(0,3,4,1,5,2,6);
    const LT P1452673 = AL(0,3,4,1,5,6,2);
    const LT P1452736 = AL(0,3,4,1,6,2,5);
    const LT P1452763 = AL(0,3,4,1,6,5,2);
    const LT P1453267 = AL(0,3,4,2,1,5,6);
    const LT P1453276 = AL(0,3,4,2,1,6,5);
    const LT P1453627 = AL(0,3,4,2,5,1,6);
    const LT P1453672 = AL(0,3,4,2,5,6,1);
    const LT P1453726 = AL(0,3,4,2,6,1,5);
    const LT P1453762 = AL(0,3,4,2,6,5,1);
    const LT P1456237 = AL(0,3,4,5,1,2,6);
    const LT P1456273 = AL(0,3,4,5,1,6,2);
    const LT P1456327 = AL(0,3,4,5,2,1,6);
    const LT P1456372 = AL(0,3,4,5,2,6,1);
    const LT P1456723 = AL(0,3,4,5,6,1,2);
    const LT P1456732 = AL(0,3,4,5,6,2,1);
    const LT P1457236 = AL(0,3,4,6,1,2,5);
    const LT P1457263 = AL(0,3,4,6,1,5,2);
    const LT P1457326 = AL(0,3,4,6,2,1,5);
    const LT P1457362 = AL(0,3,4,6,2,5,1);
    const LT P1457623 = AL(0,3,4,6,5,1,2);
    const LT P1457632 = AL(0,3,4,6,5,2,1);
    const LT P1462357 = AL(0,3,5,1,2,4,6);
    const LT P1462375 = AL(0,3,5,1,2,6,4);
    const LT P1462537 = AL(0,3,5,1,4,2,6);
    const LT P1462573 = AL(0,3,5,1,4,6,2);
    const LT P1462735 = AL(0,3,5,1,6,2,4);
    const LT P1462753 = AL(0,3,5,1,6,4,2);
    const LT P1463257 = AL(0,3,5,2,1,4,6);
    const LT P1463275 = AL(0,3,5,2,1,6,4);
    const LT P1463527 = AL(0,3,5,2,4,1,6);
    const LT P1463572 = AL(0,3,5,2,4,6,1);
    const LT P1463725 = AL(0,3,5,2,6,1,4);
    const LT P1463752 = AL(0,3,5,2,6,4,1);
    const LT P1465237 = AL(0,3,5,4,1,2,6);
    const LT P1465273 = AL(0,3,5,4,1,6,2);
    const LT P1465327 = AL(0,3,5,4,2,1,6);
    const LT P1465372 = AL(0,3,5,4,2,6,1);
    const LT P1465723 = AL(0,3,5,4,6,1,2);
    const LT P1465732 = AL(0,3,5,4,6,2,1);
    const LT P1467235 = AL(0,3,5,6,1,2,4);
    const LT P1467253 = AL(0,3,5,6,1,4,2);
    const LT P1467325 = AL(0,3,5,6,2,1,4);
    const LT P1467352 = AL(0,3,5,6,2,4,1);
    const LT P1467523 = AL(0,3,5,6,4,1,2);
    const LT P1467532 = AL(0,3,5,6,4,2,1);
    const LT P1472356 = AL(0,3,6,1,2,4,5);
    const LT P1472365 = AL(0,3,6,1,2,5,4);
    const LT P1472536 = AL(0,3,6,1,4,2,5);
    const LT P1472563 = AL(0,3,6,1,4,5,2);
    const LT P1472635 = AL(0,3,6,1,5,2,4);
    const LT P1472653 = AL(0,3,6,1,5,4,2);
    const LT P1473256 = AL(0,3,6,2,1,4,5);
    const LT P1473265 = AL(0,3,6,2,1,5,4);
    const LT P1473526 = AL(0,3,6,2,4,1,5);
    const LT P1473562 = AL(0,3,6,2,4,5,1);
    const LT P1473625 = AL(0,3,6,2,5,1,4);
    const LT P1473652 = AL(0,3,6,2,5,4,1);
    const LT P1475236 = AL(0,3,6,4,1,2,5);
    const LT P1475263 = AL(0,3,6,4,1,5,2);
    const LT P1475326 = AL(0,3,6,4,2,1,5);
    const LT P1475362 = AL(0,3,6,4,2,5,1);
    const LT P1475623 = AL(0,3,6,4,5,1,2);
    const LT P1475632 = AL(0,3,6,4,5,2,1);
    const LT P1476235 = AL(0,3,6,5,1,2,4);
    const LT P1476253 = AL(0,3,6,5,1,4,2);
    const LT P1476325 = AL(0,3,6,5,2,1,4);
    const LT P1476352 = AL(0,3,6,5,2,4,1);
    const LT P1476523 = AL(0,3,6,5,4,1,2);
    const LT P1476532 = AL(0,3,6,5,4,2,1);
    const LT P1523467 = AL(0,4,1,2,3,5,6);
    const LT P1523476 = AL(0,4,1,2,3,6,5);
    const LT P1523647 = AL(0,4,1,2,5,3,6);
    const LT P1523674 = AL(0,4,1,2,5,6,3);
    const LT P1523746 = AL(0,4,1,2,6,3,5);
    const LT P1523764 = AL(0,4,1,2,6,5,3);
    const LT P1524367 = AL(0,4,1,3,2,5,6);
    const LT P1524376 = AL(0,4,1,3,2,6,5);
    const LT P1524637 = AL(0,4,1,3,5,2,6);
    const LT P1524673 = AL(0,4,1,3,5,6,2);
    const LT P1524736 = AL(0,4,1,3,6,2,5);
    const LT P1524763 = AL(0,4,1,3,6,5,2);
    const LT P1526347 = AL(0,4,1,5,2,3,6);
    const LT P1526374 = AL(0,4,1,5,2,6,3);
    const LT P1526437 = AL(0,4,1,5,3,2,6);
    const LT P1526473 = AL(0,4,1,5,3,6,2);
    const LT P1526734 = AL(0,4,1,5,6,2,3);
    const LT P1526743 = AL(0,4,1,5,6,3,2);
    const LT P1527346 = AL(0,4,1,6,2,3,5);
    const LT P1527364 = AL(0,4,1,6,2,5,3);
    const LT P1527436 = AL(0,4,1,6,3,2,5);
    const LT P1527463 = AL(0,4,1,6,3,5,2);
    const LT P1527634 = AL(0,4,1,6,5,2,3);
    const LT P1527643 = AL(0,4,1,6,5,3,2);
    const LT P1532467 = AL(0,4,2,1,3,5,6);
    const LT P1532476 = AL(0,4,2,1,3,6,5);
    const LT P1532647 = AL(0,4,2,1,5,3,6);
    const LT P1532674 = AL(0,4,2,1,5,6,3);
    const LT P1532746 = AL(0,4,2,1,6,3,5);
    const LT P1532764 = AL(0,4,2,1,6,5,3);
    const LT P1534267 = AL(0,4,2,3,1,5,6);
    const LT P1534276 = AL(0,4,2,3,1,6,5);
    const LT P1534627 = AL(0,4,2,3,5,1,6);
    const LT P1534672 = AL(0,4,2,3,5,6,1);
    const LT P1534726 = AL(0,4,2,3,6,1,5);
    const LT P1534762 = AL(0,4,2,3,6,5,1);
    const LT P1536247 = AL(0,4,2,5,1,3,6);
    const LT P1536274 = AL(0,4,2,5,1,6,3);
    const LT P1536427 = AL(0,4,2,5,3,1,6);
    const LT P1536472 = AL(0,4,2,5,3,6,1);
    const LT P1536724 = AL(0,4,2,5,6,1,3);
    const LT P1536742 = AL(0,4,2,5,6,3,1);
    const LT P1537246 = AL(0,4,2,6,1,3,5);
    const LT P1537264 = AL(0,4,2,6,1,5,3);
    const LT P1537426 = AL(0,4,2,6,3,1,5);
    const LT P1537462 = AL(0,4,2,6,3,5,1);
    const LT P1537624 = AL(0,4,2,6,5,1,3);
    const LT P1537642 = AL(0,4,2,6,5,3,1);
    const LT P1542367 = AL(0,4,3,1,2,5,6);
    const LT P1542376 = AL(0,4,3,1,2,6,5);
    const LT P1542637 = AL(0,4,3,1,5,2,6);
    const LT P1542673 = AL(0,4,3,1,5,6,2);
    const LT P1542736 = AL(0,4,3,1,6,2,5);
    const LT P1542763 = AL(0,4,3,1,6,5,2);
    const LT P1543267 = AL(0,4,3,2,1,5,6);
    const LT P1543276 = AL(0,4,3,2,1,6,5);
    const LT P1543627 = AL(0,4,3,2,5,1,6);
    const LT P1543672 = AL(0,4,3,2,5,6,1);
    const LT P1543726 = AL(0,4,3,2,6,1,5);
    const LT P1543762 = AL(0,4,3,2,6,5,1);
    const LT P1546237 = AL(0,4,3,5,1,2,6);
    const LT P1546273 = AL(0,4,3,5,1,6,2);
    const LT P1546327 = AL(0,4,3,5,2,1,6);
    const LT P1546372 = AL(0,4,3,5,2,6,1);
    const LT P1546723 = AL(0,4,3,5,6,1,2);
    const LT P1546732 = AL(0,4,3,5,6,2,1);
    const LT P1547236 = AL(0,4,3,6,1,2,5);
    const LT P1547263 = AL(0,4,3,6,1,5,2);
    const LT P1547326 = AL(0,4,3,6,2,1,5);
    const LT P1547362 = AL(0,4,3,6,2,5,1);
    const LT P1547623 = AL(0,4,3,6,5,1,2);
    const LT P1547632 = AL(0,4,3,6,5,2,1);
    const LT P1562347 = AL(0,4,5,1,2,3,6);
    const LT P1562374 = AL(0,4,5,1,2,6,3);
    const LT P1562437 = AL(0,4,5,1,3,2,6);
    const LT P1562473 = AL(0,4,5,1,3,6,2);
    const LT P1562734 = AL(0,4,5,1,6,2,3);
    const LT P1562743 = AL(0,4,5,1,6,3,2);
    const LT P1563247 = AL(0,4,5,2,1,3,6);
    const LT P1563274 = AL(0,4,5,2,1,6,3);
    const LT P1563427 = AL(0,4,5,2,3,1,6);
    const LT P1563472 = AL(0,4,5,2,3,6,1);
    const LT P1563724 = AL(0,4,5,2,6,1,3);
    const LT P1563742 = AL(0,4,5,2,6,3,1);
    const LT P1564237 = AL(0,4,5,3,1,2,6);
    const LT P1564273 = AL(0,4,5,3,1,6,2);
    const LT P1564327 = AL(0,4,5,3,2,1,6);
    const LT P1564372 = AL(0,4,5,3,2,6,1);
    const LT P1564723 = AL(0,4,5,3,6,1,2);
    const LT P1564732 = AL(0,4,5,3,6,2,1);
    const LT P1567234 = AL(0,4,5,6,1,2,3);
    const LT P1567243 = AL(0,4,5,6,1,3,2);
    const LT P1567324 = AL(0,4,5,6,2,1,3);
    const LT P1567342 = AL(0,4,5,6,2,3,1);
    const LT P1567423 = AL(0,4,5,6,3,1,2);
    const LT P1567432 = AL(0,4,5,6,3,2,1);
    const LT P1572346 = AL(0,4,6,1,2,3,5);
    const LT P1572364 = AL(0,4,6,1,2,5,3);
    const LT P1572436 = AL(0,4,6,1,3,2,5);
    const LT P1572463 = AL(0,4,6,1,3,5,2);
    const LT P1572634 = AL(0,4,6,1,5,2,3);
    const LT P1572643 = AL(0,4,6,1,5,3,2);
    const LT P1573246 = AL(0,4,6,2,1,3,5);
    const LT P1573264 = AL(0,4,6,2,1,5,3);
    const LT P1573426 = AL(0,4,6,2,3,1,5);
    const LT P1573462 = AL(0,4,6,2,3,5,1);
    const LT P1573624 = AL(0,4,6,2,5,1,3);
    const LT P1573642 = AL(0,4,6,2,5,3,1);
    const LT P1574236 = AL(0,4,6,3,1,2,5);
    const LT P1574263 = AL(0,4,6,3,1,5,2);
    const LT P1574326 = AL(0,4,6,3,2,1,5);
    const LT P1574362 = AL(0,4,6,3,2,5,1);
    const LT P1574623 = AL(0,4,6,3,5,1,2);
    const LT P1574632 = AL(0,4,6,3,5,2,1);
    const LT P1576234 = AL(0,4,6,5,1,2,3);
    const LT P1576243 = AL(0,4,6,5,1,3,2);
    const LT P1576324 = AL(0,4,6,5,2,1,3);
    const LT P1576342 = AL(0,4,6,5,2,3,1);
    const LT P1576423 = AL(0,4,6,5,3,1,2);
    const LT P1576432 = AL(0,4,6,5,3,2,1);
    const LT P1623457 = AL(0,5,1,2,3,4,6);
    const LT P1623475 = AL(0,5,1,2,3,6,4);
    const LT P1623547 = AL(0,5,1,2,4,3,6);
    const LT P1623574 = AL(0,5,1,2,4,6,3);
    const LT P1623745 = AL(0,5,1,2,6,3,4);
    const LT P1623754 = AL(0,5,1,2,6,4,3);
    const LT P1624357 = AL(0,5,1,3,2,4,6);
    const LT P1624375 = AL(0,5,1,3,2,6,4);
    const LT P1624537 = AL(0,5,1,3,4,2,6);
    const LT P1624573 = AL(0,5,1,3,4,6,2);
    const LT P1624735 = AL(0,5,1,3,6,2,4);
    const LT P1624753 = AL(0,5,1,3,6,4,2);
    const LT P1625347 = AL(0,5,1,4,2,3,6);
    const LT P1625374 = AL(0,5,1,4,2,6,3);
    const LT P1625437 = AL(0,5,1,4,3,2,6);
    const LT P1625473 = AL(0,5,1,4,3,6,2);
    const LT P1625734 = AL(0,5,1,4,6,2,3);
    const LT P1625743 = AL(0,5,1,4,6,3,2);
    const LT P1627345 = AL(0,5,1,6,2,3,4);
    const LT P1627354 = AL(0,5,1,6,2,4,3);
    const LT P1627435 = AL(0,5,1,6,3,2,4);
    const LT P1627453 = AL(0,5,1,6,3,4,2);
    const LT P1627534 = AL(0,5,1,6,4,2,3);
    const LT P1627543 = AL(0,5,1,6,4,3,2);
    const LT P1632457 = AL(0,5,2,1,3,4,6);
    const LT P1632475 = AL(0,5,2,1,3,6,4);
    const LT P1632547 = AL(0,5,2,1,4,3,6);
    const LT P1632574 = AL(0,5,2,1,4,6,3);
    const LT P1632745 = AL(0,5,2,1,6,3,4);
    const LT P1632754 = AL(0,5,2,1,6,4,3);
    const LT P1634257 = AL(0,5,2,3,1,4,6);
    const LT P1634275 = AL(0,5,2,3,1,6,4);
    const LT P1634527 = AL(0,5,2,3,4,1,6);
    const LT P1634572 = AL(0,5,2,3,4,6,1);
    const LT P1634725 = AL(0,5,2,3,6,1,4);
    const LT P1634752 = AL(0,5,2,3,6,4,1);
    const LT P1635247 = AL(0,5,2,4,1,3,6);
    const LT P1635274 = AL(0,5,2,4,1,6,3);
    const LT P1635427 = AL(0,5,2,4,3,1,6);
    const LT P1635472 = AL(0,5,2,4,3,6,1);
    const LT P1635724 = AL(0,5,2,4,6,1,3);
    const LT P1635742 = AL(0,5,2,4,6,3,1);
    const LT P1637245 = AL(0,5,2,6,1,3,4);
    const LT P1637254 = AL(0,5,2,6,1,4,3);
    const LT P1637425 = AL(0,5,2,6,3,1,4);
    const LT P1637452 = AL(0,5,2,6,3,4,1);
    const LT P1637524 = AL(0,5,2,6,4,1,3);
    const LT P1637542 = AL(0,5,2,6,4,3,1);
    const LT P1642357 = AL(0,5,3,1,2,4,6);
    const LT P1642375 = AL(0,5,3,1,2,6,4);
    const LT P1642537 = AL(0,5,3,1,4,2,6);
    const LT P1642573 = AL(0,5,3,1,4,6,2);
    const LT P1642735 = AL(0,5,3,1,6,2,4);
    const LT P1642753 = AL(0,5,3,1,6,4,2);
    const LT P1643257 = AL(0,5,3,2,1,4,6);
    const LT P1643275 = AL(0,5,3,2,1,6,4);
    const LT P1643527 = AL(0,5,3,2,4,1,6);
    const LT P1643572 = AL(0,5,3,2,4,6,1);
    const LT P1643725 = AL(0,5,3,2,6,1,4);
    const LT P1643752 = AL(0,5,3,2,6,4,1);
    const LT P1645237 = AL(0,5,3,4,1,2,6);
    const LT P1645273 = AL(0,5,3,4,1,6,2);
    const LT P1645327 = AL(0,5,3,4,2,1,6);
    const LT P1645372 = AL(0,5,3,4,2,6,1);
    const LT P1645723 = AL(0,5,3,4,6,1,2);
    const LT P1645732 = AL(0,5,3,4,6,2,1);
    const LT P1647235 = AL(0,5,3,6,1,2,4);
    const LT P1647253 = AL(0,5,3,6,1,4,2);
    const LT P1647325 = AL(0,5,3,6,2,1,4);
    const LT P1647352 = AL(0,5,3,6,2,4,1);
    const LT P1647523 = AL(0,5,3,6,4,1,2);
    const LT P1647532 = AL(0,5,3,6,4,2,1);
    const LT P1652347 = AL(0,5,4,1,2,3,6);
    const LT P1652374 = AL(0,5,4,1,2,6,3);
    const LT P1652437 = AL(0,5,4,1,3,2,6);
    const LT P1652473 = AL(0,5,4,1,3,6,2);
    const LT P1652734 = AL(0,5,4,1,6,2,3);
    const LT P1652743 = AL(0,5,4,1,6,3,2);
    const LT P1653247 = AL(0,5,4,2,1,3,6);
    const LT P1653274 = AL(0,5,4,2,1,6,3);
    const LT P1653427 = AL(0,5,4,2,3,1,6);
    const LT P1653472 = AL(0,5,4,2,3,6,1);
    const LT P1653724 = AL(0,5,4,2,6,1,3);
    const LT P1653742 = AL(0,5,4,2,6,3,1);
    const LT P1654237 = AL(0,5,4,3,1,2,6);
    const LT P1654273 = AL(0,5,4,3,1,6,2);
    const LT P1654327 = AL(0,5,4,3,2,1,6);
    const LT P1654372 = AL(0,5,4,3,2,6,1);
    const LT P1654723 = AL(0,5,4,3,6,1,2);
    const LT P1654732 = AL(0,5,4,3,6,2,1);
    const LT P1657234 = AL(0,5,4,6,1,2,3);
    const LT P1657243 = AL(0,5,4,6,1,3,2);
    const LT P1657324 = AL(0,5,4,6,2,1,3);
    const LT P1657342 = AL(0,5,4,6,2,3,1);
    const LT P1657423 = AL(0,5,4,6,3,1,2);
    const LT P1657432 = AL(0,5,4,6,3,2,1);
    const LT P1672345 = AL(0,5,6,1,2,3,4);
    const LT P1672354 = AL(0,5,6,1,2,4,3);
    const LT P1672435 = AL(0,5,6,1,3,2,4);
    const LT P1672453 = AL(0,5,6,1,3,4,2);
    const LT P1672534 = AL(0,5,6,1,4,2,3);
    const LT P1672543 = AL(0,5,6,1,4,3,2);
    const LT P1673245 = AL(0,5,6,2,1,3,4);
    const LT P1673254 = AL(0,5,6,2,1,4,3);
    const LT P1673425 = AL(0,5,6,2,3,1,4);
    const LT P1673452 = AL(0,5,6,2,3,4,1);
    const LT P1673524 = AL(0,5,6,2,4,1,3);
    const LT P1673542 = AL(0,5,6,2,4,3,1);
    const LT P1674235 = AL(0,5,6,3,1,2,4);
    const LT P1674253 = AL(0,5,6,3,1,4,2);
    const LT P1674325 = AL(0,5,6,3,2,1,4);
    const LT P1674352 = AL(0,5,6,3,2,4,1);
    const LT P1674523 = AL(0,5,6,3,4,1,2);
    const LT P1674532 = AL(0,5,6,3,4,2,1);
    const LT P1675234 = AL(0,5,6,4,1,2,3);
    const LT P1675243 = AL(0,5,6,4,1,3,2);
    const LT P1675324 = AL(0,5,6,4,2,1,3);
    const LT P1675342 = AL(0,5,6,4,2,3,1);
    const LT P1675423 = AL(0,5,6,4,3,1,2);
    const LT P1675432 = AL(0,5,6,4,3,2,1);
    const LT P1723456 = AL(0,6,1,2,3,4,5);
    const LT P1723465 = AL(0,6,1,2,3,5,4);
    const LT P1723546 = AL(0,6,1,2,4,3,5);
    const LT P1723564 = AL(0,6,1,2,4,5,3);
    const LT P1723645 = AL(0,6,1,2,5,3,4);
    const LT P1723654 = AL(0,6,1,2,5,4,3);
    const LT P1724356 = AL(0,6,1,3,2,4,5);
    const LT P1724365 = AL(0,6,1,3,2,5,4);
    const LT P1724536 = AL(0,6,1,3,4,2,5);
    const LT P1724563 = AL(0,6,1,3,4,5,2);
    const LT P1724635 = AL(0,6,1,3,5,2,4);
    const LT P1724653 = AL(0,6,1,3,5,4,2);
    const LT P1725346 = AL(0,6,1,4,2,3,5);
    const LT P1725364 = AL(0,6,1,4,2,5,3);
    const LT P1725436 = AL(0,6,1,4,3,2,5);
    const LT P1725463 = AL(0,6,1,4,3,5,2);
    const LT P1725634 = AL(0,6,1,4,5,2,3);
    const LT P1725643 = AL(0,6,1,4,5,3,2);
    const LT P1726345 = AL(0,6,1,5,2,3,4);
    const LT P1726354 = AL(0,6,1,5,2,4,3);
    const LT P1726435 = AL(0,6,1,5,3,2,4);
    const LT P1726453 = AL(0,6,1,5,3,4,2);
    const LT P1726534 = AL(0,6,1,5,4,2,3);
    const LT P1726543 = AL(0,6,1,5,4,3,2);
    const LT P1732456 = AL(0,6,2,1,3,4,5);
    const LT P1732465 = AL(0,6,2,1,3,5,4);
    const LT P1732546 = AL(0,6,2,1,4,3,5);
    const LT P1732564 = AL(0,6,2,1,4,5,3);
    const LT P1732645 = AL(0,6,2,1,5,3,4);
    const LT P1732654 = AL(0,6,2,1,5,4,3);
    const LT P1734256 = AL(0,6,2,3,1,4,5);
    const LT P1734265 = AL(0,6,2,3,1,5,4);
    const LT P1734526 = AL(0,6,2,3,4,1,5);
    const LT P1734562 = AL(0,6,2,3,4,5,1);
    const LT P1734625 = AL(0,6,2,3,5,1,4);
    const LT P1734652 = AL(0,6,2,3,5,4,1);
    const LT P1735246 = AL(0,6,2,4,1,3,5);
    const LT P1735264 = AL(0,6,2,4,1,5,3);
    const LT P1735426 = AL(0,6,2,4,3,1,5);
    const LT P1735462 = AL(0,6,2,4,3,5,1);
    const LT P1735624 = AL(0,6,2,4,5,1,3);
    const LT P1735642 = AL(0,6,2,4,5,3,1);
    const LT P1736245 = AL(0,6,2,5,1,3,4);
    const LT P1736254 = AL(0,6,2,5,1,4,3);
    const LT P1736425 = AL(0,6,2,5,3,1,4);
    const LT P1736452 = AL(0,6,2,5,3,4,1);
    const LT P1736524 = AL(0,6,2,5,4,1,3);
    const LT P1736542 = AL(0,6,2,5,4,3,1);
    const LT P1742356 = AL(0,6,3,1,2,4,5);
    const LT P1742365 = AL(0,6,3,1,2,5,4);
    const LT P1742536 = AL(0,6,3,1,4,2,5);
    const LT P1742563 = AL(0,6,3,1,4,5,2);
    const LT P1742635 = AL(0,6,3,1,5,2,4);
    const LT P1742653 = AL(0,6,3,1,5,4,2);
    const LT P1743256 = AL(0,6,3,2,1,4,5);
    const LT P1743265 = AL(0,6,3,2,1,5,4);
    const LT P1743526 = AL(0,6,3,2,4,1,5);
    const LT P1743562 = AL(0,6,3,2,4,5,1);
    const LT P1743625 = AL(0,6,3,2,5,1,4);
    const LT P1743652 = AL(0,6,3,2,5,4,1);
    const LT P1745236 = AL(0,6,3,4,1,2,5);
    const LT P1745263 = AL(0,6,3,4,1,5,2);
    const LT P1745326 = AL(0,6,3,4,2,1,5);
    const LT P1745362 = AL(0,6,3,4,2,5,1);
    const LT P1745623 = AL(0,6,3,4,5,1,2);
    const LT P1745632 = AL(0,6,3,4,5,2,1);
    const LT P1746235 = AL(0,6,3,5,1,2,4);
    const LT P1746253 = AL(0,6,3,5,1,4,2);
    const LT P1746325 = AL(0,6,3,5,2,1,4);
    const LT P1746352 = AL(0,6,3,5,2,4,1);
    const LT P1746523 = AL(0,6,3,5,4,1,2);
    const LT P1746532 = AL(0,6,3,5,4,2,1);
    const LT P1752346 = AL(0,6,4,1,2,3,5);
    const LT P1752364 = AL(0,6,4,1,2,5,3);
    const LT P1752436 = AL(0,6,4,1,3,2,5);
    const LT P1752463 = AL(0,6,4,1,3,5,2);
    const LT P1752634 = AL(0,6,4,1,5,2,3);
    const LT P1752643 = AL(0,6,4,1,5,3,2);
    const LT P1753246 = AL(0,6,4,2,1,3,5);
    const LT P1753264 = AL(0,6,4,2,1,5,3);
    const LT P1753426 = AL(0,6,4,2,3,1,5);
    const LT P1753462 = AL(0,6,4,2,3,5,1);
    const LT P1753624 = AL(0,6,4,2,5,1,3);
    const LT P1753642 = AL(0,6,4,2,5,3,1);
    const LT P1754236 = AL(0,6,4,3,1,2,5);
    const LT P1754263 = AL(0,6,4,3,1,5,2);
    const LT P1754326 = AL(0,6,4,3,2,1,5);
    const LT P1754362 = AL(0,6,4,3,2,5,1);
    const LT P1754623 = AL(0,6,4,3,5,1,2);
    const LT P1754632 = AL(0,6,4,3,5,2,1);
    const LT P1756234 = AL(0,6,4,5,1,2,3);
    const LT P1756243 = AL(0,6,4,5,1,3,2);
    const LT P1756324 = AL(0,6,4,5,2,1,3);
    const LT P1756342 = AL(0,6,4,5,2,3,1);
    const LT P1756423 = AL(0,6,4,5,3,1,2);
    const LT P1756432 = AL(0,6,4,5,3,2,1);
    const LT P1762345 = AL(0,6,5,1,2,3,4);
    const LT P1762354 = AL(0,6,5,1,2,4,3);
    const LT P1762435 = AL(0,6,5,1,3,2,4);
    const LT P1762453 = AL(0,6,5,1,3,4,2);
    const LT P1762534 = AL(0,6,5,1,4,2,3);
    const LT P1762543 = AL(0,6,5,1,4,3,2);
    const LT P1763245 = AL(0,6,5,2,1,3,4);
    const LT P1763254 = AL(0,6,5,2,1,4,3);
    const LT P1763425 = AL(0,6,5,2,3,1,4);
    const LT P1763452 = AL(0,6,5,2,3,4,1);
    const LT P1763524 = AL(0,6,5,2,4,1,3);
    const LT P1763542 = AL(0,6,5,2,4,3,1);
    const LT P1764235 = AL(0,6,5,3,1,2,4);
    const LT P1764253 = AL(0,6,5,3,1,4,2);
    const LT P1764325 = AL(0,6,5,3,2,1,4);
    const LT P1764352 = AL(0,6,5,3,2,4,1);
    const LT P1764523 = AL(0,6,5,3,4,1,2);
    const LT P1764532 = AL(0,6,5,3,4,2,1);
    const LT P1765234 = AL(0,6,5,4,1,2,3);
    const LT P1765243 = AL(0,6,5,4,1,3,2);
    const LT P1765324 = AL(0,6,5,4,2,1,3);
    const LT P1765342 = AL(0,6,5,4,2,3,1);
    const LT P1765423 = AL(0,6,5,4,3,1,2);
    const LT P1765432 = AL(0,6,5,4,3,2,1);
    fvpart[0] += P1765432/Nc;
    fvpart[1] += P1675432/Nc;
    fvpart[2] += P1756432/Nc;
    fvpart[3] += P1576432/Nc;
    fvpart[4] += P1657432/Nc;
    fvpart[5] += P1567432/Nc;
    fvpart[6] += P1764532/Nc;
    fvpart[7] += P1674532/Nc;
    fvpart[8] += P1746532/Nc;
    fvpart[9] += P1476532/Nc;
    fvpart[10] += P1647532/Nc;
    fvpart[11] += P1467532/Nc;
    fvpart[12] += P1754632/Nc;
    fvpart[13] += P1574632/Nc;
    fvpart[14] += P1745632/Nc;
    fvpart[15] += P1475632/Nc;
    fvpart[16] += P1547632/Nc;
    fvpart[17] += P1457632/Nc;
    fvpart[18] += P1654732/Nc;
    fvpart[19] += P1564732/Nc;
    fvpart[20] += P1645732/Nc;
    fvpart[21] += P1465732/Nc;
    fvpart[22] += P1546732/Nc;
    fvpart[23] += P1456732/Nc;
    fvpart[24] += P1765342/Nc;
    fvpart[25] += P1675342/Nc;
    fvpart[26] += P1756342/Nc;
    fvpart[27] += P1576342/Nc;
    fvpart[28] += P1657342/Nc;
    fvpart[29] += P1567342/Nc;
    fvpart[30] += P1763542/Nc;
    fvpart[31] += P1673542/Nc;
    fvpart[32] += P1736542/Nc;
    fvpart[33] += P1376542/Nc;
    fvpart[34] += P1637542/Nc;
    fvpart[35] += P1367542/Nc;
    fvpart[36] += P1753642/Nc;
    fvpart[37] += P1573642/Nc;
    fvpart[38] += P1735642/Nc;
    fvpart[39] += P1375642/Nc;
    fvpart[40] += P1537642/Nc;
    fvpart[41] += P1357642/Nc;
    fvpart[42] += P1653742/Nc;
    fvpart[43] += P1563742/Nc;
    fvpart[44] += P1635742/Nc;
    fvpart[45] += P1365742/Nc;
    fvpart[46] += P1536742/Nc;
    fvpart[47] += P1356742/Nc;
    fvpart[48] += P1764352/Nc;
    fvpart[49] += P1674352/Nc;
    fvpart[50] += P1746352/Nc;
    fvpart[51] += P1476352/Nc;
    fvpart[52] += P1647352/Nc;
    fvpart[53] += P1467352/Nc;
    fvpart[54] += P1763452/Nc;
    fvpart[55] += P1673452/Nc;
    fvpart[56] += P1736452/Nc;
    fvpart[57] += P1376452/Nc;
    fvpart[58] += P1637452/Nc;
    fvpart[59] += P1367452/Nc;
    fvpart[60] += P1743652/Nc;
    fvpart[61] += P1473652/Nc;
    fvpart[62] += P1734652/Nc;
    fvpart[63] += P1374652/Nc;
    fvpart[64] += P1437652/Nc;
    fvpart[65] += P1347652/Nc;
    fvpart[66] += P1643752/Nc;
    fvpart[67] += P1463752/Nc;
    fvpart[68] += P1634752/Nc;
    fvpart[69] += P1364752/Nc;
    fvpart[70] += P1436752/Nc;
    fvpart[71] += P1346752/Nc;
    fvpart[72] += P1754362/Nc;
    fvpart[73] += P1574362/Nc;
    fvpart[74] += P1745362/Nc;
    fvpart[75] += P1475362/Nc;
    fvpart[76] += P1547362/Nc;
    fvpart[77] += P1457362/Nc;
    fvpart[78] += P1753462/Nc;
    fvpart[79] += P1573462/Nc;
    fvpart[80] += P1735462/Nc;
    fvpart[81] += P1375462/Nc;
    fvpart[82] += P1537462/Nc;
    fvpart[83] += P1357462/Nc;
    fvpart[84] += P1743562/Nc;
    fvpart[85] += P1473562/Nc;
    fvpart[86] += P1734562/Nc;
    fvpart[87] += P1374562/Nc;
    fvpart[88] += P1437562/Nc;
    fvpart[89] += P1347562/Nc;
    fvpart[90] += P1543762/Nc;
    fvpart[91] += P1453762/Nc;
    fvpart[92] += P1534762/Nc;
    fvpart[93] += P1354762/Nc;
    fvpart[94] += P1435762/Nc;
    fvpart[95] += P1345762/Nc;
    fvpart[96] += P1654372/Nc;
    fvpart[97] += P1564372/Nc;
    fvpart[98] += P1645372/Nc;
    fvpart[99] += P1465372/Nc;
    fvpart[100] += P1546372/Nc;
    fvpart[101] += P1456372/Nc;
    fvpart[102] += P1653472/Nc;
    fvpart[103] += P1563472/Nc;
    fvpart[104] += P1635472/Nc;
    fvpart[105] += P1365472/Nc;
    fvpart[106] += P1536472/Nc;
    fvpart[107] += P1356472/Nc;
    fvpart[108] += P1643572/Nc;
    fvpart[109] += P1463572/Nc;
    fvpart[110] += P1634572/Nc;
    fvpart[111] += P1364572/Nc;
    fvpart[112] += P1436572/Nc;
    fvpart[113] += P1346572/Nc;
    fvpart[114] += P1543672/Nc;
    fvpart[115] += P1453672/Nc;
    fvpart[116] += P1534672/Nc;
    fvpart[117] += P1354672/Nc;
    fvpart[118] += P1435672/Nc;
    fvpart[119] += P1345672/Nc;
    fvpart[120] = -P1234756-P1247563-P1256347-P1263475-P1275634-P1324756-P1342756-P1347256-P1347526
    -P1347562-P1427563-P1472563-P1475263-P1475623-P1475632-P1526347-P1562347-P1563247
    -P1563427-P1563472-P1623475-P1632475-P1634275-P1634725-P1634752-P1725634-P1752634
    -P1756234-P1756324-P1756342;
    fvpart[121] = -P1234765-P1247653-P1253476-P1265347-P1276534-P1324765-P1342765-P1347265-P1347625
    -P1347652-P1427653-P1472653-P1476253-P1476523-P1476532-P1523476-P1532476-P1534276
    -P1534726-P1534762-P1625347-P1652347-P1653247-P1653427-P1653472-P1726534-P1762534
    -P1765234-P1765324-P1765342;
    fvpart[122] = -P1237564-P1243756-P1256437-P1264375-P1275643-P1327564-P1372564-P1375264-P1375624
    -P1375642-P1423756-P1432756-P1437256-P1437526-P1437562-P1526437-P1562437-P1564237
    -P1564327-P1564372-P1624375-P1642375-P1643275-P1643725-P1643752-P1725643-P1752643
    -P1756243-P1756423-P1756432;
    fvpart[123] = -P1237654-P1243765-P1254376-P1265437-P1276543-P1327654-P1372654-P1376254-P1376524
    -P1376542-P1423765-P1432765-P1437265-P1437625-P1437652-P1524376-P1542376-P1543276
    -P1543726-P1543762-P1625437-P1652437-P1654237-P1654327-P1654372-P1726543-P1762543
    -P1765243-P1765423-P1765432;
    fvpart[124] = -P1234567-P1245673-P1256734-P1267345-P1273456-P1324567-P1342567-P1345267-P1345627
    -P1345672-P1425673-P1452673-P1456273-P1456723-P1456732-P1526734-P1562734-P1567234
    -P1567324-P1567342-P1627345-P1672345-P1673245-P1673425-P1673452-P1723456-P1732456
    -P1734256-P1734526-P1734562;
    fvpart[125] = -P1234657-P1246573-P1257346-P1265734-P1273465-P1324657-P1342657-P1346257-P1346527
    -P1346572-P1426573-P1462573-P1465273-P1465723-P1465732-P1527346-P1572346-P1573246
    -P1573426-P1573462-P1625734-P1652734-P1657234-P1657324-P1657342-P1723465-P1732465
    -P1734265-P1734625-P1734652;
    fvpart[126] = -P1235674-P1243567-P1256743-P1267435-P1274356-P1325674-P1352674-P1356274-P1356724
    -P1356742-P1423567-P1432567-P1435267-P1435627-P1435672-P1526743-P1562743-P1567243
    -P1567423-P1567432-P1627435-P1672435-P1674235-P1674325-P1674352-P1724356-P1742356
    -P1743256-P1743526-P1743562;
    fvpart[127] = -P1236574-P1243657-P1257436-P1265743-P1274365-P1326574-P1362574-P1365274-P1365724
    -P1365742-P1423657-P1432657-P1436257-P1436527-P1436572-P1527436-P1572436-P1574236
    -P1574326-P1574362-P1625743-P1652743-P1657243-P1657423-P1657432-P1724365-P1742365
    -P1743265-P1743625-P1743652;
    fvpart[128] = -P1234675-P1246753-P1253467-P1267534-P1275346-P1324675-P1342675-P1346275-P1346725
    -P1346752-P1426753-P1462753-P1467253-P1467523-P1467532-P1523467-P1532467-P1534267
    -P1534627-P1534672-P1627534-P1672534-P1675234-P1675324-P1675342-P1725346-P1752346
    -P1753246-P1753426-P1753462;
    fvpart[129] = -P1236754-P1243675-P1254367-P1267543-P1275436-P1326754-P1362754-P1367254-P1367524
    -P1367542-P1423675-P1432675-P1436275-P1436725-P1436752-P1524367-P1542367-P1543267
    -P1543627-P1543672-P1627543-P1672543-P1675243-P1675423-P1675432-P1725436-P1752436
    -P1754236-P1754326-P1754362;
    fvpart[130] = -P1234576-P1245763-P1257634-P1263457-P1276345-P1324576-P1342576-P1345276-P1345726
    -P1345762-P1425763-P1452763-P1457263-P1457623-P1457632-P1527634-P1572634-P1576234
    -P1576324-P1576342-P1623457-P1632457-P1634257-P1634527-P1634572-P1726345-P1762345
    -P1763245-P1763425-P1763452;
    fvpart[131] = -P1235764-P1243576-P1257643-P1264357-P1276435-P1325764-P1352764-P1357264-P1357624
    -P1357642-P1423576-P1432576-P1435276-P1435726-P1435762-P1527643-P1572643-P1576243
    -P1576423-P1576432-P1624357-P1642357-P1643257-P1643527-P1643572-P1726435-P1762435
    -P1764235-P1764325-P1764352;
    fvpart[132] = -P1235746-P1246357-P1257463-P1263574-P1274635-P1325746-P1352746-P1357246-P1357426
    -P1357462-P1426357-P1462357-P1463257-P1463527-P1463572-P1527463-P1572463-P1574263
    -P1574623-P1574632-P1623574-P1632574-P1635274-P1635724-P1635742-P1724635-P1742635
    -P1746235-P1746325-P1746352;
    fvpart[133] = -P1237465-P1246537-P1253746-P1265374-P1274653-P1327465-P1372465-P1374265-P1374625
    -P1374652-P1426537-P1462537-P1465237-P1465327-P1465372-P1523746-P1532746-P1537246
    -P1537426-P1537462-P1625374-P1652374-P1653274-P1653724-P1653742-P1724653-P1742653
    -P1746253-P1746523-P1746532;
    fvpart[134] = -P1237645-P1245376-P1253764-P1264537-P1276453-P1327645-P1372645-P1376245-P1376425
    -P1376452-P1425376-P1452376-P1453276-P1453726-P1453762-P1523764-P1532764-P1537264
    -P1537624-P1537642-P1624537-P1642537-P1645237-P1645327-P1645372-P1726453-P1762453
    -P1764253-P1764523-P1764532;
    fvpart[135] = -P1235467-P1246735-P1254673-P1267354-P1273546-P1325467-P1352467-P1354267-P1354627
    -P1354672-P1426735-P1462735-P1467235-P1467325-P1467352-P1524673-P1542673-P1546273
    -P1546723-P1546732-P1627354-P1672354-P1673254-P1673524-P1673542-P1723546-P1732546
    -P1735246-P1735426-P1735462;
    fvpart[136] = -P1235647-P1247356-P1256473-P1264735-P1273564-P1325647-P1352647-P1356247-P1356427
    -P1356472-P1427356-P1472356-P1473256-P1473526-P1473562-P1526473-P1562473-P1564273
    -P1564723-P1564732-P1624735-P1642735-P1647235-P1647325-P1647352-P1723564-P1732564
    -P1735264-P1735624-P1735642;
    fvpart[137] = -P1236475-P1247536-P1253647-P1264753-P1275364-P1326475-P1362475-P1364275-P1364725
    -P1364752-P1427536-P1472536-P1475236-P1475326-P1475362-P1523647-P1532647-P1536247
    -P1536427-P1536472-P1624753-P1642753-P1647253-P1647523-P1647532-P1725364-P1752364
    -P1753264-P1753624-P1753642;
    fvpart[138] = -P1236745-P1245367-P1253674-P1267453-P1274536-P1326745-P1362745-P1367245-P1367425
    -P1367452-P1425367-P1452367-P1453267-P1453627-P1453672-P1523674-P1532674-P1536274
    -P1536724-P1536742-P1627453-P1672453-P1674253-P1674523-P1674532-P1724536-P1742536
    -P1745236-P1745326-P1745362;
    fvpart[139] = -P1235476-P1247635-P1254763-P1263547-P1276354-P1325476-P1352476-P1354276-P1354726
    -P1354762-P1427635-P1472635-P1476235-P1476325-P1476352-P1524763-P1542763-P1547263
    -P1547623-P1547632-P1623547-P1632547-P1635247-P1635427-P1635472-P1726354-P1762354
    -P1763254-P1763524-P1763542;
    fvpart[140] = -P1237456-P1245637-P1256374-P1263745-P1274563-P1327456-P1372456-P1374256-P1374526
    -P1374562-P1425637-P1452637-P1456237-P1456327-P1456372-P1526374-P1562374-P1563274
    -P1563724-P1563742-P1623745-P1632745-P1637245-P1637425-P1637452-P1724563-P1742563
    -P1745263-P1745623-P1745632;
    fvpart[141] = -P1237546-P1246375-P1254637-P1263754-P1275463-P1327546-P1372546-P1375246-P1375426
    -P1375462-P1426375-P1462375-P1463275-P1463725-P1463752-P1524637-P1542637-P1546237
    -P1546327-P1546372-P1623754-P1632754-P1637254-P1637524-P1637542-P1725463-P1752463
    -P1754263-P1754623-P1754632;
    fvpart[142] = -P1236457-P1245736-P1257364-P1264573-P1273645-P1326457-P1362457-P1364257-P1364527
    -P1364572-P1425736-P1452736-P1457236-P1457326-P1457362-P1527364-P1572364-P1573264
    -P1573624-P1573642-P1624573-P1642573-P1645273-P1645723-P1645732-P1723645-P1732645
    -P1736245-P1736425-P1736452;
    fvpart[143] = -P1236547-P1247365-P1254736-P1265473-P1273654-P1326547-P1362547-P1365247-P1365427
    -P1365472-P1427365-P1472365-P1473265-P1473625-P1473652-P1524736-P1542736-P1547236
    -P1547326-P1547362-P1625473-P1652473-P1654273-P1654723-P1654732-P1723654-P1732654
    -P1736254-P1736524-P1736542;
    fvpart[144] = -P1243567-P1243675-P1243756-P1245367-P1245637-P1245673-P1246375-P1246735-P1246753
    -P1247356-P1247536-P1247563-P1254367-P1254637-P1254673-P1256437-P1256473-P1256743
    -P1264375-P1264735-P1264753-P1267435-P1267453-P1267543-P1274356-P1274536-P1274563
    -P1275436-P1275463-P1275643-P1524367-P1524637-P1524673-P1526437-P1526473-P1526743
    -P1562437-P1562473-P1562743-P1567243-P1624375-P1624735-P1624753-P1627435-P1627453
    -P1627543-P1672435-P1672453-P1672543-P1675243-P1724356-P1724536-P1724563-P1725436
    -P1725463-P1725643-P1752436-P1752463-P1752643-P1756243;
    fvpart[145] = -P1243576-P1243657-P1243765-P1245376-P1245736-P1245763-P1246357-P1246537-P1246573
    -P1247365-P1247635-P1247653-P1254376-P1254736-P1254763-P1257436-P1257463-P1257643
    -P1264357-P1264537-P1264573-P1265437-P1265473-P1265743-P1274365-P1274635-P1274653
    -P1276435-P1276453-P1276543-P1524376-P1524736-P1524763-P1527436-P1527463-P1527643
    -P1572436-P1572463-P1572643-P1576243-P1624357-P1624537-P1624573-P1625437-P1625473
    -P1625743-P1652437-P1652473-P1652743-P1657243-P1724365-P1724635-P1724653-P1726435
    -P1726453-P1726543-P1762435-P1762453-P1762543-P1765243;
    fvpart[146] = -P1245367-P1245637-P1245673-P1246537-P1246573-P1246753-P1253467-P1253674-P1253746
    -P1254367-P1254637-P1254673-P1256374-P1256734-P1256743-P1257346-P1257436-P1257463
    -P1265374-P1265734-P1265743-P1267453-P1267534-P1267543-P1274536-P1274563-P1274653
    -P1275346-P1275436-P1275463-P1425367-P1425637-P1425673-P1426537-P1426573-P1426753
    -P1462537-P1462573-P1462753-P1467253-P1625374-P1625734-P1625743-P1627453-P1627534
    -P1627543-P1672453-P1672534-P1672543-P1674253-P1724536-P1724563-P1724653-P1725346
    -P1725436-P1725463-P1742536-P1742563-P1742653-P1746253;
    fvpart[147] = -P1245376-P1245736-P1245763-P1247536-P1247563-P1247653-P1253476-P1253647-P1253764
    -P1254376-P1254736-P1254763-P1256347-P1256437-P1256473-P1257364-P1257634-P1257643
    -P1264537-P1264573-P1264753-P1265347-P1265437-P1265473-P1275364-P1275634-P1275643
    -P1276453-P1276534-P1276543-P1425376-P1425736-P1425763-P1427536-P1427563-P1427653
    -P1472536-P1472563-P1472653-P1476253-P1624537-P1624573-P1624753-P1625347-P1625437
    -P1625473-P1642537-P1642573-P1642753-P1647253-P1725364-P1725634-P1725643-P1726453
    -P1726534-P1726543-P1762453-P1762534-P1762543-P1764253;
    fvpart[148] = -P1245637-P1245673-P1245763-P1246357-P1246537-P1246573-P1256374-P1256734-P1256743
    -P1257463-P1257634-P1257643-P1263457-P1263574-P1263745-P1264357-P1264537-P1264573
    -P1265374-P1265734-P1265743-P1267345-P1267435-P1267453-P1274563-P1274635-P1274653
    -P1276345-P1276435-P1276453-P1425637-P1425673-P1425763-P1426357-P1426537-P1426573
    -P1452637-P1452673-P1452763-P1457263-P1526374-P1526734-P1526743-P1527463-P1527634
    -P1527643-P1572463-P1572634-P1572643-P1574263-P1724563-P1724635-P1724653-P1726345
    -P1726435-P1726453-P1742563-P1742635-P1742653-P1745263;
    fvpart[149] = -P1246375-P1246735-P1246753-P1247563-P1247635-P1247653-P1254637-P1254673-P1254763
    -P1256347-P1256437-P1256473-P1263475-P1263547-P1263754-P1264375-P1264735-P1264753
    -P1265347-P1265437-P1265473-P1267354-P1267534-P1267543-P1275463-P1275634-P1275643
    -P1276354-P1276534-P1276543-P1426375-P1426735-P1426753-P1427563-P1427635-P1427653
    -P1472563-P1472635-P1472653-P1475263-P1524637-P1524673-P1524763-P1526347-P1526437
    -P1526473-P1542637-P1542673-P1542763-P1547263-P1725463-P1725634-P1725643-P1726354
    -P1726534-P1726543-P1752463-P1752634-P1752643-P1754263;
    fvpart[150] = -P1245673-P1245736-P1245763-P1247356-P1247536-P1247563-P1256473-P1256734-P1256743
    -P1257364-P1257634-P1257643-P1264573-P1264735-P1264753-P1267345-P1267435-P1267453
    -P1273456-P1273564-P1273645-P1274356-P1274536-P1274563-P1275364-P1275634-P1275643
    -P1276345-P1276435-P1276453-P1425673-P1425736-P1425763-P1427356-P1427536-P1427563
    -P1452673-P1452736-P1452763-P1456273-P1526473-P1526734-P1526743-P1527364-P1527634
    -P1527643-P1562473-P1562734-P1562743-P1564273-P1624573-P1624735-P1624753-P1627345
    -P1627435-P1627453-P1642573-P1642735-P1642753-P1645273;
    fvpart[151] = -P1246573-P1246735-P1246753-P1247365-P1247635-P1247653-P1254673-P1254736-P1254763
    -P1257346-P1257436-P1257463-P1265473-P1265734-P1265743-P1267354-P1267534-P1267543
    -P1273465-P1273546-P1273654-P1274365-P1274635-P1274653-P1275346-P1275436-P1275463
    -P1276354-P1276534-P1276543-P1426573-P1426735-P1426753-P1427365-P1427635-P1427653
    -P1462573-P1462735-P1462753-P1465273-P1524673-P1524736-P1524763-P1527346-P1527436
    -P1527463-P1542673-P1542736-P1542763-P1546273-P1625473-P1625734-P1625743-P1627354
    -P1627534-P1627543-P1652473-P1652734-P1652743-P1654273;
    fvpart[152] = -P1234567-P1234675-P1234756-P1235467-P1235647-P1235674-P1236475-P1236745-P1236754
    -P1237456-P1237546-P1237564-P1253467-P1253647-P1253674-P1256347-P1256374-P1256734
    -P1263475-P1263745-P1263754-P1267345-P1267354-P1267534-P1273456-P1273546-P1273564
    -P1275346-P1275364-P1275634-P1523467-P1523647-P1523674-P1526347-P1526374-P1526734
    -P1562347-P1562374-P1562734-P1567234-P1623475-P1623745-P1623754-P1627345-P1627354
    -P1627534-P1672345-P1672354-P1672534-P1675234-P1723456-P1723546-P1723564-P1725346
    -P1725364-P1725634-P1752346-P1752364-P1752634-P1756234;
    fvpart[153] = -P1234576-P1234657-P1234765-P1235476-P1235746-P1235764-P1236457-P1236547-P1236574
    -P1237465-P1237645-P1237654-P1253476-P1253746-P1253764-P1257346-P1257364-P1257634
    -P1263457-P1263547-P1263574-P1265347-P1265374-P1265734-P1273465-P1273645-P1273654
    -P1276345-P1276354-P1276534-P1523476-P1523746-P1523764-P1527346-P1527364-P1527634
    -P1572346-P1572364-P1572634-P1576234-P1623457-P1623547-P1623574-P1625347-P1625374
    -P1625734-P1652347-P1652374-P1652734-P1657234-P1723465-P1723645-P1723654-P1726345
    -P1726354-P1726534-P1762345-P1762354-P1762534-P1765234;
    fvpart[154] = -P1235467-P1235647-P1235674-P1236547-P1236574-P1236754-P1253467-P1253647-P1253674
    -P1254367-P1254673-P1254736-P1256473-P1256734-P1256743-P1257346-P1257364-P1257436
    -P1265473-P1265734-P1265743-P1267354-P1267534-P1267543-P1273546-P1273564-P1273654
    -P1275346-P1275364-P1275436-P1325467-P1325647-P1325674-P1326547-P1326574-P1326754
    -P1362547-P1362574-P1362754-P1367254-P1625473-P1625734-P1625743-P1627354-P1627534
    -P1627543-P1672354-P1672534-P1672543-P1673254-P1723546-P1723564-P1723654-P1725346
    -P1725364-P1725436-P1732546-P1732564-P1732654-P1736254;
    fvpart[155] = -P1235476-P1235746-P1235764-P1237546-P1237564-P1237654-P1253476-P1253746-P1253764
    -P1254376-P1254637-P1254763-P1256347-P1256374-P1256437-P1257463-P1257634-P1257643
    -P1263547-P1263574-P1263754-P1265347-P1265374-P1265437-P1275463-P1275634-P1275643
    -P1276354-P1276534-P1276543-P1325476-P1325746-P1325764-P1327546-P1327564-P1327654
    -P1372546-P1372564-P1372654-P1376254-P1623547-P1623574-P1623754-P1625347-P1625374
    -P1625437-P1632547-P1632574-P1632754-P1637254-P1725463-P1725634-P1725643-P1726354
    -P1726534-P1726543-P1762354-P1762534-P1762543-P1763254;
    fvpart[156] = -P1235647-P1235674-P1235764-P1236457-P1236547-P1236574-P1256473-P1256734-P1256743
    -P1257364-P1257634-P1257643-P1263457-P1263547-P1263574-P1264357-P1264573-P1264735
    -P1265473-P1265734-P1265743-P1267345-P1267354-P1267435-P1273564-P1273645-P1273654
    -P1276345-P1276354-P1276435-P1325647-P1325674-P1325764-P1326457-P1326547-P1326574
    -P1352647-P1352674-P1352764-P1357264-P1526473-P1526734-P1526743-P1527364-P1527634
    -P1527643-P1572364-P1572634-P1572643-P1573264-P1723564-P1723645-P1723654-P1726345
    -P1726354-P1726435-P1732564-P1732645-P1732654-P1735264;
    fvpart[157] = -P1236475-P1236745-P1236754-P1237564-P1237645-P1237654-P1253647-P1253674-P1253764
    -P1256347-P1256374-P1256437-P1263475-P1263745-P1263754-P1264375-P1264537-P1264753
    -P1265347-P1265374-P1265437-P1267453-P1267534-P1267543-P1275364-P1275634-P1275643
    -P1276453-P1276534-P1276543-P1326475-P1326745-P1326754-P1327564-P1327645-P1327654
    -P1372564-P1372645-P1372654-P1375264-P1523647-P1523674-P1523764-P1526347-P1526374
    -P1526437-P1532647-P1532674-P1532764-P1537264-P1725364-P1725634-P1725643-P1726453
    -P1726534-P1726543-P1752364-P1752634-P1752643-P1753264;
    fvpart[158] = -P1235674-P1235746-P1235764-P1237456-P1237546-P1237564-P1256374-P1256734-P1256743
    -P1257463-P1257634-P1257643-P1263574-P1263745-P1263754-P1267345-P1267354-P1267435
    -P1273456-P1273546-P1273564-P1274356-P1274563-P1274635-P1275463-P1275634-P1275643
    -P1276345-P1276354-P1276435-P1325674-P1325746-P1325764-P1327456-P1327546-P1327564
    -P1352674-P1352746-P1352764-P1356274-P1526374-P1526734-P1526743-P1527463-P1527634
    -P1527643-P1562374-P1562734-P1562743-P1563274-P1623574-P1623745-P1623754-P1627345
    -P1627354-P1627435-P1632574-P1632745-P1632754-P1635274;
    fvpart[159] = -P1236574-P1236745-P1236754-P1237465-P1237645-P1237654-P1253674-P1253746-P1253764
    -P1257346-P1257364-P1257436-P1265374-P1265734-P1265743-P1267453-P1267534-P1267543
    -P1273465-P1273645-P1273654-P1274365-P1274536-P1274653-P1275346-P1275364-P1275436
    -P1276453-P1276534-P1276543-P1326574-P1326745-P1326754-P1327465-P1327645-P1327654
    -P1362574-P1362745-P1362754-P1365274-P1523674-P1523746-P1523764-P1527346-P1527364
    -P1527436-P1532674-P1532746-P1532764-P1536274-P1625374-P1625734-P1625743-P1627453
    -P1627534-P1627543-P1652374-P1652734-P1652743-P1653274;
    fvpart[160] = -P1234567-P1234657-P1234675-P1235467-P1235674-P1235746-P1236574-P1236745-P1236754
    -P1237456-P1237465-P1237546-P1243567-P1243657-P1243675-P1246357-P1246375-P1246735
    -P1263574-P1263745-P1263754-P1267345-P1267354-P1267435-P1273456-P1273465-P1273546
    -P1274356-P1274365-P1274635-P1423567-P1423657-P1423675-P1426357-P1426375-P1426735
    -P1462357-P1462375-P1462735-P1467235-P1623574-P1623745-P1623754-P1627345-P1627354
    -P1627435-P1672345-P1672354-P1672435-P1674235-P1723456-P1723465-P1723546-P1724356
    -P1724365-P1724635-P1742356-P1742365-P1742635-P1746235;
    fvpart[161] = -P1234576-P1234756-P1234765-P1235476-P1235647-P1235764-P1236457-P1236475-P1236547
    -P1237564-P1237645-P1237654-P1243576-P1243756-P1243765-P1247356-P1247365-P1247635
    -P1263457-P1263475-P1263547-P1264357-P1264375-P1264735-P1273564-P1273645-P1273654
    -P1276345-P1276354-P1276435-P1423576-P1423756-P1423765-P1427356-P1427365-P1427635
    -P1472356-P1472365-P1472635-P1476235-P1623457-P1623475-P1623547-P1624357-P1624375
    -P1624735-P1642357-P1642375-P1642735-P1647235-P1723564-P1723645-P1723654-P1726345
    -P1726354-P1726435-P1762345-P1762354-P1762435-P1764235;
    fvpart[162] = -P1234567-P1234657-P1234675-P1236457-P1236475-P1236745-P1243567-P1243657-P1243675
    -P1245367-P1245673-P1245736-P1246573-P1246735-P1246753-P1247356-P1247365-P1247536
    -P1264573-P1264735-P1264753-P1267345-P1267435-P1267453-P1273456-P1273465-P1273645
    -P1274356-P1274365-P1274536-P1324567-P1324657-P1324675-P1326457-P1326475-P1326745
    -P1362457-P1362475-P1362745-P1367245-P1624573-P1624735-P1624753-P1627345-P1627435
    -P1627453-P1672345-P1672435-P1672453-P1673245-P1723456-P1723465-P1723645-P1724356
    -P1724365-P1724536-P1732456-P1732465-P1732645-P1736245;
    fvpart[163] = -P1234576-P1234756-P1234765-P1237456-P1237465-P1237645-P1243576-P1243756-P1243765
    -P1245376-P1245637-P1245763-P1246357-P1246375-P1246537-P1247563-P1247635-P1247653
    -P1263457-P1263475-P1263745-P1264357-P1264375-P1264537-P1274563-P1274635-P1274653
    -P1276345-P1276435-P1276453-P1324576-P1324756-P1324765-P1327456-P1327465-P1327645
    -P1372456-P1372465-P1372645-P1376245-P1623457-P1623475-P1623745-P1624357-P1624375
    -P1624537-P1632457-P1632475-P1632745-P1637245-P1724563-P1724635-P1724653-P1726345
    -P1726435-P1726453-P1762345-P1762435-P1762453-P1763245;
    fvpart[164] = -P1234657-P1234675-P1234765-P1236457-P1236475-P1236547-P1246573-P1246735-P1246753
    -P1247365-P1247635-P1247653-P1263457-P1263475-P1263547-P1264573-P1264735-P1264753
    -P1265347-P1265473-P1265734-P1267345-P1267354-P1267534-P1273465-P1273645-P1273654
    -P1276345-P1276354-P1276534-P1324657-P1324675-P1324765-P1326457-P1326475-P1326547
    -P1342657-P1342675-P1342765-P1347265-P1426573-P1426735-P1426753-P1427365-P1427635
    -P1427653-P1472365-P1472635-P1472653-P1473265-P1723465-P1723645-P1723654-P1726345
    -P1726354-P1726534-P1732465-P1732645-P1732654-P1734265;
    fvpart[165] = -P1236574-P1236745-P1236754-P1237465-P1237645-P1237654-P1243657-P1243675-P1243765
    -P1246357-P1246375-P1246537-P1263574-P1263745-P1263754-P1264357-P1264375-P1264537
    -P1265374-P1265437-P1265743-P1267435-P1267453-P1267543-P1274365-P1274635-P1274653
    -P1276435-P1276453-P1276543-P1326574-P1326745-P1326754-P1327465-P1327645-P1327654
    -P1372465-P1372645-P1372654-P1374265-P1423657-P1423675-P1423765-P1426357-P1426375
    -P1426537-P1432657-P1432675-P1432765-P1437265-P1724365-P1724635-P1724653-P1726435
    -P1726453-P1726543-P1742365-P1742635-P1742653-P1743265;
    fvpart[166] = -P1234675-P1234756-P1234765-P1237456-P1237465-P1237546-P1246375-P1246735-P1246753
    -P1247563-P1247635-P1247653-P1263475-P1263745-P1263754-P1267345-P1267354-P1267534
    -P1273456-P1273465-P1273546-P1274563-P1274635-P1274653-P1275346-P1275463-P1275634
    -P1276345-P1276354-P1276534-P1324675-P1324756-P1324765-P1327456-P1327465-P1327546
    -P1342675-P1342756-P1342765-P1346275-P1426375-P1426735-P1426753-P1427563-P1427635
    -P1427653-P1462375-P1462735-P1462753-P1463275-P1623475-P1623745-P1623754-P1627345
    -P1627354-P1627534-P1632475-P1632745-P1632754-P1634275;
    fvpart[167] = -P1236475-P1236745-P1236754-P1237564-P1237645-P1237654-P1243675-P1243756-P1243765
    -P1247356-P1247365-P1247536-P1264375-P1264735-P1264753-P1267435-P1267453-P1267543
    -P1273564-P1273645-P1273654-P1274356-P1274365-P1274536-P1275364-P1275436-P1275643
    -P1276435-P1276453-P1276543-P1326475-P1326745-P1326754-P1327564-P1327645-P1327654
    -P1362475-P1362745-P1362754-P1364275-P1423675-P1423756-P1423765-P1427356-P1427365
    -P1427536-P1432675-P1432756-P1432765-P1436275-P1624375-P1624735-P1624753-P1627435
    -P1627453-P1627543-P1642375-P1642735-P1642753-P1643275;
    fvpart[168] = -P1234567-P1234576-P1234657-P1235674-P1235746-P1235764-P1236457-P1236574-P1236745
    -P1237456-P1237465-P1237645-P1243567-P1243576-P1243657-P1245367-P1245376-P1245736
    -P1253674-P1253746-P1253764-P1257346-P1257364-P1257436-P1273456-P1273465-P1273645
    -P1274356-P1274365-P1274536-P1423567-P1423576-P1423657-P1425367-P1425376-P1425736
    -P1452367-P1452376-P1452736-P1457236-P1523674-P1523746-P1523764-P1527346-P1527364
    -P1527436-P1572346-P1572364-P1572436-P1574236-P1723456-P1723465-P1723645-P1724356
    -P1724365-P1724536-P1742356-P1742365-P1742536-P1745236;
    fvpart[169] = -P1234675-P1234756-P1234765-P1235467-P1235476-P1235647-P1236475-P1236547-P1236754
    -P1237546-P1237564-P1237654-P1243675-P1243756-P1243765-P1247356-P1247365-P1247536
    -P1253467-P1253476-P1253647-P1254367-P1254376-P1254736-P1273546-P1273564-P1273654
    -P1275346-P1275364-P1275436-P1423675-P1423756-P1423765-P1427356-P1427365-P1427536
    -P1472356-P1472365-P1472536-P1475236-P1523467-P1523476-P1523647-P1524367-P1524376
    -P1524736-P1542367-P1542376-P1542736-P1547236-P1723546-P1723564-P1723654-P1725346
    -P1725364-P1725436-P1752346-P1752364-P1752436-P1754236;
    fvpart[170] = -P1234567-P1234576-P1234657-P1235467-P1235476-P1235746-P1243567-P1243576-P1243657
    -P1245673-P1245736-P1245763-P1246357-P1246573-P1246735-P1247356-P1247365-P1247635
    -P1254673-P1254736-P1254763-P1257346-P1257436-P1257463-P1273456-P1273465-P1273546
    -P1274356-P1274365-P1274635-P1324567-P1324576-P1324657-P1325467-P1325476-P1325746
    -P1352467-P1352476-P1352746-P1357246-P1524673-P1524736-P1524763-P1527346-P1527436
    -P1527463-P1572346-P1572436-P1572463-P1573246-P1723456-P1723465-P1723546-P1724356
    -P1724365-P1724635-P1732456-P1732465-P1732546-P1735246;
    fvpart[171] = -P1234675-P1234756-P1234765-P1237456-P1237465-P1237546-P1243675-P1243756-P1243765
    -P1245367-P1245376-P1245637-P1246375-P1246537-P1246753-P1247536-P1247563-P1247653
    -P1253467-P1253476-P1253746-P1254367-P1254376-P1254637-P1274536-P1274563-P1274653
    -P1275346-P1275436-P1275463-P1324675-P1324756-P1324765-P1327456-P1327465-P1327546
    -P1372456-P1372465-P1372546-P1375246-P1523467-P1523476-P1523746-P1524367-P1524376
    -P1524637-P1532467-P1532476-P1532746-P1537246-P1724536-P1724563-P1724653-P1725346
    -P1725436-P1725463-P1752346-P1752436-P1752463-P1753246;
    fvpart[172] = -P1234567-P1234576-P1234756-P1235467-P1235476-P1235647-P1245673-P1245736-P1245763
    -P1247356-P1247536-P1247563-P1253467-P1253476-P1253647-P1254673-P1254736-P1254763
    -P1256347-P1256473-P1256734-P1257346-P1257364-P1257634-P1273456-P1273546-P1273564
    -P1275346-P1275364-P1275634-P1324567-P1324576-P1324756-P1325467-P1325476-P1325647
    -P1342567-P1342576-P1342756-P1347256-P1425673-P1425736-P1425763-P1427356-P1427536
    -P1427563-P1472356-P1472536-P1472563-P1473256-P1723456-P1723546-P1723564-P1725346
    -P1725364-P1725634-P1732456-P1732546-P1732564-P1734256;
    fvpart[173] = -P1235674-P1235746-P1235764-P1237456-P1237546-P1237564-P1243567-P1243576-P1243756
    -P1245367-P1245376-P1245637-P1253674-P1253746-P1253764-P1254367-P1254376-P1254637
    -P1256374-P1256437-P1256743-P1257436-P1257463-P1257643-P1274356-P1274536-P1274563
    -P1275436-P1275463-P1275643-P1325674-P1325746-P1325764-P1327456-P1327546-P1327564
    -P1372456-P1372546-P1372564-P1374256-P1423567-P1423576-P1423756-P1425367-P1425376
    -P1425637-P1432567-P1432576-P1432756-P1437256-P1724356-P1724536-P1724563-P1725436
    -P1725463-P1725643-P1742356-P1742536-P1742563-P1743256;
    fvpart[174] = -P1234576-P1234756-P1234765-P1237456-P1237465-P1237645-P1245376-P1245736-P1245763
    -P1247536-P1247563-P1247653-P1253476-P1253746-P1253764-P1257346-P1257364-P1257634
    -P1273456-P1273465-P1273645-P1274536-P1274563-P1274653-P1275346-P1275364-P1275634
    -P1276345-P1276453-P1276534-P1324576-P1324756-P1324765-P1327456-P1327465-P1327645
    -P1342576-P1342756-P1342765-P1345276-P1425376-P1425736-P1425763-P1427536-P1427563
    -P1427653-P1452376-P1452736-P1452763-P1453276-P1523476-P1523746-P1523764-P1527346
    -P1527364-P1527634-P1532476-P1532746-P1532764-P1534276;
    fvpart[175] = -P1235476-P1235746-P1235764-P1237546-P1237564-P1237654-P1243576-P1243756-P1243765
    -P1247356-P1247365-P1247635-P1254376-P1254736-P1254763-P1257436-P1257463-P1257643
    -P1273546-P1273564-P1273654-P1274356-P1274365-P1274635-P1275436-P1275463-P1275643
    -P1276354-P1276435-P1276543-P1325476-P1325746-P1325764-P1327546-P1327564-P1327654
    -P1352476-P1352746-P1352764-P1354276-P1423576-P1423756-P1423765-P1427356-P1427365
    -P1427635-P1432576-P1432756-P1432765-P1435276-P1524376-P1524736-P1524763-P1527436
    -P1527463-P1527643-P1542376-P1542736-P1542763-P1543276;
    fvpart[176] = -P1234567-P1234576-P1234756-P1235647-P1235674-P1235764-P1236457-P1236475-P1236745
    -P1237456-P1237564-P1237645-P1243567-P1243576-P1243756-P1245367-P1245376-P1245637
    -P1253647-P1253674-P1253764-P1256347-P1256374-P1256437-P1263457-P1263475-P1263745
    -P1264357-P1264375-P1264537-P1423567-P1423576-P1423756-P1425367-P1425376-P1425637
    -P1452367-P1452376-P1452637-P1456237-P1523647-P1523674-P1523764-P1526347-P1526374
    -P1526437-P1562347-P1562374-P1562437-P1564237-P1623457-P1623475-P1623745-P1624357
    -P1624375-P1624537-P1642357-P1642375-P1642537-P1645237;
    fvpart[177] = -P1234657-P1234675-P1234765-P1235467-P1235476-P1235746-P1236547-P1236574-P1236754
    -P1237465-P1237546-P1237654-P1243657-P1243675-P1243765-P1246357-P1246375-P1246537
    -P1253467-P1253476-P1253746-P1254367-P1254376-P1254637-P1263547-P1263574-P1263754
    -P1265347-P1265374-P1265437-P1423657-P1423675-P1423765-P1426357-P1426375-P1426537
    -P1462357-P1462375-P1462537-P1465237-P1523467-P1523476-P1523746-P1524367-P1524376
    -P1524637-P1542367-P1542376-P1542637-P1546237-P1623547-P1623574-P1623754-P1625347
    -P1625374-P1625437-P1652347-P1652374-P1652437-P1654237;
    fvpart[178] = -P1234567-P1234576-P1234756-P1235467-P1235476-P1235647-P1243567-P1243576-P1243756
    -P1245637-P1245673-P1245763-P1246357-P1246375-P1246735-P1247356-P1247563-P1247635
    -P1254637-P1254673-P1254763-P1256347-P1256437-P1256473-P1263457-P1263475-P1263547
    -P1264357-P1264375-P1264735-P1324567-P1324576-P1324756-P1325467-P1325476-P1325647
    -P1352467-P1352476-P1352647-P1356247-P1524637-P1524673-P1524763-P1526347-P1526437
    -P1526473-P1562347-P1562437-P1562473-P1563247-P1623457-P1623475-P1623547-P1624357
    -P1624375-P1624735-P1632457-P1632475-P1632547-P1635247;
    fvpart[179] = -P1234657-P1234675-P1234765-P1236457-P1236475-P1236547-P1243657-P1243675-P1243765
    -P1245367-P1245376-P1245736-P1246537-P1246573-P1246753-P1247365-P1247536-P1247653
    -P1253467-P1253476-P1253647-P1254367-P1254376-P1254736-P1264537-P1264573-P1264753
    -P1265347-P1265437-P1265473-P1324657-P1324675-P1324765-P1326457-P1326475-P1326547
    -P1362457-P1362475-P1362547-P1365247-P1523467-P1523476-P1523647-P1524367-P1524376
    -P1524736-P1532467-P1532476-P1532647-P1536247-P1624537-P1624573-P1624753-P1625347
    -P1625437-P1625473-P1652347-P1652437-P1652473-P1653247;
    fvpart[180] = -P1234567-P1234576-P1234657-P1235467-P1235476-P1235746-P1245637-P1245673-P1245763
    -P1246357-P1246537-P1246573-P1253467-P1253476-P1253746-P1254637-P1254673-P1254763
    -P1256347-P1256374-P1256734-P1257346-P1257463-P1257634-P1263457-P1263547-P1263574
    -P1265347-P1265374-P1265734-P1324567-P1324576-P1324657-P1325467-P1325476-P1325746
    -P1342567-P1342576-P1342657-P1346257-P1425637-P1425673-P1425763-P1426357-P1426537
    -P1426573-P1462357-P1462537-P1462573-P1463257-P1623457-P1623547-P1623574-P1625347
    -P1625374-P1625734-P1632457-P1632547-P1632574-P1634257;
    fvpart[181] = -P1235647-P1235674-P1235764-P1236457-P1236547-P1236574-P1243567-P1243576-P1243657
    -P1245367-P1245376-P1245736-P1253647-P1253674-P1253764-P1254367-P1254376-P1254736
    -P1256437-P1256473-P1256743-P1257364-P1257436-P1257643-P1264357-P1264537-P1264573
    -P1265437-P1265473-P1265743-P1325647-P1325674-P1325764-P1326457-P1326547-P1326574
    -P1362457-P1362547-P1362574-P1364257-P1423567-P1423576-P1423657-P1425367-P1425376
    -P1425736-P1432567-P1432576-P1432657-P1436257-P1624357-P1624537-P1624573-P1625437
    -P1625473-P1625743-P1642357-P1642537-P1642573-P1643257;
    fvpart[182] = -P1234567-P1234657-P1234675-P1236457-P1236475-P1236745-P1245367-P1245637-P1245673
    -P1246537-P1246573-P1246753-P1253467-P1253647-P1253674-P1256347-P1256374-P1256734
    -P1263457-P1263475-P1263745-P1264537-P1264573-P1264753-P1265347-P1265374-P1265734
    -P1267345-P1267453-P1267534-P1324567-P1324657-P1324675-P1326457-P1326475-P1326745
    -P1342567-P1342657-P1342675-P1345267-P1425367-P1425637-P1425673-P1426537-P1426573
    -P1426753-P1452367-P1452637-P1452673-P1453267-P1523467-P1523647-P1523674-P1526347
    -P1526374-P1526734-P1532467-P1532647-P1532674-P1534267;
    fvpart[183] = -P1235467-P1235647-P1235674-P1236547-P1236574-P1236754-P1243567-P1243657-P1243675
    -P1246357-P1246375-P1246735-P1254367-P1254637-P1254673-P1256437-P1256473-P1256743
    -P1263547-P1263574-P1263754-P1264357-P1264375-P1264735-P1265437-P1265473-P1265743
    -P1267354-P1267435-P1267543-P1325467-P1325647-P1325674-P1326547-P1326574-P1326754
    -P1352467-P1352647-P1352674-P1354267-P1423567-P1423657-P1423675-P1426357-P1426375
    -P1426735-P1432567-P1432657-P1432675-P1435267-P1524367-P1524637-P1524673-P1526437
    -P1526473-P1526743-P1542367-P1542637-P1542673-P1543267;
    fvpart[184] = P1234567+P1235674+P1236745+P1237456+P1243567+P1245367+P1245637+P1245673+P1253674
    +P1256374+P1256734+P1256743+P1263745+P1267345+P1267435+P1267453+P1273456+P1274356
    +P1274536+P1274563+P1423567+P1425367+P1425637+P1425673+P1452367+P1452637+P1452673
    +P1456237+P1456273+P1456723+P1523674+P1526374+P1526734+P1526743+P1562374+P1562734
    +P1562743+P1567234+P1567243+P1567423+P1623745+P1627345+P1627435+P1627453+P1672345
    +P1672435+P1672453+P1674235+P1674253+P1674523+P1723456+P1724356+P1724536+P1724563
    +P1742356+P1742536+P1742563+P1745236+P1745263+P1745623;
    fvpart[185] = P1234576+P1235764+P1236457+P1237645+P1243576+P1245376+P1245736+P1245763+P1253764
    +P1257364+P1257634+P1257643+P1263457+P1264357+P1264537+P1264573+P1273645+P1276345
    +P1276435+P1276453+P1423576+P1425376+P1425736+P1425763+P1452376+P1452736+P1452763
    +P1457236+P1457263+P1457623+P1523764+P1527364+P1527634+P1527643+P1572364+P1572634
    +P1572643+P1576234+P1576243+P1576423+P1623457+P1624357+P1624537+P1624573+P1642357
    +P1642537+P1642573+P1645237+P1645273+P1645723+P1723645+P1726345+P1726435+P1726453
    +P1762345+P1762435+P1762453+P1764235+P1764253+P1764523;
    fvpart[186] = P1234675+P1235467+P1236754+P1237546+P1243675+P1246375+P1246735+P1246753+P1253467
    +P1254367+P1254637+P1254673+P1263754+P1267354+P1267534+P1267543+P1273546+P1275346
    +P1275436+P1275463+P1423675+P1426375+P1426735+P1426753+P1462375+P1462735+P1462753
    +P1467235+P1467253+P1467523+P1523467+P1524367+P1524637+P1524673+P1542367+P1542637
    +P1542673+P1546237+P1546273+P1546723+P1623754+P1627354+P1627534+P1627543+P1672354
    +P1672534+P1672543+P1675234+P1675243+P1675423+P1723546+P1725346+P1725436+P1725463
    +P1752346+P1752436+P1752463+P1754236+P1754263+P1754623;
    fvpart[187] = P1234765+P1235476+P1236547+P1237654+P1243765+P1247365+P1247635+P1247653+P1253476
    +P1254376+P1254736+P1254763+P1263547+P1265347+P1265437+P1265473+P1273654+P1276354
    +P1276534+P1276543+P1423765+P1427365+P1427635+P1427653+P1472365+P1472635+P1472653
    +P1476235+P1476253+P1476523+P1523476+P1524376+P1524736+P1524763+P1542376+P1542736
    +P1542763+P1547236+P1547263+P1547623+P1623547+P1625347+P1625437+P1625473+P1652347
    +P1652437+P1652473+P1654237+P1654273+P1654723+P1723654+P1726354+P1726534+P1726543
    +P1762354+P1762534+P1762543+P1765234+P1765243+P1765423;
    fvpart[188] = P1234657+P1235746+P1236574+P1237465+P1243657+P1246357+P1246537+P1246573+P1253746
    +P1257346+P1257436+P1257463+P1263574+P1265374+P1265734+P1265743+P1273465+P1274365
    +P1274635+P1274653+P1423657+P1426357+P1426537+P1426573+P1462357+P1462537+P1462573
    +P1465237+P1465273+P1465723+P1523746+P1527346+P1527436+P1527463+P1572346+P1572436
    +P1572463+P1574236+P1574263+P1574623+P1623574+P1625374+P1625734+P1625743+P1652374
    +P1652734+P1652743+P1657234+P1657243+P1657423+P1723465+P1724365+P1724635+P1724653
    +P1742365+P1742635+P1742653+P1746235+P1746253+P1746523;
    fvpart[189] = P1234756+P1235647+P1236475+P1237564+P1243756+P1247356+P1247536+P1247563+P1253647
    +P1256347+P1256437+P1256473+P1263475+P1264375+P1264735+P1264753+P1273564+P1275364
    +P1275634+P1275643+P1423756+P1427356+P1427536+P1427563+P1472356+P1472536+P1472563
    +P1475236+P1475263+P1475623+P1523647+P1526347+P1526437+P1526473+P1562347+P1562437
    +P1562473+P1564237+P1564273+P1564723+P1623475+P1624375+P1624735+P1624753+P1642375
    +P1642735+P1642753+P1647235+P1647253+P1647523+P1723564+P1725364+P1725634+P1725643
    +P1752364+P1752634+P1752643+P1756234+P1756243+P1756423;
    fvpart[190] = P1234567+P1235467+P1235647+P1235674+P1243567+P1245673+P1246735+P1247356+P1254673
    +P1256473+P1256734+P1256743+P1264735+P1267345+P1267354+P1267435+P1273456+P1273546
    +P1273564+P1274356+P1324567+P1325467+P1325647+P1325674+P1352467+P1352647+P1352674
    +P1356247+P1356274+P1356724+P1524673+P1526473+P1526734+P1526743+P1562473+P1562734
    +P1562743+P1567234+P1567243+P1567324+P1624735+P1627345+P1627354+P1627435+P1672345
    +P1672354+P1672435+P1673245+P1673254+P1673524+P1723456+P1723546+P1723564+P1724356
    +P1732456+P1732546+P1732564+P1735246+P1735264+P1735624;
    fvpart[191] = P1234576+P1235476+P1235746+P1235764+P1243576+P1245763+P1246357+P1247635+P1254763
    +P1257463+P1257634+P1257643+P1263457+P1263547+P1263574+P1264357+P1274635+P1276345
    +P1276354+P1276435+P1324576+P1325476+P1325746+P1325764+P1352476+P1352746+P1352764
    +P1357246+P1357264+P1357624+P1524763+P1527463+P1527634+P1527643+P1572463+P1572634
    +P1572643+P1576234+P1576243+P1576324+P1623457+P1623547+P1623574+P1624357+P1632457
    +P1632547+P1632574+P1635247+P1635274+P1635724+P1724635+P1726345+P1726354+P1726435
    +P1762345+P1762354+P1762435+P1763245+P1763254+P1763524;
    fvpart[192] = P1234675+P1236475+P1236745+P1236754+P1243675+P1245367+P1246753+P1247536+P1253467
    +P1253647+P1253674+P1254367+P1264753+P1267453+P1267534+P1267543+P1274536+P1275346
    +P1275364+P1275436+P1324675+P1326475+P1326745+P1326754+P1362475+P1362745+P1362754
    +P1367245+P1367254+P1367524+P1523467+P1523647+P1523674+P1524367+P1532467+P1532647
    +P1532674+P1536247+P1536274+P1536724+P1624753+P1627453+P1627534+P1627543+P1672453
    +P1672534+P1672543+P1675234+P1675243+P1675324+P1724536+P1725346+P1725364+P1725436
    +P1752346+P1752364+P1752436+P1753246+P1753264+P1753624;
    fvpart[193] = P1234765+P1237465+P1237645+P1237654+P1243765+P1245376+P1246537+P1247653+P1253476
    +P1253746+P1253764+P1254376+P1264537+P1265347+P1265374+P1265437+P1274653+P1276453
    +P1276534+P1276543+P1324765+P1327465+P1327645+P1327654+P1372465+P1372645+P1372654
    +P1376245+P1376254+P1376524+P1523476+P1523746+P1523764+P1524376+P1532476+P1532746
    +P1532764+P1537246+P1537264+P1537624+P1624537+P1625347+P1625374+P1625437+P1652347
    +P1652374+P1652437+P1653247+P1653274+P1653724+P1724653+P1726453+P1726534+P1726543
    +P1762453+P1762534+P1762543+P1765234+P1765243+P1765324;
    fvpart[194] = P1234657+P1236457+P1236547+P1236574+P1243657+P1245736+P1246573+P1247365+P1254736
    +P1257346+P1257364+P1257436+P1264573+P1265473+P1265734+P1265743+P1273465+P1273645
    +P1273654+P1274365+P1324657+P1326457+P1326547+P1326574+P1362457+P1362547+P1362574
    +P1365247+P1365274+P1365724+P1524736+P1527346+P1527364+P1527436+P1572346+P1572364
    +P1572436+P1573246+P1573264+P1573624+P1624573+P1625473+P1625734+P1625743+P1652473
    +P1652734+P1652743+P1657234+P1657243+P1657324+P1723465+P1723645+P1723654+P1724365
    +P1732465+P1732645+P1732654+P1736245+P1736254+P1736524;
    fvpart[195] = P1234756+P1237456+P1237546+P1237564+P1243756+P1245637+P1246375+P1247563+P1254637
    +P1256347+P1256374+P1256437+P1263475+P1263745+P1263754+P1264375+P1274563+P1275463
    +P1275634+P1275643+P1324756+P1327456+P1327546+P1327564+P1372456+P1372546+P1372564
    +P1375246+P1375264+P1375624+P1524637+P1526347+P1526374+P1526437+P1562347+P1562374
    +P1562437+P1563247+P1563274+P1563724+P1623475+P1623745+P1623754+P1624375+P1632475
    +P1632745+P1632754+P1637245+P1637254+P1637524+P1724563+P1725463+P1725634+P1725643
    +P1752463+P1752634+P1752643+P1756234+P1756243+P1756324;
    fvpart[196] = P1234567+P1234657+P1234675+P1235467+P1245673+P1246573+P1246735+P1246753+P1253467
    +P1254673+P1256734+P1257346+P1265734+P1267345+P1267354+P1267534+P1273456+P1273465
    +P1273546+P1275346+P1324567+P1324657+P1324675+P1325467+P1342567+P1342657+P1342675
    +P1346257+P1346275+P1346725+P1425673+P1426573+P1426735+P1426753+P1462573+P1462735
    +P1462753+P1467235+P1467253+P1467325+P1625734+P1627345+P1627354+P1627534+P1672345
    +P1672354+P1672534+P1673245+P1673254+P1673425+P1723456+P1723465+P1723546+P1725346
    +P1732456+P1732465+P1732546+P1734256+P1734265+P1734625;
    fvpart[197] = P1234576+P1234756+P1234765+P1235476+P1245763+P1247563+P1247635+P1247653+P1253476
    +P1254763+P1256347+P1257634+P1263457+P1263475+P1263547+P1265347+P1275634+P1276345
    +P1276354+P1276534+P1324576+P1324756+P1324765+P1325476+P1342576+P1342756+P1342765
    +P1347256+P1347265+P1347625+P1425763+P1427563+P1427635+P1427653+P1472563+P1472635
    +P1472653+P1476235+P1476253+P1476325+P1623457+P1623475+P1623547+P1625347+P1632457
    +P1632475+P1632547+P1634257+P1634275+P1634725+P1725634+P1726345+P1726354+P1726534
    +P1762345+P1762354+P1762534+P1763245+P1763254+P1763425;
    fvpart[198] = P1235674+P1236574+P1236745+P1236754+P1243567+P1243657+P1243675+P1245367+P1253674
    +P1254367+P1256743+P1257436+P1265743+P1267435+P1267453+P1267543+P1274356+P1274365
    +P1274536+P1275436+P1325674+P1326574+P1326745+P1326754+P1362574+P1362745+P1362754
    +P1367245+P1367254+P1367425+P1423567+P1423657+P1423675+P1425367+P1432567+P1432657
    +P1432675+P1436257+P1436275+P1436725+P1625743+P1627435+P1627453+P1627543+P1672435
    +P1672453+P1672543+P1674235+P1674253+P1674325+P1724356+P1724365+P1724536+P1725436
    +P1742356+P1742365+P1742536+P1743256+P1743265+P1743625;
    fvpart[199] = P1235764+P1237564+P1237645+P1237654+P1243576+P1243756+P1243765+P1245376+P1253764
    +P1254376+P1256437+P1257643+P1264357+P1264375+P1264537+P1265437+P1275643+P1276435
    +P1276453+P1276543+P1325764+P1327564+P1327645+P1327654+P1372564+P1372645+P1372654
    +P1376245+P1376254+P1376425+P1423576+P1423756+P1423765+P1425376+P1432576+P1432756
    +P1432765+P1437256+P1437265+P1437625+P1624357+P1624375+P1624537+P1625437+P1642357
    +P1642375+P1642537+P1643257+P1643275+P1643725+P1725643+P1726435+P1726453+P1726543
    +P1762435+P1762453+P1762543+P1764235+P1764253+P1764325;
    fvpart[200] = P1235647+P1236457+P1236475+P1236547+P1245736+P1247356+P1247365+P1247536+P1253647
    +P1254736+P1256473+P1257364+P1264573+P1264735+P1264753+P1265473+P1273564+P1273645
    +P1273654+P1275364+P1325647+P1326457+P1326475+P1326547+P1362457+P1362475+P1362547
    +P1364257+P1364275+P1364725+P1425736+P1427356+P1427365+P1427536+P1472356+P1472365
    +P1472536+P1473256+P1473265+P1473625+P1624573+P1624735+P1624753+P1625473+P1642573
    +P1642735+P1642753+P1647235+P1647253+P1647325+P1723564+P1723645+P1723654+P1725364
    +P1732564+P1732645+P1732654+P1736245+P1736254+P1736425;
    fvpart[201] = P1235746+P1237456+P1237465+P1237546+P1245637+P1246357+P1246375+P1246537+P1253746
    +P1254637+P1256374+P1257463+P1263574+P1263745+P1263754+P1265374+P1274563+P1274635
    +P1274653+P1275463+P1325746+P1327456+P1327465+P1327546+P1372456+P1372465+P1372546
    +P1374256+P1374265+P1374625+P1425637+P1426357+P1426375+P1426537+P1462357+P1462375
    +P1462537+P1463257+P1463275+P1463725+P1623574+P1623745+P1623754+P1625374+P1632574
    +P1632745+P1632754+P1637245+P1637254+P1637425+P1724563+P1724635+P1724653+P1725463
    +P1742563+P1742635+P1742653+P1746235+P1746253+P1746325;
    fvpart[202] = P1234567+P1234576+P1234657+P1236457+P1245673+P1245736+P1245763+P1246573+P1256734
    +P1257346+P1257364+P1257634+P1263457+P1264573+P1265734+P1267345+P1273456+P1273465
    +P1273645+P1276345+P1324567+P1324576+P1324657+P1326457+P1342567+P1342576+P1342657
    +P1345267+P1345276+P1345726+P1425673+P1425736+P1425763+P1426573+P1452673+P1452736
    +P1452763+P1457236+P1457263+P1457326+P1526734+P1527346+P1527364+P1527634+P1572346
    +P1572364+P1572634+P1573246+P1573264+P1573426+P1723456+P1723465+P1723645+P1726345
    +P1732456+P1732465+P1732645+P1734256+P1734265+P1734526;
    fvpart[203] = P1234675+P1234756+P1234765+P1236475+P1246753+P1247536+P1247563+P1247653+P1253467
    +P1253476+P1253647+P1256347+P1263475+P1264753+P1265347+P1267534+P1275346+P1275364
    +P1275634+P1276534+P1324675+P1324756+P1324765+P1326475+P1342675+P1342756+P1342765
    +P1347256+P1347265+P1347526+P1426753+P1427536+P1427563+P1427653+P1472536+P1472563
    +P1472653+P1475236+P1475263+P1475326+P1523467+P1523476+P1523647+P1526347+P1532467
    +P1532476+P1532647+P1534267+P1534276+P1534726+P1725346+P1725364+P1725634+P1726534
    +P1752346+P1752364+P1752634+P1753246+P1753264+P1753426;
    fvpart[204] = P1235674+P1235746+P1235764+P1236574+P1243567+P1243576+P1243657+P1246357+P1256743
    +P1257436+P1257463+P1257643+P1263574+P1264357+P1265743+P1267435+P1274356+P1274365
    +P1274635+P1276435+P1325674+P1325746+P1325764+P1326574+P1352674+P1352746+P1352764
    +P1357246+P1357264+P1357426+P1423567+P1423576+P1423657+P1426357+P1432567+P1432576
    +P1432657+P1435267+P1435276+P1435726+P1526743+P1527436+P1527463+P1527643+P1572436
    +P1572463+P1572643+P1574236+P1574263+P1574326+P1724356+P1724365+P1724635+P1726435
    +P1742356+P1742365+P1742635+P1743256+P1743265+P1743526;
    fvpart[205] = P1236754+P1237546+P1237564+P1237654+P1243675+P1243756+P1243765+P1246375+P1254367
    +P1254376+P1254637+P1256437+P1263754+P1264375+P1265437+P1267543+P1275436+P1275463
    +P1275643+P1276543+P1326754+P1327546+P1327564+P1327654+P1372546+P1372564+P1372654
    +P1375246+P1375264+P1375426+P1423675+P1423756+P1423765+P1426375+P1432675+P1432756
    +P1432765+P1437256+P1437265+P1437526+P1524367+P1524376+P1524637+P1526437+P1542367
    +P1542376+P1542637+P1543267+P1543276+P1543726+P1725436+P1725463+P1725643+P1726543
    +P1752436+P1752463+P1752643+P1754236+P1754263+P1754326;
    fvpart[206] = P1235467+P1235476+P1235647+P1236547+P1246735+P1247356+P1247365+P1247635+P1254673
    +P1254736+P1254763+P1256473+P1263547+P1264735+P1265473+P1267354+P1273546+P1273564
    +P1273654+P1276354+P1325467+P1325476+P1325647+P1326547+P1352467+P1352476+P1352647
    +P1354267+P1354276+P1354726+P1426735+P1427356+P1427365+P1427635+P1472356+P1472365
    +P1472635+P1473256+P1473265+P1473526+P1524673+P1524736+P1524763+P1526473+P1542673
    +P1542736+P1542763+P1547236+P1547263+P1547326+P1723546+P1723564+P1723654+P1726354
    +P1732546+P1732564+P1732654+P1735246+P1735264+P1735426;
    fvpart[207] = P1236745+P1237456+P1237465+P1237645+P1245367+P1245376+P1245637+P1246537+P1253674
    +P1253746+P1253764+P1256374+P1263745+P1264537+P1265374+P1267453+P1274536+P1274563
    +P1274653+P1276453+P1326745+P1327456+P1327465+P1327645+P1372456+P1372465+P1372645
    +P1374256+P1374265+P1374526+P1425367+P1425376+P1425637+P1426537+P1452367+P1452376
    +P1452637+P1453267+P1453276+P1453726+P1523674+P1523746+P1523764+P1526374+P1532674
    +P1532746+P1532764+P1537246+P1537264+P1537426+P1724536+P1724563+P1724653+P1726453
    +P1742536+P1742563+P1742653+P1745236+P1745263+P1745326;
    fvpart[208] = P1234567+P1234576+P1234756+P1237456+P1245637+P1245673+P1245763+P1247563+P1256347
    +P1256374+P1256734+P1257634+P1263457+P1263475+P1263745+P1267345+P1273456+P1274563
    +P1275634+P1276345+P1324567+P1324576+P1324756+P1327456+P1342567+P1342576+P1342756
    +P1345267+P1345276+P1345627+P1425637+P1425673+P1425763+P1427563+P1452637+P1452673
    +P1452763+P1456237+P1456273+P1456327+P1526347+P1526374+P1526734+P1527634+P1562347
    +P1562374+P1562734+P1563247+P1563274+P1563427+P1623457+P1623475+P1623745+P1627345
    +P1632457+P1632475+P1632745+P1634257+P1634275+P1634527;
    fvpart[209] = P1234657+P1234675+P1234765+P1237465+P1246537+P1246573+P1246753+P1247653+P1253467
    +P1253476+P1253746+P1257346+P1265347+P1265374+P1265734+P1267534+P1273465+P1274653
    +P1275346+P1276534+P1324657+P1324675+P1324765+P1327465+P1342657+P1342675+P1342765
    +P1346257+P1346275+P1346527+P1426537+P1426573+P1426753+P1427653+P1462537+P1462573
    +P1462753+P1465237+P1465273+P1465327+P1523467+P1523476+P1523746+P1527346+P1532467
    +P1532476+P1532746+P1534267+P1534276+P1534627+P1625347+P1625374+P1625734+P1627534
    +P1652347+P1652374+P1652734+P1653247+P1653274+P1653427;
    fvpart[210] = P1235647+P1235674+P1235764+P1237564+P1243567+P1243576+P1243756+P1247356+P1256437
    +P1256473+P1256743+P1257643+P1264357+P1264375+P1264735+P1267435+P1273564+P1274356
    +P1275643+P1276435+P1325647+P1325674+P1325764+P1327564+P1352647+P1352674+P1352764
    +P1356247+P1356274+P1356427+P1423567+P1423576+P1423756+P1427356+P1432567+P1432576
    +P1432756+P1435267+P1435276+P1435627+P1526437+P1526473+P1526743+P1527643+P1562437
    +P1562473+P1562743+P1564237+P1564273+P1564327+P1624357+P1624375+P1624735+P1627435
    +P1642357+P1642375+P1642735+P1643257+P1643275+P1643527;
    fvpart[211] = P1236547+P1236574+P1236754+P1237654+P1243657+P1243675+P1243765+P1247365+P1254367
    +P1254376+P1254736+P1257436+P1265437+P1265473+P1265743+P1267543+P1273654+P1274365
    +P1275436+P1276543+P1326547+P1326574+P1326754+P1327654+P1362547+P1362574+P1362754
    +P1365247+P1365274+P1365427+P1423657+P1423675+P1423765+P1427365+P1432657+P1432675
    +P1432765+P1436257+P1436275+P1436527+P1524367+P1524376+P1524736+P1527436+P1542367
    +P1542376+P1542736+P1543267+P1543276+P1543627+P1625437+P1625473+P1625743+P1627543
    +P1652437+P1652473+P1652743+P1654237+P1654273+P1654327;
    fvpart[212] = P1235467+P1235476+P1235746+P1237546+P1246357+P1246375+P1246735+P1247635+P1254637
    +P1254673+P1254763+P1257463+P1263547+P1263574+P1263754+P1267354+P1273546+P1274635
    +P1275463+P1276354+P1325467+P1325476+P1325746+P1327546+P1352467+P1352476+P1352746
    +P1354267+P1354276+P1354627+P1426357+P1426375+P1426735+P1427635+P1462357+P1462375
    +P1462735+P1463257+P1463275+P1463527+P1524637+P1524673+P1524763+P1527463+P1542637
    +P1542673+P1542763+P1546237+P1546273+P1546327+P1623547+P1623574+P1623754+P1627354
    +P1632547+P1632574+P1632754+P1635247+P1635274+P1635427;
    fvpart[213] = P1236457+P1236475+P1236745+P1237645+P1245367+P1245376+P1245736+P1247536+P1253647
    +P1253674+P1253764+P1257364+P1264537+P1264573+P1264753+P1267453+P1273645+P1274536
    +P1275364+P1276453+P1326457+P1326475+P1326745+P1327645+P1362457+P1362475+P1362745
    +P1364257+P1364275+P1364527+P1425367+P1425376+P1425736+P1427536+P1452367+P1452376
    +P1452736+P1453267+P1453276+P1453627+P1523647+P1523674+P1523764+P1527364+P1532647
    +P1532674+P1532764+P1536247+P1536274+P1536427+P1624537+P1624573+P1624753+P1627453
    +P1642537+P1642573+P1642753+P1645237+P1645273+P1645327;
    fvpart[214] = P1254367+P1254376+P1254637+P1254673+P1254736+P1254763+P1256437+P1256473+P1256743
    +P1257436+P1257463+P1257643+P1265437+P1265473+P1265743+P1267543+P1275436+P1275463
    +P1275643+P1276543+P1625437+P1625473+P1625743+P1627543+P1672543+P1725436+P1725463
    +P1725643+P1726543+P1762543;
    fvpart[215] = P1245367+P1245376+P1245637+P1245673+P1245736+P1245763+P1246537+P1246573+P1246753
    +P1247536+P1247563+P1247653+P1264537+P1264573+P1264753+P1267453+P1274536+P1274563
    +P1274653+P1276453+P1624537+P1624573+P1624753+P1627453+P1672453+P1724536+P1724563
    +P1724653+P1726453+P1762453;
    fvpart[216] = P1256437+P1256473+P1256743+P1257643+P1264357+P1264375+P1264537+P1264573+P1264735
    +P1264753+P1265437+P1265473+P1265743+P1267435+P1267453+P1267543+P1275643+P1276435
    +P1276453+P1276543+P1526437+P1526473+P1526743+P1527643+P1572643+P1725643+P1726435
    +P1726453+P1726543+P1752643;
    fvpart[217] = P1245637+P1245673+P1245763+P1246357+P1246375+P1246537+P1246573+P1246735+P1246753
    +P1247563+P1247635+P1247653+P1254637+P1254673+P1254763+P1257463+P1274563+P1274635
    +P1274653+P1275463+P1524637+P1524673+P1524763+P1527463+P1572463+P1724563+P1724635
    +P1724653+P1725463+P1752463;
    fvpart[218] = P1256743+P1257436+P1257463+P1257643+P1265743+P1267435+P1267453+P1267543+P1274356
    +P1274365+P1274536+P1274563+P1274635+P1274653+P1275436+P1275463+P1275643+P1276435
    +P1276453+P1276543+P1526743+P1527436+P1527463+P1527643+P1562743+P1625743+P1627435
    +P1627453+P1627543+P1652743;
    fvpart[219] = P1245673+P1245736+P1245763+P1246573+P1246735+P1246753+P1247356+P1247365+P1247536
    +P1247563+P1247635+P1247653+P1254673+P1254736+P1254763+P1256473+P1264573+P1264735
    +P1264753+P1265473+P1524673+P1524736+P1524763+P1526473+P1562473+P1624573+P1624735
    +P1624753+P1625473+P1652473;
    fvpart[220] = P1246537+P1246573+P1246753+P1247653+P1264537+P1264573+P1264753+P1265347+P1265374
    +P1265437+P1265473+P1265734+P1265743+P1267453+P1267534+P1267543+P1274653+P1276453
    +P1276534+P1276543+P1426537+P1426573+P1426753+P1427653+P1472653+P1724653+P1726453
    +P1726534+P1726543+P1742653;
    fvpart[221] = P1245637+P1245673+P1245763+P1247563+P1254637+P1254673+P1254763+P1256347+P1256374
    +P1256437+P1256473+P1256734+P1256743+P1257463+P1257634+P1257643+P1274563+P1275463
    +P1275634+P1275643+P1425637+P1425673+P1425763+P1427563+P1472563+P1724563+P1725463
    +P1725634+P1725643+P1742563;
    fvpart[222] = P1246753+P1247536+P1247563+P1247653+P1264753+P1267453+P1267534+P1267543+P1274536
    +P1274563+P1274653+P1275346+P1275364+P1275436+P1275463+P1275634+P1275643+P1276453
    +P1276534+P1276543+P1426753+P1427536+P1427563+P1427653+P1462753+P1624753+P1627453
    +P1627534+P1627543+P1642753;
    fvpart[223] = P1245673+P1245736+P1245763+P1246573+P1254673+P1254736+P1254763+P1256473+P1256734
    +P1256743+P1257346+P1257364+P1257436+P1257463+P1257634+P1257643+P1264573+P1265473
    +P1265734+P1265743+P1425673+P1425736+P1425763+P1426573+P1462573+P1624573+P1625473
    +P1625734+P1625743+P1642573;
    fvpart[224] = P1245763+P1247563+P1247635+P1247653+P1254763+P1257463+P1257634+P1257643+P1274563
    +P1274635+P1274653+P1275463+P1275634+P1275643+P1276345+P1276354+P1276435+P1276453
    +P1276534+P1276543+P1425763+P1427563+P1427635+P1427653+P1452763+P1524763+P1527463
    +P1527634+P1527643+P1542763;
    fvpart[225] = P1245673+P1246573+P1246735+P1246753+P1254673+P1256473+P1256734+P1256743+P1264573
    +P1264735+P1264753+P1265473+P1265734+P1265743+P1267345+P1267354+P1267435+P1267453
    +P1267534+P1267543+P1425673+P1426573+P1426735+P1426753+P1452673+P1524673+P1526473
    +P1526734+P1526743+P1542673;
    fvpart[226] = P1253467+P1253476+P1253647+P1253674+P1253746+P1253764+P1256347+P1256374+P1256734
    +P1257346+P1257364+P1257634+P1265347+P1265374+P1265734+P1267534+P1275346+P1275364
    +P1275634+P1276534+P1625347+P1625374+P1625734+P1627534+P1672534+P1725346+P1725364
    +P1725634+P1726534+P1762534;
    fvpart[227] = P1235467+P1235476+P1235647+P1235674+P1235746+P1235764+P1236547+P1236574+P1236754
    +P1237546+P1237564+P1237654+P1263547+P1263574+P1263754+P1267354+P1273546+P1273564
    +P1273654+P1276354+P1623547+P1623574+P1623754+P1627354+P1672354+P1723546+P1723564
    +P1723654+P1726354+P1762354;
    fvpart[228] = P1256347+P1256374+P1256734+P1257634+P1263457+P1263475+P1263547+P1263574+P1263745
    +P1263754+P1265347+P1265374+P1265734+P1267345+P1267354+P1267534+P1275634+P1276345
    +P1276354+P1276534+P1526347+P1526374+P1526734+P1527634+P1572634+P1725634+P1726345
    +P1726354+P1726534+P1752634;
    fvpart[229] = P1235647+P1235674+P1235764+P1236457+P1236475+P1236547+P1236574+P1236745+P1236754
    +P1237564+P1237645+P1237654+P1253647+P1253674+P1253764+P1257364+P1273564+P1273645
    +P1273654+P1275364+P1523647+P1523674+P1523764+P1527364+P1572364+P1723564+P1723645
    +P1723654+P1725364+P1752364;
    fvpart[230] = P1256734+P1257346+P1257364+P1257634+P1265734+P1267345+P1267354+P1267534+P1273456
    +P1273465+P1273546+P1273564+P1273645+P1273654+P1275346+P1275364+P1275634+P1276345
    +P1276354+P1276534+P1526734+P1527346+P1527364+P1527634+P1562734+P1625734+P1627345
    +P1627354+P1627534+P1652734;
    fvpart[231] = P1235674+P1235746+P1235764+P1236574+P1236745+P1236754+P1237456+P1237465+P1237546
    +P1237564+P1237645+P1237654+P1253674+P1253746+P1253764+P1256374+P1263574+P1263745
    +P1263754+P1265374+P1523674+P1523746+P1523764+P1526374+P1562374+P1623574+P1623745
    +P1623754+P1625374+P1652374;
    fvpart[232] = P1236547+P1236574+P1236754+P1237654+P1263547+P1263574+P1263754+P1265347+P1265374
    +P1265437+P1265473+P1265734+P1265743+P1267354+P1267534+P1267543+P1273654+P1276354
    +P1276534+P1276543+P1326547+P1326574+P1326754+P1327654+P1372654+P1723654+P1726354
    +P1726534+P1726543+P1732654;
    fvpart[233] = P1235647+P1235674+P1235764+P1237564+P1253647+P1253674+P1253764+P1256347+P1256374
    +P1256437+P1256473+P1256734+P1256743+P1257364+P1257634+P1257643+P1273564+P1275364
    +P1275634+P1275643+P1325647+P1325674+P1325764+P1327564+P1372564+P1723564+P1725364
    +P1725634+P1725643+P1732564;
    fvpart[234] = P1236754+P1237546+P1237564+P1237654+P1263754+P1267354+P1267534+P1267543+P1273546
    +P1273564+P1273654+P1275346+P1275364+P1275436+P1275463+P1275634+P1275643+P1276354
    +P1276534+P1276543+P1326754+P1327546+P1327564+P1327654+P1362754+P1623754+P1627354
    +P1627534+P1627543+P1632754;
    fvpart[235] = P1235674+P1235746+P1235764+P1236574+P1253674+P1253746+P1253764+P1256374+P1256734
    +P1256743+P1257346+P1257364+P1257436+P1257463+P1257634+P1257643+P1263574+P1265374
    +P1265734+P1265743+P1325674+P1325746+P1325764+P1326574+P1362574+P1623574+P1625374
    +P1625734+P1625743+P1632574;
    fvpart[236] = P1235764+P1237564+P1237645+P1237654+P1253764+P1257364+P1257634+P1257643+P1273564
    +P1273645+P1273654+P1275364+P1275634+P1275643+P1276345+P1276354+P1276435+P1276453
    +P1276534+P1276543+P1325764+P1327564+P1327645+P1327654+P1352764+P1523764+P1527364
    +P1527634+P1527643+P1532764;
    fvpart[237] = P1235674+P1236574+P1236745+P1236754+P1253674+P1256374+P1256734+P1256743+P1263574
    +P1263745+P1263754+P1265374+P1265734+P1265743+P1267345+P1267354+P1267435+P1267453
    +P1267534+P1267543+P1325674+P1326574+P1326745+P1326754+P1352674+P1523674+P1526374
    +P1526734+P1526743+P1532674;
    fvpart[238] = P1243567+P1243576+P1243657+P1243675+P1243756+P1243765+P1246357+P1246375+P1246735
    +P1247356+P1247365+P1247635+P1264357+P1264375+P1264735+P1267435+P1274356+P1274365
    +P1274635+P1276435+P1624357+P1624375+P1624735+P1627435+P1672435+P1724356+P1724365
    +P1724635+P1726435+P1762435;
    fvpart[239] = P1234567+P1234576+P1234657+P1234675+P1234756+P1234765+P1236457+P1236475+P1236745
    +P1237456+P1237465+P1237645+P1263457+P1263475+P1263745+P1267345+P1273456+P1273465
    +P1273645+P1276345+P1623457+P1623475+P1623745+P1627345+P1672345+P1723456+P1723465
    +P1723645+P1726345+P1762345;
    fvpart[240] = P1246357+P1246375+P1246735+P1247635+P1263457+P1263475+P1263547+P1263574+P1263745
    +P1263754+P1264357+P1264375+P1264735+P1267345+P1267354+P1267435+P1274635+P1276345
    +P1276354+P1276435+P1426357+P1426375+P1426735+P1427635+P1472635+P1724635+P1726345
    +P1726354+P1726435+P1742635;
    fvpart[241] = P1234657+P1234675+P1234765+P1236457+P1236475+P1236547+P1236574+P1236745+P1236754
    +P1237465+P1237645+P1237654+P1243657+P1243675+P1243765+P1247365+P1273465+P1273645
    +P1273654+P1274365+P1423657+P1423675+P1423765+P1427365+P1472365+P1723465+P1723645
    +P1723654+P1724365+P1742365;
    fvpart[242] = P1246735+P1247356+P1247365+P1247635+P1264735+P1267345+P1267354+P1267435+P1273456
    +P1273465+P1273546+P1273564+P1273645+P1273654+P1274356+P1274365+P1274635+P1276345
    +P1276354+P1276435+P1426735+P1427356+P1427365+P1427635+P1462735+P1624735+P1627345
    +P1627354+P1627435+P1642735;
    fvpart[243] = P1234675+P1234756+P1234765+P1236475+P1236745+P1236754+P1237456+P1237465+P1237546
    +P1237564+P1237645+P1237654+P1243675+P1243756+P1243765+P1246375+P1263475+P1263745
    +P1263754+P1264375+P1423675+P1423756+P1423765+P1426375+P1462375+P1623475+P1623745
    +P1623754+P1624375+P1642375;
    fvpart[244] = P1236457+P1236475+P1236745+P1237645+P1263457+P1263475+P1263745+P1264357+P1264375
    +P1264537+P1264573+P1264735+P1264753+P1267345+P1267435+P1267453+P1273645+P1276345
    +P1276435+P1276453+P1326457+P1326475+P1326745+P1327645+P1372645+P1723645+P1726345
    +P1726435+P1726453+P1732645;
    fvpart[245] = P1234657+P1234675+P1234765+P1237465+P1243657+P1243675+P1243765+P1246357+P1246375
    +P1246537+P1246573+P1246735+P1246753+P1247365+P1247635+P1247653+P1273465+P1274365
    +P1274635+P1274653+P1324657+P1324675+P1324765+P1327465+P1372465+P1723465+P1724365
    +P1724635+P1724653+P1732465;
    fvpart[246] = P1236745+P1237456+P1237465+P1237645+P1263745+P1267345+P1267435+P1267453+P1273456
    +P1273465+P1273645+P1274356+P1274365+P1274536+P1274563+P1274635+P1274653+P1276345
    +P1276435+P1276453+P1326745+P1327456+P1327465+P1327645+P1362745+P1623745+P1627345
    +P1627435+P1627453+P1632745;
    fvpart[247] = P1234675+P1234756+P1234765+P1236475+P1243675+P1243756+P1243765+P1246375+P1246735
    +P1246753+P1247356+P1247365+P1247536+P1247563+P1247635+P1247653+P1263475+P1264375
    +P1264735+P1264753+P1324675+P1324756+P1324765+P1326475+P1362475+P1623475+P1624375
    +P1624735+P1624753+P1632475;
    fvpart[248] = P1234765+P1237465+P1237645+P1237654+P1243765+P1247365+P1247635+P1247653+P1273465
    +P1273645+P1273654+P1274365+P1274635+P1274653+P1276345+P1276354+P1276435+P1276453
    +P1276534+P1276543+P1324765+P1327465+P1327645+P1327654+P1342765+P1423765+P1427365
    +P1427635+P1427653+P1432765;
    fvpart[249] = P1234675+P1236475+P1236745+P1236754+P1243675+P1246375+P1246735+P1246753+P1263475
    +P1263745+P1263754+P1264375+P1264735+P1264753+P1267345+P1267354+P1267435+P1267453
    +P1267534+P1267543+P1324675+P1326475+P1326745+P1326754+P1342675+P1423675+P1426375
    +P1426735+P1426753+P1432675;
    fvpart[250] = P1243567+P1243576+P1243657+P1243675+P1243756+P1243765+P1245367+P1245376+P1245736
    +P1247356+P1247365+P1247536+P1254367+P1254376+P1254736+P1257436+P1274356+P1274365
    +P1274536+P1275436+P1524367+P1524376+P1524736+P1527436+P1572436+P1724356+P1724365
    +P1724536+P1725436+P1752436;
    fvpart[251] = P1234567+P1234576+P1234657+P1234675+P1234756+P1234765+P1235467+P1235476+P1235746
    +P1237456+P1237465+P1237546+P1253467+P1253476+P1253746+P1257346+P1273456+P1273465
    +P1273546+P1275346+P1523467+P1523476+P1523746+P1527346+P1572346+P1723456+P1723465
    +P1723546+P1725346+P1752346;
    fvpart[252] = P1245367+P1245376+P1245736+P1247536+P1253467+P1253476+P1253647+P1253674+P1253746
    +P1253764+P1254367+P1254376+P1254736+P1257346+P1257364+P1257436+P1274536+P1275346
    +P1275364+P1275436+P1425367+P1425376+P1425736+P1427536+P1472536+P1724536+P1725346
    +P1725364+P1725436+P1742536;
    fvpart[253] = P1234567+P1234576+P1234756+P1235467+P1235476+P1235647+P1235674+P1235746+P1235764
    +P1237456+P1237546+P1237564+P1243567+P1243576+P1243756+P1247356+P1273456+P1273546
    +P1273564+P1274356+P1423567+P1423576+P1423756+P1427356+P1472356+P1723456+P1723546
    +P1723564+P1724356+P1742356;
    fvpart[254] = P1245736+P1247356+P1247365+P1247536+P1254736+P1257346+P1257364+P1257436+P1273456
    +P1273465+P1273546+P1273564+P1273645+P1273654+P1274356+P1274365+P1274536+P1275346
    +P1275364+P1275436+P1425736+P1427356+P1427365+P1427536+P1452736+P1524736+P1527346
    +P1527364+P1527436+P1542736;
    fvpart[255] = P1234576+P1234756+P1234765+P1235476+P1235746+P1235764+P1237456+P1237465+P1237546
    +P1237564+P1237645+P1237654+P1243576+P1243756+P1243765+P1245376+P1253476+P1253746
    +P1253764+P1254376+P1423576+P1423756+P1423765+P1425376+P1452376+P1523476+P1523746
    +P1523764+P1524376+P1542376;
    fvpart[256] = P1235467+P1235476+P1235746+P1237546+P1253467+P1253476+P1253746+P1254367+P1254376
    +P1254637+P1254673+P1254736+P1254763+P1257346+P1257436+P1257463+P1273546+P1275346
    +P1275436+P1275463+P1325467+P1325476+P1325746+P1327546+P1372546+P1723546+P1725346
    +P1725436+P1725463+P1732546;
    fvpart[257] = P1234567+P1234576+P1234756+P1237456+P1243567+P1243576+P1243756+P1245367+P1245376
    +P1245637+P1245673+P1245736+P1245763+P1247356+P1247536+P1247563+P1273456+P1274356
    +P1274536+P1274563+P1324567+P1324576+P1324756+P1327456+P1372456+P1723456+P1724356
    +P1724536+P1724563+P1732456;
    fvpart[258] = P1235746+P1237456+P1237465+P1237546+P1253746+P1257346+P1257436+P1257463+P1273456
    +P1273465+P1273546+P1274356+P1274365+P1274536+P1274563+P1274635+P1274653+P1275346
    +P1275436+P1275463+P1325746+P1327456+P1327465+P1327546+P1352746+P1523746+P1527346
    +P1527436+P1527463+P1532746;
    fvpart[259] = P1234576+P1234756+P1234765+P1235476+P1243576+P1243756+P1243765+P1245376+P1245736
    +P1245763+P1247356+P1247365+P1247536+P1247563+P1247635+P1247653+P1253476+P1254376
    +P1254736+P1254763+P1324576+P1324756+P1324765+P1325476+P1352476+P1523476+P1524376
    +P1524736+P1524763+P1532476;
    fvpart[260] = P1234756+P1237456+P1237546+P1237564+P1243756+P1247356+P1247536+P1247563+P1273456
    +P1273546+P1273564+P1274356+P1274536+P1274563+P1275346+P1275364+P1275436+P1275463
    +P1275634+P1275643+P1324756+P1327456+P1327546+P1327564+P1342756+P1423756+P1427356
    +P1427536+P1427563+P1432756;
    fvpart[261] = P1234576+P1235476+P1235746+P1235764+P1243576+P1245376+P1245736+P1245763+P1253476
    +P1253746+P1253764+P1254376+P1254736+P1254763+P1257346+P1257364+P1257436+P1257463
    +P1257634+P1257643+P1324576+P1325476+P1325746+P1325764+P1342576+P1423576+P1425376
    +P1425736+P1425763+P1432576;
    fvpart[262] = P1243567+P1243576+P1243657+P1243675+P1243756+P1243765+P1245367+P1245376+P1245637
    +P1246357+P1246375+P1246537+P1254367+P1254376+P1254637+P1256437+P1264357+P1264375
    +P1264537+P1265437+P1524367+P1524376+P1524637+P1526437+P1562437+P1624357+P1624375
    +P1624537+P1625437+P1652437;
    fvpart[263] = P1234567+P1234576+P1234657+P1234675+P1234756+P1234765+P1235467+P1235476+P1235647
    +P1236457+P1236475+P1236547+P1253467+P1253476+P1253647+P1256347+P1263457+P1263475
    +P1263547+P1265347+P1523467+P1523476+P1523647+P1526347+P1562347+P1623457+P1623475
    +P1623547+P1625347+P1652347;
    fvpart[264] = P1245367+P1245376+P1245637+P1246537+P1253467+P1253476+P1253647+P1253674+P1253746
    +P1253764+P1254367+P1254376+P1254637+P1256347+P1256374+P1256437+P1264537+P1265347
    +P1265374+P1265437+P1425367+P1425376+P1425637+P1426537+P1462537+P1624537+P1625347
    +P1625374+P1625437+P1642537;
    fvpart[265] = P1234567+P1234576+P1234657+P1235467+P1235476+P1235647+P1235674+P1235746+P1235764
    +P1236457+P1236547+P1236574+P1243567+P1243576+P1243657+P1246357+P1263457+P1263547
    +P1263574+P1264357+P1423567+P1423576+P1423657+P1426357+P1462357+P1623457+P1623547
    +P1623574+P1624357+P1642357;
    fvpart[266] = P1245637+P1246357+P1246375+P1246537+P1254637+P1256347+P1256374+P1256437+P1263457
    +P1263475+P1263547+P1263574+P1263745+P1263754+P1264357+P1264375+P1264537+P1265347
    +P1265374+P1265437+P1425637+P1426357+P1426375+P1426537+P1452637+P1524637+P1526347
    +P1526374+P1526437+P1542637;
    fvpart[267] = P1234567+P1234657+P1234675+P1235467+P1235647+P1235674+P1236457+P1236475+P1236547
    +P1236574+P1236745+P1236754+P1243567+P1243657+P1243675+P1245367+P1253467+P1253647
    +P1253674+P1254367+P1423567+P1423657+P1423675+P1425367+P1452367+P1523467+P1523647
    +P1523674+P1524367+P1542367;
    fvpart[268] = P1235467+P1235476+P1235647+P1236547+P1253467+P1253476+P1253647+P1254367+P1254376
    +P1254637+P1254673+P1254736+P1254763+P1256347+P1256437+P1256473+P1263547+P1265347
    +P1265437+P1265473+P1325467+P1325476+P1325647+P1326547+P1362547+P1623547+P1625347
    +P1625437+P1625473+P1632547;
    fvpart[269] = P1234567+P1234576+P1234657+P1236457+P1243567+P1243576+P1243657+P1245367+P1245376
    +P1245637+P1245673+P1245736+P1245763+P1246357+P1246537+P1246573+P1263457+P1264357
    +P1264537+P1264573+P1324567+P1324576+P1324657+P1326457+P1362457+P1623457+P1624357
    +P1624537+P1624573+P1632457;
    fvpart[270] = P1235647+P1236457+P1236475+P1236547+P1253647+P1256347+P1256437+P1256473+P1263457
    +P1263475+P1263547+P1264357+P1264375+P1264537+P1264573+P1264735+P1264753+P1265347
    +P1265437+P1265473+P1325647+P1326457+P1326475+P1326547+P1352647+P1523647+P1526347
    +P1526437+P1526473+P1532647;
    fvpart[271] = P1234567+P1234657+P1234675+P1235467+P1243567+P1243657+P1243675+P1245367+P1245637
    +P1245673+P1246357+P1246375+P1246537+P1246573+P1246735+P1246753+P1253467+P1254367
    +P1254637+P1254673+P1324567+P1324657+P1324675+P1325467+P1352467+P1523467+P1524367
    +P1524637+P1524673+P1532467;
    fvpart[272] = P1234657+P1236457+P1236547+P1236574+P1243657+P1246357+P1246537+P1246573+P1263457
    +P1263547+P1263574+P1264357+P1264537+P1264573+P1265347+P1265374+P1265437+P1265473
    +P1265734+P1265743+P1324657+P1326457+P1326547+P1326574+P1342657+P1423657+P1426357
    +P1426537+P1426573+P1432657;
    fvpart[273] = P1234567+P1235467+P1235647+P1235674+P1243567+P1245367+P1245637+P1245673+P1253467
    +P1253647+P1253674+P1254367+P1254637+P1254673+P1256347+P1256374+P1256437+P1256473
    +P1256734+P1256743+P1324567+P1325467+P1325647+P1325674+P1342567+P1423567+P1425367
    +P1425637+P1425673+P1432567;
#else
    std::cout << "Warning: too large norderL, recompile " __FILE__ " with --enable-debug" << std::endl;
#endif
    }}
  }
  if (Nf != 0.) {
    const int norder = norderF;

    if (norder >= 0) {
    // [0] order 120 primitives
    const LT P1234567 = AF(0,1,2,3,4,5,6);
    const LT P1234576 = AF(0,1,2,3,4,6,5);
    const LT P1234657 = AF(0,1,2,3,5,4,6);
    const LT P1234675 = AF(0,1,2,3,5,6,4);
    const LT P1234756 = AF(0,1,2,3,6,4,5);
    const LT P1234765 = AF(0,1,2,3,6,5,4);
    const LT P1235467 = AF(0,1,2,4,3,5,6);
    const LT P1235476 = AF(0,1,2,4,3,6,5);
    const LT P1235647 = AF(0,1,2,4,5,3,6);
    const LT P1235674 = AF(0,1,2,4,5,6,3);
    const LT P1235746 = AF(0,1,2,4,6,3,5);
    const LT P1235764 = AF(0,1,2,4,6,5,3);
    const LT P1236457 = AF(0,1,2,5,3,4,6);
    const LT P1236475 = AF(0,1,2,5,3,6,4);
    const LT P1236547 = AF(0,1,2,5,4,3,6);
    const LT P1236574 = AF(0,1,2,5,4,6,3);
    const LT P1236745 = AF(0,1,2,5,6,3,4);
    const LT P1236754 = AF(0,1,2,5,6,4,3);
    const LT P1237456 = AF(0,1,2,6,3,4,5);
    const LT P1237465 = AF(0,1,2,6,3,5,4);
    const LT P1237546 = AF(0,1,2,6,4,3,5);
    const LT P1237564 = AF(0,1,2,6,4,5,3);
    const LT P1237645 = AF(0,1,2,6,5,3,4);
    const LT P1237654 = AF(0,1,2,6,5,4,3);
    const LT P1243567 = AF(0,1,3,2,4,5,6);
    const LT P1243576 = AF(0,1,3,2,4,6,5);
    const LT P1243657 = AF(0,1,3,2,5,4,6);
    const LT P1243675 = AF(0,1,3,2,5,6,4);
    const LT P1243756 = AF(0,1,3,2,6,4,5);
    const LT P1243765 = AF(0,1,3,2,6,5,4);
    const LT P1245367 = AF(0,1,3,4,2,5,6);
    const LT P1245376 = AF(0,1,3,4,2,6,5);
    const LT P1245637 = AF(0,1,3,4,5,2,6);
    const LT P1245673 = AF(0,1,3,4,5,6,2);
    const LT P1245736 = AF(0,1,3,4,6,2,5);
    const LT P1245763 = AF(0,1,3,4,6,5,2);
    const LT P1246357 = AF(0,1,3,5,2,4,6);
    const LT P1246375 = AF(0,1,3,5,2,6,4);
    const LT P1246537 = AF(0,1,3,5,4,2,6);
    const LT P1246573 = AF(0,1,3,5,4,6,2);
    const LT P1246735 = AF(0,1,3,5,6,2,4);
    const LT P1246753 = AF(0,1,3,5,6,4,2);
    const LT P1247356 = AF(0,1,3,6,2,4,5);
    const LT P1247365 = AF(0,1,3,6,2,5,4);
    const LT P1247536 = AF(0,1,3,6,4,2,5);
    const LT P1247563 = AF(0,1,3,6,4,5,2);
    const LT P1247635 = AF(0,1,3,6,5,2,4);
    const LT P1247653 = AF(0,1,3,6,5,4,2);
    const LT P1253467 = AF(0,1,4,2,3,5,6);
    const LT P1253476 = AF(0,1,4,2,3,6,5);
    const LT P1253647 = AF(0,1,4,2,5,3,6);
    const LT P1253674 = AF(0,1,4,2,5,6,3);
    const LT P1253746 = AF(0,1,4,2,6,3,5);
    const LT P1253764 = AF(0,1,4,2,6,5,3);
    const LT P1254367 = AF(0,1,4,3,2,5,6);
    const LT P1254376 = AF(0,1,4,3,2,6,5);
    const LT P1254637 = AF(0,1,4,3,5,2,6);
    const LT P1254673 = AF(0,1,4,3,5,6,2);
    const LT P1254736 = AF(0,1,4,3,6,2,5);
    const LT P1254763 = AF(0,1,4,3,6,5,2);
    const LT P1256347 = AF(0,1,4,5,2,3,6);
    const LT P1256374 = AF(0,1,4,5,2,6,3);
    const LT P1256437 = AF(0,1,4,5,3,2,6);
    const LT P1256473 = AF(0,1,4,5,3,6,2);
    const LT P1256734 = AF(0,1,4,5,6,2,3);
    const LT P1256743 = AF(0,1,4,5,6,3,2);
    const LT P1257346 = AF(0,1,4,6,2,3,5);
    const LT P1257364 = AF(0,1,4,6,2,5,3);
    const LT P1257436 = AF(0,1,4,6,3,2,5);
    const LT P1257463 = AF(0,1,4,6,3,5,2);
    const LT P1257634 = AF(0,1,4,6,5,2,3);
    const LT P1257643 = AF(0,1,4,6,5,3,2);
    const LT P1263457 = AF(0,1,5,2,3,4,6);
    const LT P1263475 = AF(0,1,5,2,3,6,4);
    const LT P1263547 = AF(0,1,5,2,4,3,6);
    const LT P1263574 = AF(0,1,5,2,4,6,3);
    const LT P1263745 = AF(0,1,5,2,6,3,4);
    const LT P1263754 = AF(0,1,5,2,6,4,3);
    const LT P1264357 = AF(0,1,5,3,2,4,6);
    const LT P1264375 = AF(0,1,5,3,2,6,4);
    const LT P1264537 = AF(0,1,5,3,4,2,6);
    const LT P1264573 = AF(0,1,5,3,4,6,2);
    const LT P1264735 = AF(0,1,5,3,6,2,4);
    const LT P1264753 = AF(0,1,5,3,6,4,2);
    const LT P1265347 = AF(0,1,5,4,2,3,6);
    const LT P1265374 = AF(0,1,5,4,2,6,3);
    const LT P1265437 = AF(0,1,5,4,3,2,6);
    const LT P1265473 = AF(0,1,5,4,3,6,2);
    const LT P1265734 = AF(0,1,5,4,6,2,3);
    const LT P1265743 = AF(0,1,5,4,6,3,2);
    const LT P1267345 = AF(0,1,5,6,2,3,4);
    const LT P1267354 = AF(0,1,5,6,2,4,3);
    const LT P1267435 = AF(0,1,5,6,3,2,4);
    const LT P1267453 = AF(0,1,5,6,3,4,2);
    const LT P1267534 = AF(0,1,5,6,4,2,3);
    const LT P1267543 = AF(0,1,5,6,4,3,2);
    const LT P1273456 = AF(0,1,6,2,3,4,5);
    const LT P1273465 = AF(0,1,6,2,3,5,4);
    const LT P1273546 = AF(0,1,6,2,4,3,5);
    const LT P1273564 = AF(0,1,6,2,4,5,3);
    const LT P1273645 = AF(0,1,6,2,5,3,4);
    const LT P1273654 = AF(0,1,6,2,5,4,3);
    const LT P1274356 = AF(0,1,6,3,2,4,5);
    const LT P1274365 = AF(0,1,6,3,2,5,4);
    const LT P1274536 = AF(0,1,6,3,4,2,5);
    const LT P1274563 = AF(0,1,6,3,4,5,2);
    const LT P1274635 = AF(0,1,6,3,5,2,4);
    const LT P1274653 = AF(0,1,6,3,5,4,2);
    const LT P1275346 = AF(0,1,6,4,2,3,5);
    const LT P1275364 = AF(0,1,6,4,2,5,3);
    const LT P1275436 = AF(0,1,6,4,3,2,5);
    const LT P1275463 = AF(0,1,6,4,3,5,2);
    const LT P1275634 = AF(0,1,6,4,5,2,3);
    const LT P1275643 = AF(0,1,6,4,5,3,2);
    const LT P1276345 = AF(0,1,6,5,2,3,4);
    const LT P1276354 = AF(0,1,6,5,2,4,3);
    const LT P1276435 = AF(0,1,6,5,3,2,4);
    const LT P1276453 = AF(0,1,6,5,3,4,2);
    const LT P1276534 = AF(0,1,6,5,4,2,3);
    const LT P1276543 = AF(0,1,6,5,4,3,2);
    fvpart[0] += Nf*(-P1234567);
    fvpart[1] += Nf*(-P1234576);
    fvpart[2] += Nf*(-P1234657);
    fvpart[3] += Nf*(-P1234675);
    fvpart[4] += Nf*(-P1234756);
    fvpart[5] += Nf*(-P1234765);
    fvpart[6] += Nf*(-P1235467);
    fvpart[7] += Nf*(-P1235476);
    fvpart[8] += Nf*(-P1235647);
    fvpart[9] += Nf*(-P1235674);
    fvpart[10] += Nf*(-P1235746);
    fvpart[11] += Nf*(-P1235764);
    fvpart[12] += Nf*(-P1236457);
    fvpart[13] += Nf*(-P1236475);
    fvpart[14] += Nf*(-P1236547);
    fvpart[15] += Nf*(-P1236574);
    fvpart[16] += Nf*(-P1236745);
    fvpart[17] += Nf*(-P1236754);
    fvpart[18] += Nf*(-P1237456);
    fvpart[19] += Nf*(-P1237465);
    fvpart[20] += Nf*(-P1237546);
    fvpart[21] += Nf*(-P1237564);
    fvpart[22] += Nf*(-P1237645);
    fvpart[23] += Nf*(-P1237654);
    fvpart[24] += Nf*(-P1243567);
    fvpart[25] += Nf*(-P1243576);
    fvpart[26] += Nf*(-P1243657);
    fvpart[27] += Nf*(-P1243675);
    fvpart[28] += Nf*(-P1243756);
    fvpart[29] += Nf*(-P1243765);
    fvpart[30] += Nf*(-P1245367);
    fvpart[31] += Nf*(-P1245376);
    fvpart[32] += Nf*(-P1245637);
    fvpart[33] += Nf*(-P1245673);
    fvpart[34] += Nf*(-P1245736);
    fvpart[35] += Nf*(-P1245763);
    fvpart[36] += Nf*(-P1246357);
    fvpart[37] += Nf*(-P1246375);
    fvpart[38] += Nf*(-P1246537);
    fvpart[39] += Nf*(-P1246573);
    fvpart[40] += Nf*(-P1246735);
    fvpart[41] += Nf*(-P1246753);
    fvpart[42] += Nf*(-P1247356);
    fvpart[43] += Nf*(-P1247365);
    fvpart[44] += Nf*(-P1247536);
    fvpart[45] += Nf*(-P1247563);
    fvpart[46] += Nf*(-P1247635);
    fvpart[47] += Nf*(-P1247653);
    fvpart[48] += Nf*(-P1253467);
    fvpart[49] += Nf*(-P1253476);
    fvpart[50] += Nf*(-P1253647);
    fvpart[51] += Nf*(-P1253674);
    fvpart[52] += Nf*(-P1253746);
    fvpart[53] += Nf*(-P1253764);
    fvpart[54] += Nf*(-P1254367);
    fvpart[55] += Nf*(-P1254376);
    fvpart[56] += Nf*(-P1254637);
    fvpart[57] += Nf*(-P1254673);
    fvpart[58] += Nf*(-P1254736);
    fvpart[59] += Nf*(-P1254763);
    fvpart[60] += Nf*(-P1256347);
    fvpart[61] += Nf*(-P1256374);
    fvpart[62] += Nf*(-P1256437);
    fvpart[63] += Nf*(-P1256473);
    fvpart[64] += Nf*(-P1256734);
    fvpart[65] += Nf*(-P1256743);
    fvpart[66] += Nf*(-P1257346);
    fvpart[67] += Nf*(-P1257364);
    fvpart[68] += Nf*(-P1257436);
    fvpart[69] += Nf*(-P1257463);
    fvpart[70] += Nf*(-P1257634);
    fvpart[71] += Nf*(-P1257643);
    fvpart[72] += Nf*(-P1263457);
    fvpart[73] += Nf*(-P1263475);
    fvpart[74] += Nf*(-P1263547);
    fvpart[75] += Nf*(-P1263574);
    fvpart[76] += Nf*(-P1263745);
    fvpart[77] += Nf*(-P1263754);
    fvpart[78] += Nf*(-P1264357);
    fvpart[79] += Nf*(-P1264375);
    fvpart[80] += Nf*(-P1264537);
    fvpart[81] += Nf*(-P1264573);
    fvpart[82] += Nf*(-P1264735);
    fvpart[83] += Nf*(-P1264753);
    fvpart[84] += Nf*(-P1265347);
    fvpart[85] += Nf*(-P1265374);
    fvpart[86] += Nf*(-P1265437);
    fvpart[87] += Nf*(-P1265473);
    fvpart[88] += Nf*(-P1265734);
    fvpart[89] += Nf*(-P1265743);
    fvpart[90] += Nf*(-P1267345);
    fvpart[91] += Nf*(-P1267354);
    fvpart[92] += Nf*(-P1267435);
    fvpart[93] += Nf*(-P1267453);
    fvpart[94] += Nf*(-P1267534);
    fvpart[95] += Nf*(-P1267543);
    fvpart[96] += Nf*(-P1273456);
    fvpart[97] += Nf*(-P1273465);
    fvpart[98] += Nf*(-P1273546);
    fvpart[99] += Nf*(-P1273564);
    fvpart[100] += Nf*(-P1273645);
    fvpart[101] += Nf*(-P1273654);
    fvpart[102] += Nf*(-P1274356);
    fvpart[103] += Nf*(-P1274365);
    fvpart[104] += Nf*(-P1274536);
    fvpart[105] += Nf*(-P1274563);
    fvpart[106] += Nf*(-P1274635);
    fvpart[107] += Nf*(-P1274653);
    fvpart[108] += Nf*(-P1275346);
    fvpart[109] += Nf*(-P1275364);
    fvpart[110] += Nf*(-P1275436);
    fvpart[111] += Nf*(-P1275463);
    fvpart[112] += Nf*(-P1275634);
    fvpart[113] += Nf*(-P1275643);
    fvpart[114] += Nf*(-P1276345);
    fvpart[115] += Nf*(-P1276354);
    fvpart[116] += Nf*(-P1276435);
    fvpart[117] += Nf*(-P1276453);
    fvpart[118] += Nf*(-P1276534);
    fvpart[119] += Nf*(-P1276543);

    if (norder >= 1) {
#ifndef NDEBUG
    // [-1] order 116 primitives
    const LT P1324567 = AF(0,2,1,3,4,5,6);
    const LT P1324576 = AF(0,2,1,3,4,6,5);
    const LT P1324657 = AF(0,2,1,3,5,4,6);
    const LT P1324675 = AF(0,2,1,3,5,6,4);
    const LT P1324756 = AF(0,2,1,3,6,4,5);
    const LT P1324765 = AF(0,2,1,3,6,5,4);
    const LT P1325467 = AF(0,2,1,4,3,5,6);
    const LT P1325476 = AF(0,2,1,4,3,6,5);
    const LT P1325647 = AF(0,2,1,4,5,3,6);
    const LT P1325674 = AF(0,2,1,4,5,6,3);
    const LT P1325746 = AF(0,2,1,4,6,3,5);
    const LT P1325764 = AF(0,2,1,4,6,5,3);
    const LT P1326457 = AF(0,2,1,5,3,4,6);
    const LT P1326475 = AF(0,2,1,5,3,6,4);
    const LT P1326547 = AF(0,2,1,5,4,3,6);
    const LT P1326574 = AF(0,2,1,5,4,6,3);
    const LT P1326745 = AF(0,2,1,5,6,3,4);
    const LT P1326754 = AF(0,2,1,5,6,4,3);
    const LT P1327456 = AF(0,2,1,6,3,4,5);
    const LT P1327465 = AF(0,2,1,6,3,5,4);
    const LT P1327546 = AF(0,2,1,6,4,3,5);
    const LT P1327564 = AF(0,2,1,6,4,5,3);
    const LT P1327645 = AF(0,2,1,6,5,3,4);
    const LT P1327654 = AF(0,2,1,6,5,4,3);
    const LT P1342567 = AF(0,2,3,1,4,5,6);
    const LT P1342576 = AF(0,2,3,1,4,6,5);
    const LT P1342657 = AF(0,2,3,1,5,4,6);
    const LT P1342675 = AF(0,2,3,1,5,6,4);
    const LT P1342756 = AF(0,2,3,1,6,4,5);
    const LT P1342765 = AF(0,2,3,1,6,5,4);
    const LT P1346257 = AF(0,2,3,5,1,4,6);
    const LT P1347256 = AF(0,2,3,6,1,4,5);
    const LT P1352467 = AF(0,2,4,1,3,5,6);
    const LT P1352476 = AF(0,2,4,1,3,6,5);
    const LT P1352647 = AF(0,2,4,1,5,3,6);
    const LT P1352674 = AF(0,2,4,1,5,6,3);
    const LT P1352746 = AF(0,2,4,1,6,3,5);
    const LT P1352764 = AF(0,2,4,1,6,5,3);
    const LT P1356247 = AF(0,2,4,5,1,3,6);
    const LT P1357246 = AF(0,2,4,6,1,3,5);
    const LT P1362457 = AF(0,2,5,1,3,4,6);
    const LT P1362475 = AF(0,2,5,1,3,6,4);
    const LT P1362547 = AF(0,2,5,1,4,3,6);
    const LT P1362574 = AF(0,2,5,1,4,6,3);
    const LT P1362745 = AF(0,2,5,1,6,3,4);
    const LT P1362754 = AF(0,2,5,1,6,4,3);
    const LT P1365247 = AF(0,2,5,4,1,3,6);
    const LT P1367245 = AF(0,2,5,6,1,3,4);
    const LT P1372456 = AF(0,2,6,1,3,4,5);
    const LT P1372465 = AF(0,2,6,1,3,5,4);
    const LT P1372546 = AF(0,2,6,1,4,3,5);
    const LT P1372564 = AF(0,2,6,1,4,5,3);
    const LT P1372645 = AF(0,2,6,1,5,3,4);
    const LT P1372654 = AF(0,2,6,1,5,4,3);
    const LT P1375246 = AF(0,2,6,4,1,3,5);
    const LT P1376245 = AF(0,2,6,5,1,3,4);
    const LT P1423567 = AF(0,3,1,2,4,5,6);
    const LT P1423576 = AF(0,3,1,2,4,6,5);
    const LT P1423657 = AF(0,3,1,2,5,4,6);
    const LT P1423675 = AF(0,3,1,2,5,6,4);
    const LT P1423756 = AF(0,3,1,2,6,4,5);
    const LT P1423765 = AF(0,3,1,2,6,5,4);
    const LT P1425367 = AF(0,3,1,4,2,5,6);
    const LT P1425376 = AF(0,3,1,4,2,6,5);
    const LT P1425637 = AF(0,3,1,4,5,2,6);
    const LT P1425673 = AF(0,3,1,4,5,6,2);
    const LT P1425736 = AF(0,3,1,4,6,2,5);
    const LT P1425763 = AF(0,3,1,4,6,5,2);
    const LT P1426357 = AF(0,3,1,5,2,4,6);
    const LT P1426375 = AF(0,3,1,5,2,6,4);
    const LT P1426537 = AF(0,3,1,5,4,2,6);
    const LT P1426573 = AF(0,3,1,5,4,6,2);
    const LT P1426735 = AF(0,3,1,5,6,2,4);
    const LT P1426753 = AF(0,3,1,5,6,4,2);
    const LT P1427356 = AF(0,3,1,6,2,4,5);
    const LT P1427365 = AF(0,3,1,6,2,5,4);
    const LT P1427536 = AF(0,3,1,6,4,2,5);
    const LT P1427563 = AF(0,3,1,6,4,5,2);
    const LT P1427635 = AF(0,3,1,6,5,2,4);
    const LT P1427653 = AF(0,3,1,6,5,4,2);
    const LT P1432567 = AF(0,3,2,1,4,5,6);
    const LT P1432576 = AF(0,3,2,1,4,6,5);
    const LT P1432657 = AF(0,3,2,1,5,4,6);
    const LT P1432675 = AF(0,3,2,1,5,6,4);
    const LT P1432756 = AF(0,3,2,1,6,4,5);
    const LT P1452367 = AF(0,3,4,1,2,5,6);
    const LT P1452376 = AF(0,3,4,1,2,6,5);
    const LT P1452637 = AF(0,3,4,1,5,2,6);
    const LT P1462357 = AF(0,3,5,1,2,4,6);
    const LT P1462375 = AF(0,3,5,1,2,6,4);
    const LT P1462537 = AF(0,3,5,1,4,2,6);
    const LT P1472356 = AF(0,3,6,1,2,4,5);
    const LT P1472365 = AF(0,3,6,1,2,5,4);
    const LT P1472536 = AF(0,3,6,1,4,2,5);
    const LT P1523467 = AF(0,4,1,2,3,5,6);
    const LT P1523476 = AF(0,4,1,2,3,6,5);
    const LT P1523647 = AF(0,4,1,2,5,3,6);
    const LT P1523674 = AF(0,4,1,2,5,6,3);
    const LT P1523746 = AF(0,4,1,2,6,3,5);
    const LT P1523764 = AF(0,4,1,2,6,5,3);
    const LT P1524367 = AF(0,4,1,3,2,5,6);
    const LT P1524376 = AF(0,4,1,3,2,6,5);
    const LT P1524637 = AF(0,4,1,3,5,2,6);
    const LT P1524673 = AF(0,4,1,3,5,6,2);
    const LT P1526347 = AF(0,4,1,5,2,3,6);
    const LT P1526734 = AF(0,4,1,5,6,2,3);
    const LT P1526743 = AF(0,4,1,5,6,3,2);
    const LT P1527346 = AF(0,4,1,6,2,3,5);
    const LT P1562347 = AF(0,4,5,1,2,3,6);
    const LT P1572346 = AF(0,4,6,1,2,3,5);
    const LT P1623457 = AF(0,5,1,2,3,4,6);
    const LT P1623547 = AF(0,5,1,2,4,3,6);
    const LT P1623745 = AF(0,5,1,2,6,3,4);
    const LT P1624357 = AF(0,5,1,3,2,4,6);
    const LT P1624537 = AF(0,5,1,3,4,2,6);
    const LT P1625347 = AF(0,5,1,4,2,3,6);
    fvpart[120] += Nf*((-P1234765+P1236574-P1236745-P1236754+P1243657-P1243756-P1243765+P1245763+P1254763
    +P1256347+P1256734+P1256743-P1257346+P1263475+P1264375+P1265734+P1265743-P1324675
    +P1325674+P1326574-P1326745-P1326754-P1342675-P1346257+P1347256-P1362457-P1362475
    -P1362547-P1365247+P1375246+P1423657-P1423756-P1423765+P1425763+P1432657-P1432675
    -P1432756+(P1235647-P1235746+P1235764-P1236457-P1236475-P1236547+P1237456+P1237465
    +P1237546-P1237564+P1237645+P1237654-P1245637-P1245673+P1245736+P1246357+P1246375
    +P1246537+P1246573-P1246735-P1246753-P1247356-P1247365-P1247536+P1253647-P1253746
    +P1253764-P1254637-P1254673+P1254736+P1256374-P1256473-P1257364+P1257463+P1263574
    -P1263745-P1263754-P1264573+P1264735+P1264753+P1265374-P1265473-P1273564+P1273645
    +P1273654+P1274563-P1274635-P1274653-P1275364+P1275463+P1325647-P1325746-P1325764
    -P1326457-P1326475-P1326547+P1327456+P1327465+P1327546-P1327564+P1327645+P1327654
    +P1372456+P1372465+P1372546-P1425637-P1425673+P1425736+P1426357+P1426375+P1426537
    +P1426573-P1426735-P1426753-P1427356-P1427365-P1427536-P1462357+P1462375-P1462537)*0.5
    -P1472365-P1523674+P1523764+P1526347+P1526734+P1526743-P1527346+P1562347-P1572346)/Nc);
    fvpart[121] += Nf*((P1234765-P1235476-P1235764+P1237564+P1237654+P1243567+P1243576+P1243756+P1243765
    +P1245376-P1245673-P1246573+P1246735+P1246753-P1253764-P1256734+P1257346-P1264375
    +P1265347-P1265734-P1267354+P1267435+P1267453-P1267534-P1324567+P1324756-P1325764
    +P1327564+P1346257-P1347256+P1365247-P1375246+P1423567+P1423756+P1423765-P1425673
    -P1426573+2.*P1432756+P1462357+P1462537+(-P1235647+3.*P1235674+P1235746+P1236457+P1236475
    +P1236547-P1236574-P1236745-P1236754-P1237456+P1237465+P1237546+P1245637+P1245736
    +P1245763-P1246357-P1246375-P1246537+P1247356-P1247365+P1247536+P1247563-P1247635
    -P1247653-P1253647+P1253674+P1253746+P1254637+P1254736+P1254763+P1256374-P1256473
    +P1257364-P1257463-P1263574-P1263745-P1263754-P1264573-P1264735-P1264753-P1265374
    +P1265473+P1273564-P1273645-P1273654-P1274563+P1274635+P1274653+P1275364-P1275463
    -P1325647+P1325674-P1325746+P1326457-P1326475+3.*P1326547+P1326574+P1326745-P1326754
    +P1327456+P1327465+P1327546-P1342567-P1342576+P1342657-P1342675+P1342756-P1342765
    +P1362457+P1362475+P1362547+P1372456+P1372465+P1372546+P1372564+P1372645+P1372654
    +P1425637+P1425736+P1425763-P1426357-P1426375-P1426537+P1427356-P1427365+P1427536
    +P1427563-P1427635-P1427653-P1472356+P1472365-P1472536)*0.5+P1523746+P1524673-P1526734
    +P1527346-P1562347+P1572346-P1623547-P1623745+P1624357+P1624537)/Nc);
    fvpart[122] += Nf*((P1234567+P1234576+P1235467+P1235476+P1235674+P1236574+P1243567+P1243576+P1243657
    -P1245367-P1245376-P1247563+P1247635-P1247653-P1253467-P1253476-P1253674-P1254367
    -P1254376+P1256347-P1263457-P1263547-P1264357+P1264537+P1265347+P1265437+P1267345
    +P1267354+P1267435-P1267453-P1267534-P1267543-P1273456-P1273465-P1273546-P1274356
    -P1274365+P1274536+P1275346+P1275436-P1275634-P1275643+P1276345+P1276354+P1276435
    -P1276453-P1276534-P1276543+P1324657+P1324675+P1326574+P1326745+P1326754-P1342567
    -P1342576+P1342675+P1346257-P1347256+P1365247-P1375246+P1423765-P1432567-P1432576
    +P1432657+P1432675+3.*(P1234765+P1243756+P1243765-P1256734-P1256743+P1257346+P1362457
    +P1362475+P1362547+P1423756+P1432756)+(P1235647+3.*P1235746-P1235764+3.*P1236457+P1236475
    +3.*P1236547+P1237456+P1237465+P1237546+5.*P1237564+3.*P1237645+3.*P1237654+3.*P1245637
    -3.*P1245673+P1245736-3.*P1246357-P1246375+P1246537-3.*P1246573+5.*P1246735+P1246753-P1247356
    -P1247365+3.*P1247536-3.*P1253647-P1253746-5.*P1253764+3.*P1254637-3.*P1254673+P1254736
    +P1256374-P1256473+P1257364-P1257463-3.*P1263574-P1263745-P1263754-P1264573+P1264735
    -3.*P1264753+P1265374-P1265473-P1273564-3.*P1273645-3.*P1273654-3.*P1274563+3.*P1274635
    -P1274653+3.*P1275364-3.*P1275463+3.*P1325647+3.*P1325746-3.*P1325764+5.*P1326457+P1326475
    +5.*P1326547+3.*P1327456+3.*P1327465+3.*P1327546+3.*P1327564+P1327645+P1327654+5.*P1372456
    +5.*P1372465+5.*P1372546+P1425637-7.*P1425673+P1425736-P1426357-P1426375+3.*P1426537-3.*P1426573
    +3.*P1426735-P1426753+P1427356-3.*P1427365+5.*P1427536+3.*P1462357+P1462375+3.*P1462537)*0.5
    +P1472365+P1523467+P1523476-P1523647+P1523674+P1523764+P1524367+P1524376+P1524637
    +2.*(P1234657+P1234675+P1234756+P1236745+P1236754+P1243675-P1245763-P1254763+P1256437
    +P1257436-P1257634-P1257643-P1263475-P1264375-P1265734-P1265743+P1324756+P1324765
    +P1342657+P1342756+P1352467+P1352476+P1352647+P1352674+P1352746+P1352764+P1362574
    +P1362745+P1362754+P1372564+P1372645+P1372654+P1423657+P1423675-P1425367-P1425376
    -P1425763-P1427653+P1524673)-P1526347-P1526734-P1526743+P1527346-P1562347+P1572346)/Nc);
    fvpart[123] += Nf*((P1235647+P1235674-P1236745-P1236754+P1245763+P1246537-P1247653+P1254763-P1273564
    -P1274653+P1325647-P1326475+P1326547+P1326574-P1326745-P1346257+P1347256+P1352467
    +P1352476+P1352647+P1352674+P1352746+P1352764+P1362574+P1362745+P1362754-P1365247
    +P1375246+P1425763+P1426537-P1427653+P1432657+(3.*P1234567+3.*P1234576+P1234657+P1234675
    +P1234756-P1234765+P1235467+P1235476+P1235746+3.*P1235764+P1237456+3.*P1237465+3.*P1237546
    +P1237564+3.*P1237645+5.*P1237654+P1243567+P1243576+P1243657+P1243675-P1243756-P1243765
    -P1245367-P1245376-3.*P1245673+3.*P1245736-P1246573-P1246735-3.*P1246753-P1247356-3.*P1247365
    +P1247536-P1253467-P1253476-P1253746+P1253764-P1254367-P1254376-3.*P1254673+3.*P1254736
    +P1256347+3.*P1256374+P1256437-3.*P1256473+3.*P1256734+P1256743-P1257346+P1257436-P1257634
    -P1257643-P1263457-P1263475-P1263547-P1263574-3.*P1263745-3.*P1263754-P1264357+P1264375
    +P1264537-3.*P1264573+P1264735-P1264753+P1265347+P1265374+3.*P1265437-P1265473+P1265734
    -P1265743+3.*P1267345+P1267354+P1267435-P1267453-P1267534-3.*P1267543-P1273456-P1273465
    -P1273546-P1274356-P1274365+P1274536+P1275346+P1275436-P1275634-P1275643+P1276345
    +P1276354+P1276435-P1276453-P1276534-P1276543+P1324567+P1324576+P1324657-3.*P1324675
    +3.*P1324756+P1324765+P1325467+P1325476-P1325746-P1325764+3.*P1327456+3.*P1327465+3.*P1327546
    +P1327564+3.*P1327645+3.*P1327654+P1342567+P1342576+P1342657-3.*P1342675+3.*P1342756+P1342765
    +3.*P1372564+3.*P1372645+3.*P1372654+P1423567+P1423576+P1423657+P1423675-P1423756-P1423765
    -P1425367-P1425376-3.*P1425673+3.*P1425736-P1426573-P1426735-3.*P1426753-P1427356-3.*P1427365
    +P1427536-P1462357+P1462375-P1462537)*0.5-P1472365-P1523674+P1523746+2.*(P1325674-P1326754
    +P1372456+P1372465+P1372546-P1432675+P1523764+P1526734)+P1526743-P1527346+P1562347
    -P1572346+P1623457-P1623745)/Nc);
    fvpart[124] += Nf*((P1235647+P1235674-P1236745-P1236754+P1245763+P1246537-P1247653+P1254763-P1273564
    -P1274653+P1325647-P1326475+P1326547+P1326574-P1326745-P1346257+P1347256+P1352467
    +P1352476+P1352647+P1352674+P1352746+P1352764+P1362574+P1362745+P1362754-P1365247
    +P1375246+P1425763+P1426537-P1427653+P1432657+(3.*P1234567+3.*P1234576+P1234657+P1234675
    +P1234756-P1234765+P1235467+P1235476+P1235746+3.*P1235764+P1237456+3.*P1237465+3.*P1237546
    +P1237564+3.*P1237645+5.*P1237654+P1243567+P1243576+P1243657+P1243675-P1243756-P1243765
    -P1245367-P1245376-3.*P1245673+3.*P1245736-P1246573-P1246735-3.*P1246753-P1247356-3.*P1247365
    +P1247536-P1253467-P1253476-P1253746+P1253764-P1254367-P1254376-3.*P1254673+3.*P1254736
    +P1256347+3.*P1256374+P1256437-3.*P1256473+3.*P1256734+P1256743-P1257346+P1257436-P1257634
    -P1257643-P1263457-P1263475-P1263547-P1263574-3.*P1263745-3.*P1263754-P1264357+P1264375
    +P1264537-3.*P1264573+P1264735-P1264753+P1265347+P1265374+3.*P1265437-P1265473+P1265734
    -P1265743+3.*P1267345+P1267354+P1267435-P1267453-P1267534-3.*P1267543-P1273456-P1273465
    -P1273546-P1274356-P1274365+P1274536+P1275346+P1275436-P1275634-P1275643+P1276345
    +P1276354+P1276435-P1276453-P1276534-P1276543+P1324567+P1324576+P1324657-3.*P1324675
    +3.*P1324756+P1324765+P1325467+P1325476-P1325746-P1325764+3.*P1327456+3.*P1327465+3.*P1327546
    +P1327564+3.*P1327645+3.*P1327654+P1342567+P1342576+P1342657-3.*P1342675+3.*P1342756+P1342765
    +3.*P1372564+3.*P1372645+3.*P1372654+P1423567+P1423576+P1423657+P1423675-P1423756-P1423765
    -P1425367-P1425376-3.*P1425673+3.*P1425736-P1426573-P1426735-3.*P1426753-P1427356-3.*P1427365
    +P1427536-P1462357+P1462375-P1462537)*0.5-P1472365-P1523674+P1523746+2.*(P1325674-P1326754
    +P1372456+P1372465+P1372546-P1432675+P1523764+P1526734)+P1526743-P1527346+P1562347
    -P1572346+P1623457-P1623745)/Nc);
    fvpart[125] += Nf*((P1234567+P1234576+P1235467+P1235476+P1235674+P1236574+P1243567+P1243576+P1243657
    -P1245367-P1245376-P1247563+P1247635-P1247653-P1253467-P1253476-P1253674-P1254367
    -P1254376+P1256347-P1263457-P1263547-P1264357+P1264537+P1265347+P1265437+P1267345
    +P1267354+P1267435-P1267453-P1267534-P1267543-P1273456-P1273465-P1273546-P1274356
    -P1274365+P1274536+P1275346+P1275436-P1275634-P1275643+P1276345+P1276354+P1276435
    -P1276453-P1276534-P1276543+P1324657+P1324675+P1326574+P1326745+P1326754-P1342567
    -P1342576+P1342675+P1346257-P1347256+P1365247-P1375246+P1423765-P1432567-P1432576
    +P1432657+P1432675+3.*(P1234765+P1243756+P1243765-P1256734-P1256743+P1257346+P1362457
    +P1362475+P1362547+P1423756+P1432756)+(P1235647+3.*P1235746-P1235764+3.*P1236457+P1236475
    +3.*P1236547+P1237456+P1237465+P1237546+5.*P1237564+3.*P1237645+3.*P1237654+3.*P1245637
    -3.*P1245673+P1245736-3.*P1246357-P1246375+P1246537-3.*P1246573+5.*P1246735+P1246753-P1247356
    -P1247365+3.*P1247536-3.*P1253647-P1253746-5.*P1253764+3.*P1254637-3.*P1254673+P1254736
    +P1256374-P1256473+P1257364-P1257463-3.*P1263574-P1263745-P1263754-P1264573+P1264735
    -3.*P1264753+P1265374-P1265473-P1273564-3.*P1273645-3.*P1273654-3.*P1274563+3.*P1274635
    -P1274653+3.*P1275364-3.*P1275463+3.*P1325647+3.*P1325746-3.*P1325764+5.*P1326457+P1326475
    +5.*P1326547+3.*P1327456+3.*P1327465+3.*P1327546+3.*P1327564+P1327645+P1327654+5.*P1372456
    +5.*P1372465+5.*P1372546+P1425637-7.*P1425673+P1425736-P1426357-P1426375+3.*P1426537-3.*P1426573
    +3.*P1426735-P1426753+P1427356-3.*P1427365+5.*P1427536+3.*P1462357+P1462375+3.*P1462537)*0.5
    +P1472365+P1523467+P1523476-P1523647+P1523674+P1523764+P1524367+P1524376+P1524637
    +2.*(P1234657+P1234675+P1234756+P1236745+P1236754+P1243675-P1245763-P1254763+P1256437
    +P1257436-P1257634-P1257643-P1263475-P1264375-P1265734-P1265743+P1324756+P1324765
    +P1342657+P1342756+P1352467+P1352476+P1352647+P1352674+P1352746+P1352764+P1362574
    +P1362745+P1362754+P1372564+P1372645+P1372654+P1423657+P1423675-P1425367-P1425376
    -P1425763-P1427653+P1524673)-P1526347-P1526734-P1526743+P1527346-P1562347+P1572346)/Nc);
    fvpart[126] += Nf*((P1234765-P1235476-P1235764+P1237564+P1237654+P1243567+P1243576+P1243756+P1243765
    +P1245376-P1245673-P1246573+P1246735+P1246753-P1253764-P1256734+P1257346-P1264375
    +P1265347-P1265734-P1267354+P1267435+P1267453-P1267534-P1324567+P1324756-P1325764
    +P1327564+P1346257-P1347256+P1365247-P1375246+P1423567+P1423756+P1423765-P1425673
    -P1426573+2.*P1432756+P1462357+P1462537+(-P1235647+3.*P1235674+P1235746+P1236457+P1236475
    +P1236547-P1236574-P1236745-P1236754-P1237456+P1237465+P1237546+P1245637+P1245736
    +P1245763-P1246357-P1246375-P1246537+P1247356-P1247365+P1247536+P1247563-P1247635
    -P1247653-P1253647+P1253674+P1253746+P1254637+P1254736+P1254763+P1256374-P1256473
    +P1257364-P1257463-P1263574-P1263745-P1263754-P1264573-P1264735-P1264753-P1265374
    +P1265473+P1273564-P1273645-P1273654-P1274563+P1274635+P1274653+P1275364-P1275463
    -P1325647+P1325674-P1325746+P1326457-P1326475+3.*P1326547+P1326574+P1326745-P1326754
    +P1327456+P1327465+P1327546-P1342567-P1342576+P1342657-P1342675+P1342756-P1342765
    +P1362457+P1362475+P1362547+P1372456+P1372465+P1372546+P1372564+P1372645+P1372654
    +P1425637+P1425736+P1425763-P1426357-P1426375-P1426537+P1427356-P1427365+P1427536
    +P1427563-P1427635-P1427653-P1472356+P1472365-P1472536)*0.5+P1523746+P1524673-P1526734
    +P1527346-P1562347+P1572346-P1623547-P1623745+P1624357+P1624537)/Nc);
    fvpart[127] += Nf*((-P1234765+P1236574-P1236745-P1236754+P1243657-P1243756-P1243765+P1245763+P1254763
    +P1256347+P1256734+P1256743-P1257346+P1263475+P1264375+P1265734+P1265743-P1324675
    +P1325674+P1326574-P1326745-P1326754-P1342675-P1346257+P1347256-P1362457-P1362475
    -P1362547-P1365247+P1375246+P1423657-P1423756-P1423765+P1425763+P1432657-P1432675
    -P1432756+(P1235647-P1235746+P1235764-P1236457-P1236475-P1236547+P1237456+P1237465
    +P1237546-P1237564+P1237645+P1237654-P1245637-P1245673+P1245736+P1246357+P1246375
    +P1246537+P1246573-P1246735-P1246753-P1247356-P1247365-P1247536+P1253647-P1253746
    +P1253764-P1254637-P1254673+P1254736+P1256374-P1256473-P1257364+P1257463+P1263574
    -P1263745-P1263754-P1264573+P1264735+P1264753+P1265374-P1265473-P1273564+P1273645
    +P1273654+P1274563-P1274635-P1274653-P1275364+P1275463+P1325647-P1325746-P1325764
    -P1326457-P1326475-P1326547+P1327456+P1327465+P1327546-P1327564+P1327645+P1327654
    +P1372456+P1372465+P1372546-P1425637-P1425673+P1425736+P1426357+P1426375+P1426537
    +P1426573-P1426735-P1426753-P1427356-P1427365-P1427536-P1462357+P1462375-P1462537)*0.5
    -P1472365-P1523674+P1523764+P1526347+P1526734+P1526743-P1527346+P1562347-P1572346)/Nc);
    fvpart[128] += Nf*((P1234675-P1235476+P1235674-P1236574+P1243576+P1245376+P1247563-P1247635-P1247653
    +P1253467+P1253674+P1264357-P1267354+P1267453+P1325647-3.*P1325764+P1326547+P1327456
    +P1327465+P1327546+P1327645+P1327654-P1342567-P1342576+P1342756+P1356247-P1357246
    -P1362457-P1362475-P1362547-P1362574-P1362745-P1362754+P1365247+P1372456+P1372465
    +P1372546-P1375246+P1427563-P1427635-P1427653-P1432567-P1432576+P1432756+(P1235647
    -P1235746-P1235764-P1236457-P1236475-P1236547-P1237456+P1237465+P1237546+P1237564
    +3.*P1237645+5.*P1237654-P1245637-5.*P1245673+P1245736+P1246357+P1246375+P1246537-P1246573
    +P1246735+3.*P1246753+P1247356-P1247365-P1247536+P1253647+P1253746-P1253764-P1254637
    -3.*P1254673+P1254736+P1256374-P1256473-P1257364+P1257463+P1263574-P1263745-P1263754
    -P1264573-P1264735+P1264753-P1265374+P1265473-P1273564+P1273645+P1273654+P1274563
    -P1274635-P1274653-P1275364+P1275463-3.*P1324567-P1324576-P1324657-3.*P1324675+3.*P1324756
    +P1324765-P1325467-P1325476+3.*P1325674+P1326574-3.*P1326745-5.*P1326754-P1352467-P1352476
    +P1352647-P1352674-3.*P1352746-P1352764+P1372564+P1372645+P1372654-P1425637-5.*P1425673
    +P1425736+P1426357+P1426375+P1426537-P1426573-P1426735+P1426753+P1427356-P1427365
    -P1427536+P1462357+P1462375+3.*P1462537)*0.5-P1472536+P1523467-P1523674+P1523746+2.*(
    -P1236745-P1236754+P1245763+P1254763-P1325746-P1326475-P1342675+P1425763-P1432675
    +P1523764)+P1524673-P1623547-P1623745+P1624357+P1624537)/Nc);
    fvpart[129] += Nf*((P1234576-P1236475-P1236754+P1237546+P1237645+P1243675+P1245736+P1246375+P1254367
    -P1254673-P1257364+P1257463+P1263457-P1263754-P1264573-P1324567-P1324657+P1324756
    +P1324765+P1325674-P1325746+P1326574+P1327546+P1327645-P1342567-P1342576+P1342756
    -P1362457-P1362475-P1362745-P1367245+P1372456+P1372465+P1372546-P1376245-P1426573
    +P1427356-P1427365-P1427635-P1432567-P1432576+P1432756+P1452637+(-P1235467-P1235476
    -P1235647-P1235764+P1236547-3.*P1236745-P1237456+P1237465+3.*P1237564+3.*P1237654+P1245367
    +P1245376+P1245637-3.*P1245673-P1246537-3.*P1246573+P1246753+P1247356-P1247365-P1247635
    +P1253674+P1253746+P1254736+3.*P1254763+P1256374-P1256473+P1263547-P1263745-P1264537
    -P1264735-P1265374+P1265473-P1267354+P1267453+P1273546+P1273564-P1273654-P1274536
    -P1274563+P1274653-P1276354+P1276453-P1325467-P1325476+P1325647-5.*P1325764+3.*P1326547
    -3.*P1326745+P1327456+3.*P1327465+P1327564+P1327654-P1352467-P1352476-P1352647-P1352674
    -P1352746-P1352764+P1372564+P1372645+P1372654-P1423567-P1423576+P1423657+P1423675
    +P1423756-P1423765+P1425736+3.*P1425763+P1426357+P1426375-P1426735+P1427536+P1427563
    -P1427653+P1462357+P1462375+P1462537)*0.5-P1523647-P1523674+P1523746+2.*(P1245763-P1324675
    -P1326475-P1326754-P1342675-P1425673-P1432675+P1523764)+P1524367+P1524637+P1524673
    +P1623457-P1623745)/Nc);
    fvpart[130] += Nf*((P1234576-P1236475-P1236754+P1237546+P1237645+P1243675+P1245736+P1246375+P1254367
    -P1254673-P1257364+P1257463+P1263457-P1263754-P1264573-P1324567-P1324657+P1324756
    +P1324765+P1325674-P1325746+P1326574+P1327546+P1327645-P1342567-P1342576+P1342756
    -P1362457-P1362475-P1362745-P1367245+P1372456+P1372465+P1372546-P1376245-P1426573
    +P1427356-P1427365-P1427635-P1432567-P1432576+P1432756+P1452637+(-P1235467-P1235476
    -P1235647-P1235764+P1236547-3.*P1236745-P1237456+P1237465+3.*P1237564+3.*P1237654+P1245367
    +P1245376+P1245637-3.*P1245673-P1246537-3.*P1246573+P1246753+P1247356-P1247365-P1247635
    +P1253674+P1253746+P1254736+3.*P1254763+P1256374-P1256473+P1263547-P1263745-P1264537
    -P1264735-P1265374+P1265473-P1267354+P1267453+P1273546+P1273564-P1273654-P1274536
    -P1274563+P1274653-P1276354+P1276453-P1325467-P1325476+P1325647-5.*P1325764+3.*P1326547
    -3.*P1326745+P1327456+3.*P1327465+P1327564+P1327654-P1352467-P1352476-P1352647-P1352674
    -P1352746-P1352764+P1372564+P1372645+P1372654-P1423567-P1423576+P1423657+P1423675
    +P1423756-P1423765+P1425736+3.*P1425763+P1426357+P1426375-P1426735+P1427536+P1427563
    -P1427653+P1462357+P1462375+P1462537)*0.5-P1523647-P1523674+P1523746+2.*(P1245763-P1324675
    -P1326475-P1326754-P1342675-P1425673-P1432675+P1523764)+P1524367+P1524637+P1524673
    +P1623457-P1623745)/Nc);
    fvpart[131] += Nf*((P1234675-P1235476+P1235674-P1236574+P1243576+P1245376+P1247563-P1247635-P1247653
    +P1253467+P1253674+P1264357-P1267354+P1267453+P1325647-3.*P1325764+P1326547+P1327456
    +P1327465+P1327546+P1327645+P1327654-P1342567-P1342576+P1342756+P1356247-P1357246
    -P1362457-P1362475-P1362547-P1362574-P1362745-P1362754+P1365247+P1372456+P1372465
    +P1372546-P1375246+P1427563-P1427635-P1427653-P1432567-P1432576+P1432756+(P1235647
    -P1235746-P1235764-P1236457-P1236475-P1236547-P1237456+P1237465+P1237546+P1237564
    +3.*P1237645+5.*P1237654-P1245637-5.*P1245673+P1245736+P1246357+P1246375+P1246537-P1246573
    +P1246735+3.*P1246753+P1247356-P1247365-P1247536+P1253647+P1253746-P1253764-P1254637
    -3.*P1254673+P1254736+P1256374-P1256473-P1257364+P1257463+P1263574-P1263745-P1263754
    -P1264573-P1264735+P1264753-P1265374+P1265473-P1273564+P1273645+P1273654+P1274563
    -P1274635-P1274653-P1275364+P1275463-3.*P1324567-P1324576-P1324657-3.*P1324675+3.*P1324756
    +P1324765-P1325467-P1325476+3.*P1325674+P1326574-3.*P1326745-5.*P1326754-P1352467-P1352476
    +P1352647-P1352674-3.*P1352746-P1352764+P1372564+P1372645+P1372654-P1425637-5.*P1425673
    +P1425736+P1426357+P1426375+P1426537-P1426573-P1426735+P1426753+P1427356-P1427365
    -P1427536+P1462357+P1462375+3.*P1462537)*0.5-P1472536+P1523467-P1523674+P1523746+2.*(
    -P1236745-P1236754+P1245763+P1254763-P1325746-P1326475-P1342675+P1425763-P1432675
    +P1523764)+P1524673-P1623547-P1623745+P1624357+P1624537)/Nc);
    fvpart[132] += Nf*((P1234567+P1234576+P1234657+P1234675+P1234756+P1234765+P1235467+P1235476+P1237456
    +P1237465+P1237546+P1243567+P1243576+P1243657+P1243675+P1243756+P1243765-P1245367
    -P1245376+P1245736-P1247356-P1247365-P1253467-P1253476-P1253746-P1254367-P1254376
    +P1254736+P1256347+P1256374+P1256437-P1256473-P1256734-P1256743+P1257346+P1257436
    -P1257634-P1257643-P1263457-P1263475-P1263547-P1263574-P1263745-P1263754-P1264357
    -P1264375+P1264537-P1264573+P1264735-P1264753+P1265347+P1265374+P1265437-P1265473
    -P1265734-P1265743+P1267345+P1267354+P1267435-P1267453-P1267534-P1267543-P1273456
    -P1273465-P1273546-P1274356-P1274365+P1274536+P1275346+P1275436-P1275634-P1275643
    +P1276345+P1276354+P1276435-P1276453-P1276534-P1276543+P1324567+P1324576+P1324756
    +P1325467+P1325476+P1327456+P1327546+P1342567+P1342576+P1342756+P1352647-P1356247
    +P1357246+3.*(P1352746+P1362457+P1362475+P1362547+P1362574+P1362745+P1362754)-P1365247
    +P1375246+P1423567+P1423576+P1423657+P1423675+P1423756+P1423765-P1425367-P1425376
    +P1425736-P1427356-P1427365+(P1235647+P1235674+3.*P1235764+3.*P1236457+5.*P1236475+3.*P1236547
    +3.*P1236574+5.*P1236745+5.*P1236754+P1237564-P1237645-P1237654+3.*P1245637+P1245673-5.*P1245763
    -P1246357-3.*P1246375+P1246537-P1246573+P1246735-3.*P1246753-3.*P1247563+3.*P1247635-P1247653
    -P1253647-3.*P1253674-P1253764+3.*P1254637+P1254673-5.*P1254763+3.*P1257364-3.*P1257463
    -P1273564-3.*P1273645-3.*P1273654-3.*P1274563+3.*P1274635-P1274653+3.*P1275364-3.*P1275463
    -P1325647-P1325674+5.*P1325764+3.*P1326457+5.*P1326475+3.*P1326547+3.*P1326574+5.*P1326745
    +5.*P1326754+3.*P1327564-P1327645-P1327654+3.*P1372456+3.*P1372465+3.*P1372546+3.*P1372564
    +3.*P1372645+3.*P1372654+3.*P1425637+P1425673-5.*P1425763-P1426357-3.*P1426375+P1426537
    -P1426573+P1426735-3.*P1426753-3.*P1427563+3.*P1427635-P1427653)*0.5+P1432567+P1432576
    +2.*(P1235746+P1247536+P1324657+P1324675+P1325746+P1342657+P1342675+P1352467+P1352476
    +P1352674+P1352764+P1427536+P1432657+P1432675)+P1432756-P1462537+P1472536+P1523647
    +P1523674-P1523764)/Nc);
    fvpart[133] += Nf*((-P1234567-P1234576-P1234657-P1234675-P1234756-P1234765-P1235467-P1235476-P1236457
    -P1236475-P1236547-P1243567-P1243576-P1243657-P1243675-P1243756-P1243765+P1245367
    +P1245376-P1245637+P1246357+P1246375+P1253467+P1253476+P1253647+P1254367+P1254376
    -P1254637-P1256347-P1256437+P1256734+P1256743-P1257346-P1257364-P1257436+P1257463
    +P1257634+P1257643+P1263457+P1263475+P1263547+P1264357+P1264375-P1264537-P1265347
    -P1265437+P1265734+P1265743-P1267345-P1267354-P1267435+P1267453+P1267534+P1267543
    +P1273456+P1273465+P1273546+P1273564+P1273645+P1273654+P1274356+P1274365-P1274536
    +P1274563-P1274635+P1274653-P1275346-P1275364-P1275436+P1275463+P1275634+P1275643
    -P1276345-P1276354-P1276435+P1276453+P1276534+P1276543-P1324567-P1324576-P1324657
    -P1325467-P1325476-P1326457-P1326547-P1342567-P1342576-P1342657-P1352647-3.*P1352746
    +P1356247-P1357246+P1365247-P1372456-P1372465-P1372546-P1372564-P1372645-P1372654
    -P1375246-P1423567-P1423576-P1423657-P1423675-P1423756-P1423765+P1425367+P1425376
    -P1425637+P1426357+P1426375+(-P1235674-3.*P1235746-3.*P1235764-3.*P1236574-5.*P1236745
    -5.*P1236754-P1237456+P1237465-P1237546-P1237564+P1237645+P1237654-P1245673-P1245736
    +5.*P1245763+P1246573-P1246735+3.*P1246753+3.*P1247356+P1247365-3.*P1247536+3.*P1247563
    -3.*P1247635+P1247653+3.*P1253674+3.*P1253746+P1253764-P1254673-P1254736+5.*P1254763-P1256374
    +P1256473+3.*P1263574+P1263745+P1263754+P1264573-P1264735+3.*P1264753-P1265374+P1265473
    +P1325674-5.*P1325746-5.*P1325764-P1326574-5.*P1326745-5.*P1326754-P1327456+P1327465-P1327546
    -P1327564+P1327645+P1327654-5.*P1362457-5.*P1362475-5.*P1362547-5.*P1362574-5.*P1362745
    -5.*P1362754-P1425673-P1425736+5.*P1425763+P1426573-P1426735+3.*P1426753+3.*P1427356+P1427365
    -3.*P1427536+3.*P1427563-3.*P1427635+P1427653)*0.5-P1432567-P1432576-P1432657+2.*(-P1324675
    -P1326475-P1342675-P1352467-P1352476-P1352674-P1352764-P1432675)+P1462537-P1472536
    -P1523674+P1523746+P1523764)/Nc);
    fvpart[134] += Nf*((P1235467+P1235476+P1264537+P1267354-P1267453+P1324567+P1324675-P1324756+P1325467
    +P1326475-P1326547+P1342567+P1342675-P1342756-P1356247+P1357246-P1365247+P1375246
    +(-P1235674+P1235746+P1235764+P1236574+3.*P1236745+3.*P1236754+P1237456-P1237465-P1237546
    -P1237564-P1237645-3.*P1237654+3.*P1245673-P1245736-3.*P1245763+P1246573+P1246735-P1246753
    -P1247356+P1247365+P1247536-P1247563+P1247635+P1247653-P1253674-P1253746+P1253764
    +3.*P1254673-P1254736-3.*P1254763-P1256374+P1256473-P1263574+P1263745+P1263754+P1264573
    +P1264735-P1264753+P1265374-P1265473-P1325674+3.*P1325746+3.*P1325764-P1326574+P1326745
    +3.*P1326754-P1327456-P1327465-P1327546-P1327564-P1327645-P1327654+P1352467+P1352476
    -P1352647+P1352674+3.*P1352746+P1352764+P1362457+P1362475+P1362547+P1362574+P1362745
    +P1362754-P1372456-P1372465-P1372546-P1372564-P1372645-P1372654+3.*P1425673-P1425736
    -3.*P1425763+P1426573+P1426735-P1426753-P1427356+P1427365+P1427536-P1427563+P1427635
    +P1427653)*0.5+P1432567+P1432675-P1432756-P1462537+P1472536+P1523674-P1523746-P1523764
    +P1623547+P1623745)/Nc);
    fvpart[135] += Nf*((P1235467+P1235476+P1264537+P1267354-P1267453+P1324567+P1324675-P1324756+P1325467
    +P1326475-P1326547+P1342567+P1342675-P1342756-P1356247+P1357246-P1365247+P1375246
    +(-P1235674+P1235746+P1235764+P1236574+3.*P1236745+3.*P1236754+P1237456-P1237465-P1237546
    -P1237564-P1237645-3.*P1237654+3.*P1245673-P1245736-3.*P1245763+P1246573+P1246735-P1246753
    -P1247356+P1247365+P1247536-P1247563+P1247635+P1247653-P1253674-P1253746+P1253764
    +3.*P1254673-P1254736-3.*P1254763-P1256374+P1256473-P1263574+P1263745+P1263754+P1264573
    +P1264735-P1264753+P1265374-P1265473-P1325674+3.*P1325746+3.*P1325764-P1326574+P1326745
    +3.*P1326754-P1327456-P1327465-P1327546-P1327564-P1327645-P1327654+P1352467+P1352476
    -P1352647+P1352674+3.*P1352746+P1352764+P1362457+P1362475+P1362547+P1362574+P1362745
    +P1362754-P1372456-P1372465-P1372546-P1372564-P1372645-P1372654+3.*P1425673-P1425736
    -3.*P1425763+P1426573+P1426735-P1426753-P1427356+P1427365+P1427536-P1427563+P1427635
    +P1427653)*0.5+P1432567+P1432675-P1432756-P1462537+P1472536+P1523674-P1523746-P1523764
    +P1623547+P1623745)/Nc);
    fvpart[136] += Nf*((-P1234567-P1234576-P1234657-P1234675-P1234756-P1234765-P1235467-P1235476-P1236457
    -P1236475-P1236547-P1243567-P1243576-P1243657-P1243675-P1243756-P1243765+P1245367
    +P1245376-P1245637+P1246357+P1246375+P1253467+P1253476+P1253647+P1254367+P1254376
    -P1254637-P1256347-P1256437+P1256734+P1256743-P1257346-P1257364-P1257436+P1257463
    +P1257634+P1257643+P1263457+P1263475+P1263547+P1264357+P1264375-P1264537-P1265347
    -P1265437+P1265734+P1265743-P1267345-P1267354-P1267435+P1267453+P1267534+P1267543
    +P1273456+P1273465+P1273546+P1273564+P1273645+P1273654+P1274356+P1274365-P1274536
    +P1274563-P1274635+P1274653-P1275346-P1275364-P1275436+P1275463+P1275634+P1275643
    -P1276345-P1276354-P1276435+P1276453+P1276534+P1276543-P1324567-P1324576-P1324657
    -P1325467-P1325476-P1326457-P1326547-P1342567-P1342576-P1342657-P1352647-3.*P1352746
    +P1356247-P1357246+P1365247-P1372456-P1372465-P1372546-P1372564-P1372645-P1372654
    -P1375246-P1423567-P1423576-P1423657-P1423675-P1423756-P1423765+P1425367+P1425376
    -P1425637+P1426357+P1426375+(-P1235674-3.*P1235746-3.*P1235764-3.*P1236574-5.*P1236745
    -5.*P1236754-P1237456+P1237465-P1237546-P1237564+P1237645+P1237654-P1245673-P1245736
    +5.*P1245763+P1246573-P1246735+3.*P1246753+3.*P1247356+P1247365-3.*P1247536+3.*P1247563
    -3.*P1247635+P1247653+3.*P1253674+3.*P1253746+P1253764-P1254673-P1254736+5.*P1254763-P1256374
    +P1256473+3.*P1263574+P1263745+P1263754+P1264573-P1264735+3.*P1264753-P1265374+P1265473
    +P1325674-5.*P1325746-5.*P1325764-P1326574-5.*P1326745-5.*P1326754-P1327456+P1327465-P1327546
    -P1327564+P1327645+P1327654-5.*P1362457-5.*P1362475-5.*P1362547-5.*P1362574-5.*P1362745
    -5.*P1362754-P1425673-P1425736+5.*P1425763+P1426573-P1426735+3.*P1426753+3.*P1427356+P1427365
    -3.*P1427536+3.*P1427563-3.*P1427635+P1427653)*0.5-P1432567-P1432576-P1432657+2.*(-P1324675
    -P1326475-P1342675-P1352467-P1352476-P1352674-P1352764-P1432675)+P1462537-P1472536
    -P1523674+P1523746+P1523764)/Nc);
    fvpart[137] += Nf*((P1234567+P1234576+P1234657+P1234675+P1234756+P1234765+P1235467+P1235476+P1237456
    +P1237465+P1237546+P1243567+P1243576+P1243657+P1243675+P1243756+P1243765-P1245367
    -P1245376+P1245736-P1247356-P1247365-P1253467-P1253476-P1253746-P1254367-P1254376
    +P1254736+P1256347+P1256374+P1256437-P1256473-P1256734-P1256743+P1257346+P1257436
    -P1257634-P1257643-P1263457-P1263475-P1263547-P1263574-P1263745-P1263754-P1264357
    -P1264375+P1264537-P1264573+P1264735-P1264753+P1265347+P1265374+P1265437-P1265473
    -P1265734-P1265743+P1267345+P1267354+P1267435-P1267453-P1267534-P1267543-P1273456
    -P1273465-P1273546-P1274356-P1274365+P1274536+P1275346+P1275436-P1275634-P1275643
    +P1276345+P1276354+P1276435-P1276453-P1276534-P1276543+P1324567+P1324576+P1324756
    +P1325467+P1325476+P1327456+P1327546+P1342567+P1342576+P1342756+P1352647-P1356247
    +P1357246+3.*(P1352746+P1362457+P1362475+P1362547+P1362574+P1362745+P1362754)-P1365247
    +P1375246+P1423567+P1423576+P1423657+P1423675+P1423756+P1423765-P1425367-P1425376
    +P1425736-P1427356-P1427365+(P1235647+P1235674+3.*P1235764+3.*P1236457+5.*P1236475+3.*P1236547
    +3.*P1236574+5.*P1236745+5.*P1236754+P1237564-P1237645-P1237654+3.*P1245637+P1245673-5.*P1245763
    -P1246357-3.*P1246375+P1246537-P1246573+P1246735-3.*P1246753-3.*P1247563+3.*P1247635-P1247653
    -P1253647-3.*P1253674-P1253764+3.*P1254637+P1254673-5.*P1254763+3.*P1257364-3.*P1257463
    -P1273564-3.*P1273645-3.*P1273654-3.*P1274563+3.*P1274635-P1274653+3.*P1275364-3.*P1275463
    -P1325647-P1325674+5.*P1325764+3.*P1326457+5.*P1326475+3.*P1326547+3.*P1326574+5.*P1326745
    +5.*P1326754+3.*P1327564-P1327645-P1327654+3.*P1372456+3.*P1372465+3.*P1372546+3.*P1372564
    +3.*P1372645+3.*P1372654+3.*P1425637+P1425673-5.*P1425763-P1426357-3.*P1426375+P1426537
    -P1426573+P1426735-3.*P1426753-3.*P1427563+3.*P1427635-P1427653)*0.5+P1432567+P1432576
    +2.*(P1235746+P1247536+P1324657+P1324675+P1325746+P1342657+P1342675+P1352467+P1352476
    +P1352674+P1352764+P1427536+P1432657+P1432675)+P1432756-P1462537+P1472536+P1523647
    +P1523674-P1523764)/Nc);
    fvpart[138] += Nf*((P1236754-P1237546-P1237564-P1237654+P1245673-P1245736-P1245763+P1246573+P1263754
    +P1264573+P1324567+P1324675-P1324756+P1325746+P1325764+P1326475-P1326574+P1326754
    -P1327546-P1327564+P1342567+P1342675-P1342756+P1367245+P1376245+P1425673-P1425736
    -P1425763+P1426573+(P1235467+3.*P1235476+P1235647-P1236547+3.*P1236745+P1237456-P1237465
    -P1237645+P1245367-P1245376-P1245637+P1246537+P1246735-P1247356+P1247365+P1247635
    +P1253674-P1253746-P1253764+P1254673-P1254736-P1254763-P1256374+P1256473+P1263547
    +P1263745+P1264537+P1264735+P1265374-P1265473+P1267354-P1267453-P1273546-P1273564
    +P1273654+P1274536+P1274563-P1274653+P1276354-P1276453+P1325467+P1325476+P1325647
    -3.*P1326547+P1326745-P1327456-P1327465-P1327645+P1352467+P1352476+P1352647+P1352674
    +P1352746+P1352764+P1362457+P1362475-P1362547-P1362574+P1362745-P1362754-P1372456
    -P1372465-P1372546-P1372564-P1372645-P1372654+P1425367-P1425376-P1425637+P1426537
    +P1426735-P1427356+P1427365+P1427635)*0.5+P1432567+P1432675-P1432756-P1452637+P1523674
    -P1523746-P1523764+P1623547+P1623745)/Nc);
    fvpart[139] += Nf*((P1236754-P1237546-P1237564-P1237654+P1245673-P1245736-P1245763+P1246573+P1263754
    +P1264573+P1324567+P1324675-P1324756+P1325746+P1325764+P1326475-P1326574+P1326754
    -P1327546-P1327564+P1342567+P1342675-P1342756+P1367245+P1376245+P1425673-P1425736
    -P1425763+P1426573+(P1235467+3.*P1235476+P1235647-P1236547+3.*P1236745+P1237456-P1237465
    -P1237645+P1245367-P1245376-P1245637+P1246537+P1246735-P1247356+P1247365+P1247635
    +P1253674-P1253746-P1253764+P1254673-P1254736-P1254763-P1256374+P1256473+P1263547
    +P1263745+P1264537+P1264735+P1265374-P1265473+P1267354-P1267453-P1273546-P1273564
    +P1273654+P1274536+P1274563-P1274653+P1276354-P1276453+P1325467+P1325476+P1325647
    -3.*P1326547+P1326745-P1327456-P1327465-P1327645+P1352467+P1352476+P1352647+P1352674
    +P1352746+P1352764+P1362457+P1362475-P1362547-P1362574+P1362745-P1362754-P1372456
    -P1372465-P1372546-P1372564-P1372645-P1372654+P1425367-P1425376-P1425637+P1426537
    +P1426735-P1427356+P1427365+P1427635)*0.5+P1432567+P1432675-P1432756-P1452637+P1523674
    -P1523746-P1523764+P1623547+P1623745)/Nc);
    fvpart[140] += Nf*((-P1234567-P1234576-P1234657-P1234675-P1234756-P1234765-P1235467-P1235476-P1235647
    -P1235674-P1235746-P1235764-P1236457-P1236475-P1236574-P1243567-P1243576-P1243657
    -P1243675-P1243756-P1243765+P1245367+P1245376+P1246357+P1246375-P1246537-P1246735
    +P1246753+P1247356+P1247365-P1247536+P1247563-P1247635+P1247653+P1253467+P1253476
    +P1253647+P1253674+P1253746+P1253764+P1254367+P1254376-P1254637-P1256347-P1256437
    +P1256734+P1256743-P1257346-P1257364-P1257436+P1257463+P1257634+P1257643+P1263457
    +P1263475+P1263547+P1263574+P1263745+P1264357+P1264375-P1264537-P1264735+P1264753
    -P1265347-P1265374-P1265437+P1265473+P1265734+P1265743-P1267345-P1267354-P1267435
    +P1267453+P1267534+P1267543+P1273456+P1273465+P1273546+P1273564+P1273645+P1273654
    +P1274356+P1274365-P1274536+P1274563-P1274635+P1274653-P1275346-P1275364-P1275436
    +P1275463+P1275634+P1275643-P1276345-P1276354-P1276435+P1276453+P1276534+P1276543
    -P1324567-P1324576-P1324657-P1325467-P1325476-P1325647-P1326457-P1342567-P1342576
    -P1342657+(-5.*P1362457-5.*P1362475-3.*P1362547-3.*P1362574-5.*P1362745-3.*P1362754)*0.5-P1367245
    -P1372456-P1372465-P1372546-P1372564-P1372645-P1372654-P1376245-P1423567-P1423576
    -P1423657-P1423675-P1423756-P1423765+P1425367+P1425376+P1426357+P1426375-P1426537
    -P1426735+P1426753+P1427356+P1427365-P1427536+P1427563-P1427635+P1427653-P1432567
    -P1432576-P1432657+2.*(-P1236745-P1236754+P1245763+P1254763-P1324675-P1325746-P1325764
    -P1326475-P1326745-P1326754-P1342675-P1352467-P1352476-P1352647-P1352674-P1352746
    -P1352764+P1425763-P1432675)+P1452637-P1523674+P1523746+P1523764)/Nc);
    fvpart[141] += Nf*((P1234567+P1234576+P1234657+P1234675+P1234756+P1234765+P1235467+P1235476+P1235647
    +P1235674+P1235746+P1235764+P1236547+P1236574+P1237456+P1237465+P1237546+P1243567
    +P1243576+P1243657+P1243675+P1243756+P1243765-P1245367-P1245376+P1245637+P1245736
    -P1246357-P1246375+P1246537+P1246735-P1246753-P1247356-P1247365+P1247536-P1247563
    +P1247635-P1247653-P1253467-P1253476-P1253647-P1253674-P1253746-P1253764-P1254367
    -P1254376+P1254736+P1256347+P1256374+P1256437-P1256473-P1256734-P1256743+P1257346
    +P1257436-P1257634-P1257643-P1263457-P1263475-P1263547-P1263574-P1263745-P1264357
    -P1264375+P1264537+P1264735-P1264753+P1265347+P1265374+P1265437-P1265473-P1265734
    -P1265743+P1267345+P1267354+P1267435-P1267453-P1267534-P1267543-P1273456-P1273465
    -P1273546-P1273564-P1273645-P1273654-P1274356-P1274365+P1274536-P1274563+P1274635
    -P1274653+P1275346+P1275364+P1275436-P1275463-P1275634-P1275643+P1276345+P1276354
    +P1276435-P1276453-P1276534-P1276543+P1324567+P1324576+P1324756+P1325467+P1325476
    +P1325746+P1326547+P1326574+P1327456+P1327546+P1327564+P1342567+P1342576+P1342756
    +3.*(P1362457+P1362475+P1362745)+P1367245+(3.*P1372456+3.*P1372465+3.*P1372546+3.*P1372564
    +3.*P1372645+3.*P1372654)*0.5+P1376245+P1423567+P1423576+P1423657+P1423675+P1423756+P1423765
    -P1425367-P1425376+P1425637+P1425736-P1426357-P1426375+P1426537+P1426735-P1426753
    -P1427356-P1427365+P1427536-P1427563+P1427635-P1427653+P1432567+P1432576+2.*(P1236457
    +P1236475+P1236745+P1236754-P1245763+P1254637-P1254763+P1257364-P1257463+P1324657
    +P1324675+P1325764+P1326457+P1326475+P1326745+P1326754+P1342657+P1342675+P1352467
    +P1352476+P1352647+P1352674+P1352746+P1352764+P1362547+P1362574+P1362754-P1425763
    +P1432657+P1432675)+P1432756-P1452637+P1523647+P1523674-P1523764)/Nc);
    fvpart[142] += Nf*((P1234567+P1234576+P1234657+P1234675+P1234756+P1234765+P1235467+P1235476+P1235647
    +P1235674+P1235746+P1235764+P1236547+P1236574+P1237456+P1237465+P1237546+P1243567
    +P1243576+P1243657+P1243675+P1243756+P1243765-P1245367-P1245376+P1245637+P1245736
    -P1246357-P1246375+P1246537+P1246735-P1246753-P1247356-P1247365+P1247536-P1247563
    +P1247635-P1247653-P1253467-P1253476-P1253647-P1253674-P1253746-P1253764-P1254367
    -P1254376+P1254736+P1256347+P1256374+P1256437-P1256473-P1256734-P1256743+P1257346
    +P1257436-P1257634-P1257643-P1263457-P1263475-P1263547-P1263574-P1263745-P1264357
    -P1264375+P1264537+P1264735-P1264753+P1265347+P1265374+P1265437-P1265473-P1265734
    -P1265743+P1267345+P1267354+P1267435-P1267453-P1267534-P1267543-P1273456-P1273465
    -P1273546-P1273564-P1273645-P1273654-P1274356-P1274365+P1274536-P1274563+P1274635
    -P1274653+P1275346+P1275364+P1275436-P1275463-P1275634-P1275643+P1276345+P1276354
    +P1276435-P1276453-P1276534-P1276543+P1324567+P1324576+P1324756+P1325467+P1325476
    +P1325746+P1326547+P1326574+P1327456+P1327546+P1327564+P1342567+P1342576+P1342756
    +3.*(P1362457+P1362475+P1362745)+P1367245+(3.*P1372456+3.*P1372465+3.*P1372546+3.*P1372564
    +3.*P1372645+3.*P1372654)*0.5+P1376245+P1423567+P1423576+P1423657+P1423675+P1423756+P1423765
    -P1425367-P1425376+P1425637+P1425736-P1426357-P1426375+P1426537+P1426735-P1426753
    -P1427356-P1427365+P1427536-P1427563+P1427635-P1427653+P1432567+P1432576+2.*(P1236457
    +P1236475+P1236745+P1236754-P1245763+P1254637-P1254763+P1257364-P1257463+P1324657
    +P1324675+P1325764+P1326457+P1326475+P1326745+P1326754+P1342657+P1342675+P1352467
    +P1352476+P1352647+P1352674+P1352746+P1352764+P1362547+P1362574+P1362754-P1425763
    +P1432657+P1432675)+P1432756-P1452637+P1523647+P1523674-P1523764)/Nc);
    fvpart[143] += Nf*((-P1234567-P1234576-P1234657-P1234675-P1234756-P1234765-P1235467-P1235476-P1235647
    -P1235674-P1235746-P1235764-P1236457-P1236475-P1236574-P1243567-P1243576-P1243657
    -P1243675-P1243756-P1243765+P1245367+P1245376+P1246357+P1246375-P1246537-P1246735
    +P1246753+P1247356+P1247365-P1247536+P1247563-P1247635+P1247653+P1253467+P1253476
    +P1253647+P1253674+P1253746+P1253764+P1254367+P1254376-P1254637-P1256347-P1256437
    +P1256734+P1256743-P1257346-P1257364-P1257436+P1257463+P1257634+P1257643+P1263457
    +P1263475+P1263547+P1263574+P1263745+P1264357+P1264375-P1264537-P1264735+P1264753
    -P1265347-P1265374-P1265437+P1265473+P1265734+P1265743-P1267345-P1267354-P1267435
    +P1267453+P1267534+P1267543+P1273456+P1273465+P1273546+P1273564+P1273645+P1273654
    +P1274356+P1274365-P1274536+P1274563-P1274635+P1274653-P1275346-P1275364-P1275436
    +P1275463+P1275634+P1275643-P1276345-P1276354-P1276435+P1276453+P1276534+P1276543
    -P1324567-P1324576-P1324657-P1325467-P1325476-P1325647-P1326457-P1342567-P1342576
    -P1342657+(-5.*P1362457-5.*P1362475-3.*P1362547-3.*P1362574-5.*P1362745-3.*P1362754)*0.5-P1367245
    -P1372456-P1372465-P1372546-P1372564-P1372645-P1372654-P1376245-P1423567-P1423576
    -P1423657-P1423675-P1423756-P1423765+P1425367+P1425376+P1426357+P1426375-P1426537
    -P1426735+P1426753+P1427356+P1427365-P1427536+P1427563-P1427635+P1427653-P1432567
    -P1432576-P1432657+2.*(-P1236745-P1236754+P1245763+P1254763-P1324675-P1325746-P1325764
    -P1326475-P1326745-P1326754-P1342675-P1352467-P1352476-P1352647-P1352674-P1352746
    -P1352764+P1425763-P1432675)+P1452637-P1523674+P1523746+P1523764)/Nc);
    fvpart[144] += Nf*((P1342567+P1342576+P1342657+P1342675+P1342756+P1342765)/(2.*Nc));
    fvpart[145] += Nf*((P1342567+P1342576+P1342657+P1342675+P1342756+P1342765)/(2.*Nc));
    fvpart[146] += Nf*((P1352467+P1352476+P1352647+P1352674+P1352746+P1352764)/(2.*Nc));
    fvpart[147] += Nf*((P1352467+P1352476+P1352647+P1352674+P1352746+P1352764)/(2.*Nc));
    fvpart[148] += Nf*((P1362457+P1362475+P1362547+P1362574+P1362745+P1362754)/(2.*Nc));
    fvpart[149] += Nf*((P1362457+P1362475+P1362547+P1362574+P1362745+P1362754)/(2.*Nc));
    fvpart[150] += Nf*((P1372456+P1372465+P1372546+P1372564+P1372645+P1372654)/(2.*Nc));
    fvpart[151] += Nf*((P1372456+P1372465+P1372546+P1372564+P1372645+P1372654)/(2.*Nc));
    fvpart[152] += Nf*((-P1352467-P1352476-P1352647-P1352674-P1352746-P1352764-P1362457-P1362475-P1362547
    -P1362574-P1362745-P1362754-P1372456-P1372465-P1372546-P1372564-P1372645-P1372654
    +(-P1234567-P1234576-P1234657-P1234675-P1234756-P1234765-P1235467-P1235476-P1235647
    -P1235674-P1235746-P1235764-P1236457-P1236475-P1236547-P1236574-P1236745-P1236754
    -P1237456-P1237465-P1237546-P1237564-P1237645-P1237654-P1243567-P1243576-P1243657
    -P1243675-P1243756-P1243765+P1245367+P1245376-P1245637+P1245673-P1245736+P1245763
    +P1246357+P1246375-P1246537+P1246573-P1246735+P1246753+P1247356+P1247365-P1247536
    +P1247563-P1247635+P1247653+P1253467+P1253476+P1253647+P1253674+P1253746+P1253764
    +P1254367+P1254376-P1254637+P1254673-P1254736+P1254763-P1256347-P1256374-P1256437
    +P1256473+P1256734+P1256743-P1257346-P1257364-P1257436+P1257463+P1257634+P1257643
    +P1263457+P1263475+P1263547+P1263574+P1263745+P1263754+P1264357+P1264375-P1264537
    +P1264573-P1264735+P1264753-P1265347-P1265374-P1265437+P1265473+P1265734+P1265743
    -P1267345-P1267354-P1267435+P1267453+P1267534+P1267543+P1273456+P1273465+P1273546
    +P1273564+P1273645+P1273654+P1274356+P1274365-P1274536+P1274563-P1274635+P1274653
    -P1275346-P1275364-P1275436+P1275463+P1275634+P1275643-P1276345-P1276354-P1276435
    +P1276453+P1276534+P1276543-P1324567-P1324576-P1324657-P1324675-P1324756-P1324765
    -P1325467-P1325476-P1325647-P1325674-P1325746-P1325764-P1326457-P1326475-P1326547
    -P1326574-P1326745-P1326754-P1327456-P1327465-P1327546-P1327564-P1327645-P1327654
    -P1342567-P1342576-P1342657-P1342675-P1342756-P1342765-P1423567-P1423576-P1423657
    -P1423675-P1423756-P1423765+P1425367+P1425376-P1425637+P1425673-P1425736+P1425763
    +P1426357+P1426375-P1426537+P1426573-P1426735+P1426753+P1427356+P1427365-P1427536
    +P1427563-P1427635+P1427653)*0.5)/Nc);
    fvpart[153] += Nf*((-P1352467-P1352476-P1352647-P1352674-P1352746-P1352764-P1362457-P1362475-P1362547
    -P1362574-P1362745-P1362754-P1372456-P1372465-P1372546-P1372564-P1372645-P1372654
    +(-P1234567-P1234576-P1234657-P1234675-P1234756-P1234765-P1235467-P1235476-P1235647
    -P1235674-P1235746-P1235764-P1236457-P1236475-P1236547-P1236574-P1236745-P1236754
    -P1237456-P1237465-P1237546-P1237564-P1237645-P1237654-P1243567-P1243576-P1243657
    -P1243675-P1243756-P1243765+P1245367+P1245376-P1245637+P1245673-P1245736+P1245763
    +P1246357+P1246375-P1246537+P1246573-P1246735+P1246753+P1247356+P1247365-P1247536
    +P1247563-P1247635+P1247653+P1253467+P1253476+P1253647+P1253674+P1253746+P1253764
    +P1254367+P1254376-P1254637+P1254673-P1254736+P1254763-P1256347-P1256374-P1256437
    +P1256473+P1256734+P1256743-P1257346-P1257364-P1257436+P1257463+P1257634+P1257643
    +P1263457+P1263475+P1263547+P1263574+P1263745+P1263754+P1264357+P1264375-P1264537
    +P1264573-P1264735+P1264753-P1265347-P1265374-P1265437+P1265473+P1265734+P1265743
    -P1267345-P1267354-P1267435+P1267453+P1267534+P1267543+P1273456+P1273465+P1273546
    +P1273564+P1273645+P1273654+P1274356+P1274365-P1274536+P1274563-P1274635+P1274653
    -P1275346-P1275364-P1275436+P1275463+P1275634+P1275643-P1276345-P1276354-P1276435
    +P1276453+P1276534+P1276543-P1324567-P1324576-P1324657-P1324675-P1324756-P1324765
    -P1325467-P1325476-P1325647-P1325674-P1325746-P1325764-P1326457-P1326475-P1326547
    -P1326574-P1326745-P1326754-P1327456-P1327465-P1327546-P1327564-P1327645-P1327654
    -P1342567-P1342576-P1342657-P1342675-P1342756-P1342765-P1423567-P1423576-P1423657
    -P1423675-P1423756-P1423765+P1425367+P1425376-P1425637+P1425673-P1425736+P1425763
    +P1426357+P1426375-P1426537+P1426573-P1426735+P1426753+P1427356+P1427365-P1427536
    +P1427563-P1427635+P1427653)*0.5)/Nc);
    fvpart[154] += Nf*((P1235467+P1235476+P1235647+P1235674+P1235746+P1235764-P1236547-P1236574+P1236754
    -P1237546-P1237564+P1237654-P1245367-P1245376-P1245637-P1245673-P1245736-P1245763
    +P1246537+P1246573-P1246753+P1247536+P1247563-P1247653-P1263547-P1263574+P1263754
    +P1264537+P1264573-P1264753+P1267354-P1267453-P1273546-P1273564+P1273654+P1274536
    +P1274563-P1274653+P1276354-P1276453+P1325467+P1325476+P1325647+P1325674+P1325746
    +P1325764-P1326547-P1326574+P1326754-P1327546-P1327564+P1327654+P1352467+P1352476
    +P1352647+P1352674+P1352746+P1352764-P1425367-P1425376-P1425637-P1425673-P1425736
    -P1425763+P1426537+P1426573-P1426753+P1427536+P1427563-P1427653)/(2.*Nc));
    fvpart[155] += Nf*((P1235467+P1235476+P1235647+P1235674+P1235746+P1235764-P1236547-P1236574+P1236754
    -P1237546-P1237564+P1237654-P1245367-P1245376-P1245637-P1245673-P1245736-P1245763
    +P1246537+P1246573-P1246753+P1247536+P1247563-P1247653-P1263547-P1263574+P1263754
    +P1264537+P1264573-P1264753+P1267354-P1267453-P1273546-P1273564+P1273654+P1274536
    +P1274563-P1274653+P1276354-P1276453+P1325467+P1325476+P1325647+P1325674+P1325746
    +P1325764-P1326547-P1326574+P1326754-P1327546-P1327564+P1327654+P1352467+P1352476
    +P1352647+P1352674+P1352746+P1352764-P1425367-P1425376-P1425637-P1425673-P1425736
    -P1425763+P1426537+P1426573-P1426753+P1427536+P1427563-P1427653)/(2.*Nc));
    fvpart[156] += Nf*((-P1235647-P1235674+P1235764+P1236457+P1236475+P1236547+P1236574+P1236745+P1236754
    +P1237564-P1237645-P1237654+P1245637+P1245673-P1245763-P1246357-P1246375-P1246537
    -P1246573-P1246735-P1246753-P1247563+P1247635+P1247653-P1253647-P1253674+P1253764
    +P1254637+P1254673-P1254763+P1257364-P1257463+P1273564-P1273645-P1273654-P1274563
    +P1274635+P1274653+P1275364-P1275463-P1325647-P1325674+P1325764+P1326457+P1326475
    +P1326547+P1326574+P1326745+P1326754+P1327564-P1327645-P1327654+P1362457+P1362475
    +P1362547+P1362574+P1362745+P1362754+P1425637+P1425673-P1425763-P1426357-P1426375
    -P1426537-P1426573-P1426735-P1426753-P1427563+P1427635+P1427653)/(2.*Nc));
    fvpart[157] += Nf*((-P1235647-P1235674+P1235764+P1236457+P1236475+P1236547+P1236574+P1236745+P1236754
    +P1237564-P1237645-P1237654+P1245637+P1245673-P1245763-P1246357-P1246375-P1246537
    -P1246573-P1246735-P1246753-P1247563+P1247635+P1247653-P1253647-P1253674+P1253764
    +P1254637+P1254673-P1254763+P1257364-P1257463+P1273564-P1273645-P1273654-P1274563
    +P1274635+P1274653+P1275364-P1275463-P1325647-P1325674+P1325764+P1326457+P1326475
    +P1326547+P1326574+P1326745+P1326754+P1327564-P1327645-P1327654+P1362457+P1362475
    +P1362547+P1362574+P1362745+P1362754+P1425637+P1425673-P1425763-P1426357-P1426375
    -P1426537-P1426573-P1426735-P1426753-P1427563+P1427635+P1427653)/(2.*Nc));
    fvpart[158] += Nf*((P1235674-P1235746-P1235764+P1236574-P1236745-P1236754+P1237456+P1237465+P1237546
    +P1237564+P1237645+P1237654-P1245673+P1245736+P1245763-P1246573+P1246735+P1246753
    -P1247356-P1247365-P1247536-P1247563-P1247635-P1247653+P1253674-P1253746-P1253764
    -P1254673+P1254736+P1254763+P1256374-P1256473+P1263574-P1263745-P1263754-P1264573
    +P1264735+P1264753+P1265374-P1265473+P1325674-P1325746-P1325764+P1326574-P1326745
    -P1326754+P1327456+P1327465+P1327546+P1327564+P1327645+P1327654+P1372456+P1372465
    +P1372546+P1372564+P1372645+P1372654-P1425673+P1425736+P1425763-P1426573+P1426735
    +P1426753-P1427356-P1427365-P1427536-P1427563-P1427635-P1427653)/(2.*Nc));
    fvpart[159] += Nf*((P1235674-P1235746-P1235764+P1236574-P1236745-P1236754+P1237456+P1237465+P1237546
    +P1237564+P1237645+P1237654-P1245673+P1245736+P1245763-P1246573+P1246735+P1246753
    -P1247356-P1247365-P1247536-P1247563-P1247635-P1247653+P1253674-P1253746-P1253764
    -P1254673+P1254736+P1254763+P1256374-P1256473+P1263574-P1263745-P1263754-P1264573
    +P1264735+P1264753+P1265374-P1265473+P1325674-P1325746-P1325764+P1326574-P1326745
    -P1326754+P1327456+P1327465+P1327546+P1327564+P1327645+P1327654+P1372456+P1372465
    +P1372546+P1372564+P1372645+P1372654-P1425673+P1425736+P1425763-P1426573+P1426735
    +P1426753-P1427356-P1427365-P1427536-P1427563-P1427635-P1427653)/(2.*Nc));
    fvpart[160] += Nf*((P1324657+P1324756+P1326457+P1326547+P1326574+P1327456+P1327546+P1327564+P1342657
    +P1342756+P1362457+P1362475+P1362547+P1362574+P1362745+P1362754+P1372456+P1372465
    +P1372546+P1372564+P1372645+P1372654+(P1234567+P1234576+P1234657+P1234675+P1234756
    +P1234765+P1235467+P1235476+P1235647+P1235674+P1235746+P1235764+P1236457+P1236475
    +P1236547+P1236574+P1236745+P1236754+P1237456+P1237465+P1237546+P1237564+P1237645
    +P1237654+P1243567+P1243576+P1243657+P1243675+P1243756+P1243765-P1245367-P1245376
    +P1245637-P1245673+P1245736-P1245763-P1246357-P1246375+P1246537-P1246573+P1246735
    -P1246753-P1247356-P1247365+P1247536-P1247563+P1247635-P1247653-P1253467-P1253476
    -P1253647-P1253674-P1253746-P1253764-P1254367-P1254376+P1254637-P1254673+P1254736
    -P1254763+P1256347+P1256374+P1256437-P1256473-P1256734-P1256743+P1257346+P1257364
    +P1257436-P1257463-P1257634-P1257643-P1263457-P1263475-P1263547-P1263574-P1263745
    -P1263754-P1264357-P1264375+P1264537-P1264573+P1264735-P1264753+P1265347+P1265374
    +P1265437-P1265473-P1265734-P1265743+P1267345+P1267354+P1267435-P1267453-P1267534
    -P1267543-P1273456-P1273465-P1273546-P1273564-P1273645-P1273654-P1274356-P1274365
    +P1274536-P1274563+P1274635-P1274653+P1275346+P1275364+P1275436-P1275463-P1275634
    -P1275643+P1276345+P1276354+P1276435-P1276453-P1276534-P1276543+P1352467+P1352476
    +P1352647+P1352674+P1352746+P1352764+P1423567+P1423576+P1423657+P1423675+P1423756
    +P1423765-P1425367-P1425376+P1425637-P1425673+P1425736-P1425763-P1426357-P1426375
    +P1426537-P1426573+P1426735-P1426753-P1427356-P1427365+P1427536-P1427563+P1427635
    -P1427653)*0.5+P1432657+P1432756)/Nc);
    fvpart[161] += Nf*((P1324657+P1324756+P1326457+P1326547+P1326574+P1327456+P1327546+P1327564+P1342657
    +P1342756+P1362457+P1362475+P1362547+P1362574+P1362745+P1362754+P1372456+P1372465
    +P1372546+P1372564+P1372645+P1372654+(P1234567+P1234576+P1234657+P1234675+P1234756
    +P1234765+P1235467+P1235476+P1235647+P1235674+P1235746+P1235764+P1236457+P1236475
    +P1236547+P1236574+P1236745+P1236754+P1237456+P1237465+P1237546+P1237564+P1237645
    +P1237654+P1243567+P1243576+P1243657+P1243675+P1243756+P1243765-P1245367-P1245376
    +P1245637-P1245673+P1245736-P1245763-P1246357-P1246375+P1246537-P1246573+P1246735
    -P1246753-P1247356-P1247365+P1247536-P1247563+P1247635-P1247653-P1253467-P1253476
    -P1253647-P1253674-P1253746-P1253764-P1254367-P1254376+P1254637-P1254673+P1254736
    -P1254763+P1256347+P1256374+P1256437-P1256473-P1256734-P1256743+P1257346+P1257364
    +P1257436-P1257463-P1257634-P1257643-P1263457-P1263475-P1263547-P1263574-P1263745
    -P1263754-P1264357-P1264375+P1264537-P1264573+P1264735-P1264753+P1265347+P1265374
    +P1265437-P1265473-P1265734-P1265743+P1267345+P1267354+P1267435-P1267453-P1267534
    -P1267543-P1273456-P1273465-P1273546-P1273564-P1273645-P1273654-P1274356-P1274365
    +P1274536-P1274563+P1274635-P1274653+P1275346+P1275364+P1275436-P1275463-P1275634
    -P1275643+P1276345+P1276354+P1276435-P1276453-P1276534-P1276543+P1352467+P1352476
    +P1352647+P1352674+P1352746+P1352764+P1423567+P1423576+P1423657+P1423675+P1423756
    +P1423765-P1425367-P1425376+P1425637-P1425673+P1425736-P1425763-P1426357-P1426375
    +P1426537-P1426573+P1426735-P1426753-P1427356-P1427365+P1427536-P1427563+P1427635
    -P1427653)*0.5+P1432657+P1432756)/Nc);
    fvpart[162] += Nf*((P1236547+P1236574+P1237546+P1237564+P1245637+P1245736-P1246573-P1247563-P1263754
    -P1264573-P1273654-P1274563+P1326547+P1326574+P1327546+P1327564+P1342657+P1342756
    +P1362457+P1362475+P1362547+P1362574+P1362745+P1362754+P1372456+P1372465+P1372546
    +P1372564+P1372645+P1372654+P1423657+P1423756-P1426375-P1427365+(P1234567+P1234576
    +P1234657+P1234675+P1234756+P1234765+P1236457+P1236475+P1236745+P1237456+P1237465
    +P1237645+P1243567+P1243576+P1243657+P1243675+P1243756+P1243765-P1246357-P1246375
    +P1246735-P1247356-P1247365+P1247635-P1253467-P1253476-P1253647-P1253674-P1253746
    -P1253764-P1254367-P1254376+P1254637-P1254673+P1254736-P1254763+P1256347+P1256374
    +P1256437-P1256473-P1256734-P1256743+P1257346+P1257364+P1257436-P1257463-P1257634
    -P1257643-P1263457-P1263475-P1263745-P1264357-P1264375+P1264735+P1265347+P1265374
    +P1265437-P1265473-P1265734-P1265743+P1267345+P1267435-P1267534-P1267543-P1273456
    -P1273465-P1273645-P1274356-P1274365+P1274635+P1275346+P1275364+P1275436-P1275463
    -P1275634-P1275643+P1276345+P1276435-P1276534-P1276543+P1324567+P1324576+P1324657
    +P1324675+P1324756+P1324765+P1326457+P1326475+P1326745+P1327456+P1327465+P1327645
    +P1352467+P1352476+P1352647+P1352674+P1352746+P1352764-P1425367-P1425376+P1425637
    -P1425673+P1425736-P1425763+P1426537-P1426573-P1426753+P1427536-P1427563-P1427653)*0.5
    +P1432657+P1432756)/Nc);
    fvpart[163] += Nf*((P1236547+P1236574+P1237546+P1237564+P1245637+P1245736-P1246573-P1247563-P1263754
    -P1264573-P1273654-P1274563+P1326547+P1326574+P1327546+P1327564+P1342657+P1342756
    +P1362457+P1362475+P1362547+P1362574+P1362745+P1362754+P1372456+P1372465+P1372546
    +P1372564+P1372645+P1372654+P1423657+P1423756-P1426375-P1427365+(P1234567+P1234576
    +P1234657+P1234675+P1234756+P1234765+P1236457+P1236475+P1236745+P1237456+P1237465
    +P1237645+P1243567+P1243576+P1243657+P1243675+P1243756+P1243765-P1246357-P1246375
    +P1246735-P1247356-P1247365+P1247635-P1253467-P1253476-P1253647-P1253674-P1253746
    -P1253764-P1254367-P1254376+P1254637-P1254673+P1254736-P1254763+P1256347+P1256374
    +P1256437-P1256473-P1256734-P1256743+P1257346+P1257364+P1257436-P1257463-P1257634
    -P1257643-P1263457-P1263475-P1263745-P1264357-P1264375+P1264735+P1265347+P1265374
    +P1265437-P1265473-P1265734-P1265743+P1267345+P1267435-P1267534-P1267543-P1273456
    -P1273465-P1273645-P1274356-P1274365+P1274635+P1275346+P1275364+P1275436-P1275463
    -P1275634-P1275643+P1276345+P1276435-P1276534-P1276543+P1324567+P1324576+P1324657
    +P1324675+P1324756+P1324765+P1326457+P1326475+P1326745+P1327456+P1327465+P1327645
    +P1352467+P1352476+P1352647+P1352674+P1352746+P1352764-P1425367-P1425376+P1425637
    -P1425673+P1425736-P1425763+P1426537-P1426573-P1426753+P1427536-P1427563-P1427653)*0.5
    +P1432657+P1432756)/Nc);
    fvpart[164] += Nf*((P1234765-P1237456-P1237465-P1237546-P1237564-P1237645-P1237654+P1243765+P1245673
    -P1245736+P1246573+P1247365+P1254673-P1254736-P1256374+P1256473-P1256734-P1256743
    +P1263745+P1263754+P1264573+P1265473+P1324675-P1324756+P1325746+P1325764+P1326475
    -P1327456-P1327465-P1327546-P1327564-P1327645-P1327654+P1342675-P1342756-P1372456
    -P1372465-P1372546-P1372564-P1372645-P1372654+P1423765+P1425673-P1425736+P1426573
    +P1427365+P1432675-P1432756+(-P1235674+P1236574+3.*P1236745+3.*P1236754-3.*P1245763-P1247563
    +P1247635+P1247653-P1253674-3.*P1254763-3.*P1325674-P1326574+3.*P1326745+3.*P1326754+P1362457
    +P1362475+P1362547-3.*P1425763-P1427563+P1427635+P1427653+P1472356+P1472365+P1472536)*0.5
    +P1523674-P1523746-P1523764-P1526734-P1526743)/Nc);
    fvpart[165] += Nf*((P1234765-P1237456-P1237465-P1237546-P1237564-P1237645-P1237654+P1243765+P1245673
    -P1245736+P1246573+P1247365+P1254673-P1254736-P1256374+P1256473-P1256734-P1256743
    +P1263745+P1263754+P1264573+P1265473+P1324675-P1324756+P1325746+P1325764+P1326475
    -P1327456-P1327465-P1327546-P1327564-P1327645-P1327654+P1342675-P1342756-P1372456
    -P1372465-P1372546-P1372564-P1372645-P1372654+P1423765+P1425673-P1425736+P1426573
    +P1427365+P1432675-P1432756+(-P1235674+P1236574+3.*P1236745+3.*P1236754-3.*P1245763-P1247563
    +P1247635+P1247653-P1253674-3.*P1254763-3.*P1325674-P1326574+3.*P1326745+3.*P1326754+P1362457
    +P1362475+P1362547-3.*P1425763-P1427563+P1427635+P1427653+P1472356+P1472365+P1472536)*0.5
    +P1523674-P1523746-P1523764-P1526734-P1526743)/Nc);
    fvpart[166] += Nf*((-P1234567-P1234576-P1235467-P1235476-P1235647-P1235746-P1236475-P1237456-P1237465
    -P1237546-P1237564-P1237645-P1237654-P1243567-P1243576+P1245367+P1245376+P1245673
    -P1245736+P1246357+P1246375-P1246537+P1246573+P1247356+P1247365-P1247536+P1253467
    +P1253476+P1253647+P1253746+P1254367+P1254376+P1254673-P1254736-P1256374+P1256473
    -P1257364+P1257463+P1263457+P1263547+P1263574+P1263745+P1263754+P1264357-P1264537
    +P1264573-P1264735+P1264753-P1265347-P1265374-P1265437+P1265473-P1267345-P1267354
    -P1267435+P1267453+P1267534+P1267543+P1273456+P1273546+P1273564+P1274356-P1274536
    -P1274635+P1274653-P1275346-P1275364-P1275436-P1276345-P1276354-P1276435+P1276453
    +P1276534+P1276543-P1324675-P1325647-P1325746+P1325764-P1326475-P1327465-P1327564
    +P1342567+P1342576-P1342675-P1423765-P1425637-P1425736+P1426375+P1426573-P1426735
    +P1426753+P1432567+P1432576+3.*(-P1234765-P1243765+P1256734+P1256743-P1326457-P1326547
    -P1342657-P1362574-P1362745-P1362754-P1372456-P1372465-P1372546-P1423657+P1425673
    -P1432657)-P1432675-P1462357-P1462375-P1462537+(-P1235674-5.*P1236574-5.*P1236745-5.*P1236754
    +5.*P1245763+5.*P1247563-3.*P1247635+P1247653+3.*P1253674+5.*P1254763+P1325674-5.*P1326574
    -3.*P1326745-3.*P1326754-7.*P1362457-7.*P1362475-7.*P1362547+5.*P1425763+3.*P1427563-P1427635
    +3.*P1427653-P1472356-P1472365-P1472536)*0.5-P1523467-P1523476-P1523674-P1523764-P1524367
    -P1524376-P1524637+2.*(-P1234657-P1234675-P1234756-P1236457-P1236547-P1243657-P1243675
    -P1243756-P1245637-P1246735+P1253764-P1254637-P1256347-P1256437-P1257346-P1257436
    +P1257634+P1257643+P1263475+P1264375+P1265734+P1265743+P1273465+P1273645+P1273654
    +P1274365+P1274563+P1275463+P1275634+P1275643-P1324657-P1324756-P1324765-P1327456
    -P1327546-P1342756-P1352467-P1352476-P1352647-P1352674-P1352746-P1352764-P1372564
    -P1372645-P1372654-P1423675-P1423756+P1425367+P1425376-P1426537+P1427365-P1427536
    -P1432756-P1524673)+P1526734+P1526743)/Nc);
    fvpart[167] += Nf*((-P1234567-P1234576-P1235467-P1235476-P1235647-P1235746-P1236475-P1237456-P1237465
    -P1237546-P1237564-P1237645-P1237654-P1243567-P1243576+P1245367+P1245376+P1245673
    -P1245736+P1246357+P1246375-P1246537+P1246573+P1247356+P1247365-P1247536+P1253467
    +P1253476+P1253647+P1253746+P1254367+P1254376+P1254673-P1254736-P1256374+P1256473
    -P1257364+P1257463+P1263457+P1263547+P1263574+P1263745+P1263754+P1264357-P1264537
    +P1264573-P1264735+P1264753-P1265347-P1265374-P1265437+P1265473-P1267345-P1267354
    -P1267435+P1267453+P1267534+P1267543+P1273456+P1273546+P1273564+P1274356-P1274536
    -P1274635+P1274653-P1275346-P1275364-P1275436-P1276345-P1276354-P1276435+P1276453
    +P1276534+P1276543-P1324675-P1325647-P1325746+P1325764-P1326475-P1327465-P1327564
    +P1342567+P1342576-P1342675-P1423765-P1425637-P1425736+P1426375+P1426573-P1426735
    +P1426753+P1432567+P1432576+3.*(-P1234765-P1243765+P1256734+P1256743-P1326457-P1326547
    -P1342657-P1362574-P1362745-P1362754-P1372456-P1372465-P1372546-P1423657+P1425673
    -P1432657)-P1432675-P1462357-P1462375-P1462537+(-P1235674-5.*P1236574-5.*P1236745-5.*P1236754
    +5.*P1245763+5.*P1247563-3.*P1247635+P1247653+3.*P1253674+5.*P1254763+P1325674-5.*P1326574
    -3.*P1326745-3.*P1326754-7.*P1362457-7.*P1362475-7.*P1362547+5.*P1425763+3.*P1427563-P1427635
    +3.*P1427653-P1472356-P1472365-P1472536)*0.5-P1523467-P1523476-P1523674-P1523764-P1524367
    -P1524376-P1524637+2.*(-P1234657-P1234675-P1234756-P1236457-P1236547-P1243657-P1243675
    -P1243756-P1245637-P1246735+P1253764-P1254637-P1256347-P1256437-P1257346-P1257436
    +P1257634+P1257643+P1263475+P1264375+P1265734+P1265743+P1273465+P1273645+P1273654
    +P1274365+P1274563+P1275463+P1275634+P1275643-P1324657-P1324756-P1324765-P1327456
    -P1327546-P1342756-P1352467-P1352476-P1352647-P1352674-P1352746-P1352764-P1372564
    -P1372645-P1372654-P1423675-P1423756+P1425367+P1425376-P1426537+P1427365-P1427536
    -P1432756-P1524673)+P1526734+P1526743)/Nc);
    fvpart[168] += Nf*((-P1324576-P1324657-P1324675-P1324756-P1325476-P1325746-P1325764-P1326457-P1326475
    -P1326547-P1326574-P1326745-P1326754-P1327456-P1327546-P1327564-P1342576-P1342657
    -P1342675-P1342756-P1352467-P1352476-P1352647-P1352674-P1352746-P1352764-P1372456
    -P1372465-P1372546-P1372564-P1372645-P1372654+(-P1234567-P1234576-P1234657-P1234675
    -P1234756-P1234765-P1235467-P1235476-P1235647-P1235674-P1235746-P1235764-P1236457
    -P1236475-P1236547-P1236574-P1236745-P1236754-P1237456-P1237465-P1237546-P1237564
    -P1237645-P1237654-P1243567-P1243576-P1243657-P1243675-P1243756-P1243765+P1245367
    +P1245376-P1245637+P1245673-P1245736+P1245763+P1246357+P1246375-P1246537+P1246573
    -P1246735+P1246753+P1247356+P1247365-P1247536+P1247563-P1247635+P1247653+P1253467
    +P1253476+P1253647+P1253674+P1253746+P1253764+P1254367+P1254376-P1254637+P1254673
    -P1254736+P1254763-P1256347-P1256374-P1256437+P1256473+P1256734+P1256743-P1257346
    -P1257364-P1257436+P1257463+P1257634+P1257643+P1263457+P1263475+P1263547+P1263574
    +P1263745+P1263754+P1264357+P1264375-P1264537+P1264573-P1264735+P1264753-P1265347
    -P1265374-P1265437+P1265473+P1265734+P1265743-P1267345-P1267354-P1267435+P1267453
    +P1267534+P1267543+P1273456+P1273465+P1273546+P1273564+P1273645+P1273654+P1274356
    +P1274365-P1274536+P1274563-P1274635+P1274653-P1275346-P1275364-P1275436+P1275463
    +P1275634+P1275643-P1276345-P1276354-P1276435+P1276453+P1276534+P1276543-3.*P1362457
    -3.*P1362475-3.*P1362547-3.*P1362574-3.*P1362745-3.*P1362754-P1423567-P1423576-P1423657
    -P1423675-P1423756-P1423765+P1425367+P1425376-P1425637+P1425673-P1425736+P1425763
    +P1426357+P1426375-P1426537+P1426573-P1426735+P1426753+P1427356+P1427365-P1427536
    +P1427563-P1427635+P1427653)*0.5-P1432576-P1432657-P1432675-P1432756)/Nc);
    fvpart[169] += Nf*((-P1324576-P1324657-P1324675-P1324756-P1325476-P1325746-P1325764-P1326457-P1326475
    -P1326547-P1326574-P1326745-P1326754-P1327456-P1327546-P1327564-P1342576-P1342657
    -P1342675-P1342756-P1352467-P1352476-P1352647-P1352674-P1352746-P1352764-P1372456
    -P1372465-P1372546-P1372564-P1372645-P1372654+(-P1234567-P1234576-P1234657-P1234675
    -P1234756-P1234765-P1235467-P1235476-P1235647-P1235674-P1235746-P1235764-P1236457
    -P1236475-P1236547-P1236574-P1236745-P1236754-P1237456-P1237465-P1237546-P1237564
    -P1237645-P1237654-P1243567-P1243576-P1243657-P1243675-P1243756-P1243765+P1245367
    +P1245376-P1245637+P1245673-P1245736+P1245763+P1246357+P1246375-P1246537+P1246573
    -P1246735+P1246753+P1247356+P1247365-P1247536+P1247563-P1247635+P1247653+P1253467
    +P1253476+P1253647+P1253674+P1253746+P1253764+P1254367+P1254376-P1254637+P1254673
    -P1254736+P1254763-P1256347-P1256374-P1256437+P1256473+P1256734+P1256743-P1257346
    -P1257364-P1257436+P1257463+P1257634+P1257643+P1263457+P1263475+P1263547+P1263574
    +P1263745+P1263754+P1264357+P1264375-P1264537+P1264573-P1264735+P1264753-P1265347
    -P1265374-P1265437+P1265473+P1265734+P1265743-P1267345-P1267354-P1267435+P1267453
    +P1267534+P1267543+P1273456+P1273465+P1273546+P1273564+P1273645+P1273654+P1274356
    +P1274365-P1274536+P1274563-P1274635+P1274653-P1275346-P1275364-P1275436+P1275463
    +P1275634+P1275643-P1276345-P1276354-P1276435+P1276453+P1276534+P1276543-3.*P1362457
    -3.*P1362475-3.*P1362547-3.*P1362574-3.*P1362745-3.*P1362754-P1423567-P1423576-P1423657
    -P1423675-P1423756-P1423765+P1425367+P1425376-P1425637+P1425673-P1425736+P1425763
    +P1426357+P1426375-P1426537+P1426573-P1426735+P1426753+P1427356+P1427365-P1427536
    +P1427563-P1427635+P1427653)*0.5-P1432576-P1432657-P1432675-P1432756)/Nc);
    fvpart[170] += Nf*((-P1235764-P1236457-P1236475-P1236547-P1236574-P1236745-P1236754-P1237564-P1245637
    +P1245763+P1246357+P1246375+P1246573+P1246753+P1247563-P1247635+P1253647+P1253674
    -P1254637+P1254763-P1257364+P1257463+P1273645+P1273654+P1274563-P1274635-P1275364
    +P1275463-P1325764-P1326457-P1326475-P1326547-P1326574-P1326745-P1326754-P1327564
    -P1342576-P1342657-P1342675-P1342756-P1352467-P1352476-P1352647-P1352674-P1352746
    -P1352764-P1372456-P1372465-P1372546-P1372564-P1372645-P1372654-P1423576-P1423657
    -P1423675-P1423756+P1425367-P1425736+P1427365-P1427536+(-P1234567-P1234576-P1234657
    -P1234675-P1234756-P1234765-P1235467-P1235476-P1235746-P1237456-P1237465-P1237546
    -P1243567-P1243576-P1243657-P1243675-P1243756-P1243765+P1245367+P1245376-P1245736
    +P1247356+P1247365-P1247536+P1253467+P1253476+P1253746+P1254367+P1254376-P1254736
    -P1256347-P1256374-P1256437+P1256473+P1256734+P1256743-P1257346-P1257436+P1257634
    +P1257643+P1263457+P1263475+P1263547+P1263574+P1263745+P1263754+P1264357+P1264375
    -P1264537+P1264573-P1264735+P1264753-P1265347-P1265374-P1265437+P1265473+P1265734
    +P1265743-P1267345-P1267354-P1267435+P1267453+P1267534+P1267543+P1273456+P1273465
    +P1273546+P1274356+P1274365-P1274536-P1275346-P1275436+P1275634+P1275643-P1276345
    -P1276354-P1276435+P1276453+P1276534+P1276543-P1324567-P1324576-P1324657-P1324675
    -P1324756-P1324765-P1325467-P1325476-P1325746-P1327456-P1327465-P1327546-3.*P1362457
    -3.*P1362475-3.*P1362547-3.*P1362574-3.*P1362745-3.*P1362754-P1425637+P1425673+P1425763
    +P1426357+P1426375-P1426537+P1426573-P1426735+P1426753+P1427563-P1427635+P1427653)*0.5
    -P1432576-P1432657-P1432675-P1432756)/Nc);
    fvpart[171] += Nf*((-P1235764-P1236457-P1236475-P1236547-P1236574-P1236745-P1236754-P1237564-P1245637
    +P1245763+P1246357+P1246375+P1246573+P1246753+P1247563-P1247635+P1253647+P1253674
    -P1254637+P1254763-P1257364+P1257463+P1273645+P1273654+P1274563-P1274635-P1275364
    +P1275463-P1325764-P1326457-P1326475-P1326547-P1326574-P1326745-P1326754-P1327564
    -P1342576-P1342657-P1342675-P1342756-P1352467-P1352476-P1352647-P1352674-P1352746
    -P1352764-P1372456-P1372465-P1372546-P1372564-P1372645-P1372654-P1423576-P1423657
    -P1423675-P1423756+P1425367-P1425736+P1427365-P1427536+(-P1234567-P1234576-P1234657
    -P1234675-P1234756-P1234765-P1235467-P1235476-P1235746-P1237456-P1237465-P1237546
    -P1243567-P1243576-P1243657-P1243675-P1243756-P1243765+P1245367+P1245376-P1245736
    +P1247356+P1247365-P1247536+P1253467+P1253476+P1253746+P1254367+P1254376-P1254736
    -P1256347-P1256374-P1256437+P1256473+P1256734+P1256743-P1257346-P1257436+P1257634
    +P1257643+P1263457+P1263475+P1263547+P1263574+P1263745+P1263754+P1264357+P1264375
    -P1264537+P1264573-P1264735+P1264753-P1265347-P1265374-P1265437+P1265473+P1265734
    +P1265743-P1267345-P1267354-P1267435+P1267453+P1267534+P1267543+P1273456+P1273465
    +P1273546+P1274356+P1274365-P1274536-P1275346-P1275436+P1275634+P1275643-P1276345
    -P1276354-P1276435+P1276453+P1276534+P1276543-P1324567-P1324576-P1324657-P1324675
    -P1324756-P1324765-P1325467-P1325476-P1325746-P1327456-P1327465-P1327546-3.*P1362457
    -3.*P1362475-3.*P1362547-3.*P1362574-3.*P1362745-3.*P1362754-P1425637+P1425673+P1425763
    +P1426357+P1426375-P1426537+P1426573-P1426735+P1426753+P1427563-P1427635+P1427653)*0.5
    -P1432576-P1432657-P1432675-P1432756)/Nc);
    fvpart[172] += Nf*((-P1237645-P1237654+P1245673-P1246735+P1253764+P1254673-P1324657-P1324756-P1324765
    -P1326457-P1326547-P1326574-P1327456-P1327465-P1327546+P1342567-P1342657-P1342756
    -P1352467-P1352476-P1352647-P1352674-P1352746-P1352764-P1362457-P1362475-P1362547
    -P1362574-P1362745-P1362754-P1372564-P1372645-P1372654-P1423657-P1423675+P1425367
    +P1425376-P1425736-P1426537+P1427365-P1427536+P1427653+P1432567-P1432657-P1432756
    +(-P1234567-P1234576-P1234657-P1234675-P1234756-3.*P1234765-P1235467-P1235476-P1235647
    -P1235674-P1235746-P1236457-P1236475-P1236547-P1236574-P1236745-P1236754-P1237456
    -3.*P1237465-P1237546-P1243567-P1243576-P1243657-P1243675-P1243756-3.*P1243765+P1245367
    +P1245376-P1245637-P1245736+P1245763+P1246357+P1246375-P1246537+P1247356+P1247365
    -P1247536+P1247563-P1247635+P1247653+P1253467+P1253476+P1253647+P1253674+P1253746
    +P1254367+P1254376-P1254637-P1254736+P1254763-P1256347-P1256374-P1256437+3.*P1256473
    +3.*P1256734+3.*P1256743-P1257346-P1257364-P1257436+P1257463+P1257634+P1257643+P1263457
    +P1263475+P1263547+P1263574+P1263745+P1263754+P1264357+P1264375-P1264537+P1264573
    -P1264735+P1264753-P1265347-P1265374-P1265437+P1265473+P1265734+P1265743-P1267345
    -P1267354-P1267435+P1267453+P1267534+P1267543+P1273456+P1273465+P1273546+P1273564
    +P1273645+P1273654+P1274356+P1274365-P1274536+P1274563-P1274635+P1274653-P1275346
    -P1275364-P1275436+P1275463+P1275634+P1275643-P1276345-P1276354-P1276435+P1276453
    +P1276534+P1276543+P1325764-P1327564-P1327645-P1327654-3.*P1372456-3.*P1372465-3.*P1372546
    +5.*P1425673+P1426573-P1426735+P1426753-P1462357-P1462375-P1462537)*0.5-P1523476-P1523746
    -P1523764-P1524376-P1524673)/Nc);
    fvpart[173] += Nf*((-P1237645-P1237654+P1245673-P1246735+P1253764+P1254673-P1324657-P1324756-P1324765
    -P1326457-P1326547-P1326574-P1327456-P1327465-P1327546+P1342567-P1342657-P1342756
    -P1352467-P1352476-P1352647-P1352674-P1352746-P1352764-P1362457-P1362475-P1362547
    -P1362574-P1362745-P1362754-P1372564-P1372645-P1372654-P1423657-P1423675+P1425367
    +P1425376-P1425736-P1426537+P1427365-P1427536+P1427653+P1432567-P1432657-P1432756
    +(-P1234567-P1234576-P1234657-P1234675-P1234756-3.*P1234765-P1235467-P1235476-P1235647
    -P1235674-P1235746-P1236457-P1236475-P1236547-P1236574-P1236745-P1236754-P1237456
    -3.*P1237465-P1237546-P1243567-P1243576-P1243657-P1243675-P1243756-3.*P1243765+P1245367
    +P1245376-P1245637-P1245736+P1245763+P1246357+P1246375-P1246537+P1247356+P1247365
    -P1247536+P1247563-P1247635+P1247653+P1253467+P1253476+P1253647+P1253674+P1253746
    +P1254367+P1254376-P1254637-P1254736+P1254763-P1256347-P1256374-P1256437+3.*P1256473
    +3.*P1256734+3.*P1256743-P1257346-P1257364-P1257436+P1257463+P1257634+P1257643+P1263457
    +P1263475+P1263547+P1263574+P1263745+P1263754+P1264357+P1264375-P1264537+P1264573
    -P1264735+P1264753-P1265347-P1265374-P1265437+P1265473+P1265734+P1265743-P1267345
    -P1267354-P1267435+P1267453+P1267534+P1267543+P1273456+P1273465+P1273546+P1273564
    +P1273645+P1273654+P1274356+P1274365-P1274536+P1274563-P1274635+P1274653-P1275346
    -P1275364-P1275436+P1275463+P1275634+P1275643-P1276345-P1276354-P1276435+P1276453
    +P1276534+P1276543+P1325764-P1327564-P1327645-P1327654-3.*P1372456-3.*P1372465-3.*P1372546
    +5.*P1425673+P1426573-P1426735+P1426753-P1462357-P1462375-P1462537)*0.5-P1523476-P1523746
    -P1523764-P1524376-P1524673)/Nc);
    fvpart[174] += Nf*((P1234657+P1234675+P1234756+P1235476-P1235674+P1236457+P1236475+P1236547+P1243657
    +P1243675+P1243756-P1245367-P1245376+P1245637-P1246357-P1246375+P1247653-P1253467
    -P1253647-P1254367+P1254637+P1256347+P1256437+P1257346+P1257364+P1257436-P1257463
    -P1257634-P1257643-P1263457-P1263475-P1263547-P1264357-P1264375-P1265734-P1265743
    +P1267354-P1267453-P1273465-P1273645-P1273654-P1274365+P1274536-P1274563+P1274635
    +P1275346+P1275364+P1275436-P1275463-P1275634-P1275643+P1276345+P1276354+P1276435
    -P1327465-P1327645-P1327654-P1342567+P1342576+P1342657+P1352467+P1352476+P1352647
    +P1352674+P1352746+P1352764+2.*(P1234765+P1236574+P1243765-P1247563+P1247635-P1253674
    -P1256734-P1256743+P1325746+P1325764+P1362574+P1362745+P1362754)+P1426735+P1427536
    -P1432567+P1432576+P1432657+3.*(P1236745+P1236754-P1245763-P1254763+P1342675+P1432675)
    +(P1235746+P1235764+P1237456-P1237465-P1237546-P1237564-P1237645-3.*P1237654+3.*P1245673
    -P1245736+P1246573+P1246735-P1246753-P1247356+P1247365+P1247536-P1253746-P1253764
    +P1254673-P1254736-P1256374+P1256473-P1263574+P1263745+P1263754+P1264573+P1264735
    -P1264753+P1265374-P1265473+P1324567+P1324576+3.*P1324657+5.*P1324675-P1324756+P1324765
    -P1325467+P1325476-P1325647-5.*P1325674+3.*P1326457+5.*P1326475+P1326547+P1326574+5.*P1326745
    +7.*P1326754+5.*P1362457+5.*P1362475+5.*P1362547-P1423567+P1423576+3.*P1423657+3.*P1423675
    +P1423756+P1423765-3.*P1425367-P1425376+P1425637-5.*P1425763-P1426357-P1426375+P1426537
    -3.*P1427563+3.*P1427635+P1427653+P1472356+P1472365+P1472536)*0.5+P1523476+P1523674-P1523746
    -P1523764+P1524376-P1526734-P1526743-P1623457+P1623745-P1624357-P1624537)/Nc);
    fvpart[175] += Nf*((P1234657+P1234675+P1234756+P1235476-P1235674+P1236457+P1236475+P1236547+P1243657
    +P1243675+P1243756-P1245367-P1245376+P1245637-P1246357-P1246375+P1247653-P1253467
    -P1253647-P1254367+P1254637+P1256347+P1256437+P1257346+P1257364+P1257436-P1257463
    -P1257634-P1257643-P1263457-P1263475-P1263547-P1264357-P1264375-P1265734-P1265743
    +P1267354-P1267453-P1273465-P1273645-P1273654-P1274365+P1274536-P1274563+P1274635
    +P1275346+P1275364+P1275436-P1275463-P1275634-P1275643+P1276345+P1276354+P1276435
    -P1327465-P1327645-P1327654-P1342567+P1342576+P1342657+P1352467+P1352476+P1352647
    +P1352674+P1352746+P1352764+2.*(P1234765+P1236574+P1243765-P1247563+P1247635-P1253674
    -P1256734-P1256743+P1325746+P1325764+P1362574+P1362745+P1362754)+P1426735+P1427536
    -P1432567+P1432576+P1432657+3.*(P1236745+P1236754-P1245763-P1254763+P1342675+P1432675)
    +(P1235746+P1235764+P1237456-P1237465-P1237546-P1237564-P1237645-3.*P1237654+3.*P1245673
    -P1245736+P1246573+P1246735-P1246753-P1247356+P1247365+P1247536-P1253746-P1253764
    +P1254673-P1254736-P1256374+P1256473-P1263574+P1263745+P1263754+P1264573+P1264735
    -P1264753+P1265374-P1265473+P1324567+P1324576+3.*P1324657+5.*P1324675-P1324756+P1324765
    -P1325467+P1325476-P1325647-5.*P1325674+3.*P1326457+5.*P1326475+P1326547+P1326574+5.*P1326745
    +7.*P1326754+5.*P1362457+5.*P1362475+5.*P1362547-P1423567+P1423576+3.*P1423657+3.*P1423675
    +P1423756+P1423765-3.*P1425367-P1425376+P1425637-5.*P1425763-P1426357-P1426375+P1426537
    -3.*P1427563+3.*P1427635+P1427653+P1472356+P1472365+P1472536)*0.5+P1523476+P1523674-P1523746
    -P1523764+P1524376-P1526734-P1526743-P1623457+P1623745-P1624357-P1624537)/Nc);
    fvpart[176] += Nf*((P1324576+P1324675+P1325476+P1325746+P1325764+P1326475+P1326745+P1326754+P1342576
    +P1342675+P1352467+P1352476+P1352647+P1352674+P1352746+P1352764+P1362457+P1362475
    +P1362547+P1362574+P1362745+P1362754+(P1234567+P1234576+P1234657+P1234675+P1234756
    +P1234765+P1235467+P1235476+P1235647+P1235674+P1235746+P1235764+P1236457+P1236475
    +P1236547+P1236574+P1236745+P1236754+P1237456+P1237465+P1237546+P1237564+P1237645
    +P1237654+P1243567+P1243576+P1243657+P1243675+P1243756+P1243765-P1245367-P1245376
    +P1245637-P1245673+P1245736-P1245763-P1246357-P1246375+P1246537-P1246573+P1246735
    -P1246753-P1247356-P1247365+P1247536-P1247563+P1247635-P1247653-P1253467-P1253476
    -P1253647-P1253674-P1253746-P1253764-P1254367-P1254376+P1254637-P1254673+P1254736
    -P1254763+P1256347+P1256374+P1256437-P1256473-P1256734-P1256743+P1257346+P1257364
    +P1257436-P1257463-P1257634-P1257643-P1263457-P1263475-P1263547-P1263574-P1263745
    -P1263754-P1264357-P1264375+P1264537-P1264573+P1264735-P1264753+P1265347+P1265374
    +P1265437-P1265473-P1265734-P1265743+P1267345+P1267354+P1267435-P1267453-P1267534
    -P1267543-P1273456-P1273465-P1273546-P1273564-P1273645-P1273654-P1274356-P1274365
    +P1274536-P1274563+P1274635-P1274653+P1275346+P1275364+P1275436-P1275463-P1275634
    -P1275643+P1276345+P1276354+P1276435-P1276453-P1276534-P1276543+P1372456+P1372465
    +P1372546+P1372564+P1372645+P1372654+P1423567+P1423576+P1423657+P1423675+P1423756
    +P1423765-P1425367-P1425376+P1425637-P1425673+P1425736-P1425763-P1426357-P1426375
    +P1426537-P1426573+P1426735-P1426753-P1427356-P1427365+P1427536-P1427563+P1427635
    -P1427653)*0.5+P1432576+P1432675)/Nc);
    fvpart[177] += Nf*((P1324576+P1324675+P1325476+P1325746+P1325764+P1326475+P1326745+P1326754+P1342576
    +P1342675+P1352467+P1352476+P1352647+P1352674+P1352746+P1352764+P1362457+P1362475
    +P1362547+P1362574+P1362745+P1362754+(P1234567+P1234576+P1234657+P1234675+P1234756
    +P1234765+P1235467+P1235476+P1235647+P1235674+P1235746+P1235764+P1236457+P1236475
    +P1236547+P1236574+P1236745+P1236754+P1237456+P1237465+P1237546+P1237564+P1237645
    +P1237654+P1243567+P1243576+P1243657+P1243675+P1243756+P1243765-P1245367-P1245376
    +P1245637-P1245673+P1245736-P1245763-P1246357-P1246375+P1246537-P1246573+P1246735
    -P1246753-P1247356-P1247365+P1247536-P1247563+P1247635-P1247653-P1253467-P1253476
    -P1253647-P1253674-P1253746-P1253764-P1254367-P1254376+P1254637-P1254673+P1254736
    -P1254763+P1256347+P1256374+P1256437-P1256473-P1256734-P1256743+P1257346+P1257364
    +P1257436-P1257463-P1257634-P1257643-P1263457-P1263475-P1263547-P1263574-P1263745
    -P1263754-P1264357-P1264375+P1264537-P1264573+P1264735-P1264753+P1265347+P1265374
    +P1265437-P1265473-P1265734-P1265743+P1267345+P1267354+P1267435-P1267453-P1267534
    -P1267543-P1273456-P1273465-P1273546-P1273564-P1273645-P1273654-P1274356-P1274365
    +P1274536-P1274563+P1274635-P1274653+P1275346+P1275364+P1275436-P1275463-P1275634
    -P1275643+P1276345+P1276354+P1276435-P1276453-P1276534-P1276543+P1372456+P1372465
    +P1372546+P1372564+P1372645+P1372654+P1423567+P1423576+P1423657+P1423675+P1423756
    +P1423765-P1425367-P1425376+P1425637-P1425673+P1425736-P1425763-P1426357-P1426375
    +P1426537-P1426573+P1426735-P1426753-P1427356-P1427365+P1427536-P1427563+P1427635
    -P1427653)*0.5+P1432576+P1432675)/Nc);
    fvpart[178] += Nf*((P1235746+P1235764+P1236745+P1236754-P1245763-P1246753+P1247536+P1247635-P1253674
    -P1254763-P1263574-P1264753+P1325746+P1325764+P1326745+P1326754+P1342576+P1342675
    +P1352467+P1352476+P1352647+P1352674+P1352746+P1352764+P1362457+P1362475+P1362547
    +P1362574+P1362745+P1362754+P1423576+P1423675-P1425367-P1426357+(P1234567+P1234576
    +P1234657+P1234675+P1234756+P1234765+P1235467+P1235476+P1235647+P1236457+P1236475
    +P1236547+P1243567+P1243576+P1243657+P1243675+P1243756+P1243765-P1245367-P1245376
    +P1245637-P1246357-P1246375+P1246537-P1253467-P1253476-P1253647-P1254367-P1254376
    +P1254637+P1256347+P1256437-P1256734-P1256743+P1257346+P1257364+P1257436-P1257463
    -P1257634-P1257643-P1263457-P1263475-P1263547-P1264357-P1264375+P1264537+P1265347
    +P1265437-P1265734-P1265743+P1267345+P1267354+P1267435-P1267453-P1267534-P1267543
    -P1273456-P1273465-P1273546-P1273564-P1273645-P1273654-P1274356-P1274365+P1274536
    -P1274563+P1274635-P1274653+P1275346+P1275364+P1275436-P1275463-P1275634-P1275643
    +P1276345+P1276354+P1276435-P1276453-P1276534-P1276543+P1324567+P1324576+P1324657
    +P1324675+P1324756+P1324765+P1325467+P1325476+P1325647+P1326457+P1326475+P1326547
    +P1372456+P1372465+P1372546+P1372564+P1372645+P1372654-P1425673+P1425736-P1425763
    -P1426573+P1426735-P1426753-P1427356-P1427365+P1427536-P1427563+P1427635-P1427653)*0.5
    +P1432576+P1432675)/Nc);
    fvpart[179] += Nf*((P1235746+P1235764+P1236745+P1236754-P1245763-P1246753+P1247536+P1247635-P1253674
    -P1254763-P1263574-P1264753+P1325746+P1325764+P1326745+P1326754+P1342576+P1342675
    +P1352467+P1352476+P1352647+P1352674+P1352746+P1352764+P1362457+P1362475+P1362547
    +P1362574+P1362745+P1362754+P1423576+P1423675-P1425367-P1426357+(P1234567+P1234576
    +P1234657+P1234675+P1234756+P1234765+P1235467+P1235476+P1235647+P1236457+P1236475
    +P1236547+P1243567+P1243576+P1243657+P1243675+P1243756+P1243765-P1245367-P1245376
    +P1245637-P1246357-P1246375+P1246537-P1253467-P1253476-P1253647-P1254367-P1254376
    +P1254637+P1256347+P1256437-P1256734-P1256743+P1257346+P1257364+P1257436-P1257463
    -P1257634-P1257643-P1263457-P1263475-P1263547-P1264357-P1264375+P1264537+P1265347
    +P1265437-P1265734-P1265743+P1267345+P1267354+P1267435-P1267453-P1267534-P1267543
    -P1273456-P1273465-P1273546-P1273564-P1273645-P1273654-P1274356-P1274365+P1274536
    -P1274563+P1274635-P1274653+P1275346+P1275364+P1275436-P1275463-P1275634-P1275643
    +P1276345+P1276354+P1276435-P1276453-P1276534-P1276543+P1324567+P1324576+P1324657
    +P1324675+P1324756+P1324765+P1325467+P1325476+P1325647+P1326457+P1326475+P1326547
    +P1372456+P1372465+P1372546+P1372564+P1372645+P1372654-P1425673+P1425736-P1425763
    -P1426573+P1426735-P1426753-P1427356-P1427365+P1427536-P1427563+P1427635-P1427653)*0.5
    +P1432576+P1432675)/Nc);
    fvpart[180] += Nf*((-P1237564-P1237645-P1237654+P1245673+P1246573-P1246735+P1253764+P1254673-P1324756
    -P1324765-P1325647-P1325674-P1326457-P1326547-P1326574-P1327456-P1327465-P1327546
    +P1342576-P1342657-P1342756-P1352467-P1352476-P1352647-P1352674-P1352746-P1352764
    -P1362457-P1362475-P1362547-P1362574-P1362745-P1362754-P1372564-P1372645-P1372654
    -P1423657-P1423675-P1423756+P1425367+P1425376+P1425763-P1426537+P1427365-P1427536
    +P1427653+P1432576-P1432657-P1432756+(-P1234567-P1234576-P1234657-3.*P1234675-P1234756
    -P1234765-P1235467-P1235476-P1235647-P1235674-P1235746-P1236457-P1236475-P1236547
    -P1236574-P1236745-P1236754-P1237456-P1237465-P1237546-P1243567-P1243576-P1243657
    -3.*P1243675-P1243756-P1243765+P1245367+P1245376-P1245637-P1245736+P1245763+P1246357
    -P1246375-P1246537+P1247356+P1247365-P1247536+P1247563-P1247635+P1247653+P1253467
    +P1253476+P1253647+P1253674+P1253746+P1254367+P1254376-P1254637-P1254736+P1254763
    -P1256347-P1256374-P1256437+P1256473+P1256734+P1256743-P1257346+P1257364-P1257436
    +P1257463+3.*P1257634+3.*P1257643+P1263457+P1263475+P1263547+P1263574+P1263745+P1263754
    +P1264357+P1264375-P1264537+P1264573-P1264735+P1264753-P1265347-P1265374-P1265437
    +P1265473+P1265734+P1265743-P1267345-P1267354-P1267435+P1267453+P1267534+P1267543
    +P1273456+P1273465+P1273546+P1273564+P1273645+P1273654+P1274356+P1274365-P1274536
    +P1274563-P1274635+P1274653-P1275346-P1275364-P1275436+P1275463+P1275634+P1275643
    -P1276345-P1276354-P1276435+P1276453+P1276534+P1276543+3.*P1325764-P1327564-P1327645
    -P1327654-3.*P1372456-3.*P1372465-3.*P1372546+3.*P1425673+P1426573-P1426735+P1426753-P1462357
    -P1462375-P1462537)*0.5-P1523467-P1523764-P1524367-P1524637-P1524673)/Nc);
    fvpart[181] += Nf*((-P1237564-P1237645-P1237654+P1245673+P1246573-P1246735+P1253764+P1254673-P1324756
    -P1324765-P1325647-P1325674-P1326457-P1326547-P1326574-P1327456-P1327465-P1327546
    +P1342576-P1342657-P1342756-P1352467-P1352476-P1352647-P1352674-P1352746-P1352764
    -P1362457-P1362475-P1362547-P1362574-P1362745-P1362754-P1372564-P1372645-P1372654
    -P1423657-P1423675-P1423756+P1425367+P1425376+P1425763-P1426537+P1427365-P1427536
    +P1427653+P1432576-P1432657-P1432756+(-P1234567-P1234576-P1234657-3.*P1234675-P1234756
    -P1234765-P1235467-P1235476-P1235647-P1235674-P1235746-P1236457-P1236475-P1236547
    -P1236574-P1236745-P1236754-P1237456-P1237465-P1237546-P1243567-P1243576-P1243657
    -3.*P1243675-P1243756-P1243765+P1245367+P1245376-P1245637-P1245736+P1245763+P1246357
    -P1246375-P1246537+P1247356+P1247365-P1247536+P1247563-P1247635+P1247653+P1253467
    +P1253476+P1253647+P1253674+P1253746+P1254367+P1254376-P1254637-P1254736+P1254763
    -P1256347-P1256374-P1256437+P1256473+P1256734+P1256743-P1257346+P1257364-P1257436
    +P1257463+3.*P1257634+3.*P1257643+P1263457+P1263475+P1263547+P1263574+P1263745+P1263754
    +P1264357+P1264375-P1264537+P1264573-P1264735+P1264753-P1265347-P1265374-P1265437
    +P1265473+P1265734+P1265743-P1267345-P1267354-P1267435+P1267453+P1267534+P1267543
    +P1273456+P1273465+P1273546+P1273564+P1273645+P1273654+P1274356+P1274365-P1274536
    +P1274563-P1274635+P1274653-P1275346-P1275364-P1275436+P1275463+P1275634+P1275643
    -P1276345-P1276354-P1276435+P1276453+P1276534+P1276543+3.*P1325764-P1327564-P1327645
    -P1327654-3.*P1372456-3.*P1372465-3.*P1372546+3.*P1425673+P1426573-P1426735+P1426753-P1462357
    -P1462375-P1462537)*0.5-P1523467-P1523764-P1524367-P1524637-P1524673)/Nc);
    fvpart[182] += Nf*((-P1235674-P1235746-P1237465-P1237546-P1245736+P1247365-P1247536+P1247653-P1254736
    -P1256374+P1256473+P1263574+P1263745+P1263754+P1264573+P1264753-P1325674-P1326574
    +P1326754-P1327456-P1327465-P1327546-P1342657+P1342675-P1342756-P1352467-P1352476
    -P1352647-P1352674-P1352746-P1352764-P1362457-P1362475-P1362547-P1362574-P1362745
    -P1362754-P1372564-P1372645-P1372654-P1425736+P1427365-P1427536+P1427653-P1432657
    +P1432675-P1432756+(-P1234567-3.*P1234576-P1234657-P1234675-P1234756-P1234765-P1235467
    -P1235476-P1235647-P1235764-P1236457-P1236475-P1236547-P1237564-P1237645-3.*P1237654
    -P1243567-3.*P1243576-P1243657-P1243675-P1243756-P1243765+P1245367-P1245376-P1245637
    +3.*P1245673+P1246357+P1246375-P1246537+P1246573-P1246735+P1246753+P1253467+P1253476
    +P1253647+P1253764+P1254367+P1254376-P1254637+P1254673-P1256347-P1256437+P1256734
    +P1256743-P1257346-P1257364-P1257436+P1257463+P1257634+P1257643+P1263457+P1263475
    +P1263547+P1264357+P1264375-P1264537-P1265347-P1265437+P1265734+P1265743-P1267345
    +P1267354-P1267435+P1267453+3.*P1267534+3.*P1267543+P1273456+P1273465+P1273546+P1273564
    +P1273645+P1273654+P1274356+P1274365-P1274536+P1274563-P1274635+P1274653-P1275346
    -P1275364-P1275436+P1275463+P1275634+P1275643-P1276345-P1276354-P1276435+P1276453
    +P1276534+P1276543+P1324567-P1324576-P1324657+P1324675-3.*P1324756-P1324765-P1325467
    -P1325476-P1325647+P1325764-P1326457+P1326475-3.*P1326547-P1327564-P1327645-P1327654
    -3.*P1372456-3.*P1372465-3.*P1372546-P1423567-P1423576-P1423657-P1423675-P1423756-P1423765
    +P1425367+P1425376-P1425637+3.*P1425673+P1426357+P1426375-P1426537+P1426573+P1426735
    +3.*P1426753-P1462357-P1462375-P1462537)*0.5-P1523746-P1523764-P1524673-P1623457+P1623745
    -P1624357-P1624537)/Nc);
    fvpart[183] += Nf*((-P1235674-P1235746-P1237465-P1237546-P1245736+P1247365-P1247536+P1247653-P1254736
    -P1256374+P1256473+P1263574+P1263745+P1263754+P1264573+P1264753-P1325674-P1326574
    +P1326754-P1327456-P1327465-P1327546-P1342657+P1342675-P1342756-P1352467-P1352476
    -P1352647-P1352674-P1352746-P1352764-P1362457-P1362475-P1362547-P1362574-P1362745
    -P1362754-P1372564-P1372645-P1372654-P1425736+P1427365-P1427536+P1427653-P1432657
    +P1432675-P1432756+(-P1234567-3.*P1234576-P1234657-P1234675-P1234756-P1234765-P1235467
    -P1235476-P1235647-P1235764-P1236457-P1236475-P1236547-P1237564-P1237645-3.*P1237654
    -P1243567-3.*P1243576-P1243657-P1243675-P1243756-P1243765+P1245367-P1245376-P1245637
    +3.*P1245673+P1246357+P1246375-P1246537+P1246573-P1246735+P1246753+P1253467+P1253476
    +P1253647+P1253764+P1254367+P1254376-P1254637+P1254673-P1256347-P1256437+P1256734
    +P1256743-P1257346-P1257364-P1257436+P1257463+P1257634+P1257643+P1263457+P1263475
    +P1263547+P1264357+P1264375-P1264537-P1265347-P1265437+P1265734+P1265743-P1267345
    +P1267354-P1267435+P1267453+3.*P1267534+3.*P1267543+P1273456+P1273465+P1273546+P1273564
    +P1273645+P1273654+P1274356+P1274365-P1274536+P1274563-P1274635+P1274653-P1275346
    -P1275364-P1275436+P1275463+P1275634+P1275643-P1276345-P1276354-P1276435+P1276453
    +P1276534+P1276543+P1324567-P1324576-P1324657+P1324675-3.*P1324756-P1324765-P1325467
    -P1325476-P1325647+P1325764-P1326457+P1326475-3.*P1326547-P1327564-P1327645-P1327654
    -3.*P1372456-3.*P1372465-3.*P1372546-P1423567-P1423576-P1423657-P1423675-P1423756-P1423765
    +P1425367+P1425376-P1425637+3.*P1425673+P1426357+P1426375-P1426537+P1426573+P1426735
    +3.*P1426753-P1462357-P1462375-P1462537)*0.5-P1523746-P1523764-P1524673-P1623457+P1623745
    -P1624357-P1624537)/Nc);
    fvpart[184] += Nf*((P1237465+P1237546+P1237564+P1237645+P1237654-P1245673+P1245736-P1246573-P1247365
    -P1254673+P1254736+P1256374-P1256473-P1263745-P1263754-P1264573-P1324675+P1324756
    +P1325674-P1326475+P1326574+P1327456+P1327465+P1327546+P1327564+P1327645-P1342675
    +P1342756+P1372456+P1372465+P1372645-P1425673+P1425736-P1426573-P1427365-P1432675
    +P1432756+(-P1235467-P1235476-P1235647-P1235746-P1235764+P1236547-3.*P1236745-3.*P1236754
    +P1245367+P1245376+P1245637+3.*P1245763-P1246537+P1246753-P1247536-P1247635+P1253674
    +3.*P1254763+P1263547+P1263574-P1264537+P1264753-P1267354+P1267453+P1273546+P1273564
    -P1273654-P1274536-P1274563+P1274653-P1276354+P1276453-P1325467-P1325476-P1325647
    -3.*P1325746-3.*P1325764+P1326547-P1326745-3.*P1326754-P1352467-P1352476-P1352647-P1352674
    -P1352746-P1352764-P1362457-P1362475-P1362547+P1372546+P1372564+P1372654+P1425367
    +P1425376+P1425637+3.*P1425763-P1426537+P1426753-P1427536-P1427635+P1452367+P1452376
    +P1452637-P1472356-P1472365-P1472536)*0.5-P1523674+P1523746+P1523764-P1623745)/Nc);
    fvpart[185] += Nf*((P1236457+P1236475+P1236574+P1237465+P1237546+P1237564+P1245736-P1246357-P1246375
    -P1246573-P1247365-P1247563-P1253647+P1254637+P1254736+P1256374-P1256473+P1257364
    -P1257463-P1263745-P1263754-P1264573-P1273645+P1274635+P1275364-P1275463+P1324576
    +P1324657+P1324756-P1325647+P1325764+2.*P1326547+P1326745+P1342576+P1342657+P1342756
    +P1372456+P1372465+P1372564+P1372654+P1425736-P1426357-P1426375-P1426573-P1427365
    -P1427563+P1432576+P1432657+P1432756+(P1234567+P1234576+P1234657+P1234675+P1234756
    +P1234765-P1235647+P1235746+P1235764+3.*P1236547+P1236745+P1236754+P1243567+P1243576
    +P1243657+P1243675+P1243756+P1243765+3.*P1245637-P1245763-P1246537-P1246753+P1247536
    +P1247635-P1253467-P1253476-P1253674-P1254367-P1254376-P1254763+P1256347+P1256437
    -P1256734-P1256743+P1257346+P1257436-P1257634-P1257643-P1263457-P1263475-P1263574
    -P1264357-P1264375-P1264753+P1265347+P1265437-P1265734-P1265743+P1267345+P1267435
    -P1267534-P1267543-P1273456-P1273465+P1273564-3.*P1273654-P1274356-P1274365-3.*P1274563
    +P1274653+P1275346+P1275436-P1275634-P1275643+P1276345+P1276435-P1276534-P1276543
    -P1325467+P1325476-P1325674+3.*P1326457+P1326475+3.*P1326574+P1327456+P1327465+P1327546
    +3.*P1327564+P1327645-P1327654+P1352467+P1352476+P1352647+P1352674+P1352746+P1352764
    +3.*P1362457+3.*P1362475+3.*P1362547+3.*P1362574+3.*P1362745+3.*P1362754+P1372546+3.*P1372645
    +P1423567+P1423576+P1423657+P1423675+P1423756+P1423765+3.*P1425637-P1425763-P1426537
    -P1426753+P1427536+P1427635+P1452367+P1452376+P1452637-P1462357-P1462375-P1462537)*0.5
    +P1523746-P1623745)/Nc);
    fvpart[186] += Nf*((-P1236457-P1236475-P1236574-P1237465-P1237546-P1237564-P1245736+P1246357+P1246375
    +P1246573+P1247365+P1247563+P1253647-P1254637-P1254736-P1256374+P1256473-P1257364
    +P1257463+P1263745+P1263754+P1264573+P1273645-P1274635-P1275364+P1275463-P1324576
    -P1324657-P1324756+P1325647-P1325764-2.*P1326547-P1326745-P1342576-P1342657-P1342756
    -P1372456-P1372465-P1372564-P1372654-P1425736+P1426357+P1426375+P1426573+P1427365
    +P1427563-P1432576-P1432657-P1432756+(-P1234567-P1234576-P1234657-P1234675-P1234756
    -P1234765+P1235647-P1235746-P1235764-3.*P1236547-P1236745-P1236754-P1243567-P1243576
    -P1243657-P1243675-P1243756-P1243765-3.*P1245637+P1245763+P1246537+P1246753-P1247536
    -P1247635+P1253467+P1253476+P1253674+P1254367+P1254376+P1254763-P1256347-P1256437
    +P1256734+P1256743-P1257346-P1257436+P1257634+P1257643+P1263457+P1263475+P1263574
    +P1264357+P1264375+P1264753-P1265347-P1265437+P1265734+P1265743-P1267345-P1267435
    +P1267534+P1267543+P1273456+P1273465-P1273564+3.*P1273654+P1274356+P1274365+3.*P1274563
    -P1274653-P1275346-P1275436+P1275634+P1275643-P1276345-P1276435+P1276534+P1276543
    +P1325467-P1325476+P1325674-3.*P1326457-P1326475-3.*P1326574-P1327456-P1327465-P1327546
    -3.*P1327564-P1327645+P1327654-P1352467-P1352476-P1352647-P1352674-P1352746-P1352764
    -3.*P1362457-3.*P1362475-3.*P1362547-3.*P1362574-3.*P1362745-3.*P1362754-P1372546-3.*P1372645
    -P1423567-P1423576-P1423657-P1423675-P1423756-P1423765-3.*P1425637+P1425763+P1426537
    +P1426753-P1427536-P1427635-P1452367-P1452376-P1452637+P1462357+P1462375+P1462537)*0.5
    -P1523746+P1623745)/Nc);
    fvpart[187] += Nf*((-P1237465-P1237546-P1237564-P1237645-P1237654+P1245673-P1245736+P1246573+P1247365
    +P1254673-P1254736-P1256374+P1256473+P1263745+P1263754+P1264573+P1324675-P1324756
    -P1325674+P1326475-P1326574-P1327456-P1327465-P1327546-P1327564-P1327645+P1342675
    -P1342756-P1372456-P1372465-P1372645+P1425673-P1425736+P1426573+P1427365+P1432675
    -P1432756+(P1235467+P1235476+P1235647+P1235746+P1235764-P1236547+3.*P1236745+3.*P1236754
    -P1245367-P1245376-P1245637-3.*P1245763+P1246537-P1246753+P1247536+P1247635-P1253674
    -3.*P1254763-P1263547-P1263574+P1264537-P1264753+P1267354-P1267453-P1273546-P1273564
    +P1273654+P1274536+P1274563-P1274653+P1276354-P1276453+P1325467+P1325476+P1325647
    +3.*P1325746+3.*P1325764-P1326547+P1326745+3.*P1326754+P1352467+P1352476+P1352647+P1352674
    +P1352746+P1352764+P1362457+P1362475+P1362547-P1372546-P1372564-P1372654-P1425367
    -P1425376-P1425637-3.*P1425763+P1426537-P1426753+P1427536+P1427635-P1452367-P1452376
    -P1452637+P1472356+P1472365+P1472536)*0.5+P1523674-P1523746-P1523764+P1623745)/Nc);
    fvpart[188] += Nf*((-P1234567-P1234576-P1234657-P1234675-P1234756-P1234765-P1235467-P1235476-P1243567
    -P1243576-P1243657-P1243675-P1243756-P1243765+P1245367+P1245376+P1253467+P1253476
    +P1254367+P1254376-P1256347-P1256437+P1256734+P1256743-P1257346-P1257436+P1257634
    +P1257643+P1263457+P1263475+P1263547+P1264357+P1264375-P1264537-P1265347-P1265437
    +P1265734+P1265743-P1267345-P1267354-P1267435+P1267453+P1267534+P1267543+P1273456
    +P1273465+P1273546+P1274356+P1274365-P1274536-P1275346-P1275436+P1275634+P1275643
    -P1276345-P1276354-P1276435+P1276453+P1276534+P1276543-P1324567-P1324576-P1324657
    -P1324756-P1325467-P1325476-P1342567-P1342576-P1342657-P1342756+3.*(-P1362457-P1362475
    -P1362547)-P1372456-P1372465-P1372546-P1423567-P1423576-P1423657-P1423675-P1423756
    -P1423765+P1425367+P1425376-P1432567-P1432576-P1432657+2.*(-P1324675-P1342675-P1352467
    -P1352476-P1352647-P1352674-P1352746-P1352764-P1432675)-P1432756+(-P1235647-P1235674
    -3.*P1235746-3.*P1235764-3.*P1236457-3.*P1236475-3.*P1236547-3.*P1236574-5.*P1236745-5.*P1236754
    -P1237456-P1237465-P1237546-P1237564+P1237645+P1237654-3.*P1245637-P1245673-P1245736
    +5.*P1245763+3.*P1246357+3.*P1246375-P1246537+P1246573-P1246735+3.*P1246753+P1247356+P1247365
    -3.*P1247536+3.*P1247563-3.*P1247635+P1247653+3.*P1253647+3.*P1253674+P1253746+P1253764
    -3.*P1254637-P1254673-P1254736+5.*P1254763-P1256374+P1256473-3.*P1257364+3.*P1257463+3.*P1263574
    +P1263745+P1263754+P1264573-P1264735+3.*P1264753-P1265374+P1265473+P1273564+3.*P1273645
    +3.*P1273654+3.*P1274563-3.*P1274635+P1274653-3.*P1275364+3.*P1275463-P1325647+P1325674
    -3.*P1325746-5.*P1325764-3.*P1326457-5.*P1326475-3.*P1326547-P1326574-5.*P1326745-5.*P1326754
    -P1327456+P1327465-P1327546-3.*P1327564+P1327645+P1327654-5.*P1362574-5.*P1362745-5.*P1362754
    -3.*P1372564-3.*P1372645-3.*P1372654-3.*P1425637-P1425673-P1425736+5.*P1425763+3.*P1426357
    +3.*P1426375-P1426537+P1426573-P1426735+3.*P1426753+P1427356+P1427365-3.*P1427536+3.*P1427563
    -3.*P1427635+P1427653+P1462357+P1462375+P1462537-P1472356-P1472365-P1472536)*0.5-P1523674
    +P1523764)/Nc);
    fvpart[189] += Nf*((P1234567+P1234576+P1234657+P1234675+P1234756+P1234765+P1235467+P1235476+P1243567
    +P1243576+P1243657+P1243675+P1243756+P1243765-P1245367-P1245376-P1253467-P1253476
    -P1254367-P1254376+P1256347+P1256437-P1256734-P1256743+P1257346+P1257436-P1257634
    -P1257643-P1263457-P1263475-P1263547-P1264357-P1264375+P1264537+P1265347+P1265437
    -P1265734-P1265743+P1267345+P1267354+P1267435-P1267453-P1267534-P1267543-P1273456
    -P1273465-P1273546-P1274356-P1274365+P1274536+P1275346+P1275436-P1275634-P1275643
    +P1276345+P1276354+P1276435-P1276453-P1276534-P1276543+P1324567+P1324576+P1324657
    +P1324756+P1325467+P1325476+P1342567+P1342576+P1342657+P1342756+3.*(P1362457+P1362475
    +P1362547)+P1372456+P1372465+P1372546+P1423567+P1423576+P1423657+P1423675+P1423756
    +P1423765-P1425367-P1425376+P1432567+P1432576+P1432657+2.*(P1324675+P1342675+P1352467
    +P1352476+P1352647+P1352674+P1352746+P1352764+P1432675)+P1432756+(P1235647+P1235674
    +3.*P1235746+3.*P1235764+3.*P1236457+3.*P1236475+3.*P1236547+3.*P1236574+5.*P1236745+5.*P1236754
    +P1237456+P1237465+P1237546+P1237564-P1237645-P1237654+3.*P1245637+P1245673+P1245736
    -5.*P1245763-3.*P1246357-3.*P1246375+P1246537-P1246573+P1246735-3.*P1246753-P1247356-P1247365
    +3.*P1247536-3.*P1247563+3.*P1247635-P1247653-3.*P1253647-3.*P1253674-P1253746-P1253764
    +3.*P1254637+P1254673+P1254736-5.*P1254763+P1256374-P1256473+3.*P1257364-3.*P1257463-3.*P1263574
    -P1263745-P1263754-P1264573+P1264735-3.*P1264753+P1265374-P1265473-P1273564-3.*P1273645
    -3.*P1273654-3.*P1274563+3.*P1274635-P1274653+3.*P1275364-3.*P1275463+P1325647-P1325674
    +3.*P1325746+5.*P1325764+3.*P1326457+5.*P1326475+3.*P1326547+P1326574+5.*P1326745+5.*P1326754
    +P1327456-P1327465+P1327546+3.*P1327564-P1327645-P1327654+5.*P1362574+5.*P1362745+5.*P1362754
    +3.*P1372564+3.*P1372645+3.*P1372654+3.*P1425637+P1425673+P1425736-5.*P1425763-3.*P1426357
    -3.*P1426375+P1426537-P1426573+P1426735-3.*P1426753-P1427356-P1427365+3.*P1427536-3.*P1427563
    +3.*P1427635-P1427653-P1462357-P1462375-P1462537+P1472356+P1472365+P1472536)*0.5+P1523674
    -P1523764)/Nc);
    fvpart[190] += Nf*((-P1235476+P1245376-P1267354+P1267453-P1324567-P1324675+P1324756-P1326475+P1326547
    -P1342675+P1342756-P1352674-P1352746-P1352764+P1356247+P1365247+P1423567-P1426537
    +(P1235674-P1235746-P1235764-P1236574-3.*P1236745-3.*P1236754-P1237456+P1237465+P1237546
    +P1237564+P1237645+3.*P1237654-3.*P1245673+P1245736+3.*P1245763-P1246573-P1246735+P1246753
    +P1247356-P1247365-P1247536+P1247563-P1247635-P1247653+P1253674+P1253746+P1253764
    -P1254673+P1254736+3.*P1254763+P1256374-P1256473+P1263574-P1263745-P1263754-P1264573
    -P1264735+P1264753-P1265374+P1265473+P1325674-3.*P1325746-3.*P1325764+P1326574-P1326745
    -3.*P1326754+P1327456+P1327465+P1327546+P1327564+P1327645+P1327654-P1362457-P1362475
    -P1362547-P1362574-P1362745-P1362754+P1372456+P1372465+P1372546+P1372564+P1372645
    +P1372654-P1425673+P1425736+3.*P1425763-P1426573-P1426735+P1426753+P1427356-P1427365
    -P1427536+P1427563-P1427635-P1427653)*0.5-P1432675+P1432756-P1472536-P1523674+P1523746
    +P1523764-P1623547-P1623745+P1624537)/Nc);
    fvpart[191] += Nf*((P1235674+P1237465+P1237546+P1237654-P1245673+P1245736-P1247365-P1247653+P1254736
    +P1256374-P1256473-P1263745-P1263754-P1264573-P1325746+P1326574+P1326745+P1327456
    +P1327546+P1327564+P1342576+P1342657+P1342756+P1352467+P1352476+P1352647+P1356247
    +P1362457+P1362475+P1362547+P1362574+P1362745+P1362754+P1365247+P1372456+P1372465
    +P1372546+P1372564+P1372645+P1372654+P1423567+P1423576+P1423765-P1425367+P1425637
    -P1426375+(P1234567+P1234576+P1234657+P1234675+P1234756+P1234765+P1235467-P1235476
    +P1235647+P1236457+3.*P1236475+P1236547+P1243567+P1243576+P1243657+P1243675+P1243756
    +P1243765-P1245367+P1245376+P1245637-P1246357-3.*P1246375+P1246537-P1253467-P1253476
    -P1253647-P1254367-P1254376+P1254637+P1256347+P1256437-P1256734-P1256743+P1257346
    +3.*P1257364+P1257436-3.*P1257463-P1257634-P1257643-P1263457-P1263475-P1263547-P1264357
    -P1264375+P1264537+P1265347+P1265437-P1265734-P1265743+P1267345-P1267354+P1267435
    +P1267453-P1267534-P1267543-P1273456-P1273465-P1273546-P1273564-P1273645-P1273654
    -P1274356-P1274365+P1274536-P1274563+P1274635-P1274653+P1275346+P1275364+P1275436
    -P1275463-P1275634-P1275643+P1276345+P1276354+P1276435-P1276453-P1276534-P1276543
    -P1324567+P1324576+3.*P1324657+P1324675+3.*P1324756-P1324765+P1325467+P1325476-P1325647
    +P1326457+P1326475+3.*P1326547-P1425673+3.*P1425736+P1425763-P1426573-P1426735-P1426753
    -P1427356-P1427365-P1427536-P1427563+P1427635-P1427653)*0.5+P1432576+P1432657+P1432756
    -P1472536+P1523647+P1523746-P1524637-P1623547-P1623745+P1624537)/Nc);
    fvpart[192] += Nf*((-P1235674-P1237465-P1237546-P1237654+P1245673-P1245736+P1247365+P1247653-P1254736
    -P1256374+P1256473+P1263745+P1263754+P1264573+P1325746-P1326574-P1326745-P1327456
    -P1327546-P1327564-P1342576-P1342657-P1342756-P1352467-P1352476-P1352647-P1356247
    -P1362457-P1362475-P1362547-P1362574-P1362745-P1362754-P1365247-P1372456-P1372465
    -P1372546-P1372564-P1372645-P1372654-P1423567-P1423576-P1423765+P1425367-P1425637
    +P1426375+(-P1234567-P1234576-P1234657-P1234675-P1234756-P1234765-P1235467+P1235476
    -P1235647-P1236457-3.*P1236475-P1236547-P1243567-P1243576-P1243657-P1243675-P1243756
    -P1243765+P1245367-P1245376-P1245637+P1246357+3.*P1246375-P1246537+P1253467+P1253476
    +P1253647+P1254367+P1254376-P1254637-P1256347-P1256437+P1256734+P1256743-P1257346
    -3.*P1257364-P1257436+3.*P1257463+P1257634+P1257643+P1263457+P1263475+P1263547+P1264357
    +P1264375-P1264537-P1265347-P1265437+P1265734+P1265743-P1267345+P1267354-P1267435
    -P1267453+P1267534+P1267543+P1273456+P1273465+P1273546+P1273564+P1273645+P1273654
    +P1274356+P1274365-P1274536+P1274563-P1274635+P1274653-P1275346-P1275364-P1275436
    +P1275463+P1275634+P1275643-P1276345-P1276354-P1276435+P1276453+P1276534+P1276543
    +P1324567-P1324576-3.*P1324657-P1324675-3.*P1324756+P1324765-P1325467-P1325476+P1325647
    -P1326457-P1326475-3.*P1326547+P1425673-3.*P1425736-P1425763+P1426573+P1426735+P1426753
    +P1427356+P1427365+P1427536+P1427563-P1427635+P1427653)*0.5-P1432576-P1432657-P1432756
    +P1472536-P1523647-P1523746+P1524637+P1623547+P1623745-P1624537)/Nc);
    fvpart[193] += Nf*((P1235476-P1245376+P1267354-P1267453+P1324567+P1324675-P1324756+P1326475-P1326547
    +P1342675-P1342756+P1352674+P1352746+P1352764-P1356247-P1365247-P1423567+P1426537
    +(-P1235674+P1235746+P1235764+P1236574+3.*P1236745+3.*P1236754+P1237456-P1237465-P1237546
    -P1237564-P1237645-3.*P1237654+3.*P1245673-P1245736-3.*P1245763+P1246573+P1246735-P1246753
    -P1247356+P1247365+P1247536-P1247563+P1247635+P1247653-P1253674-P1253746-P1253764
    +P1254673-P1254736-3.*P1254763-P1256374+P1256473-P1263574+P1263745+P1263754+P1264573
    +P1264735-P1264753+P1265374-P1265473-P1325674+3.*P1325746+3.*P1325764-P1326574+P1326745
    +3.*P1326754-P1327456-P1327465-P1327546-P1327564-P1327645-P1327654+P1362457+P1362475
    +P1362547+P1362574+P1362745+P1362754-P1372456-P1372465-P1372546-P1372564-P1372645
    -P1372654+P1425673-P1425736-3.*P1425763+P1426573+P1426735-P1426753-P1427356+P1427365
    +P1427536-P1427563+P1427635+P1427653)*0.5+P1432675-P1432756+P1472536+P1523674-P1523746
    -P1523764+P1623547+P1623745-P1624537)/Nc);
    fvpart[194] += Nf*((-P1234567-P1234576-P1234657-P1234675-P1234756-P1234765-P1235467-P1235476-P1235647
    -P1235674-P1235746-P1235764-P1236457-P1236547-P1236574-P1237456-P1237465-P1237546
    -P1243567-P1243576-P1243657-P1243675-P1243756-P1243765+P1245367+P1245376-P1245637
    -P1245736+P1246357-P1246537-P1246735+P1246753+P1247356+P1247365-P1247536+P1247563
    -P1247635+P1247653+P1253467+P1253476+P1253647+P1253674+P1253746+P1253764+P1254367
    +P1254376-P1254637-P1254736-P1256347-P1256374-P1256437+P1256473+P1256734+P1256743
    -P1257346-P1257436+P1257634+P1257643+P1263457+P1263475+P1263547+P1263574+P1263745
    +P1263754+P1264357+P1264375-P1264537+P1264573-P1264735+P1264753-P1265347-P1265374
    -P1265437+P1265473+P1265734+P1265743-P1267345-P1267354-P1267435+P1267453+P1267534
    +P1267543+P1273456+P1273465+P1273546+P1273564+P1273645+P1273654+P1274356+P1274365
    -P1274536+P1274563-P1274635+P1274653-P1275346-P1275364-P1275436+P1275463+P1275634
    +P1275643-P1276345-P1276354-P1276435+P1276453+P1276534+P1276543-P1324567-P1324576
    -P1324756-P1325467-P1325476-P1325746-P1326457-P1326547-P1326574-P1327456-P1327546
    -P1327564-P1342567-P1342576-P1342657-P1342756+(-5.*P1362457-5.*P1362475-5.*P1362547-5.*P1362574
    -5.*P1362745-5.*P1362754-3.*P1372456-3.*P1372465-3.*P1372546-3.*P1372564-3.*P1372645-3.*P1372654)*0.5
    -P1423567-P1423576-P1423675-P1423756-P1423765+P1425367+P1425376-P1425736+P1426357
    +P1426375-P1426537+P1426573-P1426735+P1426753+P1427356+P1427365-P1427536+P1427563
    -P1427635+P1427653-P1432567-P1432576-P1432657+2.*(-P1236475-P1236745-P1236754+P1245763
    +P1246375+P1254763-P1257364+P1257463-P1324657-P1324675-P1325764-P1326475-P1326745
    -P1326754-P1342675-P1352467-P1352476-P1352647-P1352674-P1352746-P1352764-P1425637
    +P1425763-P1432675)-P1432756-P1523647-P1523674+P1523764+P1524637)/Nc);
    fvpart[195] += Nf*((P1234567+P1234576+P1234657+P1234675+P1234756+P1234765+P1235467+P1235476+P1235647
    +P1235674+P1235746+P1235764+P1236457+P1236547+P1236574+P1237456+P1237465+P1237546
    +P1243567+P1243576+P1243657+P1243675+P1243756+P1243765-P1245367-P1245376+P1245637
    +P1245736-P1246357+P1246537+P1246735-P1246753-P1247356-P1247365+P1247536-P1247563
    +P1247635-P1247653-P1253467-P1253476-P1253647-P1253674-P1253746-P1253764-P1254367
    -P1254376+P1254637+P1254736+P1256347+P1256374+P1256437-P1256473-P1256734-P1256743
    +P1257346+P1257436-P1257634-P1257643-P1263457-P1263475-P1263547-P1263574-P1263745
    -P1263754-P1264357-P1264375+P1264537-P1264573+P1264735-P1264753+P1265347+P1265374
    +P1265437-P1265473-P1265734-P1265743+P1267345+P1267354+P1267435-P1267453-P1267534
    -P1267543-P1273456-P1273465-P1273546-P1273564-P1273645-P1273654-P1274356-P1274365
    +P1274536-P1274563+P1274635-P1274653+P1275346+P1275364+P1275436-P1275463-P1275634
    -P1275643+P1276345+P1276354+P1276435-P1276453-P1276534-P1276543+P1324567+P1324576
    +P1324756+P1325467+P1325476+P1325746+P1326457+P1326547+P1326574+P1327456+P1327546
    +P1327564+P1342567+P1342576+P1342657+P1342756+(5.*P1362457+5.*P1362475+5.*P1362547+5.*P1362574
    +5.*P1362745+5.*P1362754+3.*P1372456+3.*P1372465+3.*P1372546+3.*P1372564+3.*P1372645+3.*P1372654)*0.5
    +P1423567+P1423576+P1423675+P1423756+P1423765-P1425367-P1425376+P1425736-P1426357
    -P1426375+P1426537-P1426573+P1426735-P1426753-P1427356-P1427365+P1427536-P1427563
    +P1427635-P1427653+P1432567+P1432576+P1432657+2.*(P1236475+P1236745+P1236754-P1245763
    -P1246375-P1254763+P1257364-P1257463+P1324657+P1324675+P1325764+P1326475+P1326745
    +P1326754+P1342675+P1352467+P1352476+P1352647+P1352674+P1352746+P1352764+P1425637
    -P1425763+P1432675)+P1432756+P1523647+P1523674-P1523764-P1524637)/Nc);
    fvpart[196] += Nf*((-P1236475-P1237465-P1237546-P1237564-P1237645-P1243576-P1245736+P1246375+P1246573
    +P1247365+P1253476+P1254673-P1254736-P1256374+P1256473-P1257364+P1257463+P1263745
    +P1263754+P1264573-P1267435+P1267534-P1325674-P1326574-P1342567-P1342576-P1342765
    +P1346257+4.*(-P1352467-P1352476-P1352647-P1352674-P1352746-P1352764-P1362574-P1362745
    -P1362754)-P1375246-P1423567-P1423576-P1425637-P1425736+P1426357+P1426375+P1426573
    +P1427356+3.*(-P1234675-P1234756-P1234765-P1235476-P1243675+P1245376+P1256743-P1257436
    +P1257634+P1257643+P1263475+P1265743-P1267354+P1267453-P1326754-P1342675-P1423657
    -P1423675+P1425367+P1425376-P1426537-P1427536)-P1432567-P1432576+2.*(-P1234567-P1234576
    -P1234657-P1235467-P1235647-P1235746-P1235764-P1236457-P1236547-P1237456-P1243567
    -P1243657-P1243756-P1243765+P1245367-P1245637+P1246357-P1246537-P1246735+P1246753
    +P1247356-P1247536+P1253467+P1253647+P1253746+P1253764+P1254367+P1254376-P1254637
    -P1256347-P1256437+P1256734-P1257346+P1263457+P1263547+P1263574+P1264357+P1264375
    -P1264537-P1264735+P1264753-P1265347-P1265374-P1265437+P1265473+P1265734-P1267345
    +P1267543+P1273456+P1273465+P1273546+P1273564+P1273645+P1273654+P1274356+P1274365
    -P1274536+P1274563-P1274635+P1274653-P1275346-P1275364-P1275436+P1275463+P1275634
    +P1275643-P1276345-P1276354-P1276435+P1276453+P1276534+P1276543-P1326745-P1342657
    -P1342756-P1423756+P1425673-P1426735+P1426753+P1427365-P1432657-P1432675)-P1432756
    -P1462375+(-3.*P1235674-5.*P1236574-7.*P1236745-7.*P1236754+7.*P1245763+5.*P1247563-5.*P1247635
    +3.*P1247653+5.*P1253674+7.*P1254763-5.*P1324567-3.*P1324576-3.*P1324657-5.*P1324675-3.*P1324756
    -5.*P1324765-3.*P1325467-3.*P1325476-5.*P1325647-7.*P1325746-3.*P1325764-5.*P1326457-5.*P1326475
    -3.*P1326547-3.*P1327456-3.*P1327465-3.*P1327546-P1327564-P1327645-P1327654-9.*P1362457
    -9.*P1362475-9.*P1362547-7.*P1372456-7.*P1372465-7.*P1372546-5.*P1372564-5.*P1372645-5.*P1372654
    +7.*P1425763+3.*P1427563-3.*P1427635+5.*P1427653-P1472356-P1472365-P1472536)*0.5-P1523476
    +P1523647-P1523674+P1523746-P1523764-P1524367-P1524376-P1524637-P1524673+P1526347
    +P1526743+P1527346+P1572346-P1623547-P1623745+P1624357+P1624537-P1625347)/Nc);
    fvpart[197] += Nf*((-P1235647-P1235764+P1236475-P1237456-P1237645-P1246375-P1246537+P1246753+P1247356
    +P1253746+P1254673+P1257364-P1257463-P1264735-P1265374+P1265473+P1273564+P1274653
    -2.*P1325647-P1325746+P1326547-P1327465+P1327564-P1327645-P1327654-P1342567-P1342765
    +P1346257-P1352467-P1352476-P1352647-P1352674-P1352746-P1352764-P1372456-P1372465
    -P1372546-P1375246-P1423657+P1423765+P1425367+P1425376+P1427653-P1432567+P1432675
    +P1432756+(-P1234567-P1234576-P1234657-P1234675-P1234756-P1234765-P1235467-3.*P1235476
    -P1235674-P1236574+P1236745+P1236754-P1243567+P1243576-P1243657-P1243675+P1243756
    +P1243765+P1245367+3.*P1245376-P1245763+P1247563-P1247635+P1247653+P1253467-P1253476
    +P1253674+P1254367+P1254376-P1254763-P1256347-P1256437-P1256734+P1256743+P1257346
    -P1257436+P1257634+P1257643+P1263457+P1263475+P1263547+P1264357-P1264375-P1264537
    -P1265347-P1265437-P1265734+P1265743-P1267345-3.*P1267354+P1267435+3.*P1267453-P1267534
    +P1267543+P1273456+P1273465+P1273546+P1274356+P1274365-P1274536-P1275346-P1275436
    +P1275634+P1275643-P1276345-P1276354-P1276435+P1276453+P1276534+P1276543-3.*P1324567
    -P1324576+P1324657+P1324675+P1324756-3.*P1324765-P1325467-P1325476-3.*P1325674+P1326574
    +3.*P1326745+P1326754-P1372564-P1372645-P1372654+P1425637+P1425673+P1425736-P1426357
    -P1426375-3.*P1426537-P1426573-P1426735+P1426753+P1427356+P1427365-P1427536+P1462357
    -P1462375+P1462537)*0.5+P1523647+P1523746-P1523764-P1524367-P1524637+P1526347-P1526734
    +P1527346+P1572346-P1623547-P1623745+P1624357+P1624537-P1625347)/Nc);
    fvpart[198] += Nf*((P1235647+P1235764-P1236475+P1237456+P1237645+P1246375+P1246537-P1246753-P1247356
    -P1253746-P1254673-P1257364+P1257463+P1264735+P1265374-P1265473-P1273564-P1274653
    +2.*P1325647+P1325746-P1326547+P1327465-P1327564+P1327645+P1327654+P1342567+P1342765
    -P1346257+P1352467+P1352476+P1352647+P1352674+P1352746+P1352764+P1372456+P1372465
    +P1372546+P1375246+P1423657-P1423765-P1425367-P1425376-P1427653+P1432567-P1432675
    -P1432756+(P1234567+P1234576+P1234657+P1234675+P1234756+P1234765+P1235467+3.*P1235476
    +P1235674+P1236574-P1236745-P1236754+P1243567-P1243576+P1243657+P1243675-P1243756
    -P1243765-P1245367-3.*P1245376+P1245763-P1247563+P1247635-P1247653-P1253467+P1253476
    -P1253674-P1254367-P1254376+P1254763+P1256347+P1256437+P1256734-P1256743-P1257346
    +P1257436-P1257634-P1257643-P1263457-P1263475-P1263547-P1264357+P1264375+P1264537
    +P1265347+P1265437+P1265734-P1265743+P1267345+3.*P1267354-P1267435-3.*P1267453+P1267534
    -P1267543-P1273456-P1273465-P1273546-P1274356-P1274365+P1274536+P1275346+P1275436
    -P1275634-P1275643+P1276345+P1276354+P1276435-P1276453-P1276534-P1276543+3.*P1324567
    +P1324576-P1324657-P1324675-P1324756+3.*P1324765+P1325467+P1325476+3.*P1325674-P1326574
    -3.*P1326745-P1326754+P1372564+P1372645+P1372654-P1425637-P1425673-P1425736+P1426357
    +P1426375+3.*P1426537+P1426573+P1426735-P1426753-P1427356-P1427365+P1427536-P1462357
    +P1462375-P1462537)*0.5-P1523647-P1523746+P1523764+P1524367+P1524637-P1526347+P1526734
    -P1527346-P1572346+P1623547+P1623745-P1624357-P1624537+P1625347)/Nc);
    fvpart[199] += Nf*((P1236475+P1237465+P1237546+P1237564+P1237645+P1243576+P1245736-P1246375-P1246573
    -P1247365-P1253476-P1254673+P1254736+P1256374-P1256473+P1257364-P1257463-P1263745
    -P1263754-P1264573+P1267435-P1267534+P1325674+P1326574+P1342567+P1342576+P1342765
    -P1346257+4.*(P1352467+P1352476+P1352647+P1352674+P1352746+P1352764+P1362574+P1362745
    +P1362754)+P1375246+P1423567+P1423576+P1425637+P1425736-P1426357-P1426375-P1426573
    -P1427356+3.*(P1234675+P1234756+P1234765+P1235476+P1243675-P1245376-P1256743+P1257436
    -P1257634-P1257643-P1263475-P1265743+P1267354-P1267453+P1326754+P1342675+P1423657
    +P1423675-P1425367-P1425376+P1426537+P1427536)+P1432567+P1432576+2.*(P1234567+P1234576
    +P1234657+P1235467+P1235647+P1235746+P1235764+P1236457+P1236547+P1237456+P1243567
    +P1243657+P1243756+P1243765-P1245367+P1245637-P1246357+P1246537+P1246735-P1246753
    -P1247356+P1247536-P1253467-P1253647-P1253746-P1253764-P1254367-P1254376+P1254637
    +P1256347+P1256437-P1256734+P1257346-P1263457-P1263547-P1263574-P1264357-P1264375
    +P1264537+P1264735-P1264753+P1265347+P1265374+P1265437-P1265473-P1265734+P1267345
    -P1267543-P1273456-P1273465-P1273546-P1273564-P1273645-P1273654-P1274356-P1274365
    +P1274536-P1274563+P1274635-P1274653+P1275346+P1275364+P1275436-P1275463-P1275634
    -P1275643+P1276345+P1276354+P1276435-P1276453-P1276534-P1276543+P1326745+P1342657
    +P1342756+P1423756-P1425673+P1426735-P1426753-P1427365+P1432657+P1432675)+P1432756
    +P1462375+(3.*P1235674+5.*P1236574+7.*P1236745+7.*P1236754-7.*P1245763-5.*P1247563+5.*P1247635
    -3.*P1247653-5.*P1253674-7.*P1254763+5.*P1324567+3.*P1324576+3.*P1324657+5.*P1324675+3.*P1324756
    +5.*P1324765+3.*P1325467+3.*P1325476+5.*P1325647+7.*P1325746+3.*P1325764+5.*P1326457+5.*P1326475
    +3.*P1326547+3.*P1327456+3.*P1327465+3.*P1327546+P1327564+P1327645+P1327654+9.*P1362457
    +9.*P1362475+9.*P1362547+7.*P1372456+7.*P1372465+7.*P1372546+5.*P1372564+5.*P1372645+5.*P1372654
    -7.*P1425763-3.*P1427563+3.*P1427635-5.*P1427653+P1472356+P1472365+P1472536)*0.5+P1523476
    -P1523647+P1523674-P1523746+P1523764+P1524367+P1524376+P1524637+P1524673-P1526347
    -P1526743-P1527346-P1572346+P1623547+P1623745-P1624357-P1624537+P1625347)/Nc);
    fvpart[200] += Nf*((-P1235647+P1235746+P1236457-P1236475+P1236547-P1237456+P1237465-P1237546+P1245637
    -P1245736-P1246357+P1246375-P1246537+P1247356-P1247365+P1247536-P1253647+P1253746
    +P1254637-P1254736+P1256374-P1256473-P1257364+P1257463-P1263574+P1263745+P1263754
    +P1264573-P1264735-P1264753-P1265374+P1265473+P1273564-P1273645-P1273654-P1274563
    +P1274635+P1274653+P1275364-P1275463-P1325647+P1325746+P1326457-P1326475+P1326547
    -P1327456+P1327465-P1327546+P1425637-P1425736-P1426357+P1426375-P1426537+P1427356
    -P1427365+P1427536)/(2.*Nc));
    fvpart[201] += Nf*((P1235647-P1235746-P1236457+P1236475-P1236547+P1237456-P1237465+P1237546-P1245637
    +P1245736+P1246357-P1246375+P1246537-P1247356+P1247365-P1247536+P1253647-P1253746
    -P1254637+P1254736-P1256374+P1256473+P1257364-P1257463+P1263574-P1263745-P1263754
    -P1264573+P1264735+P1264753+P1265374-P1265473-P1273564+P1273645+P1273654+P1274563
    -P1274635-P1274653-P1275364+P1275463+P1325647-P1325746-P1326457+P1326475-P1326547
    +P1327456-P1327465+P1327546-P1425637+P1425736+P1426357-P1426375+P1426537-P1427356
    +P1427365-P1427536)/(2.*Nc));
    fvpart[202] += Nf*((P1236547+P1236574+P1236754+P1237546+P1245637-P1245763+P1246735-P1247563-P1253764
    -P1264573-P1273654-P1274563+P1326547+P1326574+P1326754+P1327546+P1362457+P1362475
    +P1362547+P1362574+P1362745+P1362754+P1372456+P1372465+P1372546+P1423657+P1423675
    +P1423756-P1425673-P1426573-P1427365+P1432657+P1432675+P1432756+(P1234567-P1234576
    +P1234657+P1234675+P1234756+P1234765-P1235764+P1236457+P1236475+P1236745+P1237456
    +P1237465+P1237564-P1237654+P1243567+P1243576+P1243657+P1243675+3.*P1243756+3.*P1243765
    +P1245673-P1246357-P1246375-P1246573+P1246753-P1247356-P1247365+P1247635-P1253467
    -P1253476-P1253647-P1253674-P1253746-P1254367+P1254376+P1254637+P1254736-P1254763
    +P1256347+P1256374+P1256437-P1256473-3.*P1256734-P1256743+P1257346+P1257364+P1257436
    -P1257463-P1257634-P1257643-P1263457-P1263475-P1263745-P1264357-P1264375+P1264735
    +P1265347+P1265374+P1265437-P1265473-3.*P1265734-P1265743-P1267345+P1267435-P1267534
    +P1267543-P1273456-P1273465-P1273645-P1274356-P1274365+P1274635+P1275346+P1275364
    +P1275436-P1275463-P1275634-P1275643+P1276345+P1276435-P1276534-P1276543+P1324567
    -P1324576+P1324657+P1324675+P1324756+P1324765-P1325764+P1326457+P1326475+P1326745
    +P1327456+P1327465+P1327564-P1327654-P1342567-P1342576+P1342657+P1342675+P1342756
    -P1342765+P1352467+P1352476+P1352647+P1352674+P1352746+P1352764+P1372564+P1372645
    +P1372654-P1425367-P1425376+P1425637+P1425736-P1425763+P1426537+P1426735+P1427536
    -P1427563-P1427653+P1462357+P1462375+P1462537)*0.5+P1524376+P1524673-P1526734)/Nc);
    fvpart[203] += Nf*((-P1234567-P1234576-P1234657-P1234675-P1235467-P1235476-P1235647-P1235674-P1236457
    -P1236475-P1236547-P1236574-P1243567-P1243657-P1243675-P1243756-P1243765+P1245367
    +P1245376-P1245637+P1246357+P1246375-P1246537+P1247563-P1247635+P1247653+P1253467
    +P1253647+P1253674+P1254367+P1254376-P1254637-P1256347-P1256437+P1256734-P1257346
    -P1257364-P1257436+P1257463+P1257634+P1257643+P1263457+P1263475+P1263547+P1264357
    +P1264375-P1264537-P1265347-P1265437+P1265734-P1267345-P1267354+P1267453+P1267543
    +P1273456+P1273465+P1273546+P1273564+P1273645+P1273654+P1274356+P1274365-P1274536
    +P1274563-P1274635+P1274653-P1275346-P1275364-P1275436+P1275463+P1275634+P1275643
    -P1276345-P1276354-P1276435+P1276453+P1276534+P1276543-P1325746-P1325764-P1327456
    -P1327546-P1327564-P1423567-P1423657-P1423675-P1423756-P1423765+P1425367+P1425376
    -P1425637+2.*(-P1234756-P1234765-P1236745-P1236754+P1245763+P1254763+P1256743+P1265743
    -P1362574-P1362745-P1362754+P1425763)+P1426357+P1426375-P1426537+P1427563-P1427635
    +P1427653-P1432657-P1432675-P1432756+(-P1235746-P1235764-P1237456-P1237465-P1237546
    -P1237564-P1237645-P1237654+P1245673-P1245736+P1246573-P1246735+P1246753+P1247356
    +P1247365-3.*P1247536+P1253746+P1253764+P1254673-P1254736-P1256374+P1256473+3.*P1263574
    +P1263745+P1263754+P1264573-P1264735+P1264753-P1265374+P1265473-P1324567-P1324576
    -3.*P1324657-3.*P1324675-3.*P1324756-P1324765-P1325467-P1325476-P1325647+P1325674-3.*P1326457
    -3.*P1326475-3.*P1326547-P1326574-3.*P1326745-3.*P1326754-P1342567-P1342576-3.*P1342657
    -3.*P1342675-3.*P1342756-P1342765-3.*P1352467-3.*P1352476-3.*P1352647-3.*P1352674-3.*P1352746
    -3.*P1352764-5.*P1362457-5.*P1362475-5.*P1362547-3.*P1372456-3.*P1372465-3.*P1372546-3.*P1372564
    -3.*P1372645-3.*P1372654+P1425673-P1425736+P1426573-P1426735+P1426753+P1427356+P1427365
    -3.*P1427536-P1472356-P1472365-P1472536)*0.5-P1523476-P1523674+P1526743)/Nc);
    fvpart[204] += Nf*((P1234567+P1234576+P1234657+P1234675+P1235467+P1235476+P1235647+P1235674+P1236457
    +P1236475+P1236547+P1236574+P1243567+P1243657+P1243675+P1243756+P1243765-P1245367
    -P1245376+P1245637-P1246357-P1246375+P1246537-P1247563+P1247635-P1247653-P1253467
    -P1253647-P1253674-P1254367-P1254376+P1254637+P1256347+P1256437-P1256734+P1257346
    +P1257364+P1257436-P1257463-P1257634-P1257643-P1263457-P1263475-P1263547-P1264357
    -P1264375+P1264537+P1265347+P1265437-P1265734+P1267345+P1267354-P1267453-P1267543
    -P1273456-P1273465-P1273546-P1273564-P1273645-P1273654-P1274356-P1274365+P1274536
    -P1274563+P1274635-P1274653+P1275346+P1275364+P1275436-P1275463-P1275634-P1275643
    +P1276345+P1276354+P1276435-P1276453-P1276534-P1276543+P1325746+P1325764+P1327456
    +P1327546+P1327564+P1423567+P1423657+P1423675+P1423756+P1423765-P1425367-P1425376
    +P1425637+2.*(P1234756+P1234765+P1236745+P1236754-P1245763-P1254763-P1256743-P1265743
    +P1362574+P1362745+P1362754-P1425763)-P1426357-P1426375+P1426537-P1427563+P1427635
    -P1427653+P1432657+P1432675+P1432756+(P1235746+P1235764+P1237456+P1237465+P1237546
    +P1237564+P1237645+P1237654-P1245673+P1245736-P1246573+P1246735-P1246753-P1247356
    -P1247365+3.*P1247536-P1253746-P1253764-P1254673+P1254736+P1256374-P1256473-3.*P1263574
    -P1263745-P1263754-P1264573+P1264735-P1264753+P1265374-P1265473+P1324567+P1324576
    +3.*P1324657+3.*P1324675+3.*P1324756+P1324765+P1325467+P1325476+P1325647-P1325674+3.*P1326457
    +3.*P1326475+3.*P1326547+P1326574+3.*P1326745+3.*P1326754+P1342567+P1342576+3.*P1342657
    +3.*P1342675+3.*P1342756+P1342765+3.*P1352467+3.*P1352476+3.*P1352647+3.*P1352674+3.*P1352746
    +3.*P1352764+5.*P1362457+5.*P1362475+5.*P1362547+3.*P1372456+3.*P1372465+3.*P1372546+3.*P1372564
    +3.*P1372645+3.*P1372654-P1425673+P1425736-P1426573+P1426735-P1426753-P1427356-P1427365
    +3.*P1427536+P1472356+P1472365+P1472536)*0.5+P1523476+P1523674-P1526743)/Nc);
    fvpart[205] += Nf*((-P1236547-P1236574-P1236754-P1237546-P1245637+P1245763-P1246735+P1247563+P1253764
    +P1264573+P1273654+P1274563-P1326547-P1326574-P1326754-P1327546-P1362457-P1362475
    -P1362547-P1362574-P1362745-P1362754-P1372456-P1372465-P1372546-P1423657-P1423675
    -P1423756+P1425673+P1426573+P1427365-P1432657-P1432675-P1432756+(-P1234567+P1234576
    -P1234657-P1234675-P1234756-P1234765+P1235764-P1236457-P1236475-P1236745-P1237456
    -P1237465-P1237564+P1237654-P1243567-P1243576-P1243657-P1243675-3.*P1243756-3.*P1243765
    -P1245673+P1246357+P1246375+P1246573-P1246753+P1247356+P1247365-P1247635+P1253467
    +P1253476+P1253647+P1253674+P1253746+P1254367-P1254376-P1254637-P1254736+P1254763
    -P1256347-P1256374-P1256437+P1256473+3.*P1256734+P1256743-P1257346-P1257364-P1257436
    +P1257463+P1257634+P1257643+P1263457+P1263475+P1263745+P1264357+P1264375-P1264735
    -P1265347-P1265374-P1265437+P1265473+3.*P1265734+P1265743+P1267345-P1267435+P1267534
    -P1267543+P1273456+P1273465+P1273645+P1274356+P1274365-P1274635-P1275346-P1275364
    -P1275436+P1275463+P1275634+P1275643-P1276345-P1276435+P1276534+P1276543-P1324567
    +P1324576-P1324657-P1324675-P1324756-P1324765+P1325764-P1326457-P1326475-P1326745
    -P1327456-P1327465-P1327564+P1327654+P1342567+P1342576-P1342657-P1342675-P1342756
    +P1342765-P1352467-P1352476-P1352647-P1352674-P1352746-P1352764-P1372564-P1372645
    -P1372654+P1425367+P1425376-P1425637-P1425736+P1425763-P1426537-P1426735-P1427536
    +P1427563+P1427653-P1462357-P1462375-P1462537)*0.5-P1524376-P1524673+P1526734)/Nc);
    fvpart[206] += Nf*((P1235467-P1235476+P1235647-P1236547+P1236745+P1237456-P1237465-P1237645-P1245367
    +P1245376-P1245637+P1246537-P1246735-P1247356+P1247365+P1247635-P1253674+P1253746
    +P1253764+P1254673-P1254736-P1254763-P1256374+P1256473-P1263547+P1263745+P1264537
    -P1264735+P1265374-P1265473-P1267354+P1267453-P1273546-P1273564+P1273654+P1274536
    +P1274563-P1274653+P1276354-P1276453+P1325467-P1325476+P1325647-P1326547+P1326745
    +P1327456-P1327465-P1327645-P1425367+P1425376-P1425637+P1426537-P1426735-P1427356
    +P1427365+P1427635)/(2.*Nc));
    fvpart[207] += Nf*((-P1235467+P1235476-P1235647+P1236547-P1236745-P1237456+P1237465+P1237645+P1245367
    -P1245376+P1245637-P1246537+P1246735+P1247356-P1247365-P1247635+P1253674-P1253746
    -P1253764-P1254673+P1254736+P1254763+P1256374-P1256473+P1263547-P1263745-P1264537
    +P1264735-P1265374+P1265473+P1267354-P1267453+P1273546+P1273564-P1273654-P1274536
    -P1274563+P1274653-P1276354+P1276453-P1325467+P1325476-P1325647+P1326547-P1326745
    -P1327456+P1327465+P1327645+P1425367-P1425376+P1425637-P1426537+P1426735+P1427356
    -P1427365-P1427635)/(2.*Nc));
    fvpart[208] += Nf*((P1235467+P1235476+P1235647+P1235674+P1235746+P1236547+P1236574+P1237546-P1245367
    -P1245376+P1245736+P1246537+P1246735+P1247536-P1247563-P1247653-P1253764-P1263547
    -P1263574-P1263754+P1264537-P1264573-P1264753+P1267354-P1267453-P1273546-P1273564
    +P1274536-P1274563-P1274653+P1276354-P1276453+P1325674+P1325746+P1326574-P1326754
    +P1327645+P1423675+P1423756-3.*P1425673-P1426573-P1426753-P1427365-P1432567-P1432576
    +P1432657-P1432675+P1432756+(P1234567+3.*P1234576+3.*P1234657+3.*P1234675+3.*P1234756
    +3.*P1234765+P1235764+P1236457-P1236475+P1236745+P1237456+P1237465+3.*P1237564+5.*P1237654
    +P1243567+P1243576+3.*P1243657+3.*P1243675+P1243756+P1243765-5.*P1245673-P1246357+P1246375
    -3.*P1246573-P1246753-P1247356-P1247365+P1247635-P1253467-P1253476-P1253647-P1253674
    -P1253746-P1254367-3.*P1254376+P1254637+P1254736-P1254763+P1256347+P1256374+3.*P1256437
    -P1256473-P1256734-3.*P1256743+P1257346-P1257364+3.*P1257436+P1257463-3.*P1257634-3.*P1257643
    -3.*P1263457-3.*P1263475-P1263745-P1264357-P1264375+P1264735+P1265347+P1265374+3.*P1265437
    -P1265473-P1265734-3.*P1265743+3.*P1267345+P1267435-P1267534-3.*P1267543-3.*P1273456-3.*P1273465
    -P1273645-P1274356-P1274365+P1274635+P1275346+P1275364+3.*P1275436-P1275463-3.*P1275634
    -3.*P1275643+P1276345+P1276435-P1276534-P1276543-P1324567+P1324576+P1324657-P1324675
    +3.*P1324756+3.*P1324765-3.*P1325764+3.*P1326457-P1326475-P1326745+3.*P1327456+3.*P1327465
    +P1327564+3.*P1327654-P1342567-P1342576+3.*P1342657-P1342675+3.*P1342756+P1342765+3.*P1352467
    +3.*P1352476+3.*P1352647+3.*P1352674+3.*P1352746+3.*P1352764+3.*P1362457+3.*P1362475+3.*P1362547
    +3.*P1362574+3.*P1362745+3.*P1362754+5.*P1372456+5.*P1372465+5.*P1372546-3.*P1425367-3.*P1425376
    -P1425637+P1425736-P1425763+3.*P1426537+P1426735+3.*P1427536-P1427563-3.*P1427653+P1462357
    +P1462375+P1462537)*0.5+P1523467+P1523476-P1523647+2.*(P1237645-P1254673+P1325647+P1326547
    +P1327546+P1372564+P1372645+P1372654+P1423657+P1523764)+P1524367+P1524637+P1524673
    -P1526347+P1526734-P1527346)/Nc);
    fvpart[209] += Nf*((P1237456+P1237465+P1237546+P1243576+P1245736-P1247356-P1247365-P1253476-P1253746
    +P1254736+P1256374-P1256473-P1263745-P1263754-P1264357-P1264573+P1264735+P1265347
    +P1265374-P1265473+P1267435-P1267534-P1274356+P1275346-P1325764+P1326475+P1327564
    +P1327645+P1327654+4.*(P1234765-P1256743+P1362457+P1362475+P1362547+P1372456+P1372465
    +P1372546)+P1423567+P1423765+P1425736-P1427563+P1427635+3.*(P1234675+P1234756+P1236745
    +P1236754+P1243675+P1243765-P1245763-P1254763+P1256437-P1256734+P1257436-P1257634
    -P1257643-P1263475-P1265743-P1273465+P1325647+P1326457+P1326547+P1372564+P1372645
    +P1372654+P1423657+P1423675-P1425367-P1425376-P1425763+P1427536-P1427653)-P1432567
    -P1432576+P1432675+P1462357+P1462375+P1462537+(5.*P1235647+P1235764+3.*P1236457+P1236475
    +3.*P1236547+3.*P1237564+5.*P1237645+5.*P1237654+3.*P1245637-5.*P1245673-3.*P1246357-P1246375
    +3.*P1246537-3.*P1246573+5.*P1246735-P1246753-3.*P1253647-5.*P1253764+3.*P1254637-5.*P1254673
    +P1257364-P1257463-3.*P1273564-3.*P1273645-3.*P1273654-3.*P1274563+3.*P1274635-5.*P1274653
    +3.*P1275364-3.*P1275463+P1324567+P1324576+3.*P1324657+3.*P1324675+5.*P1324756+5.*P1324765
    +P1325467+P1325476+P1325674+5.*P1325746+3.*P1326574+3.*P1326745+3.*P1326754+5.*P1327456
    +3.*P1327465+5.*P1327546-P1342567-P1342576+5.*P1342657+3.*P1342675+5.*P1342756+P1342765
    +7.*P1352467+7.*P1352476+7.*P1352647+7.*P1352674+7.*P1352746+7.*P1352764+7.*P1362574+7.*P1362745
    +7.*P1362754+P1425637-9.*P1425673-P1426357-P1426375+5.*P1426537-3.*P1426573+3.*P1426735
    -3.*P1426753+P1472356+P1472365+P1472536)*0.5+P1523467-P1523647+P1523674+P1524367+P1524376
    +P1524637+2.*(P1234567+P1234576+P1234657+P1235467+P1235476+P1235674+P1235746+P1236574
    +P1243567+P1243657+P1243756-P1245367-P1245376+P1247536-P1247563+P1247635-P1247653
    -P1253467-P1253674-P1254367-P1254376+P1256347+P1257346-P1263457-P1263547-P1263574
    -P1264375+P1264537-P1264753+P1265437-P1265734+P1267345+P1267354-P1267453-P1267543
    -P1273456-P1273546-P1274365+P1274536+P1275436-P1275634-P1275643+P1276345+P1276354
    +P1276435-P1276453-P1276534-P1276543+P1423756-P1427365+P1432657+P1432756+P1523476
    +P1523764+P1524673)-P1526347-P1526743-P1527346)/Nc);
    fvpart[210] += Nf*((-P1237456-P1237465-P1237546-P1243576-P1245736+P1247356+P1247365+P1253476+P1253746
    -P1254736-P1256374+P1256473+P1263745+P1263754+P1264357+P1264573-P1264735-P1265347
    -P1265374+P1265473-P1267435+P1267534+P1274356-P1275346+P1325764-P1326475-P1327564
    -P1327645-P1327654+4.*(-P1234765+P1256743-P1362457-P1362475-P1362547-P1372456-P1372465
    -P1372546)-P1423567-P1423765-P1425736+P1427563-P1427635+3.*(-P1234675-P1234756-P1236745
    -P1236754-P1243675-P1243765+P1245763+P1254763-P1256437+P1256734-P1257436+P1257634
    +P1257643+P1263475+P1265743+P1273465-P1325647-P1326457-P1326547-P1372564-P1372645
    -P1372654-P1423657-P1423675+P1425367+P1425376+P1425763-P1427536+P1427653)+P1432567
    +P1432576-P1432675-P1462357-P1462375-P1462537+(-5.*P1235647-P1235764-3.*P1236457-P1236475
    -3.*P1236547-3.*P1237564-5.*P1237645-5.*P1237654-3.*P1245637+5.*P1245673+3.*P1246357+P1246375
    -3.*P1246537+3.*P1246573-5.*P1246735+P1246753+3.*P1253647+5.*P1253764-3.*P1254637+5.*P1254673
    -P1257364+P1257463+3.*P1273564+3.*P1273645+3.*P1273654+3.*P1274563-3.*P1274635+5.*P1274653
    -3.*P1275364+3.*P1275463-P1324567-P1324576-3.*P1324657-3.*P1324675-5.*P1324756-5.*P1324765
    -P1325467-P1325476-P1325674-5.*P1325746-3.*P1326574-3.*P1326745-3.*P1326754-5.*P1327456
    -3.*P1327465-5.*P1327546+P1342567+P1342576-5.*P1342657-3.*P1342675-5.*P1342756-P1342765
    -7.*P1352467-7.*P1352476-7.*P1352647-7.*P1352674-7.*P1352746-7.*P1352764-7.*P1362574-7.*P1362745
    -7.*P1362754-P1425637+9.*P1425673+P1426357+P1426375-5.*P1426537+3.*P1426573-3.*P1426735
    +3.*P1426753-P1472356-P1472365-P1472536)*0.5-P1523467+P1523647-P1523674-P1524367-P1524376
    -P1524637+2.*(-P1234567-P1234576-P1234657-P1235467-P1235476-P1235674-P1235746-P1236574
    -P1243567-P1243657-P1243756+P1245367+P1245376-P1247536+P1247563-P1247635+P1247653
    +P1253467+P1253674+P1254367+P1254376-P1256347-P1257346+P1263457+P1263547+P1263574
    +P1264375-P1264537+P1264753-P1265437+P1265734-P1267345-P1267354+P1267453+P1267543
    +P1273456+P1273546+P1274365-P1274536-P1275436+P1275634+P1275643-P1276345-P1276354
    -P1276435+P1276453+P1276534+P1276543-P1423756+P1427365-P1432657-P1432756-P1523476
    -P1523764-P1524673)+P1526347+P1526743+P1527346)/Nc);
    fvpart[211] += Nf*((-P1235467-P1235476-P1235647-P1235674-P1235746-P1236547-P1236574-P1237546+P1245367
    +P1245376-P1245736-P1246537-P1246735-P1247536+P1247563+P1247653+P1253764+P1263547
    +P1263574+P1263754-P1264537+P1264573+P1264753-P1267354+P1267453+P1273546+P1273564
    -P1274536+P1274563+P1274653-P1276354+P1276453-P1325674-P1325746-P1326574+P1326754
    -P1327645-P1423675-P1423756+3.*P1425673+P1426573+P1426753+P1427365+P1432567+P1432576
    -P1432657+P1432675-P1432756+(-P1234567-3.*P1234576-3.*P1234657-3.*P1234675-3.*P1234756
    -3.*P1234765-P1235764-P1236457+P1236475-P1236745-P1237456-P1237465-3.*P1237564-5.*P1237654
    -P1243567-P1243576-3.*P1243657-3.*P1243675-P1243756-P1243765+5.*P1245673+P1246357-P1246375
    +3.*P1246573+P1246753+P1247356+P1247365-P1247635+P1253467+P1253476+P1253647+P1253674
    +P1253746+P1254367+3.*P1254376-P1254637-P1254736+P1254763-P1256347-P1256374-3.*P1256437
    +P1256473+P1256734+3.*P1256743-P1257346+P1257364-3.*P1257436-P1257463+3.*P1257634+3.*P1257643
    +3.*P1263457+3.*P1263475+P1263745+P1264357+P1264375-P1264735-P1265347-P1265374-3.*P1265437
    +P1265473+P1265734+3.*P1265743-3.*P1267345-P1267435+P1267534+3.*P1267543+3.*P1273456+3.*P1273465
    +P1273645+P1274356+P1274365-P1274635-P1275346-P1275364-3.*P1275436+P1275463+3.*P1275634
    +3.*P1275643-P1276345-P1276435+P1276534+P1276543+P1324567-P1324576-P1324657+P1324675
    -3.*P1324756-3.*P1324765+3.*P1325764-3.*P1326457+P1326475+P1326745-3.*P1327456-3.*P1327465
    -P1327564-3.*P1327654+P1342567+P1342576-3.*P1342657+P1342675-3.*P1342756-P1342765-3.*P1352467
    -3.*P1352476-3.*P1352647-3.*P1352674-3.*P1352746-3.*P1352764-3.*P1362457-3.*P1362475-3.*P1362547
    -3.*P1362574-3.*P1362745-3.*P1362754-5.*P1372456-5.*P1372465-5.*P1372546+3.*P1425367+3.*P1425376
    +P1425637-P1425736+P1425763-3.*P1426537-P1426735-3.*P1427536+P1427563+3.*P1427653-P1462357
    -P1462375-P1462537)*0.5-P1523467-P1523476+P1523647+2.*(-P1237645+P1254673-P1325647-P1326547
    -P1327546-P1372564-P1372645-P1372654-P1423657-P1523764)-P1524367-P1524637-P1524673
    +P1526347-P1526734+P1527346)/Nc);
    fvpart[212] += Nf*((-P1235467+P1235476+P1235746+P1236457-P1236475-P1236745-P1237546+P1237645+P1245367
    -P1245376-P1245736-P1246357+P1246375+P1246735+P1247536-P1247635+P1253647+P1253674
    -P1253764-P1254637-P1254673+P1254763-P1257364+P1257463-P1263547-P1263574+P1263754
    +P1264537+P1264573-P1264753+P1267354-P1267453-P1273546+P1273645+P1274536-P1274635
    +P1275364-P1275463-P1276354+P1276453-P1325467+P1325476+P1325746+P1326457-P1326475
    -P1326745-P1327546+P1327645+P1425367-P1425376-P1425736-P1426357+P1426375+P1426735
    +P1427536-P1427635)/(2.*Nc));
    fvpart[213] += Nf*((P1235467-P1235476-P1235746-P1236457+P1236475+P1236745+P1237546-P1237645-P1245367
    +P1245376+P1245736+P1246357-P1246375-P1246735-P1247536+P1247635-P1253647-P1253674
    +P1253764+P1254637+P1254673-P1254763+P1257364-P1257463+P1263547+P1263574-P1263754
    -P1264537-P1264573+P1264753-P1267354+P1267453+P1273546-P1273645-P1274536+P1274635
    -P1275364+P1275463+P1276354-P1276453+P1325467-P1325476-P1325746-P1326457+P1326475
    +P1326745+P1327546-P1327645-P1425367+P1425376+P1425736+P1426357-P1426375-P1426735
    -P1427536+P1427635)/(2.*Nc));
#else
    std::cout << "Warning: too large norderF, recompile " __FILE__ " with --enable-debug" << std::endl;
#endif
    }}
  }
}

// --------- END --- automatically generated code --- END --------- //

#define INSTANTIATE(T) \
  template void Amp2q5g<T>::getfvpart1_lc(const int fv, LoopValue* fvpart); \
  template void Amp2q5g<T>::getfvpart1_lc(const int fv, LoopResult<T>* fvpart);

#ifdef USE_SD
  INSTANTIATE(double)
#endif
#ifdef USE_DD
  INSTANTIATE(dd_real)
#endif
#ifdef USE_QD
  INSTANTIATE(qd_real)
#endif
#ifdef USE_VC
  INSTANTIATE(Vc::double_v)
#endif

#undef INSTANTIATE
