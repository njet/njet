/*
* chsums/2q0gV.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef CHSUM_2Q0GV_H
#define CHSUM_2Q0GV_H

#include "NJetAmp.h"

class Amp2q0gVStatic : public AmpNStatic<2>
{
  public:
    static const int FC = 1;   // flavour permutations
    static const int C0 = 1;   // partial amplitudes
    static const int CC = 1;  // partial amplitudes

    static const int flav[FC][NN];  // flavour vectors for ngluon
    static const int fvsign[FC];    // flavour channels' signs
    static const int ccsign[NN*(NN+1)/2];  // ccborn signs
    static const int fperm[FC][NN];        // flavour pemutation matrix
    static const int fvcol[FC][CC];        // flavour-partial pemutation matrix
    static const unsigned char colmat[CC*(CC+1)/2];  // partial-partial colour factors
    static const unsigned char colmatcc[NN*(NN+1)/2][C0*(C0+1)/2];  // ccborn partial-partial factors
    static const int NmatLen = 1;
    static const int NmatccLen = 2;

    static const int HS = 1;
    static const int HSNN = NN+1;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp2q0gV : public NJetAmp<T>
{
    typedef NJetAmp<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;
    typedef typename BaseClass::ST ST;

    Amp2q0gV(const T scalefactor,
             const int mFC=1, const NJetAmpTables& tables=amptables());
    Amp2q0gV(const Flavour<double>& vflav, const T scalefactor,
             const int mFC=1, const NJetAmpTables& tables=amptables());

    void setNc(const ST Nc_);

  protected:
    using BaseClass::ngluons;
    using BaseClass::NN;
    using BaseClass::V;
    using BaseClass::Nc;
    using BaseClass::Nc2;
    using BaseClass::Nf;
    using BaseClass::C0;
    using BaseClass::CC;
    using BaseClass::Nmat;
    using BaseClass::Nmatcc;
    using BaseClass::bornFactor;
    using BaseClass::bornccFactor;
    using BaseClass::loopFactor;
    using BaseClass::getFlav;
    using BaseClass::getFperm;
    using BaseClass::fvSet;
    using BaseClass::mfv;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp2q0gVStatic>();
    }

    void initProcess(const Flavour<double>& Vflav);
    void initNc();

    virtual TreeValue A0(int p0, int p1);
    virtual LoopResult<T> AL(int p0, int p1);
//     virtual LoopResult<T> AF(int p0, int p1);

    void getfvpart0(const int fv, TreeValue* fvpart, TreeValue* fvpartC);

    void getfvpart1_full(const int fv, LoopValue* fvpart);
    void getfvpart1_full(const int fv, LoopResult<T>* fvpart);

    template <typename LT>
    void getfvpart1_full_(const int fv, LT* fvpart);
};

class Amp2q0gZStatic : public Amp2q0gVStatic
{
  public:
    static const int HS = 4;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp2q0gZ : public Amp2q0gV<T>
{
    typedef Amp2q0gV<T> BaseClass;
  public:

    Amp2q0gZ(const Flavour<double>& ff, const T scalefactor,
             const int mFC=1, const NJetAmpTables& tables=amptables())
    : BaseClass(ff, scalefactor, mFC, tables)
    { }

  protected:
    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp2q0gZStatic>();
    }
};

#endif // CHSUM_2Q0GV_H
