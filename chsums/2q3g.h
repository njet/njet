/*
* chsums/2q3g.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef CHSUM_2Q3G_H
#define CHSUM_2Q3G_H

#include "NJetAmpN.h"

class Amp2q3gStatic : public AmpNStatic<5>
{
  public:
    static const int FC = 1;   // flavour permutations
    static const int C0 = 6;   // partial amplitudes
    static const int CC = 11;  // partial amplitudes

    static const int flav[FC][NN];  // flavour vectors for ngluon
    static const int fvsign[FC];    // flavour channels' signs
    static const int ccsign[NN*(NN+1)/2];  // ccborn signs
    static const int fperm[FC][NN];        // flavour pemutation matrix
    static const int fvcol[FC][CC];        // flavour-partial pemutation matrix

    static const unsigned char colmat[CC*(CC+1)/2];  // partial-partial colour factors
    static const unsigned char colmatcc[NN*(NN+1)/2][C0*(C0+1)/2];  // ccborn partial-partial factors
    static const int NmatLen = 13;
    static const int NmatccLen = 14;

    static const int HS = 12; //16;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp2q3g : public NJetAmp5<T>
{
    typedef NJetAmp5<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;
    typedef typename BaseClass::ST ST;

    Amp2q3g(const T scalefactor, const int mFC=1, const NJetAmpTables& tables=amptables());

    void setNc(const ST Nc_);

    LoopValue virt() { return BaseClass::virt_tricksum(); }
    void born_part_fill() { BaseClass::born_part_trickfill(); }

  protected:
    using BaseClass::ngluons;
    using BaseClass::NN;
    using BaseClass::V;
    using BaseClass::Nc;
    using BaseClass::Nc2;
    using BaseClass::Nc3;
    using BaseClass::Nc4;
    using BaseClass::Nf;
    using BaseClass::C0;
    using BaseClass::Nmat;
    using BaseClass::Nmatcc;
    using BaseClass::NmatDS;
    using BaseClass::NmatDSLen;
    using BaseClass::bornFactor;
    using BaseClass::bornccFactor;
    using BaseClass::loopFactor;
    using BaseClass::getFlav;
    using BaseClass::getFperm;
    using BaseClass::mfv;
    using BaseClass::fvSet;
    using BaseClass::norderL;
    using BaseClass::norderF;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp2q3gStatic>();
    }

    void initNc();
    void initNc3();  // 3-desymmetrized

    using BaseClass::A0;
    using BaseClass::AF;
    using BaseClass::AL;
    virtual LoopResult<T> AL(int p0, int p1, int p2, int p3, int p4, int pos);
    virtual LoopResult<T> AF(int p0, int p1, int p2, int p3, int p4, int pos, int posR);

    static const int RcacheLen = 24;     // number of L primitives
    std::vector<EpsTriplet<T> > Rcache;  // rational part cache

    void getfvpart0(const int fv, TreeValue* fvpart, TreeValue* fvpartC);

    bool setLoopType(int type, int norderL_, int norderF_)
    {
      return BaseClass::setLoopTypeLCSLC(type, norderL_, norderF_);
    }

    void getfvpart1_full(const int fv, LoopValue* fvpart);
    void getfvpart1_full(const int fv, LoopResult<T>* fvpart);

    void getfvpart1_lc(const int fv, LoopValue* fvpart);
    void getfvpart1_lc(const int fv, LoopResult<T>* fvpart);

    void getfvpart1_slc(const int fv, LoopValue* fvpart);
    void getfvpart1_slc(const int fv, LoopResult<T>* fvpart);

    template <typename LT>
    void getfvpart1_full_(const int fv, LT* fvpart);

    template <typename LT>
    void getfvpart1_lc_(const int fv, LT* fvpart);

    template <typename LT>
    void getfvpart1_slc_(const int fv, LT* fvpart);

    // ds3 - 3-desymmetrized colour sum (to be used in derived classes)
    template <typename LT>
    void getfvpart1ds3_(const int fv, LT* fvpart);
};

// desymmetrized Amp2q3g_ds3

class Amp2q3g_ds3Static : public virtual Amp2q3gStatic
{
  public:
    static const int CDS = 4 + 1;
    static const unsigned int colmatds[CDS][C0];
    static const int NmatDSLen = 15;
};

template <typename T>
class Amp2q3g_ds3 : public Amp2q3g<T>
{
    typedef Amp2q3g<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;
    typedef typename BaseClass::ST ST;

    Amp2q3g_ds3(const T scalefactor,
                const int mFC=1, const NJetAmpTables& tables=amptables());

    void setNc(const ST Nc_);

    LoopValue virt() { return BaseClass::virt_dstricksum(); }
    LoopValue virt(const int* h) { return BaseClass::virtds(h); }

  protected:
    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp2q3g_ds3Static>();
    }

    void initNc();

    bool setLoopType(int type, int norderL_, int norderF_)
    {
      return BaseClass::setLoopTypeNONE(type, norderL_, norderF_);
    }

    void getfvpart1ds(const int fv, LoopValue* fvpart);
    void getfvpart1ds(const int fv, LoopResult<T>* fvpart);
};

#endif // CHSUM_2Q3G_H
