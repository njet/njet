/*
* chsums/2q2g-static.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>

#include "2q2g.h"

// class Amp2q2gStatic

const int
Amp2q2gStatic::flav[FC][NN] = {
  {-1,1,0,0}
};

const int
Amp2q2gStatic::fvsign[FC] = {1};

const int
Amp2q2gStatic::ccsign[NN*(NN+1)/2] = {1,1,1,1,1,1,1,1,1,1};

const int
Amp2q2gStatic::fperm[FC][NN] = {
  {0,1,2,3}
};

const int
Amp2q2gStatic::fvcol[FC][CC] = {
  {0,1,2}
};

const unsigned char
Amp2q2gStatic::colmat[CC*(CC+1)/2] = {0,3,0,1,1,2};

const unsigned char
Amp2q2gStatic::colmatcc[NN*(NN+1)/2][C0*(C0+1)/2] = {
  {},
  {4,5,4},
  {},
  {1,1,2},
  {2,1,1},
  {},
  {2,1,1},
  {1,1,2},
  {3,0,3},
  {}
};

const int
Amp2q2gStatic::HSarr[HS][HSNN] = {
//   {-1, 1, 1, 1},
  {-1, 1, 1,-1},
  {-1, 1,-1, 1},
//   {-1, 1,-1,-1},
//   { 1,-1,-1,-1},
  { 1,-1,-1, 1},
  { 1,-1, 1,-1},
//   { 1,-1, 1, 1}
};
