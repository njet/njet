/*
* chsums/6q1gV.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef CHSUM_6Q1GV_H
#define CHSUM_6Q1GV_H

#include "6q1g.h"
#include "../ngluon2/Model.h"

class Amp6q1gVStatic : public Amp6q1gStatic
{
  public:
    static const int HS = 8;
    static const int HSNN = NN+1;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp6q1gV : public Amp6q1g<T>
{
    typedef Amp6q1g<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;

    Amp6q1gV(const T scalefactor, const int mFC=1, const NJetAmpTables& tables=amptables());
    Amp6q1gV(const Flavour<double>& Vflav,
             const T scalefactor, const int mFC=1, const NJetAmpTables& tables=amptables());

    void born_part_fill() { BaseClass::born_part_fullfill(); }
    LoopValue virt() { return BaseClass::virt_fullsum(); }

  protected:
    using BaseClass::NN;
    using BaseClass::vhel;
    using BaseClass::getFlav;
    using BaseClass::getFperm;
    using BaseClass::mfv;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp6q1gVStatic>();
    }

    void initProcess(const Flavour<double>& ff);

    TreeValue A0(int p0, int p1, int p2, int p3, int p4, int p5, int p6);
    LoopResult<T> AL(int p0, int p1, int p2, int p3, int p4, int p5, int p6);
    LoopResult<T> AF(int p0, int p1, int p2, int p3, int p4, int p5, int p6);
};

class Amp6q1gV2Static : public Amp6q1gVStatic
{
  public:
    static const int HS = 12;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp6q1gV2 : public Amp6q1gV<T>
{
    typedef Amp6q1gV<T> BaseClass;
  public:

    Amp6q1gV2(const T scalefactor, const NJetAmpTables& tables=amptables())
      : BaseClass(scalefactor, 2, tables)
    { }

    Amp6q1gV2(const Flavour<double>& Vflav,
              const T scalefactor, const NJetAmpTables& tables=amptables())
      : BaseClass(Vflav, scalefactor, 2, tables)
    { }

  protected:
    using BaseClass::fvZero;
    using BaseClass::vhel;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp6q1gV2Static>();
    }

    void markZeroFv()
    {
      fvZero.reset();
      if (vhel[2] == vhel[3] || vhel[4] == vhel[5]) {
        fvZero[0] = true;
      }
      if (vhel[2] == vhel[5] || vhel[4] == vhel[3]) {
        fvZero[1] = true;
      }
    }
};

// symmetric quarks

class Amp6q1gV2nStatic : public Amp6q1gVStatic
{
  public:
    static const int HS = 12;
    static const int HSarr[HS][HSNN];

    static const int FC = 6;
    static const int flav[FC][NN];
    static const int fperm[FC][NN];
    static const int fvcol[FC][CC];
};

template <typename T>
class Amp6q1gV2n : public Amp6q1gV<T>
{
    typedef Amp6q1gV<T> BaseClass;
  public:

    Amp6q1gV2n(const T scalefactor, const int mFC=2, const NJetAmpTables& tables=amptables())
      : BaseClass(scalefactor, mFC, tables)
    { }

    Amp6q1gV2n(const Flavour<double>& Vflav,
               const T scalefactor, const int mFC=2, const NJetAmpTables& tables=amptables())
      : BaseClass(Vflav, scalefactor, mFC, tables)
    { }

  protected:
    using BaseClass::fvZero;
    using BaseClass::vhel;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp6q1gV2nStatic>();
    }

    void markZeroFv()
    {
      fvZero.reset();
      if (vhel[0] == vhel[1] || vhel[2] == vhel[3]) {
        fvZero[0] = true;
      }
      if (vhel[0] == vhel[3] || vhel[2] == vhel[1]) {
        fvZero[1] = true;
      }
    }
};

class Amp6q1gV6nStatic : public Amp6q1gV2nStatic
{
  public:
    static const int HS = 20;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp6q1gV6n : public Amp6q1gV2n<T>
{
    typedef Amp6q1gV2n<T> BaseClass;
  public:

    Amp6q1gV6n(const T scalefactor, const NJetAmpTables& tables=amptables())
      : BaseClass(scalefactor, 6, tables)
    { }

    Amp6q1gV6n(const Flavour<double>& Vflav,
              const T scalefactor, const NJetAmpTables& tables=amptables())
      : BaseClass(Vflav, scalefactor, 6, tables)
    { }

  protected:
    using BaseClass::fvZero;
    using BaseClass::vhel;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp6q1gV6nStatic>();
    }

    void markZeroFv()
    {
      fvZero.reset();
      if (vhel[0] == vhel[1]) {
        fvZero[0] = true;
        fvZero[5] = true;
      }
      if (vhel[0] == vhel[3]) {
        fvZero[4] = true;
        fvZero[1] = true;
      }
      if (vhel[0] == vhel[5]) {
        fvZero[2] = true;
        fvZero[3] = true;
      }

      if (vhel[2] == vhel[3]) {
        fvZero[0] = true;
        fvZero[3] = true;
      }
      if (vhel[2] == vhel[5]) {
        fvZero[4] = true;
        fvZero[5] = true;
      }
      if (vhel[2] == vhel[1]) {
        fvZero[2] = true;
        fvZero[1] = true;
      }

      if (vhel[4] == vhel[5]) {
        fvZero[0] = true;
        fvZero[1] = true;
      }
      if (vhel[4] == vhel[1]) {
        fvZero[4] = true;
        fvZero[3] = true;
      }
      if (vhel[4] == vhel[3]) {
        fvZero[2] = true;
        fvZero[5] = true;
      }
    }
};

// symmetric anti-quarks

class Amp6q1gV2bStatic : public Amp6q1gVStatic
{
  public:
    static const int HS = 12;
    static const int HSarr[HS][HSNN];

    static const int FC = 6;
    static const int flav[FC][NN];
    static const int fperm[FC][NN];
    static const int fvcol[FC][CC];
};

template <typename T>
class Amp6q1gV2b : public Amp6q1gV<T>
{
    typedef Amp6q1gV<T> BaseClass;
  public:

    Amp6q1gV2b(const T scalefactor, const int mFC=2, const NJetAmpTables& tables=amptables())
      : BaseClass(scalefactor, mFC, tables)
    { }

    Amp6q1gV2b(const Flavour<double>& Vflav,
               const T scalefactor, const int mFC=2, const NJetAmpTables& tables=amptables())
      : BaseClass(Vflav, scalefactor, mFC, tables)
    { }

  protected:
    using BaseClass::fvZero;
    using BaseClass::vhel;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp6q1gV2bStatic>();
    }

    void markZeroFv()
    {
      fvZero.reset();
      if (vhel[0] == vhel[1] || vhel[2] == vhel[3]) {
        fvZero[0] = true;
      }
      if (vhel[0] == vhel[3] || vhel[2] == vhel[1]) {
        fvZero[1] = true;
      }
    }
};

class Amp6q1gV6bStatic : public Amp6q1gV2bStatic
{
  public:
    static const int HS = 20;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp6q1gV6b : public Amp6q1gV2b<T>
{
    typedef Amp6q1gV2b<T> BaseClass;
  public:

    Amp6q1gV6b(const T scalefactor, const NJetAmpTables& tables=amptables())
      : BaseClass(scalefactor, 6, tables)
    { }

    Amp6q1gV6b(const Flavour<double>& Vflav,
              const T scalefactor, const NJetAmpTables& tables=amptables())
      : BaseClass(Vflav, scalefactor, 6, tables)
    { }

  protected:
    using BaseClass::fvZero;
    using BaseClass::vhel;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp6q1gV6bStatic>();
    }

    void markZeroFv()
    {
      fvZero.reset();
      if (vhel[0] == vhel[1]) {
        fvZero[0] = true;
        fvZero[5] = true;
      }
      if (vhel[0] == vhel[3]) {
        fvZero[1] = true;
        fvZero[2] = true;
      }
      if (vhel[0] == vhel[5]) {
        fvZero[4] = true;
        fvZero[3] = true;
      }

      if (vhel[2] == vhel[3]) {
        fvZero[0] = true;
        fvZero[3] = true;
      }
      if (vhel[2] == vhel[5]) {
        fvZero[2] = true;
        fvZero[5] = true;
      }
      if (vhel[2] == vhel[1]) {
        fvZero[4] = true;
        fvZero[1] = true;
      }

      if (vhel[4] == vhel[5]) {
        fvZero[0] = true;
        fvZero[1] = true;
      }
      if (vhel[4] == vhel[1]) {
        fvZero[2] = true;
        fvZero[3] = true;
      }
      if (vhel[4] == vhel[3]) {
        fvZero[4] = true;
        fvZero[5] = true;
      }
    }
};

// both

class Amp6q1gV4nbStatic : public Amp6q1gVStatic
{
  public:
    static const int HS = 18;
    static const int HSarr[HS][HSNN];

    static const int FC = 4;
    static const int flav[FC][NN];
    static const int fperm[FC][NN];
    static const int fvcol[FC][CC];
    static const int fvsign[FC];
};

template <typename T>
class Amp6q1gV4nb : public Amp6q1gV<T>
{
    typedef Amp6q1gV<T> BaseClass;
  public:

    Amp6q1gV4nb(const T scalefactor, const NJetAmpTables& tables=amptables())
      : BaseClass(scalefactor, 4, tables)
    { }

    Amp6q1gV4nb(const Flavour<double>& Vflav,
              const T scalefactor, const NJetAmpTables& tables=amptables())
      : BaseClass(Vflav, scalefactor, 4, tables)
    { }

  protected:
    using BaseClass::fvZero;
    using BaseClass::vhel;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp6q1gV4nbStatic>();
    }

    void markZeroFv()
    {
      fvZero.reset();
      if (vhel[0] == vhel[1] || vhel[2] == vhel[3] || vhel[4] == vhel[5]) {
        fvZero[0] = true;
      }
      if (vhel[0] == vhel[3] || vhel[2] == vhel[1] || vhel[4] == vhel[5]) {
        fvZero[1] = true;
      }
      if (vhel[4] == vhel[1] || vhel[2] == vhel[3] || vhel[0] == vhel[5]) {
        fvZero[2] = true;
      }
      if (vhel[4] == vhel[3] || vhel[2] == vhel[1] || vhel[0] == vhel[5]) {
        fvZero[3] = true;
      }
    }
};

// Z amplitudes

class Amp6q1gZStatic : public Amp6q1gVStatic
{
  public:
    static const int FC = 18;

    static const int flav[FC][NN];  // flavour vectors for ngluon
    static const int fvsign[FC];    // flavour channels' signs
    static const int fperm[FC][NN];        // flavour pemutation matrix
    static const int fvcol[FC][CC];        // flavour-partial pemutation matrix

    static const int HS = 32;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp6q1gZ : public Amp6q1gV<T>
{
    typedef Amp6q1gV<T> BaseClass;
  public:

    Amp6q1gZ(const Flavour<double>& ff, const T scalefactor,
             const int mFC=3, const NJetAmpTables& tables=amptables())
    : BaseClass(ff, scalefactor, mFC, tables)
    { }

  protected:
    using BaseClass::fvZero;
    using BaseClass::vhel;
    using BaseClass::NN;
    using BaseClass::getFlav;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp6q1gZStatic>();
    }
};

template <typename T>
class Amp6q1gZd : public Amp6q1gZ<T>
{
    typedef Amp6q1gZ<T> BaseClass;
  public:

    Amp6q1gZd(const Flavour<double>& ff, const T scalefactor,
             const int mFC=3, const NJetAmpTables& tables=BaseClass::amptables());

  protected:
    using BaseClass::NN;
    using BaseClass::getFlav;
};

class Amp6q1gZ2Static : public Amp6q1gZStatic
{
  public:
    static const int HS = 48;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp6q1gZ2 : public Amp6q1gZ<T>
{
    typedef Amp6q1gZ<T> BaseClass;
  public:

    Amp6q1gZ2(const Flavour<double>& ff, const T scalefactor,
             const int mFC=6, const NJetAmpTables& tables=amptables())
    : BaseClass(ff, scalefactor, mFC, tables)
    { }

  protected:
    using BaseClass::fvZero;
    using BaseClass::vhel;
    using BaseClass::NN;
    using BaseClass::getFlav;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp6q1gZ2Static>();
    }

    void markZeroFv()
    {
      fvZero.reset();
      if (vhel[0] == vhel[1] || vhel[2] == vhel[3]) {
        fvZero[0] = true;
        fvZero[1] = true;
        fvZero[2] = true;
      }
      if (vhel[0] == vhel[3] || vhel[2] == vhel[1]) {
        fvZero[3] = true;
        fvZero[4] = true;
        fvZero[5] = true;
      }
    }
};

template <typename T>
class Amp6q1gZ2d : public Amp6q1gZ2<T>
{
    typedef Amp6q1gZ2<T> BaseClass;
  public:

    Amp6q1gZ2d(const Flavour<double>& ff, const T scalefactor,
             const int mFC=6, const NJetAmpTables& tables=BaseClass::amptables());

  protected:
    using BaseClass::NN;
    using BaseClass::getFlav;
};

class Amp6q1gZ6Static : public Amp6q1gZStatic
{
  public:
    static const int HS = 80;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp6q1gZ6 : public Amp6q1gZ<T>
{
    typedef Amp6q1gZ<T> BaseClass;
  public:

    Amp6q1gZ6(const Flavour<double>& ff, const T scalefactor,
             const int mFC=18, const NJetAmpTables& tables=amptables())
    : BaseClass(ff, scalefactor, mFC, tables)
    { }

  protected:
    using BaseClass::fvZero;
    using BaseClass::vhel;
    using BaseClass::NN;
    using BaseClass::getFlav;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp6q1gZ6Static>();
    }

    void markZeroFv()
    {
      fvZero.reset();
      if (vhel[0] == vhel[1]) {
        fvZero[0] = true;
        fvZero[1] = true;
        fvZero[2] = true;
        fvZero[15] = true;
        fvZero[16] = true;
        fvZero[17] = true;
      }
      if (vhel[0] == vhel[3]) {
        fvZero[3] = true;
        fvZero[4] = true;
        fvZero[5] = true;
        fvZero[12] = true;
        fvZero[13] = true;
        fvZero[14] = true;
      }
      if (vhel[0] == vhel[5]) {
        fvZero[6] = true;
        fvZero[7] = true;
        fvZero[8] = true;
        fvZero[9] = true;
        fvZero[10] = true;
        fvZero[11] = true;
      }
      if (vhel[2] == vhel[3]) {
        fvZero[0] = true;
        fvZero[1] = true;
        fvZero[2] = true;
        fvZero[9] = true;
        fvZero[10] = true;
        fvZero[11] = true;
      }
      if (vhel[2] == vhel[5]) {
        fvZero[12] = true;
        fvZero[13] = true;
        fvZero[14] = true;
        fvZero[15] = true;
        fvZero[16] = true;
        fvZero[17] = true;
      }
      if (vhel[2] == vhel[1]) {
        fvZero[3] = true;
        fvZero[4] = true;
        fvZero[5] = true;
        fvZero[6] = true;
        fvZero[7] = true;
        fvZero[8] = true;
      }
      if (vhel[4] == vhel[5]) {
        fvZero[0] = true;
        fvZero[1] = true;
        fvZero[2] = true;
        fvZero[3] = true;
        fvZero[4] = true;
        fvZero[5] = true;
      }
      if (vhel[4] == vhel[1]) {
        fvZero[9] = true;
        fvZero[10] = true;
        fvZero[11] = true;
        fvZero[12] = true;
        fvZero[13] = true;
        fvZero[14] = true;
      }
      if (vhel[4] == vhel[3]) {
        fvZero[6] = true;
        fvZero[7] = true;
        fvZero[8] = true;
        fvZero[15] = true;
        fvZero[16] = true;
        fvZero[17] = true;
      }
    }
};

#endif // CHSUM_6Q1G_H
