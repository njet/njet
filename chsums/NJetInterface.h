/*
* chsums/NJetInterface.h
*
* This file is part of NJet library
* Copyright (C) 2013 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef CHSUM_NJETINTERFACE_H
#define CHSUM_NJETINTERFACE_H

#include <map>

#include "../ngluon2/utility.h"

template <typename T>
class NJetAccuracy;

template <typename T>
class NJetInterface
{
  public:
    typedef NJetAccuracy<T>* AmpType;
    typedef typename vector_traits<T>::scalar ST;

    #include "NJetChannels.h"

    NJetInterface(const ST nc_=DEFAULT_Nc,
                  const ST nf_=DEFAULT_Nf,
                  int scm_=DEFAULT_scheme,
                  int ren_=DEFAULT_renorm);
    ~NJetInterface();

    void setNc(const ST Nc_);
    void setNf(const ST Nf_);

    void setScheme(int scheme_);
    void setRenorm(int renorm_);

    AmpType getAmp(const int idx);

    void clear();

  protected:
    typedef std::map<int, AmpType> AmpMap;

    static ST DEFAULT_Nc;
    static ST DEFAULT_Nf;
    static int DEFAULT_scheme;
    static int DEFAULT_renorm;

    AmpType createAmp(const int idx);

    ST Nc, Nf;
    int scheme, renorm;

    AmpMap amps;
};

#endif /* CHSUM_NJETINTERFACE_H */
