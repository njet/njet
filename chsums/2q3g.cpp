/*
* chsums/2q3g.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>

#include "2q3g.h"
#include "../ngluon2/NGluon2.h"

// class Amp2q3g

template <typename T>
Amp2q3g<T>::Amp2q3g(const T scalefactor, const int mFC, const NJetAmpTables& tables)
  : BaseClass(mFC, tables),
    Rcache(RcacheLen)
{
  for (int fv=0; fv<mFC; fv++) {
    BaseClass::initNG(scalefactor, fv);
    BaseClass::setProcess(StandardModel::NGluon1compat(NN, getFlav(fv)), fv);
  }

  initNc();
}

template <typename T>
void Amp2q3g<T>::setNc(const ST Nc_)
{
  BaseClass::setNc(Nc_);
  initNc();
}

template <typename T>
void Amp2q3g<T>::initNc()
{
  Nmat[0] = Nc*V;
  Nmat[1] = V*V;
  Nmat[2] = Nc*(Nc2-2.);
  Nmat[3] = -V;
  Nmat[4] = -Nc;
  Nmat[5] = -2*Nc;
  Nmat[6] = 1.;
  Nmat[7] = 0.;
  Nmat[8] = 1.+Nc2;
  Nmat[9] = Nc2*V;
  Nmat[10] = Nc2;
  Nmat[11] = (-2.+Nc2)*Nc2;
  Nmat[12] = -2.*Nc2;
  assert(12 < BaseClass::NmatLen);

  Nmatcc[0] = 0.;
  Nmatcc[1] = Nc2*V;
  Nmatcc[2] = -Nc2;
  Nmatcc[3] = -Nc2-Nc4;
  Nmatcc[4] = Nc4;
  Nmatcc[5] = -Nc4*V;
  Nmatcc[6] = -Nc2*V*V;
  Nmatcc[7] = 1.+2.*Nc2-Nc4;
  Nmatcc[8] = 1.+Nc2;
  Nmatcc[9] = -Nc4;
  Nmatcc[10] = -2.*Nc4;
  Nmatcc[11] = 1.;
  Nmatcc[12] = 1.+3.*Nc2;
  Nmatcc[13] = 2.*Nc4;
  assert(13 < BaseClass::NmatccLen);

  bornFactor = V/Nc2;
  loopFactor = 2.*bornFactor;
  bornccFactor = -0.5*V/Nc3;
}

template <typename T>
LoopResult<T> Amp2q3g<T>::AF(int p0, int p1, int p2, int p3, int p4, int pos, int posR)
{
  const int* O = getFperm(mfv);
  const int order[] = {O[p0], O[p1], O[p2], O[p3], O[p4]};
  LoopResult<T> ans = ngluons[mfv]->eval(NGluon2<T>::LIGHTQ, order, true);
  const LoopResult<T> ratL = {Rcache[pos], conj(Rcache[pos])};
  const LoopResult<T> ratR = {Rcache[posR], conj(Rcache[posR])};
  ans += ratL - ratR;
  return ans;
}

template <typename T>
LoopResult<T> Amp2q3g<T>::AL(int p0, int p1, int p2, int p3, int p4, int pos)
{
  const int* O = getFperm(mfv);
  const int order[] = {O[p0], O[p1], O[p2], O[p3], O[p4]};
  const LoopResult<T> ans = ngluons[mfv]->eval(NGluon2<T>::MIXED, order);
  Rcache[pos] = ngluons[mfv]->lastValue().looprat();
  return ans;
}

template <typename T>
void Amp2q3g<T>::getfvpart1_full(const int fv, LoopValue* fvpart)
{
  return getfvpart1_full_(fv, fvpart);
}

template <typename T>
void Amp2q3g<T>::getfvpart1_full(const int fv, LoopResult<T>* fvpart)
{
  return getfvpart1_full_(fv, fvpart);
}

// -------- BEGIN -- automatically generated code -- BEGIN -------- //

template <typename T>
void Amp2q3g<T>::getfvpart0(const int fv, TreeValue* fvpart, TreeValue* fvpartC)
{
  fvSet(fv);
  fvpart[0] = A0(0,1,2,3,4);
  fvpart[1] = A0(0,1,2,4,3);
  fvpart[2] = A0(0,1,3,2,4);
  fvpart[3] = A0(0,1,3,4,2);
  fvpart[4] = A0(0,1,4,2,3);
  fvpart[5] = A0(0,1,4,3,2);

  if (fvpartC) {
    for (int i=0; i<C0; i++) {
      fvpartC[i] = conj(fvpart[i]);
    }
  }
}

template <typename T>
template <typename LT>
inline
void Amp2q3g<T>::getfvpart1_full_(const int fv, LT* fvpart)
{
  fvSet(fv);
  {
    // 24 primitives
    const LT P12345 = AL(0,1,2,3,4, 0);
    const LT P12354 = AL(0,1,2,4,3, 1);
    const LT P12435 = AL(0,1,3,2,4, 2);
    const LT P12453 = AL(0,1,3,4,2, 3);
    const LT P12534 = AL(0,1,4,2,3, 4);
    const LT P12543 = AL(0,1,4,3,2, 5);
    const LT P13245 = AL(0,2,1,3,4, 6);
    const LT P13254 = AL(0,2,1,4,3, 7);
    const LT P13425 = AL(0,2,3,1,4, 8);
    const LT P13452 = AL(0,2,3,4,1, 9);
    const LT P13524 = AL(0,2,4,1,3, 10);
    const LT P13542 = AL(0,2,4,3,1, 11);
    const LT P14235 = AL(0,3,1,2,4, 12);
    const LT P14253 = AL(0,3,1,4,2, 13);
    const LT P14325 = AL(0,3,2,1,4, 14);
    const LT P14352 = AL(0,3,2,4,1, 15);
    const LT P14523 = AL(0,3,4,1,2, 16);
    const LT P14532 = AL(0,3,4,2,1, 17);
    const LT P15234 = AL(0,4,1,2,3, 18);
    const LT P15243 = AL(0,4,1,3,2, 19);
    const LT P15324 = AL(0,4,2,1,3, 20);
    const LT P15342 = AL(0,4,2,3,1, 21);
    const LT P15423 = AL(0,4,3,1,2, 22);
    const LT P15432 = AL(0,4,3,2,1, 23);

    fvpart[0] = Nc*P12345+P15432/Nc;
    fvpart[1] = Nc*P12354+P14532/Nc;
    fvpart[2] = Nc*P12435+P15342/Nc;
    fvpart[3] = Nc*P12453+P13542/Nc;
    fvpart[4] = Nc*P12534+P14352/Nc;
    fvpart[5] = Nc*P12543+P13452/Nc;
    fvpart[6] = P12345+P12354+P12435+P12453+P12534+P12543+P13245+P13254+P13425+P14235+P14253+P14325;
    fvpart[7] = P12345+P12354+P12435+P12453+P12534+P12543+P13245+P13254+P13524+P15234+P15243+P15324;
    fvpart[8] = P12345+P12354+P12435+P12453+P12534+P12543+P14235+P14253+P14523+P15234+P15243+P15423;
    fvpart[9] = -P12354-P12435-P12543-P13254-P13524-P13542-P14235-P14325-P14352-P15243-P15423-P15432;
    fvpart[10] = -P12345-P12453-P12534-P13245-P13425-P13452-P14253-P14523-P14532-P15234-P15324-P15342;
  }
  if (Nf != 0.) {
    // 6 primitives
    const LT Q12345 = AF(0,1,2,3,4, 0,23);
    const LT Q12354 = AF(0,1,2,4,3, 1,17);
    const LT Q12435 = AF(0,1,3,2,4, 2,21);
    const LT Q12453 = AF(0,1,3,4,2, 3,11);
    const LT Q12534 = AF(0,1,4,2,3, 4,15);
    const LT Q12543 = AF(0,1,4,3,2, 5, 9);

    fvpart[0] += Nf*(-Q12345);
    fvpart[1] += Nf*(-Q12354);
    fvpart[2] += Nf*(-Q12435);
    fvpart[3] += Nf*(-Q12453);
    fvpart[4] += Nf*(-Q12534);
    fvpart[5] += Nf*(-Q12543);
    fvpart[9] += Nf*((Q12345+Q12354+Q12435+Q12453+Q12534+Q12543)/(2.*Nc));
    fvpart[10] += Nf*((Q12345+Q12354+Q12435+Q12453+Q12534+Q12543)/(2.*Nc));
  }
}

// --------- END --- automatically generated code --- END --------- //

// desymmetrized stuff

template <typename T>
void Amp2q3g<T>::initNc3()
{
  NmatDS[0]  = 0.;
  NmatDS[1]  = 12.*Nc3;
  NmatDS[2]  = 6.*Nc3;
  NmatDS[3]  = 6.*Nc;
  NmatDS[4]  = 6.*Nc*(1. + Nc2);
  NmatDS[5]  = -6.*(-1. - 2.*Nc2 + Nc4)/Nc;
  NmatDS[6]  = 9.*(-2. + Nc2);
  NmatDS[7]  = 3.*(-6. + Nc2);
  NmatDS[8]  = 6.*(1. + Nc2)/Nc;
  NmatDS[9]  = -3.*(6. - 5.*Nc2 + 2.*Nc4);
  NmatDS[10] = -6.*Nc*V;
  NmatDS[11] = -3.*(6. + Nc2);
  NmatDS[12] = 6.*(1. + 3.*Nc2)/Nc;
  NmatDS[13] = 6.*Nc*Nc4;
  NmatDS[14] = 6./Nc;
  assert(14 < NmatDSLen);
}

template <typename T>
template <typename LT>
void Amp2q3g<T>::getfvpart1ds3_(const int fv, LT* fvpart)
{
  fvSet(fv);
  fvpart[0] = AL(0,1,2,3,4);
  fvpart[1] = AL(0,2,1,3,4);
  fvpart[2] = AL(0,2,3,1,4);
  fvpart[3] = AL(0,2,3,4,1);

  if (Nf != 0.) {
    fvpart[4] = Nf*AF(0,1,2,3,4);
  } else {
    fvpart[4] = LT();
  }
}

#ifdef USE_SD
  template class Amp2q3g<double>;
#endif
#ifdef USE_DD
  template class Amp2q3g<dd_real>;
#endif
#ifdef USE_QD
  template class Amp2q3g<qd_real>;
#endif
#ifdef USE_VC
  template class Amp2q3g<Vc::double_v>;
#endif

// class Amp2q3g_ds3

template <typename T>
Amp2q3g_ds3<T>::Amp2q3g_ds3(const T scalefactor,
                            const int mFC, const NJetAmpTables& tables)
  : BaseClass(scalefactor, mFC, tables)
{
  BaseClass::initNc3();
}

template <typename T>
void Amp2q3g_ds3<T>::setNc(const ST Nc_)
{
  BaseClass::setNc(Nc_);
  BaseClass::initNc3();
}

template <typename T>
void Amp2q3g_ds3<T>::getfvpart1ds(const int fv, LoopValue* fvpart)
{
  return BaseClass::getfvpart1ds3_(fv, fvpart);
}

template <typename T>
void Amp2q3g_ds3<T>::getfvpart1ds(const int fv, LoopResult<T>* fvpart)
{
  return BaseClass::getfvpart1ds3_(fv, fvpart);
}

#ifdef USE_SD
  template class Amp2q3g_ds3<double>;
#endif
#ifdef USE_DD
  template class Amp2q3g_ds3<dd_real>;
#endif
#ifdef USE_QD
  template class Amp2q3g_ds3<qd_real>;
#endif
#ifdef USE_VC
  template class Amp2q3g_ds3<Vc::double_v>;
#endif
