/*
* chsums/2q3gA.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include "2q3gA.h"
#include "NJetAmp-T.h"

// class Amp2q3gAx

template <typename T>
Amp2q3gAx<T>::Amp2q3gAx(const Flavour<double>& ff, const T scalefactor,
                        const int mFC, const NJetAmpTables& tables)
 : BaseClass(ff, scalefactor, mFC, tables)
{
  const Flavour<double> Ax = StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C());
  for (int fv=0; fv<mFC; fv++) {
    if (fv % 2 == 0) {
      Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
      flavours.push_back(Ax);
      BaseClass::setProcess(flavours, fv);
    }
  }
}

template <typename T>
void Amp2q3gAx<T>::setNf(const ST Nf_)
{
  ST newNf = Nf_;
  const int intNf = int(to_double(newNf));
  if (double(intNf) == newNf) {
    const int nu = intNf/2;
    const int nd = (intNf+1)/2;
    newNf = -(double(nu)*Particle<ST>::getQu() + double(nd)*Particle<ST>::getQd());
  }
  BaseClass::setNf(newNf);
}

template <typename T>
void Amp2q3gAx<T>::getfvpart1_full(const int fv, LoopValue* fvpart)
{
  return getfvpart1_full_(fv, fvpart);
}

template <typename T>
void Amp2q3gAx<T>::getfvpart1_full(const int fv, LoopResult<T>* fvpart)
{
  return getfvpart1_full_(fv, fvpart);
}

template <typename T>
template <typename LT>
inline
void Amp2q3gAx<T>::getfvpart1_full_(const int fv, LT* fvpart)
{
  fvSet(fv);

  if (fv % 2 == 0 and Nf != 0.) {
    // 6 primitives
    const LT Q12345 = AFx(0,1,2,3,4);
    const LT Q12354 = AFx(0,1,2,4,3);
    const LT Q12435 = AFx(0,1,3,2,4);
    const LT Q12453 = AFx(0,1,3,4,2);
    const LT Q12534 = AFx(0,1,4,2,3);
    const LT Q12543 = AFx(0,1,4,3,2);

    fvpart[0] = Nf*(-Q12345);
    fvpart[1] = Nf*(-Q12354);
    fvpart[2] = Nf*(-Q12435);
    fvpart[3] = Nf*(-Q12453);
    fvpart[4] = Nf*(-Q12534);
    fvpart[5] = Nf*(-Q12543);
    fvpart[6] = Nf*((Q12345-Q12354+Q12435-Q12453+Q12534+Q12543)/Nc);
    fvpart[7] = Nf*((-Q12345+Q12354+Q12435+Q12453+Q12534-Q12543)/Nc);
    fvpart[8] = Nf*((Q12345+Q12354-Q12435+Q12453-Q12534+Q12543)/Nc);
    fvpart[9] = Nf*((Q12345-Q12354-Q12435+Q12453+Q12534-Q12543)/(2.*Nc));
    fvpart[10] = Nf*((-Q12345+Q12354+Q12435-Q12453-Q12534+Q12543)/(2.*Nc));
  } else {
    fvpart[0] = LT();
    fvpart[1] = LT();
    fvpart[2] = LT();
    fvpart[3] = LT();
    fvpart[4] = LT();
    fvpart[5] = LT();
    fvpart[6] = LT();
    fvpart[7] = LT();
    fvpart[8] = LT();
    fvpart[9] = LT();
    fvpart[10] = LT();
  }
}

template <typename T>
LoopResult<T> Amp2q3gAx<T>::AFx(int p0, int p1, int p2, int p3, int p4)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], O[p1], O[p2], O[p3], O[p4], NN};
  return BaseClass::AFnVax(order);
}

// class Amp2q3gAA

template <typename T>
Amp2q3gAA<T>::Amp2q3gAA(const Flavour<double>& ff, const T scalefactor,
                        const int mFC, const NJetAmpTables& tables)
  : BaseClass(scalefactor, mFC, tables)
{
  initProcess(ff);
}

template <typename T>
void Amp2q3gAA<T>::initProcess(const Flavour<double>& ff)
{
  for (int fv=0; fv<BaseClass::mFC; fv++) {
    Flavour<double>::FlavourList flavours = StandardModel::NGluon1compat(NN, getFlav(fv));
    flavours.push_back(ff);
    flavours.push_back(ff);
    BaseClass::setProcess(flavours, fv);
  }
}

template <typename T>
typename Amp2q3gAA<T>::TreeValue
Amp2q3gAA<T>::A0(int p0, int p1, int p2, int p3, int p4)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, NN+1, O[p1], O[p2], O[p3], O[p4]};
  return BaseClass::A0nAA(order);
}

template <typename T>
LoopResult<T> Amp2q3gAA<T>::AF(int p0, int p1, int p2, int p3, int p4)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, NN+1, O[p1], O[p2], O[p3], O[p4]};
  return BaseClass::AFnAA(order);
}

template <typename T>
LoopResult<T> Amp2q3gAA<T>::AL(int p0, int p1, int p2, int p3, int p4)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, NN+1, O[p1], O[p2], O[p3], O[p4]};
  return BaseClass::ALnAA(order);
}

template <typename T>
LoopResult<T> Amp2q3gAA<T>::AF(int p0, int p1, int p2, int p3, int p4, int /*pos*/, int /*posR*/)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, NN+1, O[p1], O[p2], O[p3], O[p4]};
  return BaseClass::AFnAA(order);
}

template <typename T>
LoopResult<T> Amp2q3gAA<T>::AL(int p0, int p1, int p2, int p3, int p4, int /*pos*/)
{
  const int* O = getFperm(mfv);
  int order[] = {O[p0], NN, NN+1, O[p1], O[p2], O[p3], O[p4]};
  return BaseClass::ALnAA(order);
}

#ifdef USE_SD
  template class Amp2q3gAx<double>;
  template class Amp2q3gAA<double>;
#endif
#ifdef USE_DD
  template class Amp2q3gAx<dd_real>;
  template class Amp2q3gAA<dd_real>;
#endif
#ifdef USE_QD
  template class Amp2q3gAx<qd_real>;
  template class Amp2q3gAA<qd_real>;
#endif
#ifdef USE_VC
  template class Amp2q3gAx<Vc::double_v>;
  template class Amp2q3gAA<Vc::double_v>;
#endif
