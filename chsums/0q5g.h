/*
* chsums/0q5g.h
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef CHSUM_0Q5G_H
#define CHSUM_0Q5G_H

#include "NJetAmpN.h"

class Amp0q5gStatic : public AmpNStatic<5>
{
  public:
    static const int FC = 1;   // flavour permutations
    static const int C0 = 6;   // partial amplitudes
    static const int CDS = 12 + 12;
    static const int CC = CDS;   // partial amplitudes

    static const int flav[FC][NN];  // flavour vectors for ngluon
    static const int fvsign[FC];    // flavour channels' signs
    static const int ccsign[NN*(NN+1)/2];  // ccborn signs
    static const int fperm[FC][NN];        // flavour pemutation matrix
    static const int fvcol[FC][C0];        // flavour-partial pemutation matrix

    static const unsigned char colmat[C0*(C0+1)/2];  // partial-partial colour factors
    static const unsigned char colmatcc[NN*(NN+1)/2][C0*(C0+1)/2];  // ccborn partial-partial factors
    static const unsigned int colmatds[CDS][C0];
    static const int NmatLen = 4;
    static const int NmatccLen = 7;
    static const int NmatDSLen = 7;

    static const int HS = 20; //32;
    static const int HSarr[HS][HSNN];
};

template <typename T>
class Amp0q5g : public NJetAmp5<T>
{
    typedef NJetAmp5<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;
    typedef typename BaseClass::ST ST;

    Amp0q5g(const T scalefactor, const int mFC=1, const NJetAmpTables& tables=amptables());

    void setNc(const ST Nc_);

    void born_part_fill() { BaseClass::born_part_trickfill(); }

    LoopValue virt() { return BaseClass::virt_dstricksum(); }
    LoopValue virt(const int* h) { return BaseClass::virtds(h); }

  protected:
    using BaseClass::ngluons;
    using BaseClass::NN;
    using BaseClass::V;
    using BaseClass::Nc;
    using BaseClass::Nc2;
    using BaseClass::Nc3;
    using BaseClass::Nc4;
    using BaseClass::Nf;
    using BaseClass::C0;
    using BaseClass::Nmat;
    using BaseClass::Nmatcc;
    using BaseClass::NmatDS;
    using BaseClass::bornFactor;
    using BaseClass::bornccFactor;
    using BaseClass::loopFactor;
    using BaseClass::getFlav;
    using BaseClass::getFperm;
    using BaseClass::mfv;
    using BaseClass::fvSet;

    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp0q5gStatic>();
    }

    void initNc();
    void initNc3();  // 3-desymmetrized

    using BaseClass::A0;
    using BaseClass::AF;
    using BaseClass::AL;
    virtual LoopResult<T> AL(int p0, int p1, int p2, int p3, int p4, int pos);
    virtual LoopResult<T> AF(int p0, int p1, int p2, int p3, int p4, int pos);

    static const int RcacheLen = 12;     // number of L primitives
    std::vector<EpsTriplet<T> > Rcache;  // rational part cache

    void getfvpart0(const int fv, TreeValue* fvpart, TreeValue* fvpartC);

    // we are using ds-style colour sums (primitive/primitive)
    void getfvpart1ds(const int fv, LoopValue* fvpart);
    void getfvpart1ds(const int fv, LoopResult<T>* fvpart);

    template <typename LT>
    void getfvpart1ds_(const int fv, LT* fvpart);

    // ds3 - 3-desymmetrized colour sum (to be used in derived classes)
    template <typename LT>
    void getfvpart1ds3_(const int fv, LT* fvpart);
};

// desymmetrized Amp0q5g_ds3

class Amp0q5g_ds3Static : public Amp0q5gStatic
{
  public:
    static const int CDS = 4 + 4;
    static const unsigned int colmatds[CDS][C0];
};

template <typename T>
class Amp0q5g_ds3 : public Amp0q5g<T>
{
    typedef Amp0q5g<T> BaseClass;
  public:
    typedef typename BaseClass::LoopValue LoopValue;
    typedef typename BaseClass::TreeValue TreeValue;
    typedef typename BaseClass::ST ST;

    Amp0q5g_ds3(const T scalefactor,
                const int mFC=1, const NJetAmpTables& tables=amptables());

    void setNc(const ST Nc_);

  protected:
    static NJetAmpTables amptables()
    {
      return NJetAmpTables::create<Amp0q5g_ds3Static>();
    }

    void initNc();

    void getfvpart1ds(const int fv, LoopValue* fvpart);
    void getfvpart1ds(const int fv, LoopResult<T>* fvpart);
};

#endif // CHSUM_0Q5G_H
