/*
* chsums/4q2g-static.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include "4q2g.h"

// class Amp4q2gStatic

const int
Amp4q2gStatic::flav[FC][NN] = {
    {-1,1,-2,2,0,0},
    {-1,2,-2,1,0,0}
};

const int
Amp4q2gStatic::fvsign[FC] = {1,-1};

const int
Amp4q2gStatic::ccsign[NN*(NN+1)/2] = {1,1,1,-1,1,1,1,-1,1,1,1,1,1,1,1,1,1,1,1,1,1};

const int
Amp4q2gStatic::fperm[FC][NN] = {
    {0,1,2,3,4,5},
    {0,3,2,1,4,5}
};

const int
Amp4q2gStatic::fvcol[FC][CC] = {
    {0,1,2,3,4,5,6,7,8,9,10,11,12,13},
    {9,10,11,6,7,8,3,4,5,0,1,2,13,12}
};

const unsigned char
Amp4q2gStatic::colmat[CC*(CC+1)/2] = {
    4,2,4,0,2,4,6,2,0,4,2,0,2,2,4,0,2,6,0,2,4,3,3,3,1,1,1,4,3,1,1,1,1,3,2,
    4,3,1,3,1,3,1,0,2,4,1,1,1,3,3,3,6,2,0,4,1,1,3,3,1,1,2,0,2,2,4,1,3,1,3,
    1,3,0,2,6,0,2,4,5,2,5,5,2,5,0,0,0,0,0,0,7,0,0,0,0,0,0,5,2,5,5,2,5,5,7
};

const unsigned char
Amp4q2gStatic::colmatcc[NN*(NN+1)/2][C0*(C0+1)/2] = {
    {},
    {0,10,0,14,10,0,0,7,15,0,7,15,7,10,0,15,7,0,14,10,0,12,12,12,5,5,
    5,21,5,3,3,3,3,5,0,17,3,9,3,9,3,9,7,0,18,5,5,5,12,12,12,22,
    0,7,21,3,3,5,5,3,3,0,10,0,0,17,9,3,9,3,9,3,7,0,17,7,0,18},
    {},
    {0,10,0,14,7,0,0,7,15,0,7,14,10,10,0,15,10,0,14,7,0,12,5,3,5,3,
    9,0,5,9,3,3,5,5,10,0,3,3,12,9,5,5,14,7,0,5,3,9,12,5,3,0,
    7,15,0,3,5,5,5,9,3,7,14,10,10,0,9,5,5,3,3,12,15,10,0,14,7,0},
    {18,0,17,7,0,21,17,0,7,18,0,10,0,0,17,7,0,22,7,0,21,3,5,12,9,3,
    5,0,3,3,5,9,3,12,7,0,3,3,12,9,5,5,14,7,0,9,3,5,3,5,12,0,
    10,15,0,9,3,12,3,3,5,10,15,10,7,0,9,5,5,3,3,12,15,10,0,14,7,0},
    {},
    {21,0,17,7,0,18,22,0,7,21,0,10,0,0,17,7,0,17,7,0,18,12,5,3,5,3,
    9,0,12,3,9,5,3,3,10,0,12,3,3,5,5,9,14,10,0,5,3,9,12,5,3,0,
    7,15,0,5,3,3,12,3,9,7,15,7,10,0,5,5,9,12,3,3,15,7,0,14,10,0},
    {0,7,0,14,10,0,0,10,15,0,10,14,7,7,0,15,7,0,14,10,0,3,5,12,9,3,
    5,0,5,5,3,3,9,5,7,0,12,3,3,5,5,9,14,10,0,9,3,5,3,5,12,0,
    10,15,0,3,9,5,5,5,3,10,14,7,7,0,5,5,9,12,3,3,15,7,0,14,10,0},
    {0,7,0,14,7,0,0,10,15,0,10,15,10,7,0,15,10,0,14,7,0,3,3,3,9,9,
    9,18,5,3,3,3,3,5,0,17,12,5,12,5,12,5,7,0,21,9,9,9,3,3,3,17,
    0,7,18,3,3,5,5,3,3,0,10,0,0,17,5,12,5,12,5,12,7,0,22,7,0,21},
    {},
    {4,0,0,4,6,0,4,0,6,11,0,6,0,0,11,4,4,0,6,0,0,1,1,1,2,2,
    2,11,1,2,2,2,2,1,0,11,1,13,8,2,1,8,6,0,0,1,1,1,1,1,1,4,
    0,4,4,1,8,8,1,2,13,0,6,4,0,0,1,8,13,1,2,8,6,0,0,4,6,0},
    {0,0,11,6,0,11,0,0,6,0,4,6,0,6,0,4,0,4,4,0,4,1,1,1,1,1,
    1,4,1,2,2,2,2,1,0,11,8,2,1,13,8,1,4,0,0,2,2,2,1,1,1,4,
    0,6,11,13,2,1,8,8,1,0,6,6,0,0,8,1,2,8,13,1,4,0,0,6,4,0},
    {0,0,11,4,0,4,0,0,4,0,6,6,0,4,0,6,0,4,6,0,11,8,1,1,8,13,
    2,0,8,2,1,13,8,1,4,0,1,2,1,2,1,2,6,0,11,13,2,1,8,8,1,0,
    6,6,0,2,2,1,1,2,2,0,6,0,0,11,1,1,1,1,1,1,4,0,4,4,0,4},
    {11,0,0,6,4,0,4,0,4,4,0,6,0,0,11,6,6,0,4,0,0,1,8,8,1,2,
    13,0,1,8,13,1,2,8,6,0,1,1,1,1,1,1,4,0,4,2,13,8,1,1,8,0,
    4,4,0,2,2,1,1,2,2,0,6,0,0,11,2,1,2,1,2,1,6,0,4,6,0,11},
    {},
    {11,0,11,6,0,0,4,0,4,4,0,6,4,0,0,6,0,0,4,6,0,1,1,1,1,1,
    1,4,1,2,13,1,8,8,0,0,1,2,8,1,8,13,4,6,0,2,2,2,1,1,1,4,
    0,6,11,2,2,1,1,2,2,0,6,0,0,11,2,1,8,1,13,8,4,4,0,6,0,0},
    {0,6,0,4,0,4,0,4,4,0,0,6,0,0,11,6,0,4,6,0,11,1,1,1,2,2,
    2,11,8,8,1,13,2,1,0,0,8,13,1,8,1,2,6,4,0,1,1,1,1,1,1,4,
    0,4,4,2,2,1,1,2,2,0,6,0,0,11,13,8,1,8,2,1,6,6,0,4,0,0},
    {0,4,0,6,0,11,0,6,6,0,0,6,0,0,11,4,0,4,4,0,4,8,8,1,13,2,
    1,0,1,2,2,2,2,1,0,11,1,1,1,1,1,1,4,0,4,8,13,2,8,1,1,0,
    0,4,0,13,8,1,8,2,1,6,6,0,4,0,2,1,2,1,2,1,6,0,4,6,0,11},
    {4,0,11,4,0,0,4,0,6,11,0,6,6,0,0,4,0,0,6,4,0,1,1,8,2,13,
    8,0,1,2,2,2,2,1,0,11,1,2,1,2,1,2,6,0,11,1,2,13,1,8,8,0,
    0,6,0,1,2,8,1,8,13,4,6,0,6,0,1,1,1,1,1,1,4,0,4,4,0,4},
    {19,0,0,20,0,19,0,0,20,19,0,24,0,0,0,20,0,0,20,0,19,0,0,0,16,16,
    16,19,0,23,16,16,23,0,0,0,0,16,0,16,0,16,20,0,19,16,16,16,0,0,0,0,
    0,20,19,16,23,0,0,23,16,0,24,0,0,0,16,0,16,0,16,0,20,0,0,20,0,19},
    {}
};

const int
Amp4q2gStatic::HSarr[HS][HSNN] = {
  { 1,-1, 1,-1, 1, 1}, { 1,-1, 1,-1, 1,-1}, { 1,-1, 1,-1,-1, 1}, { 1,-1, 1,-1,-1,-1},
  { 1,-1,-1, 1, 1, 1}, { 1,-1,-1, 1, 1,-1}, { 1,-1,-1, 1,-1, 1}, { 1,-1,-1, 1,-1,-1},
  {-1, 1,-1, 1,-1,-1}, {-1, 1,-1, 1,-1, 1}, {-1, 1,-1, 1, 1,-1}, {-1, 1,-1, 1, 1, 1},
  {-1, 1, 1,-1,-1,-1}, {-1, 1, 1,-1,-1, 1}, {-1, 1, 1,-1, 1,-1}, {-1, 1, 1,-1, 1, 1},
};

// class Amp4q2g2Static

const int
Amp4q2g2Static::HSarr[HS][HSNN] = {
  { 1, 1,-1,-1, 1, 1}, { 1, 1,-1,-1, 1,-1}, { 1, 1,-1,-1,-1, 1}, { 1, 1,-1,-1,-1,-1},
  { 1,-1, 1,-1, 1, 1}, { 1,-1, 1,-1, 1,-1}, { 1,-1, 1,-1,-1, 1}, { 1,-1, 1,-1,-1,-1},
  { 1,-1,-1, 1, 1, 1}, { 1,-1,-1, 1, 1,-1}, { 1,-1,-1, 1,-1, 1}, { 1,-1,-1, 1,-1,-1},
  {-1,-1, 1, 1,-1,-1}, {-1,-1, 1, 1,-1, 1}, {-1,-1, 1, 1, 1,-1}, {-1,-1, 1, 1, 1, 1},
  {-1, 1,-1, 1,-1,-1}, {-1, 1,-1, 1,-1, 1}, {-1, 1,-1, 1, 1,-1}, {-1, 1,-1, 1, 1, 1},
  {-1, 1, 1,-1,-1,-1}, {-1, 1, 1,-1,-1, 1}, {-1, 1, 1,-1, 1,-1}, {-1, 1, 1,-1, 1, 1},
};
