/*
* chsums/2q4g.cpp
*
* This file is part of NJet library
* Copyright (C) 2011, 2012 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#include <cassert>

#include "2q4g.h"
#include "../ngluon2/Model.h"

// class Amp2q4g

template <typename T>
Amp2q4g<T>::Amp2q4g(const T scalefactor, const int mFC, const NJetAmpTables& tables)
  : BaseClass(mFC, tables)
{
  for (int fv=0; fv<mFC; fv++) {
    BaseClass::initNG(scalefactor, fv);
    BaseClass::setProcess(StandardModel::NGluon1compat(NN, getFlav(fv)), fv);
  }

  initNc();
}

template <typename T>
void Amp2q4g<T>::setNc(const ST Nc_)
{
  BaseClass::setNc(Nc_);
  initNc();
}

template <typename T>
void Amp2q4g<T>::initNc()
{
  Nmat[ 0] = 0.;
  Nmat[ 1] = -Nc*V;
  Nmat[ 2] = Nc*(2.-V);
  Nmat[ 3] = Nc2*V;
  Nmat[ 4] = Nc;
  Nmat[ 5] = Nc*V*V;
  Nmat[ 6] = V;
  Nmat[ 7] = -(2.+V);
  Nmat[ 8] = -Nc2;
  Nmat[ 9] = 2.*Nc;
  Nmat[10] = -2.*Nc*V;
  Nmat[11] = Nc*(V-1.)*V;
  Nmat[12] = Nc*(1.-V);
  Nmat[13] = -1.;
  Nmat[14] = -2.*Nc2;
  Nmat[15] = V*V-2.;
  Nmat[16] = Nc2*(V-1.);
  Nmat[17] = -V*V;
  Nmat[18] = Nc*(1.+(V-1.)*V);
  Nmat[19] = Nc*(3.+Nc2);
  Nmat[20] = (1.+Nc2)*V;
  Nmat[21] = Nc*(2.+V);
  Nmat[22] = V*V*V;
  Nmat[23] = -(4.+3.*V);
  Nmat[24] = -Nc2*(V-2.);
  Nmat[25] = V-1.+V*V;
  Nmat[26] = Nc2*V*V;
  Nmat[27] = -Nc2*V;
  Nmat[28] = Nc2*(V-1.)*V;
  Nmat[29] = -2.*Nc2*V;
  Nmat[30] = Nc2*(3.+Nc2);
  Nmat[31] = Nc2*(1.+(V-1.)*V);
  assert(31 < BaseClass::NmatLen);

  Nmatcc[ 0] = 0.;
  Nmatcc[ 1] = -Nc2*V;
  Nmatcc[ 2] = Nc2;
  Nmatcc[ 3] = Nc2*V*V;
  Nmatcc[ 4] = Nc2+Nc4;
  Nmatcc[ 5] = Nc4*V;
  Nmatcc[ 6] = -Nc4;
  Nmatcc[ 7] = -Nc4*(-2.+V)-Nc2*V;
  Nmatcc[ 8] = Nc4;
  Nmatcc[ 9] = -(Nc2-2.)*Nc4;
  Nmatcc[10] = -Nc2*(1.+Nc2)*V;
  Nmatcc[11] = 2.*Nc4;
  Nmatcc[12] = -Nc4*V*V;
  Nmatcc[13] = -Nc4*V;
  Nmatcc[14] = Nc2+3.*Nc4;
  Nmatcc[15] = -(1.+Nc2)*Nc4;
  Nmatcc[16] = -Nc2*V*V*V;
  Nmatcc[17] = (Nc2-2.)*Nc4;
  Nmatcc[18] = Nc4-Nc2*(1.+Nc2)*V;
  Nmatcc[19] = -2.*Nc4*V;
  Nmatcc[20] = -1.-4.*Nc2+Nc4;
  Nmatcc[21] = -1.-2.*Nc2+Nc4;
  Nmatcc[22] = Nc2-2.*Nc4;
  Nmatcc[23] = -2.*Nc4;
  Nmatcc[24] = -(-3.+Nc2)*Nc4;
  Nmatcc[25] = 4.*Nc4;
  Nmatcc[26] = -1.-5.*Nc2+2.*Nc4;
  Nmatcc[27] = -1.-Nc2;
  Nmatcc[28] = 2.*Nc4*V;
  Nmatcc[29] = Nc4*Nc2;
  Nmatcc[30] = Nc2-3.*Nc4+Nc4*Nc2;
  Nmatcc[31] = -1.-3.*Nc2+3.*Nc4-Nc4*Nc2;
  Nmatcc[32] = -Nc4*Nc2;
  Nmatcc[33] = -2.*(Nc2-2.)*Nc4;
  Nmatcc[34] = 3.*Nc4-2.*Nc4*Nc2;
  Nmatcc[35] = -1.-3.*Nc2+Nc4;
  Nmatcc[36] = -1.-3.*Nc2;
  Nmatcc[37] = -1.;
  Nmatcc[38] = 2.*(Nc2-2.)*Nc4;
  Nmatcc[39] = -1.-Nc2*(Nc2-2.)*(Nc2-2.);
  Nmatcc[40] = -1.-Nc2*(6.+Nc2);
  Nmatcc[41] = -(1.+Nc2)*(1.+Nc2);
  Nmatcc[42] = -4.*Nc4;
  assert(42 < BaseClass::NmatccLen);

  bornFactor = V/Nc3;
  loopFactor = 2.*bornFactor;
  bornccFactor = -0.5*V/Nc4;
}

template <typename T>
void Amp2q4g<T>::getfvpart1_full(const int fv, LoopValue* fvpart)
{
  return getfvpart1_full_(fv, fvpart);
}

template <typename T>
void Amp2q4g<T>::getfvpart1_full(const int fv, LoopResult<T>* fvpart)
{
  return getfvpart1_full_(fv, fvpart);
}

// -------- BEGIN -- automatically generated code -- BEGIN -------- //

template <typename T>
void Amp2q4g<T>::getfvpart0(const int fv, TreeValue* fvpart, TreeValue* fvpartC)
{
  fvSet(fv);
  fvpart[0] = A0(0,1,2,3,4,5);
  fvpart[1] = A0(0,1,2,3,5,4);
  fvpart[2] = A0(0,1,2,4,3,5);
  fvpart[3] = A0(0,1,2,4,5,3);
  fvpart[4] = A0(0,1,2,5,3,4);
  fvpart[5] = A0(0,1,2,5,4,3);
  fvpart[6] = A0(0,1,3,2,4,5);
  fvpart[7] = A0(0,1,3,2,5,4);
  fvpart[8] = A0(0,1,3,4,2,5);
  fvpart[9] = A0(0,1,3,4,5,2);
  fvpart[10] = A0(0,1,3,5,2,4);
  fvpart[11] = A0(0,1,3,5,4,2);
  fvpart[12] = A0(0,1,4,2,3,5);
  fvpart[13] = A0(0,1,4,2,5,3);
  fvpart[14] = A0(0,1,4,3,2,5);
  fvpart[15] = A0(0,1,4,3,5,2);
  fvpart[16] = A0(0,1,4,5,2,3);
  fvpart[17] = A0(0,1,4,5,3,2);
  fvpart[18] = A0(0,1,5,2,3,4);
  fvpart[19] = A0(0,1,5,2,4,3);
  fvpart[20] = A0(0,1,5,3,2,4);
  fvpart[21] = A0(0,1,5,3,4,2);
  fvpart[22] = A0(0,1,5,4,2,3);
  fvpart[23] = A0(0,1,5,4,3,2);

  if (fvpartC) {
    for (int i=0; i<C0; i++) {
      fvpartC[i] = conj(fvpart[i]);
    }
  }
}

template <typename T>
template <typename LT>
inline
void Amp2q4g<T>::getfvpart1_full_(const int fv, LT* fvpart)
{
  fvSet(fv);
  {
    // 120 primitives
    const LT P123456 = AL(0,1,2,3,4,5);
    const LT P123465 = AL(0,1,2,3,5,4);
    const LT P123546 = AL(0,1,2,4,3,5);
    const LT P123564 = AL(0,1,2,4,5,3);
    const LT P123645 = AL(0,1,2,5,3,4);
    const LT P123654 = AL(0,1,2,5,4,3);
    const LT P124356 = AL(0,1,3,2,4,5);
    const LT P124365 = AL(0,1,3,2,5,4);
    const LT P124536 = AL(0,1,3,4,2,5);
    const LT P124563 = AL(0,1,3,4,5,2);
    const LT P124635 = AL(0,1,3,5,2,4);
    const LT P124653 = AL(0,1,3,5,4,2);
    const LT P125346 = AL(0,1,4,2,3,5);
    const LT P125364 = AL(0,1,4,2,5,3);
    const LT P125436 = AL(0,1,4,3,2,5);
    const LT P125463 = AL(0,1,4,3,5,2);
    const LT P125634 = AL(0,1,4,5,2,3);
    const LT P125643 = AL(0,1,4,5,3,2);
    const LT P126345 = AL(0,1,5,2,3,4);
    const LT P126354 = AL(0,1,5,2,4,3);
    const LT P126435 = AL(0,1,5,3,2,4);
    const LT P126453 = AL(0,1,5,3,4,2);
    const LT P126534 = AL(0,1,5,4,2,3);
    const LT P126543 = AL(0,1,5,4,3,2);
    const LT P132456 = AL(0,2,1,3,4,5);
    const LT P132465 = AL(0,2,1,3,5,4);
    const LT P132546 = AL(0,2,1,4,3,5);
    const LT P132564 = AL(0,2,1,4,5,3);
    const LT P132645 = AL(0,2,1,5,3,4);
    const LT P132654 = AL(0,2,1,5,4,3);
    const LT P134256 = AL(0,2,3,1,4,5);
    const LT P134265 = AL(0,2,3,1,5,4);
    const LT P134526 = AL(0,2,3,4,1,5);
    const LT P134562 = AL(0,2,3,4,5,1);
    const LT P134625 = AL(0,2,3,5,1,4);
    const LT P134652 = AL(0,2,3,5,4,1);
    const LT P135246 = AL(0,2,4,1,3,5);
    const LT P135264 = AL(0,2,4,1,5,3);
    const LT P135426 = AL(0,2,4,3,1,5);
    const LT P135462 = AL(0,2,4,3,5,1);
    const LT P135624 = AL(0,2,4,5,1,3);
    const LT P135642 = AL(0,2,4,5,3,1);
    const LT P136245 = AL(0,2,5,1,3,4);
    const LT P136254 = AL(0,2,5,1,4,3);
    const LT P136425 = AL(0,2,5,3,1,4);
    const LT P136452 = AL(0,2,5,3,4,1);
    const LT P136524 = AL(0,2,5,4,1,3);
    const LT P136542 = AL(0,2,5,4,3,1);
    const LT P142356 = AL(0,3,1,2,4,5);
    const LT P142365 = AL(0,3,1,2,5,4);
    const LT P142536 = AL(0,3,1,4,2,5);
    const LT P142563 = AL(0,3,1,4,5,2);
    const LT P142635 = AL(0,3,1,5,2,4);
    const LT P142653 = AL(0,3,1,5,4,2);
    const LT P143256 = AL(0,3,2,1,4,5);
    const LT P143265 = AL(0,3,2,1,5,4);
    const LT P143526 = AL(0,3,2,4,1,5);
    const LT P143562 = AL(0,3,2,4,5,1);
    const LT P143625 = AL(0,3,2,5,1,4);
    const LT P143652 = AL(0,3,2,5,4,1);
    const LT P145236 = AL(0,3,4,1,2,5);
    const LT P145263 = AL(0,3,4,1,5,2);
    const LT P145326 = AL(0,3,4,2,1,5);
    const LT P145362 = AL(0,3,4,2,5,1);
    const LT P145623 = AL(0,3,4,5,1,2);
    const LT P145632 = AL(0,3,4,5,2,1);
    const LT P146235 = AL(0,3,5,1,2,4);
    const LT P146253 = AL(0,3,5,1,4,2);
    const LT P146325 = AL(0,3,5,2,1,4);
    const LT P146352 = AL(0,3,5,2,4,1);
    const LT P146523 = AL(0,3,5,4,1,2);
    const LT P146532 = AL(0,3,5,4,2,1);
    const LT P152346 = AL(0,4,1,2,3,5);
    const LT P152364 = AL(0,4,1,2,5,3);
    const LT P152436 = AL(0,4,1,3,2,5);
    const LT P152463 = AL(0,4,1,3,5,2);
    const LT P152634 = AL(0,4,1,5,2,3);
    const LT P152643 = AL(0,4,1,5,3,2);
    const LT P153246 = AL(0,4,2,1,3,5);
    const LT P153264 = AL(0,4,2,1,5,3);
    const LT P153426 = AL(0,4,2,3,1,5);
    const LT P153462 = AL(0,4,2,3,5,1);
    const LT P153624 = AL(0,4,2,5,1,3);
    const LT P153642 = AL(0,4,2,5,3,1);
    const LT P154236 = AL(0,4,3,1,2,5);
    const LT P154263 = AL(0,4,3,1,5,2);
    const LT P154326 = AL(0,4,3,2,1,5);
    const LT P154362 = AL(0,4,3,2,5,1);
    const LT P154623 = AL(0,4,3,5,1,2);
    const LT P154632 = AL(0,4,3,5,2,1);
    const LT P156234 = AL(0,4,5,1,2,3);
    const LT P156243 = AL(0,4,5,1,3,2);
    const LT P156324 = AL(0,4,5,2,1,3);
    const LT P156342 = AL(0,4,5,2,3,1);
    const LT P156423 = AL(0,4,5,3,1,2);
    const LT P156432 = AL(0,4,5,3,2,1);
    const LT P162345 = AL(0,5,1,2,3,4);
    const LT P162354 = AL(0,5,1,2,4,3);
    const LT P162435 = AL(0,5,1,3,2,4);
    const LT P162453 = AL(0,5,1,3,4,2);
    const LT P162534 = AL(0,5,1,4,2,3);
    const LT P162543 = AL(0,5,1,4,3,2);
    const LT P163245 = AL(0,5,2,1,3,4);
    const LT P163254 = AL(0,5,2,1,4,3);
    const LT P163425 = AL(0,5,2,3,1,4);
    const LT P163452 = AL(0,5,2,3,4,1);
    const LT P163524 = AL(0,5,2,4,1,3);
    const LT P163542 = AL(0,5,2,4,3,1);
    const LT P164235 = AL(0,5,3,1,2,4);
    const LT P164253 = AL(0,5,3,1,4,2);
    const LT P164325 = AL(0,5,3,2,1,4);
    const LT P164352 = AL(0,5,3,2,4,1);
    const LT P164523 = AL(0,5,3,4,1,2);
    const LT P164532 = AL(0,5,3,4,2,1);
    const LT P165234 = AL(0,5,4,1,2,3);
    const LT P165243 = AL(0,5,4,1,3,2);
    const LT P165324 = AL(0,5,4,2,1,3);
    const LT P165342 = AL(0,5,4,2,3,1);
    const LT P165423 = AL(0,5,4,3,1,2);
    const LT P165432 = AL(0,5,4,3,2,1);

    fvpart[0] = P123456*Nc-P165432/Nc;
    fvpart[1] = P123465*Nc-P156432/Nc;
    fvpart[2] = P123546*Nc-P164532/Nc;
    fvpart[3] = P123564*Nc-P146532/Nc;
    fvpart[4] = P123645*Nc-P154632/Nc;
    fvpart[5] = P123654*Nc-P145632/Nc;
    fvpart[6] = P124356*Nc-P165342/Nc;
    fvpart[7] = P124365*Nc-P156342/Nc;
    fvpart[8] = P124536*Nc-P163542/Nc;
    fvpart[9] = P124563*Nc-P136542/Nc;
    fvpart[10] = P124635*Nc-P153642/Nc;
    fvpart[11] = P124653*Nc-P135642/Nc;
    fvpart[12] = P125346*Nc-P164352/Nc;
    fvpart[13] = P125364*Nc-P146352/Nc;
    fvpart[14] = P125436*Nc-P163452/Nc;
    fvpart[15] = P125463*Nc-P136452/Nc;
    fvpart[16] = P125634*Nc-P143652/Nc;
    fvpart[17] = P125643*Nc-P134652/Nc;
    fvpart[18] = P126345*Nc-P154362/Nc;
    fvpart[19] = P126354*Nc-P145362/Nc;
    fvpart[20] = P126435*Nc-P153462/Nc;
    fvpart[21] = P126453*Nc-P135462/Nc;
    fvpart[22] = P126534*Nc-P143562/Nc;
    fvpart[23] = P126543*Nc-P134562/Nc;
    fvpart[24] = P123456+P123465+P123546+P123564+P123645+P123654+P125346+P125364+P125634+P126345+P126354
    +P126534+P152346+P152364+P152634+P156234+P162345+P162354+P162534+P165234;
    fvpart[25] = P123456+P123465+P123546+P123564+P123645+P123654+P124356+P124365+P124635+P126345+P126354
    +P126435+P142356+P142365+P142635+P146235+P162345+P162354+P162435+P164235;
    fvpart[26] = P123456+P123465+P123546+P123564+P123645+P123654+P124356+P124365+P124536+P125346+P125364
    +P125436+P142356+P142365+P142536+P145236+P152346+P152364+P152436+P154236;
    fvpart[27] = P124356+P124365+P124536+P124563+P124635+P124653+P125436+P125463+P125643+P126435+P126453
    +P126543+P152436+P152463+P152643+P156243+P162435+P162453+P162543+P165243;
    fvpart[28] = P123456+P123465+P123645+P124356+P124365+P124536+P124563+P124635+P124653+P126345+P126435
    +P126453+P132456+P132465+P132645+P136245+P162345+P162435+P162453+P163245;
    fvpart[29] = P123456+P123465+P123546+P124356+P124365+P124536+P124563+P124635+P124653+P125346+P125436
    +P125463+P132456+P132465+P132546+P135246+P152346+P152436+P152463+P153246;
    fvpart[30] = P124536+P124563+P124653+P125346+P125364+P125436+P125463+P125634+P125643+P126453+P126534
    +P126543+P142536+P142563+P142653+P146253+P162453+P162534+P162543+P164253;
    fvpart[31] = P123546+P123564+P123654+P125346+P125364+P125436+P125463+P125634+P125643+P126354+P126534
    +P126543+P132546+P132564+P132654+P136254+P162354+P162534+P162543+P163254;
    fvpart[32] = P123456+P123546+P123564+P124356+P124536+P124563+P125346+P125364+P125436+P125463+P125634
    +P125643+P132456+P132546+P132564+P134256+P142356+P142536+P142563+P143256;
    fvpart[33] = P124563+P124635+P124653+P125463+P125634+P125643+P126345+P126354+P126435+P126453+P126534
    +P126543+P142563+P142635+P142653+P145263+P152463+P152634+P152643+P154263;
    fvpart[34] = P123564+P123645+P123654+P125364+P125634+P125643+P126345+P126354+P126435+P126453+P126534
    +P126543+P132564+P132645+P132654+P135264+P152364+P152634+P152643+P153264;
    fvpart[35] = P123465+P123645+P123654+P124365+P124635+P124653+P126345+P126354+P126435+P126453+P126534
    +P126543+P132465+P132645+P132654+P134265+P142365+P142635+P142653+P143265;
    fvpart[36] = -P123465-P123546-P123654-P124365-P124635-P124653-P125346-P125436-P125463-P126354
    -P126534-P126543-P142365-P142635-P142653-P146235-P146253-P146523-P152346-P152436-P152463
    -P154236-P154263-P154623-P162354-P162534-P162543-P165234-P165243-P165423;
    fvpart[37] = -P123456-P123564-P123645-P124356-P124536-P124563-P125364-P125634-P125643-P126345
    -P126435-P126453-P142356-P142536-P142563-P145236-P145263-P145623-P152364-P152634-P152643
    -P156234-P156243-P156423-P162345-P162435-P162453-P164235-P164253-P164523;
    fvpart[38] = -P123465-P123645-P123654-P124365-P124536-P124653-P125346-P125364-P125436-P126453
    -P126534-P126543-P132465-P132645-P132654-P136245-P136254-P136524-P152346-P152364-P152436
    -P153246-P153264-P153624-P162453-P162534-P162543-P165234-P165243-P165324;
    fvpart[39] = -P123456-P123546-P123564-P124356-P124563-P124635-P125463-P125634-P125643-P126345
    -P126354-P126435-P132456-P132546-P132564-P135246-P135264-P135624-P152463-P152634-P152643
    -P156234-P156243-P156324-P162345-P162354-P162435-P163245-P163254-P163524;
    fvpart[40] = -P123564-P123645-P123654-P124356-P124365-P124536-P125364-P125436-P125643-P126435
    -P126453-P126543-P132564-P132645-P132654-P136245-P136254-P136425-P142356-P142365-P142536
    -P143256-P143265-P143625-P162435-P162453-P162543-P164235-P164253-P164325;
    fvpart[41] = -P123456-P123465-P123546-P124563-P124635-P124653-P125346-P125463-P125634-P126345
    -P126354-P126534-P132456-P132465-P132546-P134256-P134265-P134625-P142563-P142635-P142653
    -P146235-P146253-P146325-P162345-P162354-P162534-P163245-P163254-P163425;
    fvpart[42] = -P123546-P123564-P123654-P124356-P124365-P124635-P125436-P125463-P125643-P126354
    -P126435-P126543-P132546-P132564-P132654-P135246-P135264-P135426-P142356-P142365-P142635
    -P143256-P143265-P143526-P152436-P152463-P152643-P154236-P154263-P154326;
    fvpart[43] = -P123456-P123465-P123645-P124536-P124563-P124653-P125346-P125364-P125634-P126345
    -P126453-P126534-P132456-P132465-P132645-P134256-P134265-P134526-P142536-P142563-P142653
    -P145236-P145263-P145326-P152346-P152364-P152634-P153246-P153264-P153426;
    fvpart[44] = P123654+P124365+P125436+P126543+P132654+P136254+P136524+P136542+P142365+P143265+P143625
    +P143652+P152436+P154236+P154326+P154362+P162543+P165243+P165423+P165432;
    fvpart[45] = P123564+P124356+P125643+P126435+P132564+P135264+P135624+P135642+P142356+P143256+P143526
    +P143562+P152643+P156243+P156423+P156432+P162435+P164235+P164325+P164352;
    fvpart[46] = P123645+P124536+P125364+P126453+P132645+P136245+P136425+P136452+P142536+P145236+P145326
    +P145362+P152364+P153264+P153624+P153642+P162453+P164253+P164523+P164532;
    fvpart[47] = P123465+P124653+P125346+P126534+P132465+P134265+P134625+P134652+P142653+P146253+P146523
    +P146532+P152346+P153246+P153426+P153462+P162534+P165234+P165324+P165342;
    fvpart[48] = P123546+P124635+P125463+P126354+P132546+P135246+P135426+P135462+P142635+P146235+P146325
    +P146352+P152463+P154263+P154623+P154632+P162354+P163254+P163524+P163542;
    fvpart[49] = P123456+P124563+P125634+P126345+P132456+P134256+P134526+P134562+P142563+P145263+P145623
    +P145632+P152634+P156234+P156324+P156342+P162345+P163245+P163425+P163452;

  }
  if (Nf != 0.) {
    // 33 primitives
    const LT Q123456 = AF(0,1,2,3,4,5);
    const LT Q123465 = AF(0,1,2,3,5,4);
    const LT Q123546 = AF(0,1,2,4,3,5);
    const LT Q123564 = AF(0,1,2,4,5,3);
    const LT Q123645 = AF(0,1,2,5,3,4);
    const LT Q123654 = AF(0,1,2,5,4,3);
    const LT Q124356 = AF(0,1,3,2,4,5);
    const LT Q124365 = AF(0,1,3,2,5,4);
    const LT Q124536 = AF(0,1,3,4,2,5);
    const LT Q124563 = AF(0,1,3,4,5,2);
    const LT Q124635 = AF(0,1,3,5,2,4);
    const LT Q124653 = AF(0,1,3,5,4,2);
    const LT Q125346 = AF(0,1,4,2,3,5);
    const LT Q125364 = AF(0,1,4,2,5,3);
    const LT Q125436 = AF(0,1,4,3,2,5);
    const LT Q125634 = AF(0,1,4,5,2,3);
    const LT Q125643 = AF(0,1,4,5,3,2);
    const LT Q126345 = AF(0,1,5,2,3,4);
    const LT Q126354 = AF(0,1,5,2,4,3);
    const LT Q126435 = AF(0,1,5,3,2,4);
    const LT Q126453 = AF(0,1,5,3,4,2);
    const LT Q126534 = AF(0,1,5,4,2,3);
    const LT Q126543 = AF(0,1,5,4,3,2);
    const LT Q132456 = AF(0,2,1,3,4,5);
    const LT Q132645 = AF(0,2,1,5,3,4);
    const LT Q142356 = AF(0,3,1,2,4,5);
    const LT Q142536 = AF(0,3,1,4,2,5);
    const LT Q142635 = AF(0,3,1,5,2,4);
    const LT Q152346 = AF(0,4,1,2,3,5);
    const LT Q152436 = AF(0,4,1,3,2,5);
    const LT Q162435 = AF(0,5,1,3,2,4);
    const LT Q162453 = AF(0,5,1,3,4,2);
    const LT Q162534 = AF(0,5,1,4,2,3);

    fvpart[0] += Nf*(-Q123456);
    fvpart[1] += Nf*(-Q123465);
    fvpart[2] += Nf*(-Q123546);
    fvpart[3] += Nf*(-Q123564);
    fvpart[4] += Nf*(-Q123645);
    fvpart[5] += Nf*(-Q123654);
    fvpart[6] += Nf*(-Q124356);
    fvpart[7] += Nf*(-Q124365);
    fvpart[8] += Nf*(-Q124536);
    fvpart[9] += Nf*(-Q124563);
    fvpart[10] += Nf*(-Q124635);
    fvpart[11] += Nf*(-Q124653);
    fvpart[12] += Nf*(-Q125346);
    fvpart[13] += Nf*(-Q125364);
    fvpart[14] += Nf*(-Q125436);
    fvpart[15] += Nf*(-(-Q123456-Q123465+Q123546+Q123564-Q123645-Q123654+Q124356+Q124365+Q124536-Q124563
    +Q124635+Q124653+Q125346+Q125364+Q125436+Q125634-Q125643+Q126345+Q126354+Q126435+Q126453
    +Q126534-Q126543-2.*Q132456+2.*Q142356+2.*Q142635+2.*Q152436+2.*Q162453+2.*Q162534));
    fvpart[16] += Nf*(-Q125634);
    fvpart[17] += Nf*(-Q125643);
    fvpart[18] += Nf*(-Q126345);
    fvpart[19] += Nf*(-Q126354);
    fvpart[20] += Nf*(-Q126435);
    fvpart[21] += Nf*(-Q126453);
    fvpart[22] += Nf*(-Q126534);
    fvpart[23] += Nf*(-Q126543);
    fvpart[36] += Nf*((Q123546+Q123564+Q124653+Q126453-Q132456+Q142356+Q162453)/Nc);
    fvpart[37] += Nf*((Q123546+Q123564+Q124653+Q126453-Q132456+Q142356+Q162453)/Nc);
    fvpart[38] += Nf*((Q124356+Q124365+Q125634+Q126534-Q142536+Q152436+Q162534)/Nc);
    fvpart[39] += Nf*((Q124356+Q124365+Q125634+Q126534-Q142536+Q152436+Q162534)/Nc);
    fvpart[40] += Nf*((Q124635+Q125346+Q125364+Q126435+Q142536-Q152436+Q162435)/Nc);
    fvpart[41] += Nf*((Q124635+Q125346+Q125364+Q126435+Q142536-Q152436+Q162435)/Nc);
    fvpart[42] += Nf*((Q124536+Q125436+Q126345+Q126354+Q142635+Q152436-Q162435)/Nc);
    fvpart[43] += Nf*((Q124536+Q125436+Q126345+Q126354+Q142635+Q152436-Q162435)/Nc);
    fvpart[44] += Nf*((Q123456-Q124365+Q124563-Q125436+Q132456-Q152436)/Nc);
    fvpart[45] += Nf*((Q123465-Q123564-Q124356+Q125346-Q142356+Q152346)/Nc);
    fvpart[46] += Nf*((-Q123645+Q124635+Q126354-Q126453-Q132645+Q142635)/Nc);
    fvpart[47] += Nf*((-Q123465+Q123564+Q124356-Q125346+Q142356-Q152346)/Nc);
    fvpart[48] += Nf*((Q123645-Q124635-Q126354+Q126453+Q132645-Q142635)/Nc);
    fvpart[49] += Nf*((-Q123456+Q124365-Q124563+Q125436-Q132456+Q152436)/Nc);
  }
}

// --------- END --- automatically generated code --- END --------- //

#ifdef USE_SD
  template class Amp2q4g<double>;
#endif
#ifdef USE_DD
  template class Amp2q4g<dd_real>;
#endif
#ifdef USE_QD
  template class Amp2q4g<qd_real>;
#endif
#ifdef USE_VC
  template class Amp2q4g<Vc::double_v>;
#endif
