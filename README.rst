========
Overview
========

NJet [1]_ [2]_ is a C++ library for multi-parton one-loop matrix elements.

The latest version of the code is available at the project homepage
https://bitbucket.org/njet/njet

.. NOTE this file is written using reStructuredText markup format
.. see http://docutils.sourceforge.net/rst.html for details

Installation
============

To build on Unix/Linux and similar systems use::

   ./configure
   make
   make install

see INSTALL file for the detailed explanation of ``./configure`` options


Dependencies
------------

NJet is distributed with adapted versions of QCDLoop [3]_ [4]_ for scalar loop integrals
and the qd package [5]_ for double-double and quad-double precision.


Cross Checks and Examples
-------------------------

After successful installation::

  make check

will create examples of evaluation for 3 and 4 jets via the Binoth Les Houches
Accord interface [6]_ [7]_. For instance command::

  ./ex_3j

will run some tests for 3 jets using the order file found in the examples directory.
The python script ``njet.py``, found in the bin directory of the installation prefix,
processes the order file to create the contract file used by NJet::

  njet.py -o <output_contract_file> <input_order_file>

See::

  njet.py --help

for more options.


Usage
=====

A more complete documentation of the usage of the NJet library can be found
at NJet Project wiki pages https://bitbucket.org/njet/njet/wiki/.

Run ``make check`` to compile short example programs in the ``examples`` directory.

We first must prepare an BLHA order file, e.g. 5 gluon and qqb+3 gluon amplitudes.
NJet supports both BLHA1 and BLHA2 versions of the interface see [6]_ [7]_ for the details
and ``blha/BLHA-doc.txt`` for description of NJet-specific parameters.

::

  # a comment...
  CorrectionType          QCD
  IRregularisation        CDR
  AlphasPower             2
  UnknownParameter        123   # example of bad parameter
  NJetReturnAccuracy      2     # example of NJet-specific parameter
  # process list
  21 21 -> 21 21 21
  -1 1 -> 21 21 21

This file can be saved as OLE_order_3j.lh. The file is processed with::

  njet.py -o OLE_contract.lh OLE_order_3j.lh

to produce ``OLE_contract.lh`` which can be read by NJet.

If there were any problems with the order file (e.g. unsupported parameter)
``njet.py`` will mark it as invalid and write a comment about it in the contract.
NJet library will refuse to use a contract with errors in it, therefore it is
responsibility of the user to check that all necessary parameters have been recognized.

The unknown parameters should be corrected in the order file or simply removed
from the contract file (or both). The removal of unrecognized options is the only
allowed manual change to the contract file. Any other changes must be done in
the order file first and then passed through ``njet.py`` to get a new contract.

Having a valid contract file we can then use inside the C++ code::

  #include "njet.h"
  int status;
  OLP_Start("OLE_contract_3j.lh", &status);
  OLP_EvalSubProcess2(<process=1,2>, &momenta[0], muR, &out[0], &racc);
  // or for BLHA1
  OLP_EvalSubProcess(<process=1,2>, &momenta[0], muR, &alphas[0], &out[0]);

Note that in BLHA2 ``alphas`` has to be set by calling SetParameter::

  OLP_SetParameter("alphas", &alphas, &zero, &rstatus);

The code should be compiled and linked with ``libnjet2``::

  c++ <program.cpp> -c -I<yourpath>/include
  c++ <program.o> -L<yourpath>/lib -lnjet2

In addition, examples directory contains a variety of tests using NJet Python interface.

Selected tests can be run via ``runtest.py``, for instance::

  examples/runtest.py --run NJ_2j,ML_V2j,ML_A2j

will run tests for pp->2j, pp->2j+V, pp->2j+A.

To show full list of available test with short description use::

  examples/runtest.py --list

Limitations
-----------

Massive top quarks circulating inside the loop processes are excluded.
Contributions of Z-boson or Photon coupled directly to a quark-loop are excluded
(for some processes they are available as a separate AmplitudeType ``loopax``).


Compilers
---------

NJet has been mainly tested with the GNU C compiler gcc. Compilation with
Intel compilers is supported. We have also tested the open source
`clang`__ compiler which delivered favourable compilation and running times.

__ http://clang.llvm.org/

Hints and Tips for Compilation
------------------------------

If you encounter any problems with fortran compilers during installation try
to specify a working fortran compiler during configuration. For example to use
the Intel compiler suite you might try::

  ./configure FC=ifort F77=ifort CXX=icpc CC=icc

note both FC and F77 must be specified.

Modern fortran compilers may require the flag::

  FFLAGS='-std=legacy' ./configure

In case of other problems please check for updates at https://bitbucket.org/njet/njet.
Bugs and other issues with the code may also be reported there.

File and Directory Structure
----------------------------

After unpacking the tar archive njet-2.x.x.tar.gz you should find the following source files

blha/ - source code for the BLHA interface
chsums/ - source code for colour sums and squared amplitudes
ngluon2/ - source code for primitive amplitude computations
analytic/ - source code for primitive amplitudes known analytically
tools/ - additonal tools for tests and examples
examples/ - some examples for linking via the BLHA


References
==========

.. [1] Badger, Biedermann, Uwer, Yundin,
       *Numerical evaluation of virtual corrections to multi-jet production in massless QCD*,
       `[arXiv:1209.0100]`__.
       https://bitbucket.org/njet/njet/
__ http://arxiv.org/abs/1209.0100

.. [2] Badger, Biedermann, Uwer, *NGluon: A Package to Calculate One-loop Multi-gluon Amplitudes*,
       Comput.Phys.Commun. 182 (2011) 1674–1692, `[arXiv:1011.2900]`__.
       http://www.physik.hu-berlin.de/pep/tools/
__ http://arxiv.org/abs/1011.2900

.. [3] Ellis, Zanderighi, *Scalar one-loop integrals for QCD*,
       JHEP 0802 (2008) 002, `[arXiv:0712.1851]`__.
       http://qcdloop.fnal.gov/
__ http://arxiv.org/abs/0712.1851

.. [4] van Oldenborgh, *FF: A Package to evaluate one loop Feynman diagrams*,
       Comput.Phys.Commun. 66 (1991) 1-15, `doi:10.1016/0010-4655(91)90002-3`__.
__ http://dx.doi.org/10.1016/0010-4655(91)90002-3

.. [5] Hida, Li, Bailey, *libqd: quad-double / double-double computation package*,
       http://crd.lbl.gov/~dhbailey/mpdist/

.. [6] Binoth et al., *A Proposal for a standard interface between Monte Carlo tools and one-loop programs*,
       Comput.Phys.Commun. 181 (2010) 1612-1622, `[arXiv:1001.1307]`__.
__ http://arxiv.org/abs/1001.1307

.. [7] Alioli et al., *Update of the Binoth Les Houches Accord for a standard interface between Monte Carlo tools and one-loop programs*,
       `[arXiv:1308.3462]`__.
__ http://arxiv.org/abs/1308.3462
